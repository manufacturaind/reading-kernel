#!/bin/zsh

pygmentize -f html $1 |                        # run pygments to create HTML
sed -E 's/^[ \t]*//' |                         # remove whitespaces at line start
sed -E '/^\s*$/d' |                            # delete empty lines
sed -E 's/^<span/<p><span/' |                  # add <p> tag on lines that begin with a span
sed 's/span>$/span><\/p>/g' |                  # add closing </p> tag on those lines
sed -E 's/<(\/)?pre>//g' |                     # remove <pre> tag
sed -E '/"cm"/s/<p>/<p class="comment">/g' |   # add "comment" class on lines that contain a comment (which contain the "cm" string)
sed -E "s/(\/\*|\*\/| \* )//g" |             # remove comment markers
sed -E '/<p class="comment"><span class="cm">[ \t\*]*<\/span><\/p>/d' > $2
