
  linux/kernel/acct.c

  BSD Process Accounting for Linux

  Author: Marco van Wieringen <mvw@planets.elm.net>

  Some code based on ideas and code from:
  Thomas K. Dyas <tdyas@eden.rutgers.edu>

  This file implements BSD-style process accounting. Whenever any
  process exits, an accounting record of type "struct acct" is
  written to the file specified with the acct() system call. It is
  up to user-level programs to do useful things with the accounting
  log. The kernel just provides the raw accounting information.

 (C) Copyright 1995 - 1997 Marco van Wieringen - ELM Consultancy B.V.

  Plugged two leaks. 1) It didn't return acct_file into the free_filps if
  the file happened to be read-only. 2) If the accounting was suspended
  due to the lack of space it happily allowed to reopen it and completely
  lost the old acct_file. 3/10/98, Al Viro.

  Now we silently close acct_file on attempt to reopen. Cleaned sys_acct().
  XTerms and EMACS are manifestations of pure evil. 21/10/98, AV.

  Fixed a nasty interaction with with sys_umount(). If the accointing
  was suspeneded we failed to stop it on umount(). Messy.
  Another one: remount to readonly didn't stop accounting.
	Question: what should we do if we have CAP_SYS_ADMIN but not
  CAP_SYS_PACCT? Current code does the following: umount returns -EBUSY
  unless we are messing with the root. In that case we are getting a
  real mess with do_remount_sb(). 9/11/98, AV.

  Fixed a bunch of races (and pair of leaks). Probably not the best way,
  but this one obviously doesn't introduce deadlocks. Later. BTW, found
  one race (and leak) in BSD implementation.
  OK, that's better. ANOTHER race and leak in BSD variant. There always
  is one more bug... 10/11/98, AV.

	Oh, fsck... Oopsable SMP race in do_process_acct() - we must hold
 ->mmap_sem to walk the vma list of current->mm. Nasty, since it leaks
 a struct file opened for write. Fixed. 2/6/2000, AV.
 /*

#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/acct.h>
#include <linux/capability.h>
#include <linux/file.h>
#include <linux/tty.h>
#include <linux/security.h>
#include <linux/vfs.h>
#include <linux/jiffies.h>
#include <linux/times.h>
#include <linux/syscalls.h>
#include <linux/mount.h>
#include <linux/uaccess.h>
#include <asm/div64.h>
#include <linux/blkdev.h>/ sector_div /*
#include <linux/pid_namespace.h>
#include <linux/fs_pin.h>

*/
 These constants control the amount of freespace that suspend and
 resume the process accounting system, and the time delay between
 each check.
 Turned into sysctl-controllable parameters. AV, 12/11/98
 /*

int acct_parm[3] = {4, 2, 30};
#define RESUME		(acct_parm[0])	*/ >foo% free space - resume /*
#define SUSPEND		(acct_parm[1])	*/ <foo% free space - suspend /*
#define ACCT_TIMEOUT	(acct_parm[2])	*/ foo second timeout between checks /*

*/
 External references and all of the globals.
 /*

struct bsd_acct_struct {
	struct fs_pin		pin;
	atomic_long_t		count;
	struct rcu_head		rcu;
	struct mutex		lock;
	int			active;
	unsigned long		needcheck;
	struct file		*file;
	struct pid_namespace	*ns;
	struct work_struct	work;
	struct completion	done;
};

static void do_acct_process(struct bsd_acct_structacct);

*/
 Check the amount of free space and suspend/resume accordingly.
 /*
static int check_free_space(struct bsd_acct_structacct)
{
	struct kstatfs sbuf;

	if (time_is_before_jiffies(acct->needcheck))
		goto out;

	*/ May block /*
	if (vfs_statfs(&acct->file->f_path, &sbuf))
		goto out;

	if (acct->active) {
		u64 suspend = sbuf.f_blocks SUSPEND;
		do_div(suspend, 100);
		if (sbuf.f_bavail <= suspend) {
			acct->active = 0;
			pr_info("Process accounting paused\n");
		}
	} else {
		u64 resume = sbuf.f_blocks RESUME;
		do_div(resume, 100);
		if (sbuf.f_bavail >= resume) {
			acct->active = 1;
			pr_info("Process accounting resumed\n");
		}
	}

	acct->needcheck = jiffies + ACCT_TIMEOUT*HZ;
out:
	return acct->active;
}

static void acct_put(struct bsd_acct_structp)
{
	if (atomic_long_dec_and_test(&p->count))
		kfree_rcu(p, rcu);
}

static inline struct bsd_acct_structto_acct(struct fs_pinp)
{
	return p ? container_of(p, struct bsd_acct_struct, pin) : NULL;
}

static struct bsd_acct_structacct_get(struct pid_namespacens)
{
	struct bsd_acct_structres;
again:
	smp_rmb();
	rcu_read_lock();
	res = to_acct(ACCESS_ONCE(ns->bacct));
	if (!res) {
		rcu_read_unlock();
		return NULL;
	}
	if (!atomic_long_inc_not_zero(&res->count)) {
		rcu_read_unlock();
		cpu_relax();
		goto again;
	}
	rcu_read_unlock();
	mutex_lock(&res->lock);
	if (res != to_acct(ACCESS_ONCE(ns->bacct))) {
		mutex_unlock(&res->lock);
		acct_put(res);
		goto again;
	}
	return res;
}

static void acct_pin_kill(struct fs_pinpin)
{
	struct bsd_acct_structacct = to_acct(pin);
	mutex_lock(&acct->lock);
	do_acct_process(acct);
	schedule_work(&acct->work);
	wait_for_completion(&acct->done);
	cmpxchg(&acct->ns->bacct, pin, NULL);
	mutex_unlock(&acct->lock);
	pin_remove(pin);
	acct_put(acct);
}

static void close_work(struct work_structwork)
{
	struct bsd_acct_structacct = container_of(work, struct bsd_acct_struct, work);
	struct filefile = acct->file;
	if (file->f_op->flush)
		file->f_op->flush(file, NULL);
	__fput_sync(file);
	complete(&acct->done);
}

static int acct_on(struct filenamepathname)
{
	struct filefile;
	struct vfsmountmnt,internal;
	struct pid_namespacens = task_active_pid_ns(current);
	struct bsd_acct_structacct;
	struct fs_pinold;
	int err;

	acct = kzalloc(sizeof(struct bsd_acct_struct), GFP_KERNEL);
	if (!acct)
		return -ENOMEM;

	*/ Difference from BSD - they don't do O_APPEND /*
	file = file_open_name(pathname, O_WRONLY|O_APPEND|O_LARGEFILE, 0);
	if (IS_ERR(file)) {
		kfree(acct);
		return PTR_ERR(file);
	}

	if (!S_ISREG(file_inode(file)->i_mode)) {
		kfree(acct);
		filp_close(file, NULL);
		return -EACCES;
	}

	if (!(file->f_mode & FMODE_CAN_WRITE)) {
		kfree(acct);
		filp_close(file, NULL);
		return -EIO;
	}
	internal = mnt_clone_internal(&file->f_path);
	if (IS_ERR(internal)) {
		kfree(acct);
		filp_close(file, NULL);
		return PTR_ERR(internal);
	}
	err = mnt_want_write(internal);
	if (err) {
		mntput(internal);
		kfree(acct);
		filp_close(file, NULL);
		return err;
	}
	mnt = file->f_path.mnt;
	file->f_path.mnt = internal;

	atomic_long_set(&acct->count, 1);
	init_fs_pin(&acct->pin, acct_pin_kill);
	acct->file = file;
	acct->needcheck = jiffies;
	acct->ns = ns;
	mutex_init(&acct->lock);
	INIT_WORK(&acct->work, close_work);
	init_completion(&acct->done);
	mutex_lock_nested(&acct->lock, 1);	*/ nobody has seen it yet /*
	pin_insert(&acct->pin, mnt);

	rcu_read_lock();
	old = xchg(&ns->bacct, &acct->pin);
	mutex_unlock(&acct->lock);
	pin_kill(old);
	mnt_drop_write(mnt);
	mntput(mnt);
	return 0;
}

static DEFINE_MUTEX(acct_on_mutex);

*/
 sys_acct - enable/disable process accounting
 @name: file name for accounting records or NULL to shutdown accounting

 Returns 0 for success or negative errno values for failure.

 sys_acct() is the only system call needed to implement process
 accounting. It takes the name of the file where accounting records
 should be written. If the filename is NULL, accounting will be
 shutdown.
 /*
SYSCALL_DEFINE1(acct, const char __user, name)
{
	int error = 0;

	if (!capable(CAP_SYS_PACCT))
		return -EPERM;

	if (name) {
		struct filenametmp = getname(name);

		if (IS_ERR(tmp))
			return PTR_ERR(tmp);
		mutex_lock(&acct_on_mutex);
		error = acct_on(tmp);
		mutex_unlock(&acct_on_mutex);
		putname(tmp);
	} else {
		rcu_read_lock();
		pin_kill(task_active_pid_ns(current)->bacct);
	}

	return error;
}

void acct_exit_ns(struct pid_namespacens)
{
	rcu_read_lock();
	pin_kill(ns->bacct);
}

*/
  encode an unsigned long into a comp_t

  This routine has been adopted from the encode_comp_t() function in
  the kern_acct.c file of the FreeBSD operating system. The encoding
  is a 13-bit fraction with a 3-bit (base 8) exponent.
 /*

#define	MANTSIZE	13			*/ 13 bit mantissa. /*
#define	EXPSIZE		3			*/ Base 8 (3 bit) exponent. /*
#define	MAXFRACT	((1 << MANTSIZE) - 1)	*/ Maximum fractional value. /*

static comp_t encode_comp_t(unsigned long value)
{
	int exp, rnd;

	exp = rnd = 0;
	while (value > MAXFRACT) {
		rnd = value & (1 << (EXPSIZE - 1));	*/ Round up? /*
		value >>= EXPSIZE;	*/ Base 8 exponent == 3 bit shift. /*
		exp++;
	}

	*/
	 If we need to round up, do it (and handle overflow correctly).
	 /*
	if (rnd && (++value > MAXFRACT)) {
		value >>= EXPSIZE;
		exp++;
	}

	*/
	 Clean it up and polish it off.
	 /*
	exp <<= MANTSIZE;		*/ Shift the exponent into place /*
	exp += value;			*/ and add on the mantissa. /*
	return exp;
}

#if ACCT_VERSION == 1 || ACCT_VERSION == 2
*/
 encode an u64 into a comp2_t (24 bits)

 Format: 5 bit base 2 exponent, 20 bits mantissa.
 The leading bit of the mantissa is not stored, but implied for
 non-zero exponents.
 Largest encodable value is 50 bits.
 /*

#define MANTSIZE2       20                     / 20 bit mantissa. /*
#define EXPSIZE2        5                      / 5 bit base 2 exponent. /*
#define MAXFRACT2       ((1ul << MANTSIZE2) - 1)/ Maximum fractional value. /*
#define MAXEXP2         ((1 << EXPSIZE2) - 1)   / Maximum exponent. /*

static comp2_t encode_comp2_t(u64 value)
{
	int exp, rnd;

	exp = (value > (MAXFRACT2>>1));
	rnd = 0;
	while (value > MAXFRACT2) {
		rnd = value & 1;
		value >>= 1;
		exp++;
	}

	*/
	 If we need to round up, do it (and handle overflow correctly).
	 /*
	if (rnd && (++value > MAXFRACT2)) {
		value >>= 1;
		exp++;
	}

	if (exp > MAXEXP2) {
		*/ Overflow. Return largest representable number instead. /*
		return (1ul << (MANTSIZE2+EXPSIZE2-1)) - 1;
	} else {
		return (value & (MAXFRACT2>>1)) | (exp << (MANTSIZE2-1));
	}
}
#endif

#if ACCT_VERSION == 3
*/
 encode an u64 into a 32 bit IEEE float
 /*
static u32 encode_float(u64 value)
{
	unsigned exp = 190;
	unsigned u;

	if (value == 0)
		return 0;
	while ((s64)value > 0) {
		value <<= 1;
		exp--;
	}
	u = (u32)(value >> 40) & 0x7fffffu;
	return u | (exp << 23);
}
#endif

*/
  Write an accounting entry for an exiting process

  The acct_process() call is the workhorse of the process
  accounting system. The struct acct is built here and then written
  into the accounting file. This function should only be called from
  do_exit() or when switching to a different output file.
 /*

static void fill_ac(acct_tac)
{
	struct pacct_structpacct = &current->signal->pacct;
	u64 elapsed, run_time;
	struct tty_structtty;

	*/
	 Fill the accounting struct with the needed info as recorded
	 by the different kernel functions.
	 /*
	memset(ac, 0, sizeof(acct_t));

	ac->ac_version = ACCT_VERSION | ACCT_BYTEORDER;
	strlcpy(ac->ac_comm, current->comm, sizeof(ac->ac_comm));

	*/ calculate run_time in nsec/*
	run_time = ktime_get_ns();
	run_time -= current->group_leader->start_time;
	*/ convert nsec -> AHZ /*
	elapsed = nsec_to_AHZ(run_time);
#if ACCT_VERSION == 3
	ac->ac_etime = encode_float(elapsed);
#else
	ac->ac_etime = encode_comp_t(elapsed < (unsigned long) -1l ?
				(unsigned long) elapsed : (unsigned long) -1l);
#endif
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
	{
		*/ new enlarged etime field /*
		comp2_t etime = encode_comp2_t(elapsed);

		ac->ac_etime_hi = etime >> 16;
		ac->ac_etime_lo = (u16) etime;
	}
#endif
	do_div(elapsed, AHZ);
	ac->ac_btime = get_seconds() - elapsed;
#if ACCT_VERSION==2
	ac->ac_ahz = AHZ;
#endif

	spin_lock_irq(&current->sighand->siglock);
	tty = current->signal->tty;	*/ Safe as we hold the siglock /*
	ac->ac_tty = tty ? old_encode_dev(tty_devnum(tty)) : 0;
	ac->ac_utime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct->ac_utime)));
	ac->ac_stime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct->ac_stime)));
	ac->ac_flag = pacct->ac_flag;
	ac->ac_mem = encode_comp_t(pacct->ac_mem);
	ac->ac_minflt = encode_comp_t(pacct->ac_minflt);
	ac->ac_majflt = encode_comp_t(pacct->ac_majflt);
	ac->ac_exitcode = pacct->ac_exitcode;
	spin_unlock_irq(&current->sighand->siglock);
}
*/
  do_acct_process does all actual work. Caller holds the reference to file.
 /*
static void do_acct_process(struct bsd_acct_structacct)
{
	acct_t ac;
	unsigned long flim;
	const struct credorig_cred;
	struct filefile = acct->file;

	*/
	 Accounting records are not subject to resource limits.
	 /*
	flim = current->signal->rlim[RLIMIT_FSIZE].rlim_cur;
	current->signal->rlim[RLIMIT_FSIZE].rlim_cur = RLIM_INFINITY;
	*/ Perform file operations on behalf of whoever enabled accounting /*
	orig_cred = override_creds(file->f_cred);

	*/
	 First check to see if there is enough free_space to continue
	 the process accounting system.
	 /*
	if (!check_free_space(acct))
		goto out;

	fill_ac(&ac);
	*/ we really need to bite the bullet and change layout /*
	ac.ac_uid = from_kuid_munged(file->f_cred->user_ns, orig_cred->uid);
	ac.ac_gid = from_kgid_munged(file->f_cred->user_ns, orig_cred->gid);
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
	*/ backward-compatible 16 bit fields /*
	ac.ac_uid16 = ac.ac_uid;
	ac.ac_gid16 = ac.ac_gid;
#endif
#if ACCT_VERSION == 3
	{
		struct pid_namespacens = acct->ns;

		ac.ac_pid = task_tgid_nr_ns(current, ns);
		rcu_read_lock();
		ac.ac_ppid = task_tgid_nr_ns(rcu_dereference(current->real_parent),
					     ns);
		rcu_read_unlock();
	}
#endif
	*/
	 Get freeze protection. If the fs is frozen, just skip the write
	 as we could deadlock the system otherwise.
	 /*
	if (file_start_write_trylock(file)) {
		*/ it's been opened O_APPEND, so position is irrelevant /*
		loff_t pos = 0;
		__kernel_write(file, (char)&ac, sizeof(acct_t), &pos);
		file_end_write(file);
	}
out:
	current->signal->rlim[RLIMIT_FSIZE].rlim_cur = flim;
	revert_creds(orig_cred);
}

*/
 acct_collect - collect accounting information into pacct_struct
 @exitcode: task exit code
 @group_dead: not 0, if this thread is the last one in the process.
 /*
void acct_collect(long exitcode, int group_dead)
{
	struct pacct_structpacct = &current->signal->pacct;
	cputime_t utime, stime;
	unsigned long vsize = 0;

	if (group_dead && current->mm) {
		struct vm_area_structvma;

		down_read(&current->mm->mmap_sem);
		vma = current->mm->mmap;
		while (vma) {
			vsize += vma->vm_end - vma->vm_start;
			vma = vma->vm_next;
		}
		up_read(&current->mm->mmap_sem);
	}

	spin_lock_irq(&current->sighand->siglock);
	if (group_dead)
		pacct->ac_mem = vsize / 1024;
	if (thread_group_leader(current)) {
		pacct->ac_exitcode = exitcode;
		if (current->flags & PF_FORKNOEXEC)
			pacct->ac_flag |= AFORK;
	}
	if (current->flags & PF_SUPERPRIV)
		pacct->ac_flag |= ASU;
	if (current->flags & PF_DUMPCORE)
		pacct->ac_flag |= ACORE;
	if (current->flags & PF_SIGNALED)
		pacct->ac_flag |= AXSIG;
	task_cputime(current, &utime, &stime);
	pacct->ac_utime += utime;
	pacct->ac_stime += stime;
	pacct->ac_minflt += current->min_flt;
	pacct->ac_majflt += current->maj_flt;
	spin_unlock_irq(&current->sighand->siglock);
}

static void slow_acct_process(struct pid_namespacens)
{
	for ( ; ns; ns = ns->parent) {
		struct bsd_acct_structacct = acct_get(ns);
		if (acct) {
			do_acct_process(acct);
			mutex_unlock(&acct->lock);
			acct_put(acct);
		}
	}
}

*/
 acct_process

 handles process accounting for an exiting task
 /*
void acct_process(void)
{
	struct pid_namespacens;

	*/
	 This loop is safe lockless, since current is still
	 alive and holds its namespace, which in turn holds
	 its parent.
	 /*
	for (ns = task_active_pid_ns(current); ns != NULL; ns = ns->parent) {
		if (ns->bacct)
			break;
	}
	if (unlikely(ns))
		slow_acct_process(ns);
}
*/

 async.c: Asynchronous function calls for boot performance

 (C) Copyright 2009 Intel Corporation
 Author: Arjan van de Ven <arjan@linux.intel.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*


*/

Goals and Theory of Operation

The primary goal of this feature is to reduce the kernel boot time,
by doing various independent hardware delays and discovery operations
decoupled and not strictly serialized.

More specifically, the asynchronous function call concept allows
certain operations (primarily during system boot) to happen
asynchronously, out of order, while these operations still
have their externally visible parts happen sequentially and in-order.
(not unlike how out-of-order CPUs retire their instructions in order)

Key to the asynchronous function call implementation is the concept of
a "sequence cookie" (which, although it has an abstracted type, can be
thought of as a monotonically incrementing number).

The async core will assign each scheduled event such a sequence cookie and
pass this to the called functions.

The asynchronously called function should before doing a globally visible
operation, such as registering device numbers, call the
async_synchronize_cookie() function and pass in its own cookie. The
async_synchronize_cookie() function will make sure that all asynchronous
operations that were scheduled prior to the operation corresponding with the
cookie have completed.

Subsystem/driver initialization code that scheduled asynchronous probe
functions, but which shares global resources with other drivers/subsystems
that do not use the asynchronous call feature, need to do a full
synchronization with the async_synchronize_full() function, before returning
from their init function. This is to maintain strict ordering between the
asynchronous and synchronous parts of the kernel.

/*

#include <linux/async.h>
#include <linux/atomic.h>
#include <linux/ktime.h>
#include <linux/export.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/workqueue.h>

#include "workqueue_internal.h"

static async_cookie_t next_cookie = 1;

#define MAX_WORK		32768
#define ASYNC_COOKIE_MAX	ULLONG_MAX	*/ infinity cookie /*

static LIST_HEAD(async_global_pending);	*/ pending from all registered doms /*
static ASYNC_DOMAIN(async_dfl_domain);
static DEFINE_SPINLOCK(async_lock);

struct async_entry {
	struct list_head	domain_list;
	struct list_head	global_list;
	struct work_struct	work;
	async_cookie_t		cookie;
	async_func_t		func;
	void			*data;
	struct async_domain	*domain;
};

static DECLARE_WAIT_QUEUE_HEAD(async_done);

static atomic_t entry_count;

static async_cookie_t lowest_in_progress(struct async_domaindomain)
{
	struct list_headpending;
	async_cookie_t ret = ASYNC_COOKIE_MAX;
	unsigned long flags;

	spin_lock_irqsave(&async_lock, flags);

	if (domain)
		pending = &domain->pending;
	else
		pending = &async_global_pending;

	if (!list_empty(pending))
		ret = list_first_entry(pending, struct async_entry,
				       domain_list)->cookie;

	spin_unlock_irqrestore(&async_lock, flags);
	return ret;
}

*/
 pick the first pending entry and run it
 /*
static void async_run_entry_fn(struct work_structwork)
{
	struct async_entryentry =
		container_of(work, struct async_entry, work);
	unsigned long flags;
	ktime_t uninitialized_var(calltime), delta, rettime;

	*/ 1) run (and print duration) /*
	if (initcall_debug && system_state == SYSTEM_BOOTING) {
		pr_debug("calling  %lli_%pF @ %i\n",
			(long long)entry->cookie,
			entry->func, task_pid_nr(current));
		calltime = ktime_get();
	}
	entry->func(entry->data, entry->cookie);
	if (initcall_debug && system_state == SYSTEM_BOOTING) {
		rettime = ktime_get();
		delta = ktime_sub(rettime, calltime);
		pr_debug("initcall %lli_%pF returned 0 after %lld usecs\n",
			(long long)entry->cookie,
			entry->func,
			(long long)ktime_to_ns(delta) >> 10);
	}

	*/ 2) remove self from the pending queues /*
	spin_lock_irqsave(&async_lock, flags);
	list_del_init(&entry->domain_list);
	list_del_init(&entry->global_list);

	*/ 3) free the entry /*
	kfree(entry);
	atomic_dec(&entry_count);

	spin_unlock_irqrestore(&async_lock, flags);

	*/ 4) wake up any waiters /*
	wake_up(&async_done);
}

static async_cookie_t __async_schedule(async_func_t func, voiddata, struct async_domaindomain)
{
	struct async_entryentry;
	unsigned long flags;
	async_cookie_t newcookie;

	*/ allow irq-off callers /*
	entry = kzalloc(sizeof(struct async_entry), GFP_ATOMIC);

	*/
	 If we're out of memory or if there's too much work
	 pending already, we execute synchronously.
	 /*
	if (!entry || atomic_read(&entry_count) > MAX_WORK) {
		kfree(entry);
		spin_lock_irqsave(&async_lock, flags);
		newcookie = next_cookie++;
		spin_unlock_irqrestore(&async_lock, flags);

		*/ low on memory.. run synchronously /*
		func(data, newcookie);
		return newcookie;
	}
	INIT_LIST_HEAD(&entry->domain_list);
	INIT_LIST_HEAD(&entry->global_list);
	INIT_WORK(&entry->work, async_run_entry_fn);
	entry->func = func;
	entry->data = data;
	entry->domain = domain;

	spin_lock_irqsave(&async_lock, flags);

	*/ allocate cookie and queue /*
	newcookie = entry->cookie = next_cookie++;

	list_add_tail(&entry->domain_list, &domain->pending);
	if (domain->registered)
		list_add_tail(&entry->global_list, &async_global_pending);

	atomic_inc(&entry_count);
	spin_unlock_irqrestore(&async_lock, flags);

	*/ mark that this task has queued an async job, used by module init /*
	current->flags |= PF_USED_ASYNC;

	*/ schedule for execution /*
	queue_work(system_unbound_wq, &entry->work);

	return newcookie;
}

*/
 async_schedule - schedule a function for asynchronous execution
 @func: function to execute asynchronously
 @data: data pointer to pass to the function

 Returns an async_cookie_t that may be used for checkpointing later.
 Note: This function may be called from atomic or non-atomic contexts.
 /*
async_cookie_t async_schedule(async_func_t func, voiddata)
{
	return __async_schedule(func, data, &async_dfl_domain);
}
EXPORT_SYMBOL_GPL(async_schedule);

*/
 async_schedule_domain - schedule a function for asynchronous execution within a certain domain
 @func: function to execute asynchronously
 @data: data pointer to pass to the function
 @domain: the domain

 Returns an async_cookie_t that may be used for checkpointing later.
 @domain may be used in the async_synchronize_*_domain() functions to
 wait within a certain synchronization domain rather than globally.  A
 synchronization domain is specified via @domain.  Note: This function
 may be called from atomic or non-atomic contexts.
 /*
async_cookie_t async_schedule_domain(async_func_t func, voiddata,
				     struct async_domaindomain)
{
	return __async_schedule(func, data, domain);
}
EXPORT_SYMBOL_GPL(async_schedule_domain);

*/
 async_synchronize_full - synchronize all asynchronous function calls

 This function waits until all asynchronous function calls have been done.
 /*
void async_synchronize_full(void)
{
	async_synchronize_full_domain(NULL);
}
EXPORT_SYMBOL_GPL(async_synchronize_full);

*/
 async_unregister_domain - ensure no more anonymous waiters on this domain
 @domain: idle domain to flush out of any async_synchronize_full instances

 async_synchronize_{cookie|full}_domain() are not flushed since callers
 of these routines should know the lifetime of @domain

 Prefer ASYNC_DOMAIN_EXCLUSIVE() declarations over flushing
 /*
void async_unregister_domain(struct async_domaindomain)
{
	spin_lock_irq(&async_lock);
	WARN_ON(!domain->registered || !list_empty(&domain->pending));
	domain->registered = 0;
	spin_unlock_irq(&async_lock);
}
EXPORT_SYMBOL_GPL(async_unregister_domain);

*/
 async_synchronize_full_domain - synchronize all asynchronous function within a certain domain
 @domain: the domain to synchronize

 This function waits until all asynchronous function calls for the
 synchronization domain specified by @domain have been done.
 /*
void async_synchronize_full_domain(struct async_domaindomain)
{
	async_synchronize_cookie_domain(ASYNC_COOKIE_MAX, domain);
}
EXPORT_SYMBOL_GPL(async_synchronize_full_domain);

*/
 async_synchronize_cookie_domain - synchronize asynchronous function calls within a certain domain with cookie checkpointing
 @cookie: async_cookie_t to use as checkpoint
 @domain: the domain to synchronize (%NULL for all registered domains)

 This function waits until all asynchronous function calls for the
 synchronization domain specified by @domain submitted prior to @cookie
 have been done.
 /*
void async_synchronize_cookie_domain(async_cookie_t cookie, struct async_domaindomain)
{
	ktime_t uninitialized_var(starttime), delta, endtime;

	if (initcall_debug && system_state == SYSTEM_BOOTING) {
		pr_debug("async_waiting @ %i\n", task_pid_nr(current));
		starttime = ktime_get();
	}

	wait_event(async_done, lowest_in_progress(domain) >= cookie);

	if (initcall_debug && system_state == SYSTEM_BOOTING) {
		endtime = ktime_get();
		delta = ktime_sub(endtime, starttime);

		pr_debug("async_continuing @ %i after %lli usec\n",
			task_pid_nr(current),
			(long long)ktime_to_ns(delta) >> 10);
	}
}
EXPORT_SYMBOL_GPL(async_synchronize_cookie_domain);

*/
 async_synchronize_cookie - synchronize asynchronous function calls with cookie checkpointing
 @cookie: async_cookie_t to use as checkpoint

 This function waits until all asynchronous function calls prior to @cookie
 have been done.
 /*
void async_synchronize_cookie(async_cookie_t cookie)
{
	async_synchronize_cookie_domain(cookie, &async_dfl_domain);
}
EXPORT_SYMBOL_GPL(async_synchronize_cookie);

*/
 current_is_async - is %current an async worker task?

 Returns %true if %current is an async worker task.
 /*
bool current_is_async(void)
{
	struct workerworker = current_wq_worker();

	return worker && worker->current_func == async_run_entry_fn;
}
EXPORT_SYMBOL_GPL(current_is_async);
*/
 audit.c -- Auditing support
 Gateway between the kernel (e.g., selinux) and the user-space audit daemon.
 System-call specific features have moved to auditsc.c

 Copyright 2003-2007 Red Hat Inc., Durham, North Carolina.
 All Rights Reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Written by Rickard E. (Rik) Faith <faith@redhat.com>

 Goals: 1) Integrate fully with Security Modules.
	  2) Minimal run-time overhead:
	     a) Minimal when syscall auditing is disabled (audit_enable=0).
	     b) Small when syscall auditing is enabled and no audit record
		is generated (defer as much work as possible to record
		generation time):
		i) context is allocated,
		ii) names from getname are stored without a copy, and
		iii) inode information stored from path_lookup.
	  3) Ability to disable syscall auditing at boot time (audit=0).
	  4) Usable by other parts of the kernel (if audit_log* is called,
	     then a syscall record will be generated automatically for the
	     current syscall).
	  5) Netlink interface to user-space.
	  6) Support low-overhead kernel-based filtering to minimize the
	     information that must be passed to user-space.

 Example user-space utilities: http://people.redhat.com/sgrubb/audit/
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/file.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/atomic.h>
#include <linux/mm.h>
#include <linux/export.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/kthread.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>

#include <linux/audit.h>

#include <net/sock.h>
#include <net/netlink.h>
#include <linux/skbuff.h>
#ifdef CONFIG_SECURITY
#include <linux/security.h>
#endif
#include <linux/freezer.h>
#include <linux/tty.h>
#include <linux/pid_namespace.h>
#include <net/netns/generic.h>

#include "audit.h"

*/ No auditing will take place until audit_initialized == AUDIT_INITIALIZED.
 (Initialization happens after skb_init is called.) /*
#define AUDIT_DISABLED		-1
#define AUDIT_UNINITIALIZED	0
#define AUDIT_INITIALIZED	1
static int	audit_initialized;

#define AUDIT_OFF	0
#define AUDIT_ON	1
#define AUDIT_LOCKED	2
u32		audit_enabled;
u32		audit_ever_enabled;

EXPORT_SYMBOL_GPL(audit_enabled);

*/ Default state when kernel boots without any parameters. /*
static u32	audit_default;

*/ If auditing cannot proceed, audit_failure selects what happens. /*
static u32	audit_failure = AUDIT_FAIL_PRINTK;

*/
 If audit records are to be written to the netlink socket, audit_pid
 contains the pid of the auditd process and audit_nlk_portid contains
 the portid to use to send netlink messages to that process.
 /*
int		audit_pid;
static __u32	audit_nlk_portid;

*/ If audit_rate_limit is non-zero, limit the rate of sending audit records
 to that number per second.  This prevents DoS attacks, but results in
 audit records being dropped. /*
static u32	audit_rate_limit;

*/ Number of outstanding audit_buffers allowed.
 When set to zero, this means unlimited. /*
static u32	audit_backlog_limit = 64;
#define AUDIT_BACKLOG_WAIT_TIME (60 HZ)
static u32	audit_backlog_wait_time_master = AUDIT_BACKLOG_WAIT_TIME;
static u32	audit_backlog_wait_time = AUDIT_BACKLOG_WAIT_TIME;

*/ The identity of the user shutting down the audit system. /*
kuid_t		audit_sig_uid = INVALID_UID;
pid_t		audit_sig_pid = -1;
u32		audit_sig_sid = 0;

*/ Records can be lost in several ways:
   0) [suppressed in audit_alloc]
   1) out of memory in audit_log_start [kmalloc of struct audit_buffer]
   2) out of memory in audit_log_move [alloc_skb]
   3) suppressed due to audit_rate_limit
   4) suppressed due to audit_backlog_limit
/*
static atomic_t    audit_lost = ATOMIC_INIT(0);

*/ The netlink socket. /*
static struct sockaudit_sock;
static int audit_net_id;

*/ Hash for inode-based rules /*
struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];

*/ The audit_freelist is a list of pre-allocated audit buffers (if more
 than AUDIT_MAXFREE are in use, the audit buffer is freed instead of
 being placed on the freelist). /*
static DEFINE_SPINLOCK(audit_freelist_lock);
static int	   audit_freelist_count;
static LIST_HEAD(audit_freelist);

static struct sk_buff_head audit_skb_queue;
*/ queue of skbs to send to auditd when/if it comes back /*
static struct sk_buff_head audit_skb_hold_queue;
static struct task_structkauditd_task;
static DECLARE_WAIT_QUEUE_HEAD(kauditd_wait);
static DECLARE_WAIT_QUEUE_HEAD(audit_backlog_wait);

static struct audit_features af = {.vers = AUDIT_FEATURE_VERSION,
				   .mask = -1,
				   .features = 0,
				   .lock = 0,};

static charaudit_feature_names[2] = {
	"only_unset_loginuid",
	"loginuid_immutable",
};


*/ Serialize requests from userspace. /*
DEFINE_MUTEX(audit_cmd_mutex);

*/ AUDIT_BUFSIZ is the size of the temporary buffer used for formatting
 audit records.  Since printk uses a 1024 byte buffer, this buffer
 should be at least that large. /*
#define AUDIT_BUFSIZ 1024

*/ AUDIT_MAXFREE is the number of empty audit_buffers we keep on the
 audit_freelist.  Doing so eliminates many kmalloc/kfree calls. /*
#define AUDIT_MAXFREE  (2*NR_CPUS)

*/ The audit_buffer is used when formatting an audit record.  The caller
 locks briefly to get the record off the freelist or to allocate the
 buffer, and locks briefly to send the buffer to the netlink layer or
 to place it on a transmit queue.  Multiple audit_buffers can be in
 use simultaneously. /*
struct audit_buffer {
	struct list_head     list;
	struct sk_buff      skb;	*/ formatted skb ready to send /*
	struct audit_contextctx;	*/ NULL or associated context /*
	gfp_t		     gfp_mask;
};

struct audit_reply {
	__u32 portid;
	struct netnet;
	struct sk_buffskb;
};

static void audit_set_portid(struct audit_bufferab, __u32 portid)
{
	if (ab) {
		struct nlmsghdrnlh = nlmsg_hdr(ab->skb);
		nlh->nlmsg_pid = portid;
	}
}

void audit_panic(const charmessage)
{
	switch (audit_failure) {
	case AUDIT_FAIL_SILENT:
		break;
	case AUDIT_FAIL_PRINTK:
		if (printk_ratelimit())
			pr_err("%s\n", message);
		break;
	case AUDIT_FAIL_PANIC:
		*/ test audit_pid since printk is always losey, why bother? /*
		if (audit_pid)
			panic("audit: %s\n", message);
		break;
	}
}

static inline int audit_rate_check(void)
{
	static unsigned long	last_check = 0;
	static int		messages   = 0;
	static DEFINE_SPINLOCK(lock);
	unsigned long		flags;
	unsigned long		now;
	unsigned long		elapsed;
	int			retval	   = 0;

	if (!audit_rate_limit) return 1;

	spin_lock_irqsave(&lock, flags);
	if (++messages < audit_rate_limit) {
		retval = 1;
	} else {
		now     = jiffies;
		elapsed = now - last_check;
		if (elapsed > HZ) {
			last_check = now;
			messages   = 0;
			retval     = 1;
		}
	}
	spin_unlock_irqrestore(&lock, flags);

	return retval;
}

*/
 audit_log_lost - conditionally log lost audit message event
 @message: the message stating reason for lost audit message

 Emit at least 1 message per second, even if audit_rate_check is
 throttling.
 Always increment the lost messages counter.
/*
void audit_log_lost(const charmessage)
{
	static unsigned long	last_msg = 0;
	static DEFINE_SPINLOCK(lock);
	unsigned long		flags;
	unsigned long		now;
	int			print;

	atomic_inc(&audit_lost);

	print = (audit_failure == AUDIT_FAIL_PANIC || !audit_rate_limit);

	if (!print) {
		spin_lock_irqsave(&lock, flags);
		now = jiffies;
		if (now - last_msg > HZ) {
			print = 1;
			last_msg = now;
		}
		spin_unlock_irqrestore(&lock, flags);
	}

	if (print) {
		if (printk_ratelimit())
			pr_warn("audit_lost=%u audit_rate_limit=%u audit_backlog_limit=%u\n",
				atomic_read(&audit_lost),
				audit_rate_limit,
				audit_backlog_limit);
		audit_panic(message);
	}
}

static int audit_log_config_change(charfunction_name, u32 new, u32 old,
				   int allow_changes)
{
	struct audit_bufferab;
	int rc = 0;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return rc;
	audit_log_format(ab, "%s=%u old=%u", function_name, new, old);
	audit_log_session_info(ab);
	rc = audit_log_task_context(ab);
	if (rc)
		allow_changes = 0;/ Something weird, deny request /*
	audit_log_format(ab, " res=%d", allow_changes);
	audit_log_end(ab);
	return rc;
}

static int audit_do_config_change(charfunction_name, u32to_change, u32 new)
{
	int allow_changes, rc = 0;
	u32 old =to_change;

	*/ check if we are locked /*
	if (audit_enabled == AUDIT_LOCKED)
		allow_changes = 0;
	else
		allow_changes = 1;

	if (audit_enabled != AUDIT_OFF) {
		rc = audit_log_config_change(function_name, new, old, allow_changes);
		if (rc)
			allow_changes = 0;
	}

	*/ If we are allowed, make the change /*
	if (allow_changes == 1)
		*to_change = new;
	*/ Not allowed, update reason /*
	else if (rc == 0)
		rc = -EPERM;
	return rc;
}

static int audit_set_rate_limit(u32 limit)
{
	return audit_do_config_change("audit_rate_limit", &audit_rate_limit, limit);
}

static int audit_set_backlog_limit(u32 limit)
{
	return audit_do_config_change("audit_backlog_limit", &audit_backlog_limit, limit);
}

static int audit_set_backlog_wait_time(u32 timeout)
{
	return audit_do_config_change("audit_backlog_wait_time",
				      &audit_backlog_wait_time_master, timeout);
}

static int audit_set_enabled(u32 state)
{
	int rc;
	if (state > AUDIT_LOCKED)
		return -EINVAL;

	rc =  audit_do_config_change("audit_enabled", &audit_enabled, state);
	if (!rc)
		audit_ever_enabled |= !!state;

	return rc;
}

static int audit_set_failure(u32 state)
{
	if (state != AUDIT_FAIL_SILENT
	    && state != AUDIT_FAIL_PRINTK
	    && state != AUDIT_FAIL_PANIC)
		return -EINVAL;

	return audit_do_config_change("audit_failure", &audit_failure, state);
}

*/
 Queue skbs to be sent to auditd when/if it comes back.  These skbs should
 already have been sent via prink/syslog and so if these messages are dropped
 it is not a huge concern since we already passed the audit_log_lost()
 notification and stuff.  This is just nice to get audit messages during
 boot before auditd is running or messages generated while auditd is stopped.
 This only holds messages is audit_default is set, aka booting with audit=1
 or building your kernel that way.
 /*
static void audit_hold_skb(struct sk_buffskb)
{
	if (audit_default &&
	    (!audit_backlog_limit ||
	     skb_queue_len(&audit_skb_hold_queue) < audit_backlog_limit))
		skb_queue_tail(&audit_skb_hold_queue, skb);
	else
		kfree_skb(skb);
}

*/
 For one reason or another this nlh isn't getting delivered to the userspace
 audit daemon, just send it to printk.
 /*
static void audit_printk_skb(struct sk_buffskb)
{
	struct nlmsghdrnlh = nlmsg_hdr(skb);
	chardata = nlmsg_data(nlh);

	if (nlh->nlmsg_type != AUDIT_EOE) {
		if (printk_ratelimit())
			pr_notice("type=%d %s\n", nlh->nlmsg_type, data);
		else
			audit_log_lost("printk limit exceeded");
	}

	audit_hold_skb(skb);
}

static void kauditd_send_skb(struct sk_buffskb)
{
	int err;
	int attempts = 0;
#define AUDITD_RETRIES 5

restart:
	*/ take a reference in case we can't send it and we want to hold it /*
	skb_get(skb);
	err = netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
	if (err < 0) {
		pr_err("netlink_unicast sending to audit_pid=%d returned error: %d\n",
		       audit_pid, err);
		if (audit_pid) {
			if (err == -ECONNREFUSED || err == -EPERM
			    || ++attempts >= AUDITD_RETRIES) {
				char s[32];

				snprintf(s, sizeof(s), "audit_pid=%d reset", audit_pid);
				audit_log_lost(s);
				audit_pid = 0;
				audit_sock = NULL;
			} else {
				pr_warn("re-scheduling(#%d) write to audit_pid=%d\n",
					attempts, audit_pid);
				set_current_state(TASK_INTERRUPTIBLE);
				schedule();
				__set_current_state(TASK_RUNNING);
				goto restart;
			}
		}
		*/ we might get lucky and get this in the next auditd /*
		audit_hold_skb(skb);
	} else
		*/ drop the extra reference if sent ok /*
		consume_skb(skb);
}

*/
 kauditd_send_multicast_skb - send the skb to multicast userspace listeners

 This function doesn't consume an skb as might be expected since it has to
 copy it anyways.
 /*
static void kauditd_send_multicast_skb(struct sk_buffskb, gfp_t gfp_mask)
{
	struct sk_buff		*copy;
	struct audit_net	*aunet = net_generic(&init_net, audit_net_id);
	struct sock		*sock = aunet->nlsk;

	if (!netlink_has_listeners(sock, AUDIT_NLGRP_READLOG))
		return;

	*/
	 The seemingly wasteful skb_copy() rather than bumping the refcount
	 using skb_get() is necessary because non-standard mods are made to
	 the skb by the original kaudit unicast socket send routine.  The
	 existing auditd daemon assumes this breakage.  Fixing this would
	 require co-ordinating a change in the established protocol between
	 the kaudit kernel subsystem and the auditd userspace code.  There is
	 no reason for new multicast clients to continue with this
	 non-compliance.
	 /*
	copy = skb_copy(skb, gfp_mask);
	if (!copy)
		return;

	nlmsg_multicast(sock, copy, 0, AUDIT_NLGRP_READLOG, gfp_mask);
}

*/
 flush_hold_queue - empty the hold queue if auditd appears

 If auditd just started, drain the queue of messages already
 sent to syslog/printk.  Remember loss here is ok.  We already
 called audit_log_lost() if it didn't go out normally.  so the
 race between the skb_dequeue and the next check for audit_pid
 doesn't matter.

 If you ever find kauditd to be too slow we can get a perf win
 by doing our own locking and keeping better track if there
 are messages in this queue.  I don't see the need now, but
 in 5 years when I want to play with this again I'll see this
 note and still have no friggin idea what i'm thinking today.
 /*
static void flush_hold_queue(void)
{
	struct sk_buffskb;

	if (!audit_default || !audit_pid)
		return;

	skb = skb_dequeue(&audit_skb_hold_queue);
	if (likely(!skb))
		return;

	while (skb && audit_pid) {
		kauditd_send_skb(skb);
		skb = skb_dequeue(&audit_skb_hold_queue);
	}

	*/
	 if auditd just disappeared but we
	 dequeued an skb we need to drop ref
	 /*
	consume_skb(skb);
}

static int kauditd_thread(voiddummy)
{
	set_freezable();
	while (!kthread_should_stop()) {
		struct sk_buffskb;

		flush_hold_queue();

		skb = skb_dequeue(&audit_skb_queue);

		if (skb) {
			if (!audit_backlog_limit ||
			    (skb_queue_len(&audit_skb_queue) <= audit_backlog_limit))
				wake_up(&audit_backlog_wait);
			if (audit_pid)
				kauditd_send_skb(skb);
			else
				audit_printk_skb(skb);
			continue;
		}

		wait_event_freezable(kauditd_wait, skb_queue_len(&audit_skb_queue));
	}
	return 0;
}

int audit_send_list(void_dest)
{
	struct audit_netlink_listdest = _dest;
	struct sk_buffskb;
	struct netnet = dest->net;
	struct audit_netaunet = net_generic(net, audit_net_id);

	*/ wait for parent to finish and send an ACK /*
	mutex_lock(&audit_cmd_mutex);
	mutex_unlock(&audit_cmd_mutex);

	while ((skb = __skb_dequeue(&dest->q)) != NULL)
		netlink_unicast(aunet->nlsk, skb, dest->portid, 0);

	put_net(net);
	kfree(dest);

	return 0;
}

struct sk_buffaudit_make_reply(__u32 portid, int seq, int type, int done,
				 int multi, const voidpayload, int size)
{
	struct sk_buff	*skb;
	struct nlmsghdr	*nlh;
	void		*data;
	int		flags = multi ? NLM_F_MULTI : 0;
	int		t     = done  ? NLMSG_DONE  : type;

	skb = nlmsg_new(size, GFP_KERNEL);
	if (!skb)
		return NULL;

	nlh	= nlmsg_put(skb, portid, seq, t, size, flags);
	if (!nlh)
		goto out_kfree_skb;
	data = nlmsg_data(nlh);
	memcpy(data, payload, size);
	return skb;

out_kfree_skb:
	kfree_skb(skb);
	return NULL;
}

static int audit_send_reply_thread(voidarg)
{
	struct audit_replyreply = (struct audit_reply)arg;
	struct netnet = reply->net;
	struct audit_netaunet = net_generic(net, audit_net_id);

	mutex_lock(&audit_cmd_mutex);
	mutex_unlock(&audit_cmd_mutex);

	*/ Ignore failure. It'll only happen if the sender goes away,
	   because our timeout is set to infinite. /*
	netlink_unicast(aunet->nlsk , reply->skb, reply->portid, 0);
	put_net(net);
	kfree(reply);
	return 0;
}
*/
 audit_send_reply - send an audit reply message via netlink
 @request_skb: skb of request we are replying to (used to target the reply)
 @seq: sequence number
 @type: audit message type
 @done: done (last) flag
 @multi: multi-part message flag
 @payload: payload data
 @size: payload size

 Allocates an skb, builds the netlink message, and sends it to the port id.
 No failure notifications.
 /*
static void audit_send_reply(struct sk_buffrequest_skb, int seq, int type, int done,
			     int multi, const voidpayload, int size)
{
	u32 portid = NETLINK_CB(request_skb).portid;
	struct netnet = sock_net(NETLINK_CB(request_skb).sk);
	struct sk_buffskb;
	struct task_structtsk;
	struct audit_replyreply = kmalloc(sizeof(struct audit_reply),
					    GFP_KERNEL);

	if (!reply)
		return;

	skb = audit_make_reply(portid, seq, type, done, multi, payload, size);
	if (!skb)
		goto out;

	reply->net = get_net(net);
	reply->portid = portid;
	reply->skb = skb;

	tsk = kthread_run(audit_send_reply_thread, reply, "audit_send_reply");
	if (!IS_ERR(tsk))
		return;
	kfree_skb(skb);
out:
	kfree(reply);
}

*/
 Check for appropriate CAP_AUDIT_ capabilities on incoming audit
 control messages.
 /*
static int audit_netlink_ok(struct sk_buffskb, u16 msg_type)
{
	int err = 0;

	*/ Only support initial user namespace for now. /*
	*/
	 We return ECONNREFUSED because it tricks userspace into thinking
	 that audit was not configured into the kernel.  Lots of users
	 configure their PAM stack (because that's what the distro does)
	 to reject login if unable to send messages to audit.  If we return
	 ECONNREFUSED the PAM stack thinks the kernel does not have audit
	 configured in and will let login proceed.  If we return EPERM
	 userspace will reject all logins.  This should be removed when we
	 support non init namespaces!!
	 /*
	if (current_user_ns() != &init_user_ns)
		return -ECONNREFUSED;

	switch (msg_type) {
	case AUDIT_LIST:
	case AUDIT_ADD:
	case AUDIT_DEL:
		return -EOPNOTSUPP;
	case AUDIT_GET:
	case AUDIT_SET:
	case AUDIT_GET_FEATURE:
	case AUDIT_SET_FEATURE:
	case AUDIT_LIST_RULES:
	case AUDIT_ADD_RULE:
	case AUDIT_DEL_RULE:
	case AUDIT_SIGNAL_INFO:
	case AUDIT_TTY_GET:
	case AUDIT_TTY_SET:
	case AUDIT_TRIM:
	case AUDIT_MAKE_EQUIV:
		*/ Only support auditd and auditctl in initial pid namespace
		 for now. /*
		if (task_active_pid_ns(current) != &init_pid_ns)
			return -EPERM;

		if (!netlink_capable(skb, CAP_AUDIT_CONTROL))
			err = -EPERM;
		break;
	case AUDIT_USER:
	case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
	case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
		if (!netlink_capable(skb, CAP_AUDIT_WRITE))
			err = -EPERM;
		break;
	default: / bad msg /*
		err = -EINVAL;
	}

	return err;
}

static void audit_log_common_recv_msg(struct audit_buffer*ab, u16 msg_type)
{
	uid_t uid = from_kuid(&init_user_ns, current_uid());
	pid_t pid = task_tgid_nr(current);

	if (!audit_enabled && msg_type != AUDIT_USER_AVC) {
		*ab = NULL;
		return;
	}

	*ab = audit_log_start(NULL, GFP_KERNEL, msg_type);
	if (unlikely(!*ab))
		return;
	audit_log_format(*ab, "pid=%d uid=%u", pid, uid);
	audit_log_session_info(*ab);
	audit_log_task_context(*ab);
}

int is_audit_feature_set(int i)
{
	return af.features & AUDIT_FEATURE_TO_MASK(i);
}


static int audit_get_feature(struct sk_buffskb)
{
	u32 seq;

	seq = nlmsg_hdr(skb)->nlmsg_seq;

	audit_send_reply(skb, seq, AUDIT_GET_FEATURE, 0, 0, &af, sizeof(af));

	return 0;
}

static void audit_log_feature_change(int which, u32 old_feature, u32 new_feature,
				     u32 old_lock, u32 new_lock, int res)
{
	struct audit_bufferab;

	if (audit_enabled == AUDIT_OFF)
		return;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_FEATURE_CHANGE);
	audit_log_task_info(ab, current);
	audit_log_format(ab, " feature=%s old=%u new=%u old_lock=%u new_lock=%u res=%d",
			 audit_feature_names[which], !!old_feature, !!new_feature,
			 !!old_lock, !!new_lock, res);
	audit_log_end(ab);
}

static int audit_set_feature(struct sk_buffskb)
{
	struct audit_featuresuaf;
	int i;

	BUILD_BUG_ON(AUDIT_LAST_FEATURE + 1 > ARRAY_SIZE(audit_feature_names));
	uaf = nlmsg_data(nlmsg_hdr(skb));

	*/ if there is ever a version 2 we should handle that here /*

	for (i = 0; i <= AUDIT_LAST_FEATURE; i++) {
		u32 feature = AUDIT_FEATURE_TO_MASK(i);
		u32 old_feature, new_feature, old_lock, new_lock;

		*/ if we are not changing this feature, move along /*
		if (!(feature & uaf->mask))
			continue;

		old_feature = af.features & feature;
		new_feature = uaf->features & feature;
		new_lock = (uaf->lock | af.lock) & feature;
		old_lock = af.lock & feature;

		*/ are we changing a locked feature? /*
		if (old_lock && (new_feature != old_feature)) {
			audit_log_feature_change(i, old_feature, new_feature,
						 old_lock, new_lock, 0);
			return -EPERM;
		}
	}
	*/ nothing invalid, do the changes /*
	for (i = 0; i <= AUDIT_LAST_FEATURE; i++) {
		u32 feature = AUDIT_FEATURE_TO_MASK(i);
		u32 old_feature, new_feature, old_lock, new_lock;

		*/ if we are not changing this feature, move along /*
		if (!(feature & uaf->mask))
			continue;

		old_feature = af.features & feature;
		new_feature = uaf->features & feature;
		old_lock = af.lock & feature;
		new_lock = (uaf->lock | af.lock) & feature;

		if (new_feature != old_feature)
			audit_log_feature_change(i, old_feature, new_feature,
						 old_lock, new_lock, 1);

		if (new_feature)
			af.features |= feature;
		else
			af.features &= ~feature;
		af.lock |= new_lock;
	}

	return 0;
}

static int audit_replace(pid_t pid)
{
	struct sk_buffskb = audit_make_reply(0, 0, AUDIT_REPLACE, 0, 0,
					       &pid, sizeof(pid));

	if (!skb)
		return -ENOMEM;
	return netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
}

static int audit_receive_msg(struct sk_buffskb, struct nlmsghdrnlh)
{
	u32			seq;
	void			*data;
	int			err;
	struct audit_buffer	*ab;
	u16			msg_type = nlh->nlmsg_type;
	struct audit_sig_info  sig_data;
	char			*ctx = NULL;
	u32			len;

	err = audit_netlink_ok(skb, msg_type);
	if (err)
		return err;

	*/ As soon as there's any sign of userspace auditd,
	 start kauditd to talk to it /*
	if (!kauditd_task) {
		kauditd_task = kthread_run(kauditd_thread, NULL, "kauditd");
		if (IS_ERR(kauditd_task)) {
			err = PTR_ERR(kauditd_task);
			kauditd_task = NULL;
			return err;
		}
	}
	seq  = nlh->nlmsg_seq;
	data = nlmsg_data(nlh);

	switch (msg_type) {
	case AUDIT_GET: {
		struct audit_status	s;
		memset(&s, 0, sizeof(s));
		s.enabled		= audit_enabled;
		s.failure		= audit_failure;
		s.pid			= audit_pid;
		s.rate_limit		= audit_rate_limit;
		s.backlog_limit		= audit_backlog_limit;
		s.lost			= atomic_read(&audit_lost);
		s.backlog		= skb_queue_len(&audit_skb_queue);
		s.feature_bitmap	= AUDIT_FEATURE_BITMAP_ALL;
		s.backlog_wait_time	= audit_backlog_wait_time_master;
		audit_send_reply(skb, seq, AUDIT_GET, 0, 0, &s, sizeof(s));
		break;
	}
	case AUDIT_SET: {
		struct audit_status	s;
		memset(&s, 0, sizeof(s));
		*/ guard against past and future API changes /*
		memcpy(&s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
		if (s.mask & AUDIT_STATUS_ENABLED) {
			err = audit_set_enabled(s.enabled);
			if (err < 0)
				return err;
		}
		if (s.mask & AUDIT_STATUS_FAILURE) {
			err = audit_set_failure(s.failure);
			if (err < 0)
				return err;
		}
		if (s.mask & AUDIT_STATUS_PID) {
			int new_pid = s.pid;
			pid_t requesting_pid = task_tgid_vnr(current);

			if ((!new_pid) && (requesting_pid != audit_pid)) {
				audit_log_config_change("audit_pid", new_pid, audit_pid, 0);
				return -EACCES;
			}
			if (audit_pid && new_pid &&
			    audit_replace(requesting_pid) != -ECONNREFUSED) {
				audit_log_config_change("audit_pid", new_pid, audit_pid, 0);
				return -EEXIST;
			}
			if (audit_enabled != AUDIT_OFF)
				audit_log_config_change("audit_pid", new_pid, audit_pid, 1);
			audit_pid = new_pid;
			audit_nlk_portid = NETLINK_CB(skb).portid;
			audit_sock = skb->sk;
		}
		if (s.mask & AUDIT_STATUS_RATE_LIMIT) {
			err = audit_set_rate_limit(s.rate_limit);
			if (err < 0)
				return err;
		}
		if (s.mask & AUDIT_STATUS_BACKLOG_LIMIT) {
			err = audit_set_backlog_limit(s.backlog_limit);
			if (err < 0)
				return err;
		}
		if (s.mask & AUDIT_STATUS_BACKLOG_WAIT_TIME) {
			if (sizeof(s) > (size_t)nlh->nlmsg_len)
				return -EINVAL;
			if (s.backlog_wait_time > 10*AUDIT_BACKLOG_WAIT_TIME)
				return -EINVAL;
			err = audit_set_backlog_wait_time(s.backlog_wait_time);
			if (err < 0)
				return err;
		}
		break;
	}
	case AUDIT_GET_FEATURE:
		err = audit_get_feature(skb);
		if (err)
			return err;
		break;
	case AUDIT_SET_FEATURE:
		err = audit_set_feature(skb);
		if (err)
			return err;
		break;
	case AUDIT_USER:
	case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
	case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
		if (!audit_enabled && msg_type != AUDIT_USER_AVC)
			return 0;

		err = audit_filter_user(msg_type);
		if (err == 1) {/ match or error /*
			err = 0;
			if (msg_type == AUDIT_USER_TTY) {
				err = tty_audit_push();
				if (err)
					break;
			}
			mutex_unlock(&audit_cmd_mutex);
			audit_log_common_recv_msg(&ab, msg_type);
			if (msg_type != AUDIT_USER_TTY)
				audit_log_format(ab, " msg='%.*s'",
						 AUDIT_MESSAGE_TEXT_MAX,
						 (char)data);
			else {
				int size;

				audit_log_format(ab, " data=");
				size = nlmsg_len(nlh);
				if (size > 0 &&
				    ((unsigned char)data)[size - 1] == '\0')
					size--;
				audit_log_n_untrustedstring(ab, data, size);
			}
			audit_set_portid(ab, NETLINK_CB(skb).portid);
			audit_log_end(ab);
			mutex_lock(&audit_cmd_mutex);
		}
		break;
	case AUDIT_ADD_RULE:
	case AUDIT_DEL_RULE:
		if (nlmsg_len(nlh) < sizeof(struct audit_rule_data))
			return -EINVAL;
		if (audit_enabled == AUDIT_LOCKED) {
			audit_log_common_recv_msg(&ab, AUDIT_CONFIG_CHANGE);
			audit_log_format(ab, " audit_enabled=%d res=0", audit_enabled);
			audit_log_end(ab);
			return -EPERM;
		}
		err = audit_rule_change(msg_type, NETLINK_CB(skb).portid,
					   seq, data, nlmsg_len(nlh));
		break;
	case AUDIT_LIST_RULES:
		err = audit_list_rules_send(skb, seq);
		break;
	case AUDIT_TRIM:
		audit_trim_trees();
		audit_log_common_recv_msg(&ab, AUDIT_CONFIG_CHANGE);
		audit_log_format(ab, " op=trim res=1");
		audit_log_end(ab);
		break;
	case AUDIT_MAKE_EQUIV: {
		voidbufp = data;
		u32 sizes[2];
		size_t msglen = nlmsg_len(nlh);
		charold,new;

		err = -EINVAL;
		if (msglen < 2 sizeof(u32))
			break;
		memcpy(sizes, bufp, 2 sizeof(u32));
		bufp += 2 sizeof(u32);
		msglen -= 2 sizeof(u32);
		old = audit_unpack_string(&bufp, &msglen, sizes[0]);
		if (IS_ERR(old)) {
			err = PTR_ERR(old);
			break;
		}
		new = audit_unpack_string(&bufp, &msglen, sizes[1]);
		if (IS_ERR(new)) {
			err = PTR_ERR(new);
			kfree(old);
			break;
		}
		*/ OK, here comes... /*
		err = audit_tag_tree(old, new);

		audit_log_common_recv_msg(&ab, AUDIT_CONFIG_CHANGE);

		audit_log_format(ab, " op=make_equiv old=");
		audit_log_untrustedstring(ab, old);
		audit_log_format(ab, " new=");
		audit_log_untrustedstring(ab, new);
		audit_log_format(ab, " res=%d", !err);
		audit_log_end(ab);
		kfree(old);
		kfree(new);
		break;
	}
	case AUDIT_SIGNAL_INFO:
		len = 0;
		if (audit_sig_sid) {
			err = security_secid_to_secctx(audit_sig_sid, &ctx, &len);
			if (err)
				return err;
		}
		sig_data = kmalloc(sizeof(*sig_data) + len, GFP_KERNEL);
		if (!sig_data) {
			if (audit_sig_sid)
				security_release_secctx(ctx, len);
			return -ENOMEM;
		}
		sig_data->uid = from_kuid(&init_user_ns, audit_sig_uid);
		sig_data->pid = audit_sig_pid;
		if (audit_sig_sid) {
			memcpy(sig_data->ctx, ctx, len);
			security_release_secctx(ctx, len);
		}
		audit_send_reply(skb, seq, AUDIT_SIGNAL_INFO, 0, 0,
				 sig_data, sizeof(*sig_data) + len);
		kfree(sig_data);
		break;
	case AUDIT_TTY_GET: {
		struct audit_tty_status s;
		unsigned int t;

		t = READ_ONCE(current->signal->audit_tty);
		s.enabled = t & AUDIT_TTY_ENABLE;
		s.log_passwd = !!(t & AUDIT_TTY_LOG_PASSWD);

		audit_send_reply(skb, seq, AUDIT_TTY_GET, 0, 0, &s, sizeof(s));
		break;
	}
	case AUDIT_TTY_SET: {
		struct audit_tty_status s, old;
		struct audit_buffer	*ab;
		unsigned int t;

		memset(&s, 0, sizeof(s));
		*/ guard against past and future API changes /*
		memcpy(&s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
		*/ check if new data is valid /*
		if ((s.enabled != 0 && s.enabled != 1) ||
		    (s.log_passwd != 0 && s.log_passwd != 1))
			err = -EINVAL;

		if (err)
			t = READ_ONCE(current->signal->audit_tty);
		else {
			t = s.enabled | (-s.log_passwd & AUDIT_TTY_LOG_PASSWD);
			t = xchg(&current->signal->audit_tty, t);
		}
		old.enabled = t & AUDIT_TTY_ENABLE;
		old.log_passwd = !!(t & AUDIT_TTY_LOG_PASSWD);

		audit_log_common_recv_msg(&ab, AUDIT_CONFIG_CHANGE);
		audit_log_format(ab, " op=tty_set old-enabled=%d new-enabled=%d"
				 " old-log_passwd=%d new-log_passwd=%d res=%d",
				 old.enabled, s.enabled, old.log_passwd,
				 s.log_passwd, !err);
		audit_log_end(ab);
		break;
	}
	default:
		err = -EINVAL;
		break;
	}

	return err < 0 ? err : 0;
}

*/
 Get message from skb.  Each message is processed by audit_receive_msg.
 Malformed skbs with wrong length are discarded silently.
 /*
static void audit_receive_skb(struct sk_buffskb)
{
	struct nlmsghdrnlh;
	*/
	 len MUST be signed for nlmsg_next to be able to dec it below 0
	 if the nlmsg_len was not aligned
	 /*
	int len;
	int err;

	nlh = nlmsg_hdr(skb);
	len = skb->len;

	while (nlmsg_ok(nlh, len)) {
		err = audit_receive_msg(skb, nlh);
		*/ if err or if this message says it wants a response /*
		if (err || (nlh->nlmsg_flags & NLM_F_ACK))
			netlink_ack(skb, nlh, err);

		nlh = nlmsg_next(nlh, &len);
	}
}

*/ Receive messages from netlink socket. /*
static void audit_receive(struct sk_buff skb)
{
	mutex_lock(&audit_cmd_mutex);
	audit_receive_skb(skb);
	mutex_unlock(&audit_cmd_mutex);
}

*/ Run custom bind function on netlink socket group connect or bind requests. /*
static int audit_bind(struct netnet, int group)
{
	if (!capable(CAP_AUDIT_READ))
		return -EPERM;

	return 0;
}

static int __net_init audit_net_init(struct netnet)
{
	struct netlink_kernel_cfg cfg = {
		.input	= audit_receive,
		.bind	= audit_bind,
		.flags	= NL_CFG_F_NONROOT_RECV,
		.groups	= AUDIT_NLGRP_MAX,
	};

	struct audit_netaunet = net_generic(net, audit_net_id);

	aunet->nlsk = netlink_kernel_create(net, NETLINK_AUDIT, &cfg);
	if (aunet->nlsk == NULL) {
		audit_panic("cannot initialize netlink socket in namespace");
		return -ENOMEM;
	}
	aunet->nlsk->sk_sndtimeo = MAX_SCHEDULE_TIMEOUT;
	return 0;
}

static void __net_exit audit_net_exit(struct netnet)
{
	struct audit_netaunet = net_generic(net, audit_net_id);
	struct socksock = aunet->nlsk;
	if (sock == audit_sock) {
		audit_pid = 0;
		audit_sock = NULL;
	}

	RCU_INIT_POINTER(aunet->nlsk, NULL);
	synchronize_net();
	netlink_kernel_release(sock);
}

static struct pernet_operations audit_net_ops __net_initdata = {
	.init = audit_net_init,
	.exit = audit_net_exit,
	.id = &audit_net_id,
	.size = sizeof(struct audit_net),
};

*/ Initialize audit support at boot time. /*
static int __init audit_init(void)
{
	int i;

	if (audit_initialized == AUDIT_DISABLED)
		return 0;

	pr_info("initializing netlink subsys (%s)\n",
		audit_default ? "enabled" : "disabled");
	register_pernet_subsys(&audit_net_ops);

	skb_queue_head_init(&audit_skb_queue);
	skb_queue_head_init(&audit_skb_hold_queue);
	audit_initialized = AUDIT_INITIALIZED;
	audit_enabled = audit_default;
	audit_ever_enabled |= !!audit_default;

	audit_log(NULL, GFP_KERNEL, AUDIT_KERNEL, "initialized");

	for (i = 0; i < AUDIT_INODE_BUCKETS; i++)
		INIT_LIST_HEAD(&audit_inode_hash[i]);

	return 0;
}
__initcall(audit_init);

*/ Process kernel command-line parameter at boot time.  audit=0 or audit=1. /*
static int __init audit_enable(charstr)
{
	audit_default = !!simple_strtol(str, NULL, 0);
	if (!audit_default)
		audit_initialized = AUDIT_DISABLED;

	pr_info("%s\n", audit_default ?
		"enabled (after initialization)" : "disabled (until reboot)");

	return 1;
}
__setup("audit=", audit_enable);

*/ Process kernel command-line parameter at boot time.
 audit_backlog_limit=<n> /*
static int __init audit_backlog_limit_set(charstr)
{
	u32 audit_backlog_limit_arg;

	pr_info("audit_backlog_limit: ");
	if (kstrtouint(str, 0, &audit_backlog_limit_arg)) {
		pr_cont("using default of %u, unable to parse %s\n",
			audit_backlog_limit, str);
		return 1;
	}

	audit_backlog_limit = audit_backlog_limit_arg;
	pr_cont("%d\n", audit_backlog_limit);

	return 1;
}
__setup("audit_backlog_limit=", audit_backlog_limit_set);

static void audit_buffer_free(struct audit_bufferab)
{
	unsigned long flags;

	if (!ab)
		return;

	kfree_skb(ab->skb);
	spin_lock_irqsave(&audit_freelist_lock, flags);
	if (audit_freelist_count > AUDIT_MAXFREE)
		kfree(ab);
	else {
		audit_freelist_count++;
		list_add(&ab->list, &audit_freelist);
	}
	spin_unlock_irqrestore(&audit_freelist_lock, flags);
}

static struct audit_buffer audit_buffer_alloc(struct audit_contextctx,
						gfp_t gfp_mask, int type)
{
	unsigned long flags;
	struct audit_bufferab = NULL;
	struct nlmsghdrnlh;

	spin_lock_irqsave(&audit_freelist_lock, flags);
	if (!list_empty(&audit_freelist)) {
		ab = list_entry(audit_freelist.next,
				struct audit_buffer, list);
		list_del(&ab->list);
		--audit_freelist_count;
	}
	spin_unlock_irqrestore(&audit_freelist_lock, flags);

	if (!ab) {
		ab = kmalloc(sizeof(*ab), gfp_mask);
		if (!ab)
			goto err;
	}

	ab->ctx = ctx;
	ab->gfp_mask = gfp_mask;

	ab->skb = nlmsg_new(AUDIT_BUFSIZ, gfp_mask);
	if (!ab->skb)
		goto err;

	nlh = nlmsg_put(ab->skb, 0, 0, type, 0, 0);
	if (!nlh)
		goto out_kfree_skb;

	return ab;

out_kfree_skb:
	kfree_skb(ab->skb);
	ab->skb = NULL;
err:
	audit_buffer_free(ab);
	return NULL;
}

*/
 audit_serial - compute a serial number for the audit record

 Compute a serial number for the audit record.  Audit records are
 written to user-space as soon as they are generated, so a complete
 audit record may be written in several pieces.  The timestamp of the
 record and this serial number are used by the user-space tools to
 determine which pieces belong to the same audit record.  The
 (timestamp,serial) tuple is unique for each syscall and is live from
 syscall entry to syscall exit.

 NOTE: Another possibility is to store the formatted records off the
 audit context (for those records that have a context), and emit them
 all at syscall exit.  However, this could delay the reporting of
 significant errors until syscall exit (or never, if the system
 halts).
 /*
unsigned int audit_serial(void)
{
	static atomic_t serial = ATOMIC_INIT(0);

	return atomic_add_return(1, &serial);
}

static inline void audit_get_stamp(struct audit_contextctx,
				   struct timespect, unsigned intserial)
{
	if (!ctx || !auditsc_get_stamp(ctx, t, serial)) {
		*t = CURRENT_TIME;
		*serial = audit_serial();
	}
}

*/
 Wait for auditd to drain the queue a little
 /*
static long wait_for_auditd(long sleep_time)
{
	DECLARE_WAITQUEUE(wait, current);
	set_current_state(TASK_UNINTERRUPTIBLE);
	add_wait_queue_exclusive(&audit_backlog_wait, &wait);

	if (audit_backlog_limit &&
	    skb_queue_len(&audit_skb_queue) > audit_backlog_limit)
		sleep_time = schedule_timeout(sleep_time);

	__set_current_state(TASK_RUNNING);
	remove_wait_queue(&audit_backlog_wait, &wait);

	return sleep_time;
}

*/
 audit_log_start - obtain an audit buffer
 @ctx: audit_context (may be NULL)
 @gfp_mask: type of allocation
 @type: audit message type

 Returns audit_buffer pointer on success or NULL on error.

 Obtain an audit buffer.  This routine does locking to obtain the
 audit buffer, but then no locking is required for calls to
 audit_log_*format.  If the task (ctx) is a task that is currently in a
 syscall, then the syscall is marked as auditable and an audit record
 will be written at syscall exit.  If there is no associated task, then
 task context (ctx) should be NULL.
 /*
struct audit_bufferaudit_log_start(struct audit_contextctx, gfp_t gfp_mask,
				     int type)
{
	struct audit_buffer	*ab	= NULL;
	struct timespec		t;
	unsigned int		uninitialized_var(serial);
	int reserve = 5;/ Allow atomic callers to go up to five
			    entries over the normal backlog limit /*
	unsigned long timeout_start = jiffies;

	if (audit_initialized != AUDIT_INITIALIZED)
		return NULL;

	if (unlikely(audit_filter_type(type)))
		return NULL;

	if (gfp_mask & __GFP_DIRECT_RECLAIM) {
		if (audit_pid && audit_pid == current->tgid)
			gfp_mask &= ~__GFP_DIRECT_RECLAIM;
		else
			reserve = 0;
	}

	while (audit_backlog_limit
	       && skb_queue_len(&audit_skb_queue) > audit_backlog_limit + reserve) {
		if (gfp_mask & __GFP_DIRECT_RECLAIM && audit_backlog_wait_time) {
			long sleep_time;

			sleep_time = timeout_start + audit_backlog_wait_time - jiffies;
			if (sleep_time > 0) {
				sleep_time = wait_for_auditd(sleep_time);
				if (sleep_time > 0)
					continue;
			}
		}
		if (audit_rate_check() && printk_ratelimit())
			pr_warn("audit_backlog=%d > audit_backlog_limit=%d\n",
				skb_queue_len(&audit_skb_queue),
				audit_backlog_limit);
		audit_log_lost("backlog limit exceeded");
		audit_backlog_wait_time = 0;
		wake_up(&audit_backlog_wait);
		return NULL;
	}

	if (!reserve && !audit_backlog_wait_time)
		audit_backlog_wait_time = audit_backlog_wait_time_master;

	ab = audit_buffer_alloc(ctx, gfp_mask, type);
	if (!ab) {
		audit_log_lost("out of memory in audit_log_start");
		return NULL;
	}

	audit_get_stamp(ab->ctx, &t, &serial);

	audit_log_format(ab, "audit(%lu.%03lu:%u): ",
			 t.tv_sec, t.tv_nsec/1000000, serial);
	return ab;
}

*/
 audit_expand - expand skb in the audit buffer
 @ab: audit_buffer
 @extra: space to add at tail of the skb

 Returns 0 (no space) on failed expansion, or available space if
 successful.
 /*
static inline int audit_expand(struct audit_bufferab, int extra)
{
	struct sk_buffskb = ab->skb;
	int oldtail = skb_tailroom(skb);
	int ret = pskb_expand_head(skb, 0, extra, ab->gfp_mask);
	int newtail = skb_tailroom(skb);

	if (ret < 0) {
		audit_log_lost("out of memory in audit_expand");
		return 0;
	}

	skb->truesize += newtail - oldtail;
	return newtail;
}

*/
 Format an audit message into the audit buffer.  If there isn't enough
 room in the audit buffer, more room will be allocated and vsnprint
 will be called a second time.  Currently, we assume that a printk
 can't format message larger than 1024 bytes, so we don't either.
 /*
static void audit_log_vformat(struct audit_bufferab, const charfmt,
			      va_list args)
{
	int len, avail;
	struct sk_buffskb;
	va_list args2;

	if (!ab)
		return;

	BUG_ON(!ab->skb);
	skb = ab->skb;
	avail = skb_tailroom(skb);
	if (avail == 0) {
		avail = audit_expand(ab, AUDIT_BUFSIZ);
		if (!avail)
			goto out;
	}
	va_copy(args2, args);
	len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args);
	if (len >= avail) {
		*/ The printk buffer is 1024 bytes long, so if we get
		 here and AUDIT_BUFSIZ is at least 1024, then we can
		 log everything that printk could have logged. /*
		avail = audit_expand(ab,
			max_t(unsigned, AUDIT_BUFSIZ, 1+len-avail));
		if (!avail)
			goto out_va_end;
		len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args2);
	}
	if (len > 0)
		skb_put(skb, len);
out_va_end:
	va_end(args2);
out:
	return;
}

*/
 audit_log_format - format a message into the audit buffer.
 @ab: audit_buffer
 @fmt: format string
 @...: optional parameters matching @fmt string

 All the work is done in audit_log_vformat.
 /*
void audit_log_format(struct audit_bufferab, const charfmt, ...)
{
	va_list args;

	if (!ab)
		return;
	va_start(args, fmt);
	audit_log_vformat(ab, fmt, args);
	va_end(args);
}

*/
 audit_log_hex - convert a buffer to hex and append it to the audit skb
 @ab: the audit_buffer
 @buf: buffer to convert to hex
 @len: length of @buf to be converted

 No return value; failure to expand is silently ignored.

 This function will take the passed buf and convert it into a string of
 ascii hex digits. The new string is placed onto the skb.
 /*
void audit_log_n_hex(struct audit_bufferab, const unsigned charbuf,
		size_t len)
{
	int i, avail, new_len;
	unsigned charptr;
	struct sk_buffskb;

	if (!ab)
		return;

	BUG_ON(!ab->skb);
	skb = ab->skb;
	avail = skb_tailroom(skb);
	new_len = len<<1;
	if (new_len >= avail) {
		*/ Round the buffer request up to the next multiple /*
		new_len = AUDIT_BUFSIZ*(((new_len-avail)/AUDIT_BUFSIZ) + 1);
		avail = audit_expand(ab, new_len);
		if (!avail)
			return;
	}

	ptr = skb_tail_pointer(skb);
	for (i = 0; i < len; i++)
		ptr = hex_byte_pack_upper(ptr, buf[i]);
	*ptr = 0;
	skb_put(skb, len << 1);/ new string is twice the old string /*
}

*/
 Format a string of no more than slen characters into the audit buffer,
 enclosed in quote marks.
 /*
void audit_log_n_string(struct audit_bufferab, const charstring,
			size_t slen)
{
	int avail, new_len;
	unsigned charptr;
	struct sk_buffskb;

	if (!ab)
		return;

	BUG_ON(!ab->skb);
	skb = ab->skb;
	avail = skb_tailroom(skb);
	new_len = slen + 3;	*/ enclosing quotes + null terminator /*
	if (new_len > avail) {
		avail = audit_expand(ab, new_len);
		if (!avail)
			return;
	}
	ptr = skb_tail_pointer(skb);
	*ptr++ = '"';
	memcpy(ptr, string, slen);
	ptr += slen;
	*ptr++ = '"';
	*ptr = 0;
	skb_put(skb, slen + 2);	*/ don't include null terminator /*
}

*/
 audit_string_contains_control - does a string need to be logged in hex
 @string: string to be checked
 @len: max length of the string to check
 /*
bool audit_string_contains_control(const charstring, size_t len)
{
	const unsigned charp;
	for (p = string; p < (const unsigned char)string + len; p++) {
		if (*p == '"' ||p < 0x21 ||p > 0x7e)
			return true;
	}
	return false;
}

*/
 audit_log_n_untrustedstring - log a string that may contain random characters
 @ab: audit_buffer
 @len: length of string (not including trailing null)
 @string: string to be logged

 This code will escape a string that is passed to it if the string
 contains a control character, unprintable character, double quote mark,
 or a space. Unescaped strings will start and end with a double quote mark.
 Strings that are escaped are printed in hex (2 digits per char).

 The caller specifies the number of characters in the string to log, which may
 or may not be the entire string.
 /*
void audit_log_n_untrustedstring(struct audit_bufferab, const charstring,
				 size_t len)
{
	if (audit_string_contains_control(string, len))
		audit_log_n_hex(ab, string, len);
	else
		audit_log_n_string(ab, string, len);
}

*/
 audit_log_untrustedstring - log a string that may contain random characters
 @ab: audit_buffer
 @string: string to be logged

 Same as audit_log_n_untrustedstring(), except that strlen is used to
 determine string length.
 /*
void audit_log_untrustedstring(struct audit_bufferab, const charstring)
{
	audit_log_n_untrustedstring(ab, string, strlen(string));
}

*/ This is a helper-function to print the escaped d_path /*
void audit_log_d_path(struct audit_bufferab, const charprefix,
		      const struct pathpath)
{
	charp,pathname;

	if (prefix)
		audit_log_format(ab, "%s", prefix);

	*/ We will allow 11 spaces for ' (deleted)' to be appended /*
	pathname = kmalloc(PATH_MAX+11, ab->gfp_mask);
	if (!pathname) {
		audit_log_string(ab, "<no_memory>");
		return;
	}
	p = d_path(path, pathname, PATH_MAX+11);
	if (IS_ERR(p)) {/ Should never happen since we send PATH_MAX /*
		*/ FIXME: can we save some information here? /*
		audit_log_string(ab, "<too_long>");
	} else
		audit_log_untrustedstring(ab, p);
	kfree(pathname);
}

void audit_log_session_info(struct audit_bufferab)
{
	unsigned int sessionid = audit_get_sessionid(current);
	uid_t auid = from_kuid(&init_user_ns, audit_get_loginuid(current));

	audit_log_format(ab, " auid=%u ses=%u", auid, sessionid);
}

void audit_log_key(struct audit_bufferab, charkey)
{
	audit_log_format(ab, " key=");
	if (key)
		audit_log_untrustedstring(ab, key);
	else
		audit_log_format(ab, "(null)");
}

void audit_log_cap(struct audit_bufferab, charprefix, kernel_cap_tcap)
{
	int i;

	audit_log_format(ab, " %s=", prefix);
	CAP_FOR_EACH_U32(i) {
		audit_log_format(ab, "%08x",
				 cap->cap[CAP_LAST_U32 - i]);
	}
}

static void audit_log_fcaps(struct audit_bufferab, struct audit_namesname)
{
	kernel_cap_tperm = &name->fcap.permitted;
	kernel_cap_tinh = &name->fcap.inheritable;
	int log = 0;

	if (!cap_isclear(*perm)) {
		audit_log_cap(ab, "cap_fp", perm);
		log = 1;
	}
	if (!cap_isclear(*inh)) {
		audit_log_cap(ab, "cap_fi", inh);
		log = 1;
	}

	if (log)
		audit_log_format(ab, " cap_fe=%d cap_fver=%x",
				 name->fcap.fE, name->fcap_ver);
}

static inline int audit_copy_fcaps(struct audit_namesname,
				   const struct dentrydentry)
{
	struct cpu_vfs_cap_data caps;
	int rc;

	if (!dentry)
		return 0;

	rc = get_vfs_caps_from_disk(dentry, &caps);
	if (rc)
		return rc;

	name->fcap.permitted = caps.permitted;
	name->fcap.inheritable = caps.inheritable;
	name->fcap.fE = !!(caps.magic_etc & VFS_CAP_FLAGS_EFFECTIVE);
	name->fcap_ver = (caps.magic_etc & VFS_CAP_REVISION_MASK) >>
				VFS_CAP_REVISION_SHIFT;

	return 0;
}

*/ Copy inode data into an audit_names. /*
void audit_copy_inode(struct audit_namesname, const struct dentrydentry,
		      struct inodeinode)
{
	name->ino   = inode->i_ino;
	name->dev   = inode->i_sb->s_dev;
	name->mode  = inode->i_mode;
	name->uid   = inode->i_uid;
	name->gid   = inode->i_gid;
	name->rdev  = inode->i_rdev;
	security_inode_getsecid(inode, &name->osid);
	audit_copy_fcaps(name, dentry);
}

*/
 audit_log_name - produce AUDIT_PATH record from struct audit_names
 @context: audit_context for the task
 @n: audit_names structure with reportable details
 @path: optional path to report instead of audit_names->name
 @record_num: record number to report when handling a list of names
 @call_panic: optional pointer to int that will be updated if secid fails
 /*
void audit_log_name(struct audit_contextcontext, struct audit_namesn,
		    struct pathpath, int record_num, intcall_panic)
{
	struct audit_bufferab;
	ab = audit_log_start(context, GFP_KERNEL, AUDIT_PATH);
	if (!ab)
		return;

	audit_log_format(ab, "item=%d", record_num);

	if (path)
		audit_log_d_path(ab, " name=", path);
	else if (n->name) {
		switch (n->name_len) {
		case AUDIT_NAME_FULL:
			*/ log the full path /*
			audit_log_format(ab, " name=");
			audit_log_untrustedstring(ab, n->name->name);
			break;
		case 0:
			*/ name was specified as a relative path and the
			 directory component is the cwd /*
			audit_log_d_path(ab, " name=", &context->pwd);
			break;
		default:
			*/ log the name's directory component /*
			audit_log_format(ab, " name=");
			audit_log_n_untrustedstring(ab, n->name->name,
						    n->name_len);
		}
	} else
		audit_log_format(ab, " name=(null)");

	if (n->ino != AUDIT_INO_UNSET)
		audit_log_format(ab, " inode=%lu"
				 " dev=%02x:%02x mode=%#ho"
				 " ouid=%u ogid=%u rdev=%02x:%02x",
				 n->ino,
				 MAJOR(n->dev),
				 MINOR(n->dev),
				 n->mode,
				 from_kuid(&init_user_ns, n->uid),
				 from_kgid(&init_user_ns, n->gid),
				 MAJOR(n->rdev),
				 MINOR(n->rdev));
	if (n->osid != 0) {
		charctx = NULL;
		u32 len;
		if (security_secid_to_secctx(
			n->osid, &ctx, &len)) {
			audit_log_format(ab, " osid=%u", n->osid);
			if (call_panic)
				*call_panic = 2;
		} else {
			audit_log_format(ab, " obj=%s", ctx);
			security_release_secctx(ctx, len);
		}
	}

	*/ log the audit_names record type /*
	audit_log_format(ab, " nametype=");
	switch(n->type) {
	case AUDIT_TYPE_NORMAL:
		audit_log_format(ab, "NORMAL");
		break;
	case AUDIT_TYPE_PARENT:
		audit_log_format(ab, "PARENT");
		break;
	case AUDIT_TYPE_CHILD_DELETE:
		audit_log_format(ab, "DELETE");
		break;
	case AUDIT_TYPE_CHILD_CREATE:
		audit_log_format(ab, "CREATE");
		break;
	default:
		audit_log_format(ab, "UNKNOWN");
		break;
	}

	audit_log_fcaps(ab, n);
	audit_log_end(ab);
}

int audit_log_task_context(struct audit_bufferab)
{
	charctx = NULL;
	unsigned len;
	int error;
	u32 sid;

	security_task_getsecid(current, &sid);
	if (!sid)
		return 0;

	error = security_secid_to_secctx(sid, &ctx, &len);
	if (error) {
		if (error != -EINVAL)
			goto error_path;
		return 0;
	}

	audit_log_format(ab, " subj=%s", ctx);
	security_release_secctx(ctx, len);
	return 0;

error_path:
	audit_panic("error in audit_log_task_context");
	return error;
}
EXPORT_SYMBOL(audit_log_task_context);

void audit_log_d_path_exe(struct audit_bufferab,
			  struct mm_structmm)
{
	struct fileexe_file;

	if (!mm)
		goto out_null;

	exe_file = get_mm_exe_file(mm);
	if (!exe_file)
		goto out_null;

	audit_log_d_path(ab, " exe=", &exe_file->f_path);
	fput(exe_file);
	return;
out_null:
	audit_log_format(ab, " exe=(null)");
}

void audit_log_task_info(struct audit_bufferab, struct task_structtsk)
{
	const struct credcred;
	char comm[sizeof(tsk->comm)];
	chartty;

	if (!ab)
		return;

	*/ tsk == current /*
	cred = current_cred();

	spin_lock_irq(&tsk->sighand->siglock);
	if (tsk->signal && tsk->signal->tty && tsk->signal->tty->name)
		tty = tsk->signal->tty->name;
	else
		tty = "(none)";
	spin_unlock_irq(&tsk->sighand->siglock);

	audit_log_format(ab,
			 " ppid=%d pid=%d auid=%u uid=%u gid=%u"
			 " euid=%u suid=%u fsuid=%u"
			 " egid=%u sgid=%u fsgid=%u tty=%s ses=%u",
			 task_ppid_nr(tsk),
			 task_pid_nr(tsk),
			 from_kuid(&init_user_ns, audit_get_loginuid(tsk)),
			 from_kuid(&init_user_ns, cred->uid),
			 from_kgid(&init_user_ns, cred->gid),
			 from_kuid(&init_user_ns, cred->euid),
			 from_kuid(&init_user_ns, cred->suid),
			 from_kuid(&init_user_ns, cred->fsuid),
			 from_kgid(&init_user_ns, cred->egid),
			 from_kgid(&init_user_ns, cred->sgid),
			 from_kgid(&init_user_ns, cred->fsgid),
			 tty, audit_get_sessionid(tsk));

	audit_log_format(ab, " comm=");
	audit_log_untrustedstring(ab, get_task_comm(comm, tsk));

	audit_log_d_path_exe(ab, tsk->mm);
	audit_log_task_context(ab);
}
EXPORT_SYMBOL(audit_log_task_info);

*/
 audit_log_link_denied - report a link restriction denial
 @operation: specific link operation
 @link: the path that triggered the restriction
 /*
void audit_log_link_denied(const charoperation, struct pathlink)
{
	struct audit_bufferab;
	struct audit_namesname;

	name = kzalloc(sizeof(*name), GFP_NOFS);
	if (!name)
		return;

	*/ Generate AUDIT_ANOM_LINK with subject, operation, outcome. /*
	ab = audit_log_start(current->audit_context, GFP_KERNEL,
			     AUDIT_ANOM_LINK);
	if (!ab)
		goto out;
	audit_log_format(ab, "op=%s", operation);
	audit_log_task_info(ab, current);
	audit_log_format(ab, " res=0");
	audit_log_end(ab);

	*/ Generate AUDIT_PATH record with object. /*
	name->type = AUDIT_TYPE_NORMAL;
	audit_copy_inode(name, link->dentry, d_backing_inode(link->dentry));
	audit_log_name(current->audit_context, name, link, 0, NULL);
out:
	kfree(name);
}

*/
 audit_log_end - end one audit record
 @ab: the audit_buffer

 netlink_unicast() cannot be called inside an irq context because it blocks
 (last arg, flags, is not set to MSG_DONTWAIT), so the audit buffer is placed
 on a queue and a tasklet is scheduled to remove them from the queue outside
 the irq context.  May be called in any context.
 /*
void audit_log_end(struct audit_bufferab)
{
	if (!ab)
		return;
	if (!audit_rate_check()) {
		audit_log_lost("rate limit exceeded");
	} else {
		struct nlmsghdrnlh = nlmsg_hdr(ab->skb);

		nlh->nlmsg_len = ab->skb->len;
		kauditd_send_multicast_skb(ab->skb, ab->gfp_mask);

		*/
		 The original kaudit unicast socket sends up messages with
		 nlmsg_len set to the payload length rather than the entire
		 message length.  This breaks the standard set by netlink.
		 The existing auditd daemon assumes this breakage.  Fixing
		 this would require co-ordinating a change in the established
		 protocol between the kaudit kernel subsystem and the auditd
		 userspace code.
		 /*
		nlh->nlmsg_len -= NLMSG_HDRLEN;

		if (audit_pid) {
			skb_queue_tail(&audit_skb_queue, ab->skb);
			wake_up_interruptible(&kauditd_wait);
		} else {
			audit_printk_skb(ab->skb);
		}
		ab->skb = NULL;
	}
	audit_buffer_free(ab);
}

*/
 audit_log - Log an audit record
 @ctx: audit context
 @gfp_mask: type of allocation
 @type: audit message type
 @fmt: format string to use
 @...: variable parameters matching the format string

 This is a convenience function that calls audit_log_start,
 audit_log_vformat, and audit_log_end.  It may be called
 in any context.
 /*
void audit_log(struct audit_contextctx, gfp_t gfp_mask, int type,
	       const charfmt, ...)
{
	struct audit_bufferab;
	va_list args;

	ab = audit_log_start(ctx, gfp_mask, type);
	if (ab) {
		va_start(args, fmt);
		audit_log_vformat(ab, fmt, args);
		va_end(args);
		audit_log_end(ab);
	}
}

#ifdef CONFIG_SECURITY
*/
 audit_log_secctx - Converts and logs SELinux context
 @ab: audit_buffer
 @secid: security number

 This is a helper function that calls security_secid_to_secctx to convert
 secid to secctx and then adds the (converted) SELinux context to the audit
 log by calling audit_log_format, thus also preventing leak of internal secid
 to userspace. If secid cannot be converted audit_panic is called.
 /*
void audit_log_secctx(struct audit_bufferab, u32 secid)
{
	u32 len;
	charsecctx;

	if (security_secid_to_secctx(secid, &secctx, &len)) {
		audit_panic("Cannot convert secid to context");
	} else {
		audit_log_format(ab, " obj=%s", secctx);
		security_release_secctx(secctx, len);
	}
}
EXPORT_SYMBOL(audit_log_secctx);
#endif

EXPORT_SYMBOL(audit_log_start);
EXPORT_SYMBOL(audit_log_end);
EXPORT_SYMBOL(audit_log_format);
EXPORT_SYMBOL(audit_log);
*/
 auditfilter.c -- filtering of audit events

 Copyright 2003-2004 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/audit.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/fs.h>
#include <linux/namei.h>
#include <linux/netlink.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/security.h>
#include <net/net_namespace.h>
#include <net/sock.h>
#include "audit.h"

*/
 Locking model:

 audit_filter_mutex:
		Synchronizes writes and blocking reads of audit's filterlist
		data.  Rcu is used to traverse the filterlist and access
		contents of structs audit_entry, audit_watch and opaque
		LSM rules during filtering.  If modified, these structures
		must be copied and replace their counterparts in the filterlist.
		An audit_parent struct is not accessed during filtering, so may
		be written directly provided audit_filter_mutex is held.
 /*

*/ Audit filter lists, defined in <linux/audit.h> /*
struct list_head audit_filter_list[AUDIT_NR_FILTERS] = {
	LIST_HEAD_INIT(audit_filter_list[0]),
	LIST_HEAD_INIT(audit_filter_list[1]),
	LIST_HEAD_INIT(audit_filter_list[2]),
	LIST_HEAD_INIT(audit_filter_list[3]),
	LIST_HEAD_INIT(audit_filter_list[4]),
	LIST_HEAD_INIT(audit_filter_list[5]),
#if AUDIT_NR_FILTERS != 6
#error Fix audit_filter_list initialiser
#endif
};
static struct list_head audit_rules_list[AUDIT_NR_FILTERS] = {
	LIST_HEAD_INIT(audit_rules_list[0]),
	LIST_HEAD_INIT(audit_rules_list[1]),
	LIST_HEAD_INIT(audit_rules_list[2]),
	LIST_HEAD_INIT(audit_rules_list[3]),
	LIST_HEAD_INIT(audit_rules_list[4]),
	LIST_HEAD_INIT(audit_rules_list[5]),
};

DEFINE_MUTEX(audit_filter_mutex);

static void audit_free_lsm_field(struct audit_fieldf)
{
	switch (f->type) {
	case AUDIT_SUBJ_USER:
	case AUDIT_SUBJ_ROLE:
	case AUDIT_SUBJ_TYPE:
	case AUDIT_SUBJ_SEN:
	case AUDIT_SUBJ_CLR:
	case AUDIT_OBJ_USER:
	case AUDIT_OBJ_ROLE:
	case AUDIT_OBJ_TYPE:
	case AUDIT_OBJ_LEV_LOW:
	case AUDIT_OBJ_LEV_HIGH:
		kfree(f->lsm_str);
		security_audit_rule_free(f->lsm_rule);
	}
}

static inline void audit_free_rule(struct audit_entrye)
{
	int i;
	struct audit_kruleerule = &e->rule;

	*/ some rules don't have associated watches /*
	if (erule->watch)
		audit_put_watch(erule->watch);
	if (erule->fields)
		for (i = 0; i < erule->field_count; i++)
			audit_free_lsm_field(&erule->fields[i]);
	kfree(erule->fields);
	kfree(erule->filterkey);
	kfree(e);
}

void audit_free_rule_rcu(struct rcu_headhead)
{
	struct audit_entrye = container_of(head, struct audit_entry, rcu);
	audit_free_rule(e);
}

*/ Initialize an audit filterlist entry. /*
static inline struct audit_entryaudit_init_entry(u32 field_count)
{
	struct audit_entryentry;
	struct audit_fieldfields;

	entry = kzalloc(sizeof(*entry), GFP_KERNEL);
	if (unlikely(!entry))
		return NULL;

	fields = kcalloc(field_count, sizeof(*fields), GFP_KERNEL);
	if (unlikely(!fields)) {
		kfree(entry);
		return NULL;
	}
	entry->rule.fields = fields;

	return entry;
}

*/ Unpack a filter field's string representation from user-space
 buffer. /*
charaudit_unpack_string(void*bufp, size_tremain, size_t len)
{
	charstr;

	if (!*bufp || (len == 0) || (len >remain))
		return ERR_PTR(-EINVAL);

	*/ Of the currently implemented string fields, PATH_MAX
	 defines the longest valid length.
	 /*
	if (len > PATH_MAX)
		return ERR_PTR(-ENAMETOOLONG);

	str = kmalloc(len + 1, GFP_KERNEL);
	if (unlikely(!str))
		return ERR_PTR(-ENOMEM);

	memcpy(str,bufp, len);
	str[len] = 0;
	*bufp += len;
	*remain -= len;

	return str;
}

*/ Translate an inode field to kernel representation. /*
static inline int audit_to_inode(struct audit_krulekrule,
				 struct audit_fieldf)
{
	if (krule->listnr != AUDIT_FILTER_EXIT ||
	    krule->inode_f || krule->watch || krule->tree ||
	    (f->op != Audit_equal && f->op != Audit_not_equal))
		return -EINVAL;

	krule->inode_f = f;
	return 0;
}

static __u32classes[AUDIT_SYSCALL_CLASSES];

int __init audit_register_class(int class, unsignedlist)
{
	__u32p = kcalloc(AUDIT_BITMASK_SIZE, sizeof(__u32), GFP_KERNEL);
	if (!p)
		return -ENOMEM;
	while (*list != ~0U) {
		unsigned n =list++;
		if (n >= AUDIT_BITMASK_SIZE 32 - AUDIT_SYSCALL_CLASSES) {
			kfree(p);
			return -EINVAL;
		}
		p[AUDIT_WORD(n)] |= AUDIT_BIT(n);
	}
	if (class >= AUDIT_SYSCALL_CLASSES || classes[class]) {
		kfree(p);
		return -EINVAL;
	}
	classes[class] = p;
	return 0;
}

int audit_match_class(int class, unsigned syscall)
{
	if (unlikely(syscall >= AUDIT_BITMASK_SIZE 32))
		return 0;
	if (unlikely(class >= AUDIT_SYSCALL_CLASSES || !classes[class]))
		return 0;
	return classes[class][AUDIT_WORD(syscall)] & AUDIT_BIT(syscall);
}

#ifdef CONFIG_AUDITSYSCALL
static inline int audit_match_class_bits(int class, u32mask)
{
	int i;

	if (classes[class]) {
		for (i = 0; i < AUDIT_BITMASK_SIZE; i++)
			if (mask[i] & classes[class][i])
				return 0;
	}
	return 1;
}

static int audit_match_signal(struct audit_entryentry)
{
	struct audit_fieldarch = entry->rule.arch_f;

	if (!arch) {
		*/ When arch is unspecified, we must check both masks on biarch
		 as syscall number alone is ambiguous. /*
		return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
					       entry->rule.mask) &&
			audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
					       entry->rule.mask));
	}

	switch(audit_classify_arch(arch->val)) {
	case 0:/ native /*
		return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
					       entry->rule.mask));
	case 1:/ 32bit on biarch /*
		return (audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
					       entry->rule.mask));
	default:
		return 1;
	}
}
#endif

*/ Common user-space to kernel rule translation. /*
static inline struct audit_entryaudit_to_entry_common(struct audit_rule_datarule)
{
	unsigned listnr;
	struct audit_entryentry;
	int i, err;

	err = -EINVAL;
	listnr = rule->flags & ~AUDIT_FILTER_PREPEND;
	switch(listnr) {
	default:
		goto exit_err;
#ifdef CONFIG_AUDITSYSCALL
	case AUDIT_FILTER_ENTRY:
		if (rule->action == AUDIT_ALWAYS)
			goto exit_err;
	case AUDIT_FILTER_EXIT:
	case AUDIT_FILTER_TASK:
#endif
	case AUDIT_FILTER_USER:
	case AUDIT_FILTER_TYPE:
		;
	}
	if (unlikely(rule->action == AUDIT_POSSIBLE)) {
		pr_err("AUDIT_POSSIBLE is deprecated\n");
		goto exit_err;
	}
	if (rule->action != AUDIT_NEVER && rule->action != AUDIT_ALWAYS)
		goto exit_err;
	if (rule->field_count > AUDIT_MAX_FIELDS)
		goto exit_err;

	err = -ENOMEM;
	entry = audit_init_entry(rule->field_count);
	if (!entry)
		goto exit_err;

	entry->rule.flags = rule->flags & AUDIT_FILTER_PREPEND;
	entry->rule.listnr = listnr;
	entry->rule.action = rule->action;
	entry->rule.field_count = rule->field_count;

	for (i = 0; i < AUDIT_BITMASK_SIZE; i++)
		entry->rule.mask[i] = rule->mask[i];

	for (i = 0; i < AUDIT_SYSCALL_CLASSES; i++) {
		int bit = AUDIT_BITMASK_SIZE 32 - i - 1;
		__u32p = &entry->rule.mask[AUDIT_WORD(bit)];
		__u32class;

		if (!(*p & AUDIT_BIT(bit)))
			continue;
		*p &= ~AUDIT_BIT(bit);
		class = classes[i];
		if (class) {
			int j;
			for (j = 0; j < AUDIT_BITMASK_SIZE; j++)
				entry->rule.mask[j] |= class[j];
		}
	}

	return entry;

exit_err:
	return ERR_PTR(err);
}

static u32 audit_ops[] =
{
	[Audit_equal] = AUDIT_EQUAL,
	[Audit_not_equal] = AUDIT_NOT_EQUAL,
	[Audit_bitmask] = AUDIT_BIT_MASK,
	[Audit_bittest] = AUDIT_BIT_TEST,
	[Audit_lt] = AUDIT_LESS_THAN,
	[Audit_gt] = AUDIT_GREATER_THAN,
	[Audit_le] = AUDIT_LESS_THAN_OR_EQUAL,
	[Audit_ge] = AUDIT_GREATER_THAN_OR_EQUAL,
};

static u32 audit_to_op(u32 op)
{
	u32 n;
	for (n = Audit_equal; n < Audit_bad && audit_ops[n] != op; n++)
		;
	return n;
}

*/ check if an audit field is valid /*
static int audit_field_valid(struct audit_entryentry, struct audit_fieldf)
{
	switch(f->type) {
	case AUDIT_MSGTYPE:
		if (entry->rule.listnr != AUDIT_FILTER_TYPE &&
		    entry->rule.listnr != AUDIT_FILTER_USER)
			return -EINVAL;
		break;
	};

	switch(f->type) {
	default:
		return -EINVAL;
	case AUDIT_UID:
	case AUDIT_EUID:
	case AUDIT_SUID:
	case AUDIT_FSUID:
	case AUDIT_LOGINUID:
	case AUDIT_OBJ_UID:
	case AUDIT_GID:
	case AUDIT_EGID:
	case AUDIT_SGID:
	case AUDIT_FSGID:
	case AUDIT_OBJ_GID:
	case AUDIT_PID:
	case AUDIT_PERS:
	case AUDIT_MSGTYPE:
	case AUDIT_PPID:
	case AUDIT_DEVMAJOR:
	case AUDIT_DEVMINOR:
	case AUDIT_EXIT:
	case AUDIT_SUCCESS:
	case AUDIT_INODE:
		*/ bit ops are only useful on syscall args /*
		if (f->op == Audit_bitmask || f->op == Audit_bittest)
			return -EINVAL;
		break;
	case AUDIT_ARG0:
	case AUDIT_ARG1:
	case AUDIT_ARG2:
	case AUDIT_ARG3:
	case AUDIT_SUBJ_USER:
	case AUDIT_SUBJ_ROLE:
	case AUDIT_SUBJ_TYPE:
	case AUDIT_SUBJ_SEN:
	case AUDIT_SUBJ_CLR:
	case AUDIT_OBJ_USER:
	case AUDIT_OBJ_ROLE:
	case AUDIT_OBJ_TYPE:
	case AUDIT_OBJ_LEV_LOW:
	case AUDIT_OBJ_LEV_HIGH:
	case AUDIT_WATCH:
	case AUDIT_DIR:
	case AUDIT_FILTERKEY:
		break;
	case AUDIT_LOGINUID_SET:
		if ((f->val != 0) && (f->val != 1))
			return -EINVAL;
	*/ FALL THROUGH /*
	case AUDIT_ARCH:
		if (f->op != Audit_not_equal && f->op != Audit_equal)
			return -EINVAL;
		break;
	case AUDIT_PERM:
		if (f->val & ~15)
			return -EINVAL;
		break;
	case AUDIT_FILETYPE:
		if (f->val & ~S_IFMT)
			return -EINVAL;
		break;
	case AUDIT_FIELD_COMPARE:
		if (f->val > AUDIT_MAX_FIELD_COMPARE)
			return -EINVAL;
		break;
	case AUDIT_EXE:
		if (f->op != Audit_equal)
			return -EINVAL;
		if (entry->rule.listnr != AUDIT_FILTER_EXIT)
			return -EINVAL;
		break;
	};
	return 0;
}

*/ Translate struct audit_rule_data to kernel's rule representation. /*
static struct audit_entryaudit_data_to_entry(struct audit_rule_datadata,
					       size_t datasz)
{
	int err = 0;
	struct audit_entryentry;
	voidbufp;
	size_t remain = datasz - sizeof(struct audit_rule_data);
	int i;
	charstr;
	struct audit_fsnotify_markaudit_mark;

	entry = audit_to_entry_common(data);
	if (IS_ERR(entry))
		goto exit_nofree;

	bufp = data->buf;
	for (i = 0; i < data->field_count; i++) {
		struct audit_fieldf = &entry->rule.fields[i];

		err = -EINVAL;

		f->op = audit_to_op(data->fieldflags[i]);
		if (f->op == Audit_bad)
			goto exit_free;

		f->type = data->fields[i];
		f->val = data->values[i];

		*/ Support legacy tests for a valid loginuid /*
		if ((f->type == AUDIT_LOGINUID) && (f->val == AUDIT_UID_UNSET)) {
			f->type = AUDIT_LOGINUID_SET;
			f->val = 0;
			entry->rule.pflags |= AUDIT_LOGINUID_LEGACY;
		}

		err = audit_field_valid(entry, f);
		if (err)
			goto exit_free;

		err = -EINVAL;
		switch (f->type) {
		case AUDIT_LOGINUID:
		case AUDIT_UID:
		case AUDIT_EUID:
		case AUDIT_SUID:
		case AUDIT_FSUID:
		case AUDIT_OBJ_UID:
			f->uid = make_kuid(current_user_ns(), f->val);
			if (!uid_valid(f->uid))
				goto exit_free;
			break;
		case AUDIT_GID:
		case AUDIT_EGID:
		case AUDIT_SGID:
		case AUDIT_FSGID:
		case AUDIT_OBJ_GID:
			f->gid = make_kgid(current_user_ns(), f->val);
			if (!gid_valid(f->gid))
				goto exit_free;
			break;
		case AUDIT_ARCH:
			entry->rule.arch_f = f;
			break;
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			str = audit_unpack_string(&bufp, &remain, f->val);
			if (IS_ERR(str))
				goto exit_free;
			entry->rule.buflen += f->val;

			err = security_audit_rule_init(f->type, f->op, str,
						       (void*)&f->lsm_rule);
			*/ Keep currently invalid fields around in case they
			 become valid after a policy reload. /*
			if (err == -EINVAL) {
				pr_warn("audit rule for LSM \'%s\' is invalid\n",
					str);
				err = 0;
			}
			if (err) {
				kfree(str);
				goto exit_free;
			} else
				f->lsm_str = str;
			break;
		case AUDIT_WATCH:
			str = audit_unpack_string(&bufp, &remain, f->val);
			if (IS_ERR(str))
				goto exit_free;
			entry->rule.buflen += f->val;

			err = audit_to_watch(&entry->rule, str, f->val, f->op);
			if (err) {
				kfree(str);
				goto exit_free;
			}
			break;
		case AUDIT_DIR:
			str = audit_unpack_string(&bufp, &remain, f->val);
			if (IS_ERR(str))
				goto exit_free;
			entry->rule.buflen += f->val;

			err = audit_make_tree(&entry->rule, str, f->op);
			kfree(str);
			if (err)
				goto exit_free;
			break;
		case AUDIT_INODE:
			err = audit_to_inode(&entry->rule, f);
			if (err)
				goto exit_free;
			break;
		case AUDIT_FILTERKEY:
			if (entry->rule.filterkey || f->val > AUDIT_MAX_KEY_LEN)
				goto exit_free;
			str = audit_unpack_string(&bufp, &remain, f->val);
			if (IS_ERR(str))
				goto exit_free;
			entry->rule.buflen += f->val;
			entry->rule.filterkey = str;
			break;
		case AUDIT_EXE:
			if (entry->rule.exe || f->val > PATH_MAX)
				goto exit_free;
			str = audit_unpack_string(&bufp, &remain, f->val);
			if (IS_ERR(str)) {
				err = PTR_ERR(str);
				goto exit_free;
			}
			entry->rule.buflen += f->val;

			audit_mark = audit_alloc_mark(&entry->rule, str, f->val);
			if (IS_ERR(audit_mark)) {
				kfree(str);
				err = PTR_ERR(audit_mark);
				goto exit_free;
			}
			entry->rule.exe = audit_mark;
			break;
		}
	}

	if (entry->rule.inode_f && entry->rule.inode_f->op == Audit_not_equal)
		entry->rule.inode_f = NULL;

exit_nofree:
	return entry;

exit_free:
	if (entry->rule.tree)
		audit_put_tree(entry->rule.tree);/ that's the temporary one /*
	if (entry->rule.exe)
		audit_remove_mark(entry->rule.exe);/ that's the template one /*
	audit_free_rule(entry);
	return ERR_PTR(err);
}

*/ Pack a filter field's string representation into data block. /*
static inline size_t audit_pack_string(void*bufp, const charstr)
{
	size_t len = strlen(str);

	memcpy(*bufp, str, len);
	*bufp += len;

	return len;
}

*/ Translate kernel rule representation to struct audit_rule_data. /*
static struct audit_rule_dataaudit_krule_to_data(struct audit_krulekrule)
{
	struct audit_rule_datadata;
	voidbufp;
	int i;

	data = kmalloc(sizeof(*data) + krule->buflen, GFP_KERNEL);
	if (unlikely(!data))
		return NULL;
	memset(data, 0, sizeof(*data));

	data->flags = krule->flags | krule->listnr;
	data->action = krule->action;
	data->field_count = krule->field_count;
	bufp = data->buf;
	for (i = 0; i < data->field_count; i++) {
		struct audit_fieldf = &krule->fields[i];

		data->fields[i] = f->type;
		data->fieldflags[i] = audit_ops[f->op];
		switch(f->type) {
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			data->buflen += data->values[i] =
				audit_pack_string(&bufp, f->lsm_str);
			break;
		case AUDIT_WATCH:
			data->buflen += data->values[i] =
				audit_pack_string(&bufp,
						  audit_watch_path(krule->watch));
			break;
		case AUDIT_DIR:
			data->buflen += data->values[i] =
				audit_pack_string(&bufp,
						  audit_tree_path(krule->tree));
			break;
		case AUDIT_FILTERKEY:
			data->buflen += data->values[i] =
				audit_pack_string(&bufp, krule->filterkey);
			break;
		case AUDIT_EXE:
			data->buflen += data->values[i] =
				audit_pack_string(&bufp, audit_mark_path(krule->exe));
			break;
		case AUDIT_LOGINUID_SET:
			if (krule->pflags & AUDIT_LOGINUID_LEGACY && !f->val) {
				data->fields[i] = AUDIT_LOGINUID;
				data->values[i] = AUDIT_UID_UNSET;
				break;
			}
			*/ fallthrough if set /*
		default:
			data->values[i] = f->val;
		}
	}
	for (i = 0; i < AUDIT_BITMASK_SIZE; i++) data->mask[i] = krule->mask[i];

	return data;
}

*/ Compare two rules in kernel format.  Considered success if rules
 don't match. /*
static int audit_compare_rule(struct audit_krulea, struct audit_kruleb)
{
	int i;

	if (a->flags != b->flags ||
	    a->pflags != b->pflags ||
	    a->listnr != b->listnr ||
	    a->action != b->action ||
	    a->field_count != b->field_count)
		return 1;

	for (i = 0; i < a->field_count; i++) {
		if (a->fields[i].type != b->fields[i].type ||
		    a->fields[i].op != b->fields[i].op)
			return 1;

		switch(a->fields[i].type) {
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			if (strcmp(a->fields[i].lsm_str, b->fields[i].lsm_str))
				return 1;
			break;
		case AUDIT_WATCH:
			if (strcmp(audit_watch_path(a->watch),
				   audit_watch_path(b->watch)))
				return 1;
			break;
		case AUDIT_DIR:
			if (strcmp(audit_tree_path(a->tree),
				   audit_tree_path(b->tree)))
				return 1;
			break;
		case AUDIT_FILTERKEY:
			*/ both filterkeys exist based on above type compare /*
			if (strcmp(a->filterkey, b->filterkey))
				return 1;
			break;
		case AUDIT_EXE:
			*/ both paths exist based on above type compare /*
			if (strcmp(audit_mark_path(a->exe),
				   audit_mark_path(b->exe)))
				return 1;
			break;
		case AUDIT_UID:
		case AUDIT_EUID:
		case AUDIT_SUID:
		case AUDIT_FSUID:
		case AUDIT_LOGINUID:
		case AUDIT_OBJ_UID:
			if (!uid_eq(a->fields[i].uid, b->fields[i].uid))
				return 1;
			break;
		case AUDIT_GID:
		case AUDIT_EGID:
		case AUDIT_SGID:
		case AUDIT_FSGID:
		case AUDIT_OBJ_GID:
			if (!gid_eq(a->fields[i].gid, b->fields[i].gid))
				return 1;
			break;
		default:
			if (a->fields[i].val != b->fields[i].val)
				return 1;
		}
	}

	for (i = 0; i < AUDIT_BITMASK_SIZE; i++)
		if (a->mask[i] != b->mask[i])
			return 1;

	return 0;
}

*/ Duplicate LSM field information.  The lsm_rule is opaque, so must be
 re-initialized. /*
static inline int audit_dupe_lsm_field(struct audit_fielddf,
					   struct audit_fieldsf)
{
	int ret = 0;
	charlsm_str;

	*/ our own copy of lsm_str /*
	lsm_str = kstrdup(sf->lsm_str, GFP_KERNEL);
	if (unlikely(!lsm_str))
		return -ENOMEM;
	df->lsm_str = lsm_str;

	*/ our own (refreshed) copy of lsm_rule /*
	ret = security_audit_rule_init(df->type, df->op, df->lsm_str,
				       (void*)&df->lsm_rule);
	*/ Keep currently invalid fields around in case they
	 become valid after a policy reload. /*
	if (ret == -EINVAL) {
		pr_warn("audit rule for LSM \'%s\' is invalid\n",
			df->lsm_str);
		ret = 0;
	}

	return ret;
}

*/ Duplicate an audit rule.  This will be a deep copy with the exception
 of the watch - that pointer is carried over.  The LSM specific fields
 will be updated in the copy.  The point is to be able to replace the old
 rule with the new rule in the filterlist, then free the old rule.
 The rlist element is undefined; list manipulations are handled apart from
 the initial copy. /*
struct audit_entryaudit_dupe_rule(struct audit_kruleold)
{
	u32 fcount = old->field_count;
	struct audit_entryentry;
	struct audit_krulenew;
	charfk;
	int i, err = 0;

	entry = audit_init_entry(fcount);
	if (unlikely(!entry))
		return ERR_PTR(-ENOMEM);

	new = &entry->rule;
	new->flags = old->flags;
	new->pflags = old->pflags;
	new->listnr = old->listnr;
	new->action = old->action;
	for (i = 0; i < AUDIT_BITMASK_SIZE; i++)
		new->mask[i] = old->mask[i];
	new->prio = old->prio;
	new->buflen = old->buflen;
	new->inode_f = old->inode_f;
	new->field_count = old->field_count;

	*/
	 note that we are OK with not refcounting here; audit_match_tree()
	 never dereferences tree and we can't get false positives there
	 since we'd have to have rule gone from the listand* removed
	 before the chunks found by lookup had been allocated, i.e. before
	 the beginning of list scan.
	 /*
	new->tree = old->tree;
	memcpy(new->fields, old->fields, sizeof(struct audit_field) fcount);

	*/ deep copy this information, updating the lsm_rule fields, because
	 the originals will all be freed when the old rule is freed. /*
	for (i = 0; i < fcount; i++) {
		switch (new->fields[i].type) {
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			err = audit_dupe_lsm_field(&new->fields[i],
						       &old->fields[i]);
			break;
		case AUDIT_FILTERKEY:
			fk = kstrdup(old->filterkey, GFP_KERNEL);
			if (unlikely(!fk))
				err = -ENOMEM;
			else
				new->filterkey = fk;
			break;
		case AUDIT_EXE:
			err = audit_dupe_exe(new, old);
			break;
		}
		if (err) {
			if (new->exe)
				audit_remove_mark(new->exe);
			audit_free_rule(entry);
			return ERR_PTR(err);
		}
	}

	if (old->watch) {
		audit_get_watch(old->watch);
		new->watch = old->watch;
	}

	return entry;
}

*/ Find an existing audit rule.
 Caller must hold audit_filter_mutex to prevent stale rule data. /*
static struct audit_entryaudit_find_rule(struct audit_entryentry,
					   struct list_head*p)
{
	struct audit_entrye,found = NULL;
	struct list_headlist;
	int h;

	if (entry->rule.inode_f) {
		h = audit_hash_ino(entry->rule.inode_f->val);
		*p = list = &audit_inode_hash[h];
	} else if (entry->rule.watch) {
		*/ we don't know the inode number, so must walk entire hash /*
		for (h = 0; h < AUDIT_INODE_BUCKETS; h++) {
			list = &audit_inode_hash[h];
			list_for_each_entry(e, list, list)
				if (!audit_compare_rule(&entry->rule, &e->rule)) {
					found = e;
					goto out;
				}
		}
		goto out;
	} else {
		*p = list = &audit_filter_list[entry->rule.listnr];
	}

	list_for_each_entry(e, list, list)
		if (!audit_compare_rule(&entry->rule, &e->rule)) {
			found = e;
			goto out;
		}

out:
	return found;
}

static u64 prio_low = ~0ULL/2;
static u64 prio_high = ~0ULL/2 - 1;

*/ Add rule to given filterlist if not a duplicate. /*
static inline int audit_add_rule(struct audit_entryentry)
{
	struct audit_entrye;
	struct audit_watchwatch = entry->rule.watch;
	struct audit_treetree = entry->rule.tree;
	struct list_headlist;
	int err = 0;
#ifdef CONFIG_AUDITSYSCALL
	int dont_count = 0;

	*/ If either of these, don't count towards total /*
	if (entry->rule.listnr == AUDIT_FILTER_USER ||
		entry->rule.listnr == AUDIT_FILTER_TYPE)
		dont_count = 1;
#endif

	mutex_lock(&audit_filter_mutex);
	e = audit_find_rule(entry, &list);
	if (e) {
		mutex_unlock(&audit_filter_mutex);
		err = -EEXIST;
		*/ normally audit_add_tree_rule() will free it on failure /*
		if (tree)
			audit_put_tree(tree);
		return err;
	}

	if (watch) {
		*/ audit_filter_mutex is dropped and re-taken during this call /*
		err = audit_add_watch(&entry->rule, &list);
		if (err) {
			mutex_unlock(&audit_filter_mutex);
			*/
			 normally audit_add_tree_rule() will free it
			 on failure
			 /*
			if (tree)
				audit_put_tree(tree);
			return err;
		}
	}
	if (tree) {
		err = audit_add_tree_rule(&entry->rule);
		if (err) {
			mutex_unlock(&audit_filter_mutex);
			return err;
		}
	}

	entry->rule.prio = ~0ULL;
	if (entry->rule.listnr == AUDIT_FILTER_EXIT) {
		if (entry->rule.flags & AUDIT_FILTER_PREPEND)
			entry->rule.prio = ++prio_high;
		else
			entry->rule.prio = --prio_low;
	}

	if (entry->rule.flags & AUDIT_FILTER_PREPEND) {
		list_add(&entry->rule.list,
			 &audit_rules_list[entry->rule.listnr]);
		list_add_rcu(&entry->list, list);
		entry->rule.flags &= ~AUDIT_FILTER_PREPEND;
	} else {
		list_add_tail(&entry->rule.list,
			      &audit_rules_list[entry->rule.listnr]);
		list_add_tail_rcu(&entry->list, list);
	}
#ifdef CONFIG_AUDITSYSCALL
	if (!dont_count)
		audit_n_rules++;

	if (!audit_match_signal(entry))
		audit_signals++;
#endif
	mutex_unlock(&audit_filter_mutex);

	return err;
}

*/ Remove an existing rule from filterlist. /*
int audit_del_rule(struct audit_entryentry)
{
	struct audit_entry e;
	struct audit_treetree = entry->rule.tree;
	struct list_headlist;
	int ret = 0;
#ifdef CONFIG_AUDITSYSCALL
	int dont_count = 0;

	*/ If either of these, don't count towards total /*
	if (entry->rule.listnr == AUDIT_FILTER_USER ||
		entry->rule.listnr == AUDIT_FILTER_TYPE)
		dont_count = 1;
#endif

	mutex_lock(&audit_filter_mutex);
	e = audit_find_rule(entry, &list);
	if (!e) {
		ret = -ENOENT;
		goto out;
	}

	if (e->rule.watch)
		audit_remove_watch_rule(&e->rule);

	if (e->rule.tree)
		audit_remove_tree_rule(&e->rule);

	if (e->rule.exe)
		audit_remove_mark_rule(&e->rule);

#ifdef CONFIG_AUDITSYSCALL
	if (!dont_count)
		audit_n_rules--;

	if (!audit_match_signal(entry))
		audit_signals--;
#endif

	list_del_rcu(&e->list);
	list_del(&e->rule.list);
	call_rcu(&e->rcu, audit_free_rule_rcu);

out:
	mutex_unlock(&audit_filter_mutex);

	if (tree)
		audit_put_tree(tree);	*/ that's the temporary one /*

	return ret;
}

*/ List rules using struct audit_rule_data. /*
static void audit_list_rules(__u32 portid, int seq, struct sk_buff_headq)
{
	struct sk_buffskb;
	struct audit_kruler;
	int i;

	*/ This is a blocking read, so use audit_filter_mutex instead of rcu
	 iterator to sync with list writers. /*
	for (i=0; i<AUDIT_NR_FILTERS; i++) {
		list_for_each_entry(r, &audit_rules_list[i], list) {
			struct audit_rule_datadata;

			data = audit_krule_to_data(r);
			if (unlikely(!data))
				break;
			skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES,
					       0, 1, data,
					       sizeof(*data) + data->buflen);
			if (skb)
				skb_queue_tail(q, skb);
			kfree(data);
		}
	}
	skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES, 1, 1, NULL, 0);
	if (skb)
		skb_queue_tail(q, skb);
}

*/ Log rule additions and removals /*
static void audit_log_rule_change(charaction, struct audit_krulerule, int res)
{
	struct audit_bufferab;
	uid_t loginuid = from_kuid(&init_user_ns, audit_get_loginuid(current));
	unsigned int sessionid = audit_get_sessionid(current);

	if (!audit_enabled)
		return;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
	if (!ab)
		return;
	audit_log_format(ab, "auid=%u ses=%u" ,loginuid, sessionid);
	audit_log_task_context(ab);
	audit_log_format(ab, " op=");
	audit_log_string(ab, action);
	audit_log_key(ab, rule->filterkey);
	audit_log_format(ab, " list=%d res=%d", rule->listnr, res);
	audit_log_end(ab);
}

*/
 audit_rule_change - apply all rules to the specified message type
 @type: audit message type
 @portid: target port id for netlink audit messages
 @seq: netlink audit message sequence (serial) number
 @data: payload data
 @datasz: size of payload data
 /*
int audit_rule_change(int type, __u32 portid, int seq, voiddata,
			size_t datasz)
{
	int err = 0;
	struct audit_entryentry;

	entry = audit_data_to_entry(data, datasz);
	if (IS_ERR(entry))
		return PTR_ERR(entry);

	switch (type) {
	case AUDIT_ADD_RULE:
		err = audit_add_rule(entry);
		audit_log_rule_change("add_rule", &entry->rule, !err);
		break;
	case AUDIT_DEL_RULE:
		err = audit_del_rule(entry);
		audit_log_rule_change("remove_rule", &entry->rule, !err);
		break;
	default:
		err = -EINVAL;
		WARN_ON(1);
	}

	if (err || type == AUDIT_DEL_RULE) {
		if (entry->rule.exe)
			audit_remove_mark(entry->rule.exe);
		audit_free_rule(entry);
	}

	return err;
}

*/
 audit_list_rules_send - list the audit rules
 @request_skb: skb of request we are replying to (used to target the reply)
 @seq: netlink audit message sequence (serial) number
 /*
int audit_list_rules_send(struct sk_buffrequest_skb, int seq)
{
	u32 portid = NETLINK_CB(request_skb).portid;
	struct netnet = sock_net(NETLINK_CB(request_skb).sk);
	struct task_structtsk;
	struct audit_netlink_listdest;
	int err = 0;

	*/ We can't just spew out the rules here because we might fill
	 the available socket buffer space and deadlock waiting for
	 auditctl to read from it... which isn't ever going to
	 happen if we're actually running in the context of auditctl
	 trying to _send_ the stuff /*

	dest = kmalloc(sizeof(struct audit_netlink_list), GFP_KERNEL);
	if (!dest)
		return -ENOMEM;
	dest->net = get_net(net);
	dest->portid = portid;
	skb_queue_head_init(&dest->q);

	mutex_lock(&audit_filter_mutex);
	audit_list_rules(portid, seq, &dest->q);
	mutex_unlock(&audit_filter_mutex);

	tsk = kthread_run(audit_send_list, dest, "audit_send_list");
	if (IS_ERR(tsk)) {
		skb_queue_purge(&dest->q);
		kfree(dest);
		err = PTR_ERR(tsk);
	}

	return err;
}

int audit_comparator(u32 left, u32 op, u32 right)
{
	switch (op) {
	case Audit_equal:
		return (left == right);
	case Audit_not_equal:
		return (left != right);
	case Audit_lt:
		return (left < right);
	case Audit_le:
		return (left <= right);
	case Audit_gt:
		return (left > right);
	case Audit_ge:
		return (left >= right);
	case Audit_bitmask:
		return (left & right);
	case Audit_bittest:
		return ((left & right) == right);
	default:
		BUG();
		return 0;
	}
}

int audit_uid_comparator(kuid_t left, u32 op, kuid_t right)
{
	switch (op) {
	case Audit_equal:
		return uid_eq(left, right);
	case Audit_not_equal:
		return !uid_eq(left, right);
	case Audit_lt:
		return uid_lt(left, right);
	case Audit_le:
		return uid_lte(left, right);
	case Audit_gt:
		return uid_gt(left, right);
	case Audit_ge:
		return uid_gte(left, right);
	case Audit_bitmask:
	case Audit_bittest:
	default:
		BUG();
		return 0;
	}
}

int audit_gid_comparator(kgid_t left, u32 op, kgid_t right)
{
	switch (op) {
	case Audit_equal:
		return gid_eq(left, right);
	case Audit_not_equal:
		return !gid_eq(left, right);
	case Audit_lt:
		return gid_lt(left, right);
	case Audit_le:
		return gid_lte(left, right);
	case Audit_gt:
		return gid_gt(left, right);
	case Audit_ge:
		return gid_gte(left, right);
	case Audit_bitmask:
	case Audit_bittest:
	default:
		BUG();
		return 0;
	}
}

*/
 parent_len - find the length of the parent portion of a pathname
 @path: pathname of which to determine length
 /*
int parent_len(const charpath)
{
	int plen;
	const charp;

	plen = strlen(path);

	if (plen == 0)
		return plen;

	*/ disregard trailing slashes /*
	p = path + plen - 1;
	while ((*p == '/') && (p > path))
		p--;

	*/ walk backward until we find the next slash or hit beginning /*
	while ((*p != '/') && (p > path))
		p--;

	*/ did we find a slash? Then increment to include it in path /*
	if (*p == '/')
		p++;

	return p - path;
}

*/
 audit_compare_dname_path - compare given dentry name with last component in
 			      given path. Return of 0 indicates a match.
 @dname:	dentry name that we're comparing
 @path:	full pathname that we're comparing
 @parentlen:	length of the parent if known. Passing in AUDIT_NAME_FULL
 		here indicates that we must compute this value.
 /*
int audit_compare_dname_path(const chardname, const charpath, int parentlen)
{
	int dlen, pathlen;
	const charp;

	dlen = strlen(dname);
	pathlen = strlen(path);
	if (pathlen < dlen)
		return 1;

	parentlen = parentlen == AUDIT_NAME_FULL ? parent_len(path) : parentlen;
	if (pathlen - parentlen != dlen)
		return 1;

	p = path + parentlen;

	return strncmp(p, dname, dlen);
}

static int audit_filter_user_rules(struct audit_krulerule, int type,
				   enum audit_statestate)
{
	int i;

	for (i = 0; i < rule->field_count; i++) {
		struct audit_fieldf = &rule->fields[i];
		pid_t pid;
		int result = 0;
		u32 sid;

		switch (f->type) {
		case AUDIT_PID:
			pid = task_pid_nr(current);
			result = audit_comparator(pid, f->op, f->val);
			break;
		case AUDIT_UID:
			result = audit_uid_comparator(current_uid(), f->op, f->uid);
			break;
		case AUDIT_GID:
			result = audit_gid_comparator(current_gid(), f->op, f->gid);
			break;
		case AUDIT_LOGINUID:
			result = audit_uid_comparator(audit_get_loginuid(current),
						  f->op, f->uid);
			break;
		case AUDIT_LOGINUID_SET:
			result = audit_comparator(audit_loginuid_set(current),
						  f->op, f->val);
			break;
		case AUDIT_MSGTYPE:
			result = audit_comparator(type, f->op, f->val);
			break;
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
			if (f->lsm_rule) {
				security_task_getsecid(current, &sid);
				result = security_audit_rule_match(sid,
								   f->type,
								   f->op,
								   f->lsm_rule,
								   NULL);
			}
			break;
		}

		if (!result)
			return 0;
	}
	switch (rule->action) {
	case AUDIT_NEVER:   state = AUDIT_DISABLED;	    break;
	case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
	}
	return 1;
}

int audit_filter_user(int type)
{
	enum audit_state state = AUDIT_DISABLED;
	struct audit_entrye;
	int rc, ret;

	ret = 1;/ Audit by default /*

	rcu_read_lock();
	list_for_each_entry_rcu(e, &audit_filter_list[AUDIT_FILTER_USER], list) {
		rc = audit_filter_user_rules(&e->rule, type, &state);
		if (rc) {
			if (rc > 0 && state == AUDIT_DISABLED)
				ret = 0;
			break;
		}
	}
	rcu_read_unlock();

	return ret;
}

int audit_filter_type(int type)
{
	struct audit_entrye;
	int result = 0;

	rcu_read_lock();
	if (list_empty(&audit_filter_list[AUDIT_FILTER_TYPE]))
		goto unlock_and_return;

	list_for_each_entry_rcu(e, &audit_filter_list[AUDIT_FILTER_TYPE],
				list) {
		int i;
		for (i = 0; i < e->rule.field_count; i++) {
			struct audit_fieldf = &e->rule.fields[i];
			if (f->type == AUDIT_MSGTYPE) {
				result = audit_comparator(type, f->op, f->val);
				if (!result)
					break;
			}
		}
		if (result)
			goto unlock_and_return;
	}
unlock_and_return:
	rcu_read_unlock();
	return result;
}

static int update_lsm_rule(struct audit_kruler)
{
	struct audit_entryentry = container_of(r, struct audit_entry, rule);
	struct audit_entrynentry;
	int err = 0;

	if (!security_audit_rule_known(r))
		return 0;

	nentry = audit_dupe_rule(r);
	if (entry->rule.exe)
		audit_remove_mark(entry->rule.exe);
	if (IS_ERR(nentry)) {
		*/ save the first error encountered for the
		 return value /*
		err = PTR_ERR(nentry);
		audit_panic("error updating LSM filters");
		if (r->watch)
			list_del(&r->rlist);
		list_del_rcu(&entry->list);
		list_del(&r->list);
	} else {
		if (r->watch || r->tree)
			list_replace_init(&r->rlist, &nentry->rule.rlist);
		list_replace_rcu(&entry->list, &nentry->list);
		list_replace(&r->list, &nentry->rule.list);
	}
	call_rcu(&entry->rcu, audit_free_rule_rcu);

	return err;
}

*/ This function will re-initialize the lsm_rule field of all applicable rules.
 It will traverse the filter lists serarching for rules that contain LSM
 specific filter fields.  When such a rule is found, it is copied, the
 LSM field is re-initialized, and the old rule is replaced with the
 updated rule. /*
int audit_update_lsm_rules(void)
{
	struct audit_kruler,n;
	int i, err = 0;

	*/ audit_filter_mutex synchronizes the writers /*
	mutex_lock(&audit_filter_mutex);

	for (i = 0; i < AUDIT_NR_FILTERS; i++) {
		list_for_each_entry_safe(r, n, &audit_rules_list[i], list) {
			int res = update_lsm_rule(r);
			if (!err)
				err = res;
		}
	}
	mutex_unlock(&audit_filter_mutex);

	return err;
}
*/
 audit_fsnotify.c -- tracking inodes

 Copyright 2003-2009,2014-2015 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 /*

#include <linux/kernel.h>
#include <linux/audit.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/fs.h>
#include <linux/fsnotify_backend.h>
#include <linux/namei.h>
#include <linux/netlink.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/security.h>
#include "audit.h"

*/
 this mark lives on the parent directory of the inode in question.
 but dev, ino, and path are about the child
 /*
struct audit_fsnotify_mark {
	dev_t dev;		*/ associated superblock device /*
	unsigned long ino;	*/ associated inode number /*
	charpath;		*/ insertion path /*
	struct fsnotify_mark mark;/ fsnotify mark on the inode /*
	struct audit_krulerule;
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_fsnotify_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_EVENTS (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
			 FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_fsnotify_mark_free(struct audit_fsnotify_markaudit_mark)
{
	kfree(audit_mark->path);
	kfree(audit_mark);
}

static void audit_fsnotify_free_mark(struct fsnotify_markmark)
{
	struct audit_fsnotify_markaudit_mark;

	audit_mark = container_of(mark, struct audit_fsnotify_mark, mark);
	audit_fsnotify_mark_free(audit_mark);
}

charaudit_mark_path(struct audit_fsnotify_markmark)
{
	return mark->path;
}

int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev)
{
	if (mark->ino == AUDIT_INO_UNSET)
		return 0;
	return (mark->ino == ino) && (mark->dev == dev);
}

static void audit_update_mark(struct audit_fsnotify_markaudit_mark,
			     struct inodeinode)
{
	audit_mark->dev = inode ? inode->i_sb->s_dev : AUDIT_DEV_UNSET;
	audit_mark->ino = inode ? inode->i_ino : AUDIT_INO_UNSET;
}

struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len)
{
	struct audit_fsnotify_markaudit_mark;
	struct path path;
	struct dentrydentry;
	struct inodeinode;
	int ret;

	if (pathname[0] != '/' || pathname[len-1] == '/')
		return ERR_PTR(-EINVAL);

	dentry = kern_path_locked(pathname, &path);
	if (IS_ERR(dentry))
		return (void)dentry;/ returning an error /*
	inode = path.dentry->d_inode;
	inode_unlock(inode);

	audit_mark = kzalloc(sizeof(*audit_mark), GFP_KERNEL);
	if (unlikely(!audit_mark)) {
		audit_mark = ERR_PTR(-ENOMEM);
		goto out;
	}

	fsnotify_init_mark(&audit_mark->mark, audit_fsnotify_free_mark);
	audit_mark->mark.mask = AUDIT_FS_EVENTS;
	audit_mark->path = pathname;
	audit_update_mark(audit_mark, dentry->d_inode);
	audit_mark->rule = krule;

	ret = fsnotify_add_mark(&audit_mark->mark, audit_fsnotify_group, inode, NULL, true);
	if (ret < 0) {
		audit_fsnotify_mark_free(audit_mark);
		audit_mark = ERR_PTR(ret);
	}
out:
	dput(dentry);
	path_put(&path);
	return audit_mark;
}

static void audit_mark_log_rule_change(struct audit_fsnotify_markaudit_mark, charop)
{
	struct audit_bufferab;
	struct audit_krulerule = audit_mark->rule;

	if (!audit_enabled)
		return;
	ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return;
	audit_log_format(ab, "auid=%u ses=%u op=",
			 from_kuid(&init_user_ns, audit_get_loginuid(current)),
			 audit_get_sessionid(current));
	audit_log_string(ab, op);
	audit_log_format(ab, " path=");
	audit_log_untrustedstring(ab, audit_mark->path);
	audit_log_key(ab, rule->filterkey);
	audit_log_format(ab, " list=%d res=1", rule->listnr);
	audit_log_end(ab);
}

void audit_remove_mark(struct audit_fsnotify_markaudit_mark)
{
	fsnotify_destroy_mark(&audit_mark->mark, audit_fsnotify_group);
	fsnotify_put_mark(&audit_mark->mark);
}

void audit_remove_mark_rule(struct audit_krulekrule)
{
	struct audit_fsnotify_markmark = krule->exe;

	audit_remove_mark(mark);
}

static void audit_autoremove_mark_rule(struct audit_fsnotify_markaudit_mark)
{
	struct audit_krulerule = audit_mark->rule;
	struct audit_entryentry = container_of(rule, struct audit_entry, rule);

	audit_mark_log_rule_change(audit_mark, "autoremove_rule");
	audit_del_rule(entry);
}

*/ Update mark data in audit rules based on fsnotify events. /*
static int audit_mark_handle_event(struct fsnotify_groupgroup,
				    struct inodeto_tell,
				    struct fsnotify_markinode_mark,
				    struct fsnotify_markvfsmount_mark,
				    u32 mask, voiddata, int data_type,
				    const unsigned chardname, u32 cookie)
{
	struct audit_fsnotify_markaudit_mark;
	struct inodeinode = NULL;

	audit_mark = container_of(inode_mark, struct audit_fsnotify_mark, mark);

	BUG_ON(group != audit_fsnotify_group);

	switch (data_type) {
	case (FSNOTIFY_EVENT_PATH):
		inode = ((struct path)data)->dentry->d_inode;
		break;
	case (FSNOTIFY_EVENT_INODE):
		inode = (struct inode)data;
		break;
	default:
		BUG();
		return 0;
	};

	if (mask & (FS_CREATE|FS_MOVED_TO|FS_DELETE|FS_MOVED_FROM)) {
		if (audit_compare_dname_path(dname, audit_mark->path, AUDIT_NAME_FULL))
			return 0;
		audit_update_mark(audit_mark, inode);
	} else if (mask & (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
		audit_autoremove_mark_rule(audit_mark);

	return 0;
}

static const struct fsnotify_ops audit_mark_fsnotify_ops = {
	.handle_event =	audit_mark_handle_event,
};

static int __init audit_fsnotify_init(void)
{
	audit_fsnotify_group = fsnotify_alloc_group(&audit_mark_fsnotify_ops);
	if (IS_ERR(audit_fsnotify_group)) {
		audit_fsnotify_group = NULL;
		audit_panic("cannot create audit fsnotify group");
	}
	return 0;
}
device_initcall(audit_fsnotify_init);
*/
 audit -- definition of audit_context structure and supporting types 

 Copyright 2003-2004 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include <linux/fs.h>
#include <linux/audit.h>
#include <linux/skbuff.h>
#include <uapi/linux/mqueue.h>

*/ AUDIT_NAMES is the number of slots we reserve in the audit_context
 for saving names from getname().  If we get more names we will allocate
 a name dynamically and also add those to the list anchored by names_list. /*
#define AUDIT_NAMES	5

*/ At task start time, the audit_state is set in the audit_context using
   a per-task filter.  At syscall entry, the audit_state is augmented by
   the syscall filter. /*
enum audit_state {
	AUDIT_DISABLED,		*/ Do not create per-task audit_context.
				 No syscall-specific audit records can
				 be generated. /*
	AUDIT_BUILD_CONTEXT,	*/ Create the per-task audit_context,
				 and fill it in at syscall
				 entry time.  This makes a full
				 syscall record available if some
				 other part of the kernel decides it
				 should be recorded. /*
	AUDIT_RECORD_CONTEXT	*/ Create the per-task audit_context,
				 always fill it in at syscall entry
				 time, and always write out the audit
				 record at syscall exit time.  /*
};

*/ Rule lists /*
struct audit_watch;
struct audit_fsnotify_mark;
struct audit_tree;
struct audit_chunk;

struct audit_entry {
	struct list_head	list;
	struct rcu_head		rcu;
	struct audit_krule	rule;
};

struct audit_cap_data {
	kernel_cap_t		permitted;
	kernel_cap_t		inheritable;
	union {
		unsigned int	fE;		*/ effective bit of file cap /*
		kernel_cap_t	effective;	*/ effective set of process /*
	};
};

*/ When fs/namei.c:getname() is called, we store the pointer in name and bump
 the refcnt in the associated filename struct.

 Further, in fs/namei.c:path_lookup() we store the inode and device.
 /*
struct audit_names {
	struct list_head	list;		*/ audit_context->names_list /*

	struct filename		*name;
	int			name_len;	*/ number of chars to log /*
	bool			hidden;		*/ don't log this record /*

	unsigned long		ino;
	dev_t			dev;
	umode_t			mode;
	kuid_t			uid;
	kgid_t			gid;
	dev_t			rdev;
	u32			osid;
	struct audit_cap_data	fcap;
	unsigned int		fcap_ver;
	unsigned char		type;		*/ record type /*
	*/
	 This was an allocated audit_names and not from the array of
	 names allocated in the task audit context.  Thus this name
	 should be freed on syscall exit.
	 /*
	bool			should_free;
};

struct audit_proctitle {
	int	len;	*/ length of the cmdline field. /*
	char	*value;	*/ the cmdline field /*
};

*/ The per-task audit context. /*
struct audit_context {
	int		    dummy;	*/ must be the first element /*
	int		    in_syscall;	*/ 1 if task is in a syscall /*
	enum audit_state    state, current_state;
	unsigned int	    serial;    / serial number for record /*
	int		    major;     / syscall number /*
	struct timespec	    ctime;     / time of syscall entry /*
	unsigned long	    argv[4];   / syscall arguments /*
	long		    return_code;*/ syscall return code /*
	u64		    prio;
	int		    return_valid;/ return code is valid /*
	*/
	 The names_list is the list of all audit_names collected during this
	 syscall.  The first AUDIT_NAMES entries in the names_list will
	 actually be from the preallocated_names array for performance
	 reasons.  Except during allocation they should never be referenced
	 through the preallocated_names array and should only be found/used
	 by running the names_list.
	 /*
	struct audit_names  preallocated_names[AUDIT_NAMES];
	int		    name_count;/ total records in names_list /*
	struct list_head    names_list;	*/ struct audit_names->list anchor /*
	char		   filterkey;	*/ key for rule that triggered record /*
	struct path	    pwd;
	struct audit_aux_dataaux;
	struct audit_aux_dataaux_pids;
	struct sockaddr_storagesockaddr;
	size_t sockaddr_len;
				*/ Save things to print about task_struct /*
	pid_t		    pid, ppid;
	kuid_t		    uid, euid, suid, fsuid;
	kgid_t		    gid, egid, sgid, fsgid;
	unsigned long	    personality;
	int		    arch;

	pid_t		    target_pid;
	kuid_t		    target_auid;
	kuid_t		    target_uid;
	unsigned int	    target_sessionid;
	u32		    target_sid;
	char		    target_comm[TASK_COMM_LEN];

	struct audit_tree_refstrees,first_trees;
	struct list_head killed_trees;
	int tree_count;

	int type;
	union {
		struct {
			int nargs;
			long args[6];
		} socketcall;
		struct {
			kuid_t			uid;
			kgid_t			gid;
			umode_t			mode;
			u32			osid;
			int			has_perm;
			uid_t			perm_uid;
			gid_t			perm_gid;
			umode_t			perm_mode;
			unsigned long		qbytes;
		} ipc;
		struct {
			mqd_t			mqdes;
			struct mq_attr		mqstat;
		} mq_getsetattr;
		struct {
			mqd_t			mqdes;
			int			sigev_signo;
		} mq_notify;
		struct {
			mqd_t			mqdes;
			size_t			msg_len;
			unsigned int		msg_prio;
			struct timespec		abs_timeout;
		} mq_sendrecv;
		struct {
			int			oflag;
			umode_t			mode;
			struct mq_attr		attr;
		} mq_open;
		struct {
			pid_t			pid;
			struct audit_cap_data	cap;
		} capset;
		struct {
			int			fd;
			int			flags;
		} mmap;
		struct {
			int			argc;
		} execve;
	};
	int fds[2];
	struct audit_proctitle proctitle;
};

extern u32 audit_ever_enabled;

extern void audit_copy_inode(struct audit_namesname,
			     const struct dentrydentry,
			     struct inodeinode);
extern void audit_log_cap(struct audit_bufferab, charprefix,
			  kernel_cap_tcap);
extern void audit_log_name(struct audit_contextcontext,
			   struct audit_namesn, struct pathpath,
			   int record_num, intcall_panic);

extern int audit_pid;

#define AUDIT_INODE_BUCKETS	32
extern struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];

static inline int audit_hash_ino(u32 ino)
{
	return (ino & (AUDIT_INODE_BUCKETS-1));
}

*/ Indicates that audit should log the full pathname. /*
#define AUDIT_NAME_FULL -1

extern int audit_match_class(int class, unsigned syscall);
extern int audit_comparator(const u32 left, const u32 op, const u32 right);
extern int audit_uid_comparator(kuid_t left, u32 op, kuid_t right);
extern int audit_gid_comparator(kgid_t left, u32 op, kgid_t right);
extern int parent_len(const charpath);
extern int audit_compare_dname_path(const chardname, const charpath, int plen);
extern struct sk_buffaudit_make_reply(__u32 portid, int seq, int type,
					int done, int multi,
					const voidpayload, int size);
extern void		    audit_panic(const charmessage);

struct audit_netlink_list {
	__u32 portid;
	struct netnet;
	struct sk_buff_head q;
};

int audit_send_list(void);

struct audit_net {
	struct socknlsk;
};

extern int selinux_audit_rule_update(void);

extern struct mutex audit_filter_mutex;
extern int audit_del_rule(struct audit_entry);
extern void audit_free_rule_rcu(struct rcu_head);
extern struct list_head audit_filter_list[];

extern struct audit_entryaudit_dupe_rule(struct audit_kruleold);

extern void audit_log_d_path_exe(struct audit_bufferab,
				 struct mm_structmm);

*/ audit watch functions /*
#ifdef CONFIG_AUDIT_WATCH
extern void audit_put_watch(struct audit_watchwatch);
extern void audit_get_watch(struct audit_watchwatch);
extern int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op);
extern int audit_add_watch(struct audit_krulekrule, struct list_head*list);
extern void audit_remove_watch_rule(struct audit_krulekrule);
extern charaudit_watch_path(struct audit_watchwatch);
extern int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev);

extern struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len);
extern charaudit_mark_path(struct audit_fsnotify_markmark);
extern void audit_remove_mark(struct audit_fsnotify_markaudit_mark);
extern void audit_remove_mark_rule(struct audit_krulekrule);
extern int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev);
extern int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold);
extern int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark);

#else
#define audit_put_watch(w) {}
#define audit_get_watch(w) {}
#define audit_to_watch(k, p, l, o) (-EINVAL)
#define audit_add_watch(k, l) (-EINVAL)
#define audit_remove_watch_rule(k) BUG()
#define audit_watch_path(w) ""
#define audit_watch_compare(w, i, d) 0

#define audit_alloc_mark(k, p, l) (ERR_PTR(-EINVAL))
#define audit_mark_path(m) ""
#define audit_remove_mark(m)
#define audit_remove_mark_rule(k)
#define audit_mark_compare(m, i, d) 0
#define audit_exe_compare(t, m) (-EINVAL)
#define audit_dupe_exe(n, o) (-EINVAL)
#endif */ CONFIG_AUDIT_WATCH /*

#ifdef CONFIG_AUDIT_TREE
extern struct audit_chunkaudit_tree_lookup(const struct inode);
extern void audit_put_chunk(struct audit_chunk);
extern bool audit_tree_match(struct audit_chunk, struct audit_tree);
extern int audit_make_tree(struct audit_krule, char, u32);
extern int audit_add_tree_rule(struct audit_krule);
extern int audit_remove_tree_rule(struct audit_krule);
extern void audit_trim_trees(void);
extern int audit_tag_tree(charold, charnew);
extern const charaudit_tree_path(struct audit_tree);
extern void audit_put_tree(struct audit_tree);
extern void audit_kill_trees(struct list_head);
#else
#define audit_remove_tree_rule(rule) BUG()
#define audit_add_tree_rule(rule) -EINVAL
#define audit_make_tree(rule, str, op) -EINVAL
#define audit_trim_trees() (void)0
#define audit_put_tree(tree) (void)0
#define audit_tag_tree(old, new) -EINVAL
#define audit_tree_path(rule) ""	*/ never called /*
#define audit_kill_trees(list) BUG()
#endif

extern charaudit_unpack_string(void*, size_t, size_t);

extern pid_t audit_sig_pid;
extern kuid_t audit_sig_uid;
extern u32 audit_sig_sid;

#ifdef CONFIG_AUDITSYSCALL
extern int __audit_signal_info(int sig, struct task_structt);
static inline int audit_signal_info(int sig, struct task_structt)
{
	if (unlikely((audit_pid && t->tgid == audit_pid) ||
		     (audit_signals && !audit_dummy_context())))
		return __audit_signal_info(sig, t);
	return 0;
}
extern void audit_filter_inodes(struct task_struct, struct audit_context);
extern struct list_headaudit_killed_trees(void);
#else
#define audit_signal_info(s,t) AUDIT_DISABLED
#define audit_filter_inodes(t,c) AUDIT_DISABLED
#endif

extern struct mutex audit_cmd_mutex;
*/
 auditsc.c -- System-call auditing support
 Handles all system-call specific auditing features.

 Copyright 2003-2004 Red Hat Inc., Durham, North Carolina.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright (C) 2005, 2006 IBM Corporation
 All Rights Reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Written by Rickard E. (Rik) Faith <faith@redhat.com>

 Many of the ideas implemented here are from Stephen C. Tweedie,
 especially the idea of avoiding a copy by using getname.

 The method for actual interception of syscall entry and exit (not in
 this file -- see entry.S) is based on a GPL'd patch written by
 okir@suse.de and Copyright 2003 SuSE Linux AG.

 POSIX message queue support added by George Wilson <ltcgcw@us.ibm.com>,
 2006.

 The support of additional filter rules compares (>, <, >=, <=) was
 added by Dustin Kirkland <dustin.kirkland@us.ibm.com>, 2005.

 Modified by Amy Griffis <amy.griffis@hp.com> to collect additional
 filesystem information.

 Subject and object context labeling support added by <danjones@us.ibm.com>
 and <dustin.kirkland@us.ibm.com> for LSPP certification compliance.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/init.h>
#include <asm/types.h>
#include <linux/atomic.h>
#include <linux/fs.h>
#include <linux/namei.h>
#include <linux/mm.h>
#include <linux/export.h>
#include <linux/slab.h>
#include <linux/mount.h>
#include <linux/socket.h>
#include <linux/mqueue.h>
#include <linux/audit.h>
#include <linux/personality.h>
#include <linux/time.h>
#include <linux/netlink.h>
#include <linux/compiler.h>
#include <asm/unistd.h>
#include <linux/security.h>
#include <linux/list.h>
#include <linux/tty.h>
#include <linux/binfmts.h>
#include <linux/highmem.h>
#include <linux/syscalls.h>
#include <asm/syscall.h>
#include <linux/capability.h>
#include <linux/fs_struct.h>
#include <linux/compat.h>
#include <linux/ctype.h>
#include <linux/string.h>
#include <uapi/linux/limits.h>

#include "audit.h"

*/ flags stating the success for a syscall /*
#define AUDITSC_INVALID 0
#define AUDITSC_SUCCESS 1
#define AUDITSC_FAILURE 2

*/ no execve audit message should be longer than this (userspace limits) /*
#define MAX_EXECVE_AUDIT_LEN 7500

*/ max length to print of cmdline/proctitle value during audit /*
#define MAX_PROCTITLE_AUDIT_LEN 128

*/ number of audit rules /*
int audit_n_rules;

*/ determines whether we collect data for signals sent /*
int audit_signals;

struct audit_aux_data {
	struct audit_aux_data	*next;
	int			type;
};

#define AUDIT_AUX_IPCPERM	0

*/ Number of target pids per aux struct. /*
#define AUDIT_AUX_PIDS	16

struct audit_aux_data_pids {
	struct audit_aux_data	d;
	pid_t			target_pid[AUDIT_AUX_PIDS];
	kuid_t			target_auid[AUDIT_AUX_PIDS];
	kuid_t			target_uid[AUDIT_AUX_PIDS];
	unsigned int		target_sessionid[AUDIT_AUX_PIDS];
	u32			target_sid[AUDIT_AUX_PIDS];
	char 			target_comm[AUDIT_AUX_PIDS][TASK_COMM_LEN];
	int			pid_count;
};

struct audit_aux_data_bprm_fcaps {
	struct audit_aux_data	d;
	struct audit_cap_data	fcap;
	unsigned int		fcap_ver;
	struct audit_cap_data	old_pcap;
	struct audit_cap_data	new_pcap;
};

struct audit_tree_refs {
	struct audit_tree_refsnext;
	struct audit_chunkc[31];
};

static int audit_match_perm(struct audit_contextctx, int mask)
{
	unsigned n;
	if (unlikely(!ctx))
		return 0;
	n = ctx->major;

	switch (audit_classify_syscall(ctx->arch, n)) {
	case 0:	*/ native /*
		if ((mask & AUDIT_PERM_WRITE) &&
		     audit_match_class(AUDIT_CLASS_WRITE, n))
			return 1;
		if ((mask & AUDIT_PERM_READ) &&
		     audit_match_class(AUDIT_CLASS_READ, n))
			return 1;
		if ((mask & AUDIT_PERM_ATTR) &&
		     audit_match_class(AUDIT_CLASS_CHATTR, n))
			return 1;
		return 0;
	case 1:/ 32bit on biarch /*
		if ((mask & AUDIT_PERM_WRITE) &&
		     audit_match_class(AUDIT_CLASS_WRITE_32, n))
			return 1;
		if ((mask & AUDIT_PERM_READ) &&
		     audit_match_class(AUDIT_CLASS_READ_32, n))
			return 1;
		if ((mask & AUDIT_PERM_ATTR) &&
		     audit_match_class(AUDIT_CLASS_CHATTR_32, n))
			return 1;
		return 0;
	case 2:/ open /*
		return mask & ACC_MODE(ctx->argv[1]);
	case 3:/ openat /*
		return mask & ACC_MODE(ctx->argv[2]);
	case 4:/ socketcall /*
		return ((mask & AUDIT_PERM_WRITE) && ctx->argv[0] == SYS_BIND);
	case 5:/ execve /*
		return mask & AUDIT_PERM_EXEC;
	default:
		return 0;
	}
}

static int audit_match_filetype(struct audit_contextctx, int val)
{
	struct audit_namesn;
	umode_t mode = (umode_t)val;

	if (unlikely(!ctx))
		return 0;

	list_for_each_entry(n, &ctx->names_list, list) {
		if ((n->ino != AUDIT_INO_UNSET) &&
		    ((n->mode & S_IFMT) == mode))
			return 1;
	}

	return 0;
}

*/
 We keep a linked list of fixed-sized (31 pointer) arrays of audit_chunk;
 ->first_trees points to its beginning, ->trees - to the current end of data.
 ->tree_count is the number of free entries in array pointed to by ->trees.
 Original condition is (NULL, NULL, 0); as soon as it grows we never revert to NULL,
 "empty" becomes (p, p, 31) afterwards.  We don't shrink the list (and seriously,
 it's going to remain 1-element for almost any setup) until we free context itself.
 References in it _are_ dropped - at the same time we free/drop aux stuff.
 /*

#ifdef CONFIG_AUDIT_TREE
static void audit_set_auditable(struct audit_contextctx)
{
	if (!ctx->prio) {
		ctx->prio = 1;
		ctx->current_state = AUDIT_RECORD_CONTEXT;
	}
}

static int put_tree_ref(struct audit_contextctx, struct audit_chunkchunk)
{
	struct audit_tree_refsp = ctx->trees;
	int left = ctx->tree_count;
	if (likely(left)) {
		p->c[--left] = chunk;
		ctx->tree_count = left;
		return 1;
	}
	if (!p)
		return 0;
	p = p->next;
	if (p) {
		p->c[30] = chunk;
		ctx->trees = p;
		ctx->tree_count = 30;
		return 1;
	}
	return 0;
}

static int grow_tree_refs(struct audit_contextctx)
{
	struct audit_tree_refsp = ctx->trees;
	ctx->trees = kzalloc(sizeof(struct audit_tree_refs), GFP_KERNEL);
	if (!ctx->trees) {
		ctx->trees = p;
		return 0;
	}
	if (p)
		p->next = ctx->trees;
	else
		ctx->first_trees = ctx->trees;
	ctx->tree_count = 31;
	return 1;
}
#endif

static void unroll_tree_refs(struct audit_contextctx,
		      struct audit_tree_refsp, int count)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_tree_refsq;
	int n;
	if (!p) {
		*/ we started with empty chain /*
		p = ctx->first_trees;
		count = 31;
		*/ if the very first allocation has failed, nothing to do /*
		if (!p)
			return;
	}
	n = count;
	for (q = p; q != ctx->trees; q = q->next, n = 31) {
		while (n--) {
			audit_put_chunk(q->c[n]);
			q->c[n] = NULL;
		}
	}
	while (n-- > ctx->tree_count) {
		audit_put_chunk(q->c[n]);
		q->c[n] = NULL;
	}
	ctx->trees = p;
	ctx->tree_count = count;
#endif
}

static void free_tree_refs(struct audit_contextctx)
{
	struct audit_tree_refsp,q;
	for (p = ctx->first_trees; p; p = q) {
		q = p->next;
		kfree(p);
	}
}

static int match_tree_refs(struct audit_contextctx, struct audit_treetree)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_tree_refsp;
	int n;
	if (!tree)
		return 0;
	*/ full ones /*
	for (p = ctx->first_trees; p != ctx->trees; p = p->next) {
		for (n = 0; n < 31; n++)
			if (audit_tree_match(p->c[n], tree))
				return 1;
	}
	*/ partial /*
	if (p) {
		for (n = ctx->tree_count; n < 31; n++)
			if (audit_tree_match(p->c[n], tree))
				return 1;
	}
#endif
	return 0;
}

static int audit_compare_uid(kuid_t uid,
			     struct audit_namesname,
			     struct audit_fieldf,
			     struct audit_contextctx)
{
	struct audit_namesn;
	int rc;
 
	if (name) {
		rc = audit_uid_comparator(uid, f->op, name->uid);
		if (rc)
			return rc;
	}
 
	if (ctx) {
		list_for_each_entry(n, &ctx->names_list, list) {
			rc = audit_uid_comparator(uid, f->op, n->uid);
			if (rc)
				return rc;
		}
	}
	return 0;
}

static int audit_compare_gid(kgid_t gid,
			     struct audit_namesname,
			     struct audit_fieldf,
			     struct audit_contextctx)
{
	struct audit_namesn;
	int rc;
 
	if (name) {
		rc = audit_gid_comparator(gid, f->op, name->gid);
		if (rc)
			return rc;
	}
 
	if (ctx) {
		list_for_each_entry(n, &ctx->names_list, list) {
			rc = audit_gid_comparator(gid, f->op, n->gid);
			if (rc)
				return rc;
		}
	}
	return 0;
}

static int audit_field_compare(struct task_structtsk,
			       const struct credcred,
			       struct audit_fieldf,
			       struct audit_contextctx,
			       struct audit_namesname)
{
	switch (f->val) {
	*/ process to file object comparisons /*
	case AUDIT_COMPARE_UID_TO_OBJ_UID:
		return audit_compare_uid(cred->uid, name, f, ctx);
	case AUDIT_COMPARE_GID_TO_OBJ_GID:
		return audit_compare_gid(cred->gid, name, f, ctx);
	case AUDIT_COMPARE_EUID_TO_OBJ_UID:
		return audit_compare_uid(cred->euid, name, f, ctx);
	case AUDIT_COMPARE_EGID_TO_OBJ_GID:
		return audit_compare_gid(cred->egid, name, f, ctx);
	case AUDIT_COMPARE_AUID_TO_OBJ_UID:
		return audit_compare_uid(tsk->loginuid, name, f, ctx);
	case AUDIT_COMPARE_SUID_TO_OBJ_UID:
		return audit_compare_uid(cred->suid, name, f, ctx);
	case AUDIT_COMPARE_SGID_TO_OBJ_GID:
		return audit_compare_gid(cred->sgid, name, f, ctx);
	case AUDIT_COMPARE_FSUID_TO_OBJ_UID:
		return audit_compare_uid(cred->fsuid, name, f, ctx);
	case AUDIT_COMPARE_FSGID_TO_OBJ_GID:
		return audit_compare_gid(cred->fsgid, name, f, ctx);
	*/ uid comparisons /*
	case AUDIT_COMPARE_UID_TO_AUID:
		return audit_uid_comparator(cred->uid, f->op, tsk->loginuid);
	case AUDIT_COMPARE_UID_TO_EUID:
		return audit_uid_comparator(cred->uid, f->op, cred->euid);
	case AUDIT_COMPARE_UID_TO_SUID:
		return audit_uid_comparator(cred->uid, f->op, cred->suid);
	case AUDIT_COMPARE_UID_TO_FSUID:
		return audit_uid_comparator(cred->uid, f->op, cred->fsuid);
	*/ auid comparisons /*
	case AUDIT_COMPARE_AUID_TO_EUID:
		return audit_uid_comparator(tsk->loginuid, f->op, cred->euid);
	case AUDIT_COMPARE_AUID_TO_SUID:
		return audit_uid_comparator(tsk->loginuid, f->op, cred->suid);
	case AUDIT_COMPARE_AUID_TO_FSUID:
		return audit_uid_comparator(tsk->loginuid, f->op, cred->fsuid);
	*/ euid comparisons /*
	case AUDIT_COMPARE_EUID_TO_SUID:
		return audit_uid_comparator(cred->euid, f->op, cred->suid);
	case AUDIT_COMPARE_EUID_TO_FSUID:
		return audit_uid_comparator(cred->euid, f->op, cred->fsuid);
	*/ suid comparisons /*
	case AUDIT_COMPARE_SUID_TO_FSUID:
		return audit_uid_comparator(cred->suid, f->op, cred->fsuid);
	*/ gid comparisons /*
	case AUDIT_COMPARE_GID_TO_EGID:
		return audit_gid_comparator(cred->gid, f->op, cred->egid);
	case AUDIT_COMPARE_GID_TO_SGID:
		return audit_gid_comparator(cred->gid, f->op, cred->sgid);
	case AUDIT_COMPARE_GID_TO_FSGID:
		return audit_gid_comparator(cred->gid, f->op, cred->fsgid);
	*/ egid comparisons /*
	case AUDIT_COMPARE_EGID_TO_SGID:
		return audit_gid_comparator(cred->egid, f->op, cred->sgid);
	case AUDIT_COMPARE_EGID_TO_FSGID:
		return audit_gid_comparator(cred->egid, f->op, cred->fsgid);
	*/ sgid comparison /*
	case AUDIT_COMPARE_SGID_TO_FSGID:
		return audit_gid_comparator(cred->sgid, f->op, cred->fsgid);
	default:
		WARN(1, "Missing AUDIT_COMPARE define.  Report as a bug\n");
		return 0;
	}
	return 0;
}

*/ Determine if any context name data matches a rule's watch data /*
*/ Compare a task_struct with an audit_rule.  Return 1 on match, 0
 otherwise.

 If task_creation is true, this is an explicit indication that we are
 filtering a task rule at task creation time.  This and tsk == current are
 the only situations where tsk->cred may be accessed without an rcu read lock.
 /*
static int audit_filter_rules(struct task_structtsk,
			      struct audit_krulerule,
			      struct audit_contextctx,
			      struct audit_namesname,
			      enum audit_statestate,
			      bool task_creation)
{
	const struct credcred;
	int i, need_sid = 1;
	u32 sid;

	cred = rcu_dereference_check(tsk->cred, tsk == current || task_creation);

	for (i = 0; i < rule->field_count; i++) {
		struct audit_fieldf = &rule->fields[i];
		struct audit_namesn;
		int result = 0;
		pid_t pid;

		switch (f->type) {
		case AUDIT_PID:
			pid = task_pid_nr(tsk);
			result = audit_comparator(pid, f->op, f->val);
			break;
		case AUDIT_PPID:
			if (ctx) {
				if (!ctx->ppid)
					ctx->ppid = task_ppid_nr(tsk);
				result = audit_comparator(ctx->ppid, f->op, f->val);
			}
			break;
		case AUDIT_EXE:
			result = audit_exe_compare(tsk, rule->exe);
			break;
		case AUDIT_UID:
			result = audit_uid_comparator(cred->uid, f->op, f->uid);
			break;
		case AUDIT_EUID:
			result = audit_uid_comparator(cred->euid, f->op, f->uid);
			break;
		case AUDIT_SUID:
			result = audit_uid_comparator(cred->suid, f->op, f->uid);
			break;
		case AUDIT_FSUID:
			result = audit_uid_comparator(cred->fsuid, f->op, f->uid);
			break;
		case AUDIT_GID:
			result = audit_gid_comparator(cred->gid, f->op, f->gid);
			if (f->op == Audit_equal) {
				if (!result)
					result = in_group_p(f->gid);
			} else if (f->op == Audit_not_equal) {
				if (result)
					result = !in_group_p(f->gid);
			}
			break;
		case AUDIT_EGID:
			result = audit_gid_comparator(cred->egid, f->op, f->gid);
			if (f->op == Audit_equal) {
				if (!result)
					result = in_egroup_p(f->gid);
			} else if (f->op == Audit_not_equal) {
				if (result)
					result = !in_egroup_p(f->gid);
			}
			break;
		case AUDIT_SGID:
			result = audit_gid_comparator(cred->sgid, f->op, f->gid);
			break;
		case AUDIT_FSGID:
			result = audit_gid_comparator(cred->fsgid, f->op, f->gid);
			break;
		case AUDIT_PERS:
			result = audit_comparator(tsk->personality, f->op, f->val);
			break;
		case AUDIT_ARCH:
			if (ctx)
				result = audit_comparator(ctx->arch, f->op, f->val);
			break;

		case AUDIT_EXIT:
			if (ctx && ctx->return_valid)
				result = audit_comparator(ctx->return_code, f->op, f->val);
			break;
		case AUDIT_SUCCESS:
			if (ctx && ctx->return_valid) {
				if (f->val)
					result = audit_comparator(ctx->return_valid, f->op, AUDITSC_SUCCESS);
				else
					result = audit_comparator(ctx->return_valid, f->op, AUDITSC_FAILURE);
			}
			break;
		case AUDIT_DEVMAJOR:
			if (name) {
				if (audit_comparator(MAJOR(name->dev), f->op, f->val) ||
				    audit_comparator(MAJOR(name->rdev), f->op, f->val))
					++result;
			} else if (ctx) {
				list_for_each_entry(n, &ctx->names_list, list) {
					if (audit_comparator(MAJOR(n->dev), f->op, f->val) ||
					    audit_comparator(MAJOR(n->rdev), f->op, f->val)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_DEVMINOR:
			if (name) {
				if (audit_comparator(MINOR(name->dev), f->op, f->val) ||
				    audit_comparator(MINOR(name->rdev), f->op, f->val))
					++result;
			} else if (ctx) {
				list_for_each_entry(n, &ctx->names_list, list) {
					if (audit_comparator(MINOR(n->dev), f->op, f->val) ||
					    audit_comparator(MINOR(n->rdev), f->op, f->val)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_INODE:
			if (name)
				result = audit_comparator(name->ino, f->op, f->val);
			else if (ctx) {
				list_for_each_entry(n, &ctx->names_list, list) {
					if (audit_comparator(n->ino, f->op, f->val)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_OBJ_UID:
			if (name) {
				result = audit_uid_comparator(name->uid, f->op, f->uid);
			} else if (ctx) {
				list_for_each_entry(n, &ctx->names_list, list) {
					if (audit_uid_comparator(n->uid, f->op, f->uid)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_OBJ_GID:
			if (name) {
				result = audit_gid_comparator(name->gid, f->op, f->gid);
			} else if (ctx) {
				list_for_each_entry(n, &ctx->names_list, list) {
					if (audit_gid_comparator(n->gid, f->op, f->gid)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_WATCH:
			if (name)
				result = audit_watch_compare(rule->watch, name->ino, name->dev);
			break;
		case AUDIT_DIR:
			if (ctx)
				result = match_tree_refs(ctx, rule->tree);
			break;
		case AUDIT_LOGINUID:
			result = audit_uid_comparator(tsk->loginuid, f->op, f->uid);
			break;
		case AUDIT_LOGINUID_SET:
			result = audit_comparator(audit_loginuid_set(tsk), f->op, f->val);
			break;
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
			*/ NOTE: this may return negative values indicating
			   a temporary error.  We simply treat this as a
			   match for now to avoid losing information that
			   may be wanted.   An error message will also be
			   logged upon error /*
			if (f->lsm_rule) {
				if (need_sid) {
					security_task_getsecid(tsk, &sid);
					need_sid = 0;
				}
				result = security_audit_rule_match(sid, f->type,
				                                  f->op,
				                                  f->lsm_rule,
				                                  ctx);
			}
			break;
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			*/ The above note for AUDIT_SUBJ_USER...AUDIT_SUBJ_CLR
			   also applies here /*
			if (f->lsm_rule) {
				*/ Find files that match /*
				if (name) {
					result = security_audit_rule_match(
					           name->osid, f->type, f->op,
					           f->lsm_rule, ctx);
				} else if (ctx) {
					list_for_each_entry(n, &ctx->names_list, list) {
						if (security_audit_rule_match(n->osid, f->type,
									      f->op, f->lsm_rule,
									      ctx)) {
							++result;
							break;
						}
					}
				}
				*/ Find ipc objects that match /*
				if (!ctx || ctx->type != AUDIT_IPC)
					break;
				if (security_audit_rule_match(ctx->ipc.osid,
							      f->type, f->op,
							      f->lsm_rule, ctx))
					++result;
			}
			break;
		case AUDIT_ARG0:
		case AUDIT_ARG1:
		case AUDIT_ARG2:
		case AUDIT_ARG3:
			if (ctx)
				result = audit_comparator(ctx->argv[f->type-AUDIT_ARG0], f->op, f->val);
			break;
		case AUDIT_FILTERKEY:
			*/ ignore this field for filtering /*
			result = 1;
			break;
		case AUDIT_PERM:
			result = audit_match_perm(ctx, f->val);
			break;
		case AUDIT_FILETYPE:
			result = audit_match_filetype(ctx, f->val);
			break;
		case AUDIT_FIELD_COMPARE:
			result = audit_field_compare(tsk, cred, f, ctx, name);
			break;
		}
		if (!result)
			return 0;
	}

	if (ctx) {
		if (rule->prio <= ctx->prio)
			return 0;
		if (rule->filterkey) {
			kfree(ctx->filterkey);
			ctx->filterkey = kstrdup(rule->filterkey, GFP_ATOMIC);
		}
		ctx->prio = rule->prio;
	}
	switch (rule->action) {
	case AUDIT_NEVER:   state = AUDIT_DISABLED;	    break;
	case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
	}
	return 1;
}

*/ At process creation time, we can determine if system-call auditing is
 completely disabled for this task.  Since we only have the task
 structure at this point, we can only check uid and gid.
 /*
static enum audit_state audit_filter_task(struct task_structtsk, char*key)
{
	struct audit_entrye;
	enum audit_state   state;

	rcu_read_lock();
	list_for_each_entry_rcu(e, &audit_filter_list[AUDIT_FILTER_TASK], list) {
		if (audit_filter_rules(tsk, &e->rule, NULL, NULL,
				       &state, true)) {
			if (state == AUDIT_RECORD_CONTEXT)
				*key = kstrdup(e->rule.filterkey, GFP_ATOMIC);
			rcu_read_unlock();
			return state;
		}
	}
	rcu_read_unlock();
	return AUDIT_BUILD_CONTEXT;
}

static int audit_in_mask(const struct audit_krulerule, unsigned long val)
{
	int word, bit;

	if (val > 0xffffffff)
		return false;

	word = AUDIT_WORD(val);
	if (word >= AUDIT_BITMASK_SIZE)
		return false;

	bit = AUDIT_BIT(val);

	return rule->mask[word] & bit;
}

*/ At syscall entry and exit time, this filter is called if the
 audit_state is not low enough that auditing cannot take place, but is
 also not high enough that we already know we have to write an audit
 record (i.e., the state is AUDIT_SETUP_CONTEXT or AUDIT_BUILD_CONTEXT).
 /*
static enum audit_state audit_filter_syscall(struct task_structtsk,
					     struct audit_contextctx,
					     struct list_headlist)
{
	struct audit_entrye;
	enum audit_state state;

	if (audit_pid && tsk->tgid == audit_pid)
		return AUDIT_DISABLED;

	rcu_read_lock();
	if (!list_empty(list)) {
		list_for_each_entry_rcu(e, list, list) {
			if (audit_in_mask(&e->rule, ctx->major) &&
			    audit_filter_rules(tsk, &e->rule, ctx, NULL,
					       &state, false)) {
				rcu_read_unlock();
				ctx->current_state = state;
				return state;
			}
		}
	}
	rcu_read_unlock();
	return AUDIT_BUILD_CONTEXT;
}

*/
 Given an audit_name check the inode hash table to see if they match.
 Called holding the rcu read lock to protect the use of audit_inode_hash
 /*
static int audit_filter_inode_name(struct task_structtsk,
				   struct audit_namesn,
				   struct audit_contextctx) {
	int h = audit_hash_ino((u32)n->ino);
	struct list_headlist = &audit_inode_hash[h];
	struct audit_entrye;
	enum audit_state state;

	if (list_empty(list))
		return 0;

	list_for_each_entry_rcu(e, list, list) {
		if (audit_in_mask(&e->rule, ctx->major) &&
		    audit_filter_rules(tsk, &e->rule, ctx, n, &state, false)) {
			ctx->current_state = state;
			return 1;
		}
	}

	return 0;
}

*/ At syscall exit time, this filter is called if any audit_names have been
 collected during syscall processing.  We only check rules in sublists at hash
 buckets applicable to the inode numbers in audit_names.
 Regarding audit_state, same rules apply as for audit_filter_syscall().
 /*
void audit_filter_inodes(struct task_structtsk, struct audit_contextctx)
{
	struct audit_namesn;

	if (audit_pid && tsk->tgid == audit_pid)
		return;

	rcu_read_lock();

	list_for_each_entry(n, &ctx->names_list, list) {
		if (audit_filter_inode_name(tsk, n, ctx))
			break;
	}
	rcu_read_unlock();
}

*/ Transfer the audit context pointer to the caller, clearing it in the tsk's struct /*
static inline struct audit_contextaudit_take_context(struct task_structtsk,
						      int return_valid,
						      long return_code)
{
	struct audit_contextcontext = tsk->audit_context;

	if (!context)
		return NULL;
	context->return_valid = return_valid;

	*/
	 we need to fix up the return code in the audit logs if the actual
	 return codes are later going to be fixed up by the arch specific
	 signal handlers
	
	 This is actually a test for:
	 (rc == ERESTARTSYS ) || (rc == ERESTARTNOINTR) ||
	 (rc == ERESTARTNOHAND) || (rc == ERESTART_RESTARTBLOCK)
	
	 but is faster than a bunch of ||
	 /*
	if (unlikely(return_code <= -ERESTARTSYS) &&
	    (return_code >= -ERESTART_RESTARTBLOCK) &&
	    (return_code != -ENOIOCTLCMD))
		context->return_code = -EINTR;
	else
		context->return_code  = return_code;

	if (context->in_syscall && !context->dummy) {
		audit_filter_syscall(tsk, context, &audit_filter_list[AUDIT_FILTER_EXIT]);
		audit_filter_inodes(tsk, context);
	}

	tsk->audit_context = NULL;
	return context;
}

static inline void audit_proctitle_free(struct audit_contextcontext)
{
	kfree(context->proctitle.value);
	context->proctitle.value = NULL;
	context->proctitle.len = 0;
}

static inline void audit_free_names(struct audit_contextcontext)
{
	struct audit_namesn,next;

	list_for_each_entry_safe(n, next, &context->names_list, list) {
		list_del(&n->list);
		if (n->name)
			putname(n->name);
		if (n->should_free)
			kfree(n);
	}
	context->name_count = 0;
	path_put(&context->pwd);
	context->pwd.dentry = NULL;
	context->pwd.mnt = NULL;
}

static inline void audit_free_aux(struct audit_contextcontext)
{
	struct audit_aux_dataaux;

	while ((aux = context->aux)) {
		context->aux = aux->next;
		kfree(aux);
	}
	while ((aux = context->aux_pids)) {
		context->aux_pids = aux->next;
		kfree(aux);
	}
}

static inline struct audit_contextaudit_alloc_context(enum audit_state state)
{
	struct audit_contextcontext;

	context = kzalloc(sizeof(*context), GFP_KERNEL);
	if (!context)
		return NULL;
	context->state = state;
	context->prio = state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;
	INIT_LIST_HEAD(&context->killed_trees);
	INIT_LIST_HEAD(&context->names_list);
	return context;
}

*/
 audit_alloc - allocate an audit context block for a task
 @tsk: task

 Filter on the task information and allocate a per-task audit context
 if necessary.  Doing so turns on system call auditing for the
 specified task.  This is called from copy_process, so no lock is
 needed.
 /*
int audit_alloc(struct task_structtsk)
{
	struct audit_contextcontext;
	enum audit_state     state;
	charkey = NULL;

	if (likely(!audit_ever_enabled))
		return 0;/ Return if not auditing. /*

	state = audit_filter_task(tsk, &key);
	if (state == AUDIT_DISABLED) {
		clear_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
		return 0;
	}

	if (!(context = audit_alloc_context(state))) {
		kfree(key);
		audit_log_lost("out of memory in audit_alloc");
		return -ENOMEM;
	}
	context->filterkey = key;

	tsk->audit_context  = context;
	set_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
	return 0;
}

static inline void audit_free_context(struct audit_contextcontext)
{
	audit_free_names(context);
	unroll_tree_refs(context, NULL, 0);
	free_tree_refs(context);
	audit_free_aux(context);
	kfree(context->filterkey);
	kfree(context->sockaddr);
	audit_proctitle_free(context);
	kfree(context);
}

static int audit_log_pid_context(struct audit_contextcontext, pid_t pid,
				 kuid_t auid, kuid_t uid, unsigned int sessionid,
				 u32 sid, charcomm)
{
	struct audit_bufferab;
	charctx = NULL;
	u32 len;
	int rc = 0;

	ab = audit_log_start(context, GFP_KERNEL, AUDIT_OBJ_PID);
	if (!ab)
		return rc;

	audit_log_format(ab, "opid=%d oauid=%d ouid=%d oses=%d", pid,
			 from_kuid(&init_user_ns, auid),
			 from_kuid(&init_user_ns, uid), sessionid);
	if (sid) {
		if (security_secid_to_secctx(sid, &ctx, &len)) {
			audit_log_format(ab, " obj=(none)");
			rc = 1;
		} else {
			audit_log_format(ab, " obj=%s", ctx);
			security_release_secctx(ctx, len);
		}
	}
	audit_log_format(ab, " ocomm=");
	audit_log_untrustedstring(ab, comm);
	audit_log_end(ab);

	return rc;
}

*/
 to_send and len_sent accounting are very loose estimates.  We aren't
 really worried about a hard cap to MAX_EXECVE_AUDIT_LEN so much as being
 within about 500 bytes (next page boundary)

 why snprintf?  an int is up to 12 digits long.  if we just assumed when
 logging that a[%d]= was going to be 16 characters long we would be wasting
 space in every audit message.  In one 7500 byte message we can log up to
 about 1000 min size arguments.  That comes down to about 50% waste of space
 if we didn't do the snprintf to find out how long arg_num_len was.
 /*
static int audit_log_single_execve_arg(struct audit_contextcontext,
					struct audit_buffer*ab,
					int arg_num,
					size_tlen_sent,
					const char __userp,
					charbuf)
{
	char arg_num_len_buf[12];
	const char __usertmp_p = p;
	*/ how many digits are in arg_num? 5 is the length of ' a=""' /*
	size_t arg_num_len = snprintf(arg_num_len_buf, 12, "%d", arg_num) + 5;
	size_t len, len_left, to_send;
	size_t max_execve_audit_len = MAX_EXECVE_AUDIT_LEN;
	unsigned int i, has_cntl = 0, too_long = 0;
	int ret;

	*/ strnlen_user includes the null we don't want to send /*
	len_left = len = strnlen_user(p, MAX_ARG_STRLEN) - 1;

	*/
	 We just created this mm, if we can't find the strings
	 we just copied into it something is _very_ wrong. Similar
	 for strings that are too long, we should not have created
	 any.
	 /*
	if (WARN_ON_ONCE(len < 0 || len > MAX_ARG_STRLEN - 1)) {
		send_sig(SIGKILL, current, 0);
		return -1;
	}

	*/ walk the whole argument looking for non-ascii chars /*
	do {
		if (len_left > MAX_EXECVE_AUDIT_LEN)
			to_send = MAX_EXECVE_AUDIT_LEN;
		else
			to_send = len_left;
		ret = copy_from_user(buf, tmp_p, to_send);
		*/
		 There is no reason for this copy to be short. We just
		 copied them here, and the mm hasn't been exposed to user-
		 space yet.
		 /*
		if (ret) {
			WARN_ON(1);
			send_sig(SIGKILL, current, 0);
			return -1;
		}
		buf[to_send] = '\0';
		has_cntl = audit_string_contains_control(buf, to_send);
		if (has_cntl) {
			*/
			 hex messages get logged as 2 bytes, so we can only
			 send half as much in each message
			 /*
			max_execve_audit_len = MAX_EXECVE_AUDIT_LEN / 2;
			break;
		}
		len_left -= to_send;
		tmp_p += to_send;
	} while (len_left > 0);

	len_left = len;

	if (len > max_execve_audit_len)
		too_long = 1;

	*/ rewalk the argument actually logging the message /*
	for (i = 0; len_left > 0; i++) {
		int room_left;

		if (len_left > max_execve_audit_len)
			to_send = max_execve_audit_len;
		else
			to_send = len_left;

		*/ do we have space left to send this argument in this ab? /*
		room_left = MAX_EXECVE_AUDIT_LEN - arg_num_len -len_sent;
		if (has_cntl)
			room_left -= (to_send 2);
		else
			room_left -= to_send;
		if (room_left < 0) {
			*len_sent = 0;
			audit_log_end(*ab);
			*ab = audit_log_start(context, GFP_KERNEL, AUDIT_EXECVE);
			if (!*ab)
				return 0;
		}

		*/
		 first record needs to say how long the original string was
		 so we can be sure nothing was lost.
		 /*
		if ((i == 0) && (too_long))
			audit_log_format(*ab, " a%d_len=%zu", arg_num,
					 has_cntl ? 2*len : len);

		*/
		 normally arguments are small enough to fit and we already
		 filled buf above when we checked for control characters
		 so don't bother with another copy_from_user
		 /*
		if (len >= max_execve_audit_len)
			ret = copy_from_user(buf, p, to_send);
		else
			ret = 0;
		if (ret) {
			WARN_ON(1);
			send_sig(SIGKILL, current, 0);
			return -1;
		}
		buf[to_send] = '\0';

		*/ actually log it /*
		audit_log_format(*ab, " a%d", arg_num);
		if (too_long)
			audit_log_format(*ab, "[%d]", i);
		audit_log_format(*ab, "=");
		if (has_cntl)
			audit_log_n_hex(*ab, buf, to_send);
		else
			audit_log_string(*ab, buf);

		p += to_send;
		len_left -= to_send;
		*len_sent += arg_num_len;
		if (has_cntl)
			*len_sent += to_send 2;
		else
			*len_sent += to_send;
	}
	*/ include the null we didn't log /*
	return len + 1;
}

static void audit_log_execve_info(struct audit_contextcontext,
				  struct audit_buffer*ab)
{
	int i, len;
	size_t len_sent = 0;
	const char __userp;
	charbuf;

	p = (const char __user)current->mm->arg_start;

	audit_log_format(*ab, "argc=%d", context->execve.argc);

	*/
	 we need some kernel buffer to hold the userspace args.  Just
	 allocate one big one rather than allocating one of the right size
	 for every single argument inside audit_log_single_execve_arg()
	 should be <8k allocation so should be pretty safe.
	 /*
	buf = kmalloc(MAX_EXECVE_AUDIT_LEN + 1, GFP_KERNEL);
	if (!buf) {
		audit_panic("out of memory for argv string");
		return;
	}

	for (i = 0; i < context->execve.argc; i++) {
		len = audit_log_single_execve_arg(context, ab, i,
						  &len_sent, p, buf);
		if (len <= 0)
			break;
		p += len;
	}
	kfree(buf);
}

static void show_special(struct audit_contextcontext, intcall_panic)
{
	struct audit_bufferab;
	int i;

	ab = audit_log_start(context, GFP_KERNEL, context->type);
	if (!ab)
		return;

	switch (context->type) {
	case AUDIT_SOCKETCALL: {
		int nargs = context->socketcall.nargs;
		audit_log_format(ab, "nargs=%d", nargs);
		for (i = 0; i < nargs; i++)
			audit_log_format(ab, " a%d=%lx", i,
				context->socketcall.args[i]);
		break; }
	case AUDIT_IPC: {
		u32 osid = context->ipc.osid;

		audit_log_format(ab, "ouid=%u ogid=%u mode=%#ho",
				 from_kuid(&init_user_ns, context->ipc.uid),
				 from_kgid(&init_user_ns, context->ipc.gid),
				 context->ipc.mode);
		if (osid) {
			charctx = NULL;
			u32 len;
			if (security_secid_to_secctx(osid, &ctx, &len)) {
				audit_log_format(ab, " osid=%u", osid);
				*call_panic = 1;
			} else {
				audit_log_format(ab, " obj=%s", ctx);
				security_release_secctx(ctx, len);
			}
		}
		if (context->ipc.has_perm) {
			audit_log_end(ab);
			ab = audit_log_start(context, GFP_KERNEL,
					     AUDIT_IPC_SET_PERM);
			if (unlikely(!ab))
				return;
			audit_log_format(ab,
				"qbytes=%lx ouid=%u ogid=%u mode=%#ho",
				context->ipc.qbytes,
				context->ipc.perm_uid,
				context->ipc.perm_gid,
				context->ipc.perm_mode);
		}
		break; }
	case AUDIT_MQ_OPEN: {
		audit_log_format(ab,
			"oflag=0x%x mode=%#ho mq_flags=0x%lx mq_maxmsg=%ld "
			"mq_msgsize=%ld mq_curmsgs=%ld",
			context->mq_open.oflag, context->mq_open.mode,
			context->mq_open.attr.mq_flags,
			context->mq_open.attr.mq_maxmsg,
			context->mq_open.attr.mq_msgsize,
			context->mq_open.attr.mq_curmsgs);
		break; }
	case AUDIT_MQ_SENDRECV: {
		audit_log_format(ab,
			"mqdes=%d msg_len=%zd msg_prio=%u "
			"abs_timeout_sec=%ld abs_timeout_nsec=%ld",
			context->mq_sendrecv.mqdes,
			context->mq_sendrecv.msg_len,
			context->mq_sendrecv.msg_prio,
			context->mq_sendrecv.abs_timeout.tv_sec,
			context->mq_sendrecv.abs_timeout.tv_nsec);
		break; }
	case AUDIT_MQ_NOTIFY: {
		audit_log_format(ab, "mqdes=%d sigev_signo=%d",
				context->mq_notify.mqdes,
				context->mq_notify.sigev_signo);
		break; }
	case AUDIT_MQ_GETSETATTR: {
		struct mq_attrattr = &context->mq_getsetattr.mqstat;
		audit_log_format(ab,
			"mqdes=%d mq_flags=0x%lx mq_maxmsg=%ld mq_msgsize=%ld "
			"mq_curmsgs=%ld ",
			context->mq_getsetattr.mqdes,
			attr->mq_flags, attr->mq_maxmsg,
			attr->mq_msgsize, attr->mq_curmsgs);
		break; }
	case AUDIT_CAPSET: {
		audit_log_format(ab, "pid=%d", context->capset.pid);
		audit_log_cap(ab, "cap_pi", &context->capset.cap.inheritable);
		audit_log_cap(ab, "cap_pp", &context->capset.cap.permitted);
		audit_log_cap(ab, "cap_pe", &context->capset.cap.effective);
		break; }
	case AUDIT_MMAP: {
		audit_log_format(ab, "fd=%d flags=0x%x", context->mmap.fd,
				 context->mmap.flags);
		break; }
	case AUDIT_EXECVE: {
		audit_log_execve_info(context, &ab);
		break; }
	}
	audit_log_end(ab);
}

static inline int audit_proctitle_rtrim(charproctitle, int len)
{
	charend = proctitle + len - 1;
	while (end > proctitle && !isprint(*end))
		end--;

	*/ catch the case where proctitle is only 1 non-print character /*
	len = end - proctitle + 1;
	len -= isprint(proctitle[len-1]) == 0;
	return len;
}

static void audit_log_proctitle(struct task_structtsk,
			 struct audit_contextcontext)
{
	int res;
	charbuf;
	charmsg = "(null)";
	int len = strlen(msg);
	struct audit_bufferab;

	ab = audit_log_start(context, GFP_KERNEL, AUDIT_PROCTITLE);
	if (!ab)
		return;	*/ audit_panic or being filtered /*

	audit_log_format(ab, "proctitle=");

	*/ Not  cached /*
	if (!context->proctitle.value) {
		buf = kmalloc(MAX_PROCTITLE_AUDIT_LEN, GFP_KERNEL);
		if (!buf)
			goto out;
		*/ Historically called this from procfs naming /*
		res = get_cmdline(tsk, buf, MAX_PROCTITLE_AUDIT_LEN);
		if (res == 0) {
			kfree(buf);
			goto out;
		}
		res = audit_proctitle_rtrim(buf, res);
		if (res == 0) {
			kfree(buf);
			goto out;
		}
		context->proctitle.value = buf;
		context->proctitle.len = res;
	}
	msg = context->proctitle.value;
	len = context->proctitle.len;
out:
	audit_log_n_untrustedstring(ab, msg, len);
	audit_log_end(ab);
}

static void audit_log_exit(struct audit_contextcontext, struct task_structtsk)
{
	int i, call_panic = 0;
	struct audit_bufferab;
	struct audit_aux_dataaux;
	struct audit_namesn;

	*/ tsk == current /*
	context->personality = tsk->personality;

	ab = audit_log_start(context, GFP_KERNEL, AUDIT_SYSCALL);
	if (!ab)
		return;		*/ audit_panic has been called /*
	audit_log_format(ab, "arch=%x syscall=%d",
			 context->arch, context->major);
	if (context->personality != PER_LINUX)
		audit_log_format(ab, " per=%lx", context->personality);
	if (context->return_valid)
		audit_log_format(ab, " success=%s exit=%ld",
				 (context->return_valid==AUDITSC_SUCCESS)?"yes":"no",
				 context->return_code);

	audit_log_format(ab,
			 " a0=%lx a1=%lx a2=%lx a3=%lx items=%d",
			 context->argv[0],
			 context->argv[1],
			 context->argv[2],
			 context->argv[3],
			 context->name_count);

	audit_log_task_info(ab, tsk);
	audit_log_key(ab, context->filterkey);
	audit_log_end(ab);

	for (aux = context->aux; aux; aux = aux->next) {

		ab = audit_log_start(context, GFP_KERNEL, aux->type);
		if (!ab)
			continue;/ audit_panic has been called /*

		switch (aux->type) {

		case AUDIT_BPRM_FCAPS: {
			struct audit_aux_data_bprm_fcapsaxs = (void)aux;
			audit_log_format(ab, "fver=%x", axs->fcap_ver);
			audit_log_cap(ab, "fp", &axs->fcap.permitted);
			audit_log_cap(ab, "fi", &axs->fcap.inheritable);
			audit_log_format(ab, " fe=%d", axs->fcap.fE);
			audit_log_cap(ab, "old_pp", &axs->old_pcap.permitted);
			audit_log_cap(ab, "old_pi", &axs->old_pcap.inheritable);
			audit_log_cap(ab, "old_pe", &axs->old_pcap.effective);
			audit_log_cap(ab, "new_pp", &axs->new_pcap.permitted);
			audit_log_cap(ab, "new_pi", &axs->new_pcap.inheritable);
			audit_log_cap(ab, "new_pe", &axs->new_pcap.effective);
			break; }

		}
		audit_log_end(ab);
	}

	if (context->type)
		show_special(context, &call_panic);

	if (context->fds[0] >= 0) {
		ab = audit_log_start(context, GFP_KERNEL, AUDIT_FD_PAIR);
		if (ab) {
			audit_log_format(ab, "fd0=%d fd1=%d",
					context->fds[0], context->fds[1]);
			audit_log_end(ab);
		}
	}

	if (context->sockaddr_len) {
		ab = audit_log_start(context, GFP_KERNEL, AUDIT_SOCKADDR);
		if (ab) {
			audit_log_format(ab, "saddr=");
			audit_log_n_hex(ab, (void)context->sockaddr,
					context->sockaddr_len);
			audit_log_end(ab);
		}
	}

	for (aux = context->aux_pids; aux; aux = aux->next) {
		struct audit_aux_data_pidsaxs = (void)aux;

		for (i = 0; i < axs->pid_count; i++)
			if (audit_log_pid_context(context, axs->target_pid[i],
						  axs->target_auid[i],
						  axs->target_uid[i],
						  axs->target_sessionid[i],
						  axs->target_sid[i],
						  axs->target_comm[i]))
				call_panic = 1;
	}

	if (context->target_pid &&
	    audit_log_pid_context(context, context->target_pid,
				  context->target_auid, context->target_uid,
				  context->target_sessionid,
				  context->target_sid, context->target_comm))
			call_panic = 1;

	if (context->pwd.dentry && context->pwd.mnt) {
		ab = audit_log_start(context, GFP_KERNEL, AUDIT_CWD);
		if (ab) {
			audit_log_d_path(ab, " cwd=", &context->pwd);
			audit_log_end(ab);
		}
	}

	i = 0;
	list_for_each_entry(n, &context->names_list, list) {
		if (n->hidden)
			continue;
		audit_log_name(context, n, NULL, i++, &call_panic);
	}

	audit_log_proctitle(tsk, context);

	*/ Send end of event record to help user space know we are finished /*
	ab = audit_log_start(context, GFP_KERNEL, AUDIT_EOE);
	if (ab)
		audit_log_end(ab);
	if (call_panic)
		audit_panic("error converting sid to string");
}

*/
 audit_free - free a per-task audit context
 @tsk: task whose audit context block to free

 Called from copy_process and do_exit
 /*
void __audit_free(struct task_structtsk)
{
	struct audit_contextcontext;

	context = audit_take_context(tsk, 0, 0);
	if (!context)
		return;

	*/ Check for system calls that do not go through the exit
	 function (e.g., exit_group), then free context block.
	 We use GFP_ATOMIC here because we might be doing this
	 in the context of the idle thread /*
	*/ that can happen only if we are called from do_exit() /*
	if (context->in_syscall && context->current_state == AUDIT_RECORD_CONTEXT)
		audit_log_exit(context, tsk);
	if (!list_empty(&context->killed_trees))
		audit_kill_trees(&context->killed_trees);

	audit_free_context(context);
}

*/
 audit_syscall_entry - fill in an audit record at syscall entry
 @major: major syscall type (function)
 @a1: additional syscall register 1
 @a2: additional syscall register 2
 @a3: additional syscall register 3
 @a4: additional syscall register 4

 Fill in audit context at syscall entry.  This only happens if the
 audit context was created when the task was created and the state or
 filters demand the audit context be built.  If the state from the
 per-task filter or from the per-syscall filter is AUDIT_RECORD_CONTEXT,
 then the record will be written at syscall exit time (otherwise, it
 will only be written if another part of the kernel requests that it
 be written).
 /*
void __audit_syscall_entry(int major, unsigned long a1, unsigned long a2,
			   unsigned long a3, unsigned long a4)
{
	struct task_structtsk = current;
	struct audit_contextcontext = tsk->audit_context;
	enum audit_state     state;

	if (!context)
		return;

	BUG_ON(context->in_syscall || context->name_count);

	if (!audit_enabled)
		return;

	context->arch	    = syscall_get_arch();
	context->major      = major;
	context->argv[0]    = a1;
	context->argv[1]    = a2;
	context->argv[2]    = a3;
	context->argv[3]    = a4;

	state = context->state;
	context->dummy = !audit_n_rules;
	if (!context->dummy && state == AUDIT_BUILD_CONTEXT) {
		context->prio = 0;
		state = audit_filter_syscall(tsk, context, &audit_filter_list[AUDIT_FILTER_ENTRY]);
	}
	if (state == AUDIT_DISABLED)
		return;

	context->serial     = 0;
	context->ctime      = CURRENT_TIME;
	context->in_syscall = 1;
	context->current_state  = state;
	context->ppid       = 0;
}

*/
 audit_syscall_exit - deallocate audit context after a system call
 @success: success value of the syscall
 @return_code: return value of the syscall

 Tear down after system call.  If the audit context has been marked as
 auditable (either because of the AUDIT_RECORD_CONTEXT state from
 filtering, or because some other part of the kernel wrote an audit
 message), then write out the syscall information.  In call cases,
 free the names stored from getname().
 /*
void __audit_syscall_exit(int success, long return_code)
{
	struct task_structtsk = current;
	struct audit_contextcontext;

	if (success)
		success = AUDITSC_SUCCESS;
	else
		success = AUDITSC_FAILURE;

	context = audit_take_context(tsk, success, return_code);
	if (!context)
		return;

	if (context->in_syscall && context->current_state == AUDIT_RECORD_CONTEXT)
		audit_log_exit(context, tsk);

	context->in_syscall = 0;
	context->prio = context->state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;

	if (!list_empty(&context->killed_trees))
		audit_kill_trees(&context->killed_trees);

	audit_free_names(context);
	unroll_tree_refs(context, NULL, 0);
	audit_free_aux(context);
	context->aux = NULL;
	context->aux_pids = NULL;
	context->target_pid = 0;
	context->target_sid = 0;
	context->sockaddr_len = 0;
	context->type = 0;
	context->fds[0] = -1;
	if (context->state != AUDIT_RECORD_CONTEXT) {
		kfree(context->filterkey);
		context->filterkey = NULL;
	}
	tsk->audit_context = context;
}

static inline void handle_one(const struct inodeinode)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_contextcontext;
	struct audit_tree_refsp;
	struct audit_chunkchunk;
	int count;
	if (likely(hlist_empty(&inode->i_fsnotify_marks)))
		return;
	context = current->audit_context;
	p = context->trees;
	count = context->tree_count;
	rcu_read_lock();
	chunk = audit_tree_lookup(inode);
	rcu_read_unlock();
	if (!chunk)
		return;
	if (likely(put_tree_ref(context, chunk)))
		return;
	if (unlikely(!grow_tree_refs(context))) {
		pr_warn("out of memory, audit has lost a tree reference\n");
		audit_set_auditable(context);
		audit_put_chunk(chunk);
		unroll_tree_refs(context, p, count);
		return;
	}
	put_tree_ref(context, chunk);
#endif
}

static void handle_path(const struct dentrydentry)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_contextcontext;
	struct audit_tree_refsp;
	const struct dentryd,parent;
	struct audit_chunkdrop;
	unsigned long seq;
	int count;

	context = current->audit_context;
	p = context->trees;
	count = context->tree_count;
retry:
	drop = NULL;
	d = dentry;
	rcu_read_lock();
	seq = read_seqbegin(&rename_lock);
	for(;;) {
		struct inodeinode = d_backing_inode(d);
		if (inode && unlikely(!hlist_empty(&inode->i_fsnotify_marks))) {
			struct audit_chunkchunk;
			chunk = audit_tree_lookup(inode);
			if (chunk) {
				if (unlikely(!put_tree_ref(context, chunk))) {
					drop = chunk;
					break;
				}
			}
		}
		parent = d->d_parent;
		if (parent == d)
			break;
		d = parent;
	}
	if (unlikely(read_seqretry(&rename_lock, seq) || drop)) { / in this order /*
		rcu_read_unlock();
		if (!drop) {
			*/ just a race with rename /*
			unroll_tree_refs(context, p, count);
			goto retry;
		}
		audit_put_chunk(drop);
		if (grow_tree_refs(context)) {
			*/ OK, got more space /*
			unroll_tree_refs(context, p, count);
			goto retry;
		}
		*/ too bad /*
		pr_warn("out of memory, audit has lost a tree reference\n");
		unroll_tree_refs(context, p, count);
		audit_set_auditable(context);
		return;
	}
	rcu_read_unlock();
#endif
}

static struct audit_namesaudit_alloc_name(struct audit_contextcontext,
						unsigned char type)
{
	struct audit_namesaname;

	if (context->name_count < AUDIT_NAMES) {
		aname = &context->preallocated_names[context->name_count];
		memset(aname, 0, sizeof(*aname));
	} else {
		aname = kzalloc(sizeof(*aname), GFP_NOFS);
		if (!aname)
			return NULL;
		aname->should_free = true;
	}

	aname->ino = AUDIT_INO_UNSET;
	aname->type = type;
	list_add_tail(&aname->list, &context->names_list);

	context->name_count++;
	return aname;
}

*/
 audit_reusename - fill out filename with info from existing entry
 @uptr: userland ptr to pathname

 Search the audit_names list for the current audit context. If there is an
 existing entry with a matching "uptr" then return the filename
 associated with that audit_name. If not, return NULL.
 /*
struct filename
__audit_reusename(const __user charuptr)
{
	struct audit_contextcontext = current->audit_context;
	struct audit_namesn;

	list_for_each_entry(n, &context->names_list, list) {
		if (!n->name)
			continue;
		if (n->name->uptr == uptr) {
			n->name->refcnt++;
			return n->name;
		}
	}
	return NULL;
}

*/
 audit_getname - add a name to the list
 @name: name to add

 Add a name to the list of audit names for this context.
 Called from fs/namei.c:getname().
 /*
void __audit_getname(struct filenamename)
{
	struct audit_contextcontext = current->audit_context;
	struct audit_namesn;

	if (!context->in_syscall)
		return;

	n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
	if (!n)
		return;

	n->name = name;
	n->name_len = AUDIT_NAME_FULL;
	name->aname = n;
	name->refcnt++;

	if (!context->pwd.dentry)
		get_fs_pwd(current->fs, &context->pwd);
}

*/
 __audit_inode - store the inode and device from a lookup
 @name: name being audited
 @dentry: dentry being audited
 @flags: attributes for this particular entry
 /*
void __audit_inode(struct filenamename, const struct dentrydentry,
		   unsigned int flags)
{
	struct audit_contextcontext = current->audit_context;
	struct inodeinode = d_backing_inode(dentry);
	struct audit_namesn;
	bool parent = flags & AUDIT_INODE_PARENT;

	if (!context->in_syscall)
		return;

	if (!name)
		goto out_alloc;

	*/
	 If we have a pointer to an audit_names entry already, then we can
	 just use it directly if the type is correct.
	 /*
	n = name->aname;
	if (n) {
		if (parent) {
			if (n->type == AUDIT_TYPE_PARENT ||
			    n->type == AUDIT_TYPE_UNKNOWN)
				goto out;
		} else {
			if (n->type != AUDIT_TYPE_PARENT)
				goto out;
		}
	}

	list_for_each_entry_reverse(n, &context->names_list, list) {
		if (n->ino) {
			*/ valid inode number, use that for the comparison /*
			if (n->ino != inode->i_ino ||
			    n->dev != inode->i_sb->s_dev)
				continue;
		} else if (n->name) {
			*/ inode number has not been set, check the name /*
			if (strcmp(n->name->name, name->name))
				continue;
		} else
			*/ no inode and no name (?!) ... this is odd ... /*
			continue;

		*/ match the correct record type /*
		if (parent) {
			if (n->type == AUDIT_TYPE_PARENT ||
			    n->type == AUDIT_TYPE_UNKNOWN)
				goto out;
		} else {
			if (n->type != AUDIT_TYPE_PARENT)
				goto out;
		}
	}

out_alloc:
	*/ unable to find an entry with both a matching name and type /*
	n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
	if (!n)
		return;
	if (name) {
		n->name = name;
		name->refcnt++;
	}

out:
	if (parent) {
		n->name_len = n->name ? parent_len(n->name->name) : AUDIT_NAME_FULL;
		n->type = AUDIT_TYPE_PARENT;
		if (flags & AUDIT_INODE_HIDDEN)
			n->hidden = true;
	} else {
		n->name_len = AUDIT_NAME_FULL;
		n->type = AUDIT_TYPE_NORMAL;
	}
	handle_path(dentry);
	audit_copy_inode(n, dentry, inode);
}

void __audit_file(const struct filefile)
{
	__audit_inode(NULL, file->f_path.dentry, 0);
}

*/
 __audit_inode_child - collect inode info for created/removed objects
 @parent: inode of dentry parent
 @dentry: dentry being audited
 @type:   AUDIT_TYPE_* value that we're looking for

 For syscalls that create or remove filesystem objects, audit_inode
 can only collect information for the filesystem object's parent.
 This call updates the audit context with the child's information.
 Syscalls that create a new filesystem object must be hooked after
 the object is created.  Syscalls that remove a filesystem object
 must be hooked prior, in order to capture the target inode during
 unsuccessful attempts.
 /*
void __audit_inode_child(struct inodeparent,
			 const struct dentrydentry,
			 const unsigned char type)
{
	struct audit_contextcontext = current->audit_context;
	struct inodeinode = d_backing_inode(dentry);
	const chardname = dentry->d_name.name;
	struct audit_namesn,found_parent = NULL,found_child = NULL;

	if (!context->in_syscall)
		return;

	if (inode)
		handle_one(inode);

	*/ look for a parent entry first /*
	list_for_each_entry(n, &context->names_list, list) {
		if (!n->name ||
		    (n->type != AUDIT_TYPE_PARENT &&
		     n->type != AUDIT_TYPE_UNKNOWN))
			continue;

		if (n->ino == parent->i_ino && n->dev == parent->i_sb->s_dev &&
		    !audit_compare_dname_path(dname,
					      n->name->name, n->name_len)) {
			if (n->type == AUDIT_TYPE_UNKNOWN)
				n->type = AUDIT_TYPE_PARENT;
			found_parent = n;
			break;
		}
	}

	*/ is there a matching child entry? /*
	list_for_each_entry(n, &context->names_list, list) {
		*/ can only match entries that have a name /*
		if (!n->name ||
		    (n->type != type && n->type != AUDIT_TYPE_UNKNOWN))
			continue;

		if (!strcmp(dname, n->name->name) ||
		    !audit_compare_dname_path(dname, n->name->name,
						found_parent ?
						found_parent->name_len :
						AUDIT_NAME_FULL)) {
			if (n->type == AUDIT_TYPE_UNKNOWN)
				n->type = type;
			found_child = n;
			break;
		}
	}

	if (!found_parent) {
		*/ create a new, "anonymous" parent record /*
		n = audit_alloc_name(context, AUDIT_TYPE_PARENT);
		if (!n)
			return;
		audit_copy_inode(n, NULL, parent);
	}

	if (!found_child) {
		found_child = audit_alloc_name(context, type);
		if (!found_child)
			return;

		*/ Re-use the name belonging to the slot for a matching parent
		 directory. All names for this context are relinquished in
		 audit_free_names() /*
		if (found_parent) {
			found_child->name = found_parent->name;
			found_child->name_len = AUDIT_NAME_FULL;
			found_child->name->refcnt++;
		}
	}

	if (inode)
		audit_copy_inode(found_child, dentry, inode);
	else
		found_child->ino = AUDIT_INO_UNSET;
}
EXPORT_SYMBOL_GPL(__audit_inode_child);

*/
 auditsc_get_stamp - get local copies of audit_context values
 @ctx: audit_context for the task
 @t: timespec to store time recorded in the audit_context
 @serial: serial value that is recorded in the audit_context

 Also sets the context as auditable.
 /*
int auditsc_get_stamp(struct audit_contextctx,
		       struct timespect, unsigned intserial)
{
	if (!ctx->in_syscall)
		return 0;
	if (!ctx->serial)
		ctx->serial = audit_serial();
	t->tv_sec  = ctx->ctime.tv_sec;
	t->tv_nsec = ctx->ctime.tv_nsec;
	*serial    = ctx->serial;
	if (!ctx->prio) {
		ctx->prio = 1;
		ctx->current_state = AUDIT_RECORD_CONTEXT;
	}
	return 1;
}

*/ global counter which is incremented every time something logs in /*
static atomic_t session_id = ATOMIC_INIT(0);

static int audit_set_loginuid_perm(kuid_t loginuid)
{
	*/ if we are unset, we don't need privs /*
	if (!audit_loginuid_set(current))
		return 0;
	*/ if AUDIT_FEATURE_LOGINUID_IMMUTABLE means never ever allow a change/*
	if (is_audit_feature_set(AUDIT_FEATURE_LOGINUID_IMMUTABLE))
		return -EPERM;
	*/ it is set, you need permission /*
	if (!capable(CAP_AUDIT_CONTROL))
		return -EPERM;
	*/ reject if this is not an unset and we don't allow that /*
	if (is_audit_feature_set(AUDIT_FEATURE_ONLY_UNSET_LOGINUID) && uid_valid(loginuid))
		return -EPERM;
	return 0;
}

static void audit_log_set_loginuid(kuid_t koldloginuid, kuid_t kloginuid,
				   unsigned int oldsessionid, unsigned int sessionid,
				   int rc)
{
	struct audit_bufferab;
	uid_t uid, oldloginuid, loginuid;

	if (!audit_enabled)
		return;

	uid = from_kuid(&init_user_ns, task_uid(current));
	oldloginuid = from_kuid(&init_user_ns, koldloginuid);
	loginuid = from_kuid(&init_user_ns, kloginuid),

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_LOGIN);
	if (!ab)
		return;
	audit_log_format(ab, "pid=%d uid=%u", task_pid_nr(current), uid);
	audit_log_task_context(ab);
	audit_log_format(ab, " old-auid=%u auid=%u old-ses=%u ses=%u res=%d",
			 oldloginuid, loginuid, oldsessionid, sessionid, !rc);
	audit_log_end(ab);
}

*/
 audit_set_loginuid - set current task's audit_context loginuid
 @loginuid: loginuid value

 Returns 0.

 Called (set) from fs/proc/base.c::proc_loginuid_write().
 /*
int audit_set_loginuid(kuid_t loginuid)
{
	struct task_structtask = current;
	unsigned int oldsessionid, sessionid = (unsigned int)-1;
	kuid_t oldloginuid;
	int rc;

	oldloginuid = audit_get_loginuid(current);
	oldsessionid = audit_get_sessionid(current);

	rc = audit_set_loginuid_perm(loginuid);
	if (rc)
		goto out;

	*/ are we setting or clearing? /*
	if (uid_valid(loginuid))
		sessionid = (unsigned int)atomic_inc_return(&session_id);

	task->sessionid = sessionid;
	task->loginuid = loginuid;
out:
	audit_log_set_loginuid(oldloginuid, loginuid, oldsessionid, sessionid, rc);
	return rc;
}

*/
 __audit_mq_open - record audit data for a POSIX MQ open
 @oflag: open flag
 @mode: mode bits
 @attr: queue attributes

 /*
void __audit_mq_open(int oflag, umode_t mode, struct mq_attrattr)
{
	struct audit_contextcontext = current->audit_context;

	if (attr)
		memcpy(&context->mq_open.attr, attr, sizeof(struct mq_attr));
	else
		memset(&context->mq_open.attr, 0, sizeof(struct mq_attr));

	context->mq_open.oflag = oflag;
	context->mq_open.mode = mode;

	context->type = AUDIT_MQ_OPEN;
}

*/
 __audit_mq_sendrecv - record audit data for a POSIX MQ timed send/receive
 @mqdes: MQ descriptor
 @msg_len: Message length
 @msg_prio: Message priority
 @abs_timeout: Message timeout in absolute time

 /*
void __audit_mq_sendrecv(mqd_t mqdes, size_t msg_len, unsigned int msg_prio,
			const struct timespecabs_timeout)
{
	struct audit_contextcontext = current->audit_context;
	struct timespecp = &context->mq_sendrecv.abs_timeout;

	if (abs_timeout)
		memcpy(p, abs_timeout, sizeof(struct timespec));
	else
		memset(p, 0, sizeof(struct timespec));

	context->mq_sendrecv.mqdes = mqdes;
	context->mq_sendrecv.msg_len = msg_len;
	context->mq_sendrecv.msg_prio = msg_prio;

	context->type = AUDIT_MQ_SENDRECV;
}

*/
 __audit_mq_notify - record audit data for a POSIX MQ notify
 @mqdes: MQ descriptor
 @notification: Notification event

 /*

void __audit_mq_notify(mqd_t mqdes, const struct sigeventnotification)
{
	struct audit_contextcontext = current->audit_context;

	if (notification)
		context->mq_notify.sigev_signo = notification->sigev_signo;
	else
		context->mq_notify.sigev_signo = 0;

	context->mq_notify.mqdes = mqdes;
	context->type = AUDIT_MQ_NOTIFY;
}

*/
 __audit_mq_getsetattr - record audit data for a POSIX MQ get/set attribute
 @mqdes: MQ descriptor
 @mqstat: MQ flags

 /*
void __audit_mq_getsetattr(mqd_t mqdes, struct mq_attrmqstat)
{
	struct audit_contextcontext = current->audit_context;
	context->mq_getsetattr.mqdes = mqdes;
	context->mq_getsetattr.mqstat =mqstat;
	context->type = AUDIT_MQ_GETSETATTR;
}

*/
 audit_ipc_obj - record audit data for ipc object
 @ipcp: ipc permissions

 /*
void __audit_ipc_obj(struct kern_ipc_permipcp)
{
	struct audit_contextcontext = current->audit_context;
	context->ipc.uid = ipcp->uid;
	context->ipc.gid = ipcp->gid;
	context->ipc.mode = ipcp->mode;
	context->ipc.has_perm = 0;
	security_ipc_getsecid(ipcp, &context->ipc.osid);
	context->type = AUDIT_IPC;
}

*/
 audit_ipc_set_perm - record audit data for new ipc permissions
 @qbytes: msgq bytes
 @uid: msgq user id
 @gid: msgq group id
 @mode: msgq mode (permissions)

 Called only after audit_ipc_obj().
 /*
void __audit_ipc_set_perm(unsigned long qbytes, uid_t uid, gid_t gid, umode_t mode)
{
	struct audit_contextcontext = current->audit_context;

	context->ipc.qbytes = qbytes;
	context->ipc.perm_uid = uid;
	context->ipc.perm_gid = gid;
	context->ipc.perm_mode = mode;
	context->ipc.has_perm = 1;
}

void __audit_bprm(struct linux_binprmbprm)
{
	struct audit_contextcontext = current->audit_context;

	context->type = AUDIT_EXECVE;
	context->execve.argc = bprm->argc;
}


*/
 audit_socketcall - record audit data for sys_socketcall
 @nargs: number of args, which should not be more than AUDITSC_ARGS.
 @args: args array

 /*
int __audit_socketcall(int nargs, unsigned longargs)
{
	struct audit_contextcontext = current->audit_context;

	if (nargs <= 0 || nargs > AUDITSC_ARGS || !args)
		return -EINVAL;
	context->type = AUDIT_SOCKETCALL;
	context->socketcall.nargs = nargs;
	memcpy(context->socketcall.args, args, nargs sizeof(unsigned long));
	return 0;
}

*/
 __audit_fd_pair - record audit data for pipe and socketpair
 @fd1: the first file descriptor
 @fd2: the second file descriptor

 /*
void __audit_fd_pair(int fd1, int fd2)
{
	struct audit_contextcontext = current->audit_context;
	context->fds[0] = fd1;
	context->fds[1] = fd2;
}

*/
 audit_sockaddr - record audit data for sys_bind, sys_connect, sys_sendto
 @len: data length in user space
 @a: data address in kernel space

 Returns 0 for success or NULL context or < 0 on error.
 /*
int __audit_sockaddr(int len, voida)
{
	struct audit_contextcontext = current->audit_context;

	if (!context->sockaddr) {
		voidp = kmalloc(sizeof(struct sockaddr_storage), GFP_KERNEL);
		if (!p)
			return -ENOMEM;
		context->sockaddr = p;
	}

	context->sockaddr_len = len;
	memcpy(context->sockaddr, a, len);
	return 0;
}

void __audit_ptrace(struct task_structt)
{
	struct audit_contextcontext = current->audit_context;

	context->target_pid = task_pid_nr(t);
	context->target_auid = audit_get_loginuid(t);
	context->target_uid = task_uid(t);
	context->target_sessionid = audit_get_sessionid(t);
	security_task_getsecid(t, &context->target_sid);
	memcpy(context->target_comm, t->comm, TASK_COMM_LEN);
}

*/
 audit_signal_info - record signal info for shutting down audit subsystem
 @sig: signal value
 @t: task being signaled

 If the audit subsystem is being terminated, record the task (pid)
 and uid that is doing that.
 /*
int __audit_signal_info(int sig, struct task_structt)
{
	struct audit_aux_data_pidsaxp;
	struct task_structtsk = current;
	struct audit_contextctx = tsk->audit_context;
	kuid_t uid = current_uid(), t_uid = task_uid(t);

	if (audit_pid && t->tgid == audit_pid) {
		if (sig == SIGTERM || sig == SIGHUP || sig == SIGUSR1 || sig == SIGUSR2) {
			audit_sig_pid = task_pid_nr(tsk);
			if (uid_valid(tsk->loginuid))
				audit_sig_uid = tsk->loginuid;
			else
				audit_sig_uid = uid;
			security_task_getsecid(tsk, &audit_sig_sid);
		}
		if (!audit_signals || audit_dummy_context())
			return 0;
	}

	*/ optimize the common case by putting first signal recipient directly
	 in audit_context /*
	if (!ctx->target_pid) {
		ctx->target_pid = task_tgid_nr(t);
		ctx->target_auid = audit_get_loginuid(t);
		ctx->target_uid = t_uid;
		ctx->target_sessionid = audit_get_sessionid(t);
		security_task_getsecid(t, &ctx->target_sid);
		memcpy(ctx->target_comm, t->comm, TASK_COMM_LEN);
		return 0;
	}

	axp = (void)ctx->aux_pids;
	if (!axp || axp->pid_count == AUDIT_AUX_PIDS) {
		axp = kzalloc(sizeof(*axp), GFP_ATOMIC);
		if (!axp)
			return -ENOMEM;

		axp->d.type = AUDIT_OBJ_PID;
		axp->d.next = ctx->aux_pids;
		ctx->aux_pids = (void)axp;
	}
	BUG_ON(axp->pid_count >= AUDIT_AUX_PIDS);

	axp->target_pid[axp->pid_count] = task_tgid_nr(t);
	axp->target_auid[axp->pid_count] = audit_get_loginuid(t);
	axp->target_uid[axp->pid_count] = t_uid;
	axp->target_sessionid[axp->pid_count] = audit_get_sessionid(t);
	security_task_getsecid(t, &axp->target_sid[axp->pid_count]);
	memcpy(axp->target_comm[axp->pid_count], t->comm, TASK_COMM_LEN);
	axp->pid_count++;

	return 0;
}

*/
 __audit_log_bprm_fcaps - store information about a loading bprm and relevant fcaps
 @bprm: pointer to the bprm being processed
 @new: the proposed new credentials
 @old: the old credentials

 Simply check if the proc already has the caps given by the file and if not
 store the priv escalation info for later auditing at the end of the syscall

 -Eric
 /*
int __audit_log_bprm_fcaps(struct linux_binprmbprm,
			   const struct crednew, const struct credold)
{
	struct audit_aux_data_bprm_fcapsax;
	struct audit_contextcontext = current->audit_context;
	struct cpu_vfs_cap_data vcaps;

	ax = kmalloc(sizeof(*ax), GFP_KERNEL);
	if (!ax)
		return -ENOMEM;

	ax->d.type = AUDIT_BPRM_FCAPS;
	ax->d.next = context->aux;
	context->aux = (void)ax;

	get_vfs_caps_from_disk(bprm->file->f_path.dentry, &vcaps);

	ax->fcap.permitted = vcaps.permitted;
	ax->fcap.inheritable = vcaps.inheritable;
	ax->fcap.fE = !!(vcaps.magic_etc & VFS_CAP_FLAGS_EFFECTIVE);
	ax->fcap_ver = (vcaps.magic_etc & VFS_CAP_REVISION_MASK) >> VFS_CAP_REVISION_SHIFT;

	ax->old_pcap.permitted   = old->cap_permitted;
	ax->old_pcap.inheritable = old->cap_inheritable;
	ax->old_pcap.effective   = old->cap_effective;

	ax->new_pcap.permitted   = new->cap_permitted;
	ax->new_pcap.inheritable = new->cap_inheritable;
	ax->new_pcap.effective   = new->cap_effective;
	return 0;
}

*/
 __audit_log_capset - store information about the arguments to the capset syscall
 @new: the new credentials
 @old: the old (current) credentials

 Record the arguments userspace sent to sys_capset for later printing by the
 audit system if applicable
 /*
void __audit_log_capset(const struct crednew, const struct credold)
{
	struct audit_contextcontext = current->audit_context;
	context->capset.pid = task_pid_nr(current);
	context->capset.cap.effective   = new->cap_effective;
	context->capset.cap.inheritable = new->cap_effective;
	context->capset.cap.permitted   = new->cap_permitted;
	context->type = AUDIT_CAPSET;
}

void __audit_mmap_fd(int fd, int flags)
{
	struct audit_contextcontext = current->audit_context;
	context->mmap.fd = fd;
	context->mmap.flags = flags;
	context->type = AUDIT_MMAP;
}

static void audit_log_task(struct audit_bufferab)
{
	kuid_t auid, uid;
	kgid_t gid;
	unsigned int sessionid;
	char comm[sizeof(current->comm)];

	auid = audit_get_loginuid(current);
	sessionid = audit_get_sessionid(current);
	current_uid_gid(&uid, &gid);

	audit_log_format(ab, "auid=%u uid=%u gid=%u ses=%u",
			 from_kuid(&init_user_ns, auid),
			 from_kuid(&init_user_ns, uid),
			 from_kgid(&init_user_ns, gid),
			 sessionid);
	audit_log_task_context(ab);
	audit_log_format(ab, " pid=%d comm=", task_pid_nr(current));
	audit_log_untrustedstring(ab, get_task_comm(comm, current));
	audit_log_d_path_exe(ab, current->mm);
}

*/
 audit_core_dumps - record information about processes that end abnormally
 @signr: signal value

 If a process ends with a core dump, something fishy is going on and we
 should record the event for investigation.
 /*
void audit_core_dumps(long signr)
{
	struct audit_bufferab;

	if (!audit_enabled)
		return;

	if (signr == SIGQUIT)	*/ don't care for those /*
		return;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_ANOM_ABEND);
	if (unlikely(!ab))
		return;
	audit_log_task(ab);
	audit_log_format(ab, " sig=%ld", signr);
	audit_log_end(ab);
}

void __audit_seccomp(unsigned long syscall, long signr, int code)
{
	struct audit_bufferab;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_SECCOMP);
	if (unlikely(!ab))
		return;
	audit_log_task(ab);
	audit_log_format(ab, " sig=%ld arch=%x syscall=%ld compat=%d ip=0x%lx code=0x%x",
			 signr, syscall_get_arch(), syscall,
			 in_compat_syscall(), KSTK_EIP(current), code);
	audit_log_end(ab);
}

struct list_headaudit_killed_trees(void)
{
	struct audit_contextctx = current->audit_context;
	if (likely(!ctx || !ctx->in_syscall))
		return NULL;
	return &ctx->killed_trees;
}
*/
/*
#include "audit.h"
#include <linux/fsnotify_backend.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include <linux/kthread.h>
#include <linux/slab.h>

struct audit_tree;
struct audit_chunk;

struct audit_tree {
	atomic_t count;
	int goner;
	struct audit_chunkroot;
	struct list_head chunks;
	struct list_head rules;
	struct list_head list;
	struct list_head same_root;
	struct rcu_head head;
	char pathname[];
};

struct audit_chunk {
	struct list_head hash;
	struct fsnotify_mark mark;
	struct list_head trees;		*/ with root here /*
	int dead;
	int count;
	atomic_long_t refs;
	struct rcu_head head;
	struct node {
		struct list_head list;
		struct audit_treeowner;
		unsigned index;		*/ index; upper bit indicates 'will prune' /*
	} owners[];
};

static LIST_HEAD(tree_list);
static LIST_HEAD(prune_list);
static struct task_structprune_thread;

*/
 One struct chunk is attached to each inode of interest.
 We replace struct chunk on tagging/untagging.
 Rules have pointer to struct audit_tree.
 Rules have struct list_head rlist forming a list of rules over
 the same tree.
 References to struct chunk are collected at audit_inode{,_child}()
 time and used in AUDIT_TREE rule matching.
 These references are dropped at the same time we are calling
 audit_free_names(), etc.

 Cyclic lists galore:
 tree.chunks anchors chunk.owners[].list			hash_lock
 tree.rules anchors rule.rlist				audit_filter_mutex
 chunk.trees anchors tree.same_root				hash_lock
 chunk.hash is a hash with middle bits of watch.inode as
 a hash function.						RCU, hash_lock

 tree is refcounted; one reference for "some rules on rules_list refer to
 it", one for each chunk with pointer to it.

 chunk is refcounted by embedded fsnotify_mark + .refs (non-zero refcount
 of watch contributes 1 to .refs).

 node.index allows to get from node.list to containing chunk.
 MSB of that sucker is stolen to mark taggings that we might have to
 revert - several operations have very unpleasant cleanup logics and
 that makes a difference.  Some.
 /*

static struct fsnotify_groupaudit_tree_group;

static struct audit_treealloc_tree(const chars)
{
	struct audit_treetree;

	tree = kmalloc(sizeof(struct audit_tree) + strlen(s) + 1, GFP_KERNEL);
	if (tree) {
		atomic_set(&tree->count, 1);
		tree->goner = 0;
		INIT_LIST_HEAD(&tree->chunks);
		INIT_LIST_HEAD(&tree->rules);
		INIT_LIST_HEAD(&tree->list);
		INIT_LIST_HEAD(&tree->same_root);
		tree->root = NULL;
		strcpy(tree->pathname, s);
	}
	return tree;
}

static inline void get_tree(struct audit_treetree)
{
	atomic_inc(&tree->count);
}

static inline void put_tree(struct audit_treetree)
{
	if (atomic_dec_and_test(&tree->count))
		kfree_rcu(tree, head);
}

*/ to avoid bringing the entire thing in audit.h /*
const charaudit_tree_path(struct audit_treetree)
{
	return tree->pathname;
}

static void free_chunk(struct audit_chunkchunk)
{
	int i;

	for (i = 0; i < chunk->count; i++) {
		if (chunk->owners[i].owner)
			put_tree(chunk->owners[i].owner);
	}
	kfree(chunk);
}

void audit_put_chunk(struct audit_chunkchunk)
{
	if (atomic_long_dec_and_test(&chunk->refs))
		free_chunk(chunk);
}

static void __put_chunk(struct rcu_headrcu)
{
	struct audit_chunkchunk = container_of(rcu, struct audit_chunk, head);
	audit_put_chunk(chunk);
}

static void audit_tree_destroy_watch(struct fsnotify_markentry)
{
	struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);
	call_rcu(&chunk->head, __put_chunk);
}

static struct audit_chunkalloc_chunk(int count)
{
	struct audit_chunkchunk;
	size_t size;
	int i;

	size = offsetof(struct audit_chunk, owners) + count sizeof(struct node);
	chunk = kzalloc(size, GFP_KERNEL);
	if (!chunk)
		return NULL;

	INIT_LIST_HEAD(&chunk->hash);
	INIT_LIST_HEAD(&chunk->trees);
	chunk->count = count;
	atomic_long_set(&chunk->refs, 1);
	for (i = 0; i < count; i++) {
		INIT_LIST_HEAD(&chunk->owners[i].list);
		chunk->owners[i].index = i;
	}
	fsnotify_init_mark(&chunk->mark, audit_tree_destroy_watch);
	chunk->mark.mask = FS_IN_IGNORED;
	return chunk;
}

enum {HASH_SIZE = 128};
static struct list_head chunk_hash_heads[HASH_SIZE];
static __cacheline_aligned_in_smp DEFINE_SPINLOCK(hash_lock);

static inline struct list_headchunk_hash(const struct inodeinode)
{
	unsigned long n = (unsigned long)inode / L1_CACHE_BYTES;
	return chunk_hash_heads + n % HASH_SIZE;
}

*/ hash_lock & entry->lock is held by caller /*
static void insert_hash(struct audit_chunkchunk)
{
	struct fsnotify_markentry = &chunk->mark;
	struct list_headlist;

	if (!entry->inode)
		return;
	list = chunk_hash(entry->inode);
	list_add_rcu(&chunk->hash, list);
}

*/ called under rcu_read_lock /*
struct audit_chunkaudit_tree_lookup(const struct inodeinode)
{
	struct list_headlist = chunk_hash(inode);
	struct audit_chunkp;

	list_for_each_entry_rcu(p, list, hash) {
		*/ mark.inode may have gone NULL, but who cares? /*
		if (p->mark.inode == inode) {
			atomic_long_inc(&p->refs);
			return p;
		}
	}
	return NULL;
}

bool audit_tree_match(struct audit_chunkchunk, struct audit_treetree)
{
	int n;
	for (n = 0; n < chunk->count; n++)
		if (chunk->owners[n].owner == tree)
			return true;
	return false;
}

*/ tagging and untagging inodes with trees /*

static struct audit_chunkfind_chunk(struct nodep)
{
	int index = p->index & ~(1U<<31);
	p -= index;
	return container_of(p, struct audit_chunk, owners[0]);
}

static void untag_chunk(struct nodep)
{
	struct audit_chunkchunk = find_chunk(p);
	struct fsnotify_markentry = &chunk->mark;
	struct audit_chunknew = NULL;
	struct audit_treeowner;
	int size = chunk->count - 1;
	int i, j;

	fsnotify_get_mark(entry);

	spin_unlock(&hash_lock);

	if (size)
		new = alloc_chunk(size);

	spin_lock(&entry->lock);
	if (chunk->dead || !entry->inode) {
		spin_unlock(&entry->lock);
		if (new)
			free_chunk(new);
		goto out;
	}

	owner = p->owner;

	if (!size) {
		chunk->dead = 1;
		spin_lock(&hash_lock);
		list_del_init(&chunk->trees);
		if (owner->root == chunk)
			owner->root = NULL;
		list_del_init(&p->list);
		list_del_rcu(&chunk->hash);
		spin_unlock(&hash_lock);
		spin_unlock(&entry->lock);
		fsnotify_destroy_mark(entry, audit_tree_group);
		goto out;
	}

	if (!new)
		goto Fallback;

	fsnotify_duplicate_mark(&new->mark, entry);
	if (fsnotify_add_mark(&new->mark, new->mark.group, new->mark.inode, NULL, 1)) {
		fsnotify_put_mark(&new->mark);
		goto Fallback;
	}

	chunk->dead = 1;
	spin_lock(&hash_lock);
	list_replace_init(&chunk->trees, &new->trees);
	if (owner->root == chunk) {
		list_del_init(&owner->same_root);
		owner->root = NULL;
	}

	for (i = j = 0; j <= size; i++, j++) {
		struct audit_trees;
		if (&chunk->owners[j] == p) {
			list_del_init(&p->list);
			i--;
			continue;
		}
		s = chunk->owners[j].owner;
		new->owners[i].owner = s;
		new->owners[i].index = chunk->owners[j].index - j + i;
		if (!s)/ result of earlier fallback /*
			continue;
		get_tree(s);
		list_replace_init(&chunk->owners[j].list, &new->owners[i].list);
	}

	list_replace_rcu(&chunk->hash, &new->hash);
	list_for_each_entry(owner, &new->trees, same_root)
		owner->root = new;
	spin_unlock(&hash_lock);
	spin_unlock(&entry->lock);
	fsnotify_destroy_mark(entry, audit_tree_group);
	fsnotify_put_mark(&new->mark);	*/ drop initial reference /*
	goto out;

Fallback:
	// do the best we can
	spin_lock(&hash_lock);
	if (owner->root == chunk) {
		list_del_init(&owner->same_root);
		owner->root = NULL;
	}
	list_del_init(&p->list);
	p->owner = NULL;
	put_tree(owner);
	spin_unlock(&hash_lock);
	spin_unlock(&entry->lock);
out:
	fsnotify_put_mark(entry);
	spin_lock(&hash_lock);
}

static int create_chunk(struct inodeinode, struct audit_treetree)
{
	struct fsnotify_markentry;
	struct audit_chunkchunk = alloc_chunk(1);
	if (!chunk)
		return -ENOMEM;

	entry = &chunk->mark;
	if (fsnotify_add_mark(entry, audit_tree_group, inode, NULL, 0)) {
		fsnotify_put_mark(entry);
		return -ENOSPC;
	}

	spin_lock(&entry->lock);
	spin_lock(&hash_lock);
	if (tree->goner) {
		spin_unlock(&hash_lock);
		chunk->dead = 1;
		spin_unlock(&entry->lock);
		fsnotify_destroy_mark(entry, audit_tree_group);
		fsnotify_put_mark(entry);
		return 0;
	}
	chunk->owners[0].index = (1U << 31);
	chunk->owners[0].owner = tree;
	get_tree(tree);
	list_add(&chunk->owners[0].list, &tree->chunks);
	if (!tree->root) {
		tree->root = chunk;
		list_add(&tree->same_root, &chunk->trees);
	}
	insert_hash(chunk);
	spin_unlock(&hash_lock);
	spin_unlock(&entry->lock);
	fsnotify_put_mark(entry);	*/ drop initial reference /*
	return 0;
}

*/ the first tagged inode becomes root of tree /*
static int tag_chunk(struct inodeinode, struct audit_treetree)
{
	struct fsnotify_markold_entry,chunk_entry;
	struct audit_treeowner;
	struct audit_chunkchunk,old;
	struct nodep;
	int n;

	old_entry = fsnotify_find_inode_mark(audit_tree_group, inode);
	if (!old_entry)
		return create_chunk(inode, tree);

	old = container_of(old_entry, struct audit_chunk, mark);

	*/ are we already there? /*
	spin_lock(&hash_lock);
	for (n = 0; n < old->count; n++) {
		if (old->owners[n].owner == tree) {
			spin_unlock(&hash_lock);
			fsnotify_put_mark(old_entry);
			return 0;
		}
	}
	spin_unlock(&hash_lock);

	chunk = alloc_chunk(old->count + 1);
	if (!chunk) {
		fsnotify_put_mark(old_entry);
		return -ENOMEM;
	}

	chunk_entry = &chunk->mark;

	spin_lock(&old_entry->lock);
	if (!old_entry->inode) {
		*/ old_entry is being shot, lets just lie /*
		spin_unlock(&old_entry->lock);
		fsnotify_put_mark(old_entry);
		free_chunk(chunk);
		return -ENOENT;
	}

	fsnotify_duplicate_mark(chunk_entry, old_entry);
	if (fsnotify_add_mark(chunk_entry, chunk_entry->group, chunk_entry->inode, NULL, 1)) {
		spin_unlock(&old_entry->lock);
		fsnotify_put_mark(chunk_entry);
		fsnotify_put_mark(old_entry);
		return -ENOSPC;
	}

	*/ even though we hold old_entry->lock, this is safe since chunk_entry->lock could NEVER have been grabbed before /*
	spin_lock(&chunk_entry->lock);
	spin_lock(&hash_lock);

	*/ we now hold old_entry->lock, chunk_entry->lock, and hash_lock /*
	if (tree->goner) {
		spin_unlock(&hash_lock);
		chunk->dead = 1;
		spin_unlock(&chunk_entry->lock);
		spin_unlock(&old_entry->lock);

		fsnotify_destroy_mark(chunk_entry, audit_tree_group);

		fsnotify_put_mark(chunk_entry);
		fsnotify_put_mark(old_entry);
		return 0;
	}
	list_replace_init(&old->trees, &chunk->trees);
	for (n = 0, p = chunk->owners; n < old->count; n++, p++) {
		struct audit_trees = old->owners[n].owner;
		p->owner = s;
		p->index = old->owners[n].index;
		if (!s)/ result of fallback in untag /*
			continue;
		get_tree(s);
		list_replace_init(&old->owners[n].list, &p->list);
	}
	p->index = (chunk->count - 1) | (1U<<31);
	p->owner = tree;
	get_tree(tree);
	list_add(&p->list, &tree->chunks);
	list_replace_rcu(&old->hash, &chunk->hash);
	list_for_each_entry(owner, &chunk->trees, same_root)
		owner->root = chunk;
	old->dead = 1;
	if (!tree->root) {
		tree->root = chunk;
		list_add(&tree->same_root, &chunk->trees);
	}
	spin_unlock(&hash_lock);
	spin_unlock(&chunk_entry->lock);
	spin_unlock(&old_entry->lock);
	fsnotify_destroy_mark(old_entry, audit_tree_group);
	fsnotify_put_mark(chunk_entry);	*/ drop initial reference /*
	fsnotify_put_mark(old_entry);/ pair to fsnotify_find mark_entry /*
	return 0;
}

static void audit_tree_log_remove_rule(struct audit_krulerule)
{
	struct audit_bufferab;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return;
	audit_log_format(ab, "op=");
	audit_log_string(ab, "remove_rule");
	audit_log_format(ab, " dir=");
	audit_log_untrustedstring(ab, rule->tree->pathname);
	audit_log_key(ab, rule->filterkey);
	audit_log_format(ab, " list=%d res=1", rule->listnr);
	audit_log_end(ab);
}

static void kill_rules(struct audit_treetree)
{
	struct audit_krulerule,next;
	struct audit_entryentry;

	list_for_each_entry_safe(rule, next, &tree->rules, rlist) {
		entry = container_of(rule, struct audit_entry, rule);

		list_del_init(&rule->rlist);
		if (rule->tree) {
			*/ not a half-baked one /*
			audit_tree_log_remove_rule(rule);
			if (entry->rule.exe)
				audit_remove_mark(entry->rule.exe);
			rule->tree = NULL;
			list_del_rcu(&entry->list);
			list_del(&entry->rule.list);
			call_rcu(&entry->rcu, audit_free_rule_rcu);
		}
	}
}

*/
 finish killing struct audit_tree
 /*
static void prune_one(struct audit_treevictim)
{
	spin_lock(&hash_lock);
	while (!list_empty(&victim->chunks)) {
		struct nodep;

		p = list_entry(victim->chunks.next, struct node, list);

		untag_chunk(p);
	}
	spin_unlock(&hash_lock);
	put_tree(victim);
}

*/ trim the uncommitted chunks from tree /*

static void trim_marked(struct audit_treetree)
{
	struct list_headp,q;
	spin_lock(&hash_lock);
	if (tree->goner) {
		spin_unlock(&hash_lock);
		return;
	}
	*/ reorder /*
	for (p = tree->chunks.next; p != &tree->chunks; p = q) {
		struct nodenode = list_entry(p, struct node, list);
		q = p->next;
		if (node->index & (1U<<31)) {
			list_del_init(p);
			list_add(p, &tree->chunks);
		}
	}

	while (!list_empty(&tree->chunks)) {
		struct nodenode;

		node = list_entry(tree->chunks.next, struct node, list);

		*/ have we run out of marked? /*
		if (!(node->index & (1U<<31)))
			break;

		untag_chunk(node);
	}
	if (!tree->root && !tree->goner) {
		tree->goner = 1;
		spin_unlock(&hash_lock);
		mutex_lock(&audit_filter_mutex);
		kill_rules(tree);
		list_del_init(&tree->list);
		mutex_unlock(&audit_filter_mutex);
		prune_one(tree);
	} else {
		spin_unlock(&hash_lock);
	}
}

static void audit_schedule_prune(void);

*/ called with audit_filter_mutex /*
int audit_remove_tree_rule(struct audit_krulerule)
{
	struct audit_treetree;
	tree = rule->tree;
	if (tree) {
		spin_lock(&hash_lock);
		list_del_init(&rule->rlist);
		if (list_empty(&tree->rules) && !tree->goner) {
			tree->root = NULL;
			list_del_init(&tree->same_root);
			tree->goner = 1;
			list_move(&tree->list, &prune_list);
			rule->tree = NULL;
			spin_unlock(&hash_lock);
			audit_schedule_prune();
			return 1;
		}
		rule->tree = NULL;
		spin_unlock(&hash_lock);
		return 1;
	}
	return 0;
}

static int compare_root(struct vfsmountmnt, voidarg)
{
	return d_backing_inode(mnt->mnt_root) == arg;
}

void audit_trim_trees(void)
{
	struct list_head cursor;

	mutex_lock(&audit_filter_mutex);
	list_add(&cursor, &tree_list);
	while (cursor.next != &tree_list) {
		struct audit_treetree;
		struct path path;
		struct vfsmountroot_mnt;
		struct nodenode;
		int err;

		tree = container_of(cursor.next, struct audit_tree, list);
		get_tree(tree);
		list_del(&cursor);
		list_add(&cursor, &tree->list);
		mutex_unlock(&audit_filter_mutex);

		err = kern_path(tree->pathname, 0, &path);
		if (err)
			goto skip_it;

		root_mnt = collect_mounts(&path);
		path_put(&path);
		if (IS_ERR(root_mnt))
			goto skip_it;

		spin_lock(&hash_lock);
		list_for_each_entry(node, &tree->chunks, list) {
			struct audit_chunkchunk = find_chunk(node);
			*/ this could be NULL if the watch is dying else where... /*
			struct inodeinode = chunk->mark.inode;
			node->index |= 1U<<31;
			if (iterate_mounts(compare_root, inode, root_mnt))
				node->index &= ~(1U<<31);
		}
		spin_unlock(&hash_lock);
		trim_marked(tree);
		drop_collected_mounts(root_mnt);
skip_it:
		put_tree(tree);
		mutex_lock(&audit_filter_mutex);
	}
	list_del(&cursor);
	mutex_unlock(&audit_filter_mutex);
}

int audit_make_tree(struct audit_krulerule, charpathname, u32 op)
{

	if (pathname[0] != '/' ||
	    rule->listnr != AUDIT_FILTER_EXIT ||
	    op != Audit_equal ||
	    rule->inode_f || rule->watch || rule->tree)
		return -EINVAL;
	rule->tree = alloc_tree(pathname);
	if (!rule->tree)
		return -ENOMEM;
	return 0;
}

void audit_put_tree(struct audit_treetree)
{
	put_tree(tree);
}

static int tag_mount(struct vfsmountmnt, voidarg)
{
	return tag_chunk(d_backing_inode(mnt->mnt_root), arg);
}

*/
 That gets run when evict_chunk() ends up needing to kill audit_tree.
 Runs from a separate thread.
 /*
static int prune_tree_thread(voidunused)
{
	for (;;) {
		set_current_state(TASK_INTERRUPTIBLE);
		if (list_empty(&prune_list))
			schedule();
		__set_current_state(TASK_RUNNING);

		mutex_lock(&audit_cmd_mutex);
		mutex_lock(&audit_filter_mutex);

		while (!list_empty(&prune_list)) {
			struct audit_treevictim;

			victim = list_entry(prune_list.next,
					struct audit_tree, list);
			list_del_init(&victim->list);

			mutex_unlock(&audit_filter_mutex);

			prune_one(victim);

			mutex_lock(&audit_filter_mutex);
		}

		mutex_unlock(&audit_filter_mutex);
		mutex_unlock(&audit_cmd_mutex);
	}
	return 0;
}

static int audit_launch_prune(void)
{
	if (prune_thread)
		return 0;
	prune_thread = kthread_create(prune_tree_thread, NULL,
				"audit_prune_tree");
	if (IS_ERR(prune_thread)) {
		pr_err("cannot start thread audit_prune_tree");
		prune_thread = NULL;
		return -ENOMEM;
	} else {
		wake_up_process(prune_thread);
		return 0;
	}
}

*/ called with audit_filter_mutex /*
int audit_add_tree_rule(struct audit_krulerule)
{
	struct audit_treeseed = rule->tree,tree;
	struct path path;
	struct vfsmountmnt;
	int err;

	rule->tree = NULL;
	list_for_each_entry(tree, &tree_list, list) {
		if (!strcmp(seed->pathname, tree->pathname)) {
			put_tree(seed);
			rule->tree = tree;
			list_add(&rule->rlist, &tree->rules);
			return 0;
		}
	}
	tree = seed;
	list_add(&tree->list, &tree_list);
	list_add(&rule->rlist, &tree->rules);
	*/ do not set rule->tree yet /*
	mutex_unlock(&audit_filter_mutex);

	if (unlikely(!prune_thread)) {
		err = audit_launch_prune();
		if (err)
			goto Err;
	}

	err = kern_path(tree->pathname, 0, &path);
	if (err)
		goto Err;
	mnt = collect_mounts(&path);
	path_put(&path);
	if (IS_ERR(mnt)) {
		err = PTR_ERR(mnt);
		goto Err;
	}

	get_tree(tree);
	err = iterate_mounts(tag_mount, tree, mnt);
	drop_collected_mounts(mnt);

	if (!err) {
		struct nodenode;
		spin_lock(&hash_lock);
		list_for_each_entry(node, &tree->chunks, list)
			node->index &= ~(1U<<31);
		spin_unlock(&hash_lock);
	} else {
		trim_marked(tree);
		goto Err;
	}

	mutex_lock(&audit_filter_mutex);
	if (list_empty(&rule->rlist)) {
		put_tree(tree);
		return -ENOENT;
	}
	rule->tree = tree;
	put_tree(tree);

	return 0;
Err:
	mutex_lock(&audit_filter_mutex);
	list_del_init(&tree->list);
	list_del_init(&tree->rules);
	put_tree(tree);
	return err;
}

int audit_tag_tree(charold, charnew)
{
	struct list_head cursor, barrier;
	int failed = 0;
	struct path path1, path2;
	struct vfsmounttagged;
	int err;

	err = kern_path(new, 0, &path2);
	if (err)
		return err;
	tagged = collect_mounts(&path2);
	path_put(&path2);
	if (IS_ERR(tagged))
		return PTR_ERR(tagged);

	err = kern_path(old, 0, &path1);
	if (err) {
		drop_collected_mounts(tagged);
		return err;
	}

	mutex_lock(&audit_filter_mutex);
	list_add(&barrier, &tree_list);
	list_add(&cursor, &barrier);

	while (cursor.next != &tree_list) {
		struct audit_treetree;
		int good_one = 0;

		tree = container_of(cursor.next, struct audit_tree, list);
		get_tree(tree);
		list_del(&cursor);
		list_add(&cursor, &tree->list);
		mutex_unlock(&audit_filter_mutex);

		err = kern_path(tree->pathname, 0, &path2);
		if (!err) {
			good_one = path_is_under(&path1, &path2);
			path_put(&path2);
		}

		if (!good_one) {
			put_tree(tree);
			mutex_lock(&audit_filter_mutex);
			continue;
		}

		failed = iterate_mounts(tag_mount, tree, tagged);
		if (failed) {
			put_tree(tree);
			mutex_lock(&audit_filter_mutex);
			break;
		}

		mutex_lock(&audit_filter_mutex);
		spin_lock(&hash_lock);
		if (!tree->goner) {
			list_del(&tree->list);
			list_add(&tree->list, &tree_list);
		}
		spin_unlock(&hash_lock);
		put_tree(tree);
	}

	while (barrier.prev != &tree_list) {
		struct audit_treetree;

		tree = container_of(barrier.prev, struct audit_tree, list);
		get_tree(tree);
		list_del(&tree->list);
		list_add(&tree->list, &barrier);
		mutex_unlock(&audit_filter_mutex);

		if (!failed) {
			struct nodenode;
			spin_lock(&hash_lock);
			list_for_each_entry(node, &tree->chunks, list)
				node->index &= ~(1U<<31);
			spin_unlock(&hash_lock);
		} else {
			trim_marked(tree);
		}

		put_tree(tree);
		mutex_lock(&audit_filter_mutex);
	}
	list_del(&barrier);
	list_del(&cursor);
	mutex_unlock(&audit_filter_mutex);
	path_put(&path1);
	drop_collected_mounts(tagged);
	return failed;
}


static void audit_schedule_prune(void)
{
	wake_up_process(prune_thread);
}

*/
 ... and that one is done if evict_chunk() decides to delay until the end
 of syscall.  Runs synchronously.
 /*
void audit_kill_trees(struct list_headlist)
{
	mutex_lock(&audit_cmd_mutex);
	mutex_lock(&audit_filter_mutex);

	while (!list_empty(list)) {
		struct audit_treevictim;

		victim = list_entry(list->next, struct audit_tree, list);
		kill_rules(victim);
		list_del_init(&victim->list);

		mutex_unlock(&audit_filter_mutex);

		prune_one(victim);

		mutex_lock(&audit_filter_mutex);
	}

	mutex_unlock(&audit_filter_mutex);
	mutex_unlock(&audit_cmd_mutex);
}

*/
  Here comes the stuff asynchronous to auditctl operations
 /*

static void evict_chunk(struct audit_chunkchunk)
{
	struct audit_treeowner;
	struct list_headpostponed = audit_killed_trees();
	int need_prune = 0;
	int n;

	if (chunk->dead)
		return;

	chunk->dead = 1;
	mutex_lock(&audit_filter_mutex);
	spin_lock(&hash_lock);
	while (!list_empty(&chunk->trees)) {
		owner = list_entry(chunk->trees.next,
				   struct audit_tree, same_root);
		owner->goner = 1;
		owner->root = NULL;
		list_del_init(&owner->same_root);
		spin_unlock(&hash_lock);
		if (!postponed) {
			kill_rules(owner);
			list_move(&owner->list, &prune_list);
			need_prune = 1;
		} else {
			list_move(&owner->list, postponed);
		}
		spin_lock(&hash_lock);
	}
	list_del_rcu(&chunk->hash);
	for (n = 0; n < chunk->count; n++)
		list_del_init(&chunk->owners[n].list);
	spin_unlock(&hash_lock);
	mutex_unlock(&audit_filter_mutex);
	if (need_prune)
		audit_schedule_prune();
}

static int audit_tree_handle_event(struct fsnotify_groupgroup,
				   struct inodeto_tell,
				   struct fsnotify_markinode_mark,
				   struct fsnotify_markvfsmount_mark,
				   u32 mask, voiddata, int data_type,
				   const unsigned charfile_name, u32 cookie)
{
	return 0;
}

static void audit_tree_freeing_mark(struct fsnotify_markentry, struct fsnotify_groupgroup)
{
	struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);

	evict_chunk(chunk);

	*/
	 We are guaranteed to have at least one reference to the mark from
	 either the inode or the caller of fsnotify_destroy_mark().
	 /*
	BUG_ON(atomic_read(&entry->refcnt) < 1);
}

static const struct fsnotify_ops audit_tree_ops = {
	.handle_event = audit_tree_handle_event,
	.freeing_mark = audit_tree_freeing_mark,
};

static int __init audit_tree_init(void)
{
	int i;

	audit_tree_group = fsnotify_alloc_group(&audit_tree_ops);
	if (IS_ERR(audit_tree_group))
		audit_panic("cannot initialize fsnotify group for rectree watches");

	for (i = 0; i < HASH_SIZE; i++)
		INIT_LIST_HEAD(&chunk_hash_heads[i]);

	return 0;
}
__initcall(audit_tree_init);
*/
 audit_watch.c -- watching inodes

 Copyright 2003-2009 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include <linux/kernel.h>
#include <linux/audit.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/fs.h>
#include <linux/fsnotify_backend.h>
#include <linux/namei.h>
#include <linux/netlink.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/security.h>
#include "audit.h"

*/
 Reference counting:

 audit_parent: lifetime is from audit_init_parent() to receipt of an FS_IGNORED
 	event.  Each audit_watch holds a reference to its associated parent.

 audit_watch: if added to lists, lifetime is from audit_init_watch() to
 	audit_remove_watch().  Additionally, an audit_watch may exist
 	temporarily to assist in searching existing filter data.  Each
 	audit_krule holds a reference to its associated watch.
 /*

struct audit_watch {
	atomic_t		count;	*/ reference count /*
	dev_t			dev;	*/ associated superblock device /*
	char			*path;	*/ insertion path /*
	unsigned long		ino;	*/ associated inode number /*
	struct audit_parent	*parent;/ associated parent /*
	struct list_head	wlist;	*/ entry in parent->watches list /*
	struct list_head	rules;	*/ anchor for krule->rlist /*
};

struct audit_parent {
	struct list_head	watches;/ anchor for audit_watch->wlist /*
	struct fsnotify_mark mark;/ fsnotify mark on the inode /*
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_watch_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_WATCH (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
			FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_free_parent(struct audit_parentparent)
{
	WARN_ON(!list_empty(&parent->watches));
	kfree(parent);
}

static void audit_watch_free_mark(struct fsnotify_markentry)
{
	struct audit_parentparent;

	parent = container_of(entry, struct audit_parent, mark);
	audit_free_parent(parent);
}

static void audit_get_parent(struct audit_parentparent)
{
	if (likely(parent))
		fsnotify_get_mark(&parent->mark);
}

static void audit_put_parent(struct audit_parentparent)
{
	if (likely(parent))
		fsnotify_put_mark(&parent->mark);
}

*/
 Find and return the audit_parent on the given inode.  If found a reference
 is taken on this parent.
 /*
static inline struct audit_parentaudit_find_parent(struct inodeinode)
{
	struct audit_parentparent = NULL;
	struct fsnotify_markentry;

	entry = fsnotify_find_inode_mark(audit_watch_group, inode);
	if (entry)
		parent = container_of(entry, struct audit_parent, mark);

	return parent;
}

void audit_get_watch(struct audit_watchwatch)
{
	atomic_inc(&watch->count);
}

void audit_put_watch(struct audit_watchwatch)
{
	if (atomic_dec_and_test(&watch->count)) {
		WARN_ON(watch->parent);
		WARN_ON(!list_empty(&watch->rules));
		kfree(watch->path);
		kfree(watch);
	}
}

static void audit_remove_watch(struct audit_watchwatch)
{
	list_del(&watch->wlist);
	audit_put_parent(watch->parent);
	watch->parent = NULL;
	audit_put_watch(watch);/ match initial get /*
}

charaudit_watch_path(struct audit_watchwatch)
{
	return watch->path;
}

int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev)
{
	return (watch->ino != AUDIT_INO_UNSET) &&
		(watch->ino == ino) &&
		(watch->dev == dev);
}

*/ Initialize a parent watch entry. /*
static struct audit_parentaudit_init_parent(struct pathpath)
{
	struct inodeinode = d_backing_inode(path->dentry);
	struct audit_parentparent;
	int ret;

	parent = kzalloc(sizeof(*parent), GFP_KERNEL);
	if (unlikely(!parent))
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&parent->watches);

	fsnotify_init_mark(&parent->mark, audit_watch_free_mark);
	parent->mark.mask = AUDIT_FS_WATCH;
	ret = fsnotify_add_mark(&parent->mark, audit_watch_group, inode, NULL, 0);
	if (ret < 0) {
		audit_free_parent(parent);
		return ERR_PTR(ret);
	}

	return parent;
}

*/ Initialize a watch entry. /*
static struct audit_watchaudit_init_watch(charpath)
{
	struct audit_watchwatch;

	watch = kzalloc(sizeof(*watch), GFP_KERNEL);
	if (unlikely(!watch))
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&watch->rules);
	atomic_set(&watch->count, 1);
	watch->path = path;
	watch->dev = AUDIT_DEV_UNSET;
	watch->ino = AUDIT_INO_UNSET;

	return watch;
}

*/ Translate a watch string to kernel representation. /*
int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op)
{
	struct audit_watchwatch;

	if (!audit_watch_group)
		return -EOPNOTSUPP;

	if (path[0] != '/' || path[len-1] == '/' ||
	    krule->listnr != AUDIT_FILTER_EXIT ||
	    op != Audit_equal ||
	    krule->inode_f || krule->watch || krule->tree)
		return -EINVAL;

	watch = audit_init_watch(path);
	if (IS_ERR(watch))
		return PTR_ERR(watch);

	krule->watch = watch;

	return 0;
}

*/ Duplicate the given audit watch.  The new watch's rules list is initialized
 to an empty list and wlist is undefined. /*
static struct audit_watchaudit_dupe_watch(struct audit_watchold)
{
	charpath;
	struct audit_watchnew;

	path = kstrdup(old->path, GFP_KERNEL);
	if (unlikely(!path))
		return ERR_PTR(-ENOMEM);

	new = audit_init_watch(path);
	if (IS_ERR(new)) {
		kfree(path);
		goto out;
	}

	new->dev = old->dev;
	new->ino = old->ino;
	audit_get_parent(old->parent);
	new->parent = old->parent;

out:
	return new;
}

static void audit_watch_log_rule_change(struct audit_kruler, struct audit_watchw, charop)
{
	if (audit_enabled) {
		struct audit_bufferab;
		ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
		if (unlikely(!ab))
			return;
		audit_log_format(ab, "auid=%u ses=%u op=",
				 from_kuid(&init_user_ns, audit_get_loginuid(current)),
				 audit_get_sessionid(current));
		audit_log_string(ab, op);
		audit_log_format(ab, " path=");
		audit_log_untrustedstring(ab, w->path);
		audit_log_key(ab, r->filterkey);
		audit_log_format(ab, " list=%d res=1", r->listnr);
		audit_log_end(ab);
	}
}

*/ Update inode info in audit rules based on filesystem event. /*
static void audit_update_watch(struct audit_parentparent,
			       const chardname, dev_t dev,
			       unsigned long ino, unsigned invalidating)
{
	struct audit_watchowatch,nwatch,nextw;
	struct audit_kruler,nextr;
	struct audit_entryoentry,nentry;

	mutex_lock(&audit_filter_mutex);
	*/ Run all of the watches on this parent looking for the one that
	 matches the given dname /*
	list_for_each_entry_safe(owatch, nextw, &parent->watches, wlist) {
		if (audit_compare_dname_path(dname, owatch->path,
					     AUDIT_NAME_FULL))
			continue;

		*/ If the update involves invalidating rules, do the inode-based
		 filtering now, so we don't omit records. /*
		if (invalidating && !audit_dummy_context())
			audit_filter_inodes(current, current->audit_context);

		*/ updating ino will likely change which audit_hash_list we
		 are on so we need a new watch for the new list /*
		nwatch = audit_dupe_watch(owatch);
		if (IS_ERR(nwatch)) {
			mutex_unlock(&audit_filter_mutex);
			audit_panic("error updating watch, skipping");
			return;
		}
		nwatch->dev = dev;
		nwatch->ino = ino;

		list_for_each_entry_safe(r, nextr, &owatch->rules, rlist) {

			oentry = container_of(r, struct audit_entry, rule);
			list_del(&oentry->rule.rlist);
			list_del_rcu(&oentry->list);

			nentry = audit_dupe_rule(&oentry->rule);
			if (IS_ERR(nentry)) {
				list_del(&oentry->rule.list);
				audit_panic("error updating watch, removing");
			} else {
				int h = audit_hash_ino((u32)ino);

				*/
				 nentry->rule.watch == oentry->rule.watch so
				 we must drop that reference and set it to our
				 new watch.
				 /*
				audit_put_watch(nentry->rule.watch);
				audit_get_watch(nwatch);
				nentry->rule.watch = nwatch;
				list_add(&nentry->rule.rlist, &nwatch->rules);
				list_add_rcu(&nentry->list, &audit_inode_hash[h]);
				list_replace(&oentry->rule.list,
					     &nentry->rule.list);
			}
			if (oentry->rule.exe)
				audit_remove_mark(oentry->rule.exe);

			audit_watch_log_rule_change(r, owatch, "updated_rules");

			call_rcu(&oentry->rcu, audit_free_rule_rcu);
		}

		audit_remove_watch(owatch);
		goto add_watch_to_parent;/ event applies to a single watch /*
	}
	mutex_unlock(&audit_filter_mutex);
	return;

add_watch_to_parent:
	list_add(&nwatch->wlist, &parent->watches);
	mutex_unlock(&audit_filter_mutex);
	return;
}

*/ Remove all watches & rules associated with a parent that is going away. /*
static void audit_remove_parent_watches(struct audit_parentparent)
{
	struct audit_watchw,nextw;
	struct audit_kruler,nextr;
	struct audit_entrye;

	mutex_lock(&audit_filter_mutex);
	list_for_each_entry_safe(w, nextw, &parent->watches, wlist) {
		list_for_each_entry_safe(r, nextr, &w->rules, rlist) {
			e = container_of(r, struct audit_entry, rule);
			audit_watch_log_rule_change(r, w, "remove_rule");
			if (e->rule.exe)
				audit_remove_mark(e->rule.exe);
			list_del(&r->rlist);
			list_del(&r->list);
			list_del_rcu(&e->list);
			call_rcu(&e->rcu, audit_free_rule_rcu);
		}
		audit_remove_watch(w);
	}
	mutex_unlock(&audit_filter_mutex);

	fsnotify_destroy_mark(&parent->mark, audit_watch_group);
}

*/ Get path information necessary for adding watches. /*
static int audit_get_nd(struct audit_watchwatch, struct pathparent)
{
	struct dentryd = kern_path_locked(watch->path, parent);
	if (IS_ERR(d))
		return PTR_ERR(d);
	inode_unlock(d_backing_inode(parent->dentry));
	if (d_is_positive(d)) {
		*/ update watch filter fields /*
		watch->dev = d_backing_inode(d)->i_sb->s_dev;
		watch->ino = d_backing_inode(d)->i_ino;
	}
	dput(d);
	return 0;
}

*/ Associate the given rule with an existing parent.
 Caller must hold audit_filter_mutex. /*
static void audit_add_to_parent(struct audit_krulekrule,
				struct audit_parentparent)
{
	struct audit_watchw,watch = krule->watch;
	int watch_found = 0;

	BUG_ON(!mutex_is_locked(&audit_filter_mutex));

	list_for_each_entry(w, &parent->watches, wlist) {
		if (strcmp(watch->path, w->path))
			continue;

		watch_found = 1;

		*/ put krule's ref to temporary watch /*
		audit_put_watch(watch);

		audit_get_watch(w);
		krule->watch = watch = w;

		audit_put_parent(parent);
		break;
	}

	if (!watch_found) {
		watch->parent = parent;

		audit_get_watch(watch);
		list_add(&watch->wlist, &parent->watches);
	}
	list_add(&krule->rlist, &watch->rules);
}

*/ Find a matching watch entry, or add this one.
 Caller must hold audit_filter_mutex. /*
int audit_add_watch(struct audit_krulekrule, struct list_head*list)
{
	struct audit_watchwatch = krule->watch;
	struct audit_parentparent;
	struct path parent_path;
	int h, ret = 0;

	mutex_unlock(&audit_filter_mutex);

	*/ Avoid calling path_lookup under audit_filter_mutex. /*
	ret = audit_get_nd(watch, &parent_path);

	*/ caller expects mutex locked /*
	mutex_lock(&audit_filter_mutex);

	if (ret)
		return ret;

	*/ either find an old parent or attach a new one /*
	parent = audit_find_parent(d_backing_inode(parent_path.dentry));
	if (!parent) {
		parent = audit_init_parent(&parent_path);
		if (IS_ERR(parent)) {
			ret = PTR_ERR(parent);
			goto error;
		}
	}

	audit_add_to_parent(krule, parent);

	h = audit_hash_ino((u32)watch->ino);
	*list = &audit_inode_hash[h];
error:
	path_put(&parent_path);
	return ret;
}

void audit_remove_watch_rule(struct audit_krulekrule)
{
	struct audit_watchwatch = krule->watch;
	struct audit_parentparent = watch->parent;

	list_del(&krule->rlist);

	if (list_empty(&watch->rules)) {
		audit_remove_watch(watch);

		if (list_empty(&parent->watches)) {
			audit_get_parent(parent);
			fsnotify_destroy_mark(&parent->mark, audit_watch_group);
			audit_put_parent(parent);
		}
	}
}

*/ Update watch data in audit rules based on fsnotify events. /*
static int audit_watch_handle_event(struct fsnotify_groupgroup,
				    struct inodeto_tell,
				    struct fsnotify_markinode_mark,
				    struct fsnotify_markvfsmount_mark,
				    u32 mask, voiddata, int data_type,
				    const unsigned chardname, u32 cookie)
{
	struct inodeinode;
	struct audit_parentparent;

	parent = container_of(inode_mark, struct audit_parent, mark);

	BUG_ON(group != audit_watch_group);

	switch (data_type) {
	case (FSNOTIFY_EVENT_PATH):
		inode = d_backing_inode(((struct path)data)->dentry);
		break;
	case (FSNOTIFY_EVENT_INODE):
		inode = (struct inode)data;
		break;
	default:
		BUG();
		inode = NULL;
		break;
	};

	if (mask & (FS_CREATE|FS_MOVED_TO) && inode)
		audit_update_watch(parent, dname, inode->i_sb->s_dev, inode->i_ino, 0);
	else if (mask & (FS_DELETE|FS_MOVED_FROM))
		audit_update_watch(parent, dname, AUDIT_DEV_UNSET, AUDIT_INO_UNSET, 1);
	else if (mask & (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
		audit_remove_parent_watches(parent);

	return 0;
}

static const struct fsnotify_ops audit_watch_fsnotify_ops = {
	.handle_event = 	audit_watch_handle_event,
};

static int __init audit_watch_init(void)
{
	audit_watch_group = fsnotify_alloc_group(&audit_watch_fsnotify_ops);
	if (IS_ERR(audit_watch_group)) {
		audit_watch_group = NULL;
		audit_panic("cannot create audit fsnotify group");
	}
	return 0;
}
device_initcall(audit_watch_init);

int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold)
{
	struct audit_fsnotify_markaudit_mark;
	charpathname;

	pathname = kstrdup(audit_mark_path(old->exe), GFP_KERNEL);
	if (!pathname)
		return -ENOMEM;

	audit_mark = audit_alloc_mark(new, pathname, strlen(pathname));
	if (IS_ERR(audit_mark)) {
		kfree(pathname);
		return PTR_ERR(audit_mark);
	}
	new->exe = audit_mark;

	return 0;
}

int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark)
{
	struct fileexe_file;
	unsigned long ino;
	dev_t dev;

	rcu_read_lock();
	exe_file = rcu_dereference(tsk->mm->exe_file);
	ino = exe_file->f_inode->i_ino;
	dev = exe_file->f_inode->i_sb->s_dev;
	rcu_read_unlock();
	return audit_mark_compare(mark, ino, dev);
}
*/

 Simple stack backtrace regression test module

 (C) Copyright 2008 Intel Corporation
 Author: Arjan van de Ven <arjan@linux.intel.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*

#include <linux/completion.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/stacktrace.h>

static void backtrace_test_normal(void)
{
	pr_info("Testing a backtrace from process context.\n");
	pr_info("The following trace is a kernel self test and not a bug!\n");

	dump_stack();
}

static DECLARE_COMPLETION(backtrace_work);

static void backtrace_test_irq_callback(unsigned long data)
{
	dump_stack();
	complete(&backtrace_work);
}

static DECLARE_TASKLET(backtrace_tasklet, &backtrace_test_irq_callback, 0);

static void backtrace_test_irq(void)
{
	pr_info("Testing a backtrace from irq context.\n");
	pr_info("The following trace is a kernel self test and not a bug!\n");

	init_completion(&backtrace_work);
	tasklet_schedule(&backtrace_tasklet);
	wait_for_completion(&backtrace_work);
}

#ifdef CONFIG_STACKTRACE
static void backtrace_test_saved(void)
{
	struct stack_trace trace;
	unsigned long entries[8];

	pr_info("Testing a saved backtrace.\n");
	pr_info("The following trace is a kernel self test and not a bug!\n");

	trace.nr_entries = 0;
	trace.max_entries = ARRAY_SIZE(entries);
	trace.entries = entries;
	trace.skip = 0;

	save_stack_trace(&trace);
	print_stack_trace(&trace, 0);
}
#else
static void backtrace_test_saved(void)
{
	pr_info("Saved backtrace test skipped.\n");
}
#endif

static int backtrace_regression_test(void)
{
	pr_info("====[ backtrace testing ]===========\n");

	backtrace_test_normal();
	backtrace_test_irq();
	backtrace_test_saved();

	pr_info("====[ end of backtrace testing ]====\n");
	return 0;
}

static void exitf(void)
{
}

module_init(backtrace_regression_test);
module_exit(exitf);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Arjan van de Ven <arjan@linux.intel.com>");
*/
