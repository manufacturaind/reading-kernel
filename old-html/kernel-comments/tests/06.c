
 Copyright (C) 2010, 2015 Mathieu Desnoyers <mathieu.desnoyers@efficios.com>

 membarrier system call

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 /*

#include <linux/syscalls.h>
#include <linux/membarrier.h>

*/
 Bitmask made from a "or" of all commands within enum membarrier_cmd,
 except MEMBARRIER_CMD_QUERY.
 /*
#define MEMBARRIER_CMD_BITMASK	(MEMBARRIER_CMD_SHARED)

*/
 sys_membarrier - issue memory barriers on a set of threads
 @cmd:   Takes command values defined in enum membarrier_cmd.
 @flags: Currently needs to be 0. For future extensions.

 If this system call is not implemented, -ENOSYS is returned. If the
 command specified does not exist, or if the command argument is invalid,
 this system call returns -EINVAL. For a given command, with flags argument
 set to 0, this system call is guaranteed to always return the same value
 until reboot.

 All memory accesses performed in program order from each targeted thread
 is guaranteed to be ordered with respect to sys_membarrier(). If we use
 the semantic "barrier()" to represent a compiler barrier forcing memory
 accesses to be performed in program order across the barrier, and
 smp_mb() to represent explicit memory barriers forcing full memory
 ordering across the barrier, we have the following ordering table for
 each pair of barrier(), sys_membarrier() and smp_mb():

 The pair ordering is detailed as (O: ordered, X: not ordered):

                        barrier()   smp_mb() sys_membarrier()
        barrier()          X           X            O
        smp_mb()           X           O            O
        sys_membarrier()   O           O            O
 /*
SYSCALL_DEFINE2(membarrier, int, cmd, int, flags)
{
	if (unlikely(flags))
		return -EINVAL;
	switch (cmd) {
	case MEMBARRIER_CMD_QUERY:
		return MEMBARRIER_CMD_BITMASK;
	case MEMBARRIER_CMD_SHARED:
		if (num_online_cpus() > 1)
			synchronize_sched();
		return 0;
	default:
		return -EINVAL;
	}
}
*/

 Copyright(c) 2015 Intel Corporation. All rights reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of version 2 of the GNU General Public License as
 published by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 /*
#include <linux/radix-tree.h>
#include <linux/memremap.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/pfn_t.h>
#include <linux/io.h>
#include <linux/mm.h>
#include <linux/memory_hotplug.h>

#ifndef ioremap_cache
*/ temporary while we convert existing ioremap_cache users to memremap /*
__weak void __iomemioremap_cache(resource_size_t offset, unsigned long size)
{
	return ioremap(offset, size);
}
#endif

static voidtry_ram_remap(resource_size_t offset, size_t size)
{
	unsigned long pfn = PHYS_PFN(offset);

	*/ In the simple case just return the existing linear address /*
	if (pfn_valid(pfn) && !PageHighMem(pfn_to_page(pfn)))
		return __va(offset);
	return NULL;/ fallback to ioremap_cache /*
}

*/
 memremap() - remap an iomem_resource as cacheable memory
 @offset: iomem resource start address
 @size: size of remap
 @flags: any of MEMREMAP_WB, MEMREMAP_WT and MEMREMAP_WC

 memremap() is "ioremap" for cases where it is known that the resource
 being mapped does not have i/o side effects and the __iomem
 annotation is not applicable. In the case of multiple flags, the different
 mapping types will be attempted in the order listed below until one of
 them succeeds.

 MEMREMAP_WB - matches the default mapping for System RAM on
 the architecture.  This is usually a read-allocate write-back cache.
 Morever, if MEMREMAP_WB is specified and the requested remap region is RAM
 memremap() will bypass establishing a new mapping and instead return
 a pointer into the direct map.

 MEMREMAP_WT - establish a mapping whereby writes either bypass the
 cache or are written through to memory and never exist in a
 cache-dirty state with respect to program visibility.  Attempts to
 map System RAM with this mapping type will fail.

 MEMREMAP_WC - establish a writecombine mapping, whereby writes may
 be coalesced together (e.g. in the CPU's write buffers), but is otherwise
 uncached. Attempts to map System RAM with this mapping type will fail.
 /*
voidmemremap(resource_size_t offset, size_t size, unsigned long flags)
{
	int is_ram = region_intersects(offset, size,
				       IORESOURCE_SYSTEM_RAM, IORES_DESC_NONE);
	voidaddr = NULL;

	if (!flags)
		return NULL;

	if (is_ram == REGION_MIXED) {
		WARN_ONCE(1, "memremap attempted on mixed range %pa size: %#lx\n",
				&offset, (unsigned long) size);
		return NULL;
	}

	*/ Try all mapping types requested until one returns non-NULL /*
	if (flags & MEMREMAP_WB) {
		*/
		 MEMREMAP_WB is special in that it can be satisifed
		 from the direct map.  Some archs depend on the
		 capability of memremap() to autodetect cases where
		 the requested range is potentially in System RAM.
		 /*
		if (is_ram == REGION_INTERSECTS)
			addr = try_ram_remap(offset, size);
		if (!addr)
			addr = ioremap_cache(offset, size);
	}

	*/
	 If we don't have a mapping yet and other request flags are
	 present then we will be attempting to establish a new virtual
	 address mapping.  Enforce that this mapping is not aliasing
	 System RAM.
	 /*
	if (!addr && is_ram == REGION_INTERSECTS && flags != MEMREMAP_WB) {
		WARN_ONCE(1, "memremap attempted on ram %pa size: %#lx\n",
				&offset, (unsigned long) size);
		return NULL;
	}

	if (!addr && (flags & MEMREMAP_WT))
		addr = ioremap_wt(offset, size);

	if (!addr && (flags & MEMREMAP_WC))
		addr = ioremap_wc(offset, size);

	return addr;
}
EXPORT_SYMBOL(memremap);

void memunmap(voidaddr)
{
	if (is_vmalloc_addr(addr))
		iounmap((void __iomem) addr);
}
EXPORT_SYMBOL(memunmap);

static void devm_memremap_release(struct devicedev, voidres)
{
	memunmap(*(void*)res);
}

static int devm_memremap_match(struct devicedev, voidres, voidmatch_data)
{
	return(void*)res == match_data;
}

voiddevm_memremap(struct devicedev, resource_size_t offset,
		size_t size, unsigned long flags)
{
	void*ptr,addr;

	ptr = devres_alloc_node(devm_memremap_release, sizeof(*ptr), GFP_KERNEL,
			dev_to_node(dev));
	if (!ptr)
		return ERR_PTR(-ENOMEM);

	addr = memremap(offset, size, flags);
	if (addr) {
		*ptr = addr;
		devres_add(dev, ptr);
	} else {
		devres_free(ptr);
		return ERR_PTR(-ENXIO);
	}

	return addr;
}
EXPORT_SYMBOL(devm_memremap);

void devm_memunmap(struct devicedev, voidaddr)
{
	WARN_ON(devres_release(dev, devm_memremap_release,
				devm_memremap_match, addr));
}
EXPORT_SYMBOL(devm_memunmap);

pfn_t phys_to_pfn_t(phys_addr_t addr, u64 flags)
{
	return __pfn_to_pfn_t(addr >> PAGE_SHIFT, flags);
}
EXPORT_SYMBOL(phys_to_pfn_t);

#ifdef CONFIG_ZONE_DEVICE
static DEFINE_MUTEX(pgmap_lock);
static RADIX_TREE(pgmap_radix, GFP_KERNEL);
#define SECTION_MASK ~((1UL << PA_SECTION_SHIFT) - 1)
#define SECTION_SIZE (1UL << PA_SECTION_SHIFT)

struct page_map {
	struct resource res;
	struct percpu_refref;
	struct dev_pagemap pgmap;
	struct vmem_altmap altmap;
};

void get_zone_device_page(struct pagepage)
{
	percpu_ref_get(page->pgmap->ref);
}
EXPORT_SYMBOL(get_zone_device_page);

void put_zone_device_page(struct pagepage)
{
	put_dev_pagemap(page->pgmap);
}
EXPORT_SYMBOL(put_zone_device_page);

static void pgmap_radix_release(struct resourceres)
{
	resource_size_t key, align_start, align_size, align_end;

	align_start = res->start & ~(SECTION_SIZE - 1);
	align_size = ALIGN(resource_size(res), SECTION_SIZE);
	align_end = align_start + align_size - 1;

	mutex_lock(&pgmap_lock);
	for (key = res->start; key <= res->end; key += SECTION_SIZE)
		radix_tree_delete(&pgmap_radix, key >> PA_SECTION_SHIFT);
	mutex_unlock(&pgmap_lock);
}

static unsigned long pfn_first(struct page_mappage_map)
{
	struct dev_pagemappgmap = &page_map->pgmap;
	const struct resourceres = &page_map->res;
	struct vmem_altmapaltmap = pgmap->altmap;
	unsigned long pfn;

	pfn = res->start >> PAGE_SHIFT;
	if (altmap)
		pfn += vmem_altmap_offset(altmap);
	return pfn;
}

static unsigned long pfn_end(struct page_mappage_map)
{
	const struct resourceres = &page_map->res;

	return (res->start + resource_size(res)) >> PAGE_SHIFT;
}

#define for_each_device_pfn(pfn, map) \
	for (pfn = pfn_first(map); pfn < pfn_end(map); pfn++)

static void devm_memremap_pages_release(struct devicedev, voiddata)
{
	struct page_mappage_map = data;
	struct resourceres = &page_map->res;
	resource_size_t align_start, align_size;
	struct dev_pagemappgmap = &page_map->pgmap;

	if (percpu_ref_tryget_live(pgmap->ref)) {
		dev_WARN(dev, "%s: page mapping is still live!\n", __func__);
		percpu_ref_put(pgmap->ref);
	}

	*/ pages are dead and unused, undo the arch mapping /*
	align_start = res->start & ~(SECTION_SIZE - 1);
	align_size = ALIGN(resource_size(res), SECTION_SIZE);
	arch_remove_memory(align_start, align_size);
	pgmap_radix_release(res);
	dev_WARN_ONCE(dev, pgmap->altmap && pgmap->altmap->alloc,
			"%s: failed to free all reserved pages\n", __func__);
}

*/ assumes rcu_read_lock() held at entry /*
struct dev_pagemapfind_dev_pagemap(resource_size_t phys)
{
	struct page_mappage_map;

	WARN_ON_ONCE(!rcu_read_lock_held());

	page_map = radix_tree_lookup(&pgmap_radix, phys >> PA_SECTION_SHIFT);
	return page_map ? &page_map->pgmap : NULL;
}

*/
 devm_memremap_pages - remap and provide memmap backing for the given resource
 @dev: hosting device for @res
 @res: "host memory" address range
 @ref: a live per-cpu reference count
 @altmap: optional descriptor for allocating the memmap from @res

 Notes:
 1/ @ref must be 'live' on entry and 'dead' before devm_memunmap_pages() time
    (or devm release event).

 2/ @res is expected to be a host memory range that could feasibly be
    treated as a "System RAM" range, i.e. not a device mmio range, but
    this is not enforced.
 /*
voiddevm_memremap_pages(struct devicedev, struct resourceres,
		struct percpu_refref, struct vmem_altmapaltmap)
{
	resource_size_t key, align_start, align_size, align_end;
	struct dev_pagemappgmap;
	struct page_mappage_map;
	int error, nid, is_ram;
	unsigned long pfn;

	align_start = res->start & ~(SECTION_SIZE - 1);
	align_size = ALIGN(res->start + resource_size(res), SECTION_SIZE)
		- align_start;
	is_ram = region_intersects(align_start, align_size,
		IORESOURCE_SYSTEM_RAM, IORES_DESC_NONE);

	if (is_ram == REGION_MIXED) {
		WARN_ONCE(1, "%s attempted on mixed region %pr\n",
				__func__, res);
		return ERR_PTR(-ENXIO);
	}

	if (is_ram == REGION_INTERSECTS)
		return __va(res->start);

	if (altmap && !IS_ENABLED(CONFIG_SPARSEMEM_VMEMMAP)) {
		dev_err(dev, "%s: altmap requires CONFIG_SPARSEMEM_VMEMMAP=y\n",
				__func__);
		return ERR_PTR(-ENXIO);
	}

	if (!ref)
		return ERR_PTR(-EINVAL);

	page_map = devres_alloc_node(devm_memremap_pages_release,
			sizeof(*page_map), GFP_KERNEL, dev_to_node(dev));
	if (!page_map)
		return ERR_PTR(-ENOMEM);
	pgmap = &page_map->pgmap;

	memcpy(&page_map->res, res, sizeof(*res));

	pgmap->dev = dev;
	if (altmap) {
		memcpy(&page_map->altmap, altmap, sizeof(*altmap));
		pgmap->altmap = &page_map->altmap;
	}
	pgmap->ref = ref;
	pgmap->res = &page_map->res;

	mutex_lock(&pgmap_lock);
	error = 0;
	align_end = align_start + align_size - 1;
	for (key = align_start; key <= align_end; key += SECTION_SIZE) {
		struct dev_pagemapdup;

		rcu_read_lock();
		dup = find_dev_pagemap(key);
		rcu_read_unlock();
		if (dup) {
			dev_err(dev, "%s: %pr collides with mapping for %s\n",
					__func__, res, dev_name(dup->dev));
			error = -EBUSY;
			break;
		}
		error = radix_tree_insert(&pgmap_radix, key >> PA_SECTION_SHIFT,
				page_map);
		if (error) {
			dev_err(dev, "%s: failed: %d\n", __func__, error);
			break;
		}
	}
	mutex_unlock(&pgmap_lock);
	if (error)
		goto err_radix;

	nid = dev_to_node(dev);
	if (nid < 0)
		nid = numa_mem_id();

	error = arch_add_memory(nid, align_start, align_size, true);
	if (error)
		goto err_add_memory;

	for_each_device_pfn(pfn, page_map) {
		struct pagepage = pfn_to_page(pfn);

		*/
		 ZONE_DEVICE pages union ->lru with a ->pgmap back
		 pointer.  It is a bug if a ZONE_DEVICE page is ever
		 freed or placed on a driver-private list.  Seed the
		 storage with LIST_POISON* values.
		 /*
		list_del(&page->lru);
		page->pgmap = pgmap;
	}
	devres_add(dev, page_map);
	return __va(res->start);

 err_add_memory:
 err_radix:
	pgmap_radix_release(res);
	devres_free(page_map);
	return ERR_PTR(error);
}
EXPORT_SYMBOL(devm_memremap_pages);

unsigned long vmem_altmap_offset(struct vmem_altmapaltmap)
{
	*/ number of pfns from base where pfn_to_page() is valid /*
	return altmap->reserve + altmap->free;
}

void vmem_altmap_free(struct vmem_altmapaltmap, unsigned long nr_pfns)
{
	altmap->alloc -= nr_pfns;
}

#ifdef CONFIG_SPARSEMEM_VMEMMAP
struct vmem_altmapto_vmem_altmap(unsigned long memmap_start)
{
	*/
	 'memmap_start' is the virtual address for the first "struct
	 page" in this range of the vmemmap array.  In the case of
	 CONFIG_SPARSEMEM_VMEMMAP a page_to_pfn conversion is simple
	 pointer arithmetic, so we can perform this to_vmem_altmap()
	 conversion without concern for the initialization state of
	 the struct page fields.
	 /*
	struct pagepage = (struct page) memmap_start;
	struct dev_pagemappgmap;

	*/
	 Unconditionally retrieve a dev_pagemap associated with the
	 given physical address, this is only for use in the
	 arch_{add|remove}_memory() for setting up and tearing down
	 the memmap.
	 /*
	rcu_read_lock();
	pgmap = find_dev_pagemap(__pfn_to_phys(page_to_pfn(page)));
	rcu_read_unlock();

	return pgmap ? pgmap->altmap : NULL;
}
#endif*/ CONFIG_SPARSEMEM_VMEMMAP /*
#endif*/ CONFIG_ZONE_DEVICE

   Copyright (C) 2002 Richard Henderson
   Copyright (C) 2001 Rusty Russell, 2002, 2010 Rusty Russell IBM.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/*
#include <linux/export.h>
#include <linux/moduleloader.h>
#include <linux/trace_events.h>
#include <linux/init.h>
#include <linux/kallsyms.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/sysfs.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/elf.h>
#include <linux/proc_fs.h>
#include <linux/security.h>
#include <linux/seq_file.h>
#include <linux/syscalls.h>
#include <linux/fcntl.h>
#include <linux/rcupdate.h>
#include <linux/capability.h>
#include <linux/cpu.h>
#include <linux/moduleparam.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/vermagic.h>
#include <linux/notifier.h>
#include <linux/sched.h>
#include <linux/device.h>
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/rculist.h>
#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <asm/mmu_context.h>
#include <linux/license.h>
#include <asm/sections.h>
#include <linux/tracepoint.h>
#include <linux/ftrace.h>
#include <linux/livepatch.h>
#include <linux/async.h>
#include <linux/percpu.h>
#include <linux/kmemleak.h>
#include <linux/jump_label.h>
#include <linux/pfn.h>
#include <linux/bsearch.h>
#include <uapi/linux/module.h>
#include "module-internal.h"

#define CREATE_TRACE_POINTS
#include <trace/events/module.h>

#ifndef ARCH_SHF_SMALL
#define ARCH_SHF_SMALL 0
#endif

*/
 Modules' sections will be aligned on page boundaries
 to ensure complete separation of code and data, but
 only when CONFIG_DEBUG_SET_MODULE_RONX=y
 /*
#ifdef CONFIG_DEBUG_SET_MODULE_RONX
# define debug_align(X) ALIGN(X, PAGE_SIZE)
#else
# define debug_align(X) (X)
#endif

*/ If this is set, the section belongs in the init part of the module /*
#define INIT_OFFSET_MASK (1UL << (BITS_PER_LONG-1))

*/
 Mutex protects:
 1) List of modules (also safely readable with preempt_disable),
 2) module_use links,
 3) module_addr_min/module_addr_max.
 (delete and add uses RCU list operations). /*
DEFINE_MUTEX(module_mutex);
EXPORT_SYMBOL_GPL(module_mutex);
static LIST_HEAD(modules);

#ifdef CONFIG_MODULES_TREE_LOOKUP

*/
 Use a latched RB-tree for __module_address(); this allows us to use
 RCU-sched lookups of the address from any context.

 This is conditional on PERF_EVENTS || TRACING because those can really hit
 __module_address() hard by doing a lot of stack unwinding; potentially from
 NMI context.
 /*

static __always_inline unsigned long __mod_tree_val(struct latch_tree_noden)
{
	struct module_layoutlayout = container_of(n, struct module_layout, mtn.node);

	return (unsigned long)layout->base;
}

static __always_inline unsigned long __mod_tree_size(struct latch_tree_noden)
{
	struct module_layoutlayout = container_of(n, struct module_layout, mtn.node);

	return (unsigned long)layout->size;
}

static __always_inline bool
mod_tree_less(struct latch_tree_nodea, struct latch_tree_nodeb)
{
	return __mod_tree_val(a) < __mod_tree_val(b);
}

static __always_inline int
mod_tree_comp(voidkey, struct latch_tree_noden)
{
	unsigned long val = (unsigned long)key;
	unsigned long start, end;

	start = __mod_tree_val(n);
	if (val < start)
		return -1;

	end = start + __mod_tree_size(n);
	if (val >= end)
		return 1;

	return 0;
}

static const struct latch_tree_ops mod_tree_ops = {
	.less = mod_tree_less,
	.comp = mod_tree_comp,
};

static struct mod_tree_root {
	struct latch_tree_root root;
	unsigned long addr_min;
	unsigned long addr_max;
} mod_tree __cacheline_aligned = {
	.addr_min = -1UL,
};

#define module_addr_min mod_tree.addr_min
#define module_addr_max mod_tree.addr_max

static noinline void __mod_tree_insert(struct mod_tree_nodenode)
{
	latch_tree_insert(&node->node, &mod_tree.root, &mod_tree_ops);
}

static void __mod_tree_remove(struct mod_tree_nodenode)
{
	latch_tree_erase(&node->node, &mod_tree.root, &mod_tree_ops);
}

*/
 These modifications: insert, remove_init and remove; are serialized by the
 module_mutex.
 /*
static void mod_tree_insert(struct modulemod)
{
	mod->core_layout.mtn.mod = mod;
	mod->init_layout.mtn.mod = mod;

	__mod_tree_insert(&mod->core_layout.mtn);
	if (mod->init_layout.size)
		__mod_tree_insert(&mod->init_layout.mtn);
}

static void mod_tree_remove_init(struct modulemod)
{
	if (mod->init_layout.size)
		__mod_tree_remove(&mod->init_layout.mtn);
}

static void mod_tree_remove(struct modulemod)
{
	__mod_tree_remove(&mod->core_layout.mtn);
	mod_tree_remove_init(mod);
}

static struct modulemod_find(unsigned long addr)
{
	struct latch_tree_nodeltn;

	ltn = latch_tree_find((void)addr, &mod_tree.root, &mod_tree_ops);
	if (!ltn)
		return NULL;

	return container_of(ltn, struct mod_tree_node, node)->mod;
}

#else */ MODULES_TREE_LOOKUP /*

static unsigned long module_addr_min = -1UL, module_addr_max = 0;

static void mod_tree_insert(struct modulemod) { }
static void mod_tree_remove_init(struct modulemod) { }
static void mod_tree_remove(struct modulemod) { }

static struct modulemod_find(unsigned long addr)
{
	struct modulemod;

	list_for_each_entry_rcu(mod, &modules, list) {
		if (within_module(addr, mod))
			return mod;
	}

	return NULL;
}

#endif */ MODULES_TREE_LOOKUP /*

*/
 Bounds of module text, for speeding up __module_address.
 Protected by module_mutex.
 /*
static void __mod_update_bounds(voidbase, unsigned int size)
{
	unsigned long min = (unsigned long)base;
	unsigned long max = min + size;

	if (min < module_addr_min)
		module_addr_min = min;
	if (max > module_addr_max)
		module_addr_max = max;
}

static void mod_update_bounds(struct modulemod)
{
	__mod_update_bounds(mod->core_layout.base, mod->core_layout.size);
	if (mod->init_layout.size)
		__mod_update_bounds(mod->init_layout.base, mod->init_layout.size);
}

#ifdef CONFIG_KGDB_KDB
struct list_headkdb_modules = &modules;/ kdb needs the list of modules /*
#endif */ CONFIG_KGDB_KDB /*

static void module_assert_mutex(void)
{
	lockdep_assert_held(&module_mutex);
}

static void module_assert_mutex_or_preempt(void)
{
#ifdef CONFIG_LOCKDEP
	if (unlikely(!debug_locks))
		return;

	WARN_ON(!rcu_read_lock_sched_held() &&
		!lockdep_is_held(&module_mutex));
#endif
}

static bool sig_enforce = IS_ENABLED(CONFIG_MODULE_SIG_FORCE);
#ifndef CONFIG_MODULE_SIG_FORCE
module_param(sig_enforce, bool_enable_only, 0644);
#endif */ !CONFIG_MODULE_SIG_FORCE /*

*/ Block module loading/unloading? /*
int modules_disabled = 0;
core_param(nomodule, modules_disabled, bint, 0);

*/ Waiting for a module to finish initializing? /*
static DECLARE_WAIT_QUEUE_HEAD(module_wq);

static BLOCKING_NOTIFIER_HEAD(module_notify_list);

int register_module_notifier(struct notifier_blocknb)
{
	return blocking_notifier_chain_register(&module_notify_list, nb);
}
EXPORT_SYMBOL(register_module_notifier);

int unregister_module_notifier(struct notifier_blocknb)
{
	return blocking_notifier_chain_unregister(&module_notify_list, nb);
}
EXPORT_SYMBOL(unregister_module_notifier);

struct load_info {
	Elf_Ehdrhdr;
	unsigned long len;
	Elf_Shdrsechdrs;
	charsecstrings,strtab;
	unsigned long symoffs, stroffs;
	struct _ddebugdebug;
	unsigned int num_debug;
	bool sig_ok;
#ifdef CONFIG_KALLSYMS
	unsigned long mod_kallsyms_init_off;
#endif
	struct {
		unsigned int sym, str, mod, vers, info, pcpu;
	} index;
};

*/ We require a truly strong try_module_get(): 0 means failure due to
   ongoing or failed initialization etc. /*
static inline int strong_try_module_get(struct modulemod)
{
	BUG_ON(mod && mod->state == MODULE_STATE_UNFORMED);
	if (mod && mod->state == MODULE_STATE_COMING)
		return -EBUSY;
	if (try_module_get(mod))
		return 0;
	else
		return -ENOENT;
}

static inline void add_taint_module(struct modulemod, unsigned flag,
				    enum lockdep_ok lockdep_ok)
{
	add_taint(flag, lockdep_ok);
	mod->taints |= (1U << flag);
}

*/
 A thread that wants to hold a reference to a module only while it
 is running can call this to safely exit.  nfsd and lockd use this.
 /*
void __module_put_and_exit(struct modulemod, long code)
{
	module_put(mod);
	do_exit(code);
}
EXPORT_SYMBOL(__module_put_and_exit);

*/ Find a module section: 0 means not found. /*
static unsigned int find_sec(const struct load_infoinfo, const charname)
{
	unsigned int i;

	for (i = 1; i < info->hdr->e_shnum; i++) {
		Elf_Shdrshdr = &info->sechdrs[i];
		*/ Alloc bit cleared means "ignore it." /*
		if ((shdr->sh_flags & SHF_ALLOC)
		    && strcmp(info->secstrings + shdr->sh_name, name) == 0)
			return i;
	}
	return 0;
}

*/ Find a module section, or NULL. /*
static voidsection_addr(const struct load_infoinfo, const charname)
{
	*/ Section 0 has sh_addr 0. /*
	return (void)info->sechdrs[find_sec(info, name)].sh_addr;
}

*/ Find a module section, or NULL.  Fill in number of "objects" in section. /*
static voidsection_objs(const struct load_infoinfo,
			  const charname,
			  size_t object_size,
			  unsigned intnum)
{
	unsigned int sec = find_sec(info, name);

	*/ Section 0 has sh_addr 0 and sh_size 0. /*
	*num = info->sechdrs[sec].sh_size / object_size;
	return (void)info->sechdrs[sec].sh_addr;
}

*/ Provided by the linker /*
extern const struct kernel_symbol __start___ksymtab[];
extern const struct kernel_symbol __stop___ksymtab[];
extern const struct kernel_symbol __start___ksymtab_gpl[];
extern const struct kernel_symbol __stop___ksymtab_gpl[];
extern const struct kernel_symbol __start___ksymtab_gpl_future[];
extern const struct kernel_symbol __stop___ksymtab_gpl_future[];
extern const unsigned long __start___kcrctab[];
extern const unsigned long __start___kcrctab_gpl[];
extern const unsigned long __start___kcrctab_gpl_future[];
#ifdef CONFIG_UNUSED_SYMBOLS
extern const struct kernel_symbol __start___ksymtab_unused[];
extern const struct kernel_symbol __stop___ksymtab_unused[];
extern const struct kernel_symbol __start___ksymtab_unused_gpl[];
extern const struct kernel_symbol __stop___ksymtab_unused_gpl[];
extern const unsigned long __start___kcrctab_unused[];
extern const unsigned long __start___kcrctab_unused_gpl[];
#endif

#ifndef CONFIG_MODVERSIONS
#define symversion(base, idx) NULL
#else
#define symversion(base, idx) ((base != NULL) ? ((base) + (idx)) : NULL)
#endif

static bool each_symbol_in_section(const struct symsearcharr,
				   unsigned int arrsize,
				   struct moduleowner,
				   bool (*fn)(const struct symsearchsyms,
					      struct moduleowner,
					      voiddata),
				   voiddata)
{
	unsigned int j;

	for (j = 0; j < arrsize; j++) {
		if (fn(&arr[j], owner, data))
			return true;
	}

	return false;
}

*/ Returns true as soon as fn returns true, otherwise false. /*
bool each_symbol_section(bool (*fn)(const struct symsearcharr,
				    struct moduleowner,
				    voiddata),
			 voiddata)
{
	struct modulemod;
	static const struct symsearch arr[] = {
		{ __start___ksymtab, __stop___ksymtab, __start___kcrctab,
		  NOT_GPL_ONLY, false },
		{ __start___ksymtab_gpl, __stop___ksymtab_gpl,
		  __start___kcrctab_gpl,
		  GPL_ONLY, false },
		{ __start___ksymtab_gpl_future, __stop___ksymtab_gpl_future,
		  __start___kcrctab_gpl_future,
		  WILL_BE_GPL_ONLY, false },
#ifdef CONFIG_UNUSED_SYMBOLS
		{ __start___ksymtab_unused, __stop___ksymtab_unused,
		  __start___kcrctab_unused,
		  NOT_GPL_ONLY, true },
		{ __start___ksymtab_unused_gpl, __stop___ksymtab_unused_gpl,
		  __start___kcrctab_unused_gpl,
		  GPL_ONLY, true },
#endif
	};

	module_assert_mutex_or_preempt();

	if (each_symbol_in_section(arr, ARRAY_SIZE(arr), NULL, fn, data))
		return true;

	list_for_each_entry_rcu(mod, &modules, list) {
		struct symsearch arr[] = {
			{ mod->syms, mod->syms + mod->num_syms, mod->crcs,
			  NOT_GPL_ONLY, false },
			{ mod->gpl_syms, mod->gpl_syms + mod->num_gpl_syms,
			  mod->gpl_crcs,
			  GPL_ONLY, false },
			{ mod->gpl_future_syms,
			  mod->gpl_future_syms + mod->num_gpl_future_syms,
			  mod->gpl_future_crcs,
			  WILL_BE_GPL_ONLY, false },
#ifdef CONFIG_UNUSED_SYMBOLS
			{ mod->unused_syms,
			  mod->unused_syms + mod->num_unused_syms,
			  mod->unused_crcs,
			  NOT_GPL_ONLY, true },
			{ mod->unused_gpl_syms,
			  mod->unused_gpl_syms + mod->num_unused_gpl_syms,
			  mod->unused_gpl_crcs,
			  GPL_ONLY, true },
#endif
		};

		if (mod->state == MODULE_STATE_UNFORMED)
			continue;

		if (each_symbol_in_section(arr, ARRAY_SIZE(arr), mod, fn, data))
			return true;
	}
	return false;
}
EXPORT_SYMBOL_GPL(each_symbol_section);

struct find_symbol_arg {
	*/ Input /*
	const charname;
	bool gplok;
	bool warn;

	*/ Output /*
	struct moduleowner;
	const unsigned longcrc;
	const struct kernel_symbolsym;
};

static bool check_symbol(const struct symsearchsyms,
				 struct moduleowner,
				 unsigned int symnum, voiddata)
{
	struct find_symbol_argfsa = data;

	if (!fsa->gplok) {
		if (syms->licence == GPL_ONLY)
			return false;
		if (syms->licence == WILL_BE_GPL_ONLY && fsa->warn) {
			pr_warn("Symbol %s is being used by a non-GPL module, "
				"which will not be allowed in the future\n",
				fsa->name);
		}
	}

#ifdef CONFIG_UNUSED_SYMBOLS
	if (syms->unused && fsa->warn) {
		pr_warn("Symbol %s is marked as UNUSED, however this module is "
			"using it.\n", fsa->name);
		pr_warn("This symbol will go away in the future.\n");
		pr_warn("Please evaluate if this is the right api to use and "
			"if it really is, submit a report to the linux kernel "
			"mailing list together with submitting your code for "
			"inclusion.\n");
	}
#endif

	fsa->owner = owner;
	fsa->crc = symversion(syms->crcs, symnum);
	fsa->sym = &syms->start[symnum];
	return true;
}

static int cmp_name(const voidva, const voidvb)
{
	const chara;
	const struct kernel_symbolb;
	a = va; b = vb;
	return strcmp(a, b->name);
}

static bool find_symbol_in_section(const struct symsearchsyms,
				   struct moduleowner,
				   voiddata)
{
	struct find_symbol_argfsa = data;
	struct kernel_symbolsym;

	sym = bsearch(fsa->name, syms->start, syms->stop - syms->start,
			sizeof(struct kernel_symbol), cmp_name);

	if (sym != NULL && check_symbol(syms, owner, sym - syms->start, data))
		return true;

	return false;
}

*/ Find a symbol and return it, along with, (optional) crc and
 (optional) module which owns it.  Needs preempt disabled or module_mutex. /*
const struct kernel_symbolfind_symbol(const charname,
					struct module*owner,
					const unsigned long*crc,
					bool gplok,
					bool warn)
{
	struct find_symbol_arg fsa;

	fsa.name = name;
	fsa.gplok = gplok;
	fsa.warn = warn;

	if (each_symbol_section(find_symbol_in_section, &fsa)) {
		if (owner)
			*owner = fsa.owner;
		if (crc)
			*crc = fsa.crc;
		return fsa.sym;
	}

	pr_debug("Failed to find symbol %s\n", name);
	return NULL;
}
EXPORT_SYMBOL_GPL(find_symbol);

*/
 Search for module by name: must hold module_mutex (or preempt disabled
 for read-only access).
 /*
static struct modulefind_module_all(const charname, size_t len,
				      bool even_unformed)
{
	struct modulemod;

	module_assert_mutex_or_preempt();

	list_for_each_entry(mod, &modules, list) {
		if (!even_unformed && mod->state == MODULE_STATE_UNFORMED)
			continue;
		if (strlen(mod->name) == len && !memcmp(mod->name, name, len))
			return mod;
	}
	return NULL;
}

struct modulefind_module(const charname)
{
	module_assert_mutex();
	return find_module_all(name, strlen(name), false);
}
EXPORT_SYMBOL_GPL(find_module);

#ifdef CONFIG_SMP

static inline void __percpumod_percpu(struct modulemod)
{
	return mod->percpu;
}

static int percpu_modalloc(struct modulemod, struct load_infoinfo)
{
	Elf_Shdrpcpusec = &info->sechdrs[info->index.pcpu];
	unsigned long align = pcpusec->sh_addralign;

	if (!pcpusec->sh_size)
		return 0;

	if (align > PAGE_SIZE) {
		pr_warn("%s: per-cpu alignment %li > %li\n",
			mod->name, align, PAGE_SIZE);
		align = PAGE_SIZE;
	}

	mod->percpu = __alloc_reserved_percpu(pcpusec->sh_size, align);
	if (!mod->percpu) {
		pr_warn("%s: Could not allocate %lu bytes percpu data\n",
			mod->name, (unsigned long)pcpusec->sh_size);
		return -ENOMEM;
	}
	mod->percpu_size = pcpusec->sh_size;
	return 0;
}

static void percpu_modfree(struct modulemod)
{
	free_percpu(mod->percpu);
}

static unsigned int find_pcpusec(struct load_infoinfo)
{
	return find_sec(info, ".data..percpu");
}

static void percpu_modcopy(struct modulemod,
			   const voidfrom, unsigned long size)
{
	int cpu;

	for_each_possible_cpu(cpu)
		memcpy(per_cpu_ptr(mod->percpu, cpu), from, size);
}

*/
 is_module_percpu_address - test whether address is from module static percpu
 @addr: address to test

 Test whether @addr belongs to module static percpu area.

 RETURNS:
 %true if @addr is from module static percpu area
 /*
bool is_module_percpu_address(unsigned long addr)
{
	struct modulemod;
	unsigned int cpu;

	preempt_disable();

	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		if (!mod->percpu_size)
			continue;
		for_each_possible_cpu(cpu) {
			voidstart = per_cpu_ptr(mod->percpu, cpu);

			if ((void)addr >= start &&
			    (void)addr < start + mod->percpu_size) {
				preempt_enable();
				return true;
			}
		}
	}

	preempt_enable();
	return false;
}

#else */ ... !CONFIG_SMP /*

static inline void __percpumod_percpu(struct modulemod)
{
	return NULL;
}
static int percpu_modalloc(struct modulemod, struct load_infoinfo)
{
	*/ UP modules shouldn't have this section: ENOMEM isn't quite right /*
	if (info->sechdrs[info->index.pcpu].sh_size != 0)
		return -ENOMEM;
	return 0;
}
static inline void percpu_modfree(struct modulemod)
{
}
static unsigned int find_pcpusec(struct load_infoinfo)
{
	return 0;
}
static inline void percpu_modcopy(struct modulemod,
				  const voidfrom, unsigned long size)
{
	*/ pcpusec should be 0, and size of that section should be 0. /*
	BUG_ON(size != 0);
}
bool is_module_percpu_address(unsigned long addr)
{
	return false;
}

#endif */ CONFIG_SMP /*

#define MODINFO_ATTR(field)	\
static void setup_modinfo_##field(struct modulemod, const chars)  \
{                                                                     \
	mod->field = kstrdup(s, GFP_KERNEL);                          \
}                                                                     \
static ssize_t show_modinfo_##field(struct module_attributemattr,   \
			struct module_kobjectmk, charbuffer)      \
{                                                                     \
	return scnprintf(buffer, PAGE_SIZE, "%s\n", mk->mod->field);  \
}                                                                     \
static int modinfo_##field##_exists(struct modulemod)               \
{                                                                     \
	return mod->field != NULL;                                    \
}                                                                     \
static void free_modinfo_##field(struct modulemod)                  \
{                                                                     \
	kfree(mod->field);                                            \
	mod->field = NULL;                                            \
}                                                                     \
static struct module_attribute modinfo_##field = {                    \
	.attr = { .name = __stringify(field), .mode = 0444 },         \
	.show = show_modinfo_##field,                                 \
	.setup = setup_modinfo_##field,                               \
	.test = modinfo_##field##_exists,                             \
	.free = free_modinfo_##field,                                 \
};

MODINFO_ATTR(version);
MODINFO_ATTR(srcversion);

static char last_unloaded_module[MODULE_NAME_LEN+1];

#ifdef CONFIG_MODULE_UNLOAD

EXPORT_TRACEPOINT_SYMBOL(module_get);

*/ MODULE_REF_BASE is the base reference count by kmodule loader. /*
#define MODULE_REF_BASE	1

*/ Init the unload section of the module. /*
static int module_unload_init(struct modulemod)
{
	*/
	 Initialize reference counter to MODULE_REF_BASE.
	 refcnt == 0 means module is going.
	 /*
	atomic_set(&mod->refcnt, MODULE_REF_BASE);

	INIT_LIST_HEAD(&mod->source_list);
	INIT_LIST_HEAD(&mod->target_list);

	*/ Hold reference count during initialization. /*
	atomic_inc(&mod->refcnt);

	return 0;
}

*/ Does a already use b? /*
static int already_uses(struct modulea, struct moduleb)
{
	struct module_useuse;

	list_for_each_entry(use, &b->source_list, source_list) {
		if (use->source == a) {
			pr_debug("%s uses %s!\n", a->name, b->name);
			return 1;
		}
	}
	pr_debug("%s does not use %s!\n", a->name, b->name);
	return 0;
}

*/
 Module a uses b
  - we add 'a' as a "source", 'b' as a "target" of module use
  - the module_use is added to the list of 'b' sources (so
    'b' can walk the list to see who sourced them), and of 'a'
    targets (so 'a' can see what modules it targets).
 /*
static int add_module_usage(struct modulea, struct moduleb)
{
	struct module_useuse;

	pr_debug("Allocating new usage for %s.\n", a->name);
	use = kmalloc(sizeof(*use), GFP_ATOMIC);
	if (!use) {
		pr_warn("%s: out of memory loading\n", a->name);
		return -ENOMEM;
	}

	use->source = a;
	use->target = b;
	list_add(&use->source_list, &b->source_list);
	list_add(&use->target_list, &a->target_list);
	return 0;
}

*/ Module a uses b: caller needs module_mutex() /*
int ref_module(struct modulea, struct moduleb)
{
	int err;

	if (b == NULL || already_uses(a, b))
		return 0;

	*/ If module isn't available, we fail. /*
	err = strong_try_module_get(b);
	if (err)
		return err;

	err = add_module_usage(a, b);
	if (err) {
		module_put(b);
		return err;
	}
	return 0;
}
EXPORT_SYMBOL_GPL(ref_module);

*/ Clear the unload stuff of the module. /*
static void module_unload_free(struct modulemod)
{
	struct module_useuse,tmp;

	mutex_lock(&module_mutex);
	list_for_each_entry_safe(use, tmp, &mod->target_list, target_list) {
		struct modulei = use->target;
		pr_debug("%s unusing %s\n", mod->name, i->name);
		module_put(i);
		list_del(&use->source_list);
		list_del(&use->target_list);
		kfree(use);
	}
	mutex_unlock(&module_mutex);
}

#ifdef CONFIG_MODULE_FORCE_UNLOAD
static inline int try_force_unload(unsigned int flags)
{
	int ret = (flags & O_TRUNC);
	if (ret)
		add_taint(TAINT_FORCED_RMMOD, LOCKDEP_NOW_UNRELIABLE);
	return ret;
}
#else
static inline int try_force_unload(unsigned int flags)
{
	return 0;
}
#endif */ CONFIG_MODULE_FORCE_UNLOAD /*

*/ Try to release refcount of module, 0 means success. /*
static int try_release_module_ref(struct modulemod)
{
	int ret;

	*/ Try to decrement refcnt which we set at loading /*
	ret = atomic_sub_return(MODULE_REF_BASE, &mod->refcnt);
	BUG_ON(ret < 0);
	if (ret)
		*/ Someone can put this right now, recover with checking /*
		ret = atomic_add_unless(&mod->refcnt, MODULE_REF_BASE, 0);

	return ret;
}

static int try_stop_module(struct modulemod, int flags, intforced)
{
	*/ If it's not unused, quit unless we're forcing. /*
	if (try_release_module_ref(mod) != 0) {
		*forced = try_force_unload(flags);
		if (!(*forced))
			return -EWOULDBLOCK;
	}

	*/ Mark it as dying. /*
	mod->state = MODULE_STATE_GOING;

	return 0;
}

*/
 module_refcount - return the refcount or -1 if unloading

 @mod:	the module we're checking

 Returns:
	-1 if the module is in the process of unloading
	otherwise the number of references in the kernel to the module
 /*
int module_refcount(struct modulemod)
{
	return atomic_read(&mod->refcnt) - MODULE_REF_BASE;
}
EXPORT_SYMBOL(module_refcount);

*/ This exists whether we can unload or not /*
static void free_module(struct modulemod);

SYSCALL_DEFINE2(delete_module, const char __user, name_user,
		unsigned int, flags)
{
	struct modulemod;
	char name[MODULE_NAME_LEN];
	int ret, forced = 0;

	if (!capable(CAP_SYS_MODULE) || modules_disabled)
		return -EPERM;

	if (strncpy_from_user(name, name_user, MODULE_NAME_LEN-1) < 0)
		return -EFAULT;
	name[MODULE_NAME_LEN-1] = '\0';

	if (mutex_lock_interruptible(&module_mutex) != 0)
		return -EINTR;

	mod = find_module(name);
	if (!mod) {
		ret = -ENOENT;
		goto out;
	}

	if (!list_empty(&mod->source_list)) {
		*/ Other modules depend on us: get rid of them first. /*
		ret = -EWOULDBLOCK;
		goto out;
	}

	*/ Doing init or already dying? /*
	if (mod->state != MODULE_STATE_LIVE) {
		*/ FIXME: if (force), slam module count damn the torpedoes /*
		pr_debug("%s already dying\n", mod->name);
		ret = -EBUSY;
		goto out;
	}

	*/ If it has an init func, it must have an exit func to unload /*
	if (mod->init && !mod->exit) {
		forced = try_force_unload(flags);
		if (!forced) {
			*/ This module can't be removed /*
			ret = -EBUSY;
			goto out;
		}
	}

	*/ Stop the machine so refcounts can't move and disable module. /*
	ret = try_stop_module(mod, flags, &forced);
	if (ret != 0)
		goto out;

	mutex_unlock(&module_mutex);
	*/ Final destruction now no one is using it. /*
	if (mod->exit != NULL)
		mod->exit();
	blocking_notifier_call_chain(&module_notify_list,
				     MODULE_STATE_GOING, mod);
	klp_module_going(mod);
	ftrace_release_mod(mod);

	async_synchronize_full();

	*/ Store the name of the last unloaded module for diagnostic purposes /*
	strlcpy(last_unloaded_module, mod->name, sizeof(last_unloaded_module));

	free_module(mod);
	return 0;
out:
	mutex_unlock(&module_mutex);
	return ret;
}

static inline void print_unload_info(struct seq_filem, struct modulemod)
{
	struct module_useuse;
	int printed_something = 0;

	seq_printf(m, " %i ", module_refcount(mod));

	*/
	 Always include a trailing , so userspace can differentiate
	 between this and the old multi-field proc format.
	 /*
	list_for_each_entry(use, &mod->source_list, source_list) {
		printed_something = 1;
		seq_printf(m, "%s,", use->source->name);
	}

	if (mod->init != NULL && mod->exit == NULL) {
		printed_something = 1;
		seq_puts(m, "[permanent],");
	}

	if (!printed_something)
		seq_puts(m, "-");
}

void __symbol_put(const charsymbol)
{
	struct moduleowner;

	preempt_disable();
	if (!find_symbol(symbol, &owner, NULL, true, false))
		BUG();
	module_put(owner);
	preempt_enable();
}
EXPORT_SYMBOL(__symbol_put);

*/ Note this assumes addr is a function, which it currently always is. /*
void symbol_put_addr(voidaddr)
{
	struct modulemodaddr;
	unsigned long a = (unsigned long)dereference_function_descriptor(addr);

	if (core_kernel_text(a))
		return;

	*/
	 Even though we hold a reference on the module; we still need to
	 disable preemption in order to safely traverse the data structure.
	 /*
	preempt_disable();
	modaddr = __module_text_address(a);
	BUG_ON(!modaddr);
	module_put(modaddr);
	preempt_enable();
}
EXPORT_SYMBOL_GPL(symbol_put_addr);

static ssize_t show_refcnt(struct module_attributemattr,
			   struct module_kobjectmk, charbuffer)
{
	return sprintf(buffer, "%i\n", module_refcount(mk->mod));
}

static struct module_attribute modinfo_refcnt =
	__ATTR(refcnt, 0444, show_refcnt, NULL);

void __module_get(struct modulemodule)
{
	if (module) {
		preempt_disable();
		atomic_inc(&module->refcnt);
		trace_module_get(module, _RET_IP_);
		preempt_enable();
	}
}
EXPORT_SYMBOL(__module_get);

bool try_module_get(struct modulemodule)
{
	bool ret = true;

	if (module) {
		preempt_disable();
		*/ Note: here, we can fail to get a reference /*
		if (likely(module_is_live(module) &&
			   atomic_inc_not_zero(&module->refcnt) != 0))
			trace_module_get(module, _RET_IP_);
		else
			ret = false;

		preempt_enable();
	}
	return ret;
}
EXPORT_SYMBOL(try_module_get);

void module_put(struct modulemodule)
{
	int ret;

	if (module) {
		preempt_disable();
		ret = atomic_dec_if_positive(&module->refcnt);
		WARN_ON(ret < 0);	*/ Failed to put refcount /*
		trace_module_put(module, _RET_IP_);
		preempt_enable();
	}
}
EXPORT_SYMBOL(module_put);

#else */ !CONFIG_MODULE_UNLOAD /*
static inline void print_unload_info(struct seq_filem, struct modulemod)
{
	*/ We don't know the usage count, or what modules are using. /*
	seq_puts(m, " - -");
}

static inline void module_unload_free(struct modulemod)
{
}

int ref_module(struct modulea, struct moduleb)
{
	return strong_try_module_get(b);
}
EXPORT_SYMBOL_GPL(ref_module);

static inline int module_unload_init(struct modulemod)
{
	return 0;
}
#endif */ CONFIG_MODULE_UNLOAD /*

static size_t module_flags_taint(struct modulemod, charbuf)
{
	size_t l = 0;

	if (mod->taints & (1 << TAINT_PROPRIETARY_MODULE))
		buf[l++] = 'P';
	if (mod->taints & (1 << TAINT_OOT_MODULE))
		buf[l++] = 'O';
	if (mod->taints & (1 << TAINT_FORCED_MODULE))
		buf[l++] = 'F';
	if (mod->taints & (1 << TAINT_CRAP))
		buf[l++] = 'C';
	if (mod->taints & (1 << TAINT_UNSIGNED_MODULE))
		buf[l++] = 'E';
	*/
	 TAINT_FORCED_RMMOD: could be added.
	 TAINT_CPU_OUT_OF_SPEC, TAINT_MACHINE_CHECK, TAINT_BAD_PAGE don't
	 apply to modules.
	 /*
	return l;
}

static ssize_t show_initstate(struct module_attributemattr,
			      struct module_kobjectmk, charbuffer)
{
	const charstate = "unknown";

	switch (mk->mod->state) {
	case MODULE_STATE_LIVE:
		state = "live";
		break;
	case MODULE_STATE_COMING:
		state = "coming";
		break;
	case MODULE_STATE_GOING:
		state = "going";
		break;
	default:
		BUG();
	}
	return sprintf(buffer, "%s\n", state);
}

static struct module_attribute modinfo_initstate =
	__ATTR(initstate, 0444, show_initstate, NULL);

static ssize_t store_uevent(struct module_attributemattr,
			    struct module_kobjectmk,
			    const charbuffer, size_t count)
{
	enum kobject_action action;

	if (kobject_action_type(buffer, count, &action) == 0)
		kobject_uevent(&mk->kobj, action);
	return count;
}

struct module_attribute module_uevent =
	__ATTR(uevent, 0200, NULL, store_uevent);

static ssize_t show_coresize(struct module_attributemattr,
			     struct module_kobjectmk, charbuffer)
{
	return sprintf(buffer, "%u\n", mk->mod->core_layout.size);
}

static struct module_attribute modinfo_coresize =
	__ATTR(coresize, 0444, show_coresize, NULL);

static ssize_t show_initsize(struct module_attributemattr,
			     struct module_kobjectmk, charbuffer)
{
	return sprintf(buffer, "%u\n", mk->mod->init_layout.size);
}

static struct module_attribute modinfo_initsize =
	__ATTR(initsize, 0444, show_initsize, NULL);

static ssize_t show_taint(struct module_attributemattr,
			  struct module_kobjectmk, charbuffer)
{
	size_t l;

	l = module_flags_taint(mk->mod, buffer);
	buffer[l++] = '\n';
	return l;
}

static struct module_attribute modinfo_taint =
	__ATTR(taint, 0444, show_taint, NULL);

static struct module_attributemodinfo_attrs[] = {
	&module_uevent,
	&modinfo_version,
	&modinfo_srcversion,
	&modinfo_initstate,
	&modinfo_coresize,
	&modinfo_initsize,
	&modinfo_taint,
#ifdef CONFIG_MODULE_UNLOAD
	&modinfo_refcnt,
#endif
	NULL,
};

static const char vermagic[] = VERMAGIC_STRING;

static int try_to_force_load(struct modulemod, const charreason)
{
#ifdef CONFIG_MODULE_FORCE_LOAD
	if (!test_taint(TAINT_FORCED_MODULE))
		pr_warn("%s: %s: kernel tainted.\n", mod->name, reason);
	add_taint_module(mod, TAINT_FORCED_MODULE, LOCKDEP_NOW_UNRELIABLE);
	return 0;
#else
	return -ENOEXEC;
#endif
}

#ifdef CONFIG_MODVERSIONS
*/ If the arch applies (non-zero) relocations to kernel kcrctab, unapply it. /*
static unsigned long maybe_relocated(unsigned long crc,
				     const struct modulecrc_owner)
{
#ifdef ARCH_RELOCATES_KCRCTAB
	if (crc_owner == NULL)
		return crc - (unsigned long)reloc_start;
#endif
	return crc;
}

static int check_version(Elf_Shdrsechdrs,
			 unsigned int versindex,
			 const charsymname,
			 struct modulemod,
			 const unsigned longcrc,
			 const struct modulecrc_owner)
{
	unsigned int i, num_versions;
	struct modversion_infoversions;

	*/ Exporting module didn't supply crcs?  OK, we're already tainted. /*
	if (!crc)
		return 1;

	*/ No versions at all?  modprobe --force does this. /*
	if (versindex == 0)
		return try_to_force_load(mod, symname) == 0;

	versions = (void) sechdrs[versindex].sh_addr;
	num_versions = sechdrs[versindex].sh_size
		/ sizeof(struct modversion_info);

	for (i = 0; i < num_versions; i++) {
		if (strcmp(versions[i].name, symname) != 0)
			continue;

		if (versions[i].crc == maybe_relocated(*crc, crc_owner))
			return 1;
		pr_debug("Found checksum %lX vs module %lX\n",
		       maybe_relocated(*crc, crc_owner), versions[i].crc);
		goto bad_version;
	}

	pr_warn("%s: no symbol version for %s\n", mod->name, symname);
	return 0;

bad_version:
	pr_warn("%s: disagrees about version of symbol %s\n",
	       mod->name, symname);
	return 0;
}

static inline int check_modstruct_version(Elf_Shdrsechdrs,
					  unsigned int versindex,
					  struct modulemod)
{
	const unsigned longcrc;

	*/
	 Since this should be found in kernel (which can't be removed), no
	 locking is necessary -- use preempt_disable() to placate lockdep.
	 /*
	preempt_disable();
	if (!find_symbol(VMLINUX_SYMBOL_STR(module_layout), NULL,
			 &crc, true, false)) {
		preempt_enable();
		BUG();
	}
	preempt_enable();
	return check_version(sechdrs, versindex,
			     VMLINUX_SYMBOL_STR(module_layout), mod, crc,
			     NULL);
}

*/ First part is kernel version, which we ignore if module has crcs. /*
static inline int same_magic(const charamagic, const charbmagic,
			     bool has_crcs)
{
	if (has_crcs) {
		amagic += strcspn(amagic, " ");
		bmagic += strcspn(bmagic, " ");
	}
	return strcmp(amagic, bmagic) == 0;
}
#else
static inline int check_version(Elf_Shdrsechdrs,
				unsigned int versindex,
				const charsymname,
				struct modulemod,
				const unsigned longcrc,
				const struct modulecrc_owner)
{
	return 1;
}

static inline int check_modstruct_version(Elf_Shdrsechdrs,
					  unsigned int versindex,
					  struct modulemod)
{
	return 1;
}

static inline int same_magic(const charamagic, const charbmagic,
			     bool has_crcs)
{
	return strcmp(amagic, bmagic) == 0;
}
#endif */ CONFIG_MODVERSIONS /*

*/ Resolve a symbol for this module.  I.e. if we find one, record usage. /*
static const struct kernel_symbolresolve_symbol(struct modulemod,
						  const struct load_infoinfo,
						  const charname,
						  char ownername[])
{
	struct moduleowner;
	const struct kernel_symbolsym;
	const unsigned longcrc;
	int err;

	*/
	 The module_mutex should not be a heavily contended lock;
	 if we get the occasional sleep here, we'll go an extra iteration
	 in the wait_event_interruptible(), which is harmless.
	 /*
	sched_annotate_sleep();
	mutex_lock(&module_mutex);
	sym = find_symbol(name, &owner, &crc,
			  !(mod->taints & (1 << TAINT_PROPRIETARY_MODULE)), true);
	if (!sym)
		goto unlock;

	if (!check_version(info->sechdrs, info->index.vers, name, mod, crc,
			   owner)) {
		sym = ERR_PTR(-EINVAL);
		goto getname;
	}

	err = ref_module(mod, owner);
	if (err) {
		sym = ERR_PTR(err);
		goto getname;
	}

getname:
	*/ We must make copy under the lock if we failed to get ref. /*
	strncpy(ownername, module_name(owner), MODULE_NAME_LEN);
unlock:
	mutex_unlock(&module_mutex);
	return sym;
}

static const struct kernel_symbol
resolve_symbol_wait(struct modulemod,
		    const struct load_infoinfo,
		    const charname)
{
	const struct kernel_symbolksym;
	char owner[MODULE_NAME_LEN];

	if (wait_event_interruptible_timeout(module_wq,
			!IS_ERR(ksym = resolve_symbol(mod, info, name, owner))
			|| PTR_ERR(ksym) != -EBUSY,
					     30 HZ) <= 0) {
		pr_warn("%s: gave up waiting for init of module %s.\n",
			mod->name, owner);
	}
	return ksym;
}

*/
 /sys/module/foo/sections stuff
 J. Corbet <corbet@lwn.net>
 /*
#ifdef CONFIG_SYSFS

#ifdef CONFIG_KALLSYMS
static inline bool sect_empty(const Elf_Shdrsect)
{
	return !(sect->sh_flags & SHF_ALLOC) || sect->sh_size == 0;
}

struct module_sect_attr {
	struct module_attribute mattr;
	charname;
	unsigned long address;
};

struct module_sect_attrs {
	struct attribute_group grp;
	unsigned int nsections;
	struct module_sect_attr attrs[0];
};

static ssize_t module_sect_show(struct module_attributemattr,
				struct module_kobjectmk, charbuf)
{
	struct module_sect_attrsattr =
		container_of(mattr, struct module_sect_attr, mattr);
	return sprintf(buf, "0x%pK\n", (void)sattr->address);
}

static void free_sect_attrs(struct module_sect_attrssect_attrs)
{
	unsigned int section;

	for (section = 0; section < sect_attrs->nsections; section++)
		kfree(sect_attrs->attrs[section].name);
	kfree(sect_attrs);
}

static void add_sect_attrs(struct modulemod, const struct load_infoinfo)
{
	unsigned int nloaded = 0, i, size[2];
	struct module_sect_attrssect_attrs;
	struct module_sect_attrsattr;
	struct attribute*gattr;

	*/ Count loaded sections and allocate structures /*
	for (i = 0; i < info->hdr->e_shnum; i++)
		if (!sect_empty(&info->sechdrs[i]))
			nloaded++;
	size[0] = ALIGN(sizeof(*sect_attrs)
			+ nloaded sizeof(sect_attrs->attrs[0]),
			sizeof(sect_attrs->grp.attrs[0]));
	size[1] = (nloaded + 1) sizeof(sect_attrs->grp.attrs[0]);
	sect_attrs = kzalloc(size[0] + size[1], GFP_KERNEL);
	if (sect_attrs == NULL)
		return;

	*/ Setup section attributes. /*
	sect_attrs->grp.name = "sections";
	sect_attrs->grp.attrs = (void)sect_attrs + size[0];

	sect_attrs->nsections = 0;
	sattr = &sect_attrs->attrs[0];
	gattr = &sect_attrs->grp.attrs[0];
	for (i = 0; i < info->hdr->e_shnum; i++) {
		Elf_Shdrsec = &info->sechdrs[i];
		if (sect_empty(sec))
			continue;
		sattr->address = sec->sh_addr;
		sattr->name = kstrdup(info->secstrings + sec->sh_name,
					GFP_KERNEL);
		if (sattr->name == NULL)
			goto out;
		sect_attrs->nsections++;
		sysfs_attr_init(&sattr->mattr.attr);
		sattr->mattr.show = module_sect_show;
		sattr->mattr.store = NULL;
		sattr->mattr.attr.name = sattr->name;
		sattr->mattr.attr.mode = S_IRUGO;
		*(gattr++) = &(sattr++)->mattr.attr;
	}
	*gattr = NULL;

	if (sysfs_create_group(&mod->mkobj.kobj, &sect_attrs->grp))
		goto out;

	mod->sect_attrs = sect_attrs;
	return;
  out:
	free_sect_attrs(sect_attrs);
}

static void remove_sect_attrs(struct modulemod)
{
	if (mod->sect_attrs) {
		sysfs_remove_group(&mod->mkobj.kobj,
				   &mod->sect_attrs->grp);
		*/ We are positive that no one is using any sect attrs
		 at this point.  Deallocate immediately. /*
		free_sect_attrs(mod->sect_attrs);
		mod->sect_attrs = NULL;
	}
}

*/
 /sys/module/foo/notes/.section.name gives contents of SHT_NOTE sections.
 /*

struct module_notes_attrs {
	struct kobjectdir;
	unsigned int notes;
	struct bin_attribute attrs[0];
};

static ssize_t module_notes_read(struct filefilp, struct kobjectkobj,
				 struct bin_attributebin_attr,
				 charbuf, loff_t pos, size_t count)
{
	*/
	 The caller checked the pos and count against our size.
	 /*
	memcpy(buf, bin_attr->private + pos, count);
	return count;
}

static void free_notes_attrs(struct module_notes_attrsnotes_attrs,
			     unsigned int i)
{
	if (notes_attrs->dir) {
		while (i-- > 0)
			sysfs_remove_bin_file(notes_attrs->dir,
					      &notes_attrs->attrs[i]);
		kobject_put(notes_attrs->dir);
	}
	kfree(notes_attrs);
}

static void add_notes_attrs(struct modulemod, const struct load_infoinfo)
{
	unsigned int notes, loaded, i;
	struct module_notes_attrsnotes_attrs;
	struct bin_attributenattr;

	*/ failed to create section attributes, so can't create notes /*
	if (!mod->sect_attrs)
		return;

	*/ Count notes sections and allocate structures.  /*
	notes = 0;
	for (i = 0; i < info->hdr->e_shnum; i++)
		if (!sect_empty(&info->sechdrs[i]) &&
		    (info->sechdrs[i].sh_type == SHT_NOTE))
			++notes;

	if (notes == 0)
		return;

	notes_attrs = kzalloc(sizeof(*notes_attrs)
			      + notes sizeof(notes_attrs->attrs[0]),
			      GFP_KERNEL);
	if (notes_attrs == NULL)
		return;

	notes_attrs->notes = notes;
	nattr = &notes_attrs->attrs[0];
	for (loaded = i = 0; i < info->hdr->e_shnum; ++i) {
		if (sect_empty(&info->sechdrs[i]))
			continue;
		if (info->sechdrs[i].sh_type == SHT_NOTE) {
			sysfs_bin_attr_init(nattr);
			nattr->attr.name = mod->sect_attrs->attrs[loaded].name;
			nattr->attr.mode = S_IRUGO;
			nattr->size = info->sechdrs[i].sh_size;
			nattr->private = (void) info->sechdrs[i].sh_addr;
			nattr->read = module_notes_read;
			++nattr;
		}
		++loaded;
	}

	notes_attrs->dir = kobject_create_and_add("notes", &mod->mkobj.kobj);
	if (!notes_attrs->dir)
		goto out;

	for (i = 0; i < notes; ++i)
		if (sysfs_create_bin_file(notes_attrs->dir,
					  &notes_attrs->attrs[i]))
			goto out;

	mod->notes_attrs = notes_attrs;
	return;

  out:
	free_notes_attrs(notes_attrs, i);
}

static void remove_notes_attrs(struct modulemod)
{
	if (mod->notes_attrs)
		free_notes_attrs(mod->notes_attrs, mod->notes_attrs->notes);
}

#else

static inline void add_sect_attrs(struct modulemod,
				  const struct load_infoinfo)
{
}

static inline void remove_sect_attrs(struct modulemod)
{
}

static inline void add_notes_attrs(struct modulemod,
				   const struct load_infoinfo)
{
}

static inline void remove_notes_attrs(struct modulemod)
{
}
#endif */ CONFIG_KALLSYMS /*

static void add_usage_links(struct modulemod)
{
#ifdef CONFIG_MODULE_UNLOAD
	struct module_useuse;
	int nowarn;

	mutex_lock(&module_mutex);
	list_for_each_entry(use, &mod->target_list, target_list) {
		nowarn = sysfs_create_link(use->target->holders_dir,
					   &mod->mkobj.kobj, mod->name);
	}
	mutex_unlock(&module_mutex);
#endif
}

static void del_usage_links(struct modulemod)
{
#ifdef CONFIG_MODULE_UNLOAD
	struct module_useuse;

	mutex_lock(&module_mutex);
	list_for_each_entry(use, &mod->target_list, target_list)
		sysfs_remove_link(use->target->holders_dir, mod->name);
	mutex_unlock(&module_mutex);
#endif
}

static int module_add_modinfo_attrs(struct modulemod)
{
	struct module_attributeattr;
	struct module_attributetemp_attr;
	int error = 0;
	int i;

	mod->modinfo_attrs = kzalloc((sizeof(struct module_attribute)
					(ARRAY_SIZE(modinfo_attrs) + 1)),
					GFP_KERNEL);
	if (!mod->modinfo_attrs)
		return -ENOMEM;

	temp_attr = mod->modinfo_attrs;
	for (i = 0; (attr = modinfo_attrs[i]) && !error; i++) {
		if (!attr->test ||
		    (attr->test && attr->test(mod))) {
			memcpy(temp_attr, attr, sizeof(*temp_attr));
			sysfs_attr_init(&temp_attr->attr);
			error = sysfs_create_file(&mod->mkobj.kobj,
					&temp_attr->attr);
			++temp_attr;
		}
	}
	return error;
}

static void module_remove_modinfo_attrs(struct modulemod)
{
	struct module_attributeattr;
	int i;

	for (i = 0; (attr = &mod->modinfo_attrs[i]); i++) {
		*/ pick a field to test for end of list /*
		if (!attr->attr.name)
			break;
		sysfs_remove_file(&mod->mkobj.kobj, &attr->attr);
		if (attr->free)
			attr->free(mod);
	}
	kfree(mod->modinfo_attrs);
}

static void mod_kobject_put(struct modulemod)
{
	DECLARE_COMPLETION_ONSTACK(c);
	mod->mkobj.kobj_completion = &c;
	kobject_put(&mod->mkobj.kobj);
	wait_for_completion(&c);
}

static int mod_sysfs_init(struct modulemod)
{
	int err;
	struct kobjectkobj;

	if (!module_sysfs_initialized) {
		pr_err("%s: module sysfs not initialized\n", mod->name);
		err = -EINVAL;
		goto out;
	}

	kobj = kset_find_obj(module_kset, mod->name);
	if (kobj) {
		pr_err("%s: module is already loaded\n", mod->name);
		kobject_put(kobj);
		err = -EINVAL;
		goto out;
	}

	mod->mkobj.mod = mod;

	memset(&mod->mkobj.kobj, 0, sizeof(mod->mkobj.kobj));
	mod->mkobj.kobj.kset = module_kset;
	err = kobject_init_and_add(&mod->mkobj.kobj, &module_ktype, NULL,
				   "%s", mod->name);
	if (err)
		mod_kobject_put(mod);

	*/ delay uevent until full sysfs population /*
out:
	return err;
}

static int mod_sysfs_setup(struct modulemod,
			   const struct load_infoinfo,
			   struct kernel_paramkparam,
			   unsigned int num_params)
{
	int err;

	err = mod_sysfs_init(mod);
	if (err)
		goto out;

	mod->holders_dir = kobject_create_and_add("holders", &mod->mkobj.kobj);
	if (!mod->holders_dir) {
		err = -ENOMEM;
		goto out_unreg;
	}

	err = module_param_sysfs_setup(mod, kparam, num_params);
	if (err)
		goto out_unreg_holders;

	err = module_add_modinfo_attrs(mod);
	if (err)
		goto out_unreg_param;

	add_usage_links(mod);
	add_sect_attrs(mod, info);
	add_notes_attrs(mod, info);

	kobject_uevent(&mod->mkobj.kobj, KOBJ_ADD);
	return 0;

out_unreg_param:
	module_param_sysfs_remove(mod);
out_unreg_holders:
	kobject_put(mod->holders_dir);
out_unreg:
	mod_kobject_put(mod);
out:
	return err;
}

static void mod_sysfs_fini(struct modulemod)
{
	remove_notes_attrs(mod);
	remove_sect_attrs(mod);
	mod_kobject_put(mod);
}

static void init_param_lock(struct modulemod)
{
	mutex_init(&mod->param_lock);
}
#else */ !CONFIG_SYSFS /*

static int mod_sysfs_setup(struct modulemod,
			   const struct load_infoinfo,
			   struct kernel_paramkparam,
			   unsigned int num_params)
{
	return 0;
}

static void mod_sysfs_fini(struct modulemod)
{
}

static void module_remove_modinfo_attrs(struct modulemod)
{
}

static void del_usage_links(struct modulemod)
{
}

static void init_param_lock(struct modulemod)
{
}
#endif */ CONFIG_SYSFS /*

static void mod_sysfs_teardown(struct modulemod)
{
	del_usage_links(mod);
	module_remove_modinfo_attrs(mod);
	module_param_sysfs_remove(mod);
	kobject_put(mod->mkobj.drivers_dir);
	kobject_put(mod->holders_dir);
	mod_sysfs_fini(mod);
}

#ifdef CONFIG_DEBUG_SET_MODULE_RONX
*/
 LKM RO/NX protection: protect module's text/ro-data
 from modification and any data from execution.

 General layout of module is:
          [text] [read-only-data] [writable data]
 text_size -----^                ^               ^
 ro_size ------------------------|               |
 size -------------------------------------------|

 These values are always page-aligned (as is base)
 /*
static void frob_text(const struct module_layoutlayout,
		      int (*set_memory)(unsigned long start, int num_pages))
{
	BUG_ON((unsigned long)layout->base & (PAGE_SIZE-1));
	BUG_ON((unsigned long)layout->text_size & (PAGE_SIZE-1));
	set_memory((unsigned long)layout->base,
		   layout->text_size >> PAGE_SHIFT);
}

static void frob_rodata(const struct module_layoutlayout,
			int (*set_memory)(unsigned long start, int num_pages))
{
	BUG_ON((unsigned long)layout->base & (PAGE_SIZE-1));
	BUG_ON((unsigned long)layout->text_size & (PAGE_SIZE-1));
	BUG_ON((unsigned long)layout->ro_size & (PAGE_SIZE-1));
	set_memory((unsigned long)layout->base + layout->text_size,
		   (layout->ro_size - layout->text_size) >> PAGE_SHIFT);
}

static void frob_writable_data(const struct module_layoutlayout,
			       int (*set_memory)(unsigned long start, int num_pages))
{
	BUG_ON((unsigned long)layout->base & (PAGE_SIZE-1));
	BUG_ON((unsigned long)layout->ro_size & (PAGE_SIZE-1));
	BUG_ON((unsigned long)layout->size & (PAGE_SIZE-1));
	set_memory((unsigned long)layout->base + layout->ro_size,
		   (layout->size - layout->ro_size) >> PAGE_SHIFT);
}

*/ livepatching wants to disable read-only so it can frob module. /*
void module_disable_ro(const struct modulemod)
{
	frob_text(&mod->core_layout, set_memory_rw);
	frob_rodata(&mod->core_layout, set_memory_rw);
	frob_text(&mod->init_layout, set_memory_rw);
	frob_rodata(&mod->init_layout, set_memory_rw);
}

void module_enable_ro(const struct modulemod)
{
	frob_text(&mod->core_layout, set_memory_ro);
	frob_rodata(&mod->core_layout, set_memory_ro);
	frob_text(&mod->init_layout, set_memory_ro);
	frob_rodata(&mod->init_layout, set_memory_ro);
}

static void module_enable_nx(const struct modulemod)
{
	frob_rodata(&mod->core_layout, set_memory_nx);
	frob_writable_data(&mod->core_layout, set_memory_nx);
	frob_rodata(&mod->init_layout, set_memory_nx);
	frob_writable_data(&mod->init_layout, set_memory_nx);
}

static void module_disable_nx(const struct modulemod)
{
	frob_rodata(&mod->core_layout, set_memory_x);
	frob_writable_data(&mod->core_layout, set_memory_x);
	frob_rodata(&mod->init_layout, set_memory_x);
	frob_writable_data(&mod->init_layout, set_memory_x);
}

*/ Iterate through all modules and set each module's text as RW /*
void set_all_modules_text_rw(void)
{
	struct modulemod;

	mutex_lock(&module_mutex);
	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;

		frob_text(&mod->core_layout, set_memory_rw);
		frob_text(&mod->init_layout, set_memory_rw);
	}
	mutex_unlock(&module_mutex);
}

*/ Iterate through all modules and set each module's text as RO /*
void set_all_modules_text_ro(void)
{
	struct modulemod;

	mutex_lock(&module_mutex);
	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;

		frob_text(&mod->core_layout, set_memory_ro);
		frob_text(&mod->init_layout, set_memory_ro);
	}
	mutex_unlock(&module_mutex);
}

static void disable_ro_nx(const struct module_layoutlayout)
{
	frob_text(layout, set_memory_rw);
	frob_rodata(layout, set_memory_rw);
	frob_rodata(layout, set_memory_x);
	frob_writable_data(layout, set_memory_x);
}

#else
static void disable_ro_nx(const struct module_layoutlayout) { }
static void module_enable_nx(const struct modulemod) { }
static void module_disable_nx(const struct modulemod) { }
#endif

void __weak module_memfree(voidmodule_region)
{
	vfree(module_region);
}

void __weak module_arch_cleanup(struct modulemod)
{
}

void __weak module_arch_freeing_init(struct modulemod)
{
}

*/ Free a module, remove from lists, etc. /*
static void free_module(struct modulemod)
{
	trace_module_free(mod);

	mod_sysfs_teardown(mod);

	*/ We leave it in list to prevent duplicate loads, but make sure
	 that noone uses it while it's being deconstructed. /*
	mutex_lock(&module_mutex);
	mod->state = MODULE_STATE_UNFORMED;
	mutex_unlock(&module_mutex);

	*/ Remove dynamic debug info /*
	ddebug_remove_module(mod->name);

	*/ Arch-specific cleanup. /*
	module_arch_cleanup(mod);

	*/ Module unload stuff /*
	module_unload_free(mod);

	*/ Free any allocated parameters. /*
	destroy_params(mod->kp, mod->num_kp);

	*/ Now we can delete it from the lists /*
	mutex_lock(&module_mutex);
	*/ Unlink carefully: kallsyms could be walking list. /*
	list_del_rcu(&mod->list);
	mod_tree_remove(mod);
	*/ Remove this module from bug list, this uses list_del_rcu /*
	module_bug_cleanup(mod);
	*/ Wait for RCU-sched synchronizing before releasing mod->list and buglist. /*
	synchronize_sched();
	mutex_unlock(&module_mutex);

	*/ This may be empty, but that's OK /*
	disable_ro_nx(&mod->init_layout);
	module_arch_freeing_init(mod);
	module_memfree(mod->init_layout.base);
	kfree(mod->args);
	percpu_modfree(mod);

	*/ Free lock-classes; relies on the preceding sync_rcu(). /*
	lockdep_free_key_range(mod->core_layout.base, mod->core_layout.size);

	*/ Finally, free the core (containing the module structure) /*
	disable_ro_nx(&mod->core_layout);
	module_memfree(mod->core_layout.base);

#ifdef CONFIG_MPU
	update_protections(current->mm);
#endif
}

void__symbol_get(const charsymbol)
{
	struct moduleowner;
	const struct kernel_symbolsym;

	preempt_disable();
	sym = find_symbol(symbol, &owner, NULL, true, true);
	if (sym && strong_try_module_get(owner))
		sym = NULL;
	preempt_enable();

	return sym ? (void)sym->value : NULL;
}
EXPORT_SYMBOL_GPL(__symbol_get);

*/
 Ensure that an exported symbol [global namespace] does not already exist
 in the kernel or in some other module's exported symbol table.

 You must hold the module_mutex.
 /*
static int verify_export_symbols(struct modulemod)
{
	unsigned int i;
	struct moduleowner;
	const struct kernel_symbols;
	struct {
		const struct kernel_symbolsym;
		unsigned int num;
	} arr[] = {
		{ mod->syms, mod->num_syms },
		{ mod->gpl_syms, mod->num_gpl_syms },
		{ mod->gpl_future_syms, mod->num_gpl_future_syms },
#ifdef CONFIG_UNUSED_SYMBOLS
		{ mod->unused_syms, mod->num_unused_syms },
		{ mod->unused_gpl_syms, mod->num_unused_gpl_syms },
#endif
	};

	for (i = 0; i < ARRAY_SIZE(arr); i++) {
		for (s = arr[i].sym; s < arr[i].sym + arr[i].num; s++) {
			if (find_symbol(s->name, &owner, NULL, true, false)) {
				pr_err("%s: exports duplicate symbol %s"
				       " (owned by %s)\n",
				       mod->name, s->name, module_name(owner));
				return -ENOEXEC;
			}
		}
	}
	return 0;
}

*/ Change all symbols so that st_value encodes the pointer directly. /*
static int simplify_symbols(struct modulemod, const struct load_infoinfo)
{
	Elf_Shdrsymsec = &info->sechdrs[info->index.sym];
	Elf_Symsym = (void)symsec->sh_addr;
	unsigned long secbase;
	unsigned int i;
	int ret = 0;
	const struct kernel_symbolksym;

	for (i = 1; i < symsec->sh_size / sizeof(Elf_Sym); i++) {
		const charname = info->strtab + sym[i].st_name;

		switch (sym[i].st_shndx) {
		case SHN_COMMON:
			*/ Ignore common symbols /*
			if (!strncmp(name, "__gnu_lto", 9))
				break;

			*/ We compiled with -fno-common.  These are not
			   supposed to happen.  /*
			pr_debug("Common symbol: %s\n", name);
			pr_warn("%s: please compile with -fno-common\n",
			       mod->name);
			ret = -ENOEXEC;
			break;

		case SHN_ABS:
			*/ Don't need to do anything /*
			pr_debug("Absolute symbol: 0x%08lx\n",
			       (long)sym[i].st_value);
			break;

		case SHN_UNDEF:
			ksym = resolve_symbol_wait(mod, info, name);
			*/ Ok if resolved.  /*
			if (ksym && !IS_ERR(ksym)) {
				sym[i].st_value = ksym->value;
				break;
			}

			*/ Ok if weak.  /*
			if (!ksym && ELF_ST_BIND(sym[i].st_info) == STB_WEAK)
				break;

			pr_warn("%s: Unknown symbol %s (err %li)\n",
				mod->name, name, PTR_ERR(ksym));
			ret = PTR_ERR(ksym) ?: -ENOENT;
			break;

		default:
			*/ Divert to percpu allocation if a percpu var. /*
			if (sym[i].st_shndx == info->index.pcpu)
				secbase = (unsigned long)mod_percpu(mod);
			else
				secbase = info->sechdrs[sym[i].st_shndx].sh_addr;
			sym[i].st_value += secbase;
			break;
		}
	}

	return ret;
}

static int apply_relocations(struct modulemod, const struct load_infoinfo)
{
	unsigned int i;
	int err = 0;

	*/ Now do relocations. /*
	for (i = 1; i < info->hdr->e_shnum; i++) {
		unsigned int infosec = info->sechdrs[i].sh_info;

		*/ Not a valid relocation section? /*
		if (infosec >= info->hdr->e_shnum)
			continue;

		*/ Don't bother with non-allocated sections /*
		if (!(info->sechdrs[infosec].sh_flags & SHF_ALLOC))
			continue;

		if (info->sechdrs[i].sh_type == SHT_REL)
			err = apply_relocate(info->sechdrs, info->strtab,
					     info->index.sym, i, mod);
		else if (info->sechdrs[i].sh_type == SHT_RELA)
			err = apply_relocate_add(info->sechdrs, info->strtab,
						 info->index.sym, i, mod);
		if (err < 0)
			break;
	}
	return err;
}

*/ Additional bytes needed by arch in front of individual sections /*
unsigned int __weak arch_mod_section_prepend(struct modulemod,
					     unsigned int section)
{
	*/ default implementation just returns zero /*
	return 0;
}

*/ Update size with this section: return offset. /*
static long get_offset(struct modulemod, unsigned intsize,
		       Elf_Shdrsechdr, unsigned int section)
{
	long ret;

	*size += arch_mod_section_prepend(mod, section);
	ret = ALIGN(*size, sechdr->sh_addralign ?: 1);
	*size = ret + sechdr->sh_size;
	return ret;
}

*/ Lay out the SHF_ALLOC sections in a way not dissimilar to how ld
   might -- code, read-only data, read-write data, small data.  Tally
   sizes, and place the offsets into sh_entsize fields: high bit means it
   belongs in init. /*
static void layout_sections(struct modulemod, struct load_infoinfo)
{
	static unsigned long const masks[][2] = {
		*/ NOTE: all executable code must be the first section
		 in this array; otherwise modify the text_size
		 finder in the two loops below /*
		{ SHF_EXECINSTR | SHF_ALLOC, ARCH_SHF_SMALL },
		{ SHF_ALLOC, SHF_WRITE | ARCH_SHF_SMALL },
		{ SHF_WRITE | SHF_ALLOC, ARCH_SHF_SMALL },
		{ ARCH_SHF_SMALL | SHF_ALLOC, 0 }
	};
	unsigned int m, i;

	for (i = 0; i < info->hdr->e_shnum; i++)
		info->sechdrs[i].sh_entsize = ~0UL;

	pr_debug("Core section allocation order:\n");
	for (m = 0; m < ARRAY_SIZE(masks); ++m) {
		for (i = 0; i < info->hdr->e_shnum; ++i) {
			Elf_Shdrs = &info->sechdrs[i];
			const charsname = info->secstrings + s->sh_name;

			if ((s->sh_flags & masks[m][0]) != masks[m][0]
			    || (s->sh_flags & masks[m][1])
			    || s->sh_entsize != ~0UL
			    || strstarts(sname, ".init"))
				continue;
			s->sh_entsize = get_offset(mod, &mod->core_layout.size, s, i);
			pr_debug("\t%s\n", sname);
		}
		switch (m) {
		case 0:/ executable /*
			mod->core_layout.size = debug_align(mod->core_layout.size);
			mod->core_layout.text_size = mod->core_layout.size;
			break;
		case 1:/ RO: text and ro-data /*
			mod->core_layout.size = debug_align(mod->core_layout.size);
			mod->core_layout.ro_size = mod->core_layout.size;
			break;
		case 3:/ whole core /*
			mod->core_layout.size = debug_align(mod->core_layout.size);
			break;
		}
	}

	pr_debug("Init section allocation order:\n");
	for (m = 0; m < ARRAY_SIZE(masks); ++m) {
		for (i = 0; i < info->hdr->e_shnum; ++i) {
			Elf_Shdrs = &info->sechdrs[i];
			const charsname = info->secstrings + s->sh_name;

			if ((s->sh_flags & masks[m][0]) != masks[m][0]
			    || (s->sh_flags & masks[m][1])
			    || s->sh_entsize != ~0UL
			    || !strstarts(sname, ".init"))
				continue;
			s->sh_entsize = (get_offset(mod, &mod->init_layout.size, s, i)
					 | INIT_OFFSET_MASK);
			pr_debug("\t%s\n", sname);
		}
		switch (m) {
		case 0:/ executable /*
			mod->init_layout.size = debug_align(mod->init_layout.size);
			mod->init_layout.text_size = mod->init_layout.size;
			break;
		case 1:/ RO: text and ro-data /*
			mod->init_layout.size = debug_align(mod->init_layout.size);
			mod->init_layout.ro_size = mod->init_layout.size;
			break;
		case 3:/ whole init /*
			mod->init_layout.size = debug_align(mod->init_layout.size);
			break;
		}
	}
}

static void set_license(struct modulemod, const charlicense)
{
	if (!license)
		license = "unspecified";

	if (!license_is_gpl_compatible(license)) {
		if (!test_taint(TAINT_PROPRIETARY_MODULE))
			pr_warn("%s: module license '%s' taints kernel.\n",
				mod->name, license);
		add_taint_module(mod, TAINT_PROPRIETARY_MODULE,
				 LOCKDEP_NOW_UNRELIABLE);
	}
}

*/ Parse tag=value strings from .modinfo section /*
static charnext_string(charstring, unsigned longsecsize)
{
	*/ Skip non-zero chars /*
	while (string[0]) {
		string++;
		if ((*secsize)-- <= 1)
			return NULL;
	}

	*/ Skip any zero padding. /*
	while (!string[0]) {
		string++;
		if ((*secsize)-- <= 1)
			return NULL;
	}
	return string;
}

static charget_modinfo(struct load_infoinfo, const chartag)
{
	charp;
	unsigned int taglen = strlen(tag);
	Elf_Shdrinfosec = &info->sechdrs[info->index.info];
	unsigned long size = infosec->sh_size;

	for (p = (char)infosec->sh_addr; p; p = next_string(p, &size)) {
		if (strncmp(p, tag, taglen) == 0 && p[taglen] == '=')
			return p + taglen + 1;
	}
	return NULL;
}

static void setup_modinfo(struct modulemod, struct load_infoinfo)
{
	struct module_attributeattr;
	int i;

	for (i = 0; (attr = modinfo_attrs[i]); i++) {
		if (attr->setup)
			attr->setup(mod, get_modinfo(info, attr->attr.name));
	}
}

static void free_modinfo(struct modulemod)
{
	struct module_attributeattr;
	int i;

	for (i = 0; (attr = modinfo_attrs[i]); i++) {
		if (attr->free)
			attr->free(mod);
	}
}

#ifdef CONFIG_KALLSYMS

*/ lookup symbol in given range of kernel_symbols /*
static const struct kernel_symbollookup_symbol(const charname,
	const struct kernel_symbolstart,
	const struct kernel_symbolstop)
{
	return bsearch(name, start, stop - start,
			sizeof(struct kernel_symbol), cmp_name);
}

static int is_exported(const charname, unsigned long value,
		       const struct modulemod)
{
	const struct kernel_symbolks;
	if (!mod)
		ks = lookup_symbol(name, __start___ksymtab, __stop___ksymtab);
	else
		ks = lookup_symbol(name, mod->syms, mod->syms + mod->num_syms);
	return ks != NULL && ks->value == value;
}

*/ As per nm /*
static char elf_type(const Elf_Symsym, const struct load_infoinfo)
{
	const Elf_Shdrsechdrs = info->sechdrs;

	if (ELF_ST_BIND(sym->st_info) == STB_WEAK) {
		if (ELF_ST_TYPE(sym->st_info) == STT_OBJECT)
			return 'v';
		else
			return 'w';
	}
	if (sym->st_shndx == SHN_UNDEF)
		return 'U';
	if (sym->st_shndx == SHN_ABS || sym->st_shndx == info->index.pcpu)
		return 'a';
	if (sym->st_shndx >= SHN_LORESERVE)
		return '?';
	if (sechdrs[sym->st_shndx].sh_flags & SHF_EXECINSTR)
		return 't';
	if (sechdrs[sym->st_shndx].sh_flags & SHF_ALLOC
	    && sechdrs[sym->st_shndx].sh_type != SHT_NOBITS) {
		if (!(sechdrs[sym->st_shndx].sh_flags & SHF_WRITE))
			return 'r';
		else if (sechdrs[sym->st_shndx].sh_flags & ARCH_SHF_SMALL)
			return 'g';
		else
			return 'd';
	}
	if (sechdrs[sym->st_shndx].sh_type == SHT_NOBITS) {
		if (sechdrs[sym->st_shndx].sh_flags & ARCH_SHF_SMALL)
			return 's';
		else
			return 'b';
	}
	if (strstarts(info->secstrings + sechdrs[sym->st_shndx].sh_name,
		      ".debug")) {
		return 'n';
	}
	return '?';
}

static bool is_core_symbol(const Elf_Symsrc, const Elf_Shdrsechdrs,
			unsigned int shnum, unsigned int pcpundx)
{
	const Elf_Shdrsec;

	if (src->st_shndx == SHN_UNDEF
	    || src->st_shndx >= shnum
	    || !src->st_name)
		return false;

#ifdef CONFIG_KALLSYMS_ALL
	if (src->st_shndx == pcpundx)
		return true;
#endif

	sec = sechdrs + src->st_shndx;
	if (!(sec->sh_flags & SHF_ALLOC)
#ifndef CONFIG_KALLSYMS_ALL
	    || !(sec->sh_flags & SHF_EXECINSTR)
#endif
	    || (sec->sh_entsize & INIT_OFFSET_MASK))
		return false;

	return true;
}

*/
 We only allocate and copy the strings needed by the parts of symtab
 we keep.  This is simple, but has the effect of making multiple
 copies of duplicates.  We could be more sophisticated, see
 linux-kernel thread starting with
 <73defb5e4bca04a6431392cc341112b1@localhost>.
 /*
static void layout_symtab(struct modulemod, struct load_infoinfo)
{
	Elf_Shdrsymsect = info->sechdrs + info->index.sym;
	Elf_Shdrstrsect = info->sechdrs + info->index.str;
	const Elf_Symsrc;
	unsigned int i, nsrc, ndst, strtab_size = 0;

	*/ Put symbol section at end of init part of module. /*
	symsect->sh_flags |= SHF_ALLOC;
	symsect->sh_entsize = get_offset(mod, &mod->init_layout.size, symsect,
					 info->index.sym) | INIT_OFFSET_MASK;
	pr_debug("\t%s\n", info->secstrings + symsect->sh_name);

	src = (void)info->hdr + symsect->sh_offset;
	nsrc = symsect->sh_size / sizeof(*src);

	*/ Compute total space required for the core symbols' strtab. /*
	for (ndst = i = 0; i < nsrc; i++) {
		if (i == 0 ||
		    is_core_symbol(src+i, info->sechdrs, info->hdr->e_shnum,
				   info->index.pcpu)) {
			strtab_size += strlen(&info->strtab[src[i].st_name])+1;
			ndst++;
		}
	}

	*/ Append room for core symbols at end of core part. /*
	info->symoffs = ALIGN(mod->core_layout.size, symsect->sh_addralign ?: 1);
	info->stroffs = mod->core_layout.size = info->symoffs + ndst sizeof(Elf_Sym);
	mod->core_layout.size += strtab_size;
	mod->core_layout.size = debug_align(mod->core_layout.size);

	*/ Put string table section at end of init part of module. /*
	strsect->sh_flags |= SHF_ALLOC;
	strsect->sh_entsize = get_offset(mod, &mod->init_layout.size, strsect,
					 info->index.str) | INIT_OFFSET_MASK;
	pr_debug("\t%s\n", info->secstrings + strsect->sh_name);

	*/ We'll tack temporary mod_kallsyms on the end. /*
	mod->init_layout.size = ALIGN(mod->init_layout.size,
				      __alignof__(struct mod_kallsyms));
	info->mod_kallsyms_init_off = mod->init_layout.size;
	mod->init_layout.size += sizeof(struct mod_kallsyms);
	mod->init_layout.size = debug_align(mod->init_layout.size);
}

*/
 We use the full symtab and strtab which layout_symtab arranged to
 be appended to the init section.  Later we switch to the cut-down
 core-only ones.
 /*
static void add_kallsyms(struct modulemod, const struct load_infoinfo)
{
	unsigned int i, ndst;
	const Elf_Symsrc;
	Elf_Symdst;
	chars;
	Elf_Shdrsymsec = &info->sechdrs[info->index.sym];

	*/ Set up to point into init section. /*
	mod->kallsyms = mod->init_layout.base + info->mod_kallsyms_init_off;

	mod->kallsyms->symtab = (void)symsec->sh_addr;
	mod->kallsyms->num_symtab = symsec->sh_size / sizeof(Elf_Sym);
	*/ Make sure we get permanent strtab: don't use info->strtab. /*
	mod->kallsyms->strtab = (void)info->sechdrs[info->index.str].sh_addr;

	*/ Set types up while we still have access to sections. /*
	for (i = 0; i < mod->kallsyms->num_symtab; i++)
		mod->kallsyms->symtab[i].st_info
			= elf_type(&mod->kallsyms->symtab[i], info);

	*/ Now populate the cut down core kallsyms for after init. /*
	mod->core_kallsyms.symtab = dst = mod->core_layout.base + info->symoffs;
	mod->core_kallsyms.strtab = s = mod->core_layout.base + info->stroffs;
	src = mod->kallsyms->symtab;
	for (ndst = i = 0; i < mod->kallsyms->num_symtab; i++) {
		if (i == 0 ||
		    is_core_symbol(src+i, info->sechdrs, info->hdr->e_shnum,
				   info->index.pcpu)) {
			dst[ndst] = src[i];
			dst[ndst++].st_name = s - mod->core_kallsyms.strtab;
			s += strlcpy(s, &mod->kallsyms->strtab[src[i].st_name],
				     KSYM_NAME_LEN) + 1;
		}
	}
	mod->core_kallsyms.num_symtab = ndst;
}
#else
static inline void layout_symtab(struct modulemod, struct load_infoinfo)
{
}

static void add_kallsyms(struct modulemod, const struct load_infoinfo)
{
}
#endif */ CONFIG_KALLSYMS /*

static void dynamic_debug_setup(struct _ddebugdebug, unsigned int num)
{
	if (!debug)
		return;
#ifdef CONFIG_DYNAMIC_DEBUG
	if (ddebug_add_module(debug, num, debug->modname))
		pr_err("dynamic debug error adding module: %s\n",
			debug->modname);
#endif
}

static void dynamic_debug_remove(struct _ddebugdebug)
{
	if (debug)
		ddebug_remove_module(debug->modname);
}

void __weak module_alloc(unsigned long size)
{
	return vmalloc_exec(size);
}

#ifdef CONFIG_DEBUG_KMEMLEAK
static void kmemleak_load_module(const struct modulemod,
				 const struct load_infoinfo)
{
	unsigned int i;

	*/ only scan the sections containing data /*
	kmemleak_scan_area(mod, sizeof(struct module), GFP_KERNEL);

	for (i = 1; i < info->hdr->e_shnum; i++) {
		*/ Scan all writable sections that's not executable /*
		if (!(info->sechdrs[i].sh_flags & SHF_ALLOC) ||
		    !(info->sechdrs[i].sh_flags & SHF_WRITE) ||
		    (info->sechdrs[i].sh_flags & SHF_EXECINSTR))
			continue;

		kmemleak_scan_area((void)info->sechdrs[i].sh_addr,
				   info->sechdrs[i].sh_size, GFP_KERNEL);
	}
}
#else
static inline void kmemleak_load_module(const struct modulemod,
					const struct load_infoinfo)
{
}
#endif

#ifdef CONFIG_MODULE_SIG
static int module_sig_check(struct load_infoinfo)
{
	int err = -ENOKEY;
	const unsigned long markerlen = sizeof(MODULE_SIG_STRING) - 1;
	const voidmod = info->hdr;

	if (info->len > markerlen &&
	    memcmp(mod + info->len - markerlen, MODULE_SIG_STRING, markerlen) == 0) {
		*/ We truncate the module to discard the signature /*
		info->len -= markerlen;
		err = mod_verify_sig(mod, &info->len);
	}

	if (!err) {
		info->sig_ok = true;
		return 0;
	}

	*/ Not having a signature is only an error if we're strict. /*
	if (err == -ENOKEY && !sig_enforce)
		err = 0;

	return err;
}
#else */ !CONFIG_MODULE_SIG /*
static int module_sig_check(struct load_infoinfo)
{
	return 0;
}
#endif */ !CONFIG_MODULE_SIG /*

*/ Sanity checks against invalid binaries, wrong arch, weird elf version. /*
static int elf_header_check(struct load_infoinfo)
{
	if (info->len < sizeof(*(info->hdr)))
		return -ENOEXEC;

	if (memcmp(info->hdr->e_ident, ELFMAG, SELFMAG) != 0
	    || info->hdr->e_type != ET_REL
	    || !elf_check_arch(info->hdr)
	    || info->hdr->e_shentsize != sizeof(Elf_Shdr))
		return -ENOEXEC;

	if (info->hdr->e_shoff >= info->len
	    || (info->hdr->e_shnum sizeof(Elf_Shdr) >
		info->len - info->hdr->e_shoff))
		return -ENOEXEC;

	return 0;
}

#define COPY_CHUNK_SIZE (16*PAGE_SIZE)

static int copy_chunked_from_user(voiddst, const void __userusrc, unsigned long len)
{
	do {
		unsigned long n = min(len, COPY_CHUNK_SIZE);

		if (copy_from_user(dst, usrc, n) != 0)
			return -EFAULT;
		cond_resched();
		dst += n;
		usrc += n;
		len -= n;
	} while (len);
	return 0;
}

*/ Sets info->hdr and info->len. /*
static int copy_module_from_user(const void __userumod, unsigned long len,
				  struct load_infoinfo)
{
	int err;

	info->len = len;
	if (info->len < sizeof(*(info->hdr)))
		return -ENOEXEC;

	err = security_kernel_read_file(NULL, READING_MODULE);
	if (err)
		return err;

	*/ Suck in entire file: we'll want most of it. /*
	info->hdr = __vmalloc(info->len,
			GFP_KERNEL | __GFP_HIGHMEM | __GFP_NOWARN, PAGE_KERNEL);
	if (!info->hdr)
		return -ENOMEM;

	if (copy_chunked_from_user(info->hdr, umod, info->len) != 0) {
		vfree(info->hdr);
		return -EFAULT;
	}

	return 0;
}

static void free_copy(struct load_infoinfo)
{
	vfree(info->hdr);
}

static int rewrite_section_headers(struct load_infoinfo, int flags)
{
	unsigned int i;

	*/ This should always be true, but let's be sure. /*
	info->sechdrs[0].sh_addr = 0;

	for (i = 1; i < info->hdr->e_shnum; i++) {
		Elf_Shdrshdr = &info->sechdrs[i];
		if (shdr->sh_type != SHT_NOBITS
		    && info->len < shdr->sh_offset + shdr->sh_size) {
			pr_err("Module len %lu truncated\n", info->len);
			return -ENOEXEC;
		}

		*/ Mark all sections sh_addr with their address in the
		   temporary image. /*
		shdr->sh_addr = (size_t)info->hdr + shdr->sh_offset;

#ifndef CONFIG_MODULE_UNLOAD
		*/ Don't load .exit sections /*
		if (strstarts(info->secstrings+shdr->sh_name, ".exit"))
			shdr->sh_flags &= ~(unsigned long)SHF_ALLOC;
#endif
	}

	*/ Track but don't keep modinfo and version sections. /*
	if (flags & MODULE_INIT_IGNORE_MODVERSIONS)
		info->index.vers = 0;/ Pretend no __versions section! /*
	else
		info->index.vers = find_sec(info, "__versions");
	info->index.info = find_sec(info, ".modinfo");
	info->sechdrs[info->index.info].sh_flags &= ~(unsigned long)SHF_ALLOC;
	info->sechdrs[info->index.vers].sh_flags &= ~(unsigned long)SHF_ALLOC;
	return 0;
}

*/
 Set up our basic convenience variables (pointers to section headers,
 search for module section index etc), and do some basic section
 verification.

 Return the temporary module pointer (we'll replace it with the final
 one when we move the module sections around).
 /*
static struct modulesetup_load_info(struct load_infoinfo, int flags)
{
	unsigned int i;
	int err;
	struct modulemod;

	*/ Set up the convenience variables /*
	info->sechdrs = (void)info->hdr + info->hdr->e_shoff;
	info->secstrings = (void)info->hdr
		+ info->sechdrs[info->hdr->e_shstrndx].sh_offset;

	err = rewrite_section_headers(info, flags);
	if (err)
		return ERR_PTR(err);

	*/ Find internal symbols and strings. /*
	for (i = 1; i < info->hdr->e_shnum; i++) {
		if (info->sechdrs[i].sh_type == SHT_SYMTAB) {
			info->index.sym = i;
			info->index.str = info->sechdrs[i].sh_link;
			info->strtab = (char)info->hdr
				+ info->sechdrs[info->index.str].sh_offset;
			break;
		}
	}

	info->index.mod = find_sec(info, ".gnu.linkonce.this_module");
	if (!info->index.mod) {
		pr_warn("No module found in object\n");
		return ERR_PTR(-ENOEXEC);
	}
	*/ This is temporary: point mod into copy of data. /*
	mod = (void)info->sechdrs[info->index.mod].sh_addr;

	if (info->index.sym == 0) {
		pr_warn("%s: module has no symbols (stripped?)\n", mod->name);
		return ERR_PTR(-ENOEXEC);
	}

	info->index.pcpu = find_pcpusec(info);

	*/ Check module struct version now, before we try to use module. /*
	if (!check_modstruct_version(info->sechdrs, info->index.vers, mod))
		return ERR_PTR(-ENOEXEC);

	return mod;
}

static int check_modinfo(struct modulemod, struct load_infoinfo, int flags)
{
	const charmodmagic = get_modinfo(info, "vermagic");
	int err;

	if (flags & MODULE_INIT_IGNORE_VERMAGIC)
		modmagic = NULL;

	*/ This is allowed: modprobe --force will invalidate it. /*
	if (!modmagic) {
		err = try_to_force_load(mod, "bad vermagic");
		if (err)
			return err;
	} else if (!same_magic(modmagic, vermagic, info->index.vers)) {
		pr_err("%s: version magic '%s' should be '%s'\n",
		       mod->name, modmagic, vermagic);
		return -ENOEXEC;
	}

	if (!get_modinfo(info, "intree"))
		add_taint_module(mod, TAINT_OOT_MODULE, LOCKDEP_STILL_OK);

	if (get_modinfo(info, "staging")) {
		add_taint_module(mod, TAINT_CRAP, LOCKDEP_STILL_OK);
		pr_warn("%s: module is from the staging directory, the quality "
			"is unknown, you have been warned.\n", mod->name);
	}

	*/ Set up license info based on the info section /*
	set_license(mod, get_modinfo(info, "license"));

	return 0;
}

static int find_module_sections(struct modulemod, struct load_infoinfo)
{
	mod->kp = section_objs(info, "__param",
			       sizeof(*mod->kp), &mod->num_kp);
	mod->syms = section_objs(info, "__ksymtab",
				 sizeof(*mod->syms), &mod->num_syms);
	mod->crcs = section_addr(info, "__kcrctab");
	mod->gpl_syms = section_objs(info, "__ksymtab_gpl",
				     sizeof(*mod->gpl_syms),
				     &mod->num_gpl_syms);
	mod->gpl_crcs = section_addr(info, "__kcrctab_gpl");
	mod->gpl_future_syms = section_objs(info,
					    "__ksymtab_gpl_future",
					    sizeof(*mod->gpl_future_syms),
					    &mod->num_gpl_future_syms);
	mod->gpl_future_crcs = section_addr(info, "__kcrctab_gpl_future");

#ifdef CONFIG_UNUSED_SYMBOLS
	mod->unused_syms = section_objs(info, "__ksymtab_unused",
					sizeof(*mod->unused_syms),
					&mod->num_unused_syms);
	mod->unused_crcs = section_addr(info, "__kcrctab_unused");
	mod->unused_gpl_syms = section_objs(info, "__ksymtab_unused_gpl",
					    sizeof(*mod->unused_gpl_syms),
					    &mod->num_unused_gpl_syms);
	mod->unused_gpl_crcs = section_addr(info, "__kcrctab_unused_gpl");
#endif
#ifdef CONFIG_CONSTRUCTORS
	mod->ctors = section_objs(info, ".ctors",
				  sizeof(*mod->ctors), &mod->num_ctors);
	if (!mod->ctors)
		mod->ctors = section_objs(info, ".init_array",
				sizeof(*mod->ctors), &mod->num_ctors);
	else if (find_sec(info, ".init_array")) {
		*/
		 This shouldn't happen with same compiler and binutils
		 building all parts of the module.
		 /*
		pr_warn("%s: has both .ctors and .init_array.\n",
		       mod->name);
		return -EINVAL;
	}
#endif

#ifdef CONFIG_TRACEPOINTS
	mod->tracepoints_ptrs = section_objs(info, "__tracepoints_ptrs",
					     sizeof(*mod->tracepoints_ptrs),
					     &mod->num_tracepoints);
#endif
#ifdef HAVE_JUMP_LABEL
	mod->jump_entries = section_objs(info, "__jump_table",
					sizeof(*mod->jump_entries),
					&mod->num_jump_entries);
#endif
#ifdef CONFIG_EVENT_TRACING
	mod->trace_events = section_objs(info, "_ftrace_events",
					 sizeof(*mod->trace_events),
					 &mod->num_trace_events);
	mod->trace_enums = section_objs(info, "_ftrace_enum_map",
					sizeof(*mod->trace_enums),
					&mod->num_trace_enums);
#endif
#ifdef CONFIG_TRACING
	mod->trace_bprintk_fmt_start = section_objs(info, "__trace_printk_fmt",
					 sizeof(*mod->trace_bprintk_fmt_start),
					 &mod->num_trace_bprintk_fmt);
#endif
#ifdef CONFIG_FTRACE_MCOUNT_RECORD
	*/ sechdrs[0].sh_size is always zero /*
	mod->ftrace_callsites = section_objs(info, "__mcount_loc",
					     sizeof(*mod->ftrace_callsites),
					     &mod->num_ftrace_callsites);
#endif

	mod->extable = section_objs(info, "__ex_table",
				    sizeof(*mod->extable), &mod->num_exentries);

	if (section_addr(info, "__obsparm"))
		pr_warn("%s: Ignoring obsolete parameters\n", mod->name);

	info->debug = section_objs(info, "__verbose",
				   sizeof(*info->debug), &info->num_debug);

	return 0;
}

static int move_module(struct modulemod, struct load_infoinfo)
{
	int i;
	voidptr;

	*/ Do the allocs. /*
	ptr = module_alloc(mod->core_layout.size);
	*/
	 The pointer to this block is stored in the module structure
	 which is inside the block. Just mark it as not being a
	 leak.
	 /*
	kmemleak_not_leak(ptr);
	if (!ptr)
		return -ENOMEM;

	memset(ptr, 0, mod->core_layout.size);
	mod->core_layout.base = ptr;

	if (mod->init_layout.size) {
		ptr = module_alloc(mod->init_layout.size);
		*/
		 The pointer to this block is stored in the module structure
		 which is inside the block. This block doesn't need to be
		 scanned as it contains data and code that will be freed
		 after the module is initialized.
		 /*
		kmemleak_ignore(ptr);
		if (!ptr) {
			module_memfree(mod->core_layout.base);
			return -ENOMEM;
		}
		memset(ptr, 0, mod->init_layout.size);
		mod->init_layout.base = ptr;
	} else
		mod->init_layout.base = NULL;

	*/ Transfer each section which specifies SHF_ALLOC /*
	pr_debug("final section addresses:\n");
	for (i = 0; i < info->hdr->e_shnum; i++) {
		voiddest;
		Elf_Shdrshdr = &info->sechdrs[i];

		if (!(shdr->sh_flags & SHF_ALLOC))
			continue;

		if (shdr->sh_entsize & INIT_OFFSET_MASK)
			dest = mod->init_layout.base
				+ (shdr->sh_entsize & ~INIT_OFFSET_MASK);
		else
			dest = mod->core_layout.base + shdr->sh_entsize;

		if (shdr->sh_type != SHT_NOBITS)
			memcpy(dest, (void)shdr->sh_addr, shdr->sh_size);
		*/ Update sh_addr to point to copy in image. /*
		shdr->sh_addr = (unsigned long)dest;
		pr_debug("\t0x%lx %s\n",
			 (long)shdr->sh_addr, info->secstrings + shdr->sh_name);
	}

	return 0;
}

static int check_module_license_and_versions(struct modulemod)
{
	*/
	 ndiswrapper is under GPL by itself, but loads proprietary modules.
	 Don't use add_taint_module(), as it would prevent ndiswrapper from
	 using GPL-only symbols it needs.
	 /*
	if (strcmp(mod->name, "ndiswrapper") == 0)
		add_taint(TAINT_PROPRIETARY_MODULE, LOCKDEP_NOW_UNRELIABLE);

	*/ driverloader was caught wrongly pretending to be under GPL /*
	if (strcmp(mod->name, "driverloader") == 0)
		add_taint_module(mod, TAINT_PROPRIETARY_MODULE,
				 LOCKDEP_NOW_UNRELIABLE);

	*/ lve claims to be GPL but upstream won't provide source /*
	if (strcmp(mod->name, "lve") == 0)
		add_taint_module(mod, TAINT_PROPRIETARY_MODULE,
				 LOCKDEP_NOW_UNRELIABLE);

#ifdef CONFIG_MODVERSIONS
	if ((mod->num_syms && !mod->crcs)
	    || (mod->num_gpl_syms && !mod->gpl_crcs)
	    || (mod->num_gpl_future_syms && !mod->gpl_future_crcs)
#ifdef CONFIG_UNUSED_SYMBOLS
	    || (mod->num_unused_syms && !mod->unused_crcs)
	    || (mod->num_unused_gpl_syms && !mod->unused_gpl_crcs)
#endif
		) {
		return try_to_force_load(mod,
					 "no versions for exported symbols");
	}
#endif
	return 0;
}

static void flush_module_icache(const struct modulemod)
{
	mm_segment_t old_fs;

	*/ flush the icache in correct context /*
	old_fs = get_fs();
	set_fs(KERNEL_DS);

	*/
	 Flush the instruction cache, since we've played with text.
	 Do it before processing of module parameters, so the module
	 can provide parameter accessor functions of its own.
	 /*
	if (mod->init_layout.base)
		flush_icache_range((unsigned long)mod->init_layout.base,
				   (unsigned long)mod->init_layout.base
				   + mod->init_layout.size);
	flush_icache_range((unsigned long)mod->core_layout.base,
			   (unsigned long)mod->core_layout.base + mod->core_layout.size);

	set_fs(old_fs);
}

int __weak module_frob_arch_sections(Elf_Ehdrhdr,
				     Elf_Shdrsechdrs,
				     charsecstrings,
				     struct modulemod)
{
	return 0;
}

static struct modulelayout_and_allocate(struct load_infoinfo, int flags)
{
	*/ Module within temporary copy. /*
	struct modulemod;
	int err;

	mod = setup_load_info(info, flags);
	if (IS_ERR(mod))
		return mod;

	err = check_modinfo(mod, info, flags);
	if (err)
		return ERR_PTR(err);

	*/ Allow arches to frob section contents and sizes.  /*
	err = module_frob_arch_sections(info->hdr, info->sechdrs,
					info->secstrings, mod);
	if (err < 0)
		return ERR_PTR(err);

	*/ We will do a special allocation for per-cpu sections later. /*
	info->sechdrs[info->index.pcpu].sh_flags &= ~(unsigned long)SHF_ALLOC;

	*/ Determine total sizes, and put offsets in sh_entsize.  For now
	   this is done generically; there doesn't appear to be any
	   special cases for the architectures. /*
	layout_sections(mod, info);
	layout_symtab(mod, info);

	*/ Allocate and move to the final place /*
	err = move_module(mod, info);
	if (err)
		return ERR_PTR(err);

	*/ Module has been copied to its final place now: return it. /*
	mod = (void)info->sechdrs[info->index.mod].sh_addr;
	kmemleak_load_module(mod, info);
	return mod;
}

*/ mod is no longer valid after this! /*
static void module_deallocate(struct modulemod, struct load_infoinfo)
{
	percpu_modfree(mod);
	module_arch_freeing_init(mod);
	module_memfree(mod->init_layout.base);
	module_memfree(mod->core_layout.base);
}

int __weak module_finalize(const Elf_Ehdrhdr,
			   const Elf_Shdrsechdrs,
			   struct moduleme)
{
	return 0;
}

static int post_relocation(struct modulemod, const struct load_infoinfo)
{
	*/ Sort exception table now relocations are done. /*
	sort_extable(mod->extable, mod->extable + mod->num_exentries);

	*/ Copy relocated percpu area over. /*
	percpu_modcopy(mod, (void)info->sechdrs[info->index.pcpu].sh_addr,
		       info->sechdrs[info->index.pcpu].sh_size);

	*/ Setup kallsyms-specific fields. /*
	add_kallsyms(mod, info);

	*/ Arch-specific module finalizing. /*
	return module_finalize(info->hdr, info->sechdrs, mod);
}

*/ Is this module of this name done loading?  No locks held. /*
static bool finished_loading(const charname)
{
	struct modulemod;
	bool ret;

	*/
	 The module_mutex should not be a heavily contended lock;
	 if we get the occasional sleep here, we'll go an extra iteration
	 in the wait_event_interruptible(), which is harmless.
	 /*
	sched_annotate_sleep();
	mutex_lock(&module_mutex);
	mod = find_module_all(name, strlen(name), true);
	ret = !mod || mod->state == MODULE_STATE_LIVE
		|| mod->state == MODULE_STATE_GOING;
	mutex_unlock(&module_mutex);

	return ret;
}

*/ Call module constructors. /*
static void do_mod_ctors(struct modulemod)
{
#ifdef CONFIG_CONSTRUCTORS
	unsigned long i;

	for (i = 0; i < mod->num_ctors; i++)
		mod->ctors[i]();
#endif
}

*/ For freeing module_init on success, in case kallsyms traversing /*
struct mod_initfree {
	struct rcu_head rcu;
	voidmodule_init;
};

static void do_free_init(struct rcu_headhead)
{
	struct mod_initfreem = container_of(head, struct mod_initfree, rcu);
	module_memfree(m->module_init);
	kfree(m);
}

*/
 This is where the real work happens.

 Keep it uninlined to provide a reliable breakpoint target, e.g. for the gdb
 helper command 'lx-symbols'.
 /*
static noinline int do_init_module(struct modulemod)
{
	int ret = 0;
	struct mod_initfreefreeinit;

	freeinit = kmalloc(sizeof(*freeinit), GFP_KERNEL);
	if (!freeinit) {
		ret = -ENOMEM;
		goto fail;
	}
	freeinit->module_init = mod->init_layout.base;

	*/
	 We want to find out whether @mod uses async during init.  Clear
	 PF_USED_ASYNC.  async_schedule*() will set it.
	 /*
	current->flags &= ~PF_USED_ASYNC;

	do_mod_ctors(mod);
	*/ Start the module /*
	if (mod->init != NULL)
		ret = do_one_initcall(mod->init);
	if (ret < 0) {
		goto fail_free_freeinit;
	}
	if (ret > 0) {
		pr_warn("%s: '%s'->init suspiciously returned %d, it should "
			"follow 0/-E convention\n"
			"%s: loading module anyway...\n",
			__func__, mod->name, ret, __func__);
		dump_stack();
	}

	*/ Now it's a first class citizen! /*
	mod->state = MODULE_STATE_LIVE;
	blocking_notifier_call_chain(&module_notify_list,
				     MODULE_STATE_LIVE, mod);

	*/
	 We need to finish all async code before the module init sequence
	 is done.  This has potential to deadlock.  For example, a newly
	 detected block device can trigger request_module() of the
	 default iosched from async probing task.  Once userland helper
	 reaches here, async_synchronize_full() will wait on the async
	 task waiting on request_module() and deadlock.
	
	 This deadlock is avoided by perfomring async_synchronize_full()
	 iff module init queued any async jobs.  This isn't a full
	 solution as it will deadlock the same if module loading from
	 async jobs nests more than once; however, due to the various
	 constraints, this hack seems to be the best option for now.
	 Please refer to the following thread for details.
	
	 http://thread.gmane.org/gmane.linux.kernel/1420814
	 /*
	if (!mod->async_probe_requested && (current->flags & PF_USED_ASYNC))
		async_synchronize_full();

	mutex_lock(&module_mutex);
	*/ Drop initial reference. /*
	module_put(mod);
	trim_init_extable(mod);
#ifdef CONFIG_KALLSYMS
	*/ Switch to core kallsyms now init is done: kallsyms may be walking! /*
	rcu_assign_pointer(mod->kallsyms, &mod->core_kallsyms);
#endif
	mod_tree_remove_init(mod);
	disable_ro_nx(&mod->init_layout);
	module_arch_freeing_init(mod);
	mod->init_layout.base = NULL;
	mod->init_layout.size = 0;
	mod->init_layout.ro_size = 0;
	mod->init_layout.text_size = 0;
	*/
	 We want to free module_init, but be aware that kallsyms may be
	 walking this with preempt disabled.  In all the failure paths, we
	 call synchronize_sched(), but we don't want to slow down the success
	 path, so use actual RCU here.
	 /*
	call_rcu_sched(&freeinit->rcu, do_free_init);
	mutex_unlock(&module_mutex);
	wake_up_all(&module_wq);

	return 0;

fail_free_freeinit:
	kfree(freeinit);
fail:
	*/ Try to protect us from buggy refcounters. /*
	mod->state = MODULE_STATE_GOING;
	synchronize_sched();
	module_put(mod);
	blocking_notifier_call_chain(&module_notify_list,
				     MODULE_STATE_GOING, mod);
	klp_module_going(mod);
	ftrace_release_mod(mod);
	free_module(mod);
	wake_up_all(&module_wq);
	return ret;
}

static int may_init_module(void)
{
	if (!capable(CAP_SYS_MODULE) || modules_disabled)
		return -EPERM;

	return 0;
}

*/
 We try to place it in the list now to make sure it's unique before
 we dedicate too many resources.  In particular, temporary percpu
 memory exhaustion.
 /*
static int add_unformed_module(struct modulemod)
{
	int err;
	struct moduleold;

	mod->state = MODULE_STATE_UNFORMED;

again:
	mutex_lock(&module_mutex);
	old = find_module_all(mod->name, strlen(mod->name), true);
	if (old != NULL) {
		if (old->state == MODULE_STATE_COMING
		    || old->state == MODULE_STATE_UNFORMED) {
			*/ Wait in case it fails to load. /*
			mutex_unlock(&module_mutex);
			err = wait_event_interruptible(module_wq,
					       finished_loading(mod->name));
			if (err)
				goto out_unlocked;
			goto again;
		}
		err = -EEXIST;
		goto out;
	}
	mod_update_bounds(mod);
	list_add_rcu(&mod->list, &modules);
	mod_tree_insert(mod);
	err = 0;

out:
	mutex_unlock(&module_mutex);
out_unlocked:
	return err;
}

static int complete_formation(struct modulemod, struct load_infoinfo)
{
	int err;

	mutex_lock(&module_mutex);

	*/ Find duplicate symbols (must be called under lock). /*
	err = verify_export_symbols(mod);
	if (err < 0)
		goto out;

	*/ This relies on module_mutex for list integrity. /*
	module_bug_finalize(info->hdr, info->sechdrs, mod);

	*/ Set RO and NX regions /*
	module_enable_ro(mod);
	module_enable_nx(mod);

	*/ Mark state as coming so strong_try_module_get() ignores us,
	 but kallsyms etc. can see us. /*
	mod->state = MODULE_STATE_COMING;
	mutex_unlock(&module_mutex);

	return 0;

out:
	mutex_unlock(&module_mutex);
	return err;
}

static int prepare_coming_module(struct modulemod)
{
	int err;

	ftrace_module_enable(mod);
	err = klp_module_coming(mod);
	if (err)
		return err;

	blocking_notifier_call_chain(&module_notify_list,
				     MODULE_STATE_COMING, mod);
	return 0;
}

static int unknown_module_param_cb(charparam, charval, const charmodname,
				   voidarg)
{
	struct modulemod = arg;
	int ret;

	if (strcmp(param, "async_probe") == 0) {
		mod->async_probe_requested = true;
		return 0;
	}

	*/ Check for magic 'dyndbg' arg /*
	ret = ddebug_dyndbg_module_param_cb(param, val, modname);
	if (ret != 0)
		pr_warn("%s: unknown parameter '%s' ignored\n", modname, param);
	return 0;
}

*/ Allocate and load the module: note that size of section 0 is always
   zero, and we rely on this for optional sections. /*
static int load_module(struct load_infoinfo, const char __useruargs,
		       int flags)
{
	struct modulemod;
	long err;
	charafter_dashes;

	err = module_sig_check(info);
	if (err)
		goto free_copy;

	err = elf_header_check(info);
	if (err)
		goto free_copy;

	*/ Figure out module layout, and allocate all the memory. /*
	mod = layout_and_allocate(info, flags);
	if (IS_ERR(mod)) {
		err = PTR_ERR(mod);
		goto free_copy;
	}

	*/ Reserve our place in the list. /*
	err = add_unformed_module(mod);
	if (err)
		goto free_module;

#ifdef CONFIG_MODULE_SIG
	mod->sig_ok = info->sig_ok;
	if (!mod->sig_ok) {
		pr_notice_once("%s: module verification failed: signature "
			       "and/or required key missing - tainting "
			       "kernel\n", mod->name);
		add_taint_module(mod, TAINT_UNSIGNED_MODULE, LOCKDEP_STILL_OK);
	}
#endif

	*/ To avoid stressing percpu allocator, do this once we're unique. /*
	err = percpu_modalloc(mod, info);
	if (err)
		goto unlink_mod;

	*/ Now module is in final location, initialize linked lists, etc. /*
	err = module_unload_init(mod);
	if (err)
		goto unlink_mod;

	init_param_lock(mod);

	*/ Now we've got everything in the final locations, we can
	 find optional sections. /*
	err = find_module_sections(mod, info);
	if (err)
		goto free_unload;

	err = check_module_license_and_versions(mod);
	if (err)
		goto free_unload;

	*/ Set up MODINFO_ATTR fields /*
	setup_modinfo(mod, info);

	*/ Fix up syms, so that st_value is a pointer to location. /*
	err = simplify_symbols(mod, info);
	if (err < 0)
		goto free_modinfo;

	err = apply_relocations(mod, info);
	if (err < 0)
		goto free_modinfo;

	err = post_relocation(mod, info);
	if (err < 0)
		goto free_modinfo;

	flush_module_icache(mod);

	*/ Now copy in args /*
	mod->args = strndup_user(uargs, ~0UL >> 1);
	if (IS_ERR(mod->args)) {
		err = PTR_ERR(mod->args);
		goto free_arch_cleanup;
	}

	dynamic_debug_setup(info->debug, info->num_debug);

	*/ Ftrace init must be called in the MODULE_STATE_UNFORMED state /*
	ftrace_module_init(mod);

	*/ Finally it's fully formed, ready to start executing. /*
	err = complete_formation(mod, info);
	if (err)
		goto ddebug_cleanup;

	err = prepare_coming_module(mod);
	if (err)
		goto bug_cleanup;

	*/ Module is ready to execute: parsing args may do that. /*
	after_dashes = parse_args(mod->name, mod->args, mod->kp, mod->num_kp,
				  -32768, 32767, mod,
				  unknown_module_param_cb);
	if (IS_ERR(after_dashes)) {
		err = PTR_ERR(after_dashes);
		goto coming_cleanup;
	} else if (after_dashes) {
		pr_warn("%s: parameters '%s' after `--' ignored\n",
		       mod->name, after_dashes);
	}

	*/ Link in to syfs. /*
	err = mod_sysfs_setup(mod, info, mod->kp, mod->num_kp);
	if (err < 0)
		goto coming_cleanup;

	*/ Get rid of temporary copy. /*
	free_copy(info);

	*/ Done! /*
	trace_module_load(mod);

	return do_init_module(mod);

 coming_cleanup:
	blocking_notifier_call_chain(&module_notify_list,
				     MODULE_STATE_GOING, mod);
	klp_module_going(mod);

 bug_cleanup:
	*/ module_bug_cleanup needs module_mutex protection /*
	mutex_lock(&module_mutex);
	module_bug_cleanup(mod);
	mutex_unlock(&module_mutex);

	*/ we can't deallocate the module until we clear memory protection /*
	module_disable_ro(mod);
	module_disable_nx(mod);

 ddebug_cleanup:
	dynamic_debug_remove(info->debug);
	synchronize_sched();
	kfree(mod->args);
 free_arch_cleanup:
	module_arch_cleanup(mod);
 free_modinfo:
	free_modinfo(mod);
 free_unload:
	module_unload_free(mod);
 unlink_mod:
	mutex_lock(&module_mutex);
	*/ Unlink carefully: kallsyms could be walking list. /*
	list_del_rcu(&mod->list);
	mod_tree_remove(mod);
	wake_up_all(&module_wq);
	*/ Wait for RCU-sched synchronizing before releasing mod->list. /*
	synchronize_sched();
	mutex_unlock(&module_mutex);
 free_module:
	*/
	 Ftrace needs to clean up what it initialized.
	 This does nothing if ftrace_module_init() wasn't called,
	 but it must be called outside of module_mutex.
	 /*
	ftrace_release_mod(mod);
	*/ Free lock-classes; relies on the preceding sync_rcu() /*
	lockdep_free_key_range(mod->core_layout.base, mod->core_layout.size);

	module_deallocate(mod, info);
 free_copy:
	free_copy(info);
	return err;
}

SYSCALL_DEFINE3(init_module, void __user, umod,
		unsigned long, len, const char __user, uargs)
{
	int err;
	struct load_info info = { };

	err = may_init_module();
	if (err)
		return err;

	pr_debug("init_module: umod=%p, len=%lu, uargs=%p\n",
	       umod, len, uargs);

	err = copy_module_from_user(umod, len, &info);
	if (err)
		return err;

	return load_module(&info, uargs, 0);
}

SYSCALL_DEFINE3(finit_module, int, fd, const char __user, uargs, int, flags)
{
	struct load_info info = { };
	loff_t size;
	voidhdr;
	int err;

	err = may_init_module();
	if (err)
		return err;

	pr_debug("finit_module: fd=%d, uargs=%p, flags=%i\n", fd, uargs, flags);

	if (flags & ~(MODULE_INIT_IGNORE_MODVERSIONS
		      |MODULE_INIT_IGNORE_VERMAGIC))
		return -EINVAL;

	err = kernel_read_file_from_fd(fd, &hdr, &size, INT_MAX,
				       READING_MODULE);
	if (err)
		return err;
	info.hdr = hdr;
	info.len = size;

	return load_module(&info, uargs, flags);
}

static inline int within(unsigned long addr, voidstart, unsigned long size)
{
	return ((void)addr >= start && (void)addr < start + size);
}

#ifdef CONFIG_KALLSYMS
*/
 This ignores the intensely annoying "mapping symbols" found
 in ARM ELF files: $a, $t and $d.
 /*
static inline int is_arm_mapping_symbol(const charstr)
{
	if (str[0] == '.' && str[1] == 'L')
		return true;
	return str[0] == '$' && strchr("axtd", str[1])
	       && (str[2] == '\0' || str[2] == '.');
}

static const charsymname(struct mod_kallsymskallsyms, unsigned int symnum)
{
	return kallsyms->strtab + kallsyms->symtab[symnum].st_name;
}

static const charget_ksymbol(struct modulemod,
			       unsigned long addr,
			       unsigned longsize,
			       unsigned longoffset)
{
	unsigned int i, best = 0;
	unsigned long nextval;
	struct mod_kallsymskallsyms = rcu_dereference_sched(mod->kallsyms);

	*/ At worse, next value is at end of module /*
	if (within_module_init(addr, mod))
		nextval = (unsigned long)mod->init_layout.base+mod->init_layout.text_size;
	else
		nextval = (unsigned long)mod->core_layout.base+mod->core_layout.text_size;

	*/ Scan for closest preceding symbol, and next symbol. (ELF
	   starts real symbols at 1). /*
	for (i = 1; i < kallsyms->num_symtab; i++) {
		if (kallsyms->symtab[i].st_shndx == SHN_UNDEF)
			continue;

		*/ We ignore unnamed symbols: they're uninformative
		 and inserted at a whim. /*
		if (*symname(kallsyms, i) == '\0'
		    || is_arm_mapping_symbol(symname(kallsyms, i)))
			continue;

		if (kallsyms->symtab[i].st_value <= addr
		    && kallsyms->symtab[i].st_value > kallsyms->symtab[best].st_value)
			best = i;
		if (kallsyms->symtab[i].st_value > addr
		    && kallsyms->symtab[i].st_value < nextval)
			nextval = kallsyms->symtab[i].st_value;
	}

	if (!best)
		return NULL;

	if (size)
		*size = nextval - kallsyms->symtab[best].st_value;
	if (offset)
		*offset = addr - kallsyms->symtab[best].st_value;
	return symname(kallsyms, best);
}

*/ For kallsyms to ask for address resolution.  NULL means not found.  Careful
 not to lock to avoid deadlock on oopses, simply disable preemption. /*
const charmodule_address_lookup(unsigned long addr,
			    unsigned longsize,
			    unsigned longoffset,
			    char*modname,
			    charnamebuf)
{
	const charret = NULL;
	struct modulemod;

	preempt_disable();
	mod = __module_address(addr);
	if (mod) {
		if (modname)
			*modname = mod->name;
		ret = get_ksymbol(mod, addr, size, offset);
	}
	*/ Make a copy in here where it's safe /*
	if (ret) {
		strncpy(namebuf, ret, KSYM_NAME_LEN - 1);
		ret = namebuf;
	}
	preempt_enable();

	return ret;
}

int lookup_module_symbol_name(unsigned long addr, charsymname)
{
	struct modulemod;

	preempt_disable();
	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		if (within_module(addr, mod)) {
			const charsym;

			sym = get_ksymbol(mod, addr, NULL, NULL);
			if (!sym)
				goto out;
			strlcpy(symname, sym, KSYM_NAME_LEN);
			preempt_enable();
			return 0;
		}
	}
out:
	preempt_enable();
	return -ERANGE;
}

int lookup_module_symbol_attrs(unsigned long addr, unsigned longsize,
			unsigned longoffset, charmodname, charname)
{
	struct modulemod;

	preempt_disable();
	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		if (within_module(addr, mod)) {
			const charsym;

			sym = get_ksymbol(mod, addr, size, offset);
			if (!sym)
				goto out;
			if (modname)
				strlcpy(modname, mod->name, MODULE_NAME_LEN);
			if (name)
				strlcpy(name, sym, KSYM_NAME_LEN);
			preempt_enable();
			return 0;
		}
	}
out:
	preempt_enable();
	return -ERANGE;
}

int module_get_kallsym(unsigned int symnum, unsigned longvalue, chartype,
			charname, charmodule_name, intexported)
{
	struct modulemod;

	preempt_disable();
	list_for_each_entry_rcu(mod, &modules, list) {
		struct mod_kallsymskallsyms;

		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		kallsyms = rcu_dereference_sched(mod->kallsyms);
		if (symnum < kallsyms->num_symtab) {
			*value = kallsyms->symtab[symnum].st_value;
			*type = kallsyms->symtab[symnum].st_info;
			strlcpy(name, symname(kallsyms, symnum), KSYM_NAME_LEN);
			strlcpy(module_name, mod->name, MODULE_NAME_LEN);
			*exported = is_exported(name,value, mod);
			preempt_enable();
			return 0;
		}
		symnum -= kallsyms->num_symtab;
	}
	preempt_enable();
	return -ERANGE;
}

static unsigned long mod_find_symname(struct modulemod, const charname)
{
	unsigned int i;
	struct mod_kallsymskallsyms = rcu_dereference_sched(mod->kallsyms);

	for (i = 0; i < kallsyms->num_symtab; i++)
		if (strcmp(name, symname(kallsyms, i)) == 0 &&
		    kallsyms->symtab[i].st_info != 'U')
			return kallsyms->symtab[i].st_value;
	return 0;
}

*/ Look for this name: can be of form module:name. /*
unsigned long module_kallsyms_lookup_name(const charname)
{
	struct modulemod;
	charcolon;
	unsigned long ret = 0;

	*/ Don't lock: we're in enough trouble already. /*
	preempt_disable();
	if ((colon = strchr(name, ':')) != NULL) {
		if ((mod = find_module_all(name, colon - name, false)) != NULL)
			ret = mod_find_symname(mod, colon+1);
	} else {
		list_for_each_entry_rcu(mod, &modules, list) {
			if (mod->state == MODULE_STATE_UNFORMED)
				continue;
			if ((ret = mod_find_symname(mod, name)) != 0)
				break;
		}
	}
	preempt_enable();
	return ret;
}

int module_kallsyms_on_each_symbol(int (*fn)(void, const char,
					     struct module, unsigned long),
				   voiddata)
{
	struct modulemod;
	unsigned int i;
	int ret;

	module_assert_mutex();

	list_for_each_entry(mod, &modules, list) {
		*/ We hold module_mutex: no need for rcu_dereference_sched /*
		struct mod_kallsymskallsyms = mod->kallsyms;

		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		for (i = 0; i < kallsyms->num_symtab; i++) {
			ret = fn(data, symname(kallsyms, i),
				 mod, kallsyms->symtab[i].st_value);
			if (ret != 0)
				return ret;
		}
	}
	return 0;
}
#endif */ CONFIG_KALLSYMS /*

static charmodule_flags(struct modulemod, charbuf)
{
	int bx = 0;

	BUG_ON(mod->state == MODULE_STATE_UNFORMED);
	if (mod->taints ||
	    mod->state == MODULE_STATE_GOING ||
	    mod->state == MODULE_STATE_COMING) {
		buf[bx++] = '(';
		bx += module_flags_taint(mod, buf + bx);
		*/ Show a - for module-is-being-unloaded /*
		if (mod->state == MODULE_STATE_GOING)
			buf[bx++] = '-';
		*/ Show a + for module-is-being-loaded /*
		if (mod->state == MODULE_STATE_COMING)
			buf[bx++] = '+';
		buf[bx++] = ')';
	}
	buf[bx] = '\0';

	return buf;
}

#ifdef CONFIG_PROC_FS
*/ Called by the /proc file system to return a list of modules. /*
static voidm_start(struct seq_filem, loff_tpos)
{
	mutex_lock(&module_mutex);
	return seq_list_start(&modules,pos);
}

static voidm_next(struct seq_filem, voidp, loff_tpos)
{
	return seq_list_next(p, &modules, pos);
}

static void m_stop(struct seq_filem, voidp)
{
	mutex_unlock(&module_mutex);
}

static int m_show(struct seq_filem, voidp)
{
	struct modulemod = list_entry(p, struct module, list);
	char buf[8];

	*/ We always ignore unformed modules. /*
	if (mod->state == MODULE_STATE_UNFORMED)
		return 0;

	seq_printf(m, "%s %u",
		   mod->name, mod->init_layout.size + mod->core_layout.size);
	print_unload_info(m, mod);

	*/ Informative for users. /*
	seq_printf(m, " %s",
		   mod->state == MODULE_STATE_GOING ? "Unloading" :
		   mod->state == MODULE_STATE_COMING ? "Loading" :
		   "Live");
	*/ Used by oprofile and other similar tools. /*
	seq_printf(m, " 0x%pK", mod->core_layout.base);

	*/ Taints info /*
	if (mod->taints)
		seq_printf(m, " %s", module_flags(mod, buf));

	seq_puts(m, "\n");
	return 0;
}

*/ Format: modulename size refcount deps address

   Where refcount is a number or -, and deps is a comma-separated list
   of depends or -.
/*
static const struct seq_operations modules_op = {
	.start	= m_start,
	.next	= m_next,
	.stop	= m_stop,
	.show	= m_show
};

static int modules_open(struct inodeinode, struct filefile)
{
	return seq_open(file, &modules_op);
}

static const struct file_operations proc_modules_operations = {
	.open		= modules_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
};

static int __init proc_modules_init(void)
{
	proc_create("modules", 0, NULL, &proc_modules_operations);
	return 0;
}
module_init(proc_modules_init);
#endif

*/ Given an address, look for it in the module exception tables. /*
const struct exception_table_entrysearch_module_extables(unsigned long addr)
{
	const struct exception_table_entrye = NULL;
	struct modulemod;

	preempt_disable();
	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		if (mod->num_exentries == 0)
			continue;

		e = search_extable(mod->extable,
				   mod->extable + mod->num_exentries - 1,
				   addr);
		if (e)
			break;
	}
	preempt_enable();

	*/ Now, if we found one, we are running inside it now, hence
	   we cannot unload the module, hence no refcnt needed. /*
	return e;
}

*/
 is_module_address - is this address inside a module?
 @addr: the address to check.

 See is_module_text_address() if you simply want to see if the address
 is code (not data).
 /*
bool is_module_address(unsigned long addr)
{
	bool ret;

	preempt_disable();
	ret = __module_address(addr) != NULL;
	preempt_enable();

	return ret;
}

*/
 __module_address - get the module which contains an address.
 @addr: the address.

 Must be called with preempt disabled or module mutex held so that
 module doesn't get freed during this.
 /*
struct module__module_address(unsigned long addr)
{
	struct modulemod;

	if (addr < module_addr_min || addr > module_addr_max)
		return NULL;

	module_assert_mutex_or_preempt();

	mod = mod_find(addr);
	if (mod) {
		BUG_ON(!within_module(addr, mod));
		if (mod->state == MODULE_STATE_UNFORMED)
			mod = NULL;
	}
	return mod;
}
EXPORT_SYMBOL_GPL(__module_address);

*/
 is_module_text_address - is this address inside module code?
 @addr: the address to check.

 See is_module_address() if you simply want to see if the address is
 anywhere in a module.  See kernel_text_address() for testing if an
 address corresponds to kernel or module code.
 /*
bool is_module_text_address(unsigned long addr)
{
	bool ret;

	preempt_disable();
	ret = __module_text_address(addr) != NULL;
	preempt_enable();

	return ret;
}

*/
 __module_text_address - get the module whose code contains an address.
 @addr: the address.

 Must be called with preempt disabled or module mutex held so that
 module doesn't get freed during this.
 /*
struct module__module_text_address(unsigned long addr)
{
	struct modulemod = __module_address(addr);
	if (mod) {
		*/ Make sure it's within the text section. /*
		if (!within(addr, mod->init_layout.base, mod->init_layout.text_size)
		    && !within(addr, mod->core_layout.base, mod->core_layout.text_size))
			mod = NULL;
	}
	return mod;
}
EXPORT_SYMBOL_GPL(__module_text_address);

*/ Don't grab lock, we're oopsing. /*
void print_modules(void)
{
	struct modulemod;
	char buf[8];

	printk(KERN_DEFAULT "Modules linked in:");
	*/ Most callers should already have preempt disabled, but make sure /*
	preempt_disable();
	list_for_each_entry_rcu(mod, &modules, list) {
		if (mod->state == MODULE_STATE_UNFORMED)
			continue;
		pr_cont(" %s%s", mod->name, module_flags(mod, buf));
	}
	preempt_enable();
	if (last_unloaded_module[0])
		pr_cont(" [last unloaded: %s]", last_unloaded_module);
	pr_cont("\n");
}

#ifdef CONFIG_MODVERSIONS
*/ Generate the signature for all relevant module structures here.
 If these change, we don't want to try to parse the module. /*
void module_layout(struct modulemod,
		   struct modversion_infover,
		   struct kernel_paramkp,
		   struct kernel_symbolks,
		   struct tracepoint consttp)
{
}
EXPORT_SYMBOL(module_layout);
#endif
*/
 Module internals

 Copyright (C) 2012 Red Hat, Inc. All Rights Reserved.
 Written by David Howells (dhowells@redhat.com)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence
 as published by the Free Software Foundation; either version
 2 of the Licence, or (at your option) any later version.
 /*

extern int mod_verify_sig(const voidmod, unsigned long_modlen);
*/
 Module signature checker

 Copyright (C) 2012 Red Hat, Inc. All Rights Reserved.
 Written by David Howells (dhowells@redhat.com)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence
 as published by the Free Software Foundation; either version
 2 of the Licence, or (at your option) any later version.
 /*

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <keys/system_keyring.h>
#include <crypto/public_key.h>
#include "module-internal.h"

enum pkey_id_type {
	PKEY_ID_PGP,		*/ OpenPGP generated key ID /*
	PKEY_ID_X509,		*/ X.509 arbitrary subjectKeyIdentifier /*
	PKEY_ID_PKCS7,		*/ Signature in PKCS#7 message /*
};

*/
 Module signature information block.

 The constituents of the signature section are, in order:

	- Signer's name
	- Key identifier
	- Signature data
	- Information block
 /*
struct module_signature {
	u8	algo;		*/ Public-key crypto algorithm [0] /*
	u8	hash;		*/ Digest algorithm [0] /*
	u8	id_type;	*/ Key identifier type [PKEY_ID_PKCS7] /*
	u8	signer_len;	*/ Length of signer's name [0] /*
	u8	key_id_len;	*/ Length of key identifier [0] /*
	u8	__pad[3];
	__be32	sig_len;	*/ Length of signature data /*
};

*/
 Verify the signature on a module.
 /*
int mod_verify_sig(const voidmod, unsigned long_modlen)
{
	struct module_signature ms;
	size_t modlen =_modlen, sig_len;

	pr_devel("==>%s(,%zu)\n", __func__, modlen);

	if (modlen <= sizeof(ms))
		return -EBADMSG;

	memcpy(&ms, mod + (modlen - sizeof(ms)), sizeof(ms));
	modlen -= sizeof(ms);

	sig_len = be32_to_cpu(ms.sig_len);
	if (sig_len >= modlen)
		return -EBADMSG;
	modlen -= sig_len;
	*_modlen = modlen;

	if (ms.id_type != PKEY_ID_PKCS7) {
		pr_err("Module is not signed with expected PKCS#7 message\n");
		return -ENOPKG;
	}

	if (ms.algo != 0 ||
	    ms.hash != 0 ||
	    ms.signer_len != 0 ||
	    ms.key_id_len != 0 ||
	    ms.__pad[0] != 0 ||
	    ms.__pad[1] != 0 ||
	    ms.__pad[2] != 0) {
		pr_err("PKCS#7 signature info has unexpected non-zero params\n");
		return -EBADMSG;
	}

	return system_verify_data(mod, modlen, mod + modlen, sig_len,
				  VERIFYING_MODULE_SIGNATURE);
}
*/
/*
#include <linux/kdebug.h>
#include <linux/kprobes.h>
#include <linux/export.h>
#include <linux/notifier.h>
#include <linux/rcupdate.h>
#include <linux/vmalloc.h>
#include <linux/reboot.h>

*/
	Notifier list for kernel code which wants to be called
	at shutdown. This is used to stop any idling DMA operations
	and the like.
 /*
BLOCKING_NOTIFIER_HEAD(reboot_notifier_list);

*/
	Notifier chain core routines.  The exported routines below
	are layered on top of these, with appropriate locking added.
 /*

static int notifier_chain_register(struct notifier_block*nl,
		struct notifier_blockn)
{
	while ((*nl) != NULL) {
		if (n->priority > (*nl)->priority)
			break;
		nl = &((*nl)->next);
	}
	n->next =nl;
	rcu_assign_pointer(*nl, n);
	return 0;
}

static int notifier_chain_cond_register(struct notifier_block*nl,
		struct notifier_blockn)
{
	while ((*nl) != NULL) {
		if ((*nl) == n)
			return 0;
		if (n->priority > (*nl)->priority)
			break;
		nl = &((*nl)->next);
	}
	n->next =nl;
	rcu_assign_pointer(*nl, n);
	return 0;
}

static int notifier_chain_unregister(struct notifier_block*nl,
		struct notifier_blockn)
{
	while ((*nl) != NULL) {
		if ((*nl) == n) {
			rcu_assign_pointer(*nl, n->next);
			return 0;
		}
		nl = &((*nl)->next);
	}
	return -ENOENT;
}

*/
 notifier_call_chain - Informs the registered notifiers about an event.
	@nl:		Pointer to head of the blocking notifier chain
	@val:		Value passed unmodified to notifier function
	@v:		Pointer passed unmodified to notifier function
	@nr_to_call:	Number of notifier functions to be called. Don't care
			value of this parameter is -1.
	@nr_calls:	Records the number of notifications sent. Don't care
			value of this field is NULL.
	@returns:	notifier_call_chain returns the value returned by the
			last notifier function called.
 /*
static int notifier_call_chain(struct notifier_block*nl,
			       unsigned long val, voidv,
			       int nr_to_call, intnr_calls)
{
	int ret = NOTIFY_DONE;
	struct notifier_blocknb,next_nb;

	nb = rcu_dereference_raw(*nl);

	while (nb && nr_to_call) {
		next_nb = rcu_dereference_raw(nb->next);

#ifdef CONFIG_DEBUG_NOTIFIERS
		if (unlikely(!func_ptr_is_kernel_text(nb->notifier_call))) {
			WARN(1, "Invalid notifier called!");
			nb = next_nb;
			continue;
		}
#endif
		ret = nb->notifier_call(nb, val, v);

		if (nr_calls)
			(*nr_calls)++;

		if ((ret & NOTIFY_STOP_MASK) == NOTIFY_STOP_MASK)
			break;
		nb = next_nb;
		nr_to_call--;
	}
	return ret;
}
NOKPROBE_SYMBOL(notifier_call_chain);

*/
	Atomic notifier chain routines.  Registration and unregistration
	use a spinlock, and call_chain is synchronized by RCU (no locks).
 /*

*/
	atomic_notifier_chain_register - Add notifier to an atomic notifier chain
	@nh: Pointer to head of the atomic notifier chain
	@n: New entry in notifier chain

	Adds a notifier to an atomic notifier chain.

	Currently always returns zero.
 /*
int atomic_notifier_chain_register(struct atomic_notifier_headnh,
		struct notifier_blockn)
{
	unsigned long flags;
	int ret;

	spin_lock_irqsave(&nh->lock, flags);
	ret = notifier_chain_register(&nh->head, n);
	spin_unlock_irqrestore(&nh->lock, flags);
	return ret;
}
EXPORT_SYMBOL_GPL(atomic_notifier_chain_register);

*/
	atomic_notifier_chain_unregister - Remove notifier from an atomic notifier chain
	@nh: Pointer to head of the atomic notifier chain
	@n: Entry to remove from notifier chain

	Removes a notifier from an atomic notifier chain.

	Returns zero on success or %-ENOENT on failure.
 /*
int atomic_notifier_chain_unregister(struct atomic_notifier_headnh,
		struct notifier_blockn)
{
	unsigned long flags;
	int ret;

	spin_lock_irqsave(&nh->lock, flags);
	ret = notifier_chain_unregister(&nh->head, n);
	spin_unlock_irqrestore(&nh->lock, flags);
	synchronize_rcu();
	return ret;
}
EXPORT_SYMBOL_GPL(atomic_notifier_chain_unregister);

*/
	__atomic_notifier_call_chain - Call functions in an atomic notifier chain
	@nh: Pointer to head of the atomic notifier chain
	@val: Value passed unmodified to notifier function
	@v: Pointer passed unmodified to notifier function
	@nr_to_call: See the comment for notifier_call_chain.
	@nr_calls: See the comment for notifier_call_chain.

	Calls each function in a notifier chain in turn.  The functions
	run in an atomic context, so they must not block.
	This routine uses RCU to synchronize with changes to the chain.

	If the return value of the notifier can be and'ed
	with %NOTIFY_STOP_MASK then atomic_notifier_call_chain()
	will return immediately, with the return value of
	the notifier function which halted execution.
	Otherwise the return value is the return value
	of the last notifier function called.
 /*
int __atomic_notifier_call_chain(struct atomic_notifier_headnh,
				 unsigned long val, voidv,
				 int nr_to_call, intnr_calls)
{
	int ret;

	rcu_read_lock();
	ret = notifier_call_chain(&nh->head, val, v, nr_to_call, nr_calls);
	rcu_read_unlock();
	return ret;
}
EXPORT_SYMBOL_GPL(__atomic_notifier_call_chain);
NOKPROBE_SYMBOL(__atomic_notifier_call_chain);

int atomic_notifier_call_chain(struct atomic_notifier_headnh,
			       unsigned long val, voidv)
{
	return __atomic_notifier_call_chain(nh, val, v, -1, NULL);
}
EXPORT_SYMBOL_GPL(atomic_notifier_call_chain);
NOKPROBE_SYMBOL(atomic_notifier_call_chain);

*/
	Blocking notifier chain routines.  All access to the chain is
	synchronized by an rwsem.
 /*

*/
	blocking_notifier_chain_register - Add notifier to a blocking notifier chain
	@nh: Pointer to head of the blocking notifier chain
	@n: New entry in notifier chain

	Adds a notifier to a blocking notifier chain.
	Must be called in process context.

	Currently always returns zero.
 /*
int blocking_notifier_chain_register(struct blocking_notifier_headnh,
		struct notifier_blockn)
{
	int ret;

	*/
	 This code gets used during boot-up, when task switching is
	 not yet working and interrupts must remain disabled.  At
	 such times we must not call down_write().
	 /*
	if (unlikely(system_state == SYSTEM_BOOTING))
		return notifier_chain_register(&nh->head, n);

	down_write(&nh->rwsem);
	ret = notifier_chain_register(&nh->head, n);
	up_write(&nh->rwsem);
	return ret;
}
EXPORT_SYMBOL_GPL(blocking_notifier_chain_register);

*/
	blocking_notifier_chain_cond_register - Cond add notifier to a blocking notifier chain
	@nh: Pointer to head of the blocking notifier chain
	@n: New entry in notifier chain

	Adds a notifier to a blocking notifier chain, only if not already
	present in the chain.
	Must be called in process context.

	Currently always returns zero.
 /*
int blocking_notifier_chain_cond_register(struct blocking_notifier_headnh,
		struct notifier_blockn)
{
	int ret;

	down_write(&nh->rwsem);
	ret = notifier_chain_cond_register(&nh->head, n);
	up_write(&nh->rwsem);
	return ret;
}
EXPORT_SYMBOL_GPL(blocking_notifier_chain_cond_register);

*/
	blocking_notifier_chain_unregister - Remove notifier from a blocking notifier chain
	@nh: Pointer to head of the blocking notifier chain
	@n: Entry to remove from notifier chain

	Removes a notifier from a blocking notifier chain.
	Must be called from process context.

	Returns zero on success or %-ENOENT on failure.
 /*
int blocking_notifier_chain_unregister(struct blocking_notifier_headnh,
		struct notifier_blockn)
{
	int ret;

	*/
	 This code gets used during boot-up, when task switching is
	 not yet working and interrupts must remain disabled.  At
	 such times we must not call down_write().
	 /*
	if (unlikely(system_state == SYSTEM_BOOTING))
		return notifier_chain_unregister(&nh->head, n);

	down_write(&nh->rwsem);
	ret = notifier_chain_unregister(&nh->head, n);
	up_write(&nh->rwsem);
	return ret;
}
EXPORT_SYMBOL_GPL(blocking_notifier_chain_unregister);

*/
	__blocking_notifier_call_chain - Call functions in a blocking notifier chain
	@nh: Pointer to head of the blocking notifier chain
	@val: Value passed unmodified to notifier function
	@v: Pointer passed unmodified to notifier function
	@nr_to_call: See comment for notifier_call_chain.
	@nr_calls: See comment for notifier_call_chain.

	Calls each function in a notifier chain in turn.  The functions
	run in a process context, so they are allowed to block.

	If the return value of the notifier can be and'ed
	with %NOTIFY_STOP_MASK then blocking_notifier_call_chain()
	will return immediately, with the return value of
	the notifier function which halted execution.
	Otherwise the return value is the return value
	of the last notifier function called.
 /*
int __blocking_notifier_call_chain(struct blocking_notifier_headnh,
				   unsigned long val, voidv,
				   int nr_to_call, intnr_calls)
{
	int ret = NOTIFY_DONE;

	*/
	 We check the head outside the lock, but if this access is
	 racy then it does not matter what the result of the test
	 is, we re-check the list after having taken the lock anyway:
	 /*
	if (rcu_access_pointer(nh->head)) {
		down_read(&nh->rwsem);
		ret = notifier_call_chain(&nh->head, val, v, nr_to_call,
					nr_calls);
		up_read(&nh->rwsem);
	}
	return ret;
}
EXPORT_SYMBOL_GPL(__blocking_notifier_call_chain);

int blocking_notifier_call_chain(struct blocking_notifier_headnh,
		unsigned long val, voidv)
{
	return __blocking_notifier_call_chain(nh, val, v, -1, NULL);
}
EXPORT_SYMBOL_GPL(blocking_notifier_call_chain);

*/
	Raw notifier chain routines.  There is no protection;
	the caller must provide it.  Use at your own risk!
 /*

*/
	raw_notifier_chain_register - Add notifier to a raw notifier chain
	@nh: Pointer to head of the raw notifier chain
	@n: New entry in notifier chain

	Adds a notifier to a raw notifier chain.
	All locking must be provided by the caller.

	Currently always returns zero.
 /*
int raw_notifier_chain_register(struct raw_notifier_headnh,
		struct notifier_blockn)
{
	return notifier_chain_register(&nh->head, n);
}
EXPORT_SYMBOL_GPL(raw_notifier_chain_register);

*/
	raw_notifier_chain_unregister - Remove notifier from a raw notifier chain
	@nh: Pointer to head of the raw notifier chain
	@n: Entry to remove from notifier chain

	Removes a notifier from a raw notifier chain.
	All locking must be provided by the caller.

	Returns zero on success or %-ENOENT on failure.
 /*
int raw_notifier_chain_unregister(struct raw_notifier_headnh,
		struct notifier_blockn)
{
	return notifier_chain_unregister(&nh->head, n);
}
EXPORT_SYMBOL_GPL(raw_notifier_chain_unregister);

*/
	__raw_notifier_call_chain - Call functions in a raw notifier chain
	@nh: Pointer to head of the raw notifier chain
	@val: Value passed unmodified to notifier function
	@v: Pointer passed unmodified to notifier function
	@nr_to_call: See comment for notifier_call_chain.
	@nr_calls: See comment for notifier_call_chain

	Calls each function in a notifier chain in turn.  The functions
	run in an undefined context.
	All locking must be provided by the caller.

	If the return value of the notifier can be and'ed
	with %NOTIFY_STOP_MASK then raw_notifier_call_chain()
	will return immediately, with the return value of
	the notifier function which halted execution.
	Otherwise the return value is the return value
	of the last notifier function called.
 /*
int __raw_notifier_call_chain(struct raw_notifier_headnh,
			      unsigned long val, voidv,
			      int nr_to_call, intnr_calls)
{
	return notifier_call_chain(&nh->head, val, v, nr_to_call, nr_calls);
}
EXPORT_SYMBOL_GPL(__raw_notifier_call_chain);

int raw_notifier_call_chain(struct raw_notifier_headnh,
		unsigned long val, voidv)
{
	return __raw_notifier_call_chain(nh, val, v, -1, NULL);
}
EXPORT_SYMBOL_GPL(raw_notifier_call_chain);

#ifdef CONFIG_SRCU
*/
	SRCU notifier chain routines.    Registration and unregistration
	use a mutex, and call_chain is synchronized by SRCU (no locks).
 /*

*/
	srcu_notifier_chain_register - Add notifier to an SRCU notifier chain
	@nh: Pointer to head of the SRCU notifier chain
	@n: New entry in notifier chain

	Adds a notifier to an SRCU notifier chain.
	Must be called in process context.

	Currently always returns zero.
 /*
int srcu_notifier_chain_register(struct srcu_notifier_headnh,
		struct notifier_blockn)
{
	int ret;

	*/
	 This code gets used during boot-up, when task switching is
	 not yet working and interrupts must remain disabled.  At
	 such times we must not call mutex_lock().
	 /*
	if (unlikely(system_state == SYSTEM_BOOTING))
		return notifier_chain_register(&nh->head, n);

	mutex_lock(&nh->mutex);
	ret = notifier_chain_register(&nh->head, n);
	mutex_unlock(&nh->mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(srcu_notifier_chain_register);

*/
	srcu_notifier_chain_unregister - Remove notifier from an SRCU notifier chain
	@nh: Pointer to head of the SRCU notifier chain
	@n: Entry to remove from notifier chain

	Removes a notifier from an SRCU notifier chain.
	Must be called from process context.

	Returns zero on success or %-ENOENT on failure.
 /*
int srcu_notifier_chain_unregister(struct srcu_notifier_headnh,
		struct notifier_blockn)
{
	int ret;

	*/
	 This code gets used during boot-up, when task switching is
	 not yet working and interrupts must remain disabled.  At
	 such times we must not call mutex_lock().
	 /*
	if (unlikely(system_state == SYSTEM_BOOTING))
		return notifier_chain_unregister(&nh->head, n);

	mutex_lock(&nh->mutex);
	ret = notifier_chain_unregister(&nh->head, n);
	mutex_unlock(&nh->mutex);
	synchronize_srcu(&nh->srcu);
	return ret;
}
EXPORT_SYMBOL_GPL(srcu_notifier_chain_unregister);

*/
	__srcu_notifier_call_chain - Call functions in an SRCU notifier chain
	@nh: Pointer to head of the SRCU notifier chain
	@val: Value passed unmodified to notifier function
	@v: Pointer passed unmodified to notifier function
	@nr_to_call: See comment for notifier_call_chain.
	@nr_calls: See comment for notifier_call_chain

	Calls each function in a notifier chain in turn.  The functions
	run in a process context, so they are allowed to block.

	If the return value of the notifier can be and'ed
	with %NOTIFY_STOP_MASK then srcu_notifier_call_chain()
	will return immediately, with the return value of
	the notifier function which halted execution.
	Otherwise the return value is the return value
	of the last notifier function called.
 /*
int __srcu_notifier_call_chain(struct srcu_notifier_headnh,
			       unsigned long val, voidv,
			       int nr_to_call, intnr_calls)
{
	int ret;
	int idx;

	idx = srcu_read_lock(&nh->srcu);
	ret = notifier_call_chain(&nh->head, val, v, nr_to_call, nr_calls);
	srcu_read_unlock(&nh->srcu, idx);
	return ret;
}
EXPORT_SYMBOL_GPL(__srcu_notifier_call_chain);

int srcu_notifier_call_chain(struct srcu_notifier_headnh,
		unsigned long val, voidv)
{
	return __srcu_notifier_call_chain(nh, val, v, -1, NULL);
}
EXPORT_SYMBOL_GPL(srcu_notifier_call_chain);

*/
	srcu_init_notifier_head - Initialize an SRCU notifier head
	@nh: Pointer to head of the srcu notifier chain

	Unlike other sorts of notifier heads, SRCU notifier heads require
	dynamic initialization.  Be sure to call this routine before
	calling any of the other SRCU notifier routines for this head.

	If an SRCU notifier head is deallocated, it must first be cleaned
	up by calling srcu_cleanup_notifier_head().  Otherwise the head's
	per-cpu data (used by the SRCU mechanism) will leak.
 /*
void srcu_init_notifier_head(struct srcu_notifier_headnh)
{
	mutex_init(&nh->mutex);
	if (init_srcu_struct(&nh->srcu) < 0)
		BUG();
	nh->head = NULL;
}
EXPORT_SYMBOL_GPL(srcu_init_notifier_head);

#endif */ CONFIG_SRCU /*

static ATOMIC_NOTIFIER_HEAD(die_chain);

int notrace notify_die(enum die_val val, const charstr,
	       struct pt_regsregs, long err, int trap, int sig)
{
	struct die_args args = {
		.regs	= regs,
		.str	= str,
		.err	= err,
		.trapnr	= trap,
		.signr	= sig,

	};
	RCU_LOCKDEP_WARN(!rcu_is_watching(),
			   "notify_die called but RCU thinks we're quiescent");
	return atomic_notifier_call_chain(&die_chain, val, &args);
}
NOKPROBE_SYMBOL(notify_die);

int register_die_notifier(struct notifier_blocknb)
{
	vmalloc_sync_all();
	return atomic_notifier_chain_register(&die_chain, nb);
}
EXPORT_SYMBOL_GPL(register_die_notifier);

int unregister_die_notifier(struct notifier_blocknb)
{
	return atomic_notifier_chain_unregister(&die_chain, nb);
}
EXPORT_SYMBOL_GPL(unregister_die_notifier);
*/

  Copyright (C) 2006 IBM Corporation

  Author: Serge Hallyn <serue@us.ibm.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, version 2 of the
  License.

  Jun 2006 - namespaces support
             OpenVZ, SWsoft Inc.
             Pavel Emelianov <xemul@openvz.org>
 /*

#include <linux/slab.h>
#include <linux/export.h>
#include <linux/nsproxy.h>
#include <linux/init_task.h>
#include <linux/mnt_namespace.h>
#include <linux/utsname.h>
#include <linux/pid_namespace.h>
#include <net/net_namespace.h>
#include <linux/ipc_namespace.h>
#include <linux/proc_ns.h>
#include <linux/file.h>
#include <linux/syscalls.h>
#include <linux/cgroup.h>

static struct kmem_cachensproxy_cachep;

struct nsproxy init_nsproxy = {
	.count			= ATOMIC_INIT(1),
	.uts_ns			= &init_uts_ns,
#if defined(CONFIG_POSIX_MQUEUE) || defined(CONFIG_SYSVIPC)
	.ipc_ns			= &init_ipc_ns,
#endif
	.mnt_ns			= NULL,
	.pid_ns_for_children	= &init_pid_ns,
#ifdef CONFIG_NET
	.net_ns			= &init_net,
#endif
#ifdef CONFIG_CGROUPS
	.cgroup_ns		= &init_cgroup_ns,
#endif
};

static inline struct nsproxycreate_nsproxy(void)
{
	struct nsproxynsproxy;

	nsproxy = kmem_cache_alloc(nsproxy_cachep, GFP_KERNEL);
	if (nsproxy)
		atomic_set(&nsproxy->count, 1);
	return nsproxy;
}

*/
 Create new nsproxy and all of its the associated namespaces.
 Return the newly created nsproxy.  Do not attach this to the task,
 leave it to the caller to do proper locking and attach it to task.
 /*
static struct nsproxycreate_new_namespaces(unsigned long flags,
	struct task_structtsk, struct user_namespaceuser_ns,
	struct fs_structnew_fs)
{
	struct nsproxynew_nsp;
	int err;

	new_nsp = create_nsproxy();
	if (!new_nsp)
		return ERR_PTR(-ENOMEM);

	new_nsp->mnt_ns = copy_mnt_ns(flags, tsk->nsproxy->mnt_ns, user_ns, new_fs);
	if (IS_ERR(new_nsp->mnt_ns)) {
		err = PTR_ERR(new_nsp->mnt_ns);
		goto out_ns;
	}

	new_nsp->uts_ns = copy_utsname(flags, user_ns, tsk->nsproxy->uts_ns);
	if (IS_ERR(new_nsp->uts_ns)) {
		err = PTR_ERR(new_nsp->uts_ns);
		goto out_uts;
	}

	new_nsp->ipc_ns = copy_ipcs(flags, user_ns, tsk->nsproxy->ipc_ns);
	if (IS_ERR(new_nsp->ipc_ns)) {
		err = PTR_ERR(new_nsp->ipc_ns);
		goto out_ipc;
	}

	new_nsp->pid_ns_for_children =
		copy_pid_ns(flags, user_ns, tsk->nsproxy->pid_ns_for_children);
	if (IS_ERR(new_nsp->pid_ns_for_children)) {
		err = PTR_ERR(new_nsp->pid_ns_for_children);
		goto out_pid;
	}

	new_nsp->cgroup_ns = copy_cgroup_ns(flags, user_ns,
					    tsk->nsproxy->cgroup_ns);
	if (IS_ERR(new_nsp->cgroup_ns)) {
		err = PTR_ERR(new_nsp->cgroup_ns);
		goto out_cgroup;
	}

	new_nsp->net_ns = copy_net_ns(flags, user_ns, tsk->nsproxy->net_ns);
	if (IS_ERR(new_nsp->net_ns)) {
		err = PTR_ERR(new_nsp->net_ns);
		goto out_net;
	}

	return new_nsp;

out_net:
	put_cgroup_ns(new_nsp->cgroup_ns);
out_cgroup:
	if (new_nsp->pid_ns_for_children)
		put_pid_ns(new_nsp->pid_ns_for_children);
out_pid:
	if (new_nsp->ipc_ns)
		put_ipc_ns(new_nsp->ipc_ns);
out_ipc:
	if (new_nsp->uts_ns)
		put_uts_ns(new_nsp->uts_ns);
out_uts:
	if (new_nsp->mnt_ns)
		put_mnt_ns(new_nsp->mnt_ns);
out_ns:
	kmem_cache_free(nsproxy_cachep, new_nsp);
	return ERR_PTR(err);
}

*/
 called from clone.  This now handles copy for nsproxy and all
 namespaces therein.
 /*
int copy_namespaces(unsigned long flags, struct task_structtsk)
{
	struct nsproxyold_ns = tsk->nsproxy;
	struct user_namespaceuser_ns = task_cred_xxx(tsk, user_ns);
	struct nsproxynew_ns;

	if (likely(!(flags & (CLONE_NEWNS | CLONE_NEWUTS | CLONE_NEWIPC |
			      CLONE_NEWPID | CLONE_NEWNET |
			      CLONE_NEWCGROUP)))) {
		get_nsproxy(old_ns);
		return 0;
	}

	if (!ns_capable(user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	*/
	 CLONE_NEWIPC must detach from the undolist: after switching
	 to a new ipc namespace, the semaphore arrays from the old
	 namespace are unreachable.  In clone parlance, CLONE_SYSVSEM
	 means share undolist with parent, so we must forbid using
	 it along with CLONE_NEWIPC.
	 /*
	if ((flags & (CLONE_NEWIPC | CLONE_SYSVSEM)) ==
		(CLONE_NEWIPC | CLONE_SYSVSEM)) 
		return -EINVAL;

	new_ns = create_new_namespaces(flags, tsk, user_ns, tsk->fs);
	if (IS_ERR(new_ns))
		return  PTR_ERR(new_ns);

	tsk->nsproxy = new_ns;
	return 0;
}

void free_nsproxy(struct nsproxyns)
{
	if (ns->mnt_ns)
		put_mnt_ns(ns->mnt_ns);
	if (ns->uts_ns)
		put_uts_ns(ns->uts_ns);
	if (ns->ipc_ns)
		put_ipc_ns(ns->ipc_ns);
	if (ns->pid_ns_for_children)
		put_pid_ns(ns->pid_ns_for_children);
	put_cgroup_ns(ns->cgroup_ns);
	put_net(ns->net_ns);
	kmem_cache_free(nsproxy_cachep, ns);
}

*/
 Called from unshare. Unshare all the namespaces part of nsproxy.
 On success, returns the new nsproxy.
 /*
int unshare_nsproxy_namespaces(unsigned long unshare_flags,
	struct nsproxy*new_nsp, struct crednew_cred, struct fs_structnew_fs)
{
	struct user_namespaceuser_ns;
	int err = 0;

	if (!(unshare_flags & (CLONE_NEWNS | CLONE_NEWUTS | CLONE_NEWIPC |
			       CLONE_NEWNET | CLONE_NEWPID | CLONE_NEWCGROUP)))
		return 0;

	user_ns = new_cred ? new_cred->user_ns : current_user_ns();
	if (!ns_capable(user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	*new_nsp = create_new_namespaces(unshare_flags, current, user_ns,
					 new_fs ? new_fs : current->fs);
	if (IS_ERR(*new_nsp)) {
		err = PTR_ERR(*new_nsp);
		goto out;
	}

out:
	return err;
}

void switch_task_namespaces(struct task_structp, struct nsproxynew)
{
	struct nsproxyns;

	might_sleep();

	task_lock(p);
	ns = p->nsproxy;
	p->nsproxy = new;
	task_unlock(p);

	if (ns && atomic_dec_and_test(&ns->count))
		free_nsproxy(ns);
}

void exit_task_namespaces(struct task_structp)
{
	switch_task_namespaces(p, NULL);
}

SYSCALL_DEFINE2(setns, int, fd, int, nstype)
{
	struct task_structtsk = current;
	struct nsproxynew_nsproxy;
	struct filefile;
	struct ns_commonns;
	int err;

	file = proc_ns_fget(fd);
	if (IS_ERR(file))
		return PTR_ERR(file);

	err = -EINVAL;
	ns = get_proc_ns(file_inode(file));
	if (nstype && (ns->ops->type != nstype))
		goto out;

	new_nsproxy = create_new_namespaces(0, tsk, current_user_ns(), tsk->fs);
	if (IS_ERR(new_nsproxy)) {
		err = PTR_ERR(new_nsproxy);
		goto out;
	}

	err = ns->ops->install(new_nsproxy, ns);
	if (err) {
		free_nsproxy(new_nsproxy);
		goto out;
	}
	switch_task_namespaces(tsk, new_nsproxy);
out:
	fput(file);
	return err;
}

int __init nsproxy_cache_init(void)
{
	nsproxy_cachep = KMEM_CACHE(nsproxy, SLAB_PANIC);
	return 0;
}
*/

 padata.c - generic interface to process data streams in parallel

 See Documentation/padata.txt for an api documentation.

 Copyright (C) 2008, 2009 secunet Security Networks AG
 Copyright (C) 2008, 2009 Steffen Klassert <steffen.klassert@secunet.com>

 This program is free software; you can redistribute it and/or modify it
 under the terms and conditions of the GNU General Public License,
 version 2, as published by the Free Software Foundation.

 This program is distributed in the hope it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 /*

#include <linux/export.h>
#include <linux/cpumask.h>
#include <linux/err.h>
#include <linux/cpu.h>
#include <linux/padata.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/rcupdate.h>

#define MAX_OBJ_NUM 1000

static int padata_index_to_cpu(struct parallel_datapd, int cpu_index)
{
	int cpu, target_cpu;

	target_cpu = cpumask_first(pd->cpumask.pcpu);
	for (cpu = 0; cpu < cpu_index; cpu++)
		target_cpu = cpumask_next(target_cpu, pd->cpumask.pcpu);

	return target_cpu;
}

static int padata_cpu_hash(struct parallel_datapd)
{
	unsigned int seq_nr;
	int cpu_index;

	*/
	 Hash the sequence numbers to the cpus by taking
	 seq_nr mod. number of cpus in use.
	 /*

	seq_nr = atomic_inc_return(&pd->seq_nr);
	cpu_index = seq_nr % cpumask_weight(pd->cpumask.pcpu);

	return padata_index_to_cpu(pd, cpu_index);
}

static void padata_parallel_worker(struct work_structparallel_work)
{
	struct padata_parallel_queuepqueue;
	struct parallel_datapd;
	struct padata_instancepinst;
	LIST_HEAD(local_list);

	local_bh_disable();
	pqueue = container_of(parallel_work,
			      struct padata_parallel_queue, work);
	pd = pqueue->pd;
	pinst = pd->pinst;

	spin_lock(&pqueue->parallel.lock);
	list_replace_init(&pqueue->parallel.list, &local_list);
	spin_unlock(&pqueue->parallel.lock);

	while (!list_empty(&local_list)) {
		struct padata_privpadata;

		padata = list_entry(local_list.next,
				    struct padata_priv, list);

		list_del_init(&padata->list);

		padata->parallel(padata);
	}

	local_bh_enable();
}

*/
 padata_do_parallel - padata parallelization function

 @pinst: padata instance
 @padata: object to be parallelized
 @cb_cpu: cpu the serialization callback function will run on,
          must be in the serial cpumask of padata(i.e. cpumask.cbcpu).

 The parallelization callback function will run with BHs off.
 Note: Every object which is parallelized by padata_do_parallel
 must be seen by padata_do_serial.
 /*
int padata_do_parallel(struct padata_instancepinst,
		       struct padata_privpadata, int cb_cpu)
{
	int target_cpu, err;
	struct padata_parallel_queuequeue;
	struct parallel_datapd;

	rcu_read_lock_bh();

	pd = rcu_dereference_bh(pinst->pd);

	err = -EINVAL;
	if (!(pinst->flags & PADATA_INIT) || pinst->flags & PADATA_INVALID)
		goto out;

	if (!cpumask_test_cpu(cb_cpu, pd->cpumask.cbcpu))
		goto out;

	err =  -EBUSY;
	if ((pinst->flags & PADATA_RESET))
		goto out;

	if (atomic_read(&pd->refcnt) >= MAX_OBJ_NUM)
		goto out;

	err = 0;
	atomic_inc(&pd->refcnt);
	padata->pd = pd;
	padata->cb_cpu = cb_cpu;

	target_cpu = padata_cpu_hash(pd);
	queue = per_cpu_ptr(pd->pqueue, target_cpu);

	spin_lock(&queue->parallel.lock);
	list_add_tail(&padata->list, &queue->parallel.list);
	spin_unlock(&queue->parallel.lock);

	queue_work_on(target_cpu, pinst->wq, &queue->work);

out:
	rcu_read_unlock_bh();

	return err;
}
EXPORT_SYMBOL(padata_do_parallel);

*/
 padata_get_next - Get the next object that needs serialization.

 Return values are:

 A pointer to the control struct of the next object that needs
 serialization, if present in one of the percpu reorder queues.

 NULL, if all percpu reorder queues are empty.

 -EINPROGRESS, if the next object that needs serialization will
  be parallel processed by another cpu and is not yet present in
  the cpu's reorder queue.

 -ENODATA, if this cpu has to do the parallel processing for
  the next object.
 /*
static struct padata_privpadata_get_next(struct parallel_datapd)
{
	int cpu, num_cpus;
	unsigned int next_nr, next_index;
	struct padata_parallel_queuenext_queue;
	struct padata_privpadata;
	struct padata_listreorder;

	num_cpus = cpumask_weight(pd->cpumask.pcpu);

	*/
	 Calculate the percpu reorder queue and the sequence
	 number of the next object.
	 /*
	next_nr = pd->processed;
	next_index = next_nr % num_cpus;
	cpu = padata_index_to_cpu(pd, next_index);
	next_queue = per_cpu_ptr(pd->pqueue, cpu);

	padata = NULL;

	reorder = &next_queue->reorder;

	if (!list_empty(&reorder->list)) {
		padata = list_entry(reorder->list.next,
				    struct padata_priv, list);

		spin_lock(&reorder->lock);
		list_del_init(&padata->list);
		atomic_dec(&pd->reorder_objects);
		spin_unlock(&reorder->lock);

		pd->processed++;

		goto out;
	}

	if (__this_cpu_read(pd->pqueue->cpu_index) == next_queue->cpu_index) {
		padata = ERR_PTR(-ENODATA);
		goto out;
	}

	padata = ERR_PTR(-EINPROGRESS);
out:
	return padata;
}

static void padata_reorder(struct parallel_datapd)
{
	int cb_cpu;
	struct padata_privpadata;
	struct padata_serial_queuesqueue;
	struct padata_instancepinst = pd->pinst;

	*/
	 We need to ensure that only one cpu can work on dequeueing of
	 the reorder queue the time. Calculating in which percpu reorder
	 queue the next object will arrive takes some time. A spinlock
	 would be highly contended. Also it is not clear in which order
	 the objects arrive to the reorder queues. So a cpu could wait to
	 get the lock just to notice that there is nothing to do at the
	 moment. Therefore we use a trylock and let the holder of the lock
	 care for all the objects enqueued during the holdtime of the lock.
	 /*
	if (!spin_trylock_bh(&pd->lock))
		return;

	while (1) {
		padata = padata_get_next(pd);

		*/
		 All reorder queues are empty, or the next object that needs
		 serialization is parallel processed by another cpu and is
		 still on it's way to the cpu's reorder queue, nothing to
		 do for now.
		 /*
		if (!padata || PTR_ERR(padata) == -EINPROGRESS)
			break;

		*/
		 This cpu has to do the parallel processing of the next
		 object. It's waiting in the cpu's parallelization queue,
		 so exit immediately.
		 /*
		if (PTR_ERR(padata) == -ENODATA) {
			del_timer(&pd->timer);
			spin_unlock_bh(&pd->lock);
			return;
		}

		cb_cpu = padata->cb_cpu;
		squeue = per_cpu_ptr(pd->squeue, cb_cpu);

		spin_lock(&squeue->serial.lock);
		list_add_tail(&padata->list, &squeue->serial.list);
		spin_unlock(&squeue->serial.lock);

		queue_work_on(cb_cpu, pinst->wq, &squeue->work);
	}

	spin_unlock_bh(&pd->lock);

	*/
	 The next object that needs serialization might have arrived to
	 the reorder queues in the meantime, we will be called again
	 from the timer function if no one else cares for it.
	 /*
	if (atomic_read(&pd->reorder_objects)
			&& !(pinst->flags & PADATA_RESET))
		mod_timer(&pd->timer, jiffies + HZ);
	else
		del_timer(&pd->timer);

	return;
}

static void padata_reorder_timer(unsigned long arg)
{
	struct parallel_datapd = (struct parallel_data)arg;

	padata_reorder(pd);
}

static void padata_serial_worker(struct work_structserial_work)
{
	struct padata_serial_queuesqueue;
	struct parallel_datapd;
	LIST_HEAD(local_list);

	local_bh_disable();
	squeue = container_of(serial_work, struct padata_serial_queue, work);
	pd = squeue->pd;

	spin_lock(&squeue->serial.lock);
	list_replace_init(&squeue->serial.list, &local_list);
	spin_unlock(&squeue->serial.lock);

	while (!list_empty(&local_list)) {
		struct padata_privpadata;

		padata = list_entry(local_list.next,
				    struct padata_priv, list);

		list_del_init(&padata->list);

		padata->serial(padata);
		atomic_dec(&pd->refcnt);
	}
	local_bh_enable();
}

*/
 padata_do_serial - padata serialization function

 @padata: object to be serialized.

 padata_do_serial must be called for every parallelized object.
 The serialization callback function will run with BHs off.
 /*
void padata_do_serial(struct padata_privpadata)
{
	int cpu;
	struct padata_parallel_queuepqueue;
	struct parallel_datapd;

	pd = padata->pd;

	cpu = get_cpu();
	pqueue = per_cpu_ptr(pd->pqueue, cpu);

	spin_lock(&pqueue->reorder.lock);
	atomic_inc(&pd->reorder_objects);
	list_add_tail(&padata->list, &pqueue->reorder.list);
	spin_unlock(&pqueue->reorder.lock);

	put_cpu();

	padata_reorder(pd);
}
EXPORT_SYMBOL(padata_do_serial);

static int padata_setup_cpumasks(struct parallel_datapd,
				 const struct cpumaskpcpumask,
				 const struct cpumaskcbcpumask)
{
	if (!alloc_cpumask_var(&pd->cpumask.pcpu, GFP_KERNEL))
		return -ENOMEM;

	cpumask_and(pd->cpumask.pcpu, pcpumask, cpu_online_mask);
	if (!alloc_cpumask_var(&pd->cpumask.cbcpu, GFP_KERNEL)) {
		free_cpumask_var(pd->cpumask.cbcpu);
		return -ENOMEM;
	}

	cpumask_and(pd->cpumask.cbcpu, cbcpumask, cpu_online_mask);
	return 0;
}

static void __padata_list_init(struct padata_listpd_list)
{
	INIT_LIST_HEAD(&pd_list->list);
	spin_lock_init(&pd_list->lock);
}

*/ Initialize all percpu queues used by serial workers /*
static void padata_init_squeues(struct parallel_datapd)
{
	int cpu;
	struct padata_serial_queuesqueue;

	for_each_cpu(cpu, pd->cpumask.cbcpu) {
		squeue = per_cpu_ptr(pd->squeue, cpu);
		squeue->pd = pd;
		__padata_list_init(&squeue->serial);
		INIT_WORK(&squeue->work, padata_serial_worker);
	}
}

*/ Initialize all percpu queues used by parallel workers /*
static void padata_init_pqueues(struct parallel_datapd)
{
	int cpu_index, cpu;
	struct padata_parallel_queuepqueue;

	cpu_index = 0;
	for_each_cpu(cpu, pd->cpumask.pcpu) {
		pqueue = per_cpu_ptr(pd->pqueue, cpu);
		pqueue->pd = pd;
		pqueue->cpu_index = cpu_index;
		cpu_index++;

		__padata_list_init(&pqueue->reorder);
		__padata_list_init(&pqueue->parallel);
		INIT_WORK(&pqueue->work, padata_parallel_worker);
		atomic_set(&pqueue->num_obj, 0);
	}
}

*/ Allocate and initialize the internal cpumask dependend resources. /*
static struct parallel_datapadata_alloc_pd(struct padata_instancepinst,
					     const struct cpumaskpcpumask,
					     const struct cpumaskcbcpumask)
{
	struct parallel_datapd;

	pd = kzalloc(sizeof(struct parallel_data), GFP_KERNEL);
	if (!pd)
		goto err;

	pd->pqueue = alloc_percpu(struct padata_parallel_queue);
	if (!pd->pqueue)
		goto err_free_pd;

	pd->squeue = alloc_percpu(struct padata_serial_queue);
	if (!pd->squeue)
		goto err_free_pqueue;
	if (padata_setup_cpumasks(pd, pcpumask, cbcpumask) < 0)
		goto err_free_squeue;

	padata_init_pqueues(pd);
	padata_init_squeues(pd);
	setup_timer(&pd->timer, padata_reorder_timer, (unsigned long)pd);
	atomic_set(&pd->seq_nr, -1);
	atomic_set(&pd->reorder_objects, 0);
	atomic_set(&pd->refcnt, 0);
	pd->pinst = pinst;
	spin_lock_init(&pd->lock);

	return pd;

err_free_squeue:
	free_percpu(pd->squeue);
err_free_pqueue:
	free_percpu(pd->pqueue);
err_free_pd:
	kfree(pd);
err:
	return NULL;
}

static void padata_free_pd(struct parallel_datapd)
{
	free_cpumask_var(pd->cpumask.pcpu);
	free_cpumask_var(pd->cpumask.cbcpu);
	free_percpu(pd->pqueue);
	free_percpu(pd->squeue);
	kfree(pd);
}

*/ Flush all objects out of the padata queues. /*
static void padata_flush_queues(struct parallel_datapd)
{
	int cpu;
	struct padata_parallel_queuepqueue;
	struct padata_serial_queuesqueue;

	for_each_cpu(cpu, pd->cpumask.pcpu) {
		pqueue = per_cpu_ptr(pd->pqueue, cpu);
		flush_work(&pqueue->work);
	}

	del_timer_sync(&pd->timer);

	if (atomic_read(&pd->reorder_objects))
		padata_reorder(pd);

	for_each_cpu(cpu, pd->cpumask.cbcpu) {
		squeue = per_cpu_ptr(pd->squeue, cpu);
		flush_work(&squeue->work);
	}

	BUG_ON(atomic_read(&pd->refcnt) != 0);
}

static void __padata_start(struct padata_instancepinst)
{
	pinst->flags |= PADATA_INIT;
}

static void __padata_stop(struct padata_instancepinst)
{
	if (!(pinst->flags & PADATA_INIT))
		return;

	pinst->flags &= ~PADATA_INIT;

	synchronize_rcu();

	get_online_cpus();
	padata_flush_queues(pinst->pd);
	put_online_cpus();
}

*/ Replace the internal control structure with a new one. /*
static void padata_replace(struct padata_instancepinst,
			   struct parallel_datapd_new)
{
	struct parallel_datapd_old = pinst->pd;
	int notification_mask = 0;

	pinst->flags |= PADATA_RESET;

	rcu_assign_pointer(pinst->pd, pd_new);

	synchronize_rcu();

	if (!cpumask_equal(pd_old->cpumask.pcpu, pd_new->cpumask.pcpu))
		notification_mask |= PADATA_CPU_PARALLEL;
	if (!cpumask_equal(pd_old->cpumask.cbcpu, pd_new->cpumask.cbcpu))
		notification_mask |= PADATA_CPU_SERIAL;

	padata_flush_queues(pd_old);
	padata_free_pd(pd_old);

	if (notification_mask)
		blocking_notifier_call_chain(&pinst->cpumask_change_notifier,
					     notification_mask,
					     &pd_new->cpumask);

	pinst->flags &= ~PADATA_RESET;
}

*/
 padata_register_cpumask_notifier - Registers a notifier that will be called
                             if either pcpu or cbcpu or both cpumasks change.

 @pinst: A poineter to padata instance
 @nblock: A pointer to notifier block.
 /*
int padata_register_cpumask_notifier(struct padata_instancepinst,
				     struct notifier_blocknblock)
{
	return blocking_notifier_chain_register(&pinst->cpumask_change_notifier,
						nblock);
}
EXPORT_SYMBOL(padata_register_cpumask_notifier);

*/
 padata_unregister_cpumask_notifier - Unregisters cpumask notifier
        registered earlier  using padata_register_cpumask_notifier

 @pinst: A pointer to data instance.
 @nlock: A pointer to notifier block.
 /*
int padata_unregister_cpumask_notifier(struct padata_instancepinst,
				       struct notifier_blocknblock)
{
	return blocking_notifier_chain_unregister(
		&pinst->cpumask_change_notifier,
		nblock);
}
EXPORT_SYMBOL(padata_unregister_cpumask_notifier);


*/ If cpumask contains no active cpu, we mark the instance as invalid. /*
static bool padata_validate_cpumask(struct padata_instancepinst,
				    const struct cpumaskcpumask)
{
	if (!cpumask_intersects(cpumask, cpu_online_mask)) {
		pinst->flags |= PADATA_INVALID;
		return false;
	}

	pinst->flags &= ~PADATA_INVALID;
	return true;
}

static int __padata_set_cpumasks(struct padata_instancepinst,
				 cpumask_var_t pcpumask,
				 cpumask_var_t cbcpumask)
{
	int valid;
	struct parallel_datapd;

	valid = padata_validate_cpumask(pinst, pcpumask);
	if (!valid) {
		__padata_stop(pinst);
		goto out_replace;
	}

	valid = padata_validate_cpumask(pinst, cbcpumask);
	if (!valid)
		__padata_stop(pinst);

out_replace:
	pd = padata_alloc_pd(pinst, pcpumask, cbcpumask);
	if (!pd)
		return -ENOMEM;

	cpumask_copy(pinst->cpumask.pcpu, pcpumask);
	cpumask_copy(pinst->cpumask.cbcpu, cbcpumask);

	padata_replace(pinst, pd);

	if (valid)
		__padata_start(pinst);

	return 0;
}

*/
 padata_set_cpumasks - Set both parallel and serial cpumasks. The first
                       one is used by parallel workers and the second one
                       by the wokers doing serialization.

 @pinst: padata instance
 @pcpumask: the cpumask to use for parallel workers
 @cbcpumask: the cpumsak to use for serial workers
 /*
int padata_set_cpumasks(struct padata_instancepinst, cpumask_var_t pcpumask,
			cpumask_var_t cbcpumask)
{
	int err;

	mutex_lock(&pinst->lock);
	get_online_cpus();

	err = __padata_set_cpumasks(pinst, pcpumask, cbcpumask);

	put_online_cpus();
	mutex_unlock(&pinst->lock);

	return err;

}
EXPORT_SYMBOL(padata_set_cpumasks);

*/
 padata_set_cpumask: Sets specified by @cpumask_type cpumask to the value
                     equivalent to @cpumask.

 @pinst: padata instance
 @cpumask_type: PADATA_CPU_SERIAL or PADATA_CPU_PARALLEL corresponding
                to parallel and serial cpumasks respectively.
 @cpumask: the cpumask to use
 /*
int padata_set_cpumask(struct padata_instancepinst, int cpumask_type,
		       cpumask_var_t cpumask)
{
	struct cpumaskserial_mask,parallel_mask;
	int err = -EINVAL;

	mutex_lock(&pinst->lock);
	get_online_cpus();

	switch (cpumask_type) {
	case PADATA_CPU_PARALLEL:
		serial_mask = pinst->cpumask.cbcpu;
		parallel_mask = cpumask;
		break;
	case PADATA_CPU_SERIAL:
		parallel_mask = pinst->cpumask.pcpu;
		serial_mask = cpumask;
		break;
	default:
		 goto out;
	}

	err =  __padata_set_cpumasks(pinst, parallel_mask, serial_mask);

out:
	put_online_cpus();
	mutex_unlock(&pinst->lock);

	return err;
}
EXPORT_SYMBOL(padata_set_cpumask);

static int __padata_add_cpu(struct padata_instancepinst, int cpu)
{
	struct parallel_datapd;

	if (cpumask_test_cpu(cpu, cpu_online_mask)) {
		pd = padata_alloc_pd(pinst, pinst->cpumask.pcpu,
				     pinst->cpumask.cbcpu);
		if (!pd)
			return -ENOMEM;

		padata_replace(pinst, pd);

		if (padata_validate_cpumask(pinst, pinst->cpumask.pcpu) &&
		    padata_validate_cpumask(pinst, pinst->cpumask.cbcpu))
			__padata_start(pinst);
	}

	return 0;
}

/*
 padata_add_cpu - add a cpu to one or both(parallel and serial)
                  padata cpumasks.

 @pinst: padata instance
 @cpu: cpu to add
 @mask: bitmask of flags specifying to which cpumask @cpu shuld be added.
        The @mask may be any combination of the following flags:
          PADATA_CPU_SERIAL   - serial cpumask
          PADATA_CPU_PARALLEL - parallel cpumask
 /*

int padata_add_cpu(struct padata_instancepinst, int cpu, int mask)
{
	int err;

	if (!(mask & (PADATA_CPU_SERIAL | PADATA_CPU_PARALLEL)))
		return -EINVAL;

	mutex_lock(&pinst->lock);

	get_online_cpus();
	if (mask & PADATA_CPU_SERIAL)
		cpumask_set_cpu(cpu, pinst->cpumask.cbcpu);
	if (mask & PADATA_CPU_PARALLEL)
		cpumask_set_cpu(cpu, pinst->cpumask.pcpu);

	err = __padata_add_cpu(pinst, cpu);
	put_online_cpus();

	mutex_unlock(&pinst->lock);

	return err;
}
EXPORT_SYMBOL(padata_add_cpu);

static int __padata_remove_cpu(struct padata_instancepinst, int cpu)
{
	struct parallel_datapd = NULL;

	if (cpumask_test_cpu(cpu, cpu_online_mask)) {

		if (!padata_validate_cpumask(pinst, pinst->cpumask.pcpu) ||
		    !padata_validate_cpumask(pinst, pinst->cpumask.cbcpu))
			__padata_stop(pinst);

		pd = padata_alloc_pd(pinst, pinst->cpumask.pcpu,
				     pinst->cpumask.cbcpu);
		if (!pd)
			return -ENOMEM;

		padata_replace(pinst, pd);

		cpumask_clear_cpu(cpu, pd->cpumask.cbcpu);
		cpumask_clear_cpu(cpu, pd->cpumask.pcpu);
	}

	return 0;
}

/*
 padata_remove_cpu - remove a cpu from the one or both(serial and parallel)
                     padata cpumasks.

 @pinst: padata instance
 @cpu: cpu to remove
 @mask: bitmask specifying from which cpumask @cpu should be removed
        The @mask may be any combination of the following flags:
          PADATA_CPU_SERIAL   - serial cpumask
          PADATA_CPU_PARALLEL - parallel cpumask
 /*
int padata_remove_cpu(struct padata_instancepinst, int cpu, int mask)
{
	int err;

	if (!(mask & (PADATA_CPU_SERIAL | PADATA_CPU_PARALLEL)))
		return -EINVAL;

	mutex_lock(&pinst->lock);

	get_online_cpus();
	if (mask & PADATA_CPU_SERIAL)
		cpumask_clear_cpu(cpu, pinst->cpumask.cbcpu);
	if (mask & PADATA_CPU_PARALLEL)
		cpumask_clear_cpu(cpu, pinst->cpumask.pcpu);

	err = __padata_remove_cpu(pinst, cpu);
	put_online_cpus();

	mutex_unlock(&pinst->lock);

	return err;
}
EXPORT_SYMBOL(padata_remove_cpu);

*/
 padata_start - start the parallel processing

 @pinst: padata instance to start
 /*
int padata_start(struct padata_instancepinst)
{
	int err = 0;

	mutex_lock(&pinst->lock);

	if (pinst->flags & PADATA_INVALID)
		err =-EINVAL;

	 __padata_start(pinst);

	mutex_unlock(&pinst->lock);

	return err;
}
EXPORT_SYMBOL(padata_start);

*/
 padata_stop - stop the parallel processing

 @pinst: padata instance to stop
 /*
void padata_stop(struct padata_instancepinst)
{
	mutex_lock(&pinst->lock);
	__padata_stop(pinst);
	mutex_unlock(&pinst->lock);
}
EXPORT_SYMBOL(padata_stop);

#ifdef CONFIG_HOTPLUG_CPU

static inline int pinst_has_cpu(struct padata_instancepinst, int cpu)
{
	return cpumask_test_cpu(cpu, pinst->cpumask.pcpu) ||
		cpumask_test_cpu(cpu, pinst->cpumask.cbcpu);
}


static int padata_cpu_callback(struct notifier_blocknfb,
			       unsigned long action, voidhcpu)
{
	int err;
	struct padata_instancepinst;
	int cpu = (unsigned long)hcpu;

	pinst = container_of(nfb, struct padata_instance, cpu_notifier);

	switch (action) {
	case CPU_ONLINE:
	case CPU_ONLINE_FROZEN:
	case CPU_DOWN_FAILED:
	case CPU_DOWN_FAILED_FROZEN:
		if (!pinst_has_cpu(pinst, cpu))
			break;
		mutex_lock(&pinst->lock);
		err = __padata_add_cpu(pinst, cpu);
		mutex_unlock(&pinst->lock);
		if (err)
			return notifier_from_errno(err);
		break;

	case CPU_DOWN_PREPARE:
	case CPU_DOWN_PREPARE_FROZEN:
	case CPU_UP_CANCELED:
	case CPU_UP_CANCELED_FROZEN:
		if (!pinst_has_cpu(pinst, cpu))
			break;
		mutex_lock(&pinst->lock);
		err = __padata_remove_cpu(pinst, cpu);
		mutex_unlock(&pinst->lock);
		if (err)
			return notifier_from_errno(err);
		break;
	}

	return NOTIFY_OK;
}
#endif

static void __padata_free(struct padata_instancepinst)
{
#ifdef CONFIG_HOTPLUG_CPU
	unregister_hotcpu_notifier(&pinst->cpu_notifier);
#endif

	padata_stop(pinst);
	padata_free_pd(pinst->pd);
	free_cpumask_var(pinst->cpumask.pcpu);
	free_cpumask_var(pinst->cpumask.cbcpu);
	kfree(pinst);
}

#define kobj2pinst(_kobj)					\
	container_of(_kobj, struct padata_instance, kobj)
#define attr2pentry(_attr)					\
	container_of(_attr, struct padata_sysfs_entry, attr)

static void padata_sysfs_release(struct kobjectkobj)
{
	struct padata_instancepinst = kobj2pinst(kobj);
	__padata_free(pinst);
}

struct padata_sysfs_entry {
	struct attribute attr;
	ssize_t (*show)(struct padata_instance, struct attribute, char);
	ssize_t (*store)(struct padata_instance, struct attribute,
			 const char, size_t);
};

static ssize_t show_cpumask(struct padata_instancepinst,
			    struct attributeattr,  charbuf)
{
	struct cpumaskcpumask;
	ssize_t len;

	mutex_lock(&pinst->lock);
	if (!strcmp(attr->name, "serial_cpumask"))
		cpumask = pinst->cpumask.cbcpu;
	else
		cpumask = pinst->cpumask.pcpu;

	len = snprintf(buf, PAGE_SIZE, "%*pb\n",
		       nr_cpu_ids, cpumask_bits(cpumask));
	mutex_unlock(&pinst->lock);
	return len < PAGE_SIZE ? len : -EINVAL;
}

static ssize_t store_cpumask(struct padata_instancepinst,
			     struct attributeattr,
			     const charbuf, size_t count)
{
	cpumask_var_t new_cpumask;
	ssize_t ret;
	int mask_type;

	if (!alloc_cpumask_var(&new_cpumask, GFP_KERNEL))
		return -ENOMEM;

	ret = bitmap_parse(buf, count, cpumask_bits(new_cpumask),
			   nr_cpumask_bits);
	if (ret < 0)
		goto out;

	mask_type = !strcmp(attr->name, "serial_cpumask") ?
		PADATA_CPU_SERIAL : PADATA_CPU_PARALLEL;
	ret = padata_set_cpumask(pinst, mask_type, new_cpumask);
	if (!ret)
		ret = count;

out:
	free_cpumask_var(new_cpumask);
	return ret;
}

#define PADATA_ATTR_RW(_name, _show_name, _store_name)		\
	static struct padata_sysfs_entry _name##_attr =		\
		__ATTR(_name, 0644, _show_name, _store_name)
#define PADATA_ATTR_RO(_name, _show_name)		\
	static struct padata_sysfs_entry _name##_attr = \
		__ATTR(_name, 0400, _show_name, NULL)

PADATA_ATTR_RW(serial_cpumask, show_cpumask, store_cpumask);
PADATA_ATTR_RW(parallel_cpumask, show_cpumask, store_cpumask);

*/
 Padata sysfs provides the following objects:
 serial_cpumask   [RW] - cpumask for serial workers
 parallel_cpumask [RW] - cpumask for parallel workers
 /*
static struct attributepadata_default_attrs[] = {
	&serial_cpumask_attr.attr,
	&parallel_cpumask_attr.attr,
	NULL,
};

static ssize_t padata_sysfs_show(struct kobjectkobj,
				 struct attributeattr, charbuf)
{
	struct padata_instancepinst;
	struct padata_sysfs_entrypentry;
	ssize_t ret = -EIO;

	pinst = kobj2pinst(kobj);
	pentry = attr2pentry(attr);
	if (pentry->show)
		ret = pentry->show(pinst, attr, buf);

	return ret;
}

static ssize_t padata_sysfs_store(struct kobjectkobj, struct attributeattr,
				  const charbuf, size_t count)
{
	struct padata_instancepinst;
	struct padata_sysfs_entrypentry;
	ssize_t ret = -EIO;

	pinst = kobj2pinst(kobj);
	pentry = attr2pentry(attr);
	if (pentry->show)
		ret = pentry->store(pinst, attr, buf, count);

	return ret;
}

static const struct sysfs_ops padata_sysfs_ops = {
	.show = padata_sysfs_show,
	.store = padata_sysfs_store,
};

static struct kobj_type padata_attr_type = {
	.sysfs_ops = &padata_sysfs_ops,
	.default_attrs = padata_default_attrs,
	.release = padata_sysfs_release,
};

*/
 padata_alloc_possible - Allocate and initialize padata instance.
                         Use the cpu_possible_mask for serial and
                         parallel workers.

 @wq: workqueue to use for the allocated padata instance
 /*
struct padata_instancepadata_alloc_possible(struct workqueue_structwq)
{
	return padata_alloc(wq, cpu_possible_mask, cpu_possible_mask);
}
EXPORT_SYMBOL(padata_alloc_possible);

*/
 padata_alloc - allocate and initialize a padata instance and specify
                cpumasks for serial and parallel workers.

 @wq: workqueue to use for the allocated padata instance
 @pcpumask: cpumask that will be used for padata parallelization
 @cbcpumask: cpumask that will be used for padata serialization
 /*
struct padata_instancepadata_alloc(struct workqueue_structwq,
				     const struct cpumaskpcpumask,
				     const struct cpumaskcbcpumask)
{
	struct padata_instancepinst;
	struct parallel_datapd = NULL;

	pinst = kzalloc(sizeof(struct padata_instance), GFP_KERNEL);
	if (!pinst)
		goto err;

	get_online_cpus();
	if (!alloc_cpumask_var(&pinst->cpumask.pcpu, GFP_KERNEL))
		goto err_free_inst;
	if (!alloc_cpumask_var(&pinst->cpumask.cbcpu, GFP_KERNEL)) {
		free_cpumask_var(pinst->cpumask.pcpu);
		goto err_free_inst;
	}
	if (!padata_validate_cpumask(pinst, pcpumask) ||
	    !padata_validate_cpumask(pinst, cbcpumask))
		goto err_free_masks;

	pd = padata_alloc_pd(pinst, pcpumask, cbcpumask);
	if (!pd)
		goto err_free_masks;

	rcu_assign_pointer(pinst->pd, pd);

	pinst->wq = wq;

	cpumask_copy(pinst->cpumask.pcpu, pcpumask);
	cpumask_copy(pinst->cpumask.cbcpu, cbcpumask);

	pinst->flags = 0;

	put_online_cpus();

	BLOCKING_INIT_NOTIFIER_HEAD(&pinst->cpumask_change_notifier);
	kobject_init(&pinst->kobj, &padata_attr_type);
	mutex_init(&pinst->lock);

#ifdef CONFIG_HOTPLUG_CPU
	pinst->cpu_notifier.notifier_call = padata_cpu_callback;
	pinst->cpu_notifier.priority = 0;
	register_hotcpu_notifier(&pinst->cpu_notifier);
#endif

	return pinst;

err_free_masks:
	free_cpumask_var(pinst->cpumask.pcpu);
	free_cpumask_var(pinst->cpumask.cbcpu);
err_free_inst:
	kfree(pinst);
	put_online_cpus();
err:
	return NULL;
}
EXPORT_SYMBOL(padata_alloc);

*/
 padata_free - free a padata instance

 @padata_inst: padata instance to free
 /*
void padata_free(struct padata_instancepinst)
{
	kobject_put(&pinst->kobj);
}
EXPORT_SYMBOL(padata_free);
*/

  linux/kernel/panic.c

  Copyright (C) 1991, 1992  Linus Torvalds
 /*

*/
 This function is used through-out the kernel (including mm and fs)
 to indicate a major problem.
 /*
#include <linux/debug_locks.h>
#include <linux/interrupt.h>
#include <linux/kmsg_dump.h>
#include <linux/kallsyms.h>
#include <linux/notifier.h>
#include <linux/module.h>
#include <linux/random.h>
#include <linux/ftrace.h>
#include <linux/reboot.h>
#include <linux/delay.h>
#include <linux/kexec.h>
#include <linux/sched.h>
#include <linux/sysrq.h>
#include <linux/init.h>
#include <linux/nmi.h>
#include <linux/console.h>
#include <linux/bug.h>

#define PANIC_TIMER_STEP 100
#define PANIC_BLINK_SPD 18

int panic_on_oops = CONFIG_PANIC_ON_OOPS_VALUE;
static unsigned long tainted_mask;
static int pause_on_oops;
static int pause_on_oops_flag;
static DEFINE_SPINLOCK(pause_on_oops_lock);
bool crash_kexec_post_notifiers;
int panic_on_warn __read_mostly;

int panic_timeout = CONFIG_PANIC_TIMEOUT;
EXPORT_SYMBOL_GPL(panic_timeout);

ATOMIC_NOTIFIER_HEAD(panic_notifier_list);

EXPORT_SYMBOL(panic_notifier_list);

static long no_blink(int state)
{
	return 0;
}

*/ Returns how long it waited in ms /*
long (*panic_blink)(int state);
EXPORT_SYMBOL(panic_blink);

*/
 Stop ourself in panic -- architecture code may override this
 /*
void __weak panic_smp_self_stop(void)
{
	while (1)
		cpu_relax();
}

*/
 Stop ourselves in NMI context if another CPU has already panicked. Arch code
 may override this to prepare for crash dumping, e.g. save regs info.
 /*
void __weak nmi_panic_self_stop(struct pt_regsregs)
{
	panic_smp_self_stop();
}

atomic_t panic_cpu = ATOMIC_INIT(PANIC_CPU_INVALID);

*/
 A variant of panic() called from NMI context. We return if we've already
 panicked on this CPU. If another CPU already panicked, loop in
 nmi_panic_self_stop() which can provide architecture dependent code such
 as saving register state for crash dump.
 /*
void nmi_panic(struct pt_regsregs, const charmsg)
{
	int old_cpu, cpu;

	cpu = raw_smp_processor_id();
	old_cpu = atomic_cmpxchg(&panic_cpu, PANIC_CPU_INVALID, cpu);

	if (old_cpu == PANIC_CPU_INVALID)
		panic("%s", msg);
	else if (old_cpu != cpu)
		nmi_panic_self_stop(regs);
}
EXPORT_SYMBOL(nmi_panic);

*/
	panic - halt the system
	@fmt: The text string to print

	Display a message, then perform cleanups.

	This function never returns.
 /*
void panic(const charfmt, ...)
{
	static char buf[1024];
	va_list args;
	long i, i_next = 0;
	int state = 0;
	int old_cpu, this_cpu;

	*/
	 Disable local interrupts. This will prevent panic_smp_self_stop
	 from deadlocking the first cpu that invokes the panic, since
	 there is nothing to prevent an interrupt handler (that runs
	 after setting panic_cpu) from invoking panic() again.
	 /*
	local_irq_disable();

	*/
	 It's possible to come here directly from a panic-assertion and
	 not have preempt disabled. Some functions called from here want
	 preempt to be disabled. No point enabling it later though...
	
	 Only one CPU is allowed to execute the panic code from here. For
	 multiple parallel invocations of panic, all other CPUs either
	 stop themself or will wait until they are stopped by the 1st CPU
	 with smp_send_stop().
	
	 `old_cpu == PANIC_CPU_INVALID' means this is the 1st CPU which
	 comes here, so go ahead.
	 `old_cpu == this_cpu' means we came from nmi_panic() which sets
	 panic_cpu to this CPU.  In this case, this is also the 1st CPU.
	 /*
	this_cpu = raw_smp_processor_id();
	old_cpu  = atomic_cmpxchg(&panic_cpu, PANIC_CPU_INVALID, this_cpu);

	if (old_cpu != PANIC_CPU_INVALID && old_cpu != this_cpu)
		panic_smp_self_stop();

	console_verbose();
	bust_spinlocks(1);
	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);
	pr_emerg("Kernel panic - not syncing: %s\n", buf);
#ifdef CONFIG_DEBUG_BUGVERBOSE
	*/
	 Avoid nested stack-dumping if a panic occurs during oops processing
	 /*
	if (!test_taint(TAINT_DIE) && oops_in_progress <= 1)
		dump_stack();
#endif

	*/
	 If we have crashed and we have a crash kernel loaded let it handle
	 everything else.
	 If we want to run this after calling panic_notifiers, pass
	 the "crash_kexec_post_notifiers" option to the kernel.
	
	 Bypass the panic_cpu check and call __crash_kexec directly.
	 /*
	if (!crash_kexec_post_notifiers)
		__crash_kexec(NULL);

	*/
	 Note smp_send_stop is the usual smp shutdown function, which
	 unfortunately means it may not be hardened to work in a panic
	 situation.
	 /*
	smp_send_stop();

	*/
	 Run any panic handlers, including those that might need to
	 add information to the kmsg dump output.
	 /*
	atomic_notifier_call_chain(&panic_notifier_list, 0, buf);

	kmsg_dump(KMSG_DUMP_PANIC);

	*/
	 If you doubt kdump always works fine in any situation,
	 "crash_kexec_post_notifiers" offers you a chance to run
	 panic_notifiers and dumping kmsg before kdump.
	 Note: since some panic_notifiers can make crashed kernel
	 more unstable, it can increase risks of the kdump failure too.
	
	 Bypass the panic_cpu check and call __crash_kexec directly.
	 /*
	if (crash_kexec_post_notifiers)
		__crash_kexec(NULL);

	bust_spinlocks(0);

	*/
	 We may have ended up stopping the CPU holding the lock (in
	 smp_send_stop()) while still having some valuable data in the console
	 buffer.  Try to acquire the lock then release it regardless of the
	 result.  The release will also print the buffers out.  Locks debug
	 should be disabled to avoid reporting bad unlock balance when
	 panic() is not being callled from OOPS.
	 /*
	debug_locks_off();
	console_flush_on_panic();

	if (!panic_blink)
		panic_blink = no_blink;

	if (panic_timeout > 0) {
		*/
		 Delay timeout seconds before rebooting the machine.
		 We can't use the "normal" timers since we just panicked.
		 /*
		pr_emerg("Rebooting in %d seconds..", panic_timeout);

		for (i = 0; i < panic_timeout 1000; i += PANIC_TIMER_STEP) {
			touch_nmi_watchdog();
			if (i >= i_next) {
				i += panic_blink(state ^= 1);
				i_next = i + 3600 / PANIC_BLINK_SPD;
			}
			mdelay(PANIC_TIMER_STEP);
		}
	}
	if (panic_timeout != 0) {
		*/
		 This will not be a clean reboot, with everything
		 shutting down.  But if there is a chance of
		 rebooting the system it will be rebooted.
		 /*
		emergency_restart();
	}
#ifdef __sparc__
	{
		extern int stop_a_enabled;
		*/ Make sure the user can actually press Stop-A (L1-A) /*
		stop_a_enabled = 1;
		pr_emerg("Press Stop-A (L1-A) to return to the boot prom\n");
	}
#endif
#if defined(CONFIG_S390)
	{
		unsigned long caller;

		caller = (unsigned long)__builtin_return_address(0);
		disabled_wait(caller);
	}
#endif
	pr_emerg("---[ end Kernel panic - not syncing: %s\n", buf);
	local_irq_enable();
	for (i = 0; ; i += PANIC_TIMER_STEP) {
		touch_softlockup_watchdog();
		if (i >= i_next) {
			i += panic_blink(state ^= 1);
			i_next = i + 3600 / PANIC_BLINK_SPD;
		}
		mdelay(PANIC_TIMER_STEP);
	}
}

EXPORT_SYMBOL(panic);


struct tnt {
	u8	bit;
	char	true;
	char	false;
};

static const struct tnt tnts[] = {
	{ TAINT_PROPRIETARY_MODULE,	'P', 'G' },
	{ TAINT_FORCED_MODULE,		'F', ' ' },
	{ TAINT_CPU_OUT_OF_SPEC,	'S', ' ' },
	{ TAINT_FORCED_RMMOD,		'R', ' ' },
	{ TAINT_MACHINE_CHECK,		'M', ' ' },
	{ TAINT_BAD_PAGE,		'B', ' ' },
	{ TAINT_USER,			'U', ' ' },
	{ TAINT_DIE,			'D', ' ' },
	{ TAINT_OVERRIDDEN_ACPI_TABLE,	'A', ' ' },
	{ TAINT_WARN,			'W', ' ' },
	{ TAINT_CRAP,			'C', ' ' },
	{ TAINT_FIRMWARE_WORKAROUND,	'I', ' ' },
	{ TAINT_OOT_MODULE,		'O', ' ' },
	{ TAINT_UNSIGNED_MODULE,	'E', ' ' },
	{ TAINT_SOFTLOCKUP,		'L', ' ' },
	{ TAINT_LIVEPATCH,		'K', ' ' },
};

*/
	print_tainted - return a string to represent the kernel taint state.

  'P' - Proprietary module has been loaded.
  'F' - Module has been forcibly loaded.
  'S' - SMP with CPUs not designed for SMP.
  'R' - User forced a module unload.
  'M' - System experienced a machine check exception.
  'B' - System has hit bad_page.
  'U' - Userspace-defined naughtiness.
  'D' - Kernel has oopsed before
  'A' - ACPI table overridden.
  'W' - Taint on warning.
  'C' - modules from drivers/staging are loaded.
  'I' - Working around severe firmware bug.
  'O' - Out-of-tree module has been loaded.
  'E' - Unsigned module has been loaded.
  'L' - A soft lockup has previously occurred.
  'K' - Kernel has been live patched.

	The string is overwritten by the next call to print_tainted().
 /*
const charprint_tainted(void)
{
	static char buf[ARRAY_SIZE(tnts) + sizeof("Tainted: ")];

	if (tainted_mask) {
		chars;
		int i;

		s = buf + sprintf(buf, "Tainted: ");
		for (i = 0; i < ARRAY_SIZE(tnts); i++) {
			const struct tntt = &tnts[i];
			*s++ = test_bit(t->bit, &tainted_mask) ?
					t->true : t->false;
		}
		*s = 0;
	} else
		snprintf(buf, sizeof(buf), "Not tainted");

	return buf;
}

int test_taint(unsigned flag)
{
	return test_bit(flag, &tainted_mask);
}
EXPORT_SYMBOL(test_taint);

unsigned long get_taint(void)
{
	return tainted_mask;
}

*/
 add_taint: add a taint flag if not already set.
 @flag: one of the TAINT_* constants.
 @lockdep_ok: whether lock debugging is still OK.

 If something bad has gone wrong, you'll want @lockdebug_ok = false, but for
 some notewortht-but-not-corrupting cases, it can be set to true.
 /*
void add_taint(unsigned flag, enum lockdep_ok lockdep_ok)
{
	if (lockdep_ok == LOCKDEP_NOW_UNRELIABLE && __debug_locks_off())
		pr_warn("Disabling lock debugging due to kernel taint\n");

	set_bit(flag, &tainted_mask);
}
EXPORT_SYMBOL(add_taint);

static void spin_msec(int msecs)
{
	int i;

	for (i = 0; i < msecs; i++) {
		touch_nmi_watchdog();
		mdelay(1);
	}
}

*/
 It just happens that oops_enter() and oops_exit() are identically
 implemented...
 /*
static void do_oops_enter_exit(void)
{
	unsigned long flags;
	static int spin_counter;

	if (!pause_on_oops)
		return;

	spin_lock_irqsave(&pause_on_oops_lock, flags);
	if (pause_on_oops_flag == 0) {
		*/ This CPU may now print the oops message /*
		pause_on_oops_flag = 1;
	} else {
		*/ We need to stall this CPU /*
		if (!spin_counter) {
			*/ This CPU gets to do the counting /*
			spin_counter = pause_on_oops;
			do {
				spin_unlock(&pause_on_oops_lock);
				spin_msec(MSEC_PER_SEC);
				spin_lock(&pause_on_oops_lock);
			} while (--spin_counter);
			pause_on_oops_flag = 0;
		} else {
			*/ This CPU waits for a different one /*
			while (spin_counter) {
				spin_unlock(&pause_on_oops_lock);
				spin_msec(1);
				spin_lock(&pause_on_oops_lock);
			}
		}
	}
	spin_unlock_irqrestore(&pause_on_oops_lock, flags);
}

*/
 Return true if the calling CPU is allowed to print oops-related info.
 This is a bit racy..
 /*
int oops_may_print(void)
{
	return pause_on_oops_flag == 0;
}

*/
 Called when the architecture enters its oops handler, before it prints
 anything.  If this is the first CPU to oops, and it's oopsing the first
 time then let it proceed.

 This is all enabled by the pause_on_oops kernel boot option.  We do all
 this to ensure that oopses don't scroll off the screen.  It has the
 side-effect of preventing later-oopsing CPUs from mucking up the display,
 too.

 It turns out that the CPU which is allowed to print ends up pausing for
 the right duration, whereas all the other CPUs pause for twice as long:
 once in oops_enter(), once in oops_exit().
 /*
void oops_enter(void)
{
	tracing_off();
	*/ can't trust the integrity of the kernel anymore: /*
	debug_locks_off();
	do_oops_enter_exit();
}

*/
 64-bit random ID for oopses:
 /*
static u64 oops_id;

static int init_oops_id(void)
{
	if (!oops_id)
		get_random_bytes(&oops_id, sizeof(oops_id));
	else
		oops_id++;

	return 0;
}
late_initcall(init_oops_id);

void print_oops_end_marker(void)
{
	init_oops_id();
	pr_warn("---[ end trace %016llx ]---\n", (unsigned long long)oops_id);
}

*/
 Called when the architecture exits its oops handler, after printing
 everything.
 /*
void oops_exit(void)
{
	do_oops_enter_exit();
	print_oops_end_marker();
	kmsg_dump(KMSG_DUMP_OOPS);
}

struct warn_args {
	const charfmt;
	va_list args;
};

void __warn(const charfile, int line, voidcaller, unsigned taint,
	    struct pt_regsregs, struct warn_argsargs)
{
	disable_trace_on_warning();

	pr_warn("------------[ cut here ]------------\n");

	if (file)
		pr_warn("WARNING: CPU: %d PID: %d at %s:%d %pS\n",
			raw_smp_processor_id(), current->pid, file, line,
			caller);
	else
		pr_warn("WARNING: CPU: %d PID: %d at %pS\n",
			raw_smp_processor_id(), current->pid, caller);

	if (args)
		vprintk(args->fmt, args->args);

	if (panic_on_warn) {
		*/
		 This thread may hit another WARN() in the panic path.
		 Resetting this prevents additional WARN() from panicking the
		 system on this thread.  Other threads are blocked by the
		 panic_mutex in panic().
		 /*
		panic_on_warn = 0;
		panic("panic_on_warn set ...\n");
	}

	print_modules();

	if (regs)
		show_regs(regs);
	else
		dump_stack();

	print_oops_end_marker();

	*/ Just a warning, don't kill lockdep. /*
	add_taint(taint, LOCKDEP_STILL_OK);
}

#ifdef WANT_WARN_ON_SLOWPATH
void warn_slowpath_fmt(const charfile, int line, const charfmt, ...)
{
	struct warn_args args;

	args.fmt = fmt;
	va_start(args.args, fmt);
	__warn(file, line, __builtin_return_address(0), TAINT_WARN, NULL,
	       &args);
	va_end(args.args);
}
EXPORT_SYMBOL(warn_slowpath_fmt);

void warn_slowpath_fmt_taint(const charfile, int line,
			     unsigned taint, const charfmt, ...)
{
	struct warn_args args;

	args.fmt = fmt;
	va_start(args.args, fmt);
	__warn(file, line, __builtin_return_address(0), taint, NULL, &args);
	va_end(args.args);
}
EXPORT_SYMBOL(warn_slowpath_fmt_taint);

void warn_slowpath_null(const charfile, int line)
{
	__warn(file, line, __builtin_return_address(0), TAINT_WARN, NULL, NULL);
}
EXPORT_SYMBOL(warn_slowpath_null);
#endif

#ifdef CONFIG_CC_STACKPROTECTOR

*/
 Called when gcc's -fstack-protector feature is used, and
 gcc detects corruption of the on-stack canary value
 /*
__visible void __stack_chk_fail(void)
{
	panic("stack-protector: Kernel stack is corrupted in: %p\n",
		__builtin_return_address(0));
}
EXPORT_SYMBOL(__stack_chk_fail);

#endif

core_param(panic, panic_timeout, int, 0644);
core_param(pause_on_oops, pause_on_oops, int, 0644);
core_param(panic_on_warn, panic_on_warn, int, 0644);

static int __init setup_crash_kexec_post_notifiers(chars)
{
	crash_kexec_post_notifiers = true;
	return 0;
}
early_param("crash_kexec_post_notifiers", setup_crash_kexec_post_notifiers);

static int __init oops_setup(chars)
{
	if (!s)
		return -EINVAL;
	if (!strcmp(s, "panic"))
		panic_on_oops = 1;
	return 0;
}
early_param("oops", oops_setup);
*/
   Helpers for initial module or kernel cmdline parsing
   Copyright (C) 2001 Rusty Russell.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/*
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/ctype.h>

#ifdef CONFIG_SYSFS
*/ Protects all built-in parameters, modules use their own param_lock /*
static DEFINE_MUTEX(param_lock);

*/ Use the module's mutex, or if built-in use the built-in mutex /*
#ifdef CONFIG_MODULES
#define KPARAM_MUTEX(mod)	((mod) ? &(mod)->param_lock : &param_lock)
#else
#define KPARAM_MUTEX(mod)	(&param_lock)
#endif

static inline void check_kparam_locked(struct modulemod)
{
	BUG_ON(!mutex_is_locked(KPARAM_MUTEX(mod)));
}
#else
static inline void check_kparam_locked(struct modulemod)
{
}
#endif */ !CONFIG_SYSFS /*

*/ This just allows us to keep track of which parameters are kmalloced. /*
struct kmalloced_param {
	struct list_head list;
	char val[];
};
static LIST_HEAD(kmalloced_params);
static DEFINE_SPINLOCK(kmalloced_params_lock);

static voidkmalloc_parameter(unsigned int size)
{
	struct kmalloced_paramp;

	p = kmalloc(sizeof(*p) + size, GFP_KERNEL);
	if (!p)
		return NULL;

	spin_lock(&kmalloced_params_lock);
	list_add(&p->list, &kmalloced_params);
	spin_unlock(&kmalloced_params_lock);

	return p->val;
}

*/ Does nothing if parameter wasn't kmalloced above. /*
static void maybe_kfree_parameter(voidparam)
{
	struct kmalloced_paramp;

	spin_lock(&kmalloced_params_lock);
	list_for_each_entry(p, &kmalloced_params, list) {
		if (p->val == param) {
			list_del(&p->list);
			kfree(p);
			break;
		}
	}
	spin_unlock(&kmalloced_params_lock);
}

static char dash2underscore(char c)
{
	if (c == '-')
		return '_';
	return c;
}

bool parameqn(const chara, const charb, size_t n)
{
	size_t i;

	for (i = 0; i < n; i++) {
		if (dash2underscore(a[i]) != dash2underscore(b[i]))
			return false;
	}
	return true;
}

bool parameq(const chara, const charb)
{
	return parameqn(a, b, strlen(a)+1);
}

static void param_check_unsafe(const struct kernel_paramkp)
{
	if (kp->flags & KERNEL_PARAM_FL_UNSAFE) {
		pr_warn("Setting dangerous option %s - tainting kernel\n",
			kp->name);
		add_taint(TAINT_USER, LOCKDEP_STILL_OK);
	}
}

static int parse_one(charparam,
		     charval,
		     const chardoing,
		     const struct kernel_paramparams,
		     unsigned num_params,
		     s16 min_level,
		     s16 max_level,
		     voidarg,
		     int (*handle_unknown)(charparam, charval,
				     const chardoing, voidarg))
{
	unsigned int i;
	int err;

	*/ Find parameter /*
	for (i = 0; i < num_params; i++) {
		if (parameq(param, params[i].name)) {
			if (params[i].level < min_level
			    || params[i].level > max_level)
				return 0;
			*/ No one handled NULL, so do it here. /*
			if (!val &&
			    !(params[i].ops->flags & KERNEL_PARAM_OPS_FL_NOARG))
				return -EINVAL;
			pr_debug("handling %s with %p\n", param,
				params[i].ops->set);
			kernel_param_lock(params[i].mod);
			param_check_unsafe(&params[i]);
			err = params[i].ops->set(val, &params[i]);
			kernel_param_unlock(params[i].mod);
			return err;
		}
	}

	if (handle_unknown) {
		pr_debug("doing %s: %s='%s'\n", doing, param, val);
		return handle_unknown(param, val, doing, arg);
	}

	pr_debug("Unknown argument '%s'\n", param);
	return -ENOENT;
}

*/ You can use " around spaces, but can't escape ". /*
*/ Hyphens and underscores equivalent in parameter names. /*
static charnext_arg(charargs, char*param, char*val)
{
	unsigned int i, equals = 0;
	int in_quote = 0, quoted = 0;
	charnext;

	if (*args == '"') {
		args++;
		in_quote = 1;
		quoted = 1;
	}

	for (i = 0; args[i]; i++) {
		if (isspace(args[i]) && !in_quote)
			break;
		if (equals == 0) {
			if (args[i] == '=')
				equals = i;
		}
		if (args[i] == '"')
			in_quote = !in_quote;
	}

	*param = args;
	if (!equals)
		*val = NULL;
	else {
		args[equals] = '\0';
		*val = args + equals + 1;

		*/ Don't include quotes in value. /*
		if (**val == '"') {
			(*val)++;
			if (args[i-1] == '"')
				args[i-1] = '\0';
		}
	}
	if (quoted && args[i-1] == '"')
		args[i-1] = '\0';

	if (args[i]) {
		args[i] = '\0';
		next = args + i + 1;
	} else
		next = args + i;

	*/ Chew up trailing spaces. /*
	return skip_spaces(next);
}

*/ Args looks like "foo=bar,bar2 baz=fuz wiz". /*
charparse_args(const chardoing,
		 charargs,
		 const struct kernel_paramparams,
		 unsigned num,
		 s16 min_level,
		 s16 max_level,
		 voidarg,
		 int (*unknown)(charparam, charval,
				const chardoing, voidarg))
{
	charparam,val,err = NULL;

	*/ Chew leading spaces /*
	args = skip_spaces(args);

	if (*args)
		pr_debug("doing %s, parsing ARGS: '%s'\n", doing, args);

	while (*args) {
		int ret;
		int irq_was_disabled;

		args = next_arg(args, &param, &val);
		*/ Stop at -- /*
		if (!val && strcmp(param, "--") == 0)
			return err ?: args;
		irq_was_disabled = irqs_disabled();
		ret = parse_one(param, val, doing, params, num,
				min_level, max_level, arg, unknown);
		if (irq_was_disabled && !irqs_disabled())
			pr_warn("%s: option '%s' enabled irq's!\n",
				doing, param);

		switch (ret) {
		case 0:
			continue;
		case -ENOENT:
			pr_err("%s: Unknown parameter `%s'\n", doing, param);
			break;
		case -ENOSPC:
			pr_err("%s: `%s' too large for parameter `%s'\n",
			       doing, val ?: "", param);
			break;
		default:
			pr_err("%s: `%s' invalid for parameter `%s'\n",
			       doing, val ?: "", param);
			break;
		}

		err = ERR_PTR(ret);
	}

	return err;
}

*/ Lazy bastard, eh? /*
#define STANDARD_PARAM_DEF(name, type, format, strtolfn)      		\
	int param_set_##name(const charval, const struct kernel_paramkp) \
	{								\
		return strtolfn(val, 0, (type)kp->arg);		\
	}								\
	int param_get_##name(charbuffer, const struct kernel_paramkp) \
	{								\
		return scnprintf(buffer, PAGE_SIZE, format,		\
				*((type)kp->arg));			\
	}								\
	const struct kernel_param_ops param_ops_##name = {			\
		.set = param_set_##name,				\
		.get = param_get_##name,				\
	};								\
	EXPORT_SYMBOL(param_set_##name);				\
	EXPORT_SYMBOL(param_get_##name);				\
	EXPORT_SYMBOL(param_ops_##name)


STANDARD_PARAM_DEF(byte, unsigned char, "%hhu", kstrtou8);
STANDARD_PARAM_DEF(short, short, "%hi", kstrtos16);
STANDARD_PARAM_DEF(ushort, unsigned short, "%hu", kstrtou16);
STANDARD_PARAM_DEF(int, int, "%i", kstrtoint);
STANDARD_PARAM_DEF(uint, unsigned int, "%u", kstrtouint);
STANDARD_PARAM_DEF(long, long, "%li", kstrtol);
STANDARD_PARAM_DEF(ulong, unsigned long, "%lu", kstrtoul);
STANDARD_PARAM_DEF(ullong, unsigned long long, "%llu", kstrtoull);

int param_set_charp(const charval, const struct kernel_paramkp)
{
	if (strlen(val) > 1024) {
		pr_err("%s: string parameter too long\n", kp->name);
		return -ENOSPC;
	}

	maybe_kfree_parameter(*(char*)kp->arg);

	*/ This is a hack.  We can't kmalloc in early boot, and we
	 don't need to; this mangled commandline is preserved. /*
	if (slab_is_available()) {
		*(char*)kp->arg = kmalloc_parameter(strlen(val)+1);
		if (!*(char*)kp->arg)
			return -ENOMEM;
		strcpy(*(char*)kp->arg, val);
	} else
		*(const char*)kp->arg = val;

	return 0;
}
EXPORT_SYMBOL(param_set_charp);

int param_get_charp(charbuffer, const struct kernel_paramkp)
{
	return scnprintf(buffer, PAGE_SIZE, "%s",((char*)kp->arg));
}
EXPORT_SYMBOL(param_get_charp);

void param_free_charp(voidarg)
{
	maybe_kfree_parameter(*((char*)arg));
}
EXPORT_SYMBOL(param_free_charp);

const struct kernel_param_ops param_ops_charp = {
	.set = param_set_charp,
	.get = param_get_charp,
	.free = param_free_charp,
};
EXPORT_SYMBOL(param_ops_charp);

*/ Actually could be a bool or an int, for historical reasons. /*
int param_set_bool(const charval, const struct kernel_paramkp)
{
	*/ No equals means "set"... /*
	if (!val) val = "1";

	*/ One of =[yYnN01] /*
	return strtobool(val, kp->arg);
}
EXPORT_SYMBOL(param_set_bool);

int param_get_bool(charbuffer, const struct kernel_paramkp)
{
	*/ Y and N chosen as being relatively non-coder friendly /*
	return sprintf(buffer, "%c",(bool)kp->arg ? 'Y' : 'N');
}
EXPORT_SYMBOL(param_get_bool);

const struct kernel_param_ops param_ops_bool = {
	.flags = KERNEL_PARAM_OPS_FL_NOARG,
	.set = param_set_bool,
	.get = param_get_bool,
};
EXPORT_SYMBOL(param_ops_bool);

int param_set_bool_enable_only(const charval, const struct kernel_paramkp)
{
	int err = 0;
	bool new_value;
	bool orig_value =(bool)kp->arg;
	struct kernel_param dummy_kp =kp;

	dummy_kp.arg = &new_value;

	err = param_set_bool(val, &dummy_kp);
	if (err)
		return err;

	*/ Don't let them unset it once it's set! /*
	if (!new_value && orig_value)
		return -EROFS;

	if (new_value)
		err = param_set_bool(val, kp);

	return err;
}
EXPORT_SYMBOL_GPL(param_set_bool_enable_only);

const struct kernel_param_ops param_ops_bool_enable_only = {
	.flags = KERNEL_PARAM_OPS_FL_NOARG,
	.set = param_set_bool_enable_only,
	.get = param_get_bool,
};
EXPORT_SYMBOL_GPL(param_ops_bool_enable_only);

*/ This one must be bool. /*
int param_set_invbool(const charval, const struct kernel_paramkp)
{
	int ret;
	bool boolval;
	struct kernel_param dummy;

	dummy.arg = &boolval;
	ret = param_set_bool(val, &dummy);
	if (ret == 0)
		*(bool)kp->arg = !boolval;
	return ret;
}
EXPORT_SYMBOL(param_set_invbool);

int param_get_invbool(charbuffer, const struct kernel_paramkp)
{
	return sprintf(buffer, "%c", (*(bool)kp->arg) ? 'N' : 'Y');
}
EXPORT_SYMBOL(param_get_invbool);

const struct kernel_param_ops param_ops_invbool = {
	.set = param_set_invbool,
	.get = param_get_invbool,
};
EXPORT_SYMBOL(param_ops_invbool);

int param_set_bint(const charval, const struct kernel_paramkp)
{
	*/ Match bool exactly, by re-using it. /*
	struct kernel_param boolkp =kp;
	bool v;
	int ret;

	boolkp.arg = &v;

	ret = param_set_bool(val, &boolkp);
	if (ret == 0)
		*(int)kp->arg = v;
	return ret;
}
EXPORT_SYMBOL(param_set_bint);

const struct kernel_param_ops param_ops_bint = {
	.flags = KERNEL_PARAM_OPS_FL_NOARG,
	.set = param_set_bint,
	.get = param_get_int,
};
EXPORT_SYMBOL(param_ops_bint);

*/ We break the rule and mangle the string. /*
static int param_array(struct modulemod,
		       const charname,
		       const charval,
		       unsigned int min, unsigned int max,
		       voidelem, int elemsize,
		       int (*set)(const char, const struct kernel_paramkp),
		       s16 level,
		       unsigned intnum)
{
	int ret;
	struct kernel_param kp;
	char save;

	*/ Get the name right for errors. /*
	kp.name = name;
	kp.arg = elem;
	kp.level = level;

	*num = 0;
	*/ We expect a comma-separated list of values. /*
	do {
		int len;

		if (*num == max) {
			pr_err("%s: can only take %i arguments\n", name, max);
			return -EINVAL;
		}
		len = strcspn(val, ",");

		*/ nul-terminate and parse /*
		save = val[len];
		((char)val)[len] = '\0';
		check_kparam_locked(mod);
		ret = set(val, &kp);

		if (ret != 0)
			return ret;
		kp.arg += elemsize;
		val += len+1;
		(*num)++;
	} while (save == ',');

	if (*num < min) {
		pr_err("%s: needs at least %i arguments\n", name, min);
		return -EINVAL;
	}
	return 0;
}

static int param_array_set(const charval, const struct kernel_paramkp)
{
	const struct kparam_arrayarr = kp->arr;
	unsigned int temp_num;

	return param_array(kp->mod, kp->name, val, 1, arr->max, arr->elem,
			   arr->elemsize, arr->ops->set, kp->level,
			   arr->num ?: &temp_num);
}

static int param_array_get(charbuffer, const struct kernel_paramkp)
{
	int i, off, ret;
	const struct kparam_arrayarr = kp->arr;
	struct kernel_param p =kp;

	for (i = off = 0; i < (arr->num ?arr->num : arr->max); i++) {
		if (i)
			buffer[off++] = ',';
		p.arg = arr->elem + arr->elemsize i;
		check_kparam_locked(p.mod);
		ret = arr->ops->get(buffer + off, &p);
		if (ret < 0)
			return ret;
		off += ret;
	}
	buffer[off] = '\0';
	return off;
}

static void param_array_free(voidarg)
{
	unsigned int i;
	const struct kparam_arrayarr = arg;

	if (arr->ops->free)
		for (i = 0; i < (arr->num ?arr->num : arr->max); i++)
			arr->ops->free(arr->elem + arr->elemsize i);
}

const struct kernel_param_ops param_array_ops = {
	.set = param_array_set,
	.get = param_array_get,
	.free = param_array_free,
};
EXPORT_SYMBOL(param_array_ops);

int param_set_copystring(const charval, const struct kernel_paramkp)
{
	const struct kparam_stringkps = kp->str;

	if (strlen(val)+1 > kps->maxlen) {
		pr_err("%s: string doesn't fit in %u chars.\n",
		       kp->name, kps->maxlen-1);
		return -ENOSPC;
	}
	strcpy(kps->string, val);
	return 0;
}
EXPORT_SYMBOL(param_set_copystring);

int param_get_string(charbuffer, const struct kernel_paramkp)
{
	const struct kparam_stringkps = kp->str;
	return strlcpy(buffer, kps->string, kps->maxlen);
}
EXPORT_SYMBOL(param_get_string);

const struct kernel_param_ops param_ops_string = {
	.set = param_set_copystring,
	.get = param_get_string,
};
EXPORT_SYMBOL(param_ops_string);

*/ sysfs output in /sys/modules/XYZ/parameters/ /*
#define to_module_attr(n) container_of(n, struct module_attribute, attr)
#define to_module_kobject(n) container_of(n, struct module_kobject, kobj)

struct param_attribute
{
	struct module_attribute mattr;
	const struct kernel_paramparam;
};

struct module_param_attrs
{
	unsigned int num;
	struct attribute_group grp;
	struct param_attribute attrs[0];
};

#ifdef CONFIG_SYSFS
#define to_param_attr(n) container_of(n, struct param_attribute, mattr)

static ssize_t param_attr_show(struct module_attributemattr,
			       struct module_kobjectmk, charbuf)
{
	int count;
	struct param_attributeattribute = to_param_attr(mattr);

	if (!attribute->param->ops->get)
		return -EPERM;

	kernel_param_lock(mk->mod);
	count = attribute->param->ops->get(buf, attribute->param);
	kernel_param_unlock(mk->mod);
	if (count > 0) {
		strcat(buf, "\n");
		++count;
	}
	return count;
}

*/ sysfs always hands a nul-terminated string in buf.  We rely on that. /*
static ssize_t param_attr_store(struct module_attributemattr,
				struct module_kobjectmk,
				const charbuf, size_t len)
{
 	int err;
	struct param_attributeattribute = to_param_attr(mattr);

	if (!attribute->param->ops->set)
		return -EPERM;

	kernel_param_lock(mk->mod);
	param_check_unsafe(attribute->param);
	err = attribute->param->ops->set(buf, attribute->param);
	kernel_param_unlock(mk->mod);
	if (!err)
		return len;
	return err;
}
#endif

#ifdef CONFIG_MODULES
#define __modinit
#else
#define __modinit __init
#endif

#ifdef CONFIG_SYSFS
void kernel_param_lock(struct modulemod)
{
	mutex_lock(KPARAM_MUTEX(mod));
}

void kernel_param_unlock(struct modulemod)
{
	mutex_unlock(KPARAM_MUTEX(mod));
}

EXPORT_SYMBOL(kernel_param_lock);
EXPORT_SYMBOL(kernel_param_unlock);

*/
 add_sysfs_param - add a parameter to sysfs
 @mk: struct module_kobject
 @kparam: the actual parameter definition to add to sysfs
 @name: name of parameter

 Create a kobject if for a (per-module) parameter if mp NULL, and
 create file in sysfs.  Returns an error on out of memory.  Always cleans up
 if there's an error.
 /*
static __modinit int add_sysfs_param(struct module_kobjectmk,
				     const struct kernel_paramkp,
				     const charname)
{
	struct module_param_attrsnew_mp;
	struct attribute*new_attrs;
	unsigned int i;

	*/ We don't bother calling this with invisible parameters. /*
	BUG_ON(!kp->perm);

	if (!mk->mp) {
		*/ First allocation. /*
		mk->mp = kzalloc(sizeof(*mk->mp), GFP_KERNEL);
		if (!mk->mp)
			return -ENOMEM;
		mk->mp->grp.name = "parameters";
		*/ NULL-terminated attribute array. /*
		mk->mp->grp.attrs = kzalloc(sizeof(mk->mp->grp.attrs[0]),
					    GFP_KERNEL);
		*/ Caller will cleanup via free_module_param_attrs /*
		if (!mk->mp->grp.attrs)
			return -ENOMEM;
	}

	*/ Enlarge allocations. /*
	new_mp = krealloc(mk->mp,
			  sizeof(*mk->mp) +
			  sizeof(mk->mp->attrs[0]) (mk->mp->num + 1),
			  GFP_KERNEL);
	if (!new_mp)
		return -ENOMEM;
	mk->mp = new_mp;

	*/ Extra pointer for NULL terminator /*
	new_attrs = krealloc(mk->mp->grp.attrs,
			     sizeof(mk->mp->grp.attrs[0]) (mk->mp->num + 2),
			     GFP_KERNEL);
	if (!new_attrs)
		return -ENOMEM;
	mk->mp->grp.attrs = new_attrs;

	*/ Tack new one on the end. /*
	memset(&mk->mp->attrs[mk->mp->num], 0, sizeof(mk->mp->attrs[0]));
	sysfs_attr_init(&mk->mp->attrs[mk->mp->num].mattr.attr);
	mk->mp->attrs[mk->mp->num].param = kp;
	mk->mp->attrs[mk->mp->num].mattr.show = param_attr_show;
	*/ Do not allow runtime DAC changes to make param writable. /*
	if ((kp->perm & (S_IWUSR | S_IWGRP | S_IWOTH)) != 0)
		mk->mp->attrs[mk->mp->num].mattr.store = param_attr_store;
	else
		mk->mp->attrs[mk->mp->num].mattr.store = NULL;
	mk->mp->attrs[mk->mp->num].mattr.attr.name = (char)name;
	mk->mp->attrs[mk->mp->num].mattr.attr.mode = kp->perm;
	mk->mp->num++;

	*/ Fix up all the pointers, since krealloc can move us /*
	for (i = 0; i < mk->mp->num; i++)
		mk->mp->grp.attrs[i] = &mk->mp->attrs[i].mattr.attr;
	mk->mp->grp.attrs[mk->mp->num] = NULL;
	return 0;
}

#ifdef CONFIG_MODULES
static void free_module_param_attrs(struct module_kobjectmk)
{
	if (mk->mp)
		kfree(mk->mp->grp.attrs);
	kfree(mk->mp);
	mk->mp = NULL;
}

*/
 module_param_sysfs_setup - setup sysfs support for one module
 @mod: module
 @kparam: module parameters (array)
 @num_params: number of module parameters

 Adds sysfs entries for module parameters under
 /sys/module/[mod->name]/parameters/
 /*
int module_param_sysfs_setup(struct modulemod,
			     const struct kernel_paramkparam,
			     unsigned int num_params)
{
	int i, err;
	bool params = false;

	for (i = 0; i < num_params; i++) {
		if (kparam[i].perm == 0)
			continue;
		err = add_sysfs_param(&mod->mkobj, &kparam[i], kparam[i].name);
		if (err) {
			free_module_param_attrs(&mod->mkobj);
			return err;
		}
		params = true;
	}

	if (!params)
		return 0;

	*/ Create the param group. /*
	err = sysfs_create_group(&mod->mkobj.kobj, &mod->mkobj.mp->grp);
	if (err)
		free_module_param_attrs(&mod->mkobj);
	return err;
}

*/
 module_param_sysfs_remove - remove sysfs support for one module
 @mod: module

 Remove sysfs entries for module parameters and the corresponding
 kobject.
 /*
void module_param_sysfs_remove(struct modulemod)
{
	if (mod->mkobj.mp) {
		sysfs_remove_group(&mod->mkobj.kobj, &mod->mkobj.mp->grp);
		*/ We are positive that no one is using any param
		 attrs at this point.  Deallocate immediately. /*
		free_module_param_attrs(&mod->mkobj);
	}
}
#endif

void destroy_params(const struct kernel_paramparams, unsigned num)
{
	unsigned int i;

	for (i = 0; i < num; i++)
		if (params[i].ops->free)
			params[i].ops->free(params[i].arg);
}

static struct module_kobject __init locate_module_kobject(const charname)
{
	struct module_kobjectmk;
	struct kobjectkobj;
	int err;

	kobj = kset_find_obj(module_kset, name);
	if (kobj) {
		mk = to_module_kobject(kobj);
	} else {
		mk = kzalloc(sizeof(struct module_kobject), GFP_KERNEL);
		BUG_ON(!mk);

		mk->mod = THIS_MODULE;
		mk->kobj.kset = module_kset;
		err = kobject_init_and_add(&mk->kobj, &module_ktype, NULL,
					   "%s", name);
#ifdef CONFIG_MODULES
		if (!err)
			err = sysfs_create_file(&mk->kobj, &module_uevent.attr);
#endif
		if (err) {
			kobject_put(&mk->kobj);
			pr_crit("Adding module '%s' to sysfs failed (%d), the system may be unstable.\n",
				name, err);
			return NULL;
		}

		*/ So that we hold reference in both cases. /*
		kobject_get(&mk->kobj);
	}

	return mk;
}

static void __init kernel_add_sysfs_param(const charname,
					  const struct kernel_paramkparam,
					  unsigned int name_skip)
{
	struct module_kobjectmk;
	int err;

	mk = locate_module_kobject(name);
	if (!mk)
		return;

	*/ We need to remove old parameters before adding more. /*
	if (mk->mp)
		sysfs_remove_group(&mk->kobj, &mk->mp->grp);

	*/ These should not fail at boot. /*
	err = add_sysfs_param(mk, kparam, kparam->name + name_skip);
	BUG_ON(err);
	err = sysfs_create_group(&mk->kobj, &mk->mp->grp);
	BUG_ON(err);
	kobject_uevent(&mk->kobj, KOBJ_ADD);
	kobject_put(&mk->kobj);
}

*/
 param_sysfs_builtin - add sysfs parameters for built-in modules

 Add module_parameters to sysfs for "modules" built into the kernel.

 The "module" name (KBUILD_MODNAME) is stored before a dot, the
 "parameter" name is stored behind a dot in kernel_param->name. So,
 extract the "module" name for all built-in kernel_param-eters,
 and for all who have the same, call kernel_add_sysfs_param.
 /*
static void __init param_sysfs_builtin(void)
{
	const struct kernel_paramkp;
	unsigned int name_len;
	char modname[MODULE_NAME_LEN];

	for (kp = __start___param; kp < __stop___param; kp++) {
		chardot;

		if (kp->perm == 0)
			continue;

		dot = strchr(kp->name, '.');
		if (!dot) {
			*/ This happens for core_param() /*
			strcpy(modname, "kernel");
			name_len = 0;
		} else {
			name_len = dot - kp->name + 1;
			strlcpy(modname, kp->name, name_len);
		}
		kernel_add_sysfs_param(modname, kp, name_len);
	}
}

ssize_t __modver_version_show(struct module_attributemattr,
			      struct module_kobjectmk, charbuf)
{
	struct module_version_attributevattr =
		container_of(mattr, struct module_version_attribute, mattr);

	return scnprintf(buf, PAGE_SIZE, "%s\n", vattr->version);
}

extern const struct module_version_attribute__start___modver[];
extern const struct module_version_attribute__stop___modver[];

static void __init version_sysfs_builtin(void)
{
	const struct module_version_attribute*p;
	struct module_kobjectmk;
	int err;

	for (p = __start___modver; p < __stop___modver; p++) {
		const struct module_version_attributevattr =p;

		mk = locate_module_kobject(vattr->module_name);
		if (mk) {
			err = sysfs_create_file(&mk->kobj, &vattr->mattr.attr);
			WARN_ON_ONCE(err);
			kobject_uevent(&mk->kobj, KOBJ_ADD);
			kobject_put(&mk->kobj);
		}
	}
}

*/ module-related sysfs stuff /*

static ssize_t module_attr_show(struct kobjectkobj,
				struct attributeattr,
				charbuf)
{
	struct module_attributeattribute;
	struct module_kobjectmk;
	int ret;

	attribute = to_module_attr(attr);
	mk = to_module_kobject(kobj);

	if (!attribute->show)
		return -EIO;

	ret = attribute->show(attribute, mk, buf);

	return ret;
}

static ssize_t module_attr_store(struct kobjectkobj,
				struct attributeattr,
				const charbuf, size_t len)
{
	struct module_attributeattribute;
	struct module_kobjectmk;
	int ret;

	attribute = to_module_attr(attr);
	mk = to_module_kobject(kobj);

	if (!attribute->store)
		return -EIO;

	ret = attribute->store(attribute, mk, buf, len);

	return ret;
}

static const struct sysfs_ops module_sysfs_ops = {
	.show = module_attr_show,
	.store = module_attr_store,
};

static int uevent_filter(struct ksetkset, struct kobjectkobj)
{
	struct kobj_typektype = get_ktype(kobj);

	if (ktype == &module_ktype)
		return 1;
	return 0;
}

static const struct kset_uevent_ops module_uevent_ops = {
	.filter = uevent_filter,
};

struct ksetmodule_kset;
int module_sysfs_initialized;

static void module_kobj_release(struct kobjectkobj)
{
	struct module_kobjectmk = to_module_kobject(kobj);
	complete(mk->kobj_completion);
}

struct kobj_type module_ktype = {
	.release   =	module_kobj_release,
	.sysfs_ops =	&module_sysfs_ops,
};

*/
 param_sysfs_init - wrapper for built-in params support
 /*
static int __init param_sysfs_init(void)
{
	module_kset = kset_create_and_add("module", &module_uevent_ops, NULL);
	if (!module_kset) {
		printk(KERN_WARNING "%s (%d): error creating kset\n",
			__FILE__, __LINE__);
		return -ENOMEM;
	}
	module_sysfs_initialized = 1;

	version_sysfs_builtin();
	param_sysfs_builtin();

	return 0;
}
subsys_initcall(param_sysfs_init);

#endif*/ CONFIG_SYSFS
