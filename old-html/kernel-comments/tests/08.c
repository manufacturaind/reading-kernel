
 Common SMP CPU bringup/teardown functions
 /*
#include <linux/cpu.h>
#include <linux/err.h>
#include <linux/smp.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/export.h>
#include <linux/percpu.h>
#include <linux/kthread.h>
#include <linux/smpboot.h>

#include "smpboot.h"

#ifdef CONFIG_SMP

#ifdef CONFIG_GENERIC_SMP_IDLE_THREAD
*/
 For the hotplug case we keep the task structs around and reuse
 them.
 /*
static DEFINE_PER_CPU(struct task_struct, idle_threads);

struct task_structidle_thread_get(unsigned int cpu)
{
	struct task_structtsk = per_cpu(idle_threads, cpu);

	if (!tsk)
		return ERR_PTR(-ENOMEM);
	init_idle(tsk, cpu);
	return tsk;
}

void __init idle_thread_set_boot_cpu(void)
{
	per_cpu(idle_threads, smp_processor_id()) = current;
}

*/
 idle_init - Initialize the idle thread for a cpu
 @cpu:	The cpu for which the idle thread should be initialized

 Creates the thread if it does not exist.
 /*
static inline void idle_init(unsigned int cpu)
{
	struct task_structtsk = per_cpu(idle_threads, cpu);

	if (!tsk) {
		tsk = fork_idle(cpu);
		if (IS_ERR(tsk))
			pr_err("SMP: fork_idle() failed for CPU %u\n", cpu);
		else
			per_cpu(idle_threads, cpu) = tsk;
	}
}

*/
 idle_threads_init - Initialize idle threads for all cpus
 /*
void __init idle_threads_init(void)
{
	unsigned int cpu, boot_cpu;

	boot_cpu = smp_processor_id();

	for_each_possible_cpu(cpu) {
		if (cpu != boot_cpu)
			idle_init(cpu);
	}
}
#endif

#endif */ #ifdef CONFIG_SMP /*

static LIST_HEAD(hotplug_threads);
static DEFINE_MUTEX(smpboot_threads_lock);

struct smpboot_thread_data {
	unsigned int			cpu;
	unsigned int			status;
	struct smp_hotplug_thread	*ht;
};

enum {
	HP_THREAD_NONE = 0,
	HP_THREAD_ACTIVE,
	HP_THREAD_PARKED,
};

*/
 smpboot_thread_fn - percpu hotplug thread loop function
 @data:	thread data pointer

 Checks for thread stop and park conditions. Calls the necessary
 setup, cleanup, park and unpark functions for the registered
 thread.

 Returns 1 when the thread should exit, 0 otherwise.
 /*
static int smpboot_thread_fn(voiddata)
{
	struct smpboot_thread_datatd = data;
	struct smp_hotplug_threadht = td->ht;

	while (1) {
		set_current_state(TASK_INTERRUPTIBLE);
		preempt_disable();
		if (kthread_should_stop()) {
			__set_current_state(TASK_RUNNING);
			preempt_enable();
			*/ cleanup must mirror setup /*
			if (ht->cleanup && td->status != HP_THREAD_NONE)
				ht->cleanup(td->cpu, cpu_online(td->cpu));
			kfree(td);
			return 0;
		}

		if (kthread_should_park()) {
			__set_current_state(TASK_RUNNING);
			preempt_enable();
			if (ht->park && td->status == HP_THREAD_ACTIVE) {
				BUG_ON(td->cpu != smp_processor_id());
				ht->park(td->cpu);
				td->status = HP_THREAD_PARKED;
			}
			kthread_parkme();
			*/ We might have been woken for stop /*
			continue;
		}

		BUG_ON(td->cpu != smp_processor_id());

		*/ Check for state change setup /*
		switch (td->status) {
		case HP_THREAD_NONE:
			__set_current_state(TASK_RUNNING);
			preempt_enable();
			if (ht->setup)
				ht->setup(td->cpu);
			td->status = HP_THREAD_ACTIVE;
			continue;

		case HP_THREAD_PARKED:
			__set_current_state(TASK_RUNNING);
			preempt_enable();
			if (ht->unpark)
				ht->unpark(td->cpu);
			td->status = HP_THREAD_ACTIVE;
			continue;
		}

		if (!ht->thread_should_run(td->cpu)) {
			preempt_enable_no_resched();
			schedule();
		} else {
			__set_current_state(TASK_RUNNING);
			preempt_enable();
			ht->thread_fn(td->cpu);
		}
	}
}

static int
__smpboot_create_thread(struct smp_hotplug_threadht, unsigned int cpu)
{
	struct task_structtsk =per_cpu_ptr(ht->store, cpu);
	struct smpboot_thread_datatd;

	if (tsk)
		return 0;

	td = kzalloc_node(sizeof(*td), GFP_KERNEL, cpu_to_node(cpu));
	if (!td)
		return -ENOMEM;
	td->cpu = cpu;
	td->ht = ht;

	tsk = kthread_create_on_cpu(smpboot_thread_fn, td, cpu,
				    ht->thread_comm);
	if (IS_ERR(tsk)) {
		kfree(td);
		return PTR_ERR(tsk);
	}
	get_task_struct(tsk);
	*per_cpu_ptr(ht->store, cpu) = tsk;
	if (ht->create) {
		*/
		 Make sure that the task has actually scheduled out
		 into park position, before calling the create
		 callback. At least the migration thread callback
		 requires that the task is off the runqueue.
		 /*
		if (!wait_task_inactive(tsk, TASK_PARKED))
			WARN_ON(1);
		else
			ht->create(cpu);
	}
	return 0;
}

int smpboot_create_threads(unsigned int cpu)
{
	struct smp_hotplug_threadcur;
	int ret = 0;

	mutex_lock(&smpboot_threads_lock);
	list_for_each_entry(cur, &hotplug_threads, list) {
		ret = __smpboot_create_thread(cur, cpu);
		if (ret)
			break;
	}
	mutex_unlock(&smpboot_threads_lock);
	return ret;
}

static void smpboot_unpark_thread(struct smp_hotplug_threadht, unsigned int cpu)
{
	struct task_structtsk =per_cpu_ptr(ht->store, cpu);

	if (!ht->selfparking)
		kthread_unpark(tsk);
}

int smpboot_unpark_threads(unsigned int cpu)
{
	struct smp_hotplug_threadcur;

	mutex_lock(&smpboot_threads_lock);
	list_for_each_entry(cur, &hotplug_threads, list)
		if (cpumask_test_cpu(cpu, cur->cpumask))
			smpboot_unpark_thread(cur, cpu);
	mutex_unlock(&smpboot_threads_lock);
	return 0;
}

static void smpboot_park_thread(struct smp_hotplug_threadht, unsigned int cpu)
{
	struct task_structtsk =per_cpu_ptr(ht->store, cpu);

	if (tsk && !ht->selfparking)
		kthread_park(tsk);
}

int smpboot_park_threads(unsigned int cpu)
{
	struct smp_hotplug_threadcur;

	mutex_lock(&smpboot_threads_lock);
	list_for_each_entry_reverse(cur, &hotplug_threads, list)
		smpboot_park_thread(cur, cpu);
	mutex_unlock(&smpboot_threads_lock);
	return 0;
}

static void smpboot_destroy_threads(struct smp_hotplug_threadht)
{
	unsigned int cpu;

	*/ We need to destroy also the parked threads of offline cpus /*
	for_each_possible_cpu(cpu) {
		struct task_structtsk =per_cpu_ptr(ht->store, cpu);

		if (tsk) {
			kthread_stop(tsk);
			put_task_struct(tsk);
			*per_cpu_ptr(ht->store, cpu) = NULL;
		}
	}
}

*/
 smpboot_register_percpu_thread_cpumask - Register a per_cpu thread related
 					    to hotplug
 @plug_thread:	Hotplug thread descriptor
 @cpumask:		The cpumask where threads run

 Creates and starts the threads on all online cpus.
 /*
int smpboot_register_percpu_thread_cpumask(struct smp_hotplug_threadplug_thread,
					   const struct cpumaskcpumask)
{
	unsigned int cpu;
	int ret = 0;

	if (!alloc_cpumask_var(&plug_thread->cpumask, GFP_KERNEL))
		return -ENOMEM;
	cpumask_copy(plug_thread->cpumask, cpumask);

	get_online_cpus();
	mutex_lock(&smpboot_threads_lock);
	for_each_online_cpu(cpu) {
		ret = __smpboot_create_thread(plug_thread, cpu);
		if (ret) {
			smpboot_destroy_threads(plug_thread);
			free_cpumask_var(plug_thread->cpumask);
			goto out;
		}
		if (cpumask_test_cpu(cpu, cpumask))
			smpboot_unpark_thread(plug_thread, cpu);
	}
	list_add(&plug_thread->list, &hotplug_threads);
out:
	mutex_unlock(&smpboot_threads_lock);
	put_online_cpus();
	return ret;
}
EXPORT_SYMBOL_GPL(smpboot_register_percpu_thread_cpumask);

*/
 smpboot_unregister_percpu_thread - Unregister a per_cpu thread related to hotplug
 @plug_thread:	Hotplug thread descriptor

 Stops all threads on all possible cpus.
 /*
void smpboot_unregister_percpu_thread(struct smp_hotplug_threadplug_thread)
{
	get_online_cpus();
	mutex_lock(&smpboot_threads_lock);
	list_del(&plug_thread->list);
	smpboot_destroy_threads(plug_thread);
	mutex_unlock(&smpboot_threads_lock);
	put_online_cpus();
	free_cpumask_var(plug_thread->cpumask);
}
EXPORT_SYMBOL_GPL(smpboot_unregister_percpu_thread);

*/
 smpboot_update_cpumask_percpu_thread - Adjust which per_cpu hotplug threads stay parked
 @plug_thread:	Hotplug thread descriptor
 @new:		Revised mask to use

 The cpumask field in the smp_hotplug_thread must not be updated directly
 by the client, but only by calling this function.
 This function can only be called on a registered smp_hotplug_thread.
 /*
int smpboot_update_cpumask_percpu_thread(struct smp_hotplug_threadplug_thread,
					 const struct cpumasknew)
{
	struct cpumaskold = plug_thread->cpumask;
	cpumask_var_t tmp;
	unsigned int cpu;

	if (!alloc_cpumask_var(&tmp, GFP_KERNEL))
		return -ENOMEM;

	get_online_cpus();
	mutex_lock(&smpboot_threads_lock);

	*/ Park threads that were exclusively enabled on the old mask. /*
	cpumask_andnot(tmp, old, new);
	for_each_cpu_and(cpu, tmp, cpu_online_mask)
		smpboot_park_thread(plug_thread, cpu);

	*/ Unpark threads that are exclusively enabled on the new mask. /*
	cpumask_andnot(tmp, new, old);
	for_each_cpu_and(cpu, tmp, cpu_online_mask)
		smpboot_unpark_thread(plug_thread, cpu);

	cpumask_copy(old, new);

	mutex_unlock(&smpboot_threads_lock);
	put_online_cpus();

	free_cpumask_var(tmp);

	return 0;
}
EXPORT_SYMBOL_GPL(smpboot_update_cpumask_percpu_thread);

static DEFINE_PER_CPU(atomic_t, cpu_hotplug_state) = ATOMIC_INIT(CPU_POST_DEAD);

*/
 Called to poll specified CPU's state, for example, when waiting for
 a CPU to come online.
 /*
int cpu_report_state(int cpu)
{
	return atomic_read(&per_cpu(cpu_hotplug_state, cpu));
}

*/
 If CPU has died properly, set its state to CPU_UP_PREPARE and
 return success.  Otherwise, return -EBUSY if the CPU died after
 cpu_wait_death() timed out.  And yet otherwise again, return -EAGAIN
 if cpu_wait_death() timed out and the CPU still hasn't gotten around
 to dying.  In the latter two cases, the CPU might not be set up
 properly, but it is up to the arch-specific code to decide.
 Finally, -EIO indicates an unanticipated problem.

 Note that it is permissible to omit this call entirely, as is
 done in architectures that do no CPU-hotplug error checking.
 /*
int cpu_check_up_prepare(int cpu)
{
	if (!IS_ENABLED(CONFIG_HOTPLUG_CPU)) {
		atomic_set(&per_cpu(cpu_hotplug_state, cpu), CPU_UP_PREPARE);
		return 0;
	}

	switch (atomic_read(&per_cpu(cpu_hotplug_state, cpu))) {

	case CPU_POST_DEAD:

		*/ The CPU died properly, so just start it up again. /*
		atomic_set(&per_cpu(cpu_hotplug_state, cpu), CPU_UP_PREPARE);
		return 0;

	case CPU_DEAD_FROZEN:

		*/
		 Timeout during CPU death, so let caller know.
		 The outgoing CPU completed its processing, but after
		 cpu_wait_death() timed out and reported the error. The
		 caller is free to proceed, in which case the state
		 will be reset properly by cpu_set_state_online().
		 Proceeding despite this -EBUSY return makes sense
		 for systems where the outgoing CPUs take themselves
		 offline, with no post-death manipulation required from
		 a surviving CPU.
		 /*
		return -EBUSY;

	case CPU_BROKEN:

		*/
		 The most likely reason we got here is that there was
		 a timeout during CPU death, and the outgoing CPU never
		 did complete its processing.  This could happen on
		 a virtualized system if the outgoing VCPU gets preempted
		 for more than five seconds, and the user attempts to
		 immediately online that same CPU.  Trying again later
		 might return -EBUSY above, hence -EAGAIN.
		 /*
		return -EAGAIN;

	default:

		*/ Should not happen.  Famous last words. /*
		return -EIO;
	}
}

*/
 Mark the specified CPU online.

 Note that it is permissible to omit this call entirely, as is
 done in architectures that do no CPU-hotplug error checking.
 /*
void cpu_set_state_online(int cpu)
{
	(void)atomic_xchg(&per_cpu(cpu_hotplug_state, cpu), CPU_ONLINE);
}

#ifdef CONFIG_HOTPLUG_CPU

*/
 Wait for the specified CPU to exit the idle loop and die.
 /*
bool cpu_wait_death(unsigned int cpu, int seconds)
{
	int jf_left = seconds HZ;
	int oldstate;
	bool ret = true;
	int sleep_jf = 1;

	might_sleep();

	*/ The outgoing CPU will normally get done quite quickly. /*
	if (atomic_read(&per_cpu(cpu_hotplug_state, cpu)) == CPU_DEAD)
		goto update_state;
	udelay(5);

	*/ But if the outgoing CPU dawdles, wait increasingly long times. /*
	while (atomic_read(&per_cpu(cpu_hotplug_state, cpu)) != CPU_DEAD) {
		schedule_timeout_uninterruptible(sleep_jf);
		jf_left -= sleep_jf;
		if (jf_left <= 0)
			break;
		sleep_jf = DIV_ROUND_UP(sleep_jf 11, 10);
	}
update_state:
	oldstate = atomic_read(&per_cpu(cpu_hotplug_state, cpu));
	if (oldstate == CPU_DEAD) {
		*/ Outgoing CPU died normally, update state. /*
		smp_mb();/ atomic_read() before update. /*
		atomic_set(&per_cpu(cpu_hotplug_state, cpu), CPU_POST_DEAD);
	} else {
		*/ Outgoing CPU still hasn't died, set state accordingly. /*
		if (atomic_cmpxchg(&per_cpu(cpu_hotplug_state, cpu),
				   oldstate, CPU_BROKEN) != oldstate)
			goto update_state;
		ret = false;
	}
	return ret;
}

*/
 Called by the outgoing CPU to report its successful death.  Return
 false if this report follows the surviving CPU's timing out.

 A separate "CPU_DEAD_FROZEN" is used when the surviving CPU
 timed out.  This approach allows architectures to omit calls to
 cpu_check_up_prepare() and cpu_set_state_online() without defeating
 the next cpu_wait_death()'s polling loop.
 /*
bool cpu_report_death(void)
{
	int oldstate;
	int newstate;
	int cpu = smp_processor_id();

	do {
		oldstate = atomic_read(&per_cpu(cpu_hotplug_state, cpu));
		if (oldstate != CPU_BROKEN)
			newstate = CPU_DEAD;
		else
			newstate = CPU_DEAD_FROZEN;
	} while (atomic_cmpxchg(&per_cpu(cpu_hotplug_state, cpu),
				oldstate, newstate) != oldstate);
	return newstate == CPU_DEAD;
}

#endif*/ #ifdef CONFIG_HOTPLUG_CPU
/*
#ifndef SMPBOOT_H
#define SMPBOOT_H

struct task_struct;

#ifdef CONFIG_GENERIC_SMP_IDLE_THREAD
struct task_structidle_thread_get(unsigned int cpu);
void idle_thread_set_boot_cpu(void);
void idle_threads_init(void);
#else
static inline struct task_structidle_thread_get(unsigned int cpu) { return NULL; }
static inline void idle_thread_set_boot_cpu(void) { }
static inline void idle_threads_init(void) { }
#endif

int smpboot_create_threads(unsigned int cpu);
int smpboot_park_threads(unsigned int cpu);
int smpboot_unpark_threads(unsigned int cpu);

void __init cpuhp_threads_init(void);

#endif
*/

 Generic helpers for smp ipi calls

 (C) Jens Axboe <jens.axboe@oracle.com> 2008
 /*
#include <linux/irq_work.h>
#include <linux/rcupdate.h>
#include <linux/rculist.h>
#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/percpu.h>
#include <linux/init.h>
#include <linux/gfp.h>
#include <linux/smp.h>
#include <linux/cpu.h>
#include <linux/sched.h>

#include "smpboot.h"

enum {
	CSD_FLAG_LOCK		= 0x01,
	CSD_FLAG_SYNCHRONOUS	= 0x02,
};

struct call_function_data {
	struct call_single_data	__percpucsd;
	cpumask_var_t		cpumask;
};

static DEFINE_PER_CPU_SHARED_ALIGNED(struct call_function_data, cfd_data);

static DEFINE_PER_CPU_SHARED_ALIGNED(struct llist_head, call_single_queue);

static void flush_smp_call_function_queue(bool warn_cpu_offline);

static int
hotplug_cfd(struct notifier_blocknfb, unsigned long action, voidhcpu)
{
	long cpu = (long)hcpu;
	struct call_function_datacfd = &per_cpu(cfd_data, cpu);

	switch (action) {
	case CPU_UP_PREPARE:
	case CPU_UP_PREPARE_FROZEN:
		if (!zalloc_cpumask_var_node(&cfd->cpumask, GFP_KERNEL,
				cpu_to_node(cpu)))
			return notifier_from_errno(-ENOMEM);
		cfd->csd = alloc_percpu(struct call_single_data);
		if (!cfd->csd) {
			free_cpumask_var(cfd->cpumask);
			return notifier_from_errno(-ENOMEM);
		}
		break;

#ifdef CONFIG_HOTPLUG_CPU
	case CPU_UP_CANCELED:
	case CPU_UP_CANCELED_FROZEN:
		*/ Fall-through to the CPU_DEAD[_FROZEN] case. /*

	case CPU_DEAD:
	case CPU_DEAD_FROZEN:
		free_cpumask_var(cfd->cpumask);
		free_percpu(cfd->csd);
		break;

	case CPU_DYING:
	case CPU_DYING_FROZEN:
		*/
		 The IPIs for the smp-call-function callbacks queued by other
		 CPUs might arrive late, either due to hardware latencies or
		 because this CPU disabled interrupts (inside stop-machine)
		 before the IPIs were sent. So flush out any pending callbacks
		 explicitly (without waiting for the IPIs to arrive), to
		 ensure that the outgoing CPU doesn't go offline with work
		 still pending.
		 /*
		flush_smp_call_function_queue(false);
		break;
#endif
	};

	return NOTIFY_OK;
}

static struct notifier_block hotplug_cfd_notifier = {
	.notifier_call		= hotplug_cfd,
};

void __init call_function_init(void)
{
	voidcpu = (void)(long)smp_processor_id();
	int i;

	for_each_possible_cpu(i)
		init_llist_head(&per_cpu(call_single_queue, i));

	hotplug_cfd(&hotplug_cfd_notifier, CPU_UP_PREPARE, cpu);
	register_cpu_notifier(&hotplug_cfd_notifier);
}

*/
 csd_lock/csd_unlock used to serialize access to per-cpu csd resources

 For non-synchronous ipi calls the csd can still be in use by the
 previous function call. For multi-cpu calls its even more interesting
 as we'll have to ensure no other cpu is observing our csd.
 /*
static __always_inline void csd_lock_wait(struct call_single_datacsd)
{
	smp_cond_acquire(!(csd->flags & CSD_FLAG_LOCK));
}

static __always_inline void csd_lock(struct call_single_datacsd)
{
	csd_lock_wait(csd);
	csd->flags |= CSD_FLAG_LOCK;

	*/
	 prevent CPU from reordering the above assignment
	 to ->flags with any subsequent assignments to other
	 fields of the specified call_single_data structure:
	 /*
	smp_wmb();
}

static __always_inline void csd_unlock(struct call_single_datacsd)
{
	WARN_ON(!(csd->flags & CSD_FLAG_LOCK));

	*/
	 ensure we're all done before releasing data:
	 /*
	smp_store_release(&csd->flags, 0);
}

static DEFINE_PER_CPU_SHARED_ALIGNED(struct call_single_data, csd_data);

*/
 Insert a previously allocated call_single_data element
 for execution on the given CPU. data must already have
 ->func, ->info, and ->flags set.
 /*
static int generic_exec_single(int cpu, struct call_single_datacsd,
			       smp_call_func_t func, voidinfo)
{
	if (cpu == smp_processor_id()) {
		unsigned long flags;

		*/
		 We can unlock early even for the synchronous on-stack case,
		 since we're doing this from the same CPU..
		 /*
		csd_unlock(csd);
		local_irq_save(flags);
		func(info);
		local_irq_restore(flags);
		return 0;
	}


	if ((unsigned)cpu >= nr_cpu_ids || !cpu_online(cpu)) {
		csd_unlock(csd);
		return -ENXIO;
	}

	csd->func = func;
	csd->info = info;

	*/
	 The list addition should be visible before sending the IPI
	 handler locks the list to pull the entry off it because of
	 normal cache coherency rules implied by spinlocks.
	
	 If IPIs can go out of order to the cache coherency protocol
	 in an architecture, sufficient synchronisation should be added
	 to arch code to make it appear to obey cache coherency WRT
	 locking and barrier primitives. Generic code isn't really
	 equipped to do the right thing...
	 /*
	if (llist_add(&csd->llist, &per_cpu(call_single_queue, cpu)))
		arch_send_call_function_single_ipi(cpu);

	return 0;
}

*/
 generic_smp_call_function_single_interrupt - Execute SMP IPI callbacks

 Invoked by arch to handle an IPI for call function single.
 Must be called with interrupts disabled.
 /*
void generic_smp_call_function_single_interrupt(void)
{
	flush_smp_call_function_queue(true);
}

*/
 flush_smp_call_function_queue - Flush pending smp-call-function callbacks

 @warn_cpu_offline: If set to 'true', warn if callbacks were queued on an
		      offline CPU. Skip this check if set to 'false'.

 Flush any pending smp-call-function callbacks queued on this CPU. This is
 invoked by the generic IPI handler, as well as by a CPU about to go offline,
 to ensure that all pending IPI callbacks are run before it goes completely
 offline.

 Loop through the call_single_queue and run all the queued callbacks.
 Must be called with interrupts disabled.
 /*
static void flush_smp_call_function_queue(bool warn_cpu_offline)
{
	struct llist_headhead;
	struct llist_nodeentry;
	struct call_single_datacsd,csd_next;
	static bool warned;

	WARN_ON(!irqs_disabled());

	head = this_cpu_ptr(&call_single_queue);
	entry = llist_del_all(head);
	entry = llist_reverse_order(entry);

	*/ There shouldn't be any pending callbacks on an offline CPU. /*
	if (unlikely(warn_cpu_offline && !cpu_online(smp_processor_id()) &&
		     !warned && !llist_empty(head))) {
		warned = true;
		WARN(1, "IPI on offline CPU %d\n", smp_processor_id());

		*/
		 We don't have to use the _safe() variant here
		 because we are not invoking the IPI handlers yet.
		 /*
		llist_for_each_entry(csd, entry, llist)
			pr_warn("IPI callback %pS sent to offline CPU\n",
				csd->func);
	}

	llist_for_each_entry_safe(csd, csd_next, entry, llist) {
		smp_call_func_t func = csd->func;
		voidinfo = csd->info;

		*/ Do we wait untilafter* callback? /*
		if (csd->flags & CSD_FLAG_SYNCHRONOUS) {
			func(info);
			csd_unlock(csd);
		} else {
			csd_unlock(csd);
			func(info);
		}
	}

	*/
	 Handle irq works queued remotely by irq_work_queue_on().
	 Smp functions above are typically synchronous so they
	 better run first since some other CPUs may be busy waiting
	 for them.
	 /*
	irq_work_run();
}

*/
 smp_call_function_single - Run a function on a specific CPU
 @func: The function to run. This must be fast and non-blocking.
 @info: An arbitrary pointer to pass to the function.
 @wait: If true, wait until function has completed on other CPUs.

 Returns 0 on success, else a negative status code.
 /*
int smp_call_function_single(int cpu, smp_call_func_t func, voidinfo,
			     int wait)
{
	struct call_single_datacsd;
	struct call_single_data csd_stack = { .flags = CSD_FLAG_LOCK | CSD_FLAG_SYNCHRONOUS };
	int this_cpu;
	int err;

	*/
	 prevent preemption and reschedule on another processor,
	 as well as CPU removal
	 /*
	this_cpu = get_cpu();

	*/
	 Can deadlock when called with interrupts disabled.
	 We allow cpu's that are not yet online though, as no one else can
	 send smp call function interrupt to this cpu and as such deadlocks
	 can't happen.
	 /*
	WARN_ON_ONCE(cpu_online(this_cpu) && irqs_disabled()
		     && !oops_in_progress);

	csd = &csd_stack;
	if (!wait) {
		csd = this_cpu_ptr(&csd_data);
		csd_lock(csd);
	}

	err = generic_exec_single(cpu, csd, func, info);

	if (wait)
		csd_lock_wait(csd);

	put_cpu();

	return err;
}
EXPORT_SYMBOL(smp_call_function_single);

*/
 smp_call_function_single_async(): Run an asynchronous function on a
 			         specific CPU.
 @cpu: The CPU to run on.
 @csd: Pre-allocated and setup data structure

 Like smp_call_function_single(), but the call is asynchonous and
 can thus be done from contexts with disabled interrupts.

 The caller passes his own pre-allocated data structure
 (ie: embedded in an object) and is responsible for synchronizing it
 such that the IPIs performed on the @csd are strictly serialized.

 NOTE: Be careful, there is unfortunately no current debugging facility to
 validate the correctness of this serialization.
 /*
int smp_call_function_single_async(int cpu, struct call_single_datacsd)
{
	int err = 0;

	preempt_disable();

	*/ We could deadlock if we have to wait here with interrupts disabled! /*
	if (WARN_ON_ONCE(csd->flags & CSD_FLAG_LOCK))
		csd_lock_wait(csd);

	csd->flags = CSD_FLAG_LOCK;
	smp_wmb();

	err = generic_exec_single(cpu, csd, csd->func, csd->info);
	preempt_enable();

	return err;
}
EXPORT_SYMBOL_GPL(smp_call_function_single_async);

*/
 smp_call_function_any - Run a function on any of the given cpus
 @mask: The mask of cpus it can run on.
 @func: The function to run. This must be fast and non-blocking.
 @info: An arbitrary pointer to pass to the function.
 @wait: If true, wait until function has completed.

 Returns 0 on success, else a negative status code (if no cpus were online).

 Selection preference:
	1) current cpu if in @mask
	2) any cpu of current node if in @mask
	3) any other online cpu in @mask
 /*
int smp_call_function_any(const struct cpumaskmask,
			  smp_call_func_t func, voidinfo, int wait)
{
	unsigned int cpu;
	const struct cpumasknodemask;
	int ret;

	*/ Try for same CPU (cheapest) /*
	cpu = get_cpu();
	if (cpumask_test_cpu(cpu, mask))
		goto call;

	*/ Try for same node. /*
	nodemask = cpumask_of_node(cpu_to_node(cpu));
	for (cpu = cpumask_first_and(nodemask, mask); cpu < nr_cpu_ids;
	     cpu = cpumask_next_and(cpu, nodemask, mask)) {
		if (cpu_online(cpu))
			goto call;
	}

	*/ Any online will do: smp_call_function_single handles nr_cpu_ids. /*
	cpu = cpumask_any_and(mask, cpu_online_mask);
call:
	ret = smp_call_function_single(cpu, func, info, wait);
	put_cpu();
	return ret;
}
EXPORT_SYMBOL_GPL(smp_call_function_any);

*/
 smp_call_function_many(): Run a function on a set of other CPUs.
 @mask: The set of cpus to run on (only runs on online subset).
 @func: The function to run. This must be fast and non-blocking.
 @info: An arbitrary pointer to pass to the function.
 @wait: If true, wait (atomically) until function has completed
        on other CPUs.

 If @wait is true, then returns once @func has returned.

 You must not call this function with disabled interrupts or from a
 hardware interrupt handler or from a bottom half handler. Preemption
 must be disabled when calling this function.
 /*
void smp_call_function_many(const struct cpumaskmask,
			    smp_call_func_t func, voidinfo, bool wait)
{
	struct call_function_datacfd;
	int cpu, next_cpu, this_cpu = smp_processor_id();

	*/
	 Can deadlock when called with interrupts disabled.
	 We allow cpu's that are not yet online though, as no one else can
	 send smp call function interrupt to this cpu and as such deadlocks
	 can't happen.
	 /*
	WARN_ON_ONCE(cpu_online(this_cpu) && irqs_disabled()
		     && !oops_in_progress && !early_boot_irqs_disabled);

	*/ Try to fastpath.  So, what's a CPU they want? Ignoring this one. /*
	cpu = cpumask_first_and(mask, cpu_online_mask);
	if (cpu == this_cpu)
		cpu = cpumask_next_and(cpu, mask, cpu_online_mask);

	*/ No online cpus?  We're done. /*
	if (cpu >= nr_cpu_ids)
		return;

	*/ Do we have another CPU which isn't us? /*
	next_cpu = cpumask_next_and(cpu, mask, cpu_online_mask);
	if (next_cpu == this_cpu)
		next_cpu = cpumask_next_and(next_cpu, mask, cpu_online_mask);

	*/ Fastpath: do that cpu by itself. /*
	if (next_cpu >= nr_cpu_ids) {
		smp_call_function_single(cpu, func, info, wait);
		return;
	}

	cfd = this_cpu_ptr(&cfd_data);

	cpumask_and(cfd->cpumask, mask, cpu_online_mask);
	cpumask_clear_cpu(this_cpu, cfd->cpumask);

	*/ Some callers race with other cpus changing the passed mask /*
	if (unlikely(!cpumask_weight(cfd->cpumask)))
		return;

	for_each_cpu(cpu, cfd->cpumask) {
		struct call_single_datacsd = per_cpu_ptr(cfd->csd, cpu);

		csd_lock(csd);
		if (wait)
			csd->flags |= CSD_FLAG_SYNCHRONOUS;
		csd->func = func;
		csd->info = info;
		llist_add(&csd->llist, &per_cpu(call_single_queue, cpu));
	}

	*/ Send a message to all CPUs in the map /*
	arch_send_call_function_ipi_mask(cfd->cpumask);

	if (wait) {
		for_each_cpu(cpu, cfd->cpumask) {
			struct call_single_datacsd;

			csd = per_cpu_ptr(cfd->csd, cpu);
			csd_lock_wait(csd);
		}
	}
}
EXPORT_SYMBOL(smp_call_function_many);

*/
 smp_call_function(): Run a function on all other CPUs.
 @func: The function to run. This must be fast and non-blocking.
 @info: An arbitrary pointer to pass to the function.
 @wait: If true, wait (atomically) until function has completed
        on other CPUs.

 Returns 0.

 If @wait is true, then returns once @func has returned; otherwise
 it returns just before the target cpu calls @func.

 You must not call this function with disabled interrupts or from a
 hardware interrupt handler or from a bottom half handler.
 /*
int smp_call_function(smp_call_func_t func, voidinfo, int wait)
{
	preempt_disable();
	smp_call_function_many(cpu_online_mask, func, info, wait);
	preempt_enable();

	return 0;
}
EXPORT_SYMBOL(smp_call_function);

*/ Setup configured maximum number of CPUs to activate /*
unsigned int setup_max_cpus = NR_CPUS;
EXPORT_SYMBOL(setup_max_cpus);


*/
 Setup routine for controlling SMP activation

 Command-line option of "nosmp" or "maxcpus=0" will disable SMP
 activation entirely (the MPS table probe still happens, though).

 Command-line option of "maxcpus=<NUM>", where <NUM> is an integer
 greater than 0, limits the maximum number of CPUs activated in
 SMP mode to <NUM>.
 /*

void __weak arch_disable_smp_support(void) { }

static int __init nosmp(charstr)
{
	setup_max_cpus = 0;
	arch_disable_smp_support();

	return 0;
}

early_param("nosmp", nosmp);

*/ this is hard limit /*
static int __init nrcpus(charstr)
{
	int nr_cpus;

	get_option(&str, &nr_cpus);
	if (nr_cpus > 0 && nr_cpus < nr_cpu_ids)
		nr_cpu_ids = nr_cpus;

	return 0;
}

early_param("nr_cpus", nrcpus);

static int __init maxcpus(charstr)
{
	get_option(&str, &setup_max_cpus);
	if (setup_max_cpus == 0)
		arch_disable_smp_support();

	return 0;
}

early_param("maxcpus", maxcpus);

*/ Setup number of possible processor ids /*
int nr_cpu_ids __read_mostly = NR_CPUS;
EXPORT_SYMBOL(nr_cpu_ids);

*/ An arch may set nr_cpu_ids earlier if needed, so this would be redundant /*
void __init setup_nr_cpu_ids(void)
{
	nr_cpu_ids = find_last_bit(cpumask_bits(cpu_possible_mask),NR_CPUS) + 1;
}

void __weak smp_announce(void)
{
	printk(KERN_INFO "Brought up %d CPUs\n", num_online_cpus());
}

*/ Called by boot processor to activate the rest. /*
void __init smp_init(void)
{
	unsigned int cpu;

	idle_threads_init();
	cpuhp_threads_init();

	*/ FIXME: This should be done in userspace --RR /*
	for_each_present_cpu(cpu) {
		if (num_online_cpus() >= setup_max_cpus)
			break;
		if (!cpu_online(cpu))
			cpu_up(cpu);
	}

	*/ Any cleanup work /*
	smp_announce();
	smp_cpus_done(setup_max_cpus);
}

*/
 Call a function on all processors.  May be used during early boot while
 early_boot_irqs_disabled is set.  Use local_irq_save/restore() instead
 of local_irq_disable/enable().
 /*
int on_each_cpu(void (*func) (voidinfo), voidinfo, int wait)
{
	unsigned long flags;
	int ret = 0;

	preempt_disable();
	ret = smp_call_function(func, info, wait);
	local_irq_save(flags);
	func(info);
	local_irq_restore(flags);
	preempt_enable();
	return ret;
}
EXPORT_SYMBOL(on_each_cpu);

*/
 on_each_cpu_mask(): Run a function on processors specified by
 cpumask, which may include the local processor.
 @mask: The set of cpus to run on (only runs on online subset).
 @func: The function to run. This must be fast and non-blocking.
 @info: An arbitrary pointer to pass to the function.
 @wait: If true, wait (atomically) until function has completed
        on other CPUs.

 If @wait is true, then returns once @func has returned.

 You must not call this function with disabled interrupts or from a
 hardware interrupt handler or from a bottom half handler.  The
 exception is that it may be used during early boot while
 early_boot_irqs_disabled is set.
 /*
void on_each_cpu_mask(const struct cpumaskmask, smp_call_func_t func,
			voidinfo, bool wait)
{
	int cpu = get_cpu();

	smp_call_function_many(mask, func, info, wait);
	if (cpumask_test_cpu(cpu, mask)) {
		unsigned long flags;
		local_irq_save(flags);
		func(info);
		local_irq_restore(flags);
	}
	put_cpu();
}
EXPORT_SYMBOL(on_each_cpu_mask);

*/
 on_each_cpu_cond(): Call a function on each processor for which
 the supplied function cond_func returns true, optionally waiting
 for all the required CPUs to finish. This may include the local
 processor.
 @cond_func:	A callback function that is passed a cpu id and
		the the info parameter. The function is called
		with preemption disabled. The function should
		return a blooean value indicating whether to IPI
		the specified CPU.
 @func:	The function to run on all applicable CPUs.
		This must be fast and non-blocking.
 @info:	An arbitrary pointer to pass to both functions.
 @wait:	If true, wait (atomically) until function has
		completed on other CPUs.
 @gfp_flags:	GFP flags to use when allocating the cpumask
		used internally by the function.

 The function might sleep if the GFP flags indicates a non
 atomic allocation is allowed.

 Preemption is disabled to protect against CPUs going offline but not online.
 CPUs going online during the call will not be seen or sent an IPI.

 You must not call this function with disabled interrupts or
 from a hardware interrupt handler or from a bottom half handler.
 /*
void on_each_cpu_cond(bool (*cond_func)(int cpu, voidinfo),
			smp_call_func_t func, voidinfo, bool wait,
			gfp_t gfp_flags)
{
	cpumask_var_t cpus;
	int cpu, ret;

	might_sleep_if(gfpflags_allow_blocking(gfp_flags));

	if (likely(zalloc_cpumask_var(&cpus, (gfp_flags|__GFP_NOWARN)))) {
		preempt_disable();
		for_each_online_cpu(cpu)
			if (cond_func(cpu, info))
				cpumask_set_cpu(cpu, cpus);
		on_each_cpu_mask(cpus, func, info, wait);
		preempt_enable();
		free_cpumask_var(cpus);
	} else {
		*/
		 No free cpumask, bother. No matter, we'll
		 just have to IPI them one by one.
		 /*
		preempt_disable();
		for_each_online_cpu(cpu)
			if (cond_func(cpu, info)) {
				ret = smp_call_function_single(cpu, func,
								info, wait);
				WARN_ON_ONCE(ret);
			}
		preempt_enable();
	}
}
EXPORT_SYMBOL(on_each_cpu_cond);

static void do_nothing(voidunused)
{
}

*/
 kick_all_cpus_sync - Force all cpus out of idle

 Used to synchronize the update of pm_idle function pointer. It's
 called after the pointer is updated and returns after the dummy
 callback function has been executed on all cpus. The execution of
 the function can only happen on the remote cpus after they have
 left the idle function which had been called via pm_idle function
 pointer. So it's guaranteed that nothing uses the previous pointer
 anymore.
 /*
void kick_all_cpus_sync(void)
{
	*/ Make sure the change is visible before we kick the cpus /*
	smp_mb();
	smp_call_function(do_nothing, NULL, 1);
}
EXPORT_SYMBOL_GPL(kick_all_cpus_sync);

*/
 wake_up_all_idle_cpus - break all cpus out of idle
 wake_up_all_idle_cpus try to break all cpus which is in idle state even
 including idle polling cpus, for non-idle cpus, we will do nothing
 for them.
 /*
void wake_up_all_idle_cpus(void)
{
	int cpu;

	preempt_disable();
	for_each_online_cpu(cpu) {
		if (cpu == smp_processor_id())
			continue;

		wake_up_if_idle(cpu);
	}
	preempt_enable();
}
EXPORT_SYMBOL_GPL(wake_up_all_idle_cpus);
*/

	linux/kernel/softirq.c

	Copyright (C) 1992 Linus Torvalds

	Distribute under GPLv2.

	Rewritten. Old one was good in 2.2, but in 2.3 it was immoral. --ANK (990903)
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/export.h>
#include <linux/kernel_stat.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/notifier.h>
#include <linux/percpu.h>
#include <linux/cpu.h>
#include <linux/freezer.h>
#include <linux/kthread.h>
#include <linux/rcupdate.h>
#include <linux/ftrace.h>
#include <linux/smp.h>
#include <linux/smpboot.h>
#include <linux/tick.h>
#include <linux/irq.h>

#define CREATE_TRACE_POINTS
#include <trace/events/irq.h>

*/
   - No shared variables, all the data are CPU local.
   - If a softirq needs serialization, let it serialize itself
     by its own spinlocks.
   - Even if softirq is serialized, only local cpu is marked for
     execution. Hence, we get something sort of weak cpu binding.
     Though it is still not clear, will it result in better locality
     or will not.

   Examples:
   - NET RX softirq. It is multithreaded and does not require
     any global serialization.
   - NET TX softirq. It kicks software netdevice queues, hence
     it is logically serialized per device, but this serialization
     is invisible to common code.
   - Tasklets: serialized wrt itself.
 /*

#ifndef __ARCH_IRQ_STAT
irq_cpustat_t irq_stat[NR_CPUS] ____cacheline_aligned;
EXPORT_SYMBOL(irq_stat);
#endif

static struct softirq_action softirq_vec[NR_SOFTIRQS] __cacheline_aligned_in_smp;

DEFINE_PER_CPU(struct task_struct, ksoftirqd);

const char const softirq_to_name[NR_SOFTIRQS] = {
	"HI", "TIMER", "NET_TX", "NET_RX", "BLOCK", "BLOCK_IOPOLL",
	"TASKLET", "SCHED", "HRTIMER", "RCU"
};

*/
 we cannot loop indefinitely here to avoid userspace starvation,
 but we also don't want to introduce a worst case 1/HZ latency
 to the pending events, so lets the scheduler to balance
 the softirq load for us.
 /*
static void wakeup_softirqd(void)
{
	*/ Interrupts are disabled: no need to stop preemption /*
	struct task_structtsk = __this_cpu_read(ksoftirqd);

	if (tsk && tsk->state != TASK_RUNNING)
		wake_up_process(tsk);
}

*/
 preempt_count and SOFTIRQ_OFFSET usage:
 - preempt_count is changed by SOFTIRQ_OFFSET on entering or leaving
   softirq processing.
 - preempt_count is changed by SOFTIRQ_DISABLE_OFFSET (= 2 SOFTIRQ_OFFSET)
   on local_bh_disable or local_bh_enable.
 This lets us distinguish between whether we are currently processing
 softirq and whether we just have bh disabled.
 /*

*/
 This one is for softirq.c-internal use,
 where hardirqs are disabled legitimately:
 /*
#ifdef CONFIG_TRACE_IRQFLAGS
void __local_bh_disable_ip(unsigned long ip, unsigned int cnt)
{
	unsigned long flags;

	WARN_ON_ONCE(in_irq());

	raw_local_irq_save(flags);
	*/
	 The preempt tracer hooks into preempt_count_add and will break
	 lockdep because it calls back into lockdep after SOFTIRQ_OFFSET
	 is set and before current->softirq_enabled is cleared.
	 We must manually increment preempt_count here and manually
	 call the trace_preempt_off later.
	 /*
	__preempt_count_add(cnt);
	*/
	 Were softirqs turned off above:
	 /*
	if (softirq_count() == (cnt & SOFTIRQ_MASK))
		trace_softirqs_off(ip);
	raw_local_irq_restore(flags);

	if (preempt_count() == cnt) {
#ifdef CONFIG_DEBUG_PREEMPT
		current->preempt_disable_ip = get_lock_parent_ip();
#endif
		trace_preempt_off(CALLER_ADDR0, get_lock_parent_ip());
	}
}
EXPORT_SYMBOL(__local_bh_disable_ip);
#endif */ CONFIG_TRACE_IRQFLAGS /*

static void __local_bh_enable(unsigned int cnt)
{
	WARN_ON_ONCE(!irqs_disabled());

	if (softirq_count() == (cnt & SOFTIRQ_MASK))
		trace_softirqs_on(_RET_IP_);
	preempt_count_sub(cnt);
}

*/
 Special-case - softirqs can safely be enabled in
 cond_resched_softirq(), or by __do_softirq(),
 without processing still-pending softirqs:
 /*
void _local_bh_enable(void)
{
	WARN_ON_ONCE(in_irq());
	__local_bh_enable(SOFTIRQ_DISABLE_OFFSET);
}
EXPORT_SYMBOL(_local_bh_enable);

void __local_bh_enable_ip(unsigned long ip, unsigned int cnt)
{
	WARN_ON_ONCE(in_irq() || irqs_disabled());
#ifdef CONFIG_TRACE_IRQFLAGS
	local_irq_disable();
#endif
	*/
	 Are softirqs going to be turned on now:
	 /*
	if (softirq_count() == SOFTIRQ_DISABLE_OFFSET)
		trace_softirqs_on(ip);
	*/
	 Keep preemption disabled until we are done with
	 softirq processing:
	 /*
	preempt_count_sub(cnt - 1);

	if (unlikely(!in_interrupt() && local_softirq_pending())) {
		*/
		 Run softirq if any pending. And do it in its own stack
		 as we may be calling this deep in a task call stack already.
		 /*
		do_softirq();
	}

	preempt_count_dec();
#ifdef CONFIG_TRACE_IRQFLAGS
	local_irq_enable();
#endif
	preempt_check_resched();
}
EXPORT_SYMBOL(__local_bh_enable_ip);

*/
 We restart softirq processing for at most MAX_SOFTIRQ_RESTART times,
 but break the loop if need_resched() is set or after 2 ms.
 The MAX_SOFTIRQ_TIME provides a nice upper bound in most cases, but in
 certain cases, such as stop_machine(), jiffies may cease to
 increment and so we need the MAX_SOFTIRQ_RESTART limit as
 well to make sure we eventually return from this method.

 These limits have been established via experimentation.
 The two things to balance is latency against fairness -
 we want to handle softirqs as soon as possible, but they
 should not be able to lock up the box.
 /*
#define MAX_SOFTIRQ_TIME  msecs_to_jiffies(2)
#define MAX_SOFTIRQ_RESTART 10

#ifdef CONFIG_TRACE_IRQFLAGS
*/
 When we run softirqs from irq_exit() and thus on the hardirq stack we need
 to keep the lockdep irq context tracking as tight as possible in order to
 not miss-qualify lock contexts and miss possible deadlocks.
 /*

static inline bool lockdep_softirq_start(void)
{
	bool in_hardirq = false;

	if (trace_hardirq_context(current)) {
		in_hardirq = true;
		trace_hardirq_exit();
	}

	lockdep_softirq_enter();

	return in_hardirq;
}

static inline void lockdep_softirq_end(bool in_hardirq)
{
	lockdep_softirq_exit();

	if (in_hardirq)
		trace_hardirq_enter();
}
#else
static inline bool lockdep_softirq_start(void) { return false; }
static inline void lockdep_softirq_end(bool in_hardirq) { }
#endif

asmlinkage __visible void __softirq_entry __do_softirq(void)
{
	unsigned long end = jiffies + MAX_SOFTIRQ_TIME;
	unsigned long old_flags = current->flags;
	int max_restart = MAX_SOFTIRQ_RESTART;
	struct softirq_actionh;
	bool in_hardirq;
	__u32 pending;
	int softirq_bit;

	*/
	 Mask out PF_MEMALLOC s current task context is borrowed for the
	 softirq. A softirq handled such as network RX might set PF_MEMALLOC
	 again if the socket is related to swap
	 /*
	current->flags &= ~PF_MEMALLOC;

	pending = local_softirq_pending();
	account_irq_enter_time(current);

	__local_bh_disable_ip(_RET_IP_, SOFTIRQ_OFFSET);
	in_hardirq = lockdep_softirq_start();

restart:
	*/ Reset the pending bitmask before enabling irqs /*
	set_softirq_pending(0);

	local_irq_enable();

	h = softirq_vec;

	while ((softirq_bit = ffs(pending))) {
		unsigned int vec_nr;
		int prev_count;

		h += softirq_bit - 1;

		vec_nr = h - softirq_vec;
		prev_count = preempt_count();

		kstat_incr_softirqs_this_cpu(vec_nr);

		trace_softirq_entry(vec_nr);
		h->action(h);
		trace_softirq_exit(vec_nr);
		if (unlikely(prev_count != preempt_count())) {
			pr_err("huh, entered softirq %u %s %p with preempt_count %08x, exited with %08x?\n",
			       vec_nr, softirq_to_name[vec_nr], h->action,
			       prev_count, preempt_count());
			preempt_count_set(prev_count);
		}
		h++;
		pending >>= softirq_bit;
	}

	rcu_bh_qs();
	local_irq_disable();

	pending = local_softirq_pending();
	if (pending) {
		if (time_before(jiffies, end) && !need_resched() &&
		    --max_restart)
			goto restart;

		wakeup_softirqd();
	}

	lockdep_softirq_end(in_hardirq);
	account_irq_exit_time(current);
	__local_bh_enable(SOFTIRQ_OFFSET);
	WARN_ON_ONCE(in_interrupt());
	tsk_restore_flags(current, old_flags, PF_MEMALLOC);
}

asmlinkage __visible void do_softirq(void)
{
	__u32 pending;
	unsigned long flags;

	if (in_interrupt())
		return;

	local_irq_save(flags);

	pending = local_softirq_pending();

	if (pending)
		do_softirq_own_stack();

	local_irq_restore(flags);
}

*/
 Enter an interrupt context.
 /*
void irq_enter(void)
{
	rcu_irq_enter();
	if (is_idle_task(current) && !in_interrupt()) {
		*/
		 Prevent raise_softirq from needlessly waking up ksoftirqd
		 here, as softirq will be serviced on return from interrupt.
		 /*
		local_bh_disable();
		tick_irq_enter();
		_local_bh_enable();
	}

	__irq_enter();
}

static inline void invoke_softirq(void)
{
	if (!force_irqthreads) {
#ifdef CONFIG_HAVE_IRQ_EXIT_ON_IRQ_STACK
		*/
		 We can safely execute softirq on the current stack if
		 it is the irq stack, because it should be near empty
		 at this stage.
		 /*
		__do_softirq();
#else
		*/
		 Otherwise, irq_exit() is called on the task stack that can
		 be potentially deep already. So call softirq in its own stack
		 to prevent from any overrun.
		 /*
		do_softirq_own_stack();
#endif
	} else {
		wakeup_softirqd();
	}
}

static inline void tick_irq_exit(void)
{
#ifdef CONFIG_NO_HZ_COMMON
	int cpu = smp_processor_id();

	*/ Make sure that timer wheel updates are propagated /*
	if ((idle_cpu(cpu) && !need_resched()) || tick_nohz_full_cpu(cpu)) {
		if (!in_interrupt())
			tick_nohz_irq_exit();
	}
#endif
}

*/
 Exit an interrupt context. Process softirqs if needed and possible:
 /*
void irq_exit(void)
{
#ifndef __ARCH_IRQ_EXIT_IRQS_DISABLED
	local_irq_disable();
#else
	WARN_ON_ONCE(!irqs_disabled());
#endif

	account_irq_exit_time(current);
	preempt_count_sub(HARDIRQ_OFFSET);
	if (!in_interrupt() && local_softirq_pending())
		invoke_softirq();

	tick_irq_exit();
	rcu_irq_exit();
	trace_hardirq_exit();/ must be last! /*
}

*/
 This function must run with irqs disabled!
 /*
inline void raise_softirq_irqoff(unsigned int nr)
{
	__raise_softirq_irqoff(nr);

	*/
	 If we're in an interrupt or softirq, we're done
	 (this also catches softirq-disabled code). We will
	 actually run the softirq once we return from
	 the irq or softirq.
	
	 Otherwise we wake up ksoftirqd to make sure we
	 schedule the softirq soon.
	 /*
	if (!in_interrupt())
		wakeup_softirqd();
}

void raise_softirq(unsigned int nr)
{
	unsigned long flags;

	local_irq_save(flags);
	raise_softirq_irqoff(nr);
	local_irq_restore(flags);
}

void __raise_softirq_irqoff(unsigned int nr)
{
	trace_softirq_raise(nr);
	or_softirq_pending(1UL << nr);
}

void open_softirq(int nr, void (*action)(struct softirq_action))
{
	softirq_vec[nr].action = action;
}

*/
 Tasklets
 /*
struct tasklet_head {
	struct tasklet_structhead;
	struct tasklet_struct*tail;
};

static DEFINE_PER_CPU(struct tasklet_head, tasklet_vec);
static DEFINE_PER_CPU(struct tasklet_head, tasklet_hi_vec);

void __tasklet_schedule(struct tasklet_structt)
{
	unsigned long flags;

	local_irq_save(flags);
	t->next = NULL;
	*__this_cpu_read(tasklet_vec.tail) = t;
	__this_cpu_write(tasklet_vec.tail, &(t->next));
	raise_softirq_irqoff(TASKLET_SOFTIRQ);
	local_irq_restore(flags);
}
EXPORT_SYMBOL(__tasklet_schedule);

void __tasklet_hi_schedule(struct tasklet_structt)
{
	unsigned long flags;

	local_irq_save(flags);
	t->next = NULL;
	*__this_cpu_read(tasklet_hi_vec.tail) = t;
	__this_cpu_write(tasklet_hi_vec.tail,  &(t->next));
	raise_softirq_irqoff(HI_SOFTIRQ);
	local_irq_restore(flags);
}
EXPORT_SYMBOL(__tasklet_hi_schedule);

void __tasklet_hi_schedule_first(struct tasklet_structt)
{
	BUG_ON(!irqs_disabled());

	t->next = __this_cpu_read(tasklet_hi_vec.head);
	__this_cpu_write(tasklet_hi_vec.head, t);
	__raise_softirq_irqoff(HI_SOFTIRQ);
}
EXPORT_SYMBOL(__tasklet_hi_schedule_first);

static void tasklet_action(struct softirq_actiona)
{
	struct tasklet_structlist;

	local_irq_disable();
	list = __this_cpu_read(tasklet_vec.head);
	__this_cpu_write(tasklet_vec.head, NULL);
	__this_cpu_write(tasklet_vec.tail, this_cpu_ptr(&tasklet_vec.head));
	local_irq_enable();

	while (list) {
		struct tasklet_structt = list;

		list = list->next;

		if (tasklet_trylock(t)) {
			if (!atomic_read(&t->count)) {
				if (!test_and_clear_bit(TASKLET_STATE_SCHED,
							&t->state))
					BUG();
				t->func(t->data);
				tasklet_unlock(t);
				continue;
			}
			tasklet_unlock(t);
		}

		local_irq_disable();
		t->next = NULL;
		*__this_cpu_read(tasklet_vec.tail) = t;
		__this_cpu_write(tasklet_vec.tail, &(t->next));
		__raise_softirq_irqoff(TASKLET_SOFTIRQ);
		local_irq_enable();
	}
}

static void tasklet_hi_action(struct softirq_actiona)
{
	struct tasklet_structlist;

	local_irq_disable();
	list = __this_cpu_read(tasklet_hi_vec.head);
	__this_cpu_write(tasklet_hi_vec.head, NULL);
	__this_cpu_write(tasklet_hi_vec.tail, this_cpu_ptr(&tasklet_hi_vec.head));
	local_irq_enable();

	while (list) {
		struct tasklet_structt = list;

		list = list->next;

		if (tasklet_trylock(t)) {
			if (!atomic_read(&t->count)) {
				if (!test_and_clear_bit(TASKLET_STATE_SCHED,
							&t->state))
					BUG();
				t->func(t->data);
				tasklet_unlock(t);
				continue;
			}
			tasklet_unlock(t);
		}

		local_irq_disable();
		t->next = NULL;
		*__this_cpu_read(tasklet_hi_vec.tail) = t;
		__this_cpu_write(tasklet_hi_vec.tail, &(t->next));
		__raise_softirq_irqoff(HI_SOFTIRQ);
		local_irq_enable();
	}
}

void tasklet_init(struct tasklet_structt,
		  void (*func)(unsigned long), unsigned long data)
{
	t->next = NULL;
	t->state = 0;
	atomic_set(&t->count, 0);
	t->func = func;
	t->data = data;
}
EXPORT_SYMBOL(tasklet_init);

void tasklet_kill(struct tasklet_structt)
{
	if (in_interrupt())
		pr_notice("Attempt to kill tasklet from interrupt\n");

	while (test_and_set_bit(TASKLET_STATE_SCHED, &t->state)) {
		do {
			yield();
		} while (test_bit(TASKLET_STATE_SCHED, &t->state));
	}
	tasklet_unlock_wait(t);
	clear_bit(TASKLET_STATE_SCHED, &t->state);
}
EXPORT_SYMBOL(tasklet_kill);

*/
 tasklet_hrtimer
 /*

*/
 The trampoline is called when the hrtimer expires. It schedules a tasklet
 to run __tasklet_hrtimer_trampoline() which in turn will call the intended
 hrtimer callback, but from softirq context.
 /*
static enum hrtimer_restart __hrtimer_tasklet_trampoline(struct hrtimertimer)
{
	struct tasklet_hrtimerttimer =
		container_of(timer, struct tasklet_hrtimer, timer);

	tasklet_hi_schedule(&ttimer->tasklet);
	return HRTIMER_NORESTART;
}

*/
 Helper function which calls the hrtimer callback from
 tasklet/softirq context
 /*
static void __tasklet_hrtimer_trampoline(unsigned long data)
{
	struct tasklet_hrtimerttimer = (void)data;
	enum hrtimer_restart restart;

	restart = ttimer->function(&ttimer->timer);
	if (restart != HRTIMER_NORESTART)
		hrtimer_restart(&ttimer->timer);
}

*/
 tasklet_hrtimer_init - Init a tasklet/hrtimer combo for softirq callbacks
 @ttimer:	 tasklet_hrtimer which is initialized
 @function:	 hrtimer callback function which gets called from softirq context
 @which_clock: clock id (CLOCK_MONOTONIC/CLOCK_REALTIME)
 @mode:	 hrtimer mode (HRTIMER_MODE_ABS/HRTIMER_MODE_REL)
 /*
void tasklet_hrtimer_init(struct tasklet_hrtimerttimer,
			  enum hrtimer_restart (*function)(struct hrtimer),
			  clockid_t which_clock, enum hrtimer_mode mode)
{
	hrtimer_init(&ttimer->timer, which_clock, mode);
	ttimer->timer.function = __hrtimer_tasklet_trampoline;
	tasklet_init(&ttimer->tasklet, __tasklet_hrtimer_trampoline,
		     (unsigned long)ttimer);
	ttimer->function = function;
}
EXPORT_SYMBOL_GPL(tasklet_hrtimer_init);

void __init softirq_init(void)
{
	int cpu;

	for_each_possible_cpu(cpu) {
		per_cpu(tasklet_vec, cpu).tail =
			&per_cpu(tasklet_vec, cpu).head;
		per_cpu(tasklet_hi_vec, cpu).tail =
			&per_cpu(tasklet_hi_vec, cpu).head;
	}

	open_softirq(TASKLET_SOFTIRQ, tasklet_action);
	open_softirq(HI_SOFTIRQ, tasklet_hi_action);
}

static int ksoftirqd_should_run(unsigned int cpu)
{
	return local_softirq_pending();
}

static void run_ksoftirqd(unsigned int cpu)
{
	local_irq_disable();
	if (local_softirq_pending()) {
		*/
		 We can safely run softirq on inline stack, as we are not deep
		 in the task stack here.
		 /*
		__do_softirq();
		local_irq_enable();
		cond_resched_rcu_qs();
		return;
	}
	local_irq_enable();
}

#ifdef CONFIG_HOTPLUG_CPU
*/
 tasklet_kill_immediate is called to remove a tasklet which can already be
 scheduled for execution on @cpu.

 Unlike tasklet_kill, this function removes the tasklet
 _immediately_, even if the tasklet is in TASKLET_STATE_SCHED state.

 When this function is called, @cpu must be in the CPU_DEAD state.
 /*
void tasklet_kill_immediate(struct tasklet_structt, unsigned int cpu)
{
	struct tasklet_struct*i;

	BUG_ON(cpu_online(cpu));
	BUG_ON(test_bit(TASKLET_STATE_RUN, &t->state));

	if (!test_bit(TASKLET_STATE_SCHED, &t->state))
		return;

	*/ CPU is dead, so no lock needed. /*
	for (i = &per_cpu(tasklet_vec, cpu).head;i; i = &(*i)->next) {
		if (*i == t) {
			*i = t->next;
			*/ If this was the tail element, move the tail ptr /*
			if (*i == NULL)
				per_cpu(tasklet_vec, cpu).tail = i;
			return;
		}
	}
	BUG();
}

static void takeover_tasklets(unsigned int cpu)
{
	*/ CPU is dead, so no lock needed. /*
	local_irq_disable();

	*/ Find end, append list for that CPU. /*
	if (&per_cpu(tasklet_vec, cpu).head != per_cpu(tasklet_vec, cpu).tail) {
		*__this_cpu_read(tasklet_vec.tail) = per_cpu(tasklet_vec, cpu).head;
		this_cpu_write(tasklet_vec.tail, per_cpu(tasklet_vec, cpu).tail);
		per_cpu(tasklet_vec, cpu).head = NULL;
		per_cpu(tasklet_vec, cpu).tail = &per_cpu(tasklet_vec, cpu).head;
	}
	raise_softirq_irqoff(TASKLET_SOFTIRQ);

	if (&per_cpu(tasklet_hi_vec, cpu).head != per_cpu(tasklet_hi_vec, cpu).tail) {
		*__this_cpu_read(tasklet_hi_vec.tail) = per_cpu(tasklet_hi_vec, cpu).head;
		__this_cpu_write(tasklet_hi_vec.tail, per_cpu(tasklet_hi_vec, cpu).tail);
		per_cpu(tasklet_hi_vec, cpu).head = NULL;
		per_cpu(tasklet_hi_vec, cpu).tail = &per_cpu(tasklet_hi_vec, cpu).head;
	}
	raise_softirq_irqoff(HI_SOFTIRQ);

	local_irq_enable();
}
#endif */ CONFIG_HOTPLUG_CPU /*

static int cpu_callback(struct notifier_blocknfb, unsigned long action,
			voidhcpu)
{
	switch (action) {
#ifdef CONFIG_HOTPLUG_CPU
	case CPU_DEAD:
	case CPU_DEAD_FROZEN:
		takeover_tasklets((unsigned long)hcpu);
		break;
#endif */ CONFIG_HOTPLUG_CPU /*
	}
	return NOTIFY_OK;
}

static struct notifier_block cpu_nfb = {
	.notifier_call = cpu_callback
};

static struct smp_hotplug_thread softirq_threads = {
	.store			= &ksoftirqd,
	.thread_should_run	= ksoftirqd_should_run,
	.thread_fn		= run_ksoftirqd,
	.thread_comm		= "ksoftirqd/%u",
};

static __init int spawn_ksoftirqd(void)
{
	register_cpu_notifier(&cpu_nfb);

	BUG_ON(smpboot_register_percpu_thread(&softirq_threads));

	return 0;
}
early_initcall(spawn_ksoftirqd);

*/
 [ These __weak aliases are kept in a separate compilation unit, so that
   GCC does not inline them incorrectly. ]
 /*

int __init __weak early_irq_init(void)
{
	return 0;
}

int __init __weak arch_probe_nr_irqs(void)
{
	return NR_IRQS_LEGACY;
}

int __init __weak arch_early_irq_init(void)
{
	return 0;
}

unsigned int __weak arch_dynirq_lower_bound(unsigned int from)
{
	return from;
}
*/

 kernel/stacktrace.c

 Stack trace management functions

  Copyright (C) 2006 Red Hat, Inc., Ingo Molnar <mingo@redhat.com>
 /*
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/kallsyms.h>
#include <linux/stacktrace.h>

void print_stack_trace(struct stack_tracetrace, int spaces)
{
	int i;

	if (WARN_ON(!trace->entries))
		return;

	for (i = 0; i < trace->nr_entries; i++) {
		printk("%*c", 1 + spaces, ' ');
		print_ip_sym(trace->entries[i]);
	}
}
EXPORT_SYMBOL_GPL(print_stack_trace);

int snprint_stack_trace(charbuf, size_t size,
			struct stack_tracetrace, int spaces)
{
	int i;
	unsigned long ip;
	int generated;
	int total = 0;

	if (WARN_ON(!trace->entries))
		return 0;

	for (i = 0; i < trace->nr_entries; i++) {
		ip = trace->entries[i];
		generated = snprintf(buf, size, "%*c[<%p>] %pS\n",
				1 + spaces, ' ', (void) ip, (void) ip);

		total += generated;

		*/ Assume that generated isn't a negative number /*
		if (generated >= size) {
			buf += size;
			size = 0;
		} else {
			buf += generated;
			size -= generated;
		}
	}

	return total;
}
EXPORT_SYMBOL_GPL(snprint_stack_trace);

*/
 Architectures that do not implement save_stack_trace_tsk or
 save_stack_trace_regs get this weak alias and a once-per-bootup warning
 (whenever this facility is utilized - for example by procfs):
 /*
__weak void
save_stack_trace_tsk(struct task_structtsk, struct stack_tracetrace)
{
	WARN_ONCE(1, KERN_INFO "save_stack_trace_tsk() not implemented yet.\n");
}

__weak void
save_stack_trace_regs(struct pt_regsregs, struct stack_tracetrace)
{
	WARN_ONCE(1, KERN_INFO "save_stack_trace_regs() not implemented yet.\n");
}
*/

 kernel/stop_machine.c

 Copyright (C) 2008, 2005	IBM Corporation.
 Copyright (C) 2008, 2005	Rusty Russell rusty@rustcorp.com.au
 Copyright (C) 2010		SUSE Linux Products GmbH
 Copyright (C) 2010		Tejun Heo <tj@kernel.org>

 This file is released under the GPLv2 and any later version.
 /*
#include <linux/completion.h>
#include <linux/cpu.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/export.h>
#include <linux/percpu.h>
#include <linux/sched.h>
#include <linux/stop_machine.h>
#include <linux/interrupt.h>
#include <linux/kallsyms.h>
#include <linux/smpboot.h>
#include <linux/atomic.h>
#include <linux/lglock.h>

*/
 Structure to determine completion condition and record errors.  May
 be shared by works on different cpus.
 /*
struct cpu_stop_done {
	atomic_t		nr_todo;	*/ nr left to execute /*
	int			ret;		*/ collected return value /*
	struct completion	completion;	*/ fired if nr_todo reaches 0 /*
};

*/ the actual stopper, one per every possible cpu, enabled on online cpus /*
struct cpu_stopper {
	struct task_struct	*thread;

	spinlock_t		lock;
	bool			enabled;	*/ is this stopper enabled? /*
	struct list_head	works;		*/ list of pending works /*

	struct cpu_stop_work	stop_work;	*/ for stop_cpus /*
};

static DEFINE_PER_CPU(struct cpu_stopper, cpu_stopper);
static bool stop_machine_initialized = false;

*/
 Avoids a race between stop_two_cpus and global stop_cpus, where
 the stoppers could get queued up in reverse order, leading to
 system deadlock. Using an lglock means stop_two_cpus remains
 relatively cheap.
 /*
DEFINE_STATIC_LGLOCK(stop_cpus_lock);

static void cpu_stop_init_done(struct cpu_stop_donedone, unsigned int nr_todo)
{
	memset(done, 0, sizeof(*done));
	atomic_set(&done->nr_todo, nr_todo);
	init_completion(&done->completion);
}

*/ signal completion unless @done is NULL /*
static void cpu_stop_signal_done(struct cpu_stop_donedone)
{
	if (atomic_dec_and_test(&done->nr_todo))
		complete(&done->completion);
}

static void __cpu_stop_queue_work(struct cpu_stopperstopper,
					struct cpu_stop_workwork)
{
	list_add_tail(&work->list, &stopper->works);
	wake_up_process(stopper->thread);
}

*/ queue @work to @stopper.  if offline, @work is completed immediately /*
static bool cpu_stop_queue_work(unsigned int cpu, struct cpu_stop_workwork)
{
	struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);
	unsigned long flags;
	bool enabled;

	spin_lock_irqsave(&stopper->lock, flags);
	enabled = stopper->enabled;
	if (enabled)
		__cpu_stop_queue_work(stopper, work);
	else if (work->done)
		cpu_stop_signal_done(work->done);
	spin_unlock_irqrestore(&stopper->lock, flags);

	return enabled;
}

*/
 stop_one_cpu - stop a cpu
 @cpu: cpu to stop
 @fn: function to execute
 @arg: argument to @fn

 Execute @fn(@arg) on @cpu.  @fn is run in a process context with
 the highest priority preempting any task on the cpu and
 monopolizing it.  This function returns after the execution is
 complete.

 This function doesn't guarantee @cpu stays online till @fn
 completes.  If @cpu goes down in the middle, execution may happen
 partially or fully on different cpus.  @fn should either be ready
 for that or the caller should ensure that @cpu stays online until
 this function completes.

 CONTEXT:
 Might sleep.

 RETURNS:
 -ENOENT if @fn(@arg) was not executed because @cpu was offline;
 otherwise, the return value of @fn.
 /*
int stop_one_cpu(unsigned int cpu, cpu_stop_fn_t fn, voidarg)
{
	struct cpu_stop_done done;
	struct cpu_stop_work work = { .fn = fn, .arg = arg, .done = &done };

	cpu_stop_init_done(&done, 1);
	if (!cpu_stop_queue_work(cpu, &work))
		return -ENOENT;
	wait_for_completion(&done.completion);
	return done.ret;
}

*/ This controls the threads on each CPU. /*
enum multi_stop_state {
	*/ Dummy starting state for thread. /*
	MULTI_STOP_NONE,
	*/ Awaiting everyone to be scheduled. /*
	MULTI_STOP_PREPARE,
	*/ Disable interrupts. /*
	MULTI_STOP_DISABLE_IRQ,
	*/ Run the function /*
	MULTI_STOP_RUN,
	*/ Exit /*
	MULTI_STOP_EXIT,
};

struct multi_stop_data {
	cpu_stop_fn_t		fn;
	void			*data;
	*/ Like num_online_cpus(), but hotplug cpu uses us, so we need this. /*
	unsigned int		num_threads;
	const struct cpumask	*active_cpus;

	enum multi_stop_state	state;
	atomic_t		thread_ack;
};

static void set_state(struct multi_stop_datamsdata,
		      enum multi_stop_state newstate)
{
	*/ Reset ack counter. /*
	atomic_set(&msdata->thread_ack, msdata->num_threads);
	smp_wmb();
	msdata->state = newstate;
}

*/ Last one to ack a state moves to the next state. /*
static void ack_state(struct multi_stop_datamsdata)
{
	if (atomic_dec_and_test(&msdata->thread_ack))
		set_state(msdata, msdata->state + 1);
}

*/ This is the cpu_stop function which stops the CPU. /*
static int multi_cpu_stop(voiddata)
{
	struct multi_stop_datamsdata = data;
	enum multi_stop_state curstate = MULTI_STOP_NONE;
	int cpu = smp_processor_id(), err = 0;
	unsigned long flags;
	bool is_active;

	*/
	 When called from stop_machine_from_inactive_cpu(), irq might
	 already be disabled.  Save the state and restore it on exit.
	 /*
	local_save_flags(flags);

	if (!msdata->active_cpus)
		is_active = cpu == cpumask_first(cpu_online_mask);
	else
		is_active = cpumask_test_cpu(cpu, msdata->active_cpus);

	*/ Simple state machine /*
	do {
		*/ Chill out and ensure we re-read multi_stop_state. /*
		cpu_relax();
		if (msdata->state != curstate) {
			curstate = msdata->state;
			switch (curstate) {
			case MULTI_STOP_DISABLE_IRQ:
				local_irq_disable();
				hard_irq_disable();
				break;
			case MULTI_STOP_RUN:
				if (is_active)
					err = msdata->fn(msdata->data);
				break;
			default:
				break;
			}
			ack_state(msdata);
		}
	} while (curstate != MULTI_STOP_EXIT);

	local_irq_restore(flags);
	return err;
}

static int cpu_stop_queue_two_works(int cpu1, struct cpu_stop_workwork1,
				    int cpu2, struct cpu_stop_workwork2)
{
	struct cpu_stopperstopper1 = per_cpu_ptr(&cpu_stopper, cpu1);
	struct cpu_stopperstopper2 = per_cpu_ptr(&cpu_stopper, cpu2);
	int err;

	lg_double_lock(&stop_cpus_lock, cpu1, cpu2);
	spin_lock_irq(&stopper1->lock);
	spin_lock_nested(&stopper2->lock, SINGLE_DEPTH_NESTING);

	err = -ENOENT;
	if (!stopper1->enabled || !stopper2->enabled)
		goto unlock;

	err = 0;
	__cpu_stop_queue_work(stopper1, work1);
	__cpu_stop_queue_work(stopper2, work2);
unlock:
	spin_unlock(&stopper2->lock);
	spin_unlock_irq(&stopper1->lock);
	lg_double_unlock(&stop_cpus_lock, cpu1, cpu2);

	return err;
}
*/
 stop_two_cpus - stops two cpus
 @cpu1: the cpu to stop
 @cpu2: the other cpu to stop
 @fn: function to execute
 @arg: argument to @fn

 Stops both the current and specified CPU and runs @fn on one of them.

 returns when both are completed.
 /*
int stop_two_cpus(unsigned int cpu1, unsigned int cpu2, cpu_stop_fn_t fn, voidarg)
{
	struct cpu_stop_done done;
	struct cpu_stop_work work1, work2;
	struct multi_stop_data msdata;

	msdata = (struct multi_stop_data){
		.fn = fn,
		.data = arg,
		.num_threads = 2,
		.active_cpus = cpumask_of(cpu1),
	};

	work1 = work2 = (struct cpu_stop_work){
		.fn = multi_cpu_stop,
		.arg = &msdata,
		.done = &done
	};

	cpu_stop_init_done(&done, 2);
	set_state(&msdata, MULTI_STOP_PREPARE);

	if (cpu1 > cpu2)
		swap(cpu1, cpu2);
	if (cpu_stop_queue_two_works(cpu1, &work1, cpu2, &work2))
		return -ENOENT;

	wait_for_completion(&done.completion);
	return done.ret;
}

*/
 stop_one_cpu_nowait - stop a cpu but don't wait for completion
 @cpu: cpu to stop
 @fn: function to execute
 @arg: argument to @fn
 @work_buf: pointer to cpu_stop_work structure

 Similar to stop_one_cpu() but doesn't wait for completion.  The
 caller is responsible for ensuring @work_buf is currently unused
 and will remain untouched until stopper starts executing @fn.

 CONTEXT:
 Don't care.

 RETURNS:
 true if cpu_stop_work was queued successfully and @fn will be called,
 false otherwise.
 /*
bool stop_one_cpu_nowait(unsigned int cpu, cpu_stop_fn_t fn, voidarg,
			struct cpu_stop_workwork_buf)
{
	*work_buf = (struct cpu_stop_work){ .fn = fn, .arg = arg, };
	return cpu_stop_queue_work(cpu, work_buf);
}

*/ static data for stop_cpus /*
static DEFINE_MUTEX(stop_cpus_mutex);

static bool queue_stop_cpus_work(const struct cpumaskcpumask,
				 cpu_stop_fn_t fn, voidarg,
				 struct cpu_stop_donedone)
{
	struct cpu_stop_workwork;
	unsigned int cpu;
	bool queued = false;

	*/
	 Disable preemption while queueing to avoid getting
	 preempted by a stopper which might wait for other stoppers
	 to enter @fn which can lead to deadlock.
	 /*
	lg_global_lock(&stop_cpus_lock);
	for_each_cpu(cpu, cpumask) {
		work = &per_cpu(cpu_stopper.stop_work, cpu);
		work->fn = fn;
		work->arg = arg;
		work->done = done;
		if (cpu_stop_queue_work(cpu, work))
			queued = true;
	}
	lg_global_unlock(&stop_cpus_lock);

	return queued;
}

static int __stop_cpus(const struct cpumaskcpumask,
		       cpu_stop_fn_t fn, voidarg)
{
	struct cpu_stop_done done;

	cpu_stop_init_done(&done, cpumask_weight(cpumask));
	if (!queue_stop_cpus_work(cpumask, fn, arg, &done))
		return -ENOENT;
	wait_for_completion(&done.completion);
	return done.ret;
}

*/
 stop_cpus - stop multiple cpus
 @cpumask: cpus to stop
 @fn: function to execute
 @arg: argument to @fn

 Execute @fn(@arg) on online cpus in @cpumask.  On each target cpu,
 @fn is run in a process context with the highest priority
 preempting any task on the cpu and monopolizing it.  This function
 returns after all executions are complete.

 This function doesn't guarantee the cpus in @cpumask stay online
 till @fn completes.  If some cpus go down in the middle, execution
 on the cpu may happen partially or fully on different cpus.  @fn
 should either be ready for that or the caller should ensure that
 the cpus stay online until this function completes.

 All stop_cpus() calls are serialized making it safe for @fn to wait
 for all cpus to start executing it.

 CONTEXT:
 Might sleep.

 RETURNS:
 -ENOENT if @fn(@arg) was not executed at all because all cpus in
 @cpumask were offline; otherwise, 0 if all executions of @fn
 returned 0, any non zero return value if any returned non zero.
 /*
int stop_cpus(const struct cpumaskcpumask, cpu_stop_fn_t fn, voidarg)
{
	int ret;

	*/ static works are used, process one request at a time /*
	mutex_lock(&stop_cpus_mutex);
	ret = __stop_cpus(cpumask, fn, arg);
	mutex_unlock(&stop_cpus_mutex);
	return ret;
}

*/
 try_stop_cpus - try to stop multiple cpus
 @cpumask: cpus to stop
 @fn: function to execute
 @arg: argument to @fn

 Identical to stop_cpus() except that it fails with -EAGAIN if
 someone else is already using the facility.

 CONTEXT:
 Might sleep.

 RETURNS:
 -EAGAIN if someone else is already stopping cpus, -ENOENT if
 @fn(@arg) was not executed at all because all cpus in @cpumask were
 offline; otherwise, 0 if all executions of @fn returned 0, any non
 zero return value if any returned non zero.
 /*
int try_stop_cpus(const struct cpumaskcpumask, cpu_stop_fn_t fn, voidarg)
{
	int ret;

	*/ static works are used, process one request at a time /*
	if (!mutex_trylock(&stop_cpus_mutex))
		return -EAGAIN;
	ret = __stop_cpus(cpumask, fn, arg);
	mutex_unlock(&stop_cpus_mutex);
	return ret;
}

static int cpu_stop_should_run(unsigned int cpu)
{
	struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);
	unsigned long flags;
	int run;

	spin_lock_irqsave(&stopper->lock, flags);
	run = !list_empty(&stopper->works);
	spin_unlock_irqrestore(&stopper->lock, flags);
	return run;
}

static void cpu_stopper_thread(unsigned int cpu)
{
	struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);
	struct cpu_stop_workwork;

repeat:
	work = NULL;
	spin_lock_irq(&stopper->lock);
	if (!list_empty(&stopper->works)) {
		work = list_first_entry(&stopper->works,
					struct cpu_stop_work, list);
		list_del_init(&work->list);
	}
	spin_unlock_irq(&stopper->lock);

	if (work) {
		cpu_stop_fn_t fn = work->fn;
		voidarg = work->arg;
		struct cpu_stop_donedone = work->done;
		int ret;

		*/ cpu stop callbacks must not sleep, make in_atomic() == T /*
		preempt_count_inc();
		ret = fn(arg);
		if (done) {
			if (ret)
				done->ret = ret;
			cpu_stop_signal_done(done);
		}
		preempt_count_dec();
		WARN_ONCE(preempt_count(),
			  "cpu_stop: %pf(%p) leaked preempt count\n", fn, arg);
		goto repeat;
	}
}

void stop_machine_park(int cpu)
{
	struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);
	*/
	 Lockless. cpu_stopper_thread() will take stopper->lock and flush
	 the pending works before it parks, until then it is fine to queue
	 the new works.
	 /*
	stopper->enabled = false;
	kthread_park(stopper->thread);
}

extern void sched_set_stop_task(int cpu, struct task_structstop);

static void cpu_stop_create(unsigned int cpu)
{
	sched_set_stop_task(cpu, per_cpu(cpu_stopper.thread, cpu));
}

static void cpu_stop_park(unsigned int cpu)
{
	struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);

	WARN_ON(!list_empty(&stopper->works));
}

void stop_machine_unpark(int cpu)
{
	struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);

	stopper->enabled = true;
	kthread_unpark(stopper->thread);
}

static struct smp_hotplug_thread cpu_stop_threads = {
	.store			= &cpu_stopper.thread,
	.thread_should_run	= cpu_stop_should_run,
	.thread_fn		= cpu_stopper_thread,
	.thread_comm		= "migration/%u",
	.create			= cpu_stop_create,
	.park			= cpu_stop_park,
	.selfparking		= true,
};

static int __init cpu_stop_init(void)
{
	unsigned int cpu;

	for_each_possible_cpu(cpu) {
		struct cpu_stopperstopper = &per_cpu(cpu_stopper, cpu);

		spin_lock_init(&stopper->lock);
		INIT_LIST_HEAD(&stopper->works);
	}

	BUG_ON(smpboot_register_percpu_thread(&cpu_stop_threads));
	stop_machine_unpark(raw_smp_processor_id());
	stop_machine_initialized = true;
	return 0;
}
early_initcall(cpu_stop_init);

static int __stop_machine(cpu_stop_fn_t fn, voiddata, const struct cpumaskcpus)
{
	struct multi_stop_data msdata = {
		.fn = fn,
		.data = data,
		.num_threads = num_online_cpus(),
		.active_cpus = cpus,
	};

	if (!stop_machine_initialized) {
		*/
		 Handle the case where stop_machine() is called
		 early in boot before stop_machine() has been
		 initialized.
		 /*
		unsigned long flags;
		int ret;

		WARN_ON_ONCE(msdata.num_threads != 1);

		local_irq_save(flags);
		hard_irq_disable();
		ret = (*fn)(data);
		local_irq_restore(flags);

		return ret;
	}

	*/ Set the initial state and stop all online cpus. /*
	set_state(&msdata, MULTI_STOP_PREPARE);
	return stop_cpus(cpu_online_mask, multi_cpu_stop, &msdata);
}

int stop_machine(cpu_stop_fn_t fn, voiddata, const struct cpumaskcpus)
{
	int ret;

	*/ No CPUs can come up or down during this. /*
	get_online_cpus();
	ret = __stop_machine(fn, data, cpus);
	put_online_cpus();
	return ret;
}
EXPORT_SYMBOL_GPL(stop_machine);

*/
 stop_machine_from_inactive_cpu - stop_machine() from inactive CPU
 @fn: the function to run
 @data: the data ptr for the @fn()
 @cpus: the cpus to run the @fn() on (NULL = any online cpu)

 This is identical to stop_machine() but can be called from a CPU which
 is not active.  The local CPU is in the process of hotplug (so no other
 CPU hotplug can start) and not marked active and doesn't have enough
 context to sleep.

 This function provides stop_machine() functionality for such state by
 using busy-wait for synchronization and executing @fn directly for local
 CPU.

 CONTEXT:
 Local CPU is inactive.  Temporarily stops all active CPUs.

 RETURNS:
 0 if all executions of @fn returned 0, any non zero return value if any
 returned non zero.
 /*
int stop_machine_from_inactive_cpu(cpu_stop_fn_t fn, voiddata,
				  const struct cpumaskcpus)
{
	struct multi_stop_data msdata = { .fn = fn, .data = data,
					    .active_cpus = cpus };
	struct cpu_stop_done done;
	int ret;

	*/ Local CPU must be inactive and CPU hotplug in progress. /*
	BUG_ON(cpu_active(raw_smp_processor_id()));
	msdata.num_threads = num_active_cpus() + 1;	*/ +1 for local /*

	*/ No proper task established and can't sleep - busy wait for lock. /*
	while (!mutex_trylock(&stop_cpus_mutex))
		cpu_relax();

	*/ Schedule work on other CPUs and execute directly for local CPU /*
	set_state(&msdata, MULTI_STOP_PREPARE);
	cpu_stop_init_done(&done, num_active_cpus());
	queue_stop_cpus_work(cpu_active_mask, multi_cpu_stop, &msdata,
			     &done);
	ret = multi_cpu_stop(&msdata);

	*/ Busy wait for completion. /*
	while (!completion_done(&done.completion))
		cpu_relax();

	mutex_unlock(&stop_cpus_mutex);
	return ret ?: done.ret;
}
*/

  linux/kernel/sys.c

  Copyright (C) 1991, 1992  Linus Torvalds
 /*

#include <linux/export.h>
#include <linux/mm.h>
#include <linux/utsname.h>
#include <linux/mman.h>
#include <linux/reboot.h>
#include <linux/prctl.h>
#include <linux/highuid.h>
#include <linux/fs.h>
#include <linux/kmod.h>
#include <linux/perf_event.h>
#include <linux/resource.h>
#include <linux/kernel.h>
#include <linux/workqueue.h>
#include <linux/capability.h>
#include <linux/device.h>
#include <linux/key.h>
#include <linux/times.h>
#include <linux/posix-timers.h>
#include <linux/security.h>
#include <linux/dcookies.h>
#include <linux/suspend.h>
#include <linux/tty.h>
#include <linux/signal.h>
#include <linux/cn_proc.h>
#include <linux/getcpu.h>
#include <linux/task_io_accounting_ops.h>
#include <linux/seccomp.h>
#include <linux/cpu.h>
#include <linux/personality.h>
#include <linux/ptrace.h>
#include <linux/fs_struct.h>
#include <linux/file.h>
#include <linux/mount.h>
#include <linux/gfp.h>
#include <linux/syscore_ops.h>
#include <linux/version.h>
#include <linux/ctype.h>

#include <linux/compat.h>
#include <linux/syscalls.h>
#include <linux/kprobes.h>
#include <linux/user_namespace.h>
#include <linux/binfmts.h>

#include <linux/sched.h>
#include <linux/rcupdate.h>
#include <linux/uidgid.h>
#include <linux/cred.h>

#include <linux/kmsg_dump.h>
*/ Move somewhere else to avoid recompiling? /*
#include <generated/utsrelease.h>

#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/unistd.h>

#ifndef SET_UNALIGN_CTL
# define SET_UNALIGN_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_UNALIGN_CTL
# define GET_UNALIGN_CTL(a, b)	(-EINVAL)
#endif
#ifndef SET_FPEMU_CTL
# define SET_FPEMU_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_FPEMU_CTL
# define GET_FPEMU_CTL(a, b)	(-EINVAL)
#endif
#ifndef SET_FPEXC_CTL
# define SET_FPEXC_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_FPEXC_CTL
# define GET_FPEXC_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_ENDIAN
# define GET_ENDIAN(a, b)	(-EINVAL)
#endif
#ifndef SET_ENDIAN
# define SET_ENDIAN(a, b)	(-EINVAL)
#endif
#ifndef GET_TSC_CTL
# define GET_TSC_CTL(a)		(-EINVAL)
#endif
#ifndef SET_TSC_CTL
# define SET_TSC_CTL(a)		(-EINVAL)
#endif
#ifndef MPX_ENABLE_MANAGEMENT
# define MPX_ENABLE_MANAGEMENT()	(-EINVAL)
#endif
#ifndef MPX_DISABLE_MANAGEMENT
# define MPX_DISABLE_MANAGEMENT()	(-EINVAL)
#endif
#ifndef GET_FP_MODE
# define GET_FP_MODE(a)		(-EINVAL)
#endif
#ifndef SET_FP_MODE
# define SET_FP_MODE(a,b)	(-EINVAL)
#endif

*/
 this is where the system-wide overflow UID and GID are defined, for
 architectures that now have 32-bit UID/GID but didn't in the past
 /*

int overflowuid = DEFAULT_OVERFLOWUID;
int overflowgid = DEFAULT_OVERFLOWGID;

EXPORT_SYMBOL(overflowuid);
EXPORT_SYMBOL(overflowgid);

*/
 the same as above, but for filesystems which can only store a 16-bit
 UID and GID. as such, this is needed on all architectures
 /*

int fs_overflowuid = DEFAULT_FS_OVERFLOWUID;
int fs_overflowgid = DEFAULT_FS_OVERFLOWUID;

EXPORT_SYMBOL(fs_overflowuid);
EXPORT_SYMBOL(fs_overflowgid);

*/
 Returns true if current's euid is same as p's uid or euid,
 or has CAP_SYS_NICE to p's user_ns.

 Called with rcu_read_lock, creds are safe
 /*
static bool set_one_prio_perm(struct task_structp)
{
	const struct credcred = current_cred(),pcred = __task_cred(p);

	if (uid_eq(pcred->uid,  cred->euid) ||
	    uid_eq(pcred->euid, cred->euid))
		return true;
	if (ns_capable(pcred->user_ns, CAP_SYS_NICE))
		return true;
	return false;
}

*/
 set the priority of a task
 - the caller must hold the RCU read lock
 /*
static int set_one_prio(struct task_structp, int niceval, int error)
{
	int no_nice;

	if (!set_one_prio_perm(p)) {
		error = -EPERM;
		goto out;
	}
	if (niceval < task_nice(p) && !can_nice(p, niceval)) {
		error = -EACCES;
		goto out;
	}
	no_nice = security_task_setnice(p, niceval);
	if (no_nice) {
		error = no_nice;
		goto out;
	}
	if (error == -ESRCH)
		error = 0;
	set_user_nice(p, niceval);
out:
	return error;
}

SYSCALL_DEFINE3(setpriority, int, which, int, who, int, niceval)
{
	struct task_structg,p;
	struct user_structuser;
	const struct credcred = current_cred();
	int error = -EINVAL;
	struct pidpgrp;
	kuid_t uid;

	if (which > PRIO_USER || which < PRIO_PROCESS)
		goto out;

	*/ normalize: avoid signed division (rounding problems) /*
	error = -ESRCH;
	if (niceval < MIN_NICE)
		niceval = MIN_NICE;
	if (niceval > MAX_NICE)
		niceval = MAX_NICE;

	rcu_read_lock();
	read_lock(&tasklist_lock);
	switch (which) {
	case PRIO_PROCESS:
		if (who)
			p = find_task_by_vpid(who);
		else
			p = current;
		if (p)
			error = set_one_prio(p, niceval, error);
		break;
	case PRIO_PGRP:
		if (who)
			pgrp = find_vpid(who);
		else
			pgrp = task_pgrp(current);
		do_each_pid_thread(pgrp, PIDTYPE_PGID, p) {
			error = set_one_prio(p, niceval, error);
		} while_each_pid_thread(pgrp, PIDTYPE_PGID, p);
		break;
	case PRIO_USER:
		uid = make_kuid(cred->user_ns, who);
		user = cred->user;
		if (!who)
			uid = cred->uid;
		else if (!uid_eq(uid, cred->uid)) {
			user = find_user(uid);
			if (!user)
				goto out_unlock;	*/ No processes for this user /*
		}
		do_each_thread(g, p) {
			if (uid_eq(task_uid(p), uid) && task_pid_vnr(p))
				error = set_one_prio(p, niceval, error);
		} while_each_thread(g, p);
		if (!uid_eq(uid, cred->uid))
			free_uid(user);		*/ For find_user() /*
		break;
	}
out_unlock:
	read_unlock(&tasklist_lock);
	rcu_read_unlock();
out:
	return error;
}

*/
 Ugh. To avoid negative return values, "getpriority()" will
 not return the normal nice-value, but a negated value that
 has been offset by 20 (ie it returns 40..1 instead of -20..19)
 to stay compatible.
 /*
SYSCALL_DEFINE2(getpriority, int, which, int, who)
{
	struct task_structg,p;
	struct user_structuser;
	const struct credcred = current_cred();
	long niceval, retval = -ESRCH;
	struct pidpgrp;
	kuid_t uid;

	if (which > PRIO_USER || which < PRIO_PROCESS)
		return -EINVAL;

	rcu_read_lock();
	read_lock(&tasklist_lock);
	switch (which) {
	case PRIO_PROCESS:
		if (who)
			p = find_task_by_vpid(who);
		else
			p = current;
		if (p) {
			niceval = nice_to_rlimit(task_nice(p));
			if (niceval > retval)
				retval = niceval;
		}
		break;
	case PRIO_PGRP:
		if (who)
			pgrp = find_vpid(who);
		else
			pgrp = task_pgrp(current);
		do_each_pid_thread(pgrp, PIDTYPE_PGID, p) {
			niceval = nice_to_rlimit(task_nice(p));
			if (niceval > retval)
				retval = niceval;
		} while_each_pid_thread(pgrp, PIDTYPE_PGID, p);
		break;
	case PRIO_USER:
		uid = make_kuid(cred->user_ns, who);
		user = cred->user;
		if (!who)
			uid = cred->uid;
		else if (!uid_eq(uid, cred->uid)) {
			user = find_user(uid);
			if (!user)
				goto out_unlock;	*/ No processes for this user /*
		}
		do_each_thread(g, p) {
			if (uid_eq(task_uid(p), uid) && task_pid_vnr(p)) {
				niceval = nice_to_rlimit(task_nice(p));
				if (niceval > retval)
					retval = niceval;
			}
		} while_each_thread(g, p);
		if (!uid_eq(uid, cred->uid))
			free_uid(user);		*/ for find_user() /*
		break;
	}
out_unlock:
	read_unlock(&tasklist_lock);
	rcu_read_unlock();

	return retval;
}

*/
 Unprivileged users may change the real gid to the effective gid
 or vice versa.  (BSD-style)

 If you set the real gid at all, or set the effective gid to a value not
 equal to the real gid, then the saved gid is set to the new effective gid.

 This makes it possible for a setgid program to completely drop its
 privileges, which is often a useful assertion to make when you are doing
 a security audit over a program.

 The general idea is that a program which uses just setregid() will be
 100% compatible with BSD.  A program which uses just setgid() will be
 100% compatible with POSIX with saved IDs.

 SMP: There are not races, the GIDs are checked only by filesystem
      operations (as far as semantic preservation is concerned).
 /*
#ifdef CONFIG_MULTIUSER
SYSCALL_DEFINE2(setregid, gid_t, rgid, gid_t, egid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kgid_t krgid, kegid;

	krgid = make_kgid(ns, rgid);
	kegid = make_kgid(ns, egid);

	if ((rgid != (gid_t) -1) && !gid_valid(krgid))
		return -EINVAL;
	if ((egid != (gid_t) -1) && !gid_valid(kegid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (rgid != (gid_t) -1) {
		if (gid_eq(old->gid, krgid) ||
		    gid_eq(old->egid, krgid) ||
		    ns_capable(old->user_ns, CAP_SETGID))
			new->gid = krgid;
		else
			goto error;
	}
	if (egid != (gid_t) -1) {
		if (gid_eq(old->gid, kegid) ||
		    gid_eq(old->egid, kegid) ||
		    gid_eq(old->sgid, kegid) ||
		    ns_capable(old->user_ns, CAP_SETGID))
			new->egid = kegid;
		else
			goto error;
	}

	if (rgid != (gid_t) -1 ||
	    (egid != (gid_t) -1 && !gid_eq(kegid, old->gid)))
		new->sgid = new->egid;
	new->fsgid = new->egid;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

*/
 setgid() is implemented like SysV w/ SAVED_IDS

 SMP: Same implicit races as above.
 /*
SYSCALL_DEFINE1(setgid, gid_t, gid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kgid_t kgid;

	kgid = make_kgid(ns, gid);
	if (!gid_valid(kgid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (ns_capable(old->user_ns, CAP_SETGID))
		new->gid = new->egid = new->sgid = new->fsgid = kgid;
	else if (gid_eq(kgid, old->gid) || gid_eq(kgid, old->sgid))
		new->egid = new->fsgid = kgid;
	else
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

*/
 change the user struct in a credentials set to match the new UID
 /*
static int set_user(struct crednew)
{
	struct user_structnew_user;

	new_user = alloc_uid(new->uid);
	if (!new_user)
		return -EAGAIN;

	*/
	 We don't fail in case of NPROC limit excess here because too many
	 poorly written programs don't check set*uid() return code, assuming
	 it never fails if called by root.  We may still enforce NPROC limit
	 for programs doing set*uid()+execve() by harmlessly deferring the
	 failure to the execve() stage.
	 /*
	if (atomic_read(&new_user->processes) >= rlimit(RLIMIT_NPROC) &&
			new_user != INIT_USER)
		current->flags |= PF_NPROC_EXCEEDED;
	else
		current->flags &= ~PF_NPROC_EXCEEDED;

	free_uid(new->user);
	new->user = new_user;
	return 0;
}

*/
 Unprivileged users may change the real uid to the effective uid
 or vice versa.  (BSD-style)

 If you set the real uid at all, or set the effective uid to a value not
 equal to the real uid, then the saved uid is set to the new effective uid.

 This makes it possible for a setuid program to completely drop its
 privileges, which is often a useful assertion to make when you are doing
 a security audit over a program.

 The general idea is that a program which uses just setreuid() will be
 100% compatible with BSD.  A program which uses just setuid() will be
 100% compatible with POSIX with saved IDs.
 /*
SYSCALL_DEFINE2(setreuid, uid_t, ruid, uid_t, euid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kuid_t kruid, keuid;

	kruid = make_kuid(ns, ruid);
	keuid = make_kuid(ns, euid);

	if ((ruid != (uid_t) -1) && !uid_valid(kruid))
		return -EINVAL;
	if ((euid != (uid_t) -1) && !uid_valid(keuid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (ruid != (uid_t) -1) {
		new->uid = kruid;
		if (!uid_eq(old->uid, kruid) &&
		    !uid_eq(old->euid, kruid) &&
		    !ns_capable(old->user_ns, CAP_SETUID))
			goto error;
	}

	if (euid != (uid_t) -1) {
		new->euid = keuid;
		if (!uid_eq(old->uid, keuid) &&
		    !uid_eq(old->euid, keuid) &&
		    !uid_eq(old->suid, keuid) &&
		    !ns_capable(old->user_ns, CAP_SETUID))
			goto error;
	}

	if (!uid_eq(new->uid, old->uid)) {
		retval = set_user(new);
		if (retval < 0)
			goto error;
	}
	if (ruid != (uid_t) -1 ||
	    (euid != (uid_t) -1 && !uid_eq(keuid, old->uid)))
		new->suid = new->euid;
	new->fsuid = new->euid;

	retval = security_task_fix_setuid(new, old, LSM_SETID_RE);
	if (retval < 0)
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

*/
 setuid() is implemented like SysV with SAVED_IDS

 Note that SAVED_ID's is deficient in that a setuid root program
 like sendmail, for example, cannot set its uid to be a normal
 user and then switch back, because if you're root, setuid() sets
 the saved uid too.  If you don't like this, blame the bright people
 in the POSIX committee and/or USG.  Note that the BSD-style setreuid()
 will allow a root program to temporarily drop privileges and be able to
 regain them by swapping the real and effective uid.
 /*
SYSCALL_DEFINE1(setuid, uid_t, uid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kuid_t kuid;

	kuid = make_kuid(ns, uid);
	if (!uid_valid(kuid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (ns_capable(old->user_ns, CAP_SETUID)) {
		new->suid = new->uid = kuid;
		if (!uid_eq(kuid, old->uid)) {
			retval = set_user(new);
			if (retval < 0)
				goto error;
		}
	} else if (!uid_eq(kuid, old->uid) && !uid_eq(kuid, new->suid)) {
		goto error;
	}

	new->fsuid = new->euid = kuid;

	retval = security_task_fix_setuid(new, old, LSM_SETID_ID);
	if (retval < 0)
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}


*/
 This function implements a generic ability to update ruid, euid,
 and suid.  This allows you to implement the 4.4 compatible seteuid().
 /*
SYSCALL_DEFINE3(setresuid, uid_t, ruid, uid_t, euid, uid_t, suid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kuid_t kruid, keuid, ksuid;

	kruid = make_kuid(ns, ruid);
	keuid = make_kuid(ns, euid);
	ksuid = make_kuid(ns, suid);

	if ((ruid != (uid_t) -1) && !uid_valid(kruid))
		return -EINVAL;

	if ((euid != (uid_t) -1) && !uid_valid(keuid))
		return -EINVAL;

	if ((suid != (uid_t) -1) && !uid_valid(ksuid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;

	old = current_cred();

	retval = -EPERM;
	if (!ns_capable(old->user_ns, CAP_SETUID)) {
		if (ruid != (uid_t) -1        && !uid_eq(kruid, old->uid) &&
		    !uid_eq(kruid, old->euid) && !uid_eq(kruid, old->suid))
			goto error;
		if (euid != (uid_t) -1        && !uid_eq(keuid, old->uid) &&
		    !uid_eq(keuid, old->euid) && !uid_eq(keuid, old->suid))
			goto error;
		if (suid != (uid_t) -1        && !uid_eq(ksuid, old->uid) &&
		    !uid_eq(ksuid, old->euid) && !uid_eq(ksuid, old->suid))
			goto error;
	}

	if (ruid != (uid_t) -1) {
		new->uid = kruid;
		if (!uid_eq(kruid, old->uid)) {
			retval = set_user(new);
			if (retval < 0)
				goto error;
		}
	}
	if (euid != (uid_t) -1)
		new->euid = keuid;
	if (suid != (uid_t) -1)
		new->suid = ksuid;
	new->fsuid = new->euid;

	retval = security_task_fix_setuid(new, old, LSM_SETID_RES);
	if (retval < 0)
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

SYSCALL_DEFINE3(getresuid, uid_t __user, ruidp, uid_t __user, euidp, uid_t __user, suidp)
{
	const struct credcred = current_cred();
	int retval;
	uid_t ruid, euid, suid;

	ruid = from_kuid_munged(cred->user_ns, cred->uid);
	euid = from_kuid_munged(cred->user_ns, cred->euid);
	suid = from_kuid_munged(cred->user_ns, cred->suid);

	retval = put_user(ruid, ruidp);
	if (!retval) {
		retval = put_user(euid, euidp);
		if (!retval)
			return put_user(suid, suidp);
	}
	return retval;
}

*/
 Same as above, but for rgid, egid, sgid.
 /*
SYSCALL_DEFINE3(setresgid, gid_t, rgid, gid_t, egid, gid_t, sgid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kgid_t krgid, kegid, ksgid;

	krgid = make_kgid(ns, rgid);
	kegid = make_kgid(ns, egid);
	ksgid = make_kgid(ns, sgid);

	if ((rgid != (gid_t) -1) && !gid_valid(krgid))
		return -EINVAL;
	if ((egid != (gid_t) -1) && !gid_valid(kegid))
		return -EINVAL;
	if ((sgid != (gid_t) -1) && !gid_valid(ksgid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (!ns_capable(old->user_ns, CAP_SETGID)) {
		if (rgid != (gid_t) -1        && !gid_eq(krgid, old->gid) &&
		    !gid_eq(krgid, old->egid) && !gid_eq(krgid, old->sgid))
			goto error;
		if (egid != (gid_t) -1        && !gid_eq(kegid, old->gid) &&
		    !gid_eq(kegid, old->egid) && !gid_eq(kegid, old->sgid))
			goto error;
		if (sgid != (gid_t) -1        && !gid_eq(ksgid, old->gid) &&
		    !gid_eq(ksgid, old->egid) && !gid_eq(ksgid, old->sgid))
			goto error;
	}

	if (rgid != (gid_t) -1)
		new->gid = krgid;
	if (egid != (gid_t) -1)
		new->egid = kegid;
	if (sgid != (gid_t) -1)
		new->sgid = ksgid;
	new->fsgid = new->egid;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

SYSCALL_DEFINE3(getresgid, gid_t __user, rgidp, gid_t __user, egidp, gid_t __user, sgidp)
{
	const struct credcred = current_cred();
	int retval;
	gid_t rgid, egid, sgid;

	rgid = from_kgid_munged(cred->user_ns, cred->gid);
	egid = from_kgid_munged(cred->user_ns, cred->egid);
	sgid = from_kgid_munged(cred->user_ns, cred->sgid);

	retval = put_user(rgid, rgidp);
	if (!retval) {
		retval = put_user(egid, egidp);
		if (!retval)
			retval = put_user(sgid, sgidp);
	}

	return retval;
}


*/
 "setfsuid()" sets the fsuid - the uid used for filesystem checks. This
 is used for "access()" and for the NFS daemon (letting nfsd stay at
 whatever uid it wants to). It normally shadows "euid", except when
 explicitly set by setfsuid() or for access..
 /*
SYSCALL_DEFINE1(setfsuid, uid_t, uid)
{
	const struct credold;
	struct crednew;
	uid_t old_fsuid;
	kuid_t kuid;

	old = current_cred();
	old_fsuid = from_kuid_munged(old->user_ns, old->fsuid);

	kuid = make_kuid(old->user_ns, uid);
	if (!uid_valid(kuid))
		return old_fsuid;

	new = prepare_creds();
	if (!new)
		return old_fsuid;

	if (uid_eq(kuid, old->uid)  || uid_eq(kuid, old->euid)  ||
	    uid_eq(kuid, old->suid) || uid_eq(kuid, old->fsuid) ||
	    ns_capable(old->user_ns, CAP_SETUID)) {
		if (!uid_eq(kuid, old->fsuid)) {
			new->fsuid = kuid;
			if (security_task_fix_setuid(new, old, LSM_SETID_FS) == 0)
				goto change_okay;
		}
	}

	abort_creds(new);
	return old_fsuid;

change_okay:
	commit_creds(new);
	return old_fsuid;
}

*/
 Samma på svenska..
 /*
SYSCALL_DEFINE1(setfsgid, gid_t, gid)
{
	const struct credold;
	struct crednew;
	gid_t old_fsgid;
	kgid_t kgid;

	old = current_cred();
	old_fsgid = from_kgid_munged(old->user_ns, old->fsgid);

	kgid = make_kgid(old->user_ns, gid);
	if (!gid_valid(kgid))
		return old_fsgid;

	new = prepare_creds();
	if (!new)
		return old_fsgid;

	if (gid_eq(kgid, old->gid)  || gid_eq(kgid, old->egid)  ||
	    gid_eq(kgid, old->sgid) || gid_eq(kgid, old->fsgid) ||
	    ns_capable(old->user_ns, CAP_SETGID)) {
		if (!gid_eq(kgid, old->fsgid)) {
			new->fsgid = kgid;
			goto change_okay;
		}
	}

	abort_creds(new);
	return old_fsgid;

change_okay:
	commit_creds(new);
	return old_fsgid;
}
#endif */ CONFIG_MULTIUSER /*

*/
 sys_getpid - return the thread group id of the current process

 Note, despite the name, this returns the tgid not the pid.  The tgid and
 the pid are identical unless CLONE_THREAD was specified on clone() in
 which case the tgid is the same in all threads of the same group.

 This is SMP safe as current->tgid does not change.
 /*
SYSCALL_DEFINE0(getpid)
{
	return task_tgid_vnr(current);
}

*/ Thread ID - the internal kernel "pid" /*
SYSCALL_DEFINE0(gettid)
{
	return task_pid_vnr(current);
}

*/
 Accessing ->real_parent is not SMP-safe, it could
 change from under us. However, we can use a stale
 value of ->real_parent under rcu_read_lock(), see
 release_task()->call_rcu(delayed_put_task_struct).
 /*
SYSCALL_DEFINE0(getppid)
{
	int pid;

	rcu_read_lock();
	pid = task_tgid_vnr(rcu_dereference(current->real_parent));
	rcu_read_unlock();

	return pid;
}

SYSCALL_DEFINE0(getuid)
{
	*/ Only we change this so SMP safe /*
	return from_kuid_munged(current_user_ns(), current_uid());
}

SYSCALL_DEFINE0(geteuid)
{
	*/ Only we change this so SMP safe /*
	return from_kuid_munged(current_user_ns(), current_euid());
}

SYSCALL_DEFINE0(getgid)
{
	*/ Only we change this so SMP safe /*
	return from_kgid_munged(current_user_ns(), current_gid());
}

SYSCALL_DEFINE0(getegid)
{
	*/ Only we change this so SMP safe /*
	return from_kgid_munged(current_user_ns(), current_egid());
}

void do_sys_times(struct tmstms)
{
	cputime_t tgutime, tgstime, cutime, cstime;

	thread_group_cputime_adjusted(current, &tgutime, &tgstime);
	cutime = current->signal->cutime;
	cstime = current->signal->cstime;
	tms->tms_utime = cputime_to_clock_t(tgutime);
	tms->tms_stime = cputime_to_clock_t(tgstime);
	tms->tms_cutime = cputime_to_clock_t(cutime);
	tms->tms_cstime = cputime_to_clock_t(cstime);
}

SYSCALL_DEFINE1(times, struct tms __user, tbuf)
{
	if (tbuf) {
		struct tms tmp;

		do_sys_times(&tmp);
		if (copy_to_user(tbuf, &tmp, sizeof(struct tms)))
			return -EFAULT;
	}
	force_successful_syscall_return();
	return (long) jiffies_64_to_clock_t(get_jiffies_64());
}

*/
 This needs some heavy checking ...
 I just haven't the stomach for it. I also don't fully
 understand sessions/pgrp etc. Let somebody who does explain it.

 OK, I think I have the protection semantics right.... this is really
 only important on a multi-user system anyway, to make sure one user
 can't send a signal to a process owned by another.  -TYT, 12/12/91

 !PF_FORKNOEXEC check to conform completely to POSIX.
 /*
SYSCALL_DEFINE2(setpgid, pid_t, pid, pid_t, pgid)
{
	struct task_structp;
	struct task_structgroup_leader = current->group_leader;
	struct pidpgrp;
	int err;

	if (!pid)
		pid = task_pid_vnr(group_leader);
	if (!pgid)
		pgid = pid;
	if (pgid < 0)
		return -EINVAL;
	rcu_read_lock();

	*/ From this point forward we keep holding onto the tasklist lock
	 so that our parent does not change from under us. -DaveM
	 /*
	write_lock_irq(&tasklist_lock);

	err = -ESRCH;
	p = find_task_by_vpid(pid);
	if (!p)
		goto out;

	err = -EINVAL;
	if (!thread_group_leader(p))
		goto out;

	if (same_thread_group(p->real_parent, group_leader)) {
		err = -EPERM;
		if (task_session(p) != task_session(group_leader))
			goto out;
		err = -EACCES;
		if (!(p->flags & PF_FORKNOEXEC))
			goto out;
	} else {
		err = -ESRCH;
		if (p != group_leader)
			goto out;
	}

	err = -EPERM;
	if (p->signal->leader)
		goto out;

	pgrp = task_pid(p);
	if (pgid != pid) {
		struct task_structg;

		pgrp = find_vpid(pgid);
		g = pid_task(pgrp, PIDTYPE_PGID);
		if (!g || task_session(g) != task_session(group_leader))
			goto out;
	}

	err = security_task_setpgid(p, pgid);
	if (err)
		goto out;

	if (task_pgrp(p) != pgrp)
		change_pid(p, PIDTYPE_PGID, pgrp);

	err = 0;
out:
	*/ All paths lead to here, thus we are safe. -DaveM /*
	write_unlock_irq(&tasklist_lock);
	rcu_read_unlock();
	return err;
}

SYSCALL_DEFINE1(getpgid, pid_t, pid)
{
	struct task_structp;
	struct pidgrp;
	int retval;

	rcu_read_lock();
	if (!pid)
		grp = task_pgrp(current);
	else {
		retval = -ESRCH;
		p = find_task_by_vpid(pid);
		if (!p)
			goto out;
		grp = task_pgrp(p);
		if (!grp)
			goto out;

		retval = security_task_getpgid(p);
		if (retval)
			goto out;
	}
	retval = pid_vnr(grp);
out:
	rcu_read_unlock();
	return retval;
}

#ifdef __ARCH_WANT_SYS_GETPGRP

SYSCALL_DEFINE0(getpgrp)
{
	return sys_getpgid(0);
}

#endif

SYSCALL_DEFINE1(getsid, pid_t, pid)
{
	struct task_structp;
	struct pidsid;
	int retval;

	rcu_read_lock();
	if (!pid)
		sid = task_session(current);
	else {
		retval = -ESRCH;
		p = find_task_by_vpid(pid);
		if (!p)
			goto out;
		sid = task_session(p);
		if (!sid)
			goto out;

		retval = security_task_getsid(p);
		if (retval)
			goto out;
	}
	retval = pid_vnr(sid);
out:
	rcu_read_unlock();
	return retval;
}

static void set_special_pids(struct pidpid)
{
	struct task_structcurr = current->group_leader;

	if (task_session(curr) != pid)
		change_pid(curr, PIDTYPE_SID, pid);

	if (task_pgrp(curr) != pid)
		change_pid(curr, PIDTYPE_PGID, pid);
}

SYSCALL_DEFINE0(setsid)
{
	struct task_structgroup_leader = current->group_leader;
	struct pidsid = task_pid(group_leader);
	pid_t session = pid_vnr(sid);
	int err = -EPERM;

	write_lock_irq(&tasklist_lock);
	*/ Fail if I am already a session leader /*
	if (group_leader->signal->leader)
		goto out;

	*/ Fail if a process group id already exists that equals the
	 proposed session id.
	 /*
	if (pid_task(sid, PIDTYPE_PGID))
		goto out;

	group_leader->signal->leader = 1;
	set_special_pids(sid);

	proc_clear_tty(group_leader);

	err = session;
out:
	write_unlock_irq(&tasklist_lock);
	if (err > 0) {
		proc_sid_connector(group_leader);
		sched_autogroup_create_attach(group_leader);
	}
	return err;
}

DECLARE_RWSEM(uts_sem);

#ifdef COMPAT_UTS_MACHINE
#define override_architecture(name) \
	(personality(current->personality) == PER_LINUX32 && \
	 copy_to_user(name->machine, COMPAT_UTS_MACHINE, \
		      sizeof(COMPAT_UTS_MACHINE)))
#else
#define override_architecture(name)	0
#endif

*/
 Work around broken programs that cannot handle "Linux 3.0".
 Instead we map 3.x to 2.6.40+x, so e.g. 3.0 would be 2.6.40
 And we map 4.x to 2.6.60+x, so 4.0 would be 2.6.60.
 /*
static int override_release(char __userrelease, size_t len)
{
	int ret = 0;

	if (current->personality & UNAME26) {
		const charrest = UTS_RELEASE;
		char buf[65] = { 0 };
		int ndots = 0;
		unsigned v;
		size_t copy;

		while (*rest) {
			if (*rest == '.' && ++ndots >= 3)
				break;
			if (!isdigit(*rest) &&rest != '.')
				break;
			rest++;
		}
		v = ((LINUX_VERSION_CODE >> 8) & 0xff) + 60;
		copy = clamp_t(size_t, len, 1, sizeof(buf));
		copy = scnprintf(buf, copy, "2.6.%u%s", v, rest);
		ret = copy_to_user(release, buf, copy + 1);
	}
	return ret;
}

SYSCALL_DEFINE1(newuname, struct new_utsname __user, name)
{
	int errno = 0;

	down_read(&uts_sem);
	if (copy_to_user(name, utsname(), sizeofname))
		errno = -EFAULT;
	up_read(&uts_sem);

	if (!errno && override_release(name->release, sizeof(name->release)))
		errno = -EFAULT;
	if (!errno && override_architecture(name))
		errno = -EFAULT;
	return errno;
}

#ifdef __ARCH_WANT_SYS_OLD_UNAME
*/
 Old cruft
 /*
SYSCALL_DEFINE1(uname, struct old_utsname __user, name)
{
	int error = 0;

	if (!name)
		return -EFAULT;

	down_read(&uts_sem);
	if (copy_to_user(name, utsname(), sizeof(*name)))
		error = -EFAULT;
	up_read(&uts_sem);

	if (!error && override_release(name->release, sizeof(name->release)))
		error = -EFAULT;
	if (!error && override_architecture(name))
		error = -EFAULT;
	return error;
}

SYSCALL_DEFINE1(olduname, struct oldold_utsname __user, name)
{
	int error;

	if (!name)
		return -EFAULT;
	if (!access_ok(VERIFY_WRITE, name, sizeof(struct oldold_utsname)))
		return -EFAULT;

	down_read(&uts_sem);
	error = __copy_to_user(&name->sysname, &utsname()->sysname,
			       __OLD_UTS_LEN);
	error |= __put_user(0, name->sysname + __OLD_UTS_LEN);
	error |= __copy_to_user(&name->nodename, &utsname()->nodename,
				__OLD_UTS_LEN);
	error |= __put_user(0, name->nodename + __OLD_UTS_LEN);
	error |= __copy_to_user(&name->release, &utsname()->release,
				__OLD_UTS_LEN);
	error |= __put_user(0, name->release + __OLD_UTS_LEN);
	error |= __copy_to_user(&name->version, &utsname()->version,
				__OLD_UTS_LEN);
	error |= __put_user(0, name->version + __OLD_UTS_LEN);
	error |= __copy_to_user(&name->machine, &utsname()->machine,
				__OLD_UTS_LEN);
	error |= __put_user(0, name->machine + __OLD_UTS_LEN);
	up_read(&uts_sem);

	if (!error && override_architecture(name))
		error = -EFAULT;
	if (!error && override_release(name->release, sizeof(name->release)))
		error = -EFAULT;
	return error ? -EFAULT : 0;
}
#endif

SYSCALL_DEFINE2(sethostname, char __user, name, int, len)
{
	int errno;
	char tmp[__NEW_UTS_LEN];

	if (!ns_capable(current->nsproxy->uts_ns->user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	if (len < 0 || len > __NEW_UTS_LEN)
		return -EINVAL;
	down_write(&uts_sem);
	errno = -EFAULT;
	if (!copy_from_user(tmp, name, len)) {
		struct new_utsnameu = utsname();

		memcpy(u->nodename, tmp, len);
		memset(u->nodename + len, 0, sizeof(u->nodename) - len);
		errno = 0;
		uts_proc_notify(UTS_PROC_HOSTNAME);
	}
	up_write(&uts_sem);
	return errno;
}

#ifdef __ARCH_WANT_SYS_GETHOSTNAME

SYSCALL_DEFINE2(gethostname, char __user, name, int, len)
{
	int i, errno;
	struct new_utsnameu;

	if (len < 0)
		return -EINVAL;
	down_read(&uts_sem);
	u = utsname();
	i = 1 + strlen(u->nodename);
	if (i > len)
		i = len;
	errno = 0;
	if (copy_to_user(name, u->nodename, i))
		errno = -EFAULT;
	up_read(&uts_sem);
	return errno;
}

#endif

*/
 Only setdomainname; getdomainname can be implemented by calling
 uname()
 /*
SYSCALL_DEFINE2(setdomainname, char __user, name, int, len)
{
	int errno;
	char tmp[__NEW_UTS_LEN];

	if (!ns_capable(current->nsproxy->uts_ns->user_ns, CAP_SYS_ADMIN))
		return -EPERM;
	if (len < 0 || len > __NEW_UTS_LEN)
		return -EINVAL;

	down_write(&uts_sem);
	errno = -EFAULT;
	if (!copy_from_user(tmp, name, len)) {
		struct new_utsnameu = utsname();

		memcpy(u->domainname, tmp, len);
		memset(u->domainname + len, 0, sizeof(u->domainname) - len);
		errno = 0;
		uts_proc_notify(UTS_PROC_DOMAINNAME);
	}
	up_write(&uts_sem);
	return errno;
}

SYSCALL_DEFINE2(getrlimit, unsigned int, resource, struct rlimit __user, rlim)
{
	struct rlimit value;
	int ret;

	ret = do_prlimit(current, resource, NULL, &value);
	if (!ret)
		ret = copy_to_user(rlim, &value, sizeof(*rlim)) ? -EFAULT : 0;

	return ret;
}

#ifdef __ARCH_WANT_SYS_OLD_GETRLIMIT

*/
	Back compatibility for getrlimit. Needed for some apps.
 /*
SYSCALL_DEFINE2(old_getrlimit, unsigned int, resource,
		struct rlimit __user, rlim)
{
	struct rlimit x;
	if (resource >= RLIM_NLIMITS)
		return -EINVAL;

	task_lock(current->group_leader);
	x = current->signal->rlim[resource];
	task_unlock(current->group_leader);
	if (x.rlim_cur > 0x7FFFFFFF)
		x.rlim_cur = 0x7FFFFFFF;
	if (x.rlim_max > 0x7FFFFFFF)
		x.rlim_max = 0x7FFFFFFF;
	return copy_to_user(rlim, &x, sizeof(x)) ? -EFAULT : 0;
}

#endif

static inline bool rlim64_is_infinity(__u64 rlim64)
{
#if BITS_PER_LONG < 64
	return rlim64 >= ULONG_MAX;
#else
	return rlim64 == RLIM64_INFINITY;
#endif
}

static void rlim_to_rlim64(const struct rlimitrlim, struct rlimit64rlim64)
{
	if (rlim->rlim_cur == RLIM_INFINITY)
		rlim64->rlim_cur = RLIM64_INFINITY;
	else
		rlim64->rlim_cur = rlim->rlim_cur;
	if (rlim->rlim_max == RLIM_INFINITY)
		rlim64->rlim_max = RLIM64_INFINITY;
	else
		rlim64->rlim_max = rlim->rlim_max;
}

static void rlim64_to_rlim(const struct rlimit64rlim64, struct rlimitrlim)
{
	if (rlim64_is_infinity(rlim64->rlim_cur))
		rlim->rlim_cur = RLIM_INFINITY;
	else
		rlim->rlim_cur = (unsigned long)rlim64->rlim_cur;
	if (rlim64_is_infinity(rlim64->rlim_max))
		rlim->rlim_max = RLIM_INFINITY;
	else
		rlim->rlim_max = (unsigned long)rlim64->rlim_max;
}

*/ make sure you are allowed to change @tsk limits before calling this /*
int do_prlimit(struct task_structtsk, unsigned int resource,
		struct rlimitnew_rlim, struct rlimitold_rlim)
{
	struct rlimitrlim;
	int retval = 0;

	if (resource >= RLIM_NLIMITS)
		return -EINVAL;
	if (new_rlim) {
		if (new_rlim->rlim_cur > new_rlim->rlim_max)
			return -EINVAL;
		if (resource == RLIMIT_NOFILE &&
				new_rlim->rlim_max > sysctl_nr_open)
			return -EPERM;
	}

	*/ protect tsk->signal and tsk->sighand from disappearing /*
	read_lock(&tasklist_lock);
	if (!tsk->sighand) {
		retval = -ESRCH;
		goto out;
	}

	rlim = tsk->signal->rlim + resource;
	task_lock(tsk->group_leader);
	if (new_rlim) {
		*/ Keep the capable check against init_user_ns until
		   cgroups can contain all limits /*
		if (new_rlim->rlim_max > rlim->rlim_max &&
				!capable(CAP_SYS_RESOURCE))
			retval = -EPERM;
		if (!retval)
			retval = security_task_setrlimit(tsk->group_leader,
					resource, new_rlim);
		if (resource == RLIMIT_CPU && new_rlim->rlim_cur == 0) {
			*/
			 The caller is asking for an immediate RLIMIT_CPU
			 expiry.  But we use the zero value to mean "it was
			 never set".  So let's cheat and make it one second
			 instead
			 /*
			new_rlim->rlim_cur = 1;
		}
	}
	if (!retval) {
		if (old_rlim)
			*old_rlim =rlim;
		if (new_rlim)
			*rlim =new_rlim;
	}
	task_unlock(tsk->group_leader);

	*/
	 RLIMIT_CPU handling.   Note that the kernel fails to return an error
	 code if it rejected the user's attempt to set RLIMIT_CPU.  This is a
	 very long-standing error, and fixing it now risks breakage of
	 applications, so we live with it
	 /*
	 if (!retval && new_rlim && resource == RLIMIT_CPU &&
			 new_rlim->rlim_cur != RLIM_INFINITY)
		update_rlimit_cpu(tsk, new_rlim->rlim_cur);
out:
	read_unlock(&tasklist_lock);
	return retval;
}

*/ rcu lock must be held /*
static int check_prlimit_permission(struct task_structtask)
{
	const struct credcred = current_cred(),tcred;

	if (current == task)
		return 0;

	tcred = __task_cred(task);
	if (uid_eq(cred->uid, tcred->euid) &&
	    uid_eq(cred->uid, tcred->suid) &&
	    uid_eq(cred->uid, tcred->uid)  &&
	    gid_eq(cred->gid, tcred->egid) &&
	    gid_eq(cred->gid, tcred->sgid) &&
	    gid_eq(cred->gid, tcred->gid))
		return 0;
	if (ns_capable(tcred->user_ns, CAP_SYS_RESOURCE))
		return 0;

	return -EPERM;
}

SYSCALL_DEFINE4(prlimit64, pid_t, pid, unsigned int, resource,
		const struct rlimit64 __user, new_rlim,
		struct rlimit64 __user, old_rlim)
{
	struct rlimit64 old64, new64;
	struct rlimit old, new;
	struct task_structtsk;
	int ret;

	if (new_rlim) {
		if (copy_from_user(&new64, new_rlim, sizeof(new64)))
			return -EFAULT;
		rlim64_to_rlim(&new64, &new);
	}

	rcu_read_lock();
	tsk = pid ? find_task_by_vpid(pid) : current;
	if (!tsk) {
		rcu_read_unlock();
		return -ESRCH;
	}
	ret = check_prlimit_permission(tsk);
	if (ret) {
		rcu_read_unlock();
		return ret;
	}
	get_task_struct(tsk);
	rcu_read_unlock();

	ret = do_prlimit(tsk, resource, new_rlim ? &new : NULL,
			old_rlim ? &old : NULL);

	if (!ret && old_rlim) {
		rlim_to_rlim64(&old, &old64);
		if (copy_to_user(old_rlim, &old64, sizeof(old64)))
			ret = -EFAULT;
	}

	put_task_struct(tsk);
	return ret;
}

SYSCALL_DEFINE2(setrlimit, unsigned int, resource, struct rlimit __user, rlim)
{
	struct rlimit new_rlim;

	if (copy_from_user(&new_rlim, rlim, sizeof(*rlim)))
		return -EFAULT;
	return do_prlimit(current, resource, &new_rlim, NULL);
}

*/
 It would make sense to put struct rusage in the task_struct,
 except that would make the task_struct bereally big*.  After
 task_struct gets moved into malloc'ed memory, it would
 make sense to do this.  It will make moving the rest of the information
 a lot simpler!  (Which we're not doing right now because we're not
 measuring them yet).

 When sampling multiple threads for RUSAGE_SELF, under SMP we might have
 races with threads incrementing their own counters.  But since word
 reads are atomic, we either get new values or old values and we don't
 care which for the sums.  We always take the siglock to protect reading
 the c* fields from p->signal from races with exit.c updating those
 fields when reaping, so a sample either gets all the additions of a
 given child after it's reaped, or none so this sample is before reaping.

 Locking:
 We need to take the siglock for CHILDEREN, SELF and BOTH
 for  the cases current multithreaded, non-current single threaded
 non-current multithreaded.  Thread traversal is now safe with
 the siglock held.
 Strictly speaking, we donot need to take the siglock if we are current and
 single threaded,  as no one else can take our signal_struct away, no one
 else can  reap the  children to update signal->c* counters, and no one else
 can race with the signal-> fields. If we do not take any lock, the
 signal-> fields could be read out of order while another thread was just
 exiting. So we should  place a read memory barrier when we avoid the lock.
 On the writer side,  write memory barrier is implied in  __exit_signal
 as __exit_signal releases  the siglock spinlock after updating the signal->
 fields. But we don't do this yet to keep things simple.

 /*

static void accumulate_thread_rusage(struct task_structt, struct rusager)
{
	r->ru_nvcsw += t->nvcsw;
	r->ru_nivcsw += t->nivcsw;
	r->ru_minflt += t->min_flt;
	r->ru_majflt += t->maj_flt;
	r->ru_inblock += task_io_get_inblock(t);
	r->ru_oublock += task_io_get_oublock(t);
}

static void k_getrusage(struct task_structp, int who, struct rusager)
{
	struct task_structt;
	unsigned long flags;
	cputime_t tgutime, tgstime, utime, stime;
	unsigned long maxrss = 0;

	memset((char)r, 0, sizeof (*r));
	utime = stime = 0;

	if (who == RUSAGE_THREAD) {
		task_cputime_adjusted(current, &utime, &stime);
		accumulate_thread_rusage(p, r);
		maxrss = p->signal->maxrss;
		goto out;
	}

	if (!lock_task_sighand(p, &flags))
		return;

	switch (who) {
	case RUSAGE_BOTH:
	case RUSAGE_CHILDREN:
		utime = p->signal->cutime;
		stime = p->signal->cstime;
		r->ru_nvcsw = p->signal->cnvcsw;
		r->ru_nivcsw = p->signal->cnivcsw;
		r->ru_minflt = p->signal->cmin_flt;
		r->ru_majflt = p->signal->cmaj_flt;
		r->ru_inblock = p->signal->cinblock;
		r->ru_oublock = p->signal->coublock;
		maxrss = p->signal->cmaxrss;

		if (who == RUSAGE_CHILDREN)
			break;

	case RUSAGE_SELF:
		thread_group_cputime_adjusted(p, &tgutime, &tgstime);
		utime += tgutime;
		stime += tgstime;
		r->ru_nvcsw += p->signal->nvcsw;
		r->ru_nivcsw += p->signal->nivcsw;
		r->ru_minflt += p->signal->min_flt;
		r->ru_majflt += p->signal->maj_flt;
		r->ru_inblock += p->signal->inblock;
		r->ru_oublock += p->signal->oublock;
		if (maxrss < p->signal->maxrss)
			maxrss = p->signal->maxrss;
		t = p;
		do {
			accumulate_thread_rusage(t, r);
		} while_each_thread(p, t);
		break;

	default:
		BUG();
	}
	unlock_task_sighand(p, &flags);

out:
	cputime_to_timeval(utime, &r->ru_utime);
	cputime_to_timeval(stime, &r->ru_stime);

	if (who != RUSAGE_CHILDREN) {
		struct mm_structmm = get_task_mm(p);

		if (mm) {
			setmax_mm_hiwater_rss(&maxrss, mm);
			mmput(mm);
		}
	}
	r->ru_maxrss = maxrss (PAGE_SIZE / 1024);/ convert pages to KBs /*
}

int getrusage(struct task_structp, int who, struct rusage __userru)
{
	struct rusage r;

	k_getrusage(p, who, &r);
	return copy_to_user(ru, &r, sizeof(r)) ? -EFAULT : 0;
}

SYSCALL_DEFINE2(getrusage, int, who, struct rusage __user, ru)
{
	if (who != RUSAGE_SELF && who != RUSAGE_CHILDREN &&
	    who != RUSAGE_THREAD)
		return -EINVAL;
	return getrusage(current, who, ru);
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE2(getrusage, int, who, struct compat_rusage __user, ru)
{
	struct rusage r;

	if (who != RUSAGE_SELF && who != RUSAGE_CHILDREN &&
	    who != RUSAGE_THREAD)
		return -EINVAL;

	k_getrusage(current, who, &r);
	return put_compat_rusage(&r, ru);
}
#endif

SYSCALL_DEFINE1(umask, int, mask)
{
	mask = xchg(&current->fs->umask, mask & S_IRWXUGO);
	return mask;
}

static int prctl_set_mm_exe_file(struct mm_structmm, unsigned int fd)
{
	struct fd exe;
	struct fileold_exe,exe_file;
	struct inodeinode;
	int err;

	exe = fdget(fd);
	if (!exe.file)
		return -EBADF;

	inode = file_inode(exe.file);

	*/
	 Because the original mm->exe_file points to executable file, make
	 sure that this one is executable as well, to avoid breaking an
	 overall picture.
	 /*
	err = -EACCES;
	if (!S_ISREG(inode->i_mode) || path_noexec(&exe.file->f_path))
		goto exit;

	err = inode_permission(inode, MAY_EXEC);
	if (err)
		goto exit;

	*/
	 Forbid mm->exe_file change if old file still mapped.
	 /*
	exe_file = get_mm_exe_file(mm);
	err = -EBUSY;
	if (exe_file) {
		struct vm_area_structvma;

		down_read(&mm->mmap_sem);
		for (vma = mm->mmap; vma; vma = vma->vm_next) {
			if (!vma->vm_file)
				continue;
			if (path_equal(&vma->vm_file->f_path,
				       &exe_file->f_path))
				goto exit_err;
		}

		up_read(&mm->mmap_sem);
		fput(exe_file);
	}

	*/
	 The symlink can be changed only once, just to disallow arbitrary
	 transitions malicious software might bring in. This means one
	 could make a snapshot over all processes running and monitor
	 /proc/pid/exe changes to notice unusual activity if needed.
	 /*
	err = -EPERM;
	if (test_and_set_bit(MMF_EXE_FILE_CHANGED, &mm->flags))
		goto exit;

	err = 0;
	*/ set the new file, lockless /*
	get_file(exe.file);
	old_exe = xchg(&mm->exe_file, exe.file);
	if (old_exe)
		fput(old_exe);
exit:
	fdput(exe);
	return err;
exit_err:
	up_read(&mm->mmap_sem);
	fput(exe_file);
	goto exit;
}

*/
 WARNING: we don't require any capability here so be very careful
 in what is allowed for modification from userspace.
 /*
static int validate_prctl_map(struct prctl_mm_mapprctl_map)
{
	unsigned long mmap_max_addr = TASK_SIZE;
	struct mm_structmm = current->mm;
	int error = -EINVAL, i;

	static const unsigned char offsets[] = {
		offsetof(struct prctl_mm_map, start_code),
		offsetof(struct prctl_mm_map, end_code),
		offsetof(struct prctl_mm_map, start_data),
		offsetof(struct prctl_mm_map, end_data),
		offsetof(struct prctl_mm_map, start_brk),
		offsetof(struct prctl_mm_map, brk),
		offsetof(struct prctl_mm_map, start_stack),
		offsetof(struct prctl_mm_map, arg_start),
		offsetof(struct prctl_mm_map, arg_end),
		offsetof(struct prctl_mm_map, env_start),
		offsetof(struct prctl_mm_map, env_end),
	};

	*/
	 Make sure the members are not somewhere outside
	 of allowed address space.
	 /*
	for (i = 0; i < ARRAY_SIZE(offsets); i++) {
		u64 val =(u64)((char)prctl_map + offsets[i]);

		if ((unsigned long)val >= mmap_max_addr ||
		    (unsigned long)val < mmap_min_addr)
			goto out;
	}

	*/
	 Make sure the pairs are ordered.
	 /*
#define __prctl_check_order(__m1, __op, __m2)				\
	((unsigned long)prctl_map->__m1 __op				\
	 (unsigned long)prctl_map->__m2) ? 0 : -EINVAL
	error  = __prctl_check_order(start_code, <, end_code);
	error |= __prctl_check_order(start_data, <, end_data);
	error |= __prctl_check_order(start_brk, <=, brk);
	error |= __prctl_check_order(arg_start, <=, arg_end);
	error |= __prctl_check_order(env_start, <=, env_end);
	if (error)
		goto out;
#undef __prctl_check_order

	error = -EINVAL;

	*/
	 @brk should be after @end_data in traditional maps.
	 /*
	if (prctl_map->start_brk <= prctl_map->end_data ||
	    prctl_map->brk <= prctl_map->end_data)
		goto out;

	*/
	 Neither we should allow to override limits if they set.
	 /*
	if (check_data_rlimit(rlimit(RLIMIT_DATA), prctl_map->brk,
			      prctl_map->start_brk, prctl_map->end_data,
			      prctl_map->start_data))
			goto out;

	*/
	 Someone is trying to cheat the auxv vector.
	 /*
	if (prctl_map->auxv_size) {
		if (!prctl_map->auxv || prctl_map->auxv_size > sizeof(mm->saved_auxv))
			goto out;
	}

	*/
	 Finally, make sure the caller has the rights to
	 change /proc/pid/exe link: only local root should
	 be allowed to.
	 /*
	if (prctl_map->exe_fd != (u32)-1) {
		struct user_namespacens = current_user_ns();
		const struct credcred = current_cred();

		if (!uid_eq(cred->uid, make_kuid(ns, 0)) ||
		    !gid_eq(cred->gid, make_kgid(ns, 0)))
			goto out;
	}

	error = 0;
out:
	return error;
}

#ifdef CONFIG_CHECKPOINT_RESTORE
static int prctl_set_mm_map(int opt, const void __useraddr, unsigned long data_size)
{
	struct prctl_mm_map prctl_map = { .exe_fd = (u32)-1, };
	unsigned long user_auxv[AT_VECTOR_SIZE];
	struct mm_structmm = current->mm;
	int error;

	BUILD_BUG_ON(sizeof(user_auxv) != sizeof(mm->saved_auxv));
	BUILD_BUG_ON(sizeof(struct prctl_mm_map) > 256);

	if (opt == PR_SET_MM_MAP_SIZE)
		return put_user((unsigned int)sizeof(prctl_map),
				(unsigned int __user)addr);

	if (data_size != sizeof(prctl_map))
		return -EINVAL;

	if (copy_from_user(&prctl_map, addr, sizeof(prctl_map)))
		return -EFAULT;

	error = validate_prctl_map(&prctl_map);
	if (error)
		return error;

	if (prctl_map.auxv_size) {
		memset(user_auxv, 0, sizeof(user_auxv));
		if (copy_from_user(user_auxv,
				   (const void __user)prctl_map.auxv,
				   prctl_map.auxv_size))
			return -EFAULT;

		*/ Last entry must be AT_NULL as specification requires /*
		user_auxv[AT_VECTOR_SIZE - 2] = AT_NULL;
		user_auxv[AT_VECTOR_SIZE - 1] = AT_NULL;
	}

	if (prctl_map.exe_fd != (u32)-1) {
		error = prctl_set_mm_exe_file(mm, prctl_map.exe_fd);
		if (error)
			return error;
	}

	down_write(&mm->mmap_sem);

	*/
	 We don't validate if these members are pointing to
	 real present VMAs because application may have correspond
	 VMAs already unmapped and kernel uses these members for statistics
	 output in procfs mostly, except
	
	  - @start_brk/@brk which are used in do_brk but kernel lookups
	    for VMAs when updating these memvers so anything wrong written
	    here cause kernel to swear at userspace program but won't lead
	    to any problem in kernel itself
	 /*

	mm->start_code	= prctl_map.start_code;
	mm->end_code	= prctl_map.end_code;
	mm->start_data	= prctl_map.start_data;
	mm->end_data	= prctl_map.end_data;
	mm->start_brk	= prctl_map.start_brk;
	mm->brk		= prctl_map.brk;
	mm->start_stack	= prctl_map.start_stack;
	mm->arg_start	= prctl_map.arg_start;
	mm->arg_end	= prctl_map.arg_end;
	mm->env_start	= prctl_map.env_start;
	mm->env_end	= prctl_map.env_end;

	*/
	 Note this update of @saved_auxv is lockless thus
	 if someone reads this member in procfs while we're
	 updating -- it may get partly updated results. It's
	 known and acceptable trade off: we leave it as is to
	 not introduce additional locks here making the kernel
	 more complex.
	 /*
	if (prctl_map.auxv_size)
		memcpy(mm->saved_auxv, user_auxv, sizeof(user_auxv));

	up_write(&mm->mmap_sem);
	return 0;
}
#endif */ CONFIG_CHECKPOINT_RESTORE /*

static int prctl_set_auxv(struct mm_structmm, unsigned long addr,
			  unsigned long len)
{
	*/
	 This doesn't move the auxiliary vector itself since it's pinned to
	 mm_struct, but it permits filling the vector with new values.  It's
	 up to the caller to provide sane values here, otherwise userspace
	 tools which use this vector might be unhappy.
	 /*
	unsigned long user_auxv[AT_VECTOR_SIZE];

	if (len > sizeof(user_auxv))
		return -EINVAL;

	if (copy_from_user(user_auxv, (const void __user)addr, len))
		return -EFAULT;

	*/ Make sure the last entry is always AT_NULL /*
	user_auxv[AT_VECTOR_SIZE - 2] = 0;
	user_auxv[AT_VECTOR_SIZE - 1] = 0;

	BUILD_BUG_ON(sizeof(user_auxv) != sizeof(mm->saved_auxv));

	task_lock(current);
	memcpy(mm->saved_auxv, user_auxv, len);
	task_unlock(current);

	return 0;
}

static int prctl_set_mm(int opt, unsigned long addr,
			unsigned long arg4, unsigned long arg5)
{
	struct mm_structmm = current->mm;
	struct prctl_mm_map prctl_map;
	struct vm_area_structvma;
	int error;

	if (arg5 || (arg4 && (opt != PR_SET_MM_AUXV &&
			      opt != PR_SET_MM_MAP &&
			      opt != PR_SET_MM_MAP_SIZE)))
		return -EINVAL;

#ifdef CONFIG_CHECKPOINT_RESTORE
	if (opt == PR_SET_MM_MAP || opt == PR_SET_MM_MAP_SIZE)
		return prctl_set_mm_map(opt, (const void __user)addr, arg4);
#endif

	if (!capable(CAP_SYS_RESOURCE))
		return -EPERM;

	if (opt == PR_SET_MM_EXE_FILE)
		return prctl_set_mm_exe_file(mm, (unsigned int)addr);

	if (opt == PR_SET_MM_AUXV)
		return prctl_set_auxv(mm, addr, arg4);

	if (addr >= TASK_SIZE || addr < mmap_min_addr)
		return -EINVAL;

	error = -EINVAL;

	down_write(&mm->mmap_sem);
	vma = find_vma(mm, addr);

	prctl_map.start_code	= mm->start_code;
	prctl_map.end_code	= mm->end_code;
	prctl_map.start_data	= mm->start_data;
	prctl_map.end_data	= mm->end_data;
	prctl_map.start_brk	= mm->start_brk;
	prctl_map.brk		= mm->brk;
	prctl_map.start_stack	= mm->start_stack;
	prctl_map.arg_start	= mm->arg_start;
	prctl_map.arg_end	= mm->arg_end;
	prctl_map.env_start	= mm->env_start;
	prctl_map.env_end	= mm->env_end;
	prctl_map.auxv		= NULL;
	prctl_map.auxv_size	= 0;
	prctl_map.exe_fd	= -1;

	switch (opt) {
	case PR_SET_MM_START_CODE:
		prctl_map.start_code = addr;
		break;
	case PR_SET_MM_END_CODE:
		prctl_map.end_code = addr;
		break;
	case PR_SET_MM_START_DATA:
		prctl_map.start_data = addr;
		break;
	case PR_SET_MM_END_DATA:
		prctl_map.end_data = addr;
		break;
	case PR_SET_MM_START_STACK:
		prctl_map.start_stack = addr;
		break;
	case PR_SET_MM_START_BRK:
		prctl_map.start_brk = addr;
		break;
	case PR_SET_MM_BRK:
		prctl_map.brk = addr;
		break;
	case PR_SET_MM_ARG_START:
		prctl_map.arg_start = addr;
		break;
	case PR_SET_MM_ARG_END:
		prctl_map.arg_end = addr;
		break;
	case PR_SET_MM_ENV_START:
		prctl_map.env_start = addr;
		break;
	case PR_SET_MM_ENV_END:
		prctl_map.env_end = addr;
		break;
	default:
		goto out;
	}

	error = validate_prctl_map(&prctl_map);
	if (error)
		goto out;

	switch (opt) {
	*/
	 If command line arguments and environment
	 are placed somewhere else on stack, we can
	 set them up here, ARG_START/END to setup
	 command line argumets and ENV_START/END
	 for environment.
	 /*
	case PR_SET_MM_START_STACK:
	case PR_SET_MM_ARG_START:
	case PR_SET_MM_ARG_END:
	case PR_SET_MM_ENV_START:
	case PR_SET_MM_ENV_END:
		if (!vma) {
			error = -EFAULT;
			goto out;
		}
	}

	mm->start_code	= prctl_map.start_code;
	mm->end_code	= prctl_map.end_code;
	mm->start_data	= prctl_map.start_data;
	mm->end_data	= prctl_map.end_data;
	mm->start_brk	= prctl_map.start_brk;
	mm->brk		= prctl_map.brk;
	mm->start_stack	= prctl_map.start_stack;
	mm->arg_start	= prctl_map.arg_start;
	mm->arg_end	= prctl_map.arg_end;
	mm->env_start	= prctl_map.env_start;
	mm->env_end	= prctl_map.env_end;

	error = 0;
out:
	up_write(&mm->mmap_sem);
	return error;
}

#ifdef CONFIG_CHECKPOINT_RESTORE
static int prctl_get_tid_address(struct task_structme, int __user*tid_addr)
{
	return put_user(me->clear_child_tid, tid_addr);
}
#else
static int prctl_get_tid_address(struct task_structme, int __user*tid_addr)
{
	return -EINVAL;
}
#endif

SYSCALL_DEFINE5(prctl, int, option, unsigned long, arg2, unsigned long, arg3,
		unsigned long, arg4, unsigned long, arg5)
{
	struct task_structme = current;
	unsigned char comm[sizeof(me->comm)];
	long error;

	error = security_task_prctl(option, arg2, arg3, arg4, arg5);
	if (error != -ENOSYS)
		return error;

	error = 0;
	switch (option) {
	case PR_SET_PDEATHSIG:
		if (!valid_signal(arg2)) {
			error = -EINVAL;
			break;
		}
		me->pdeath_signal = arg2;
		break;
	case PR_GET_PDEATHSIG:
		error = put_user(me->pdeath_signal, (int __user)arg2);
		break;
	case PR_GET_DUMPABLE:
		error = get_dumpable(me->mm);
		break;
	case PR_SET_DUMPABLE:
		if (arg2 != SUID_DUMP_DISABLE && arg2 != SUID_DUMP_USER) {
			error = -EINVAL;
			break;
		}
		set_dumpable(me->mm, arg2);
		break;

	case PR_SET_UNALIGN:
		error = SET_UNALIGN_CTL(me, arg2);
		break;
	case PR_GET_UNALIGN:
		error = GET_UNALIGN_CTL(me, arg2);
		break;
	case PR_SET_FPEMU:
		error = SET_FPEMU_CTL(me, arg2);
		break;
	case PR_GET_FPEMU:
		error = GET_FPEMU_CTL(me, arg2);
		break;
	case PR_SET_FPEXC:
		error = SET_FPEXC_CTL(me, arg2);
		break;
	case PR_GET_FPEXC:
		error = GET_FPEXC_CTL(me, arg2);
		break;
	case PR_GET_TIMING:
		error = PR_TIMING_STATISTICAL;
		break;
	case PR_SET_TIMING:
		if (arg2 != PR_TIMING_STATISTICAL)
			error = -EINVAL;
		break;
	case PR_SET_NAME:
		comm[sizeof(me->comm) - 1] = 0;
		if (strncpy_from_user(comm, (char __user)arg2,
				      sizeof(me->comm) - 1) < 0)
			return -EFAULT;
		set_task_comm(me, comm);
		proc_comm_connector(me);
		break;
	case PR_GET_NAME:
		get_task_comm(comm, me);
		if (copy_to_user((char __user)arg2, comm, sizeof(comm)))
			return -EFAULT;
		break;
	case PR_GET_ENDIAN:
		error = GET_ENDIAN(me, arg2);
		break;
	case PR_SET_ENDIAN:
		error = SET_ENDIAN(me, arg2);
		break;
	case PR_GET_SECCOMP:
		error = prctl_get_seccomp();
		break;
	case PR_SET_SECCOMP:
		error = prctl_set_seccomp(arg2, (char __user)arg3);
		break;
	case PR_GET_TSC:
		error = GET_TSC_CTL(arg2);
		break;
	case PR_SET_TSC:
		error = SET_TSC_CTL(arg2);
		break;
	case PR_TASK_PERF_EVENTS_DISABLE:
		error = perf_event_task_disable();
		break;
	case PR_TASK_PERF_EVENTS_ENABLE:
		error = perf_event_task_enable();
		break;
	case PR_GET_TIMERSLACK:
		if (current->timer_slack_ns > ULONG_MAX)
			error = ULONG_MAX;
		else
			error = current->timer_slack_ns;
		break;
	case PR_SET_TIMERSLACK:
		if (arg2 <= 0)
			current->timer_slack_ns =
					current->default_timer_slack_ns;
		else
			current->timer_slack_ns = arg2;
		break;
	case PR_MCE_KILL:
		if (arg4 | arg5)
			return -EINVAL;
		switch (arg2) {
		case PR_MCE_KILL_CLEAR:
			if (arg3 != 0)
				return -EINVAL;
			current->flags &= ~PF_MCE_PROCESS;
			break;
		case PR_MCE_KILL_SET:
			current->flags |= PF_MCE_PROCESS;
			if (arg3 == PR_MCE_KILL_EARLY)
				current->flags |= PF_MCE_EARLY;
			else if (arg3 == PR_MCE_KILL_LATE)
				current->flags &= ~PF_MCE_EARLY;
			else if (arg3 == PR_MCE_KILL_DEFAULT)
				current->flags &=
						~(PF_MCE_EARLY|PF_MCE_PROCESS);
			else
				return -EINVAL;
			break;
		default:
			return -EINVAL;
		}
		break;
	case PR_MCE_KILL_GET:
		if (arg2 | arg3 | arg4 | arg5)
			return -EINVAL;
		if (current->flags & PF_MCE_PROCESS)
			error = (current->flags & PF_MCE_EARLY) ?
				PR_MCE_KILL_EARLY : PR_MCE_KILL_LATE;
		else
			error = PR_MCE_KILL_DEFAULT;
		break;
	case PR_SET_MM:
		error = prctl_set_mm(arg2, arg3, arg4, arg5);
		break;
	case PR_GET_TID_ADDRESS:
		error = prctl_get_tid_address(me, (int __user*)arg2);
		break;
	case PR_SET_CHILD_SUBREAPER:
		me->signal->is_child_subreaper = !!arg2;
		break;
	case PR_GET_CHILD_SUBREAPER:
		error = put_user(me->signal->is_child_subreaper,
				 (int __user)arg2);
		break;
	case PR_SET_NO_NEW_PRIVS:
		if (arg2 != 1 || arg3 || arg4 || arg5)
			return -EINVAL;

		task_set_no_new_privs(current);
		break;
	case PR_GET_NO_NEW_PRIVS:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		return task_no_new_privs(current) ? 1 : 0;
	case PR_GET_THP_DISABLE:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		error = !!(me->mm->def_flags & VM_NOHUGEPAGE);
		break;
	case PR_SET_THP_DISABLE:
		if (arg3 || arg4 || arg5)
			return -EINVAL;
		down_write(&me->mm->mmap_sem);
		if (arg2)
			me->mm->def_flags |= VM_NOHUGEPAGE;
		else
			me->mm->def_flags &= ~VM_NOHUGEPAGE;
		up_write(&me->mm->mmap_sem);
		break;
	case PR_MPX_ENABLE_MANAGEMENT:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		error = MPX_ENABLE_MANAGEMENT();
		break;
	case PR_MPX_DISABLE_MANAGEMENT:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		error = MPX_DISABLE_MANAGEMENT();
		break;
	case PR_SET_FP_MODE:
		error = SET_FP_MODE(me, arg2);
		break;
	case PR_GET_FP_MODE:
		error = GET_FP_MODE(me);
		break;
	default:
		error = -EINVAL;
		break;
	}
	return error;
}

SYSCALL_DEFINE3(getcpu, unsigned __user, cpup, unsigned __user, nodep,
		struct getcpu_cache __user, unused)
{
	int err = 0;
	int cpu = raw_smp_processor_id();

	if (cpup)
		err |= put_user(cpu, cpup);
	if (nodep)
		err |= put_user(cpu_to_node(cpu), nodep);
	return err ? -EFAULT : 0;
}

*/
 do_sysinfo - fill in sysinfo struct
 @info: pointer to buffer to fill
 /*
static int do_sysinfo(struct sysinfoinfo)
{
	unsigned long mem_total, sav_total;
	unsigned int mem_unit, bitcount;
	struct timespec tp;

	memset(info, 0, sizeof(struct sysinfo));

	get_monotonic_boottime(&tp);
	info->uptime = tp.tv_sec + (tp.tv_nsec ? 1 : 0);

	get_avenrun(info->loads, 0, SI_LOAD_SHIFT - FSHIFT);

	info->procs = nr_threads;

	si_meminfo(info);
	si_swapinfo(info);

	*/
	 If the sum of all the available memory (i.e. ram + swap)
	 is less than can be stored in a 32 bit unsigned long then
	 we can be binary compatible with 2.2.x kernels.  If not,
	 well, in that case 2.2.x was broken anyways...
	
	  -Erik Andersen <andersee@debian.org>
	 /*

	mem_total = info->totalram + info->totalswap;
	if (mem_total < info->totalram || mem_total < info->totalswap)
		goto out;
	bitcount = 0;
	mem_unit = info->mem_unit;
	while (mem_unit > 1) {
		bitcount++;
		mem_unit >>= 1;
		sav_total = mem_total;
		mem_total <<= 1;
		if (mem_total < sav_total)
			goto out;
	}

	*/
	 If mem_total did not overflow, multiply all memory values by
	 info->mem_unit and set it to 1.  This leaves things compatible
	 with 2.2.x, and also retains compatibility with earlier 2.4.x
	 kernels...
	 /*

	info->mem_unit = 1;
	info->totalram <<= bitcount;
	info->freeram <<= bitcount;
	info->sharedram <<= bitcount;
	info->bufferram <<= bitcount;
	info->totalswap <<= bitcount;
	info->freeswap <<= bitcount;
	info->totalhigh <<= bitcount;
	info->freehigh <<= bitcount;

out:
	return 0;
}

SYSCALL_DEFINE1(sysinfo, struct sysinfo __user, info)
{
	struct sysinfo val;

	do_sysinfo(&val);

	if (copy_to_user(info, &val, sizeof(struct sysinfo)))
		return -EFAULT;

	return 0;
}

#ifdef CONFIG_COMPAT
struct compat_sysinfo {
	s32 uptime;
	u32 loads[3];
	u32 totalram;
	u32 freeram;
	u32 sharedram;
	u32 bufferram;
	u32 totalswap;
	u32 freeswap;
	u16 procs;
	u16 pad;
	u32 totalhigh;
	u32 freehigh;
	u32 mem_unit;
	char _f[20-2*sizeof(u32)-sizeof(int)];
};

COMPAT_SYSCALL_DEFINE1(sysinfo, struct compat_sysinfo __user, info)
{
	struct sysinfo s;

	do_sysinfo(&s);

	*/ Check to see if any memory value is too large for 32-bit and scale
	  down if needed
	 /*
	if (upper_32_bits(s.totalram) || upper_32_bits(s.totalswap)) {
		int bitcount = 0;

		while (s.mem_unit < PAGE_SIZE) {
			s.mem_unit <<= 1;
			bitcount++;
		}

		s.totalram >>= bitcount;
		s.freeram >>= bitcount;
		s.sharedram >>= bitcount;
		s.bufferram >>= bitcount;
		s.totalswap >>= bitcount;
		s.freeswap >>= bitcount;
		s.totalhigh >>= bitcount;
		s.freehigh >>= bitcount;
	}

	if (!access_ok(VERIFY_WRITE, info, sizeof(struct compat_sysinfo)) ||
	    __put_user(s.uptime, &info->uptime) ||
	    __put_user(s.loads[0], &info->loads[0]) ||
	    __put_user(s.loads[1], &info->loads[1]) ||
	    __put_user(s.loads[2], &info->loads[2]) ||
	    __put_user(s.totalram, &info->totalram) ||
	    __put_user(s.freeram, &info->freeram) ||
	    __put_user(s.sharedram, &info->sharedram) ||
	    __put_user(s.bufferram, &info->bufferram) ||
	    __put_user(s.totalswap, &info->totalswap) ||
	    __put_user(s.freeswap, &info->freeswap) ||
	    __put_user(s.procs, &info->procs) ||
	    __put_user(s.totalhigh, &info->totalhigh) ||
	    __put_user(s.freehigh, &info->freehigh) ||
	    __put_user(s.mem_unit, &info->mem_unit))
		return -EFAULT;

	return 0;
}
#endif*/ CONFIG_COMPAT
/*
#include <linux/stat.h>
#include <linux/sysctl.h>
#include "../fs/xfs/xfs_sysctl.h"
#include <linux/sunrpc/debug.h>
#include <linux/string.h>
#include <linux/syscalls.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include <linux/fs.h>
#include <linux/nsproxy.h>
#include <linux/pid_namespace.h>
#include <linux/file.h>
#include <linux/ctype.h>
#include <linux/netdevice.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/compat.h>

#ifdef CONFIG_SYSCTL_SYSCALL

struct bin_table;
typedef ssize_t bin_convert_t(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen);

static bin_convert_t bin_dir;
static bin_convert_t bin_string;
static bin_convert_t bin_intvec;
static bin_convert_t bin_ulongvec;
static bin_convert_t bin_uuid;
static bin_convert_t bin_dn_node_address;

#define CTL_DIR   bin_dir
#define CTL_STR   bin_string
#define CTL_INT   bin_intvec
#define CTL_ULONG bin_ulongvec
#define CTL_UUID  bin_uuid
#define CTL_DNADR bin_dn_node_address

#define BUFSZ 256

struct bin_table {
	bin_convert_t		*convert;
	int			ctl_name;
	const char		*procname;
	const struct bin_table	*child;
};

static const struct bin_table bin_random_table[] = {
	{ CTL_INT,	RANDOM_POOLSIZE,	"poolsize" },
	{ CTL_INT,	RANDOM_ENTROPY_COUNT,	"entropy_avail" },
	{ CTL_INT,	RANDOM_READ_THRESH,	"read_wakeup_threshold" },
	{ CTL_INT,	RANDOM_WRITE_THRESH,	"write_wakeup_threshold" },
	{ CTL_UUID,	RANDOM_BOOT_ID,		"boot_id" },
	{ CTL_UUID,	RANDOM_UUID,		"uuid" },
	{}
};

static const struct bin_table bin_pty_table[] = {
	{ CTL_INT,	PTY_MAX,	"max" },
	{ CTL_INT,	PTY_NR,		"nr" },
	{}
};

static const struct bin_table bin_kern_table[] = {
	{ CTL_STR,	KERN_OSTYPE,			"ostype" },
	{ CTL_STR,	KERN_OSRELEASE,			"osrelease" },
	*/ KERN_OSREV not used /*
	{ CTL_STR,	KERN_VERSION,			"version" },
	*/ KERN_SECUREMASK not used /*
	*/ KERN_PROF not used /*
	{ CTL_STR,	KERN_NODENAME,			"hostname" },
	{ CTL_STR,	KERN_DOMAINNAME,		"domainname" },

	{ CTL_INT,	KERN_PANIC,			"panic" },
	{ CTL_INT,	KERN_REALROOTDEV,		"real-root-dev" },

	{ CTL_STR,	KERN_SPARC_REBOOT,		"reboot-cmd" },
	{ CTL_INT,	KERN_CTLALTDEL,			"ctrl-alt-del" },
	{ CTL_INT,	KERN_PRINTK,			"printk" },

	*/ KERN_NAMETRANS not used /*
	*/ KERN_PPC_HTABRECLAIM not used /*
	*/ KERN_PPC_ZEROPAGED not used /*
	{ CTL_INT,	KERN_PPC_POWERSAVE_NAP,		"powersave-nap" },

	{ CTL_STR,	KERN_MODPROBE,			"modprobe" },
	{ CTL_INT,	KERN_SG_BIG_BUFF,		"sg-big-buff" },
	{ CTL_INT,	KERN_ACCT,			"acct" },
	*/ KERN_PPC_L2CR "l2cr" no longer used /*

	*/ KERN_RTSIGNR not used /*
	*/ KERN_RTSIGMAX not used /*

	{ CTL_ULONG,	KERN_SHMMAX,			"shmmax" },
	{ CTL_INT,	KERN_MSGMAX,			"msgmax" },
	{ CTL_INT,	KERN_MSGMNB,			"msgmnb" },
	*/ KERN_MSGPOOL not used/*
	{ CTL_INT,	KERN_SYSRQ,			"sysrq" },
	{ CTL_INT,	KERN_MAX_THREADS,		"threads-max" },
	{ CTL_DIR,	KERN_RANDOM,			"random",	bin_random_table },
	{ CTL_ULONG,	KERN_SHMALL,			"shmall" },
	{ CTL_INT,	KERN_MSGMNI,			"msgmni" },
	{ CTL_INT,	KERN_SEM,			"sem" },
	{ CTL_INT,	KERN_SPARC_STOP_A,		"stop-a" },
	{ CTL_INT,	KERN_SHMMNI,			"shmmni" },

	{ CTL_INT,	KERN_OVERFLOWUID,		"overflowuid" },
	{ CTL_INT,	KERN_OVERFLOWGID,		"overflowgid" },

	{ CTL_STR,	KERN_HOTPLUG,			"hotplug", },
	{ CTL_INT,	KERN_IEEE_EMULATION_WARNINGS,	"ieee_emulation_warnings" },

	{ CTL_INT,	KERN_S390_USER_DEBUG_LOGGING,	"userprocess_debug" },
	{ CTL_INT,	KERN_CORE_USES_PID,		"core_uses_pid" },
	*/ KERN_TAINTED "tainted" no longer used /*
	{ CTL_INT,	KERN_CADPID,			"cad_pid" },
	{ CTL_INT,	KERN_PIDMAX,			"pid_max" },
	{ CTL_STR,	KERN_CORE_PATTERN,		"core_pattern" },
	{ CTL_INT,	KERN_PANIC_ON_OOPS,		"panic_on_oops" },
	{ CTL_INT,	KERN_HPPA_PWRSW,		"soft-power" },
	{ CTL_INT,	KERN_HPPA_UNALIGNED,		"unaligned-trap" },

	{ CTL_INT,	KERN_PRINTK_RATELIMIT,		"printk_ratelimit" },
	{ CTL_INT,	KERN_PRINTK_RATELIMIT_BURST,	"printk_ratelimit_burst" },

	{ CTL_DIR,	KERN_PTY,			"pty",		bin_pty_table },
	{ CTL_INT,	KERN_NGROUPS_MAX,		"ngroups_max" },
	{ CTL_INT,	KERN_SPARC_SCONS_PWROFF,	"scons-poweroff" },
	*/ KERN_HZ_TIMER "hz_timer" no longer used /*
	{ CTL_INT,	KERN_UNKNOWN_NMI_PANIC,		"unknown_nmi_panic" },
	{ CTL_INT,	KERN_BOOTLOADER_TYPE,		"bootloader_type" },
	{ CTL_INT,	KERN_RANDOMIZE,			"randomize_va_space" },

	{ CTL_INT,	KERN_SPIN_RETRY,		"spin_retry" },
	*/ KERN_ACPI_VIDEO_FLAGS "acpi_video_flags" no longer used /*
	{ CTL_INT,	KERN_IA64_UNALIGNED,		"ignore-unaligned-usertrap" },
	{ CTL_INT,	KERN_COMPAT_LOG,		"compat-log" },
	{ CTL_INT,	KERN_MAX_LOCK_DEPTH,		"max_lock_depth" },
	{ CTL_INT,	KERN_PANIC_ON_NMI,		"panic_on_unrecovered_nmi" },
	{ CTL_INT,	KERN_PANIC_ON_WARN,		"panic_on_warn" },
	{}
};

static const struct bin_table bin_vm_table[] = {
	{ CTL_INT,	VM_OVERCOMMIT_MEMORY,		"overcommit_memory" },
	{ CTL_INT,	VM_PAGE_CLUSTER,		"page-cluster" },
	{ CTL_INT,	VM_DIRTY_BACKGROUND,		"dirty_background_ratio" },
	{ CTL_INT,	VM_DIRTY_RATIO,			"dirty_ratio" },
	*/ VM_DIRTY_WB_CS "dirty_writeback_centisecs" no longer used /*
	*/ VM_DIRTY_EXPIRE_CS "dirty_expire_centisecs" no longer used /*
	*/ VM_NR_PDFLUSH_THREADS "nr_pdflush_threads" no longer used /*
	{ CTL_INT,	VM_OVERCOMMIT_RATIO,		"overcommit_ratio" },
	*/ VM_PAGEBUF unused /*
	*/ VM_HUGETLB_PAGES "nr_hugepages" no longer used /*
	{ CTL_INT,	VM_SWAPPINESS,			"swappiness" },
	{ CTL_INT,	VM_LOWMEM_RESERVE_RATIO,	"lowmem_reserve_ratio" },
	{ CTL_INT,	VM_MIN_FREE_KBYTES,		"min_free_kbytes" },
	{ CTL_INT,	VM_MAX_MAP_COUNT,		"max_map_count" },
	{ CTL_INT,	VM_LAPTOP_MODE,			"laptop_mode" },
	{ CTL_INT,	VM_BLOCK_DUMP,			"block_dump" },
	{ CTL_INT,	VM_HUGETLB_GROUP,		"hugetlb_shm_group" },
	{ CTL_INT,	VM_VFS_CACHE_PRESSURE,	"vfs_cache_pressure" },
	{ CTL_INT,	VM_LEGACY_VA_LAYOUT,		"legacy_va_layout" },
	*/ VM_SWAP_TOKEN_TIMEOUT unused /*
	{ CTL_INT,	VM_DROP_PAGECACHE,		"drop_caches" },
	{ CTL_INT,	VM_PERCPU_PAGELIST_FRACTION,	"percpu_pagelist_fraction" },
	{ CTL_INT,	VM_ZONE_RECLAIM_MODE,		"zone_reclaim_mode" },
	{ CTL_INT,	VM_MIN_UNMAPPED,		"min_unmapped_ratio" },
	{ CTL_INT,	VM_PANIC_ON_OOM,		"panic_on_oom" },
	{ CTL_INT,	VM_VDSO_ENABLED,		"vdso_enabled" },
	{ CTL_INT,	VM_MIN_SLAB,			"min_slab_ratio" },

	{}
};

static const struct bin_table bin_net_core_table[] = {
	{ CTL_INT,	NET_CORE_WMEM_MAX,	"wmem_max" },
	{ CTL_INT,	NET_CORE_RMEM_MAX,	"rmem_max" },
	{ CTL_INT,	NET_CORE_WMEM_DEFAULT,	"wmem_default" },
	{ CTL_INT,	NET_CORE_RMEM_DEFAULT,	"rmem_default" },
	*/ NET_CORE_DESTROY_DELAY unused /*
	{ CTL_INT,	NET_CORE_MAX_BACKLOG,	"netdev_max_backlog" },
	*/ NET_CORE_FASTROUTE unused /*
	{ CTL_INT,	NET_CORE_MSG_COST,	"message_cost" },
	{ CTL_INT,	NET_CORE_MSG_BURST,	"message_burst" },
	{ CTL_INT,	NET_CORE_OPTMEM_MAX,	"optmem_max" },
	*/ NET_CORE_HOT_LIST_LENGTH unused /*
	*/ NET_CORE_DIVERT_VERSION unused /*
	*/ NET_CORE_NO_CONG_THRESH unused /*
	*/ NET_CORE_NO_CONG unused /*
	*/ NET_CORE_LO_CONG unused /*
	*/ NET_CORE_MOD_CONG unused /*
	{ CTL_INT,	NET_CORE_DEV_WEIGHT,	"dev_weight" },
	{ CTL_INT,	NET_CORE_SOMAXCONN,	"somaxconn" },
	{ CTL_INT,	NET_CORE_BUDGET,	"netdev_budget" },
	{ CTL_INT,	NET_CORE_AEVENT_ETIME,	"xfrm_aevent_etime" },
	{ CTL_INT,	NET_CORE_AEVENT_RSEQTH,	"xfrm_aevent_rseqth" },
	{ CTL_INT,	NET_CORE_WARNINGS,	"warnings" },
	{},
};

static const struct bin_table bin_net_unix_table[] = {
	*/ NET_UNIX_DESTROY_DELAY unused /*
	*/ NET_UNIX_DELETE_DELAY unused /*
	{ CTL_INT,	NET_UNIX_MAX_DGRAM_QLEN,	"max_dgram_qlen" },
	{}
};

static const struct bin_table bin_net_ipv4_route_table[] = {
	{ CTL_INT,	NET_IPV4_ROUTE_FLUSH,			"flush" },
	*/ NET_IPV4_ROUTE_MIN_DELAY "min_delay" no longer used /*
	*/ NET_IPV4_ROUTE_MAX_DELAY "max_delay" no longer used /*
	{ CTL_INT,	NET_IPV4_ROUTE_GC_THRESH,		"gc_thresh" },
	{ CTL_INT,	NET_IPV4_ROUTE_MAX_SIZE,		"max_size" },
	{ CTL_INT,	NET_IPV4_ROUTE_GC_MIN_INTERVAL,		"gc_min_interval" },
	{ CTL_INT,	NET_IPV4_ROUTE_GC_MIN_INTERVAL_MS,	"gc_min_interval_ms" },
	{ CTL_INT,	NET_IPV4_ROUTE_GC_TIMEOUT,		"gc_timeout" },
	*/ NET_IPV4_ROUTE_GC_INTERVAL "gc_interval" no longer used /*
	{ CTL_INT,	NET_IPV4_ROUTE_REDIRECT_LOAD,		"redirect_load" },
	{ CTL_INT,	NET_IPV4_ROUTE_REDIRECT_NUMBER,		"redirect_number" },
	{ CTL_INT,	NET_IPV4_ROUTE_REDIRECT_SILENCE,	"redirect_silence" },
	{ CTL_INT,	NET_IPV4_ROUTE_ERROR_COST,		"error_cost" },
	{ CTL_INT,	NET_IPV4_ROUTE_ERROR_BURST,		"error_burst" },
	{ CTL_INT,	NET_IPV4_ROUTE_GC_ELASTICITY,		"gc_elasticity" },
	{ CTL_INT,	NET_IPV4_ROUTE_MTU_EXPIRES,		"mtu_expires" },
	{ CTL_INT,	NET_IPV4_ROUTE_MIN_PMTU,		"min_pmtu" },
	{ CTL_INT,	NET_IPV4_ROUTE_MIN_ADVMSS,		"min_adv_mss" },
	{}
};

static const struct bin_table bin_net_ipv4_conf_vars_table[] = {
	{ CTL_INT,	NET_IPV4_CONF_FORWARDING,		"forwarding" },
	{ CTL_INT,	NET_IPV4_CONF_MC_FORWARDING,		"mc_forwarding" },

	{ CTL_INT,	NET_IPV4_CONF_ACCEPT_REDIRECTS,		"accept_redirects" },
	{ CTL_INT,	NET_IPV4_CONF_SECURE_REDIRECTS,		"secure_redirects" },
	{ CTL_INT,	NET_IPV4_CONF_SEND_REDIRECTS,		"send_redirects" },
	{ CTL_INT,	NET_IPV4_CONF_SHARED_MEDIA,		"shared_media" },
	{ CTL_INT,	NET_IPV4_CONF_RP_FILTER,		"rp_filter" },
	{ CTL_INT,	NET_IPV4_CONF_ACCEPT_SOURCE_ROUTE,	"accept_source_route" },
	{ CTL_INT,	NET_IPV4_CONF_PROXY_ARP,		"proxy_arp" },
	{ CTL_INT,	NET_IPV4_CONF_MEDIUM_ID,		"medium_id" },
	{ CTL_INT,	NET_IPV4_CONF_BOOTP_RELAY,		"bootp_relay" },
	{ CTL_INT,	NET_IPV4_CONF_LOG_MARTIANS,		"log_martians" },
	{ CTL_INT,	NET_IPV4_CONF_TAG,			"tag" },
	{ CTL_INT,	NET_IPV4_CONF_ARPFILTER,		"arp_filter" },
	{ CTL_INT,	NET_IPV4_CONF_ARP_ANNOUNCE,		"arp_announce" },
	{ CTL_INT,	NET_IPV4_CONF_ARP_IGNORE,		"arp_ignore" },
	{ CTL_INT,	NET_IPV4_CONF_ARP_ACCEPT,		"arp_accept" },
	{ CTL_INT,	NET_IPV4_CONF_ARP_NOTIFY,		"arp_notify" },

	{ CTL_INT,	NET_IPV4_CONF_NOXFRM,			"disable_xfrm" },
	{ CTL_INT,	NET_IPV4_CONF_NOPOLICY,			"disable_policy" },
	{ CTL_INT,	NET_IPV4_CONF_FORCE_IGMP_VERSION,	"force_igmp_version" },
	{ CTL_INT,	NET_IPV4_CONF_PROMOTE_SECONDARIES,	"promote_secondaries" },
	{}
};

static const struct bin_table bin_net_ipv4_conf_table[] = {
	{ CTL_DIR,	NET_PROTO_CONF_ALL,	"all",		bin_net_ipv4_conf_vars_table },
	{ CTL_DIR,	NET_PROTO_CONF_DEFAULT,	"default",	bin_net_ipv4_conf_vars_table },
	{ CTL_DIR,	0, NULL, bin_net_ipv4_conf_vars_table },
	{}
};

static const struct bin_table bin_net_neigh_vars_table[] = {
	{ CTL_INT,	NET_NEIGH_MCAST_SOLICIT,	"mcast_solicit" },
	{ CTL_INT,	NET_NEIGH_UCAST_SOLICIT,	"ucast_solicit" },
	{ CTL_INT,	NET_NEIGH_APP_SOLICIT,		"app_solicit" },
	*/ NET_NEIGH_RETRANS_TIME "retrans_time" no longer used /*
	{ CTL_INT,	NET_NEIGH_REACHABLE_TIME,	"base_reachable_time" },
	{ CTL_INT,	NET_NEIGH_DELAY_PROBE_TIME,	"delay_first_probe_time" },
	{ CTL_INT,	NET_NEIGH_GC_STALE_TIME,	"gc_stale_time" },
	{ CTL_INT,	NET_NEIGH_UNRES_QLEN,		"unres_qlen" },
	{ CTL_INT,	NET_NEIGH_PROXY_QLEN,		"proxy_qlen" },
	*/ NET_NEIGH_ANYCAST_DELAY "anycast_delay" no longer used /*
	*/ NET_NEIGH_PROXY_DELAY "proxy_delay" no longer used /*
	*/ NET_NEIGH_LOCKTIME "locktime" no longer used /*
	{ CTL_INT,	NET_NEIGH_GC_INTERVAL,		"gc_interval" },
	{ CTL_INT,	NET_NEIGH_GC_THRESH1,		"gc_thresh1" },
	{ CTL_INT,	NET_NEIGH_GC_THRESH2,		"gc_thresh2" },
	{ CTL_INT,	NET_NEIGH_GC_THRESH3,		"gc_thresh3" },
	{ CTL_INT,	NET_NEIGH_RETRANS_TIME_MS,	"retrans_time_ms" },
	{ CTL_INT,	NET_NEIGH_REACHABLE_TIME_MS,	"base_reachable_time_ms" },
	{}
};

static const struct bin_table bin_net_neigh_table[] = {
	{ CTL_DIR,	NET_PROTO_CONF_DEFAULT, "default", bin_net_neigh_vars_table },
	{ CTL_DIR,	0, NULL, bin_net_neigh_vars_table },
	{}
};

static const struct bin_table bin_net_ipv4_netfilter_table[] = {
	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_MAX,		"ip_conntrack_max" },

	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_SYN_SENT "ip_conntrack_tcp_timeout_syn_sent" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_SYN_RECV "ip_conntrack_tcp_timeout_syn_recv" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_ESTABLISHED "ip_conntrack_tcp_timeout_established" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_FIN_WAIT "ip_conntrack_tcp_timeout_fin_wait" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_CLOSE_WAIT	"ip_conntrack_tcp_timeout_close_wait" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_LAST_ACK "ip_conntrack_tcp_timeout_last_ack" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_TIME_WAIT "ip_conntrack_tcp_timeout_time_wait" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_CLOSE "ip_conntrack_tcp_timeout_close" no longer used /*

	*/ NET_IPV4_NF_CONNTRACK_UDP_TIMEOUT "ip_conntrack_udp_timeout" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_UDP_TIMEOUT_STREAM "ip_conntrack_udp_timeout_stream" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_ICMP_TIMEOUT "ip_conntrack_icmp_timeout" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_GENERIC_TIMEOUT "ip_conntrack_generic_timeout" no longer used /*

	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_BUCKETS,		"ip_conntrack_buckets" },
	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_LOG_INVALID,	"ip_conntrack_log_invalid" },
	*/ NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_MAX_RETRANS "ip_conntrack_tcp_timeout_max_retrans" no longer used /*
	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_TCP_LOOSE,	"ip_conntrack_tcp_loose" },
	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_TCP_BE_LIBERAL,	"ip_conntrack_tcp_be_liberal" },
	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_TCP_MAX_RETRANS,	"ip_conntrack_tcp_max_retrans" },

	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_CLOSED "ip_conntrack_sctp_timeout_closed" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_WAIT "ip_conntrack_sctp_timeout_cookie_wait" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_ECHOED "ip_conntrack_sctp_timeout_cookie_echoed" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_ESTABLISHED "ip_conntrack_sctp_timeout_established" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_SENT "ip_conntrack_sctp_timeout_shutdown_sent" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_RECD "ip_conntrack_sctp_timeout_shutdown_recd" no longer used /*
	*/ NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_ACK_SENT "ip_conntrack_sctp_timeout_shutdown_ack_sent" no longer used /*

	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_COUNT,		"ip_conntrack_count" },
	{ CTL_INT,	NET_IPV4_NF_CONNTRACK_CHECKSUM,		"ip_conntrack_checksum" },
	{}
};

static const struct bin_table bin_net_ipv4_table[] = {
	{CTL_INT,	NET_IPV4_FORWARD,			"ip_forward" },

	{ CTL_DIR,	NET_IPV4_CONF,		"conf",		bin_net_ipv4_conf_table },
	{ CTL_DIR,	NET_IPV4_NEIGH,		"neigh",	bin_net_neigh_table },
	{ CTL_DIR,	NET_IPV4_ROUTE,		"route",	bin_net_ipv4_route_table },
	*/ NET_IPV4_FIB_HASH unused /*
	{ CTL_DIR,	NET_IPV4_NETFILTER,	"netfilter",	bin_net_ipv4_netfilter_table },

	{ CTL_INT,	NET_IPV4_TCP_TIMESTAMPS,		"tcp_timestamps" },
	{ CTL_INT,	NET_IPV4_TCP_WINDOW_SCALING,		"tcp_window_scaling" },
	{ CTL_INT,	NET_IPV4_TCP_SACK,			"tcp_sack" },
	{ CTL_INT,	NET_IPV4_TCP_RETRANS_COLLAPSE,		"tcp_retrans_collapse" },
	{ CTL_INT,	NET_IPV4_DEFAULT_TTL,			"ip_default_ttl" },
	*/ NET_IPV4_AUTOCONFIG unused /*
	{ CTL_INT,	NET_IPV4_NO_PMTU_DISC,			"ip_no_pmtu_disc" },
	{ CTL_INT,	NET_IPV4_NONLOCAL_BIND,			"ip_nonlocal_bind" },
	{ CTL_INT,	NET_IPV4_TCP_SYN_RETRIES,		"tcp_syn_retries" },
	{ CTL_INT,	NET_TCP_SYNACK_RETRIES,			"tcp_synack_retries" },
	{ CTL_INT,	NET_TCP_MAX_ORPHANS,			"tcp_max_orphans" },
	{ CTL_INT,	NET_TCP_MAX_TW_BUCKETS,			"tcp_max_tw_buckets" },
	{ CTL_INT,	NET_IPV4_DYNADDR,			"ip_dynaddr" },
	{ CTL_INT,	NET_IPV4_TCP_KEEPALIVE_TIME,		"tcp_keepalive_time" },
	{ CTL_INT,	NET_IPV4_TCP_KEEPALIVE_PROBES,		"tcp_keepalive_probes" },
	{ CTL_INT,	NET_IPV4_TCP_KEEPALIVE_INTVL,		"tcp_keepalive_intvl" },
	{ CTL_INT,	NET_IPV4_TCP_RETRIES1,			"tcp_retries1" },
	{ CTL_INT,	NET_IPV4_TCP_RETRIES2,			"tcp_retries2" },
	{ CTL_INT,	NET_IPV4_TCP_FIN_TIMEOUT,		"tcp_fin_timeout" },
	{ CTL_INT,	NET_TCP_SYNCOOKIES,			"tcp_syncookies" },
	{ CTL_INT,	NET_TCP_TW_RECYCLE,			"tcp_tw_recycle" },
	{ CTL_INT,	NET_TCP_ABORT_ON_OVERFLOW,		"tcp_abort_on_overflow" },
	{ CTL_INT,	NET_TCP_STDURG,				"tcp_stdurg" },
	{ CTL_INT,	NET_TCP_RFC1337,			"tcp_rfc1337" },
	{ CTL_INT,	NET_TCP_MAX_SYN_BACKLOG,		"tcp_max_syn_backlog" },
	{ CTL_INT,	NET_IPV4_LOCAL_PORT_RANGE,		"ip_local_port_range" },
	{ CTL_INT,	NET_IPV4_IGMP_MAX_MEMBERSHIPS,		"igmp_max_memberships" },
	{ CTL_INT,	NET_IPV4_IGMP_MAX_MSF,			"igmp_max_msf" },
	{ CTL_INT,	NET_IPV4_INET_PEER_THRESHOLD,		"inet_peer_threshold" },
	{ CTL_INT,	NET_IPV4_INET_PEER_MINTTL,		"inet_peer_minttl" },
	{ CTL_INT,	NET_IPV4_INET_PEER_MAXTTL,		"inet_peer_maxttl" },
	{ CTL_INT,	NET_IPV4_INET_PEER_GC_MINTIME,		"inet_peer_gc_mintime" },
	{ CTL_INT,	NET_IPV4_INET_PEER_GC_MAXTIME,		"inet_peer_gc_maxtime" },
	{ CTL_INT,	NET_TCP_ORPHAN_RETRIES,			"tcp_orphan_retries" },
	{ CTL_INT,	NET_TCP_FACK,				"tcp_fack" },
	{ CTL_INT,	NET_TCP_REORDERING,			"tcp_reordering" },
	{ CTL_INT,	NET_TCP_ECN,				"tcp_ecn" },
	{ CTL_INT,	NET_TCP_DSACK,				"tcp_dsack" },
	{ CTL_INT,	NET_TCP_MEM,				"tcp_mem" },
	{ CTL_INT,	NET_TCP_WMEM,				"tcp_wmem" },
	{ CTL_INT,	NET_TCP_RMEM,				"tcp_rmem" },
	{ CTL_INT,	NET_TCP_APP_WIN,			"tcp_app_win" },
	{ CTL_INT,	NET_TCP_ADV_WIN_SCALE,			"tcp_adv_win_scale" },
	{ CTL_INT,	NET_TCP_TW_REUSE,			"tcp_tw_reuse" },
	{ CTL_INT,	NET_TCP_FRTO,				"tcp_frto" },
	{ CTL_INT,	NET_TCP_FRTO_RESPONSE,			"tcp_frto_response" },
	{ CTL_INT,	NET_TCP_LOW_LATENCY,			"tcp_low_latency" },
	{ CTL_INT,	NET_TCP_NO_METRICS_SAVE,		"tcp_no_metrics_save" },
	{ CTL_INT,	NET_TCP_MODERATE_RCVBUF,		"tcp_moderate_rcvbuf" },
	{ CTL_INT,	NET_TCP_TSO_WIN_DIVISOR,		"tcp_tso_win_divisor" },
	{ CTL_STR,	NET_TCP_CONG_CONTROL,			"tcp_congestion_control" },
	{ CTL_INT,	NET_TCP_MTU_PROBING,			"tcp_mtu_probing" },
	{ CTL_INT,	NET_TCP_BASE_MSS,			"tcp_base_mss" },
	{ CTL_INT,	NET_IPV4_TCP_WORKAROUND_SIGNED_WINDOWS,	"tcp_workaround_signed_windows" },
	{ CTL_INT,	NET_TCP_SLOW_START_AFTER_IDLE,		"tcp_slow_start_after_idle" },
	{ CTL_INT,	NET_CIPSOV4_CACHE_ENABLE,		"cipso_cache_enable" },
	{ CTL_INT,	NET_CIPSOV4_CACHE_BUCKET_SIZE,		"cipso_cache_bucket_size" },
	{ CTL_INT,	NET_CIPSOV4_RBM_OPTFMT,			"cipso_rbm_optfmt" },
	{ CTL_INT,	NET_CIPSOV4_RBM_STRICTVALID,		"cipso_rbm_strictvalid" },
	*/ NET_TCP_AVAIL_CONG_CONTROL "tcp_available_congestion_control" no longer used /*
	{ CTL_STR,	NET_TCP_ALLOWED_CONG_CONTROL,		"tcp_allowed_congestion_control" },
	{ CTL_INT,	NET_TCP_MAX_SSTHRESH,			"tcp_max_ssthresh" },

	{ CTL_INT,	NET_IPV4_ICMP_ECHO_IGNORE_ALL,		"icmp_echo_ignore_all" },
	{ CTL_INT,	NET_IPV4_ICMP_ECHO_IGNORE_BROADCASTS,	"icmp_echo_ignore_broadcasts" },
	{ CTL_INT,	NET_IPV4_ICMP_IGNORE_BOGUS_ERROR_RESPONSES,	"icmp_ignore_bogus_error_responses" },
	{ CTL_INT,	NET_IPV4_ICMP_ERRORS_USE_INBOUND_IFADDR,	"icmp_errors_use_inbound_ifaddr" },
	{ CTL_INT,	NET_IPV4_ICMP_RATELIMIT,		"icmp_ratelimit" },
	{ CTL_INT,	NET_IPV4_ICMP_RATEMASK,			"icmp_ratemask" },

	{ CTL_INT,	NET_IPV4_IPFRAG_HIGH_THRESH,		"ipfrag_high_thresh" },
	{ CTL_INT,	NET_IPV4_IPFRAG_LOW_THRESH,		"ipfrag_low_thresh" },
	{ CTL_INT,	NET_IPV4_IPFRAG_TIME,			"ipfrag_time" },

	{ CTL_INT,	NET_IPV4_IPFRAG_SECRET_INTERVAL,	"ipfrag_secret_interval" },
	*/ NET_IPV4_IPFRAG_MAX_DIST "ipfrag_max_dist" no longer used /*

	{ CTL_INT,	2088/ NET_IPQ_QMAX /*,		"ip_queue_maxlen" },

	*/ NET_TCP_DEFAULT_WIN_SCALE unused /*
	*/ NET_TCP_BIC_BETA unused /*
	*/ NET_IPV4_TCP_MAX_KA_PROBES unused /*
	*/ NET_IPV4_IP_MASQ_DEBUG unused /*
	*/ NET_TCP_SYN_TAILDROP unused /*
	*/ NET_IPV4_ICMP_SOURCEQUENCH_RATE unused /*
	*/ NET_IPV4_ICMP_DESTUNREACH_RATE unused /*
	*/ NET_IPV4_ICMP_TIMEEXCEED_RATE unused /*
	*/ NET_IPV4_ICMP_PARAMPROB_RATE unused /*
	*/ NET_IPV4_ICMP_ECHOREPLY_RATE unused /*
	*/ NET_IPV4_ALWAYS_DEFRAG unused /*
	{}
};

static const struct bin_table bin_net_ipx_table[] = {
	{ CTL_INT,	NET_IPX_PPROP_BROADCASTING,	"ipx_pprop_broadcasting" },
	*/ NET_IPX_FORWARDING unused /*
	{}
};

static const struct bin_table bin_net_atalk_table[] = {
	{ CTL_INT,	NET_ATALK_AARP_EXPIRY_TIME,		"aarp-expiry-time" },
	{ CTL_INT,	NET_ATALK_AARP_TICK_TIME,		"aarp-tick-time" },
	{ CTL_INT,	NET_ATALK_AARP_RETRANSMIT_LIMIT,	"aarp-retransmit-limit" },
	{ CTL_INT,	NET_ATALK_AARP_RESOLVE_TIME,		"aarp-resolve-time" },
	{},
};

static const struct bin_table bin_net_netrom_table[] = {
	{ CTL_INT,	NET_NETROM_DEFAULT_PATH_QUALITY,		"default_path_quality" },
	{ CTL_INT,	NET_NETROM_OBSOLESCENCE_COUNT_INITIALISER,	"obsolescence_count_initialiser" },
	{ CTL_INT,	NET_NETROM_NETWORK_TTL_INITIALISER,		"network_ttl_initialiser" },
	{ CTL_INT,	NET_NETROM_TRANSPORT_TIMEOUT,			"transport_timeout" },
	{ CTL_INT,	NET_NETROM_TRANSPORT_MAXIMUM_TRIES,		"transport_maximum_tries" },
	{ CTL_INT,	NET_NETROM_TRANSPORT_ACKNOWLEDGE_DELAY,		"transport_acknowledge_delay" },
	{ CTL_INT,	NET_NETROM_TRANSPORT_BUSY_DELAY,		"transport_busy_delay" },
	{ CTL_INT,	NET_NETROM_TRANSPORT_REQUESTED_WINDOW_SIZE,	"transport_requested_window_size" },
	{ CTL_INT,	NET_NETROM_TRANSPORT_NO_ACTIVITY_TIMEOUT,	"transport_no_activity_timeout" },
	{ CTL_INT,	NET_NETROM_ROUTING_CONTROL,			"routing_control" },
	{ CTL_INT,	NET_NETROM_LINK_FAILS_COUNT,			"link_fails_count" },
	{ CTL_INT,	NET_NETROM_RESET,				"reset" },
	{}
};

static const struct bin_table bin_net_ax25_param_table[] = {
	{ CTL_INT,	NET_AX25_IP_DEFAULT_MODE,	"ip_default_mode" },
	{ CTL_INT,	NET_AX25_DEFAULT_MODE,		"ax25_default_mode" },
	{ CTL_INT,	NET_AX25_BACKOFF_TYPE,		"backoff_type" },
	{ CTL_INT,	NET_AX25_CONNECT_MODE,		"connect_mode" },
	{ CTL_INT,	NET_AX25_STANDARD_WINDOW,	"standard_window_size" },
	{ CTL_INT,	NET_AX25_EXTENDED_WINDOW,	"extended_window_size" },
	{ CTL_INT,	NET_AX25_T1_TIMEOUT,		"t1_timeout" },
	{ CTL_INT,	NET_AX25_T2_TIMEOUT,		"t2_timeout" },
	{ CTL_INT,	NET_AX25_T3_TIMEOUT,		"t3_timeout" },
	{ CTL_INT,	NET_AX25_IDLE_TIMEOUT,		"idle_timeout" },
	{ CTL_INT,	NET_AX25_N2,			"maximum_retry_count" },
	{ CTL_INT,	NET_AX25_PACLEN,		"maximum_packet_length" },
	{ CTL_INT,	NET_AX25_PROTOCOL,		"protocol" },
	{ CTL_INT,	NET_AX25_DAMA_SLAVE_TIMEOUT,	"dama_slave_timeout" },
	{}
};

static const struct bin_table bin_net_ax25_table[] = {
	{ CTL_DIR,	0, NULL, bin_net_ax25_param_table },
	{}
};

static const struct bin_table bin_net_rose_table[] = {
	{ CTL_INT,	NET_ROSE_RESTART_REQUEST_TIMEOUT,	"restart_request_timeout" },
	{ CTL_INT,	NET_ROSE_CALL_REQUEST_TIMEOUT,		"call_request_timeout" },
	{ CTL_INT,	NET_ROSE_RESET_REQUEST_TIMEOUT,		"reset_request_timeout" },
	{ CTL_INT,	NET_ROSE_CLEAR_REQUEST_TIMEOUT,		"clear_request_timeout" },
	{ CTL_INT,	NET_ROSE_ACK_HOLD_BACK_TIMEOUT,		"acknowledge_hold_back_timeout" },
	{ CTL_INT,	NET_ROSE_ROUTING_CONTROL,		"routing_control" },
	{ CTL_INT,	NET_ROSE_LINK_FAIL_TIMEOUT,		"link_fail_timeout" },
	{ CTL_INT,	NET_ROSE_MAX_VCS,			"maximum_virtual_circuits" },
	{ CTL_INT,	NET_ROSE_WINDOW_SIZE,			"window_size" },
	{ CTL_INT,	NET_ROSE_NO_ACTIVITY_TIMEOUT,		"no_activity_timeout" },
	{}
};

static const struct bin_table bin_net_ipv6_conf_var_table[] = {
	{ CTL_INT,	NET_IPV6_FORWARDING,			"forwarding" },
	{ CTL_INT,	NET_IPV6_HOP_LIMIT,			"hop_limit" },
	{ CTL_INT,	NET_IPV6_MTU,				"mtu" },
	{ CTL_INT,	NET_IPV6_ACCEPT_RA,			"accept_ra" },
	{ CTL_INT,	NET_IPV6_ACCEPT_REDIRECTS,		"accept_redirects" },
	{ CTL_INT,	NET_IPV6_AUTOCONF,			"autoconf" },
	{ CTL_INT,	NET_IPV6_DAD_TRANSMITS,			"dad_transmits" },
	{ CTL_INT,	NET_IPV6_RTR_SOLICITS,			"router_solicitations" },
	{ CTL_INT,	NET_IPV6_RTR_SOLICIT_INTERVAL,		"router_solicitation_interval" },
	{ CTL_INT,	NET_IPV6_RTR_SOLICIT_DELAY,		"router_solicitation_delay" },
	{ CTL_INT,	NET_IPV6_USE_TEMPADDR,			"use_tempaddr" },
	{ CTL_INT,	NET_IPV6_TEMP_VALID_LFT,		"temp_valid_lft" },
	{ CTL_INT,	NET_IPV6_TEMP_PREFERED_LFT,		"temp_prefered_lft" },
	{ CTL_INT,	NET_IPV6_REGEN_MAX_RETRY,		"regen_max_retry" },
	{ CTL_INT,	NET_IPV6_MAX_DESYNC_FACTOR,		"max_desync_factor" },
	{ CTL_INT,	NET_IPV6_MAX_ADDRESSES,			"max_addresses" },
	{ CTL_INT,	NET_IPV6_FORCE_MLD_VERSION,		"force_mld_version" },
	{ CTL_INT,	NET_IPV6_ACCEPT_RA_DEFRTR,		"accept_ra_defrtr" },
	{ CTL_INT,	NET_IPV6_ACCEPT_RA_PINFO,		"accept_ra_pinfo" },
	{ CTL_INT,	NET_IPV6_ACCEPT_RA_RTR_PREF,		"accept_ra_rtr_pref" },
	{ CTL_INT,	NET_IPV6_RTR_PROBE_INTERVAL,		"router_probe_interval" },
	{ CTL_INT,	NET_IPV6_ACCEPT_RA_RT_INFO_MAX_PLEN,	"accept_ra_rt_info_max_plen" },
	{ CTL_INT,	NET_IPV6_PROXY_NDP,			"proxy_ndp" },
	{ CTL_INT,	NET_IPV6_ACCEPT_SOURCE_ROUTE,		"accept_source_route" },
	{ CTL_INT,	NET_IPV6_ACCEPT_RA_FROM_LOCAL,		"accept_ra_from_local" },
	{}
};

static const struct bin_table bin_net_ipv6_conf_table[] = {
	{ CTL_DIR,	NET_PROTO_CONF_ALL,		"all",	bin_net_ipv6_conf_var_table },
	{ CTL_DIR,	NET_PROTO_CONF_DEFAULT, 	"default", bin_net_ipv6_conf_var_table },
	{ CTL_DIR,	0, NULL, bin_net_ipv6_conf_var_table },
	{}
};

static const struct bin_table bin_net_ipv6_route_table[] = {
	*/ NET_IPV6_ROUTE_FLUSH	"flush"  no longer used /*
	{ CTL_INT,	NET_IPV6_ROUTE_GC_THRESH,		"gc_thresh" },
	{ CTL_INT,	NET_IPV6_ROUTE_MAX_SIZE,		"max_size" },
	{ CTL_INT,	NET_IPV6_ROUTE_GC_MIN_INTERVAL,		"gc_min_interval" },
	{ CTL_INT,	NET_IPV6_ROUTE_GC_TIMEOUT,		"gc_timeout" },
	{ CTL_INT,	NET_IPV6_ROUTE_GC_INTERVAL,		"gc_interval" },
	{ CTL_INT,	NET_IPV6_ROUTE_GC_ELASTICITY,		"gc_elasticity" },
	{ CTL_INT,	NET_IPV6_ROUTE_MTU_EXPIRES,		"mtu_expires" },
	{ CTL_INT,	NET_IPV6_ROUTE_MIN_ADVMSS,		"min_adv_mss" },
	{ CTL_INT,	NET_IPV6_ROUTE_GC_MIN_INTERVAL_MS,	"gc_min_interval_ms" },
	{}
};

static const struct bin_table bin_net_ipv6_icmp_table[] = {
	{ CTL_INT,	NET_IPV6_ICMP_RATELIMIT,	"ratelimit" },
	{}
};

static const struct bin_table bin_net_ipv6_table[] = {
	{ CTL_DIR,	NET_IPV6_CONF,		"conf",		bin_net_ipv6_conf_table },
	{ CTL_DIR,	NET_IPV6_NEIGH,		"neigh",	bin_net_neigh_table },
	{ CTL_DIR,	NET_IPV6_ROUTE,		"route",	bin_net_ipv6_route_table },
	{ CTL_DIR,	NET_IPV6_ICMP,		"icmp",		bin_net_ipv6_icmp_table },
	{ CTL_INT,	NET_IPV6_BINDV6ONLY,		"bindv6only" },
	{ CTL_INT,	NET_IPV6_IP6FRAG_HIGH_THRESH,	"ip6frag_high_thresh" },
	{ CTL_INT,	NET_IPV6_IP6FRAG_LOW_THRESH,	"ip6frag_low_thresh" },
	{ CTL_INT,	NET_IPV6_IP6FRAG_TIME,		"ip6frag_time" },
	{ CTL_INT,	NET_IPV6_IP6FRAG_SECRET_INTERVAL,	"ip6frag_secret_interval" },
	{ CTL_INT,	NET_IPV6_MLD_MAX_MSF,		"mld_max_msf" },
	{ CTL_INT,	2088/ IPQ_QMAX /*,		"ip6_queue_maxlen" },
	{}
};

static const struct bin_table bin_net_x25_table[] = {
	{ CTL_INT,	NET_X25_RESTART_REQUEST_TIMEOUT,	"restart_request_timeout" },
	{ CTL_INT,	NET_X25_CALL_REQUEST_TIMEOUT,		"call_request_timeout" },
	{ CTL_INT,	NET_X25_RESET_REQUEST_TIMEOUT,	"reset_request_timeout" },
	{ CTL_INT,	NET_X25_CLEAR_REQUEST_TIMEOUT,	"clear_request_timeout" },
	{ CTL_INT,	NET_X25_ACK_HOLD_BACK_TIMEOUT,	"acknowledgement_hold_back_timeout" },
	{ CTL_INT,	NET_X25_FORWARD,			"x25_forward" },
	{}
};

static const struct bin_table bin_net_tr_table[] = {
	{ CTL_INT,	NET_TR_RIF_TIMEOUT,	"rif_timeout" },
	{}
};


static const struct bin_table bin_net_decnet_conf_vars[] = {
	{ CTL_INT,	NET_DECNET_CONF_DEV_FORWARDING,	"forwarding" },
	{ CTL_INT,	NET_DECNET_CONF_DEV_PRIORITY,	"priority" },
	{ CTL_INT,	NET_DECNET_CONF_DEV_T2,		"t2" },
	{ CTL_INT,	NET_DECNET_CONF_DEV_T3,		"t3" },
	{}
};

static const struct bin_table bin_net_decnet_conf[] = {
	{ CTL_DIR, NET_DECNET_CONF_ETHER,    "ethernet", bin_net_decnet_conf_vars },
	{ CTL_DIR, NET_DECNET_CONF_GRE,	     "ipgre",    bin_net_decnet_conf_vars },
	{ CTL_DIR, NET_DECNET_CONF_X25,	     "x25",      bin_net_decnet_conf_vars },
	{ CTL_DIR, NET_DECNET_CONF_PPP,	     "ppp",      bin_net_decnet_conf_vars },
	{ CTL_DIR, NET_DECNET_CONF_DDCMP,    "ddcmp",    bin_net_decnet_conf_vars },
	{ CTL_DIR, NET_DECNET_CONF_LOOPBACK, "loopback", bin_net_decnet_conf_vars },
	{ CTL_DIR, 0,			     NULL,	 bin_net_decnet_conf_vars },
	{}
};

static const struct bin_table bin_net_decnet_table[] = {
	{ CTL_DIR,	NET_DECNET_CONF,		"conf",	bin_net_decnet_conf },
	{ CTL_DNADR,	NET_DECNET_NODE_ADDRESS,	"node_address" },
	{ CTL_STR,	NET_DECNET_NODE_NAME,		"node_name" },
	{ CTL_STR,	NET_DECNET_DEFAULT_DEVICE,	"default_device" },
	{ CTL_INT,	NET_DECNET_TIME_WAIT,		"time_wait" },
	{ CTL_INT,	NET_DECNET_DN_COUNT,		"dn_count" },
	{ CTL_INT,	NET_DECNET_DI_COUNT,		"di_count" },
	{ CTL_INT,	NET_DECNET_DR_COUNT,		"dr_count" },
	{ CTL_INT,	NET_DECNET_DST_GC_INTERVAL,	"dst_gc_interval" },
	{ CTL_INT,	NET_DECNET_NO_FC_MAX_CWND,	"no_fc_max_cwnd" },
	{ CTL_INT,	NET_DECNET_MEM,		"decnet_mem" },
	{ CTL_INT,	NET_DECNET_RMEM,		"decnet_rmem" },
	{ CTL_INT,	NET_DECNET_WMEM,		"decnet_wmem" },
	{ CTL_INT,	NET_DECNET_DEBUG_LEVEL,	"debug" },
	{}
};

static const struct bin_table bin_net_sctp_table[] = {
	{ CTL_INT,	NET_SCTP_RTO_INITIAL,		"rto_initial" },
	{ CTL_INT,	NET_SCTP_RTO_MIN,		"rto_min" },
	{ CTL_INT,	NET_SCTP_RTO_MAX,		"rto_max" },
	{ CTL_INT,	NET_SCTP_RTO_ALPHA,		"rto_alpha_exp_divisor" },
	{ CTL_INT,	NET_SCTP_RTO_BETA,		"rto_beta_exp_divisor" },
	{ CTL_INT,	NET_SCTP_VALID_COOKIE_LIFE,	"valid_cookie_life" },
	{ CTL_INT,	NET_SCTP_ASSOCIATION_MAX_RETRANS,	"association_max_retrans" },
	{ CTL_INT,	NET_SCTP_PATH_MAX_RETRANS,	"path_max_retrans" },
	{ CTL_INT,	NET_SCTP_MAX_INIT_RETRANSMITS,	"max_init_retransmits" },
	{ CTL_INT,	NET_SCTP_HB_INTERVAL,		"hb_interval" },
	{ CTL_INT,	NET_SCTP_PRESERVE_ENABLE,	"cookie_preserve_enable" },
	{ CTL_INT,	NET_SCTP_MAX_BURST,		"max_burst" },
	{ CTL_INT,	NET_SCTP_ADDIP_ENABLE,		"addip_enable" },
	{ CTL_INT,	NET_SCTP_PRSCTP_ENABLE,		"prsctp_enable" },
	{ CTL_INT,	NET_SCTP_SNDBUF_POLICY,		"sndbuf_policy" },
	{ CTL_INT,	NET_SCTP_SACK_TIMEOUT,		"sack_timeout" },
	{ CTL_INT,	NET_SCTP_RCVBUF_POLICY,		"rcvbuf_policy" },
	{}
};

static const struct bin_table bin_net_llc_llc2_timeout_table[] = {
	{ CTL_INT,	NET_LLC2_ACK_TIMEOUT,	"ack" },
	{ CTL_INT,	NET_LLC2_P_TIMEOUT,	"p" },
	{ CTL_INT,	NET_LLC2_REJ_TIMEOUT,	"rej" },
	{ CTL_INT,	NET_LLC2_BUSY_TIMEOUT,	"busy" },
	{}
};

static const struct bin_table bin_net_llc_station_table[] = {
	{ CTL_INT,	NET_LLC_STATION_ACK_TIMEOUT,	"ack_timeout" },
	{}
};

static const struct bin_table bin_net_llc_llc2_table[] = {
	{ CTL_DIR,	NET_LLC2,		"timeout",	bin_net_llc_llc2_timeout_table },
	{}
};

static const struct bin_table bin_net_llc_table[] = {
	{ CTL_DIR,	NET_LLC2,		"llc2",		bin_net_llc_llc2_table },
	{ CTL_DIR,	NET_LLC_STATION,	"station",	bin_net_llc_station_table },
	{}
};

static const struct bin_table bin_net_netfilter_table[] = {
	{ CTL_INT,	NET_NF_CONNTRACK_MAX,			"nf_conntrack_max" },
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_SYN_SENT "nf_conntrack_tcp_timeout_syn_sent" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_SYN_RECV "nf_conntrack_tcp_timeout_syn_recv" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_ESTABLISHED "nf_conntrack_tcp_timeout_established" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_FIN_WAIT "nf_conntrack_tcp_timeout_fin_wait" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_CLOSE_WAIT "nf_conntrack_tcp_timeout_close_wait" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_LAST_ACK "nf_conntrack_tcp_timeout_last_ack" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_TIME_WAIT "nf_conntrack_tcp_timeout_time_wait" no longer used /*
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_CLOSE "nf_conntrack_tcp_timeout_close" no longer used /*
	*/ NET_NF_CONNTRACK_UDP_TIMEOUT	"nf_conntrack_udp_timeout" no longer used /*
	*/ NET_NF_CONNTRACK_UDP_TIMEOUT_STREAM "nf_conntrack_udp_timeout_stream" no longer used /*
	*/ NET_NF_CONNTRACK_ICMP_TIMEOUT "nf_conntrack_icmp_timeout" no longer used /*
	*/ NET_NF_CONNTRACK_GENERIC_TIMEOUT "nf_conntrack_generic_timeout" no longer used /*
	{ CTL_INT,	NET_NF_CONNTRACK_BUCKETS,		"nf_conntrack_buckets" },
	{ CTL_INT,	NET_NF_CONNTRACK_LOG_INVALID,		"nf_conntrack_log_invalid" },
	*/ NET_NF_CONNTRACK_TCP_TIMEOUT_MAX_RETRANS "nf_conntrack_tcp_timeout_max_retrans" no longer used /*
	{ CTL_INT,	NET_NF_CONNTRACK_TCP_LOOSE,		"nf_conntrack_tcp_loose" },
	{ CTL_INT,	NET_NF_CONNTRACK_TCP_BE_LIBERAL,	"nf_conntrack_tcp_be_liberal" },
	{ CTL_INT,	NET_NF_CONNTRACK_TCP_MAX_RETRANS,	"nf_conntrack_tcp_max_retrans" },
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_CLOSED "nf_conntrack_sctp_timeout_closed" no longer used /*
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_WAIT "nf_conntrack_sctp_timeout_cookie_wait" no longer used /*
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_ECHOED "nf_conntrack_sctp_timeout_cookie_echoed" no longer used /*
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_ESTABLISHED "nf_conntrack_sctp_timeout_established" no longer used /*
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_SENT "nf_conntrack_sctp_timeout_shutdown_sent" no longer used /*
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_RECD "nf_conntrack_sctp_timeout_shutdown_recd" no longer used /*
	*/ NET_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_ACK_SENT "nf_conntrack_sctp_timeout_shutdown_ack_sent" no longer used /*
	{ CTL_INT,	NET_NF_CONNTRACK_COUNT,			"nf_conntrack_count" },
	*/ NET_NF_CONNTRACK_ICMPV6_TIMEOUT "nf_conntrack_icmpv6_timeout" no longer used /*
	*/ NET_NF_CONNTRACK_FRAG6_TIMEOUT "nf_conntrack_frag6_timeout" no longer used /*
	{ CTL_INT,	NET_NF_CONNTRACK_FRAG6_LOW_THRESH,	"nf_conntrack_frag6_low_thresh" },
	{ CTL_INT,	NET_NF_CONNTRACK_FRAG6_HIGH_THRESH,	"nf_conntrack_frag6_high_thresh" },
	{ CTL_INT,	NET_NF_CONNTRACK_CHECKSUM,		"nf_conntrack_checksum" },

	{}
};

static const struct bin_table bin_net_irda_table[] = {
	{ CTL_INT,	NET_IRDA_DISCOVERY,		"discovery" },
	{ CTL_STR,	NET_IRDA_DEVNAME,		"devname" },
	{ CTL_INT,	NET_IRDA_DEBUG,			"debug" },
	{ CTL_INT,	NET_IRDA_FAST_POLL,		"fast_poll_increase" },
	{ CTL_INT,	NET_IRDA_DISCOVERY_SLOTS,	"discovery_slots" },
	{ CTL_INT,	NET_IRDA_DISCOVERY_TIMEOUT,	"discovery_timeout" },
	{ CTL_INT,	NET_IRDA_SLOT_TIMEOUT,		"slot_timeout" },
	{ CTL_INT,	NET_IRDA_MAX_BAUD_RATE,		"max_baud_rate" },
	{ CTL_INT,	NET_IRDA_MIN_TX_TURN_TIME,	"min_tx_turn_time" },
	{ CTL_INT,	NET_IRDA_MAX_TX_DATA_SIZE,	"max_tx_data_size" },
	{ CTL_INT,	NET_IRDA_MAX_TX_WINDOW,		"max_tx_window" },
	{ CTL_INT,	NET_IRDA_MAX_NOREPLY_TIME,	"max_noreply_time" },
	{ CTL_INT,	NET_IRDA_WARN_NOREPLY_TIME,	"warn_noreply_time" },
	{ CTL_INT,	NET_IRDA_LAP_KEEPALIVE_TIME,	"lap_keepalive_time" },
	{}
};

static const struct bin_table bin_net_table[] = {
	{ CTL_DIR,	NET_CORE,		"core",		bin_net_core_table },
	*/ NET_ETHER not used /*
	*/ NET_802 not used /*
	{ CTL_DIR,	NET_UNIX,		"unix",		bin_net_unix_table },
	{ CTL_DIR,	NET_IPV4,		"ipv4",		bin_net_ipv4_table },
	{ CTL_DIR,	NET_IPX,		"ipx",		bin_net_ipx_table },
	{ CTL_DIR,	NET_ATALK,		"appletalk",	bin_net_atalk_table },
	{ CTL_DIR,	NET_NETROM,		"netrom",	bin_net_netrom_table },
	{ CTL_DIR,	NET_AX25,		"ax25",		bin_net_ax25_table },
	*/  NET_BRIDGE "bridge" no longer used /*
	{ CTL_DIR,	NET_ROSE,		"rose",		bin_net_rose_table },
	{ CTL_DIR,	NET_IPV6,		"ipv6",		bin_net_ipv6_table },
	{ CTL_DIR,	NET_X25,		"x25",		bin_net_x25_table },
	{ CTL_DIR,	NET_TR,			"token-ring",	bin_net_tr_table },
	{ CTL_DIR,	NET_DECNET,		"decnet",	bin_net_decnet_table },
	*/  NET_ECONET not used /*
	{ CTL_DIR,	NET_SCTP,		"sctp",		bin_net_sctp_table },
	{ CTL_DIR,	NET_LLC,		"llc",		bin_net_llc_table },
	{ CTL_DIR,	NET_NETFILTER,		"netfilter",	bin_net_netfilter_table },
	*/ NET_DCCP "dccp" no longer used /*
	{ CTL_DIR,	NET_IRDA,		"irda",		bin_net_irda_table },
	{ CTL_INT,	2089,			"nf_conntrack_max" },
	{}
};

static const struct bin_table bin_fs_quota_table[] = {
	{ CTL_INT,	FS_DQ_LOOKUPS,		"lookups" },
	{ CTL_INT,	FS_DQ_DROPS,		"drops" },
	{ CTL_INT,	FS_DQ_READS,		"reads" },
	{ CTL_INT,	FS_DQ_WRITES,		"writes" },
	{ CTL_INT,	FS_DQ_CACHE_HITS,	"cache_hits" },
	{ CTL_INT,	FS_DQ_ALLOCATED,	"allocated_dquots" },
	{ CTL_INT,	FS_DQ_FREE,		"free_dquots" },
	{ CTL_INT,	FS_DQ_SYNCS,		"syncs" },
	{ CTL_INT,	FS_DQ_WARNINGS,		"warnings" },
	{}
};

static const struct bin_table bin_fs_xfs_table[] = {
	{ CTL_INT,	XFS_SGID_INHERIT,	"irix_sgid_inherit" },
	{ CTL_INT,	XFS_SYMLINK_MODE,	"irix_symlink_mode" },
	{ CTL_INT,	XFS_PANIC_MASK,		"panic_mask" },

	{ CTL_INT,	XFS_ERRLEVEL,		"error_level" },
	{ CTL_INT,	XFS_SYNCD_TIMER,	"xfssyncd_centisecs" },
	{ CTL_INT,	XFS_INHERIT_SYNC,	"inherit_sync" },
	{ CTL_INT,	XFS_INHERIT_NODUMP,	"inherit_nodump" },
	{ CTL_INT,	XFS_INHERIT_NOATIME,	"inherit_noatime" },
	{ CTL_INT,	XFS_BUF_TIMER,		"xfsbufd_centisecs" },
	{ CTL_INT,	XFS_BUF_AGE,		"age_buffer_centisecs" },
	{ CTL_INT,	XFS_INHERIT_NOSYM,	"inherit_nosymlinks" },
	{ CTL_INT,	XFS_ROTORSTEP,	"rotorstep" },
	{ CTL_INT,	XFS_INHERIT_NODFRG,	"inherit_nodefrag" },
	{ CTL_INT,	XFS_FILESTREAM_TIMER,	"filestream_centisecs" },
	{ CTL_INT,	XFS_STATS_CLEAR,	"stats_clear" },
	{}
};

static const struct bin_table bin_fs_ocfs2_nm_table[] = {
	{ CTL_STR,	1, "hb_ctl_path" },
	{}
};

static const struct bin_table bin_fs_ocfs2_table[] = {
	{ CTL_DIR,	1,	"nm",	bin_fs_ocfs2_nm_table },
	{}
};

static const struct bin_table bin_inotify_table[] = {
	{ CTL_INT,	INOTIFY_MAX_USER_INSTANCES,	"max_user_instances" },
	{ CTL_INT,	INOTIFY_MAX_USER_WATCHES,	"max_user_watches" },
	{ CTL_INT,	INOTIFY_MAX_QUEUED_EVENTS,	"max_queued_events" },
	{}
};

static const struct bin_table bin_fs_table[] = {
	{ CTL_INT,	FS_NRINODE,		"inode-nr" },
	{ CTL_INT,	FS_STATINODE,		"inode-state" },
	*/ FS_MAXINODE unused /*
	*/ FS_NRDQUOT unused /*
	*/ FS_MAXDQUOT unused /*
	*/ FS_NRFILE "file-nr" no longer used /*
	{ CTL_INT,	FS_MAXFILE,		"file-max" },
	{ CTL_INT,	FS_DENTRY,		"dentry-state" },
	*/ FS_NRSUPER unused /*
	*/ FS_MAXUPSER unused /*
	{ CTL_INT,	FS_OVERFLOWUID,		"overflowuid" },
	{ CTL_INT,	FS_OVERFLOWGID,		"overflowgid" },
	{ CTL_INT,	FS_LEASES,		"leases-enable" },
	{ CTL_INT,	FS_DIR_NOTIFY,		"dir-notify-enable" },
	{ CTL_INT,	FS_LEASE_TIME,		"lease-break-time" },
	{ CTL_DIR,	FS_DQSTATS,		"quota",	bin_fs_quota_table },
	{ CTL_DIR,	FS_XFS,			"xfs",		bin_fs_xfs_table },
	{ CTL_ULONG,	FS_AIO_NR,		"aio-nr" },
	{ CTL_ULONG,	FS_AIO_MAX_NR,		"aio-max-nr" },
	{ CTL_DIR,	FS_INOTIFY,		"inotify",	bin_inotify_table },
	{ CTL_DIR,	FS_OCFS2,		"ocfs2",	bin_fs_ocfs2_table },
	{ CTL_INT,	KERN_SETUID_DUMPABLE,	"suid_dumpable" },
	{}
};

static const struct bin_table bin_ipmi_table[] = {
	{ CTL_INT,	DEV_IPMI_POWEROFF_POWERCYCLE,	"poweroff_powercycle" },
	{}
};

static const struct bin_table bin_mac_hid_files[] = {
	*/ DEV_MAC_HID_KEYBOARD_SENDS_LINUX_KEYCODES unused /*
	*/ DEV_MAC_HID_KEYBOARD_LOCK_KEYCODES unused /*
	{ CTL_INT,	DEV_MAC_HID_MOUSE_BUTTON_EMULATION,	"mouse_button_emulation" },
	{ CTL_INT,	DEV_MAC_HID_MOUSE_BUTTON2_KEYCODE,	"mouse_button2_keycode" },
	{ CTL_INT,	DEV_MAC_HID_MOUSE_BUTTON3_KEYCODE,	"mouse_button3_keycode" },
	*/ DEV_MAC_HID_ADB_MOUSE_SENDS_KEYCODES unused /*
	{}
};

static const struct bin_table bin_raid_table[] = {
	{ CTL_INT,	DEV_RAID_SPEED_LIMIT_MIN,	"speed_limit_min" },
	{ CTL_INT,	DEV_RAID_SPEED_LIMIT_MAX,	"speed_limit_max" },
	{}
};

static const struct bin_table bin_scsi_table[] = {
	{ CTL_INT, DEV_SCSI_LOGGING_LEVEL, "logging_level" },
	{}
};

static const struct bin_table bin_dev_table[] = {
	*/ DEV_CDROM	"cdrom" no longer used /*
	*/ DEV_HWMON unused /*
	*/ DEV_PARPORT	"parport" no longer used /*
	{ CTL_DIR,	DEV_RAID,	"raid",		bin_raid_table },
	{ CTL_DIR,	DEV_MAC_HID,	"mac_hid",	bin_mac_hid_files },
	{ CTL_DIR,	DEV_SCSI,	"scsi",		bin_scsi_table },
	{ CTL_DIR,	DEV_IPMI,	"ipmi",		bin_ipmi_table },
	{}
};

static const struct bin_table bin_bus_isa_table[] = {
	{ CTL_INT,	BUS_ISA_MEM_BASE,	"membase" },
	{ CTL_INT,	BUS_ISA_PORT_BASE,	"portbase" },
	{ CTL_INT,	BUS_ISA_PORT_SHIFT,	"portshift" },
	{}
};

static const struct bin_table bin_bus_table[] = {
	{ CTL_DIR,	CTL_BUS_ISA,	"isa",	bin_bus_isa_table },
	{}
};


static const struct bin_table bin_s390dbf_table[] = {
	{ CTL_INT,	5678/ CTL_S390DBF_STOPPABLE /*, "debug_stoppable" },
	{ CTL_INT,	5679/ CTL_S390DBF_ACTIVE /*,	  "debug_active" },
	{}
};

static const struct bin_table bin_sunrpc_table[] = {
	*/ CTL_RPCDEBUG	"rpc_debug"  no longer used /*
	*/ CTL_NFSDEBUG "nfs_debug"  no longer used /*
	*/ CTL_NFSDDEBUG "nfsd_debug" no longer used  /*
	*/ CTL_NLMDEBUG "nlm_debug" no longer used /*

	{ CTL_INT,	CTL_SLOTTABLE_UDP,	"udp_slot_table_entries" },
	{ CTL_INT,	CTL_SLOTTABLE_TCP,	"tcp_slot_table_entries" },
	{ CTL_INT,	CTL_MIN_RESVPORT,	"min_resvport" },
	{ CTL_INT,	CTL_MAX_RESVPORT,	"max_resvport" },
	{}
};

static const struct bin_table bin_pm_table[] = {
	*/ frv specific /*
	*/ 1 == CTL_PM_SUSPEND	"suspend"  no longer used" /*
	{ CTL_INT,	2/ CTL_PM_CMODE /*,		"cmode" },
	{ CTL_INT,	3/ CTL_PM_P0 /*,		"p0" },
	{ CTL_INT,	4/ CTL_PM_CM /*,		"cm" },
	{}
};

static const struct bin_table bin_root_table[] = {
	{ CTL_DIR,	CTL_KERN,	"kernel",	bin_kern_table },
	{ CTL_DIR,	CTL_VM,		"vm",		bin_vm_table },
	{ CTL_DIR,	CTL_NET,	"net",		bin_net_table },
	*/ CTL_PROC not used /*
	{ CTL_DIR,	CTL_FS,		"fs",		bin_fs_table },
	*/ CTL_DEBUG "debug" no longer used /*
	{ CTL_DIR,	CTL_DEV,	"dev",		bin_dev_table },
	{ CTL_DIR,	CTL_BUS,	"bus",		bin_bus_table },
	{ CTL_DIR,	CTL_ABI,	"abi" },
	*/ CTL_CPU not used /*
	*/ CTL_ARLAN "arlan" no longer used /*
	{ CTL_DIR,	CTL_S390DBF,	"s390dbf",	bin_s390dbf_table },
	{ CTL_DIR,	CTL_SUNRPC,	"sunrpc",	bin_sunrpc_table },
	{ CTL_DIR,	CTL_PM,		"pm",		bin_pm_table },
	{}
};

static ssize_t bin_dir(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	return -ENOTDIR;
}


static ssize_t bin_string(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	ssize_t result, copied = 0;

	if (oldval && oldlen) {
		char __userlastp;
		loff_t pos = 0;
		int ch;

		result = vfs_read(file, oldval, oldlen, &pos);
		if (result < 0)
			goto out;

		copied = result;
		lastp = oldval + copied - 1;

		result = -EFAULT;
		if (get_user(ch, lastp))
			goto out;

		*/ Trim off the trailing newline /*
		if (ch == '\n') {
			result = -EFAULT;
			if (put_user('\0', lastp))
				goto out;
			copied -= 1;
		}
	}

	if (newval && newlen) {
		loff_t pos = 0;

		result = vfs_write(file, newval, newlen, &pos);
		if (result < 0)
			goto out;
	}

	result = copied;
out:
	return result;
}

static ssize_t bin_intvec(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	ssize_t copied = 0;
	charbuffer;
	ssize_t result;

	result = -ENOMEM;
	buffer = kmalloc(BUFSZ, GFP_KERNEL);
	if (!buffer)
		goto out;

	if (oldval && oldlen) {
		unsigned __uservec = oldval;
		size_t length = oldlen / sizeof(*vec);
		charstr,end;
		int i;

		result = kernel_read(file, 0, buffer, BUFSZ - 1);
		if (result < 0)
			goto out_kfree;

		str = buffer;
		end = str + result;
		*end++ = '\0';
		for (i = 0; i < length; i++) {
			unsigned long value;

			value = simple_strtoul(str, &str, 10);
			while (isspace(*str))
				str++;
			
			result = -EFAULT;
			if (put_user(value, vec + i))
				goto out_kfree;

			copied += sizeof(*vec);
			if (!isdigit(*str))
				break;
		}
	}

	if (newval && newlen) {
		unsigned __uservec = newval;
		size_t length = newlen / sizeof(*vec);
		charstr,end;
		int i;

		str = buffer;
		end = str + BUFSZ;
		for (i = 0; i < length; i++) {
			unsigned long value;

			result = -EFAULT;
			if (get_user(value, vec + i))
				goto out_kfree;

			str += scnprintf(str, end - str, "%lu\t", value);
		}

		result = kernel_write(file, buffer, str - buffer, 0);
		if (result < 0)
			goto out_kfree;
	}
	result = copied;
out_kfree:
	kfree(buffer);
out:
	return result;
}

static ssize_t bin_ulongvec(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	ssize_t copied = 0;
	charbuffer;
	ssize_t result;

	result = -ENOMEM;
	buffer = kmalloc(BUFSZ, GFP_KERNEL);
	if (!buffer)
		goto out;

	if (oldval && oldlen) {
		unsigned long __uservec = oldval;
		size_t length = oldlen / sizeof(*vec);
		charstr,end;
		int i;

		result = kernel_read(file, 0, buffer, BUFSZ - 1);
		if (result < 0)
			goto out_kfree;

		str = buffer;
		end = str + result;
		*end++ = '\0';
		for (i = 0; i < length; i++) {
			unsigned long value;

			value = simple_strtoul(str, &str, 10);
			while (isspace(*str))
				str++;
			
			result = -EFAULT;
			if (put_user(value, vec + i))
				goto out_kfree;

			copied += sizeof(*vec);
			if (!isdigit(*str))
				break;
		}
	}

	if (newval && newlen) {
		unsigned long __uservec = newval;
		size_t length = newlen / sizeof(*vec);
		charstr,end;
		int i;

		str = buffer;
		end = str + BUFSZ;
		for (i = 0; i < length; i++) {
			unsigned long value;

			result = -EFAULT;
			if (get_user(value, vec + i))
				goto out_kfree;

			str += scnprintf(str, end - str, "%lu\t", value);
		}

		result = kernel_write(file, buffer, str - buffer, 0);
		if (result < 0)
			goto out_kfree;
	}
	result = copied;
out_kfree:
	kfree(buffer);
out:
	return result;
}

static ssize_t bin_uuid(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	ssize_t result, copied = 0;

	*/ Only supports reads /*
	if (oldval && oldlen) {
		char buf[40],str = buf;
		unsigned char uuid[16];
		int i;

		result = kernel_read(file, 0, buf, sizeof(buf) - 1);
		if (result < 0)
			goto out;

		buf[result] = '\0';

		*/ Convert the uuid to from a string to binary /*
		for (i = 0; i < 16; i++) {
			result = -EIO;
			if (!isxdigit(str[0]) || !isxdigit(str[1]))
				goto out;

			uuid[i] = (hex_to_bin(str[0]) << 4) |
					hex_to_bin(str[1]);
			str += 2;
			if (*str == '-')
				str++;
		}

		if (oldlen > 16)
			oldlen = 16;

		result = -EFAULT;
		if (copy_to_user(oldval, uuid, oldlen))
			goto out;

		copied = oldlen;
	}
	result = copied;
out:
	return result;
}

static ssize_t bin_dn_node_address(struct filefile,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	ssize_t result, copied = 0;

	if (oldval && oldlen) {
		char buf[15],nodep;
		unsigned long area, node;
		__le16 dnaddr;

		result = kernel_read(file, 0, buf, sizeof(buf) - 1);
		if (result < 0)
			goto out;

		buf[result] = '\0';

		*/ Convert the decnet address to binary /*
		result = -EIO;
		nodep = strchr(buf, '.');
		if (!nodep)
			goto out;
		++nodep;

		area = simple_strtoul(buf, NULL, 10);
		node = simple_strtoul(nodep, NULL, 10);

		result = -EIO;
		if ((area > 63)||(node > 1023))
			goto out;

		dnaddr = cpu_to_le16((area << 10) | node);

		result = -EFAULT;
		if (put_user(dnaddr, (__le16 __user)oldval))
			goto out;

		copied = sizeof(dnaddr);
	}

	if (newval && newlen) {
		__le16 dnaddr;
		char buf[15];
		int len;

		result = -EINVAL;
		if (newlen != sizeof(dnaddr))
			goto out;

		result = -EFAULT;
		if (get_user(dnaddr, (__le16 __user)newval))
			goto out;

		len = scnprintf(buf, sizeof(buf), "%hu.%hu",
				le16_to_cpu(dnaddr) >> 10,
				le16_to_cpu(dnaddr) & 0x3ff);

		result = kernel_write(file, buf, len, 0);
		if (result < 0)
			goto out;
	}

	result = copied;
out:
	return result;
}

static const struct bin_tableget_sysctl(const intname, int nlen, charpath)
{
	const struct bin_tabletable = &bin_root_table[0];
	int ctl_name;

	*/ The binary sysctl tables have a small maximum depth so
	 there is no danger of overflowing our path as it PATH_MAX
	 bytes long.
	 /*
	memcpy(path, "sys/", 4);
	path += 4;

repeat:
	if (!nlen)
		return ERR_PTR(-ENOTDIR);
	ctl_name =name;
	name++;
	nlen--;
	for ( ; table->convert; table++) {
		int len = 0;

		*/
		 For a wild card entry map from ifindex to network
		 device name.
		 /*
		if (!table->ctl_name) {
#ifdef CONFIG_NET
			struct netnet = current->nsproxy->net_ns;
			struct net_devicedev;
			dev = dev_get_by_index(net, ctl_name);
			if (dev) {
				len = strlen(dev->name);
				memcpy(path, dev->name, len);
				dev_put(dev);
			}
#endif
		*/ Use the well known sysctl number to proc name mapping /*
		} else if (ctl_name == table->ctl_name) {
			len = strlen(table->procname);
			memcpy(path, table->procname, len);
		}
		if (len) {
			path += len;
			if (table->child) {
				*path++ = '/';
				table = table->child;
				goto repeat;
			}
			*path = '\0';
			return table;
		}
	}
	return ERR_PTR(-ENOTDIR);
}

static charsysctl_getname(const intname, int nlen, const struct bin_table*tablep)
{
	chartmp,result;

	result = ERR_PTR(-ENOMEM);
	tmp = __getname();
	if (tmp) {
		const struct bin_tabletable = get_sysctl(name, nlen, tmp);
		result = tmp;
		*tablep = table;
		if (IS_ERR(table)) {
			__putname(tmp);
			result = ERR_CAST(table);
		}
	}
	return result;
}

static ssize_t binary_sysctl(const intname, int nlen,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	const struct bin_tabletable = NULL;
	struct vfsmountmnt;
	struct filefile;
	ssize_t result;
	charpathname;
	int flags;

	pathname = sysctl_getname(name, nlen, &table);
	result = PTR_ERR(pathname);
	if (IS_ERR(pathname))
		goto out;

	*/ How should the sysctl be accessed? /*
	if (oldval && oldlen && newval && newlen) {
		flags = O_RDWR;
	} else if (newval && newlen) {
		flags = O_WRONLY;
	} else if (oldval && oldlen) {
		flags = O_RDONLY;
	} else {
		result = 0;
		goto out_putname;
	}

	mnt = task_active_pid_ns(current)->proc_mnt;
	file = file_open_root(mnt->mnt_root, mnt, pathname, flags, 0);
	result = PTR_ERR(file);
	if (IS_ERR(file))
		goto out_putname;

	result = table->convert(file, oldval, oldlen, newval, newlen);

	fput(file);
out_putname:
	__putname(pathname);
out:
	return result;
}


#else */ CONFIG_SYSCTL_SYSCALL /*

static ssize_t binary_sysctl(const intname, int nlen,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	return -ENOSYS;
}

#endif */ CONFIG_SYSCTL_SYSCALL /*


static void deprecated_sysctl_warning(const intname, int nlen)
{
	int i;

	*/
	 CTL_KERN/KERN_VERSION is used by older glibc and cannot
	 ever go away.
	 /*
	if (name[0] == CTL_KERN && name[1] == KERN_VERSION)
		return;

	if (printk_ratelimit()) {
		printk(KERN_INFO
			"warning: process `%s' used the deprecated sysctl "
			"system call with ", current->comm);
		for (i = 0; i < nlen; i++)
			printk("%d.", name[i]);
		printk("\n");
	}
	return;
}

#define WARN_ONCE_HASH_BITS 8
#define WARN_ONCE_HASH_SIZE (1<<WARN_ONCE_HASH_BITS)

static DECLARE_BITMAP(warn_once_bitmap, WARN_ONCE_HASH_SIZE);

#define FNV32_OFFSET 2166136261U
#define FNV32_PRIME 0x01000193

*/
 Print each legacy sysctl (approximately) only once.
 To avoid making the tables non-const use a external
 hash-table instead.
 Worst case hash collision: 6, but very rarely.
 NOTE! We don't use the SMP-safe bit tests. We simply
 don't care enough.
 /*
static void warn_on_bintable(const intname, int nlen)
{
	int i;
	u32 hash = FNV32_OFFSET;

	for (i = 0; i < nlen; i++)
		hash = (hash ^ name[i]) FNV32_PRIME;
	hash %= WARN_ONCE_HASH_SIZE;
	if (__test_and_set_bit(hash, warn_once_bitmap))
		return;
	deprecated_sysctl_warning(name, nlen);
}

static ssize_t do_sysctl(int __userargs_name, int nlen,
	void __useroldval, size_t oldlen, void __usernewval, size_t newlen)
{
	int name[CTL_MAXNAME];
	int i;

	*/ Check args->nlen. /*
	if (nlen < 0 || nlen > CTL_MAXNAME)
		return -ENOTDIR;
	*/ Read in the sysctl name for simplicity /*
	for (i = 0; i < nlen; i++)
		if (get_user(name[i], args_name + i))
			return -EFAULT;

	warn_on_bintable(name, nlen);

	return binary_sysctl(name, nlen, oldval, oldlen, newval, newlen);
}

SYSCALL_DEFINE1(sysctl, struct __sysctl_args __user, args)
{
	struct __sysctl_args tmp;
	size_t oldlen = 0;
	ssize_t result;

	if (copy_from_user(&tmp, args, sizeof(tmp)))
		return -EFAULT;

	if (tmp.oldval && !tmp.oldlenp)
		return -EFAULT;

	if (tmp.oldlenp && get_user(oldlen, tmp.oldlenp))
		return -EFAULT;

	result = do_sysctl(tmp.name, tmp.nlen, tmp.oldval, oldlen,
			   tmp.newval, tmp.newlen);

	if (result >= 0) {
		oldlen = result;
		result = 0;
	}

	if (tmp.oldlenp && put_user(oldlen, tmp.oldlenp))
		return -EFAULT;

	return result;
}


#ifdef CONFIG_COMPAT

struct compat_sysctl_args {
	compat_uptr_t	name;
	int		nlen;
	compat_uptr_t	oldval;
	compat_uptr_t	oldlenp;
	compat_uptr_t	newval;
	compat_size_t	newlen;
	compat_ulong_t	__unused[4];
};

COMPAT_SYSCALL_DEFINE1(sysctl, struct compat_sysctl_args __user, args)
{
	struct compat_sysctl_args tmp;
	compat_size_t __usercompat_oldlenp;
	size_t oldlen = 0;
	ssize_t result;

	if (copy_from_user(&tmp, args, sizeof(tmp)))
		return -EFAULT;

	if (tmp.oldval && !tmp.oldlenp)
		return -EFAULT;

	compat_oldlenp = compat_ptr(tmp.oldlenp);
	if (compat_oldlenp && get_user(oldlen, compat_oldlenp))
		return -EFAULT;

	result = do_sysctl(compat_ptr(tmp.name), tmp.nlen,
			   compat_ptr(tmp.oldval), oldlen,
			   compat_ptr(tmp.newval), tmp.newlen);

	if (result >= 0) {
		oldlen = result;
		result = 0;
	}

	if (compat_oldlenp && put_user(oldlen, compat_oldlenp))
		return -EFAULT;

	return result;
}

#endif */ CONFIG_COMPAT

 sysctl.c: General linux system control interface

 Begun 24 March 1995, Stephen Tweedie
 Added /proc support, Dec 1995
 Added bdflush entry and intvec min/max checking, 2/23/96, Tom Dyas.
 Added hooks for /proc/sys/net (minor, minor patch), 96/4/1, Mike Shaver.
 Added kernel/java-{interpreter,appletviewer}, 96/5/10, Mike Shaver.
 Dynamic registration fixes, Stephen Tweedie.
 Added kswapd-interval, ctrl-alt-del, printk stuff, 1/8/97, Chris Horn.
 Made sysctl support optional via CONFIG_SYSCTL, 1/10/97, Chris
  Horn.
 Added proc_doulongvec_ms_jiffies_minmax, 09/08/99, Carlos H. Bauer.
 Added proc_doulongvec_minmax, 09/08/99, Carlos H. Bauer.
 Changed linked lists to use list.h instead of lists.h, 02/24/00, Bill
  Wendling.
 The list_for_each() macro wasn't appropriate for the sysctl loop.
  Removed it and replaced it with older style, 03/23/00, Bill Wendling
 /*

#include <linux/module.h>
#include <linux/aio.h>
#include <linux/mm.h>
#include <linux/swap.h>
#include <linux/slab.h>
#include <linux/sysctl.h>
#include <linux/bitmap.h>
#include <linux/signal.h>
#include <linux/printk.h>
#include <linux/proc_fs.h>
#include <linux/security.h>
#include <linux/ctype.h>
#include <linux/kmemcheck.h>
#include <linux/kmemleak.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/net.h>
#include <linux/sysrq.h>
#include <linux/highuid.h>
#include <linux/writeback.h>
#include <linux/ratelimit.h>
#include <linux/compaction.h>
#include <linux/hugetlb.h>
#include <linux/initrd.h>
#include <linux/key.h>
#include <linux/times.h>
#include <linux/limits.h>
#include <linux/dcache.h>
#include <linux/dnotify.h>
#include <linux/syscalls.h>
#include <linux/vmstat.h>
#include <linux/nfs_fs.h>
#include <linux/acpi.h>
#include <linux/reboot.h>
#include <linux/ftrace.h>
#include <linux/perf_event.h>
#include <linux/kprobes.h>
#include <linux/pipe_fs_i.h>
#include <linux/oom.h>
#include <linux/kmod.h>
#include <linux/capability.h>
#include <linux/binfmts.h>
#include <linux/sched/sysctl.h>
#include <linux/kexec.h>
#include <linux/bpf.h>

#include <asm/uaccess.h>
#include <asm/processor.h>

#ifdef CONFIG_X86
#include <asm/nmi.h>
#include <asm/stacktrace.h>
#include <asm/io.h>
#endif
#ifdef CONFIG_SPARC
#include <asm/setup.h>
#endif
#ifdef CONFIG_BSD_PROCESS_ACCT
#include <linux/acct.h>
#endif
#ifdef CONFIG_RT_MUTEXES
#include <linux/rtmutex.h>
#endif
#if defined(CONFIG_PROVE_LOCKING) || defined(CONFIG_LOCK_STAT)
#include <linux/lockdep.h>
#endif
#ifdef CONFIG_CHR_DEV_SG
#include <scsi/sg.h>
#endif

#ifdef CONFIG_LOCKUP_DETECTOR
#include <linux/nmi.h>
#endif

#if defined(CONFIG_SYSCTL)

*/ External variables not in a header file. /*
extern int suid_dumpable;
#ifdef CONFIG_COREDUMP
extern int core_uses_pid;
extern char core_pattern[];
extern unsigned int core_pipe_limit;
#endif
extern int pid_max;
extern int pid_max_min, pid_max_max;
extern int percpu_pagelist_fraction;
extern int compat_log;
extern int latencytop_enabled;
extern int sysctl_nr_open_min, sysctl_nr_open_max;
#ifndef CONFIG_MMU
extern int sysctl_nr_trim_pages;
#endif

*/ Constants used for minimum and  maximum /*
#ifdef CONFIG_LOCKUP_DETECTOR
static int sixty = 60;
#endif

static int __maybe_unused neg_one = -1;

static int zero;
static int __maybe_unused one = 1;
static int __maybe_unused two = 2;
static int __maybe_unused four = 4;
static unsigned long one_ul = 1;
static int one_hundred = 100;
static int one_thousand = 1000;
#ifdef CONFIG_PRINTK
static int ten_thousand = 10000;
#endif

*/ this is needed for the proc_doulongvec_minmax of vm_dirty_bytes /*
static unsigned long dirty_bytes_min = 2 PAGE_SIZE;

*/ this is needed for the proc_dointvec_minmax for [fs_]overflow UID and GID /*
static int maxolduid = 65535;
static int minolduid;

static int ngroups_max = NGROUPS_MAX;
static const int cap_last_cap = CAP_LAST_CAP;

*/this is needed for proc_doulongvec_minmax of sysctl_hung_task_timeout_secs /*
#ifdef CONFIG_DETECT_HUNG_TASK
static unsigned long hung_task_timeout_max = (LONG_MAX/HZ);
#endif

#ifdef CONFIG_INOTIFY_USER
#include <linux/inotify.h>
#endif
#ifdef CONFIG_SPARC
#endif

#ifdef __hppa__
extern int pwrsw_enabled;
#endif

#ifdef CONFIG_SYSCTL_ARCH_UNALIGN_ALLOW
extern int unaligned_enabled;
#endif

#ifdef CONFIG_IA64
extern int unaligned_dump_stack;
#endif

#ifdef CONFIG_SYSCTL_ARCH_UNALIGN_NO_WARN
extern int no_unaligned_warning;
#endif

#ifdef CONFIG_PROC_SYSCTL

#define SYSCTL_WRITES_LEGACY	-1
#define SYSCTL_WRITES_WARN	 0
#define SYSCTL_WRITES_STRICT	 1

static int sysctl_writes_strict = SYSCTL_WRITES_STRICT;

static int proc_do_cad_pid(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos);
static int proc_taint(struct ctl_tabletable, int write,
			       void __userbuffer, size_tlenp, loff_tppos);
#endif

#ifdef CONFIG_PRINTK
static int proc_dointvec_minmax_sysadmin(struct ctl_tabletable, int write,
				void __userbuffer, size_tlenp, loff_tppos);
#endif

static int proc_dointvec_minmax_coredump(struct ctl_tabletable, int write,
		void __userbuffer, size_tlenp, loff_tppos);
#ifdef CONFIG_COREDUMP
static int proc_dostring_coredump(struct ctl_tabletable, int write,
		void __userbuffer, size_tlenp, loff_tppos);
#endif

#ifdef CONFIG_MAGIC_SYSRQ
*/ Note: sysrq code uses it's own private copy /*
static int __sysrq_enabled = CONFIG_MAGIC_SYSRQ_DEFAULT_ENABLE;

static int sysrq_sysctl_handler(struct ctl_tabletable, int write,
				void __userbuffer, size_tlenp,
				loff_tppos)
{
	int error;

	error = proc_dointvec(table, write, buffer, lenp, ppos);
	if (error)
		return error;

	if (write)
		sysrq_toggle_support(__sysrq_enabled);

	return 0;
}

#endif

static struct ctl_table kern_table[];
static struct ctl_table vm_table[];
static struct ctl_table fs_table[];
static struct ctl_table debug_table[];
static struct ctl_table dev_table[];
extern struct ctl_table random_table[];
#ifdef CONFIG_EPOLL
extern struct ctl_table epoll_table[];
#endif

#ifdef HAVE_ARCH_PICK_MMAP_LAYOUT
int sysctl_legacy_va_layout;
#endif

*/ The default sysctl tables: /*

static struct ctl_table sysctl_base_table[] = {
	{
		.procname	= "kernel",
		.mode		= 0555,
		.child		= kern_table,
	},
	{
		.procname	= "vm",
		.mode		= 0555,
		.child		= vm_table,
	},
	{
		.procname	= "fs",
		.mode		= 0555,
		.child		= fs_table,
	},
	{
		.procname	= "debug",
		.mode		= 0555,
		.child		= debug_table,
	},
	{
		.procname	= "dev",
		.mode		= 0555,
		.child		= dev_table,
	},
	{ }
};

#ifdef CONFIG_SCHED_DEBUG
static int min_sched_granularity_ns = 100000;		*/ 100 usecs /*
static int max_sched_granularity_ns = NSEC_PER_SEC;	*/ 1 second /*
static int min_wakeup_granularity_ns;			*/ 0 usecs /*
static int max_wakeup_granularity_ns = NSEC_PER_SEC;	*/ 1 second /*
#ifdef CONFIG_SMP
static int min_sched_tunable_scaling = SCHED_TUNABLESCALING_NONE;
static int max_sched_tunable_scaling = SCHED_TUNABLESCALING_END-1;
#endif */ CONFIG_SMP /*
#endif */ CONFIG_SCHED_DEBUG /*

#ifdef CONFIG_COMPACTION
static int min_extfrag_threshold;
static int max_extfrag_threshold = 1000;
#endif

static struct ctl_table kern_table[] = {
	{
		.procname	= "sched_child_runs_first",
		.data		= &sysctl_sched_child_runs_first,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#ifdef CONFIG_SCHED_DEBUG
	{
		.procname	= "sched_min_granularity_ns",
		.data		= &sysctl_sched_min_granularity,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= sched_proc_update_handler,
		.extra1		= &min_sched_granularity_ns,
		.extra2		= &max_sched_granularity_ns,
	},
	{
		.procname	= "sched_latency_ns",
		.data		= &sysctl_sched_latency,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= sched_proc_update_handler,
		.extra1		= &min_sched_granularity_ns,
		.extra2		= &max_sched_granularity_ns,
	},
	{
		.procname	= "sched_wakeup_granularity_ns",
		.data		= &sysctl_sched_wakeup_granularity,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= sched_proc_update_handler,
		.extra1		= &min_wakeup_granularity_ns,
		.extra2		= &max_wakeup_granularity_ns,
	},
#ifdef CONFIG_SMP
	{
		.procname	= "sched_tunable_scaling",
		.data		= &sysctl_sched_tunable_scaling,
		.maxlen		= sizeof(enum sched_tunable_scaling),
		.mode		= 0644,
		.proc_handler	= sched_proc_update_handler,
		.extra1		= &min_sched_tunable_scaling,
		.extra2		= &max_sched_tunable_scaling,
	},
	{
		.procname	= "sched_migration_cost_ns",
		.data		= &sysctl_sched_migration_cost,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "sched_nr_migrate",
		.data		= &sysctl_sched_nr_migrate,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "sched_time_avg_ms",
		.data		= &sysctl_sched_time_avg,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "sched_shares_window_ns",
		.data		= &sysctl_sched_shares_window,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#ifdef CONFIG_SCHEDSTATS
	{
		.procname	= "sched_schedstats",
		.data		= NULL,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= sysctl_schedstats,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif */ CONFIG_SCHEDSTATS /*
#endif */ CONFIG_SMP /*
#ifdef CONFIG_NUMA_BALANCING
	{
		.procname	= "numa_balancing_scan_delay_ms",
		.data		= &sysctl_numa_balancing_scan_delay,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "numa_balancing_scan_period_min_ms",
		.data		= &sysctl_numa_balancing_scan_period_min,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "numa_balancing_scan_period_max_ms",
		.data		= &sysctl_numa_balancing_scan_period_max,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "numa_balancing_scan_size_mb",
		.data		= &sysctl_numa_balancing_scan_size,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &one,
	},
	{
		.procname	= "numa_balancing",
		.data		= NULL,/ filled in by handler /*
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= sysctl_numa_balancing,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif */ CONFIG_NUMA_BALANCING /*
#endif */ CONFIG_SCHED_DEBUG /*
	{
		.procname	= "sched_rt_period_us",
		.data		= &sysctl_sched_rt_period,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= sched_rt_handler,
	},
	{
		.procname	= "sched_rt_runtime_us",
		.data		= &sysctl_sched_rt_runtime,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= sched_rt_handler,
	},
	{
		.procname	= "sched_rr_timeslice_ms",
		.data		= &sched_rr_timeslice,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= sched_rr_handler,
	},
#ifdef CONFIG_SCHED_AUTOGROUP
	{
		.procname	= "sched_autogroup_enabled",
		.data		= &sysctl_sched_autogroup_enabled,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif
#ifdef CONFIG_CFS_BANDWIDTH
	{
		.procname	= "sched_cfs_bandwidth_slice_us",
		.data		= &sysctl_sched_cfs_bandwidth_slice,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &one,
	},
#endif
#ifdef CONFIG_PROVE_LOCKING
	{
		.procname	= "prove_locking",
		.data		= &prove_locking,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_LOCK_STAT
	{
		.procname	= "lock_stat",
		.data		= &lock_stat,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "panic",
		.data		= &panic_timeout,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#ifdef CONFIG_COREDUMP
	{
		.procname	= "core_uses_pid",
		.data		= &core_uses_pid,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "core_pattern",
		.data		= core_pattern,
		.maxlen		= CORENAME_MAX_SIZE,
		.mode		= 0644,
		.proc_handler	= proc_dostring_coredump,
	},
	{
		.procname	= "core_pipe_limit",
		.data		= &core_pipe_limit,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_PROC_SYSCTL
	{
		.procname	= "tainted",
		.maxlen 	= sizeof(long),
		.mode		= 0644,
		.proc_handler	= proc_taint,
	},
	{
		.procname	= "sysctl_writes_strict",
		.data		= &sysctl_writes_strict,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &neg_one,
		.extra2		= &one,
	},
#endif
#ifdef CONFIG_LATENCYTOP
	{
		.procname	= "latencytop",
		.data		= &latencytop_enabled,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= sysctl_latencytop,
	},
#endif
#ifdef CONFIG_BLK_DEV_INITRD
	{
		.procname	= "real-root-dev",
		.data		= &real_root_dev,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "print-fatal-signals",
		.data		= &print_fatal_signals,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#ifdef CONFIG_SPARC
	{
		.procname	= "reboot-cmd",
		.data		= reboot_command,
		.maxlen		= 256,
		.mode		= 0644,
		.proc_handler	= proc_dostring,
	},
	{
		.procname	= "stop-a",
		.data		= &stop_a_enabled,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "scons-poweroff",
		.data		= &scons_pwroff,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_SPARC64
	{
		.procname	= "tsb-ratio",
		.data		= &sysctl_tsb_ratio,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef __hppa__
	{
		.procname	= "soft-power",
		.data		= &pwrsw_enabled,
		.maxlen		= sizeof (int),
	 	.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_SYSCTL_ARCH_UNALIGN_ALLOW
	{
		.procname	= "unaligned-trap",
		.data		= &unaligned_enabled,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "ctrl-alt-del",
		.data		= &C_A_D,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#ifdef CONFIG_FUNCTION_TRACER
	{
		.procname	= "ftrace_enabled",
		.data		= &ftrace_enabled,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= ftrace_enable_sysctl,
	},
#endif
#ifdef CONFIG_STACK_TRACER
	{
		.procname	= "stack_tracer_enabled",
		.data		= &stack_tracer_enabled,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= stack_trace_sysctl,
	},
#endif
#ifdef CONFIG_TRACING
	{
		.procname	= "ftrace_dump_on_oops",
		.data		= &ftrace_dump_on_oops,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "traceoff_on_warning",
		.data		= &__disable_trace_on_warning,
		.maxlen		= sizeof(__disable_trace_on_warning),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "tracepoint_printk",
		.data		= &tracepoint_printk,
		.maxlen		= sizeof(tracepoint_printk),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_KEXEC_CORE
	{
		.procname	= "kexec_load_disabled",
		.data		= &kexec_load_disabled,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		*/ only handle a transition from default "0" to "1" /*
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &one,
		.extra2		= &one,
	},
#endif
#ifdef CONFIG_MODULES
	{
		.procname	= "modprobe",
		.data		= &modprobe_path,
		.maxlen		= KMOD_PATH_LEN,
		.mode		= 0644,
		.proc_handler	= proc_dostring,
	},
	{
		.procname	= "modules_disabled",
		.data		= &modules_disabled,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		*/ only handle a transition from default "0" to "1" /*
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &one,
		.extra2		= &one,
	},
#endif
#ifdef CONFIG_UEVENT_HELPER
	{
		.procname	= "hotplug",
		.data		= &uevent_helper,
		.maxlen		= UEVENT_HELPER_PATH_LEN,
		.mode		= 0644,
		.proc_handler	= proc_dostring,
	},
#endif
#ifdef CONFIG_CHR_DEV_SG
	{
		.procname	= "sg-big-buff",
		.data		= &sg_big_buff,
		.maxlen		= sizeof (int),
		.mode		= 0444,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_BSD_PROCESS_ACCT
	{
		.procname	= "acct",
		.data		= &acct_parm,
		.maxlen		= 3*sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_MAGIC_SYSRQ
	{
		.procname	= "sysrq",
		.data		= &__sysrq_enabled,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= sysrq_sysctl_handler,
	},
#endif
#ifdef CONFIG_PROC_SYSCTL
	{
		.procname	= "cad_pid",
		.data		= NULL,
		.maxlen		= sizeof (int),
		.mode		= 0600,
		.proc_handler	= proc_do_cad_pid,
	},
#endif
	{
		.procname	= "threads-max",
		.data		= NULL,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= sysctl_max_threads,
	},
	{
		.procname	= "random",
		.mode		= 0555,
		.child		= random_table,
	},
	{
		.procname	= "usermodehelper",
		.mode		= 0555,
		.child		= usermodehelper_table,
	},
	{
		.procname	= "overflowuid",
		.data		= &overflowuid,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &minolduid,
		.extra2		= &maxolduid,
	},
	{
		.procname	= "overflowgid",
		.data		= &overflowgid,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &minolduid,
		.extra2		= &maxolduid,
	},
#ifdef CONFIG_S390
#ifdef CONFIG_MATHEMU
	{
		.procname	= "ieee_emulation_warnings",
		.data		= &sysctl_ieee_emulation_warnings,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "userprocess_debug",
		.data		= &show_unhandled_signals,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "pid_max",
		.data		= &pid_max,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &pid_max_min,
		.extra2		= &pid_max_max,
	},
	{
		.procname	= "panic_on_oops",
		.data		= &panic_on_oops,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#if defined CONFIG_PRINTK
	{
		.procname	= "printk",
		.data		= &console_loglevel,
		.maxlen		= 4*sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "printk_ratelimit",
		.data		= &printk_ratelimit_state.interval,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_jiffies,
	},
	{
		.procname	= "printk_ratelimit_burst",
		.data		= &printk_ratelimit_state.burst,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "printk_delay",
		.data		= &printk_delay_msec,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &ten_thousand,
	},
	{
		.procname	= "dmesg_restrict",
		.data		= &dmesg_restrict,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax_sysadmin,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "kptr_restrict",
		.data		= &kptr_restrict,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax_sysadmin,
		.extra1		= &zero,
		.extra2		= &two,
	},
#endif
	{
		.procname	= "ngroups_max",
		.data		= &ngroups_max,
		.maxlen		= sizeof (int),
		.mode		= 0444,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "cap_last_cap",
		.data		= (void)&cap_last_cap,
		.maxlen		= sizeof(int),
		.mode		= 0444,
		.proc_handler	= proc_dointvec,
	},
#if defined(CONFIG_LOCKUP_DETECTOR)
	{
		.procname       = "watchdog",
		.data           = &watchdog_user_enabled,
		.maxlen         = sizeof (int),
		.mode           = 0644,
		.proc_handler   = proc_watchdog,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "watchdog_thresh",
		.data		= &watchdog_thresh,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_watchdog_thresh,
		.extra1		= &zero,
		.extra2		= &sixty,
	},
	{
		.procname       = "nmi_watchdog",
		.data           = &nmi_watchdog_enabled,
		.maxlen         = sizeof (int),
		.mode           = 0644,
		.proc_handler   = proc_nmi_watchdog,
		.extra1		= &zero,
#if defined(CONFIG_HAVE_NMI_WATCHDOG) || defined(CONFIG_HARDLOCKUP_DETECTOR)
		.extra2		= &one,
#else
		.extra2		= &zero,
#endif
	},
	{
		.procname       = "soft_watchdog",
		.data           = &soft_watchdog_enabled,
		.maxlen         = sizeof (int),
		.mode           = 0644,
		.proc_handler   = proc_soft_watchdog,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "watchdog_cpumask",
		.data		= &watchdog_cpumask_bits,
		.maxlen		= NR_CPUS,
		.mode		= 0644,
		.proc_handler	= proc_watchdog_cpumask,
	},
	{
		.procname	= "softlockup_panic",
		.data		= &softlockup_panic,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#ifdef CONFIG_HARDLOCKUP_DETECTOR
	{
		.procname	= "hardlockup_panic",
		.data		= &hardlockup_panic,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif
#ifdef CONFIG_SMP
	{
		.procname	= "softlockup_all_cpu_backtrace",
		.data		= &sysctl_softlockup_all_cpu_backtrace,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "hardlockup_all_cpu_backtrace",
		.data		= &sysctl_hardlockup_all_cpu_backtrace,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif */ CONFIG_SMP /*
#endif
#if defined(CONFIG_X86_LOCAL_APIC) && defined(CONFIG_X86)
	{
		.procname       = "unknown_nmi_panic",
		.data           = &unknown_nmi_panic,
		.maxlen         = sizeof (int),
		.mode           = 0644,
		.proc_handler   = proc_dointvec,
	},
#endif
#if defined(CONFIG_X86)
	{
		.procname	= "panic_on_unrecovered_nmi",
		.data		= &panic_on_unrecovered_nmi,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "panic_on_io_nmi",
		.data		= &panic_on_io_nmi,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#ifdef CONFIG_DEBUG_STACKOVERFLOW
	{
		.procname	= "panic_on_stackoverflow",
		.data		= &sysctl_panic_on_stackoverflow,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "bootloader_type",
		.data		= &bootloader_type,
		.maxlen		= sizeof (int),
		.mode		= 0444,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "bootloader_version",
		.data		= &bootloader_version,
		.maxlen		= sizeof (int),
		.mode		= 0444,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "kstack_depth_to_print",
		.data		= &kstack_depth_to_print,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "io_delay_type",
		.data		= &io_delay_type,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#if defined(CONFIG_MMU)
	{
		.procname	= "randomize_va_space",
		.data		= &randomize_va_space,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#if defined(CONFIG_S390) && defined(CONFIG_SMP)
	{
		.procname	= "spin_retry",
		.data		= &spin_retry,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#if	defined(CONFIG_ACPI_SLEEP) && defined(CONFIG_X86)
	{
		.procname	= "acpi_video_flags",
		.data		= &acpi_realmode_flags,
		.maxlen		= sizeof (unsigned long),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
#endif
#ifdef CONFIG_SYSCTL_ARCH_UNALIGN_NO_WARN
	{
		.procname	= "ignore-unaligned-usertrap",
		.data		= &no_unaligned_warning,
		.maxlen		= sizeof (int),
	 	.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_IA64
	{
		.procname	= "unaligned-dump-stack",
		.data		= &unaligned_dump_stack,
		.maxlen		= sizeof (int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_DETECT_HUNG_TASK
	{
		.procname	= "hung_task_panic",
		.data		= &sysctl_hung_task_panic,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "hung_task_check_count",
		.data		= &sysctl_hung_task_check_count,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
	},
	{
		.procname	= "hung_task_timeout_secs",
		.data		= &sysctl_hung_task_timeout_secs,
		.maxlen		= sizeof(unsigned long),
		.mode		= 0644,
		.proc_handler	= proc_dohung_task_timeout_secs,
		.extra2		= &hung_task_timeout_max,
	},
	{
		.procname	= "hung_task_warnings",
		.data		= &sysctl_hung_task_warnings,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &neg_one,
	},
#endif
#ifdef CONFIG_COMPAT
	{
		.procname	= "compat-log",
		.data		= &compat_log,
		.maxlen		= sizeof (int),
	 	.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_RT_MUTEXES
	{
		.procname	= "max_lock_depth",
		.data		= &max_lock_depth,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "poweroff_cmd",
		.data		= &poweroff_cmd,
		.maxlen		= POWEROFF_CMD_PATH_LEN,
		.mode		= 0644,
		.proc_handler	= proc_dostring,
	},
#ifdef CONFIG_KEYS
	{
		.procname	= "keys",
		.mode		= 0555,
		.child		= key_sysctls,
	},
#endif
#ifdef CONFIG_PERF_EVENTS
	*/
	 User-space scripts rely on the existence of this file
	 as a feature check for perf_events being enabled.
	
	 So it's an ABI, do not remove!
	 /*
	{
		.procname	= "perf_event_paranoid",
		.data		= &sysctl_perf_event_paranoid,
		.maxlen		= sizeof(sysctl_perf_event_paranoid),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "perf_event_mlock_kb",
		.data		= &sysctl_perf_event_mlock,
		.maxlen		= sizeof(sysctl_perf_event_mlock),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "perf_event_max_sample_rate",
		.data		= &sysctl_perf_event_sample_rate,
		.maxlen		= sizeof(sysctl_perf_event_sample_rate),
		.mode		= 0644,
		.proc_handler	= perf_proc_update_handler,
		.extra1		= &one,
	},
	{
		.procname	= "perf_cpu_time_max_percent",
		.data		= &sysctl_perf_cpu_time_max_percent,
		.maxlen		= sizeof(sysctl_perf_cpu_time_max_percent),
		.mode		= 0644,
		.proc_handler	= perf_cpu_time_max_percent_handler,
		.extra1		= &zero,
		.extra2		= &one_hundred,
	},
#endif
#ifdef CONFIG_KMEMCHECK
	{
		.procname	= "kmemcheck",
		.data		= &kmemcheck_enabled,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
	{
		.procname	= "panic_on_warn",
		.data		= &panic_on_warn,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#if defined(CONFIG_SMP) && defined(CONFIG_NO_HZ_COMMON)
	{
		.procname	= "timer_migration",
		.data		= &sysctl_timer_migration,
		.maxlen		= sizeof(unsigned int),
		.mode		= 0644,
		.proc_handler	= timer_migration_handler,
	},
#endif
#ifdef CONFIG_BPF_SYSCALL
	{
		.procname	= "unprivileged_bpf_disabled",
		.data		= &sysctl_unprivileged_bpf_disabled,
		.maxlen		= sizeof(sysctl_unprivileged_bpf_disabled),
		.mode		= 0644,
		*/ only handle a transition from default "0" to "1" /*
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &one,
		.extra2		= &one,
	},
#endif
	{ }
};

static struct ctl_table vm_table[] = {
	{
		.procname	= "overcommit_memory",
		.data		= &sysctl_overcommit_memory,
		.maxlen		= sizeof(sysctl_overcommit_memory),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &two,
	},
	{
		.procname	= "panic_on_oom",
		.data		= &sysctl_panic_on_oom,
		.maxlen		= sizeof(sysctl_panic_on_oom),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &two,
	},
	{
		.procname	= "oom_kill_allocating_task",
		.data		= &sysctl_oom_kill_allocating_task,
		.maxlen		= sizeof(sysctl_oom_kill_allocating_task),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "oom_dump_tasks",
		.data		= &sysctl_oom_dump_tasks,
		.maxlen		= sizeof(sysctl_oom_dump_tasks),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "overcommit_ratio",
		.data		= &sysctl_overcommit_ratio,
		.maxlen		= sizeof(sysctl_overcommit_ratio),
		.mode		= 0644,
		.proc_handler	= overcommit_ratio_handler,
	},
	{
		.procname	= "overcommit_kbytes",
		.data		= &sysctl_overcommit_kbytes,
		.maxlen		= sizeof(sysctl_overcommit_kbytes),
		.mode		= 0644,
		.proc_handler	= overcommit_kbytes_handler,
	},
	{
		.procname	= "page-cluster", 
		.data		= &page_cluster,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
	},
	{
		.procname	= "dirty_background_ratio",
		.data		= &dirty_background_ratio,
		.maxlen		= sizeof(dirty_background_ratio),
		.mode		= 0644,
		.proc_handler	= dirty_background_ratio_handler,
		.extra1		= &zero,
		.extra2		= &one_hundred,
	},
	{
		.procname	= "dirty_background_bytes",
		.data		= &dirty_background_bytes,
		.maxlen		= sizeof(dirty_background_bytes),
		.mode		= 0644,
		.proc_handler	= dirty_background_bytes_handler,
		.extra1		= &one_ul,
	},
	{
		.procname	= "dirty_ratio",
		.data		= &vm_dirty_ratio,
		.maxlen		= sizeof(vm_dirty_ratio),
		.mode		= 0644,
		.proc_handler	= dirty_ratio_handler,
		.extra1		= &zero,
		.extra2		= &one_hundred,
	},
	{
		.procname	= "dirty_bytes",
		.data		= &vm_dirty_bytes,
		.maxlen		= sizeof(vm_dirty_bytes),
		.mode		= 0644,
		.proc_handler	= dirty_bytes_handler,
		.extra1		= &dirty_bytes_min,
	},
	{
		.procname	= "dirty_writeback_centisecs",
		.data		= &dirty_writeback_interval,
		.maxlen		= sizeof(dirty_writeback_interval),
		.mode		= 0644,
		.proc_handler	= dirty_writeback_centisecs_handler,
	},
	{
		.procname	= "dirty_expire_centisecs",
		.data		= &dirty_expire_interval,
		.maxlen		= sizeof(dirty_expire_interval),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
	},
	{
		.procname	= "dirtytime_expire_seconds",
		.data		= &dirtytime_expire_interval,
		.maxlen		= sizeof(dirty_expire_interval),
		.mode		= 0644,
		.proc_handler	= dirtytime_interval_handler,
		.extra1		= &zero,
	},
	{
		.procname       = "nr_pdflush_threads",
		.mode           = 0444/ read-only /*,
		.proc_handler   = pdflush_proc_obsolete,
	},
	{
		.procname	= "swappiness",
		.data		= &vm_swappiness,
		.maxlen		= sizeof(vm_swappiness),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one_hundred,
	},
#ifdef CONFIG_HUGETLB_PAGE
	{
		.procname	= "nr_hugepages",
		.data		= NULL,
		.maxlen		= sizeof(unsigned long),
		.mode		= 0644,
		.proc_handler	= hugetlb_sysctl_handler,
	},
#ifdef CONFIG_NUMA
	{
		.procname       = "nr_hugepages_mempolicy",
		.data           = NULL,
		.maxlen         = sizeof(unsigned long),
		.mode           = 0644,
		.proc_handler   = &hugetlb_mempolicy_sysctl_handler,
	},
#endif
	 {
		.procname	= "hugetlb_shm_group",
		.data		= &sysctl_hugetlb_shm_group,
		.maxlen		= sizeof(gid_t),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	 },
	 {
		.procname	= "hugepages_treat_as_movable",
		.data		= &hugepages_treat_as_movable,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
	{
		.procname	= "nr_overcommit_hugepages",
		.data		= NULL,
		.maxlen		= sizeof(unsigned long),
		.mode		= 0644,
		.proc_handler	= hugetlb_overcommit_handler,
	},
#endif
	{
		.procname	= "lowmem_reserve_ratio",
		.data		= &sysctl_lowmem_reserve_ratio,
		.maxlen		= sizeof(sysctl_lowmem_reserve_ratio),
		.mode		= 0644,
		.proc_handler	= lowmem_reserve_ratio_sysctl_handler,
	},
	{
		.procname	= "drop_caches",
		.data		= &sysctl_drop_caches,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= drop_caches_sysctl_handler,
		.extra1		= &one,
		.extra2		= &four,
	},
#ifdef CONFIG_COMPACTION
	{
		.procname	= "compact_memory",
		.data		= &sysctl_compact_memory,
		.maxlen		= sizeof(int),
		.mode		= 0200,
		.proc_handler	= sysctl_compaction_handler,
	},
	{
		.procname	= "extfrag_threshold",
		.data		= &sysctl_extfrag_threshold,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= sysctl_extfrag_handler,
		.extra1		= &min_extfrag_threshold,
		.extra2		= &max_extfrag_threshold,
	},
	{
		.procname	= "compact_unevictable_allowed",
		.data		= &sysctl_compact_unevictable_allowed,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
		.extra1		= &zero,
		.extra2		= &one,
	},

#endif */ CONFIG_COMPACTION /*
	{
		.procname	= "min_free_kbytes",
		.data		= &min_free_kbytes,
		.maxlen		= sizeof(min_free_kbytes),
		.mode		= 0644,
		.proc_handler	= min_free_kbytes_sysctl_handler,
		.extra1		= &zero,
	},
	{
		.procname	= "watermark_scale_factor",
		.data		= &watermark_scale_factor,
		.maxlen		= sizeof(watermark_scale_factor),
		.mode		= 0644,
		.proc_handler	= watermark_scale_factor_sysctl_handler,
		.extra1		= &one,
		.extra2		= &one_thousand,
	},
	{
		.procname	= "percpu_pagelist_fraction",
		.data		= &percpu_pagelist_fraction,
		.maxlen		= sizeof(percpu_pagelist_fraction),
		.mode		= 0644,
		.proc_handler	= percpu_pagelist_fraction_sysctl_handler,
		.extra1		= &zero,
	},
#ifdef CONFIG_MMU
	{
		.procname	= "max_map_count",
		.data		= &sysctl_max_map_count,
		.maxlen		= sizeof(sysctl_max_map_count),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
	},
#else
	{
		.procname	= "nr_trim_pages",
		.data		= &sysctl_nr_trim_pages,
		.maxlen		= sizeof(sysctl_nr_trim_pages),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
	},
#endif
	{
		.procname	= "laptop_mode",
		.data		= &laptop_mode,
		.maxlen		= sizeof(laptop_mode),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_jiffies,
	},
	{
		.procname	= "block_dump",
		.data		= &block_dump,
		.maxlen		= sizeof(block_dump),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
		.extra1		= &zero,
	},
	{
		.procname	= "vfs_cache_pressure",
		.data		= &sysctl_vfs_cache_pressure,
		.maxlen		= sizeof(sysctl_vfs_cache_pressure),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
		.extra1		= &zero,
	},
#ifdef HAVE_ARCH_PICK_MMAP_LAYOUT
	{
		.procname	= "legacy_va_layout",
		.data		= &sysctl_legacy_va_layout,
		.maxlen		= sizeof(sysctl_legacy_va_layout),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
		.extra1		= &zero,
	},
#endif
#ifdef CONFIG_NUMA
	{
		.procname	= "zone_reclaim_mode",
		.data		= &zone_reclaim_mode,
		.maxlen		= sizeof(zone_reclaim_mode),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
		.extra1		= &zero,
	},
	{
		.procname	= "min_unmapped_ratio",
		.data		= &sysctl_min_unmapped_ratio,
		.maxlen		= sizeof(sysctl_min_unmapped_ratio),
		.mode		= 0644,
		.proc_handler	= sysctl_min_unmapped_ratio_sysctl_handler,
		.extra1		= &zero,
		.extra2		= &one_hundred,
	},
	{
		.procname	= "min_slab_ratio",
		.data		= &sysctl_min_slab_ratio,
		.maxlen		= sizeof(sysctl_min_slab_ratio),
		.mode		= 0644,
		.proc_handler	= sysctl_min_slab_ratio_sysctl_handler,
		.extra1		= &zero,
		.extra2		= &one_hundred,
	},
#endif
#ifdef CONFIG_SMP
	{
		.procname	= "stat_interval",
		.data		= &sysctl_stat_interval,
		.maxlen		= sizeof(sysctl_stat_interval),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_jiffies,
	},
#endif
#ifdef CONFIG_MMU
	{
		.procname	= "mmap_min_addr",
		.data		= &dac_mmap_min_addr,
		.maxlen		= sizeof(unsigned long),
		.mode		= 0644,
		.proc_handler	= mmap_min_addr_handler,
	},
#endif
#ifdef CONFIG_NUMA
	{
		.procname	= "numa_zonelist_order",
		.data		= &numa_zonelist_order,
		.maxlen		= NUMA_ZONELIST_ORDER_LEN,
		.mode		= 0644,
		.proc_handler	= numa_zonelist_order_handler,
	},
#endif
#if (defined(CONFIG_X86_32) && !defined(CONFIG_UML))|| \
   (defined(CONFIG_SUPERH) && defined(CONFIG_VSYSCALL))
	{
		.procname	= "vdso_enabled",
#ifdef CONFIG_X86_32
		.data		= &vdso32_enabled,
		.maxlen		= sizeof(vdso32_enabled),
#else
		.data		= &vdso_enabled,
		.maxlen		= sizeof(vdso_enabled),
#endif
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
		.extra1		= &zero,
	},
#endif
#ifdef CONFIG_HIGHMEM
	{
		.procname	= "highmem_is_dirtyable",
		.data		= &vm_highmem_is_dirtyable,
		.maxlen		= sizeof(vm_highmem_is_dirtyable),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif
#ifdef CONFIG_MEMORY_FAILURE
	{
		.procname	= "memory_failure_early_kill",
		.data		= &sysctl_memory_failure_early_kill,
		.maxlen		= sizeof(sysctl_memory_failure_early_kill),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "memory_failure_recovery",
		.data		= &sysctl_memory_failure_recovery,
		.maxlen		= sizeof(sysctl_memory_failure_recovery),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif
	{
		.procname	= "user_reserve_kbytes",
		.data		= &sysctl_user_reserve_kbytes,
		.maxlen		= sizeof(sysctl_user_reserve_kbytes),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
	{
		.procname	= "admin_reserve_kbytes",
		.data		= &sysctl_admin_reserve_kbytes,
		.maxlen		= sizeof(sysctl_admin_reserve_kbytes),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
#ifdef CONFIG_HAVE_ARCH_MMAP_RND_BITS
	{
		.procname	= "mmap_rnd_bits",
		.data		= &mmap_rnd_bits,
		.maxlen		= sizeof(mmap_rnd_bits),
		.mode		= 0600,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= (void)&mmap_rnd_bits_min,
		.extra2		= (void)&mmap_rnd_bits_max,
	},
#endif
#ifdef CONFIG_HAVE_ARCH_MMAP_RND_COMPAT_BITS
	{
		.procname	= "mmap_rnd_compat_bits",
		.data		= &mmap_rnd_compat_bits,
		.maxlen		= sizeof(mmap_rnd_compat_bits),
		.mode		= 0600,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= (void)&mmap_rnd_compat_bits_min,
		.extra2		= (void)&mmap_rnd_compat_bits_max,
	},
#endif
	{ }
};

static struct ctl_table fs_table[] = {
	{
		.procname	= "inode-nr",
		.data		= &inodes_stat,
		.maxlen		= 2*sizeof(long),
		.mode		= 0444,
		.proc_handler	= proc_nr_inodes,
	},
	{
		.procname	= "inode-state",
		.data		= &inodes_stat,
		.maxlen		= 7*sizeof(long),
		.mode		= 0444,
		.proc_handler	= proc_nr_inodes,
	},
	{
		.procname	= "file-nr",
		.data		= &files_stat,
		.maxlen		= sizeof(files_stat),
		.mode		= 0444,
		.proc_handler	= proc_nr_files,
	},
	{
		.procname	= "file-max",
		.data		= &files_stat.max_files,
		.maxlen		= sizeof(files_stat.max_files),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
	{
		.procname	= "nr_open",
		.data		= &sysctl_nr_open,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &sysctl_nr_open_min,
		.extra2		= &sysctl_nr_open_max,
	},
	{
		.procname	= "dentry-state",
		.data		= &dentry_stat,
		.maxlen		= 6*sizeof(long),
		.mode		= 0444,
		.proc_handler	= proc_nr_dentry,
	},
	{
		.procname	= "overflowuid",
		.data		= &fs_overflowuid,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &minolduid,
		.extra2		= &maxolduid,
	},
	{
		.procname	= "overflowgid",
		.data		= &fs_overflowgid,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &minolduid,
		.extra2		= &maxolduid,
	},
#ifdef CONFIG_FILE_LOCKING
	{
		.procname	= "leases-enable",
		.data		= &leases_enable,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_DNOTIFY
	{
		.procname	= "dir-notify-enable",
		.data		= &dir_notify_enable,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_MMU
#ifdef CONFIG_FILE_LOCKING
	{
		.procname	= "lease-break-time",
		.data		= &lease_break_time,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec,
	},
#endif
#ifdef CONFIG_AIO
	{
		.procname	= "aio-nr",
		.data		= &aio_nr,
		.maxlen		= sizeof(aio_nr),
		.mode		= 0444,
		.proc_handler	= proc_doulongvec_minmax,
	},
	{
		.procname	= "aio-max-nr",
		.data		= &aio_max_nr,
		.maxlen		= sizeof(aio_max_nr),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
#endif */ CONFIG_AIO /*
#ifdef CONFIG_INOTIFY_USER
	{
		.procname	= "inotify",
		.mode		= 0555,
		.child		= inotify_table,
	},
#endif	
#ifdef CONFIG_EPOLL
	{
		.procname	= "epoll",
		.mode		= 0555,
		.child		= epoll_table,
	},
#endif
#endif
	{
		.procname	= "protected_symlinks",
		.data		= &sysctl_protected_symlinks,
		.maxlen		= sizeof(int),
		.mode		= 0600,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "protected_hardlinks",
		.data		= &sysctl_protected_hardlinks,
		.maxlen		= sizeof(int),
		.mode		= 0600,
		.proc_handler	= proc_dointvec_minmax,
		.extra1		= &zero,
		.extra2		= &one,
	},
	{
		.procname	= "suid_dumpable",
		.data		= &suid_dumpable,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec_minmax_coredump,
		.extra1		= &zero,
		.extra2		= &two,
	},
#if defined(CONFIG_BINFMT_MISC) || defined(CONFIG_BINFMT_MISC_MODULE)
	{
		.procname	= "binfmt_misc",
		.mode		= 0555,
		.child		= sysctl_mount_point,
	},
#endif
	{
		.procname	= "pipe-max-size",
		.data		= &pipe_max_size,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= &pipe_proc_fn,
		.extra1		= &pipe_min_size,
	},
	{
		.procname	= "pipe-user-pages-hard",
		.data		= &pipe_user_pages_hard,
		.maxlen		= sizeof(pipe_user_pages_hard),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
	{
		.procname	= "pipe-user-pages-soft",
		.data		= &pipe_user_pages_soft,
		.maxlen		= sizeof(pipe_user_pages_soft),
		.mode		= 0644,
		.proc_handler	= proc_doulongvec_minmax,
	},
	{ }
};

static struct ctl_table debug_table[] = {
#ifdef CONFIG_SYSCTL_EXCEPTION_TRACE
	{
		.procname	= "exception-trace",
		.data		= &show_unhandled_signals,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_dointvec
	},
#endif
#if defined(CONFIG_OPTPROBES)
	{
		.procname	= "kprobes-optimization",
		.data		= &sysctl_kprobes_optimization,
		.maxlen		= sizeof(int),
		.mode		= 0644,
		.proc_handler	= proc_kprobes_optimization_handler,
		.extra1		= &zero,
		.extra2		= &one,
	},
#endif
	{ }
};

static struct ctl_table dev_table[] = {
	{ }
};

int __init sysctl_init(void)
{
	struct ctl_table_headerhdr;

	hdr = register_sysctl_table(sysctl_base_table);
	kmemleak_not_leak(hdr);
	return 0;
}

#endif */ CONFIG_SYSCTL /*

*/
 /proc/sys support
 /*

#ifdef CONFIG_PROC_SYSCTL

static int _proc_do_string(chardata, int maxlen, int write,
			   char __userbuffer,
			   size_tlenp, loff_tppos)
{
	size_t len;
	char __userp;
	char c;

	if (!data || !maxlen || !*lenp) {
		*lenp = 0;
		return 0;
	}

	if (write) {
		if (sysctl_writes_strict == SYSCTL_WRITES_STRICT) {
			*/ Only continue writes not past the end of buffer. /*
			len = strlen(data);
			if (len > maxlen - 1)
				len = maxlen - 1;

			if (*ppos > len)
				return 0;
			len =ppos;
		} else {
			*/ Start writing from beginning of buffer. /*
			len = 0;
		}

		*ppos +=lenp;
		p = buffer;
		while ((p - buffer) <lenp && len < maxlen - 1) {
			if (get_user(c, p++))
				return -EFAULT;
			if (c == 0 || c == '\n')
				break;
			data[len++] = c;
		}
		data[len] = 0;
	} else {
		len = strlen(data);
		if (len > maxlen)
			len = maxlen;

		if (*ppos > len) {
			*lenp = 0;
			return 0;
		}

		data +=ppos;
		len  -=ppos;

		if (len >lenp)
			len =lenp;
		if (len)
			if (copy_to_user(buffer, data, len))
				return -EFAULT;
		if (len <lenp) {
			if (put_user('\n', buffer + len))
				return -EFAULT;
			len++;
		}
		*lenp = len;
		*ppos += len;
	}
	return 0;
}

static void warn_sysctl_write(struct ctl_tabletable)
{
	pr_warn_once("%s wrote to %s when file position was not 0!\n"
		"This will not be supported in the future. To silence this\n"
		"warning, set kernel.sysctl_writes_strict = -1\n",
		current->comm, table->procname);
}

*/
 proc_dostring - read a string sysctl
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 Reads/writes a string from/to the user buffer. If the kernel
 buffer provided is not large enough to hold the string, the
 string is truncated. The copied string is %NULL-terminated.
 If the string is being read by the user process, it is copied
 and a newline '\n' is added. It is truncated if the buffer is
 not large enough.

 Returns 0 on success.
 /*
int proc_dostring(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos)
{
	if (write &&ppos && sysctl_writes_strict == SYSCTL_WRITES_WARN)
		warn_sysctl_write(table);

	return _proc_do_string((char)(table->data), table->maxlen, write,
			       (char __user)buffer, lenp, ppos);
}

static size_t proc_skip_spaces(char*buf)
{
	size_t ret;
	chartmp = skip_spaces(*buf);
	ret = tmp -buf;
	*buf = tmp;
	return ret;
}

static void proc_skip_char(char*buf, size_tsize, const char v)
{
	while (*size) {
		if (**buf != v)
			break;
		(*size)--;
		(*buf)++;
	}
}

#define TMPBUFLEN 22
*/
 proc_get_long - reads an ASCII formatted integer from a user buffer

 @buf: a kernel buffer
 @size: size of the kernel buffer
 @val: this is where the number will be stored
 @neg: set to %TRUE if number is negative
 @perm_tr: a vector which contains the allowed trailers
 @perm_tr_len: size of the perm_tr vector
 @tr: pointer to store the trailer character

 In case of success %0 is returned and @buf and @size are updated with
 the amount of bytes read. If @tr is non-NULL and a trailing
 character exists (size is non-zero after returning from this
 function), @tr is updated with the trailing character.
 /*
static int proc_get_long(char*buf, size_tsize,
			  unsigned longval, boolneg,
			  const charperm_tr, unsigned perm_tr_len, chartr)
{
	int len;
	charp, tmp[TMPBUFLEN];

	if (!*size)
		return -EINVAL;

	len =size;
	if (len > TMPBUFLEN - 1)
		len = TMPBUFLEN - 1;

	memcpy(tmp,buf, len);

	tmp[len] = 0;
	p = tmp;
	if (*p == '-' &&size > 1) {
		*neg = true;
		p++;
	} else
		*neg = false;
	if (!isdigit(*p))
		return -EINVAL;

	*val = simple_strtoul(p, &p, 0);

	len = p - tmp;

	*/ We don't know if the next char is whitespace thus we may accept
	 invalid integers (e.g. 1234...a) or two integers instead of one
	 (e.g. 123...1). So lets not allow such large numbers. /*
	if (len == TMPBUFLEN - 1)
		return -EINVAL;

	if (len <size && perm_tr_len && !memchr(perm_tr,p, perm_tr_len))
		return -EINVAL;

	if (tr && (len <size))
		*tr =p;

	*buf += len;
	*size -= len;

	return 0;
}

*/
 proc_put_long - converts an integer to a decimal ASCII formatted string

 @buf: the user buffer
 @size: the size of the user buffer
 @val: the integer to be converted
 @neg: sign of the number, %TRUE for negative

 In case of success %0 is returned and @buf and @size are updated with
 the amount of bytes written.
 /*
static int proc_put_long(void __user*buf, size_tsize, unsigned long val,
			  bool neg)
{
	int len;
	char tmp[TMPBUFLEN],p = tmp;

	sprintf(p, "%s%lu", neg ? "-" : "", val);
	len = strlen(tmp);
	if (len >size)
		len =size;
	if (copy_to_user(*buf, tmp, len))
		return -EFAULT;
	*size -= len;
	*buf += len;
	return 0;
}
#undef TMPBUFLEN

static int proc_put_char(void __user*buf, size_tsize, char c)
{
	if (*size) {
		char __user*buffer = (char __user*)buf;
		if (put_user(c,buffer))
			return -EFAULT;
		(*size)--, (*buffer)++;
		*buf =buffer;
	}
	return 0;
}

static int do_proc_dointvec_conv(boolnegp, unsigned longlvalp,
				 intvalp,
				 int write, voiddata)
{
	if (write) {
		if (*negp) {
			if (*lvalp > (unsigned long) INT_MAX + 1)
				return -EINVAL;
			*valp = -*lvalp;
		} else {
			if (*lvalp > (unsigned long) INT_MAX)
				return -EINVAL;
			*valp =lvalp;
		}
	} else {
		int val =valp;
		if (val < 0) {
			*negp = true;
			*lvalp = -(unsigned long)val;
		} else {
			*negp = false;
			*lvalp = (unsigned long)val;
		}
	}
	return 0;
}

static const char proc_wspace_sep[] = { ' ', '\t', '\n' };

static int __do_proc_dointvec(voidtbl_data, struct ctl_tabletable,
		  int write, void __userbuffer,
		  size_tlenp, loff_tppos,
		  int (*conv)(boolnegp, unsigned longlvalp, intvalp,
			      int write, voiddata),
		  voiddata)
{
	inti, vleft, first = 1, err = 0;
	size_t left;
	charkbuf = NULL,p;
	
	if (!tbl_data || !table->maxlen || !*lenp || (*ppos && !write)) {
		*lenp = 0;
		return 0;
	}
	
	i = (int) tbl_data;
	vleft = table->maxlen / sizeof(*i);
	left =lenp;

	if (!conv)
		conv = do_proc_dointvec_conv;

	if (write) {
		if (*ppos) {
			switch (sysctl_writes_strict) {
			case SYSCTL_WRITES_STRICT:
				goto out;
			case SYSCTL_WRITES_WARN:
				warn_sysctl_write(table);
				break;
			default:
				break;
			}
		}

		if (left > PAGE_SIZE - 1)
			left = PAGE_SIZE - 1;
		p = kbuf = memdup_user_nul(buffer, left);
		if (IS_ERR(kbuf))
			return PTR_ERR(kbuf);
	}

	for (; left && vleft--; i++, first=0) {
		unsigned long lval;
		bool neg;

		if (write) {
			left -= proc_skip_spaces(&p);

			if (!left)
				break;
			err = proc_get_long(&p, &left, &lval, &neg,
					     proc_wspace_sep,
					     sizeof(proc_wspace_sep), NULL);
			if (err)
				break;
			if (conv(&neg, &lval, i, 1, data)) {
				err = -EINVAL;
				break;
			}
		} else {
			if (conv(&neg, &lval, i, 0, data)) {
				err = -EINVAL;
				break;
			}
			if (!first)
				err = proc_put_char(&buffer, &left, '\t');
			if (err)
				break;
			err = proc_put_long(&buffer, &left, lval, neg);
			if (err)
				break;
		}
	}

	if (!write && !first && left && !err)
		err = proc_put_char(&buffer, &left, '\n');
	if (write && !err && left)
		left -= proc_skip_spaces(&p);
	if (write) {
		kfree(kbuf);
		if (first)
			return err ? : -EINVAL;
	}
	*lenp -= left;
out:
	*ppos +=lenp;
	return err;
}

static int do_proc_dointvec(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos,
		  int (*conv)(boolnegp, unsigned longlvalp, intvalp,
			      int write, voiddata),
		  voiddata)
{
	return __do_proc_dointvec(table->data, table, write,
			buffer, lenp, ppos, conv, data);
}

*/
 proc_dointvec - read a vector of integers
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 Reads/writes up to table->maxlen/sizeof(unsigned int) integer
 values from/to the user buffer, treated as an ASCII string. 

 Returns 0 on success.
 /*
int proc_dointvec(struct ctl_tabletable, int write,
		     void __userbuffer, size_tlenp, loff_tppos)
{
    return do_proc_dointvec(table,write,buffer,lenp,ppos,
		    	    NULL,NULL);
}

*/
 Taint values can only be increased
 This means we can safely use a temporary.
 /*
static int proc_taint(struct ctl_tabletable, int write,
			       void __userbuffer, size_tlenp, loff_tppos)
{
	struct ctl_table t;
	unsigned long tmptaint = get_taint();
	int err;

	if (write && !capable(CAP_SYS_ADMIN))
		return -EPERM;

	t =table;
	t.data = &tmptaint;
	err = proc_doulongvec_minmax(&t, write, buffer, lenp, ppos);
	if (err < 0)
		return err;

	if (write) {
		*/
		 Poor man's atomic or. Not worth adding a primitive
		 to everyone's atomic.h for this
		 /*
		int i;
		for (i = 0; i < BITS_PER_LONG && tmptaint >> i; i++) {
			if ((tmptaint >> i) & 1)
				add_taint(i, LOCKDEP_STILL_OK);
		}
	}

	return err;
}

#ifdef CONFIG_PRINTK
static int proc_dointvec_minmax_sysadmin(struct ctl_tabletable, int write,
				void __userbuffer, size_tlenp, loff_tppos)
{
	if (write && !capable(CAP_SYS_ADMIN))
		return -EPERM;

	return proc_dointvec_minmax(table, write, buffer, lenp, ppos);
}
#endif

struct do_proc_dointvec_minmax_conv_param {
	intmin;
	intmax;
};

static int do_proc_dointvec_minmax_conv(boolnegp, unsigned longlvalp,
					intvalp,
					int write, voiddata)
{
	struct do_proc_dointvec_minmax_conv_paramparam = data;
	if (write) {
		int val =negp ? -*lvalp :lvalp;
		if ((param->min &&param->min > val) ||
		    (param->max &&param->max < val))
			return -EINVAL;
		*valp = val;
	} else {
		int val =valp;
		if (val < 0) {
			*negp = true;
			*lvalp = -(unsigned long)val;
		} else {
			*negp = false;
			*lvalp = (unsigned long)val;
		}
	}
	return 0;
}

*/
 proc_dointvec_minmax - read a vector of integers with min/max values
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 Reads/writes up to table->maxlen/sizeof(unsigned int) integer
 values from/to the user buffer, treated as an ASCII string.

 This routine will ensure the values are within the range specified by
 table->extra1 (min) and table->extra2 (max).

 Returns 0 on success.
 /*
int proc_dointvec_minmax(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos)
{
	struct do_proc_dointvec_minmax_conv_param param = {
		.min = (int) table->extra1,
		.max = (int) table->extra2,
	};
	return do_proc_dointvec(table, write, buffer, lenp, ppos,
				do_proc_dointvec_minmax_conv, &param);
}

static void validate_coredump_safety(void)
{
#ifdef CONFIG_COREDUMP
	if (suid_dumpable == SUID_DUMP_ROOT &&
	    core_pattern[0] != '/' && core_pattern[0] != '|') {
		printk(KERN_WARNING "Unsafe core_pattern used with "\
			"suid_dumpable=2. Pipe handler or fully qualified "\
			"core dump path required.\n");
	}
#endif
}

static int proc_dointvec_minmax_coredump(struct ctl_tabletable, int write,
		void __userbuffer, size_tlenp, loff_tppos)
{
	int error = proc_dointvec_minmax(table, write, buffer, lenp, ppos);
	if (!error)
		validate_coredump_safety();
	return error;
}

#ifdef CONFIG_COREDUMP
static int proc_dostring_coredump(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos)
{
	int error = proc_dostring(table, write, buffer, lenp, ppos);
	if (!error)
		validate_coredump_safety();
	return error;
}
#endif

static int __do_proc_doulongvec_minmax(voiddata, struct ctl_tabletable, int write,
				     void __userbuffer,
				     size_tlenp, loff_tppos,
				     unsigned long convmul,
				     unsigned long convdiv)
{
	unsigned longi,min,max;
	int vleft, first = 1, err = 0;
	size_t left;
	charkbuf = NULL,p;

	if (!data || !table->maxlen || !*lenp || (*ppos && !write)) {
		*lenp = 0;
		return 0;
	}

	i = (unsigned long) data;
	min = (unsigned long) table->extra1;
	max = (unsigned long) table->extra2;
	vleft = table->maxlen / sizeof(unsigned long);
	left =lenp;

	if (write) {
		if (*ppos) {
			switch (sysctl_writes_strict) {
			case SYSCTL_WRITES_STRICT:
				goto out;
			case SYSCTL_WRITES_WARN:
				warn_sysctl_write(table);
				break;
			default:
				break;
			}
		}

		if (left > PAGE_SIZE - 1)
			left = PAGE_SIZE - 1;
		p = kbuf = memdup_user_nul(buffer, left);
		if (IS_ERR(kbuf))
			return PTR_ERR(kbuf);
	}

	for (; left && vleft--; i++, first = 0) {
		unsigned long val;

		if (write) {
			bool neg;

			left -= proc_skip_spaces(&p);

			err = proc_get_long(&p, &left, &val, &neg,
					     proc_wspace_sep,
					     sizeof(proc_wspace_sep), NULL);
			if (err)
				break;
			if (neg)
				continue;
			if ((min && val <min) || (max && val >max))
				continue;
			*i = val;
		} else {
			val = convdiv (*i) / convmul;
			if (!first) {
				err = proc_put_char(&buffer, &left, '\t');
				if (err)
					break;
			}
			err = proc_put_long(&buffer, &left, val, false);
			if (err)
				break;
		}
	}

	if (!write && !first && left && !err)
		err = proc_put_char(&buffer, &left, '\n');
	if (write && !err)
		left -= proc_skip_spaces(&p);
	if (write) {
		kfree(kbuf);
		if (first)
			return err ? : -EINVAL;
	}
	*lenp -= left;
out:
	*ppos +=lenp;
	return err;
}

static int do_proc_doulongvec_minmax(struct ctl_tabletable, int write,
				     void __userbuffer,
				     size_tlenp, loff_tppos,
				     unsigned long convmul,
				     unsigned long convdiv)
{
	return __do_proc_doulongvec_minmax(table->data, table, write,
			buffer, lenp, ppos, convmul, convdiv);
}

*/
 proc_doulongvec_minmax - read a vector of long integers with min/max values
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 Reads/writes up to table->maxlen/sizeof(unsigned long) unsigned long
 values from/to the user buffer, treated as an ASCII string.

 This routine will ensure the values are within the range specified by
 table->extra1 (min) and table->extra2 (max).

 Returns 0 on success.
 /*
int proc_doulongvec_minmax(struct ctl_tabletable, int write,
			   void __userbuffer, size_tlenp, loff_tppos)
{
    return do_proc_doulongvec_minmax(table, write, buffer, lenp, ppos, 1l, 1l);
}

*/
 proc_doulongvec_ms_jiffies_minmax - read a vector of millisecond values with min/max values
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 Reads/writes up to table->maxlen/sizeof(unsigned long) unsigned long
 values from/to the user buffer, treated as an ASCII string. The values
 are treated as milliseconds, and converted to jiffies when they are stored.

 This routine will ensure the values are within the range specified by
 table->extra1 (min) and table->extra2 (max).

 Returns 0 on success.
 /*
int proc_doulongvec_ms_jiffies_minmax(struct ctl_tabletable, int write,
				      void __userbuffer,
				      size_tlenp, loff_tppos)
{
    return do_proc_doulongvec_minmax(table, write, buffer,
				     lenp, ppos, HZ, 1000l);
}


static int do_proc_dointvec_jiffies_conv(boolnegp, unsigned longlvalp,
					 intvalp,
					 int write, voiddata)
{
	if (write) {
		if (*lvalp > LONG_MAX / HZ)
			return 1;
		*valp =negp ? -(*lvalp*HZ) : (*lvalp*HZ);
	} else {
		int val =valp;
		unsigned long lval;
		if (val < 0) {
			*negp = true;
			lval = -(unsigned long)val;
		} else {
			*negp = false;
			lval = (unsigned long)val;
		}
		*lvalp = lval / HZ;
	}
	return 0;
}

static int do_proc_dointvec_userhz_jiffies_conv(boolnegp, unsigned longlvalp,
						intvalp,
						int write, voiddata)
{
	if (write) {
		if (USER_HZ < HZ &&lvalp > (LONG_MAX / HZ) USER_HZ)
			return 1;
		*valp = clock_t_to_jiffies(*negp ? -*lvalp :lvalp);
	} else {
		int val =valp;
		unsigned long lval;
		if (val < 0) {
			*negp = true;
			lval = -(unsigned long)val;
		} else {
			*negp = false;
			lval = (unsigned long)val;
		}
		*lvalp = jiffies_to_clock_t(lval);
	}
	return 0;
}

static int do_proc_dointvec_ms_jiffies_conv(boolnegp, unsigned longlvalp,
					    intvalp,
					    int write, voiddata)
{
	if (write) {
		unsigned long jif = msecs_to_jiffies(*negp ? -*lvalp :lvalp);

		if (jif > INT_MAX)
			return 1;
		*valp = (int)jif;
	} else {
		int val =valp;
		unsigned long lval;
		if (val < 0) {
			*negp = true;
			lval = -(unsigned long)val;
		} else {
			*negp = false;
			lval = (unsigned long)val;
		}
		*lvalp = jiffies_to_msecs(lval);
	}
	return 0;
}

*/
 proc_dointvec_jiffies - read a vector of integers as seconds
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 Reads/writes up to table->maxlen/sizeof(unsigned int) integer
 values from/to the user buffer, treated as an ASCII string. 
 The values read are assumed to be in seconds, and are converted into
 jiffies.

 Returns 0 on success.
 /*
int proc_dointvec_jiffies(struct ctl_tabletable, int write,
			  void __userbuffer, size_tlenp, loff_tppos)
{
    return do_proc_dointvec(table,write,buffer,lenp,ppos,
		    	    do_proc_dointvec_jiffies_conv,NULL);
}

*/
 proc_dointvec_userhz_jiffies - read a vector of integers as 1/USER_HZ seconds
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: pointer to the file position

 Reads/writes up to table->maxlen/sizeof(unsigned int) integer
 values from/to the user buffer, treated as an ASCII string. 
 The values read are assumed to be in 1/USER_HZ seconds, and 
 are converted into jiffies.

 Returns 0 on success.
 /*
int proc_dointvec_userhz_jiffies(struct ctl_tabletable, int write,
				 void __userbuffer, size_tlenp, loff_tppos)
{
    return do_proc_dointvec(table,write,buffer,lenp,ppos,
		    	    do_proc_dointvec_userhz_jiffies_conv,NULL);
}

*/
 proc_dointvec_ms_jiffies - read a vector of integers as 1 milliseconds
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position
 @ppos: the current position in the file

 Reads/writes up to table->maxlen/sizeof(unsigned int) integer
 values from/to the user buffer, treated as an ASCII string. 
 The values read are assumed to be in 1/1000 seconds, and 
 are converted into jiffies.

 Returns 0 on success.
 /*
int proc_dointvec_ms_jiffies(struct ctl_tabletable, int write,
			     void __userbuffer, size_tlenp, loff_tppos)
{
	return do_proc_dointvec(table, write, buffer, lenp, ppos,
				do_proc_dointvec_ms_jiffies_conv, NULL);
}

static int proc_do_cad_pid(struct ctl_tabletable, int write,
			   void __userbuffer, size_tlenp, loff_tppos)
{
	struct pidnew_pid;
	pid_t tmp;
	int r;

	tmp = pid_vnr(cad_pid);

	r = __do_proc_dointvec(&tmp, table, write, buffer,
			       lenp, ppos, NULL, NULL);
	if (r || !write)
		return r;

	new_pid = find_get_pid(tmp);
	if (!new_pid)
		return -ESRCH;

	put_pid(xchg(&cad_pid, new_pid));
	return 0;
}

*/
 proc_do_large_bitmap - read/write from/to a large bitmap
 @table: the sysctl table
 @write: %TRUE if this is a write to the sysctl file
 @buffer: the user buffer
 @lenp: the size of the user buffer
 @ppos: file position

 The bitmap is stored at table->data and the bitmap length (in bits)
 in table->maxlen.

 We use a range comma separated format (e.g. 1,3-4,10-10) so that
 large bitmaps may be represented in a compact manner. Writing into
 the file will clear the bitmap then update it with the given input.

 Returns 0 on success.
 /*
int proc_do_large_bitmap(struct ctl_tabletable, int write,
			 void __userbuffer, size_tlenp, loff_tppos)
{
	int err = 0;
	bool first = 1;
	size_t left =lenp;
	unsigned long bitmap_len = table->maxlen;
	unsigned longbitmap =(unsigned long*) table->data;
	unsigned longtmp_bitmap = NULL;
	char tr_a[] = { '-', ',', '\n' }, tr_b[] = { ',', '\n', 0 }, c;

	if (!bitmap || !bitmap_len || !left || (*ppos && !write)) {
		*lenp = 0;
		return 0;
	}

	if (write) {
		charkbuf,p;

		if (left > PAGE_SIZE - 1)
			left = PAGE_SIZE - 1;

		p = kbuf = memdup_user_nul(buffer, left);
		if (IS_ERR(kbuf))
			return PTR_ERR(kbuf);

		tmp_bitmap = kzalloc(BITS_TO_LONGS(bitmap_len) sizeof(unsigned long),
				     GFP_KERNEL);
		if (!tmp_bitmap) {
			kfree(kbuf);
			return -ENOMEM;
		}
		proc_skip_char(&p, &left, '\n');
		while (!err && left) {
			unsigned long val_a, val_b;
			bool neg;

			err = proc_get_long(&p, &left, &val_a, &neg, tr_a,
					     sizeof(tr_a), &c);
			if (err)
				break;
			if (val_a >= bitmap_len || neg) {
				err = -EINVAL;
				break;
			}

			val_b = val_a;
			if (left) {
				p++;
				left--;
			}

			if (c == '-') {
				err = proc_get_long(&p, &left, &val_b,
						     &neg, tr_b, sizeof(tr_b),
						     &c);
				if (err)
					break;
				if (val_b >= bitmap_len || neg ||
				    val_a > val_b) {
					err = -EINVAL;
					break;
				}
				if (left) {
					p++;
					left--;
				}
			}

			bitmap_set(tmp_bitmap, val_a, val_b - val_a + 1);
			first = 0;
			proc_skip_char(&p, &left, '\n');
		}
		kfree(kbuf);
	} else {
		unsigned long bit_a, bit_b = 0;

		while (left) {
			bit_a = find_next_bit(bitmap, bitmap_len, bit_b);
			if (bit_a >= bitmap_len)
				break;
			bit_b = find_next_zero_bit(bitmap, bitmap_len,
						   bit_a + 1) - 1;

			if (!first) {
				err = proc_put_char(&buffer, &left, ',');
				if (err)
					break;
			}
			err = proc_put_long(&buffer, &left, bit_a, false);
			if (err)
				break;
			if (bit_a != bit_b) {
				err = proc_put_char(&buffer, &left, '-');
				if (err)
					break;
				err = proc_put_long(&buffer, &left, bit_b, false);
				if (err)
					break;
			}

			first = 0; bit_b++;
		}
		if (!err)
			err = proc_put_char(&buffer, &left, '\n');
	}

	if (!err) {
		if (write) {
			if (*ppos)
				bitmap_or(bitmap, bitmap, tmp_bitmap, bitmap_len);
			else
				bitmap_copy(bitmap, tmp_bitmap, bitmap_len);
		}
		kfree(tmp_bitmap);
		*lenp -= left;
		*ppos +=lenp;
		return 0;
	} else {
		kfree(tmp_bitmap);
		return err;
	}
}

#else*/ CONFIG_PROC_SYSCTL /*

int proc_dostring(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_dointvec(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_dointvec_minmax(struct ctl_tabletable, int write,
		    void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_dointvec_jiffies(struct ctl_tabletable, int write,
		    void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_dointvec_userhz_jiffies(struct ctl_tabletable, int write,
		    void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_dointvec_ms_jiffies(struct ctl_tabletable, int write,
			     void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_doulongvec_minmax(struct ctl_tabletable, int write,
		    void __userbuffer, size_tlenp, loff_tppos)
{
	return -ENOSYS;
}

int proc_doulongvec_ms_jiffies_minmax(struct ctl_tabletable, int write,
				      void __userbuffer,
				      size_tlenp, loff_tppos)
{
    return -ENOSYS;
}


#endif */ CONFIG_PROC_SYSCTL /*

*/
 No sense putting this after each symbol definition, twice,
 exception granted :-)
 /*
EXPORT_SYMBOL(proc_dointvec);
EXPORT_SYMBOL(proc_dointvec_jiffies);
EXPORT_SYMBOL(proc_dointvec_minmax);
EXPORT_SYMBOL(proc_dointvec_userhz_jiffies);
EXPORT_SYMBOL(proc_dointvec_ms_jiffies);
EXPORT_SYMBOL(proc_dostring);
EXPORT_SYMBOL(proc_doulongvec_minmax);
EXPORT_SYMBOL(proc_doulongvec_ms_jiffies_minmax);
*/
/*
#include <linux/linkage.h>
#include <linux/errno.h>

#include <asm/unistd.h>

*/  we can't #include <linux/syscalls.h> here,
    but tell gcc to not warn with -Wmissing-prototypes  /*
asmlinkage long sys_ni_syscall(void);

*/
 Non-implemented system calls get redirected here.
 /*
asmlinkage long sys_ni_syscall(void)
{
	return -ENOSYS;
}

cond_syscall(sys_quotactl);
cond_syscall(sys32_quotactl);
cond_syscall(sys_acct);
cond_syscall(sys_lookup_dcookie);
cond_syscall(compat_sys_lookup_dcookie);
cond_syscall(sys_swapon);
cond_syscall(sys_swapoff);
cond_syscall(sys_kexec_load);
cond_syscall(compat_sys_kexec_load);
cond_syscall(sys_kexec_file_load);
cond_syscall(sys_init_module);
cond_syscall(sys_finit_module);
cond_syscall(sys_delete_module);
cond_syscall(sys_socketpair);
cond_syscall(sys_bind);
cond_syscall(sys_listen);
cond_syscall(sys_accept);
cond_syscall(sys_accept4);
cond_syscall(sys_connect);
cond_syscall(sys_getsockname);
cond_syscall(sys_getpeername);
cond_syscall(sys_sendto);
cond_syscall(sys_send);
cond_syscall(sys_recvfrom);
cond_syscall(sys_recv);
cond_syscall(sys_socket);
cond_syscall(sys_setsockopt);
cond_syscall(compat_sys_setsockopt);
cond_syscall(sys_getsockopt);
cond_syscall(compat_sys_getsockopt);
cond_syscall(sys_shutdown);
cond_syscall(sys_sendmsg);
cond_syscall(sys_sendmmsg);
cond_syscall(compat_sys_sendmsg);
cond_syscall(compat_sys_sendmmsg);
cond_syscall(sys_recvmsg);
cond_syscall(sys_recvmmsg);
cond_syscall(compat_sys_recvmsg);
cond_syscall(compat_sys_recv);
cond_syscall(compat_sys_recvfrom);
cond_syscall(compat_sys_recvmmsg);
cond_syscall(sys_socketcall);
cond_syscall(sys_futex);
cond_syscall(compat_sys_futex);
cond_syscall(sys_set_robust_list);
cond_syscall(compat_sys_set_robust_list);
cond_syscall(sys_get_robust_list);
cond_syscall(compat_sys_get_robust_list);
cond_syscall(sys_epoll_create);
cond_syscall(sys_epoll_create1);
cond_syscall(sys_epoll_ctl);
cond_syscall(sys_epoll_wait);
cond_syscall(sys_epoll_pwait);
cond_syscall(compat_sys_epoll_pwait);
cond_syscall(sys_semget);
cond_syscall(sys_semop);
cond_syscall(sys_semtimedop);
cond_syscall(compat_sys_semtimedop);
cond_syscall(sys_semctl);
cond_syscall(compat_sys_semctl);
cond_syscall(sys_msgget);
cond_syscall(sys_msgsnd);
cond_syscall(compat_sys_msgsnd);
cond_syscall(sys_msgrcv);
cond_syscall(compat_sys_msgrcv);
cond_syscall(sys_msgctl);
cond_syscall(compat_sys_msgctl);
cond_syscall(sys_shmget);
cond_syscall(sys_shmat);
cond_syscall(compat_sys_shmat);
cond_syscall(sys_shmdt);
cond_syscall(sys_shmctl);
cond_syscall(compat_sys_shmctl);
cond_syscall(sys_mq_open);
cond_syscall(sys_mq_unlink);
cond_syscall(sys_mq_timedsend);
cond_syscall(sys_mq_timedreceive);
cond_syscall(sys_mq_notify);
cond_syscall(sys_mq_getsetattr);
cond_syscall(compat_sys_mq_open);
cond_syscall(compat_sys_mq_timedsend);
cond_syscall(compat_sys_mq_timedreceive);
cond_syscall(compat_sys_mq_notify);
cond_syscall(compat_sys_mq_getsetattr);
cond_syscall(sys_mbind);
cond_syscall(sys_get_mempolicy);
cond_syscall(sys_set_mempolicy);
cond_syscall(compat_sys_mbind);
cond_syscall(compat_sys_get_mempolicy);
cond_syscall(compat_sys_set_mempolicy);
cond_syscall(sys_add_key);
cond_syscall(sys_request_key);
cond_syscall(sys_keyctl);
cond_syscall(compat_sys_keyctl);
cond_syscall(compat_sys_socketcall);
cond_syscall(sys_inotify_init);
cond_syscall(sys_inotify_init1);
cond_syscall(sys_inotify_add_watch);
cond_syscall(sys_inotify_rm_watch);
cond_syscall(sys_migrate_pages);
cond_syscall(sys_move_pages);
cond_syscall(sys_chown16);
cond_syscall(sys_fchown16);
cond_syscall(sys_getegid16);
cond_syscall(sys_geteuid16);
cond_syscall(sys_getgid16);
cond_syscall(sys_getgroups16);
cond_syscall(sys_getresgid16);
cond_syscall(sys_getresuid16);
cond_syscall(sys_getuid16);
cond_syscall(sys_lchown16);
cond_syscall(sys_setfsgid16);
cond_syscall(sys_setfsuid16);
cond_syscall(sys_setgid16);
cond_syscall(sys_setgroups16);
cond_syscall(sys_setregid16);
cond_syscall(sys_setresgid16);
cond_syscall(sys_setresuid16);
cond_syscall(sys_setreuid16);
cond_syscall(sys_setuid16);
cond_syscall(sys_sgetmask);
cond_syscall(sys_ssetmask);
cond_syscall(sys_vm86old);
cond_syscall(sys_vm86);
cond_syscall(sys_modify_ldt);
cond_syscall(sys_ipc);
cond_syscall(compat_sys_ipc);
cond_syscall(compat_sys_sysctl);
cond_syscall(sys_flock);
cond_syscall(sys_io_setup);
cond_syscall(sys_io_destroy);
cond_syscall(sys_io_submit);
cond_syscall(sys_io_cancel);
cond_syscall(sys_io_getevents);
cond_syscall(sys_sysfs);
cond_syscall(sys_syslog);
cond_syscall(sys_process_vm_readv);
cond_syscall(sys_process_vm_writev);
cond_syscall(compat_sys_process_vm_readv);
cond_syscall(compat_sys_process_vm_writev);
cond_syscall(sys_uselib);
cond_syscall(sys_fadvise64);
cond_syscall(sys_fadvise64_64);
cond_syscall(sys_madvise);
cond_syscall(sys_setuid);
cond_syscall(sys_setregid);
cond_syscall(sys_setgid);
cond_syscall(sys_setreuid);
cond_syscall(sys_setresuid);
cond_syscall(sys_getresuid);
cond_syscall(sys_setresgid);
cond_syscall(sys_getresgid);
cond_syscall(sys_setgroups);
cond_syscall(sys_getgroups);
cond_syscall(sys_setfsuid);
cond_syscall(sys_setfsgid);
cond_syscall(sys_capget);
cond_syscall(sys_capset);
cond_syscall(sys_copy_file_range);

*/ arch-specific weak syscall entries /*
cond_syscall(sys_pciconfig_read);
cond_syscall(sys_pciconfig_write);
cond_syscall(sys_pciconfig_iobase);
cond_syscall(compat_sys_s390_ipc);
cond_syscall(ppc_rtas);
cond_syscall(sys_spu_run);
cond_syscall(sys_spu_create);
cond_syscall(sys_subpage_prot);
cond_syscall(sys_s390_pci_mmio_read);
cond_syscall(sys_s390_pci_mmio_write);

*/ mmu depending weak syscall entries /*
cond_syscall(sys_mprotect);
cond_syscall(sys_msync);
cond_syscall(sys_mlock);
cond_syscall(sys_munlock);
cond_syscall(sys_mlockall);
cond_syscall(sys_munlockall);
cond_syscall(sys_mlock2);
cond_syscall(sys_mincore);
cond_syscall(sys_madvise);
cond_syscall(sys_mremap);
cond_syscall(sys_remap_file_pages);
cond_syscall(compat_sys_move_pages);
cond_syscall(compat_sys_migrate_pages);

*/ block-layer dependent /*
cond_syscall(sys_bdflush);
cond_syscall(sys_ioprio_set);
cond_syscall(sys_ioprio_get);

*/ New file descriptors /*
cond_syscall(sys_signalfd);
cond_syscall(sys_signalfd4);
cond_syscall(compat_sys_signalfd);
cond_syscall(compat_sys_signalfd4);
cond_syscall(sys_timerfd_create);
cond_syscall(sys_timerfd_settime);
cond_syscall(sys_timerfd_gettime);
cond_syscall(compat_sys_timerfd_settime);
cond_syscall(compat_sys_timerfd_gettime);
cond_syscall(sys_eventfd);
cond_syscall(sys_eventfd2);
cond_syscall(sys_memfd_create);
cond_syscall(sys_userfaultfd);

*/ performance counters: /*
cond_syscall(sys_perf_event_open);

*/ fanotify! /*
cond_syscall(sys_fanotify_init);
cond_syscall(sys_fanotify_mark);
cond_syscall(compat_sys_fanotify_mark);

*/ open by handle /*
cond_syscall(sys_name_to_handle_at);
cond_syscall(sys_open_by_handle_at);
cond_syscall(compat_sys_open_by_handle_at);

*/ compare kernel pointers /*
cond_syscall(sys_kcmp);

*/ operate on Secure Computing state /*
cond_syscall(sys_seccomp);

*/ access BPF programs and maps /*
cond_syscall(sys_bpf);

*/ execveat /*
cond_syscall(sys_execveat);

*/ membarrier /*
cond_syscall(sys_membarrier);
*/
