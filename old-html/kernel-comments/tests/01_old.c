
  linux/kernel/acct.c

  BSD Process Accounting for Linux

  Author: Marco van Wieringen &lt;mvw@planets.elm.net&gt;

  Some code based on ideas and code from:
  Thomas K. Dyas &lt;tdyas@eden.rutgers.edu&gt;

  This file implements BSD-style process accounting. Whenever any
  process exits, an accounting record of type "struct acct" is
  written to the file specified with the acct() system call. It is
  up to user-level programs to do useful things with the accounting
  log. The kernel just provides the raw accounting information.

 (C) Copyright 1995 - 1997 Marco van Wieringen - ELM Consultancy B.V.

  Plugged two leaks. 1) It didn't return acct_file into the free_filps if
  the file happened to be read-only. 2) If the accounting was suspended
  due to the lack of space it happily allowed to reopen it and completely
  lost the old acct_file. 3/10/98, Al Viro.

  Now we silently close acct_file on attempt to reopen. Cleaned sys_acct().
  XTerms and EMACS are manifestations of pure evil. 21/10/98, AV.

  Fixed a nasty interaction with with sys_umount(). If the accointing
  was suspeneded we failed to stop it on umount(). Messy.
  Another one: remount to readonly didn't stop accounting.
	Question: what should we do if we have CAP_SYS_ADMIN but not
  CAP_SYS_PACCT? Current code does the following: umount returns -EBUSY
  unless we are messing with the root. In that case we are getting a
  real mess with do_remount_sb(). 9/11/98, AV.

  Fixed a bunch of races (and pair of leaks). Probably not the best way,
  but this one obviously doesn't introduce deadlocks. Later. BTW, found
  one race (and leak) in BSD implementation.
  OK, that's better. ANOTHER race and leak in BSD variant. There always
  is one more bug... 10/11/98, AV.

	Oh, fsck... Oopsable SMP race in do_process_acct() - we must hold
 -&gt;mmap_sem to walk the vma list of current-&gt;mm. Nasty, since it leaks
 a struct file opened for write. Fixed. 2/6/2000, AV.
 /*

#include &lt;linux/mm.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/acct.h&gt;
#include &lt;linux/capability.h&gt;
#include &lt;linux/file.h&gt;
#include &lt;linux/tty.h&gt;
#include &lt;linux/security.h&gt;
#include &lt;linux/vfs.h&gt;
#include &lt;linux/jiffies.h&gt;
#include &lt;linux/times.h&gt;
#include &lt;linux/syscalls.h&gt;
#include &lt;linux/mount.h&gt;
#include &lt;linux/uaccess.h&gt;
#include &lt;asm/div64.h&gt;
#include &lt;linux/blkdev.h&gt;*/ sector_div /*
#include &lt;linux/pid_namespace.h&gt;
#include &lt;linux/fs_pin.h&gt;

*/
 These constants control the amount of freespace that suspend and
 resume the process accounting system, and the time delay between
 each check.
 Turned into sysctl-controllable parameters. AV, 12/11/98
 /*

int acct_parm[3] = {4, 2, 30};
#define RESUME		(acct_parm[0])	*/ &gt;foo% free space - resume /*
#define SUSPEND		(acct_parm[1])	*/ &lt;foo% free space - suspend /*
#define ACCT_TIMEOUT	(acct_parm[2])	*/ foo second timeout between checks 

 External references and all of the globals.
 /*

struct bsd_acct_struct {
	struct fs_pin		pin;
	atomic_long_t		count;
	struct rcu_head		rcu;
	struct mutex		lock;
	int			active;
	unsigned long		needcheck;
	struct file		*file;
	struct pid_namespace	*ns;
	struct work_struct	work;
	struct completion	done;
};

static void do_acct_process(struct bsd_acct_structacct);

*/
 Check the amount of free space and suspend/resume accordingly.
 /*
static int check_free_space(struct bsd_acct_structacct)
{
	struct kstatfs sbuf;

	if (time_is_before_jiffies(acct-&gt;needcheck))
		goto out;

	*/ May block /*
	if (vfs_statfs(&amp;amp;acct-&gt;file-&gt;f_path, &amp;amp;sbuf))
		goto out;

	if (acct-&gt;active) {
		u64 suspend = sbuf.f_blocks SUSPEND;
		do_div(suspend, 100);
		if (sbuf.f_bavail &lt;= suspend) {
			acct-&gt;active = 0;
			pr_info("Process accounting paused\n");
		}
	} else {
		u64 resume = sbuf.f_blocks RESUME;
		do_div(resume, 100);
		if (sbuf.f_bavail &gt;= resume) {
			acct-&gt;active = 1;
			pr_info("Process accounting resumed\n");
		}
	}

	acct-&gt;needcheck = jiffies + ACCT_TIMEOUT*HZ;
out:
	return acct-&gt;active;
}

static void acct_put(struct bsd_acct_structp)
{
	if (atomic_long_dec_and_test(&amp;amp;p-&gt;count))
		kfree_rcu(p, rcu);
}

static inline struct bsd_acct_structto_acct(struct fs_pinp)
{
	return p ? container_of(p, struct bsd_acct_struct, pin) : NULL;
}

static struct bsd_acct_structacct_get(struct pid_namespacens)
{
	struct bsd_acct_structres;
again:
	smp_rmb();
	rcu_read_lock();
	res = to_acct(ACCESS_ONCE(ns-&gt;bacct));
	if (!res) {
		rcu_read_unlock();
		return NULL;
	}
	if (!atomic_long_inc_not_zero(&amp;amp;res-&gt;count)) {
		rcu_read_unlock();
		cpu_relax();
		goto again;
	}
	rcu_read_unlock();
	mutex_lock(&amp;amp;res-&gt;lock);
	if (res != to_acct(ACCESS_ONCE(ns-&gt;bacct))) {
		mutex_unlock(&amp;amp;res-&gt;lock);
		acct_put(res);
		goto again;
	}
	return res;
}

static void acct_pin_kill(struct fs_pinpin)
{
	struct bsd_acct_structacct = to_acct(pin);
	mutex_lock(&amp;amp;acct-&gt;lock);
	do_acct_process(acct);
	schedule_work(&amp;amp;acct-&gt;work);
	wait_for_completion(&amp;amp;acct-&gt;done);
	cmpxchg(&amp;amp;acct-&gt;ns-&gt;bacct, pin, NULL);
	mutex_unlock(&amp;amp;acct-&gt;lock);
	pin_remove(pin);
	acct_put(acct);
}

static void close_work(struct work_structwork)
{
	struct bsd_acct_structacct = container_of(work, struct bsd_acct_struct, work);
	struct filefile = acct-&gt;file;
	if (file-&gt;f_op-&gt;flush)
		file-&gt;f_op-&gt;flush(file, NULL);
	__fput_sync(file);
	complete(&amp;amp;acct-&gt;done);
}

static int acct_on(struct filenamepathname)
{
	struct filefile;
	struct vfsmountmnt,internal;
	struct pid_namespacens = task_active_pid_ns(current);
	struct bsd_acct_structacct;
	struct fs_pinold;
	int err;

	acct = kzalloc(sizeof(struct bsd_acct_struct), GFP_KERNEL);
	if (!acct)
		return -ENOMEM;

	*/ Difference from BSD - they don't do O_APPEND /*
	file = file_open_name(pathname, O_WRONLY|O_APPEND|O_LARGEFILE, 0);
	if (IS_ERR(file)) {
		kfree(acct);
		return PTR_ERR(file);
	}

	if (!S_ISREG(file_inode(file)-&gt;i_mode)) {
		kfree(acct);
		filp_close(file, NULL);
		return -EACCES;
	}

	if (!(file-&gt;f_mode &amp;amp; FMODE_CAN_WRITE)) {
		kfree(acct);
		filp_close(file, NULL);
		return -EIO;
	}
	internal = mnt_clone_internal(&amp;amp;file-&gt;f_path);
	if (IS_ERR(internal)) {
		kfree(acct);
		filp_close(file, NULL);
		return PTR_ERR(internal);
	}
	err = mnt_want_write(internal);
	if (err) {
		mntput(internal);
		kfree(acct);
		filp_close(file, NULL);
		return err;
	}
	mnt = file-&gt;f_path.mnt;
	file-&gt;f_path.mnt = internal;

	atomic_long_set(&amp;amp;acct-&gt;count, 1);
	init_fs_pin(&amp;amp;acct-&gt;pin, acct_pin_kill);
	acct-&gt;file = file;
	acct-&gt;needcheck = jiffies;
	acct-&gt;ns = ns;
	mutex_init(&amp;amp;acct-&gt;lock);
	INIT_WORK(&amp;amp;acct-&gt;work, close_work);
	init_completion(&amp;amp;acct-&gt;done);
	mutex_lock_nested(&amp;amp;acct-&gt;lock, 1);	*/ nobody has seen it yet /*
	pin_insert(&amp;amp;acct-&gt;pin, mnt);

	rcu_read_lock();
	old = xchg(&amp;amp;ns-&gt;bacct, &amp;amp;acct-&gt;pin);
	mutex_unlock(&amp;amp;acct-&gt;lock);
	pin_kill(old);
	mnt_drop_write(mnt);
	mntput(mnt);
	return 0;
}

static DEFINE_MUTEX(acct_on_mutex);

*/
 sys_acct - enable/disable process accounting
 @name: file name for accounting records or NULL to shutdown accounting

 Returns 0 for success or negative errno values for failure.

 sys_acct() is the only system call needed to implement process
 accounting. It takes the name of the file where accounting records
 should be written. If the filename is NULL, accounting will be
 shutdown.
 /*
SYSCALL_DEFINE1(acct, const char __user, name)
{
	int error = 0;

	if (!capable(CAP_SYS_PACCT))
		return -EPERM;

	if (name) {
		struct filenametmp = getname(name);

		if (IS_ERR(tmp))
			return PTR_ERR(tmp);
		mutex_lock(&amp;amp;acct_on_mutex);
		error = acct_on(tmp);
		mutex_unlock(&amp;amp;acct_on_mutex);
		putname(tmp);
	} else {
		rcu_read_lock();
		pin_kill(task_active_pid_ns(current)-&gt;bacct);
	}

	return error;
}

void acct_exit_ns(struct pid_namespacens)
{
	rcu_read_lock();
	pin_kill(ns-&gt;bacct);
}

*/
  encode an unsigned long into a comp_t

  This routine has been adopted from the encode_comp_t() function in
  the kern_acct.c file of the FreeBSD operating system. The encoding
  is a 13-bit fraction with a 3-bit (base 8) exponent.
 /*

#define	MANTSIZE	13			*/ 13 bit mantissa. /*
#define	EXPSIZE		3			*/ Base 8 (3 bit) exponent. /*
#define	MAXFRACT	((1 &lt;&lt; MANTSIZE) - 1)	*/ Maximum fractional value. /*

static comp_t encode_comp_t(unsigned long value)
{
	int exp, rnd;

	exp = rnd = 0;
	while (value &gt; MAXFRACT) {
		rnd = value &amp;amp; (1 &lt;&lt; (EXPSIZE - 1));	*/ Round up? /*
		value &gt;&gt;= EXPSIZE;	*/ Base 8 exponent == 3 bit shift. /*
		exp++;
	}

	*/
	 If we need to round up, do it (and handle overflow correctly).
	 /*
	if (rnd &amp;amp;&amp;amp; (++value &gt; MAXFRACT)) {
		value &gt;&gt;= EXPSIZE;
		exp++;
	}

	*/
	 Clean it up and polish it off.
	 /*
	exp &lt;&lt;= MANTSIZE;		*/ Shift the exponent into place /*
	exp += value;			*/ and add on the mantissa. /*
	return exp;
}

#if ACCT_VERSION == 1 || ACCT_VERSION == 2
*/
 encode an u64 into a comp2_t (24 bits)

 Format: 5 bit base 2 exponent, 20 bits mantissa.
 The leading bit of the mantissa is not stored, but implied for
 non-zero exponents.
 Largest encodable value is 50 bits.
 /*

#define MANTSIZE2       20                     / 20 bit mantissa. /*
#define EXPSIZE2        5                      / 5 bit base 2 exponent. /*
#define MAXFRACT2       ((1ul &lt;&lt; MANTSIZE2) - 1)/ Maximum fractional value. /*
#define MAXEXP2         ((1 &lt;&lt; EXPSIZE2) - 1)   / Maximum exponent. /*

static comp2_t encode_comp2_t(u64 value)
{
	int exp, rnd;

	exp = (value &gt; (MAXFRACT2&gt;&gt;1));
	rnd = 0;
	while (value &gt; MAXFRACT2) {
		rnd = value &amp;amp; 1;
		value &gt;&gt;= 1;
		exp++;
	}

	*/
	 If we need to round up, do it (and handle overflow correctly).
	 /*
	if (rnd &amp;amp;&amp;amp; (++value &gt; MAXFRACT2)) {
		value &gt;&gt;= 1;
		exp++;
	}

	if (exp &gt; MAXEXP2) {
		*/ Overflow. Return largest representable number instead. /*
		return (1ul &lt;&lt; (MANTSIZE2+EXPSIZE2-1)) - 1;
	} else {
		return (value &amp;amp; (MAXFRACT2&gt;&gt;1)) | (exp &lt;&lt; (MANTSIZE2-1));
	}
}
#endif

#if ACCT_VERSION == 3
*/
 encode an u64 into a 32 bit IEEE float
 /*
static u32 encode_float(u64 value)
{
	unsigned exp = 190;
	unsigned u;

	if (value == 0)
		return 0;
	while ((s64)value &gt; 0) {
		value &lt;&lt;= 1;
		exp--;
	}
	u = (u32)(value &gt;&gt; 40) &amp;amp; 0x7fffffu;
	return u | (exp &lt;&lt; 23);
}
#endif

*/
  Write an accounting entry for an exiting process

  The acct_process() call is the workhorse of the process
  accounting system. The struct acct is built here and then written
  into the accounting file. This function should only be called from
  do_exit() or when switching to a different output file.
 /*

static void fill_ac(acct_tac)
{
	struct pacct_structpacct = &amp;amp;current-&gt;signal-&gt;pacct;
	u64 elapsed, run_time;
	struct tty_structtty;

	*/
	 Fill the accounting struct with the needed info as recorded
	 by the different kernel functions.
	 /*
	memset(ac, 0, sizeof(acct_t));

	ac-&gt;ac_version = ACCT_VERSION | ACCT_BYTEORDER;
	strlcpy(ac-&gt;ac_comm, current-&gt;comm, sizeof(ac-&gt;ac_comm));

	*/ calculate run_time in nsec/*
	run_time = ktime_get_ns();
	run_time -= current-&gt;group_leader-&gt;start_time;
	*/ convert nsec -&gt; AHZ /*
	elapsed = nsec_to_AHZ(run_time);
#if ACCT_VERSION == 3
	ac-&gt;ac_etime = encode_float(elapsed);
#else
	ac-&gt;ac_etime = encode_comp_t(elapsed &lt; (unsigned long) -1l ?
				(unsigned long) elapsed : (unsigned long) -1l);
#endif
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
	{
		*/ new enlarged etime field /*
		comp2_t etime = encode_comp2_t(elapsed);

		ac-&gt;ac_etime_hi = etime &gt;&gt; 16;
		ac-&gt;ac_etime_lo = (u16) etime;
	}
#endif
	do_div(elapsed, AHZ);
	ac-&gt;ac_btime = get_seconds() - elapsed;
#if ACCT_VERSION==2
	ac-&gt;ac_ahz = AHZ;
#endif

	spin_lock_irq(&amp;amp;current-&gt;sighand-&gt;siglock);
	tty = current-&gt;signal-&gt;tty;	*/ Safe as we hold the siglock /*
	ac-&gt;ac_tty = tty ? old_encode_dev(tty_devnum(tty)) : 0;
	ac-&gt;ac_utime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&gt;ac_utime)));
	ac-&gt;ac_stime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&gt;ac_stime)));
	ac-&gt;ac_flag = pacct-&gt;ac_flag;
	ac-&gt;ac_mem = encode_comp_t(pacct-&gt;ac_mem);
	ac-&gt;ac_minflt = encode_comp_t(pacct-&gt;ac_minflt);
	ac-&gt;ac_majflt = encode_comp_t(pacct-&gt;ac_majflt);
	ac-&gt;ac_exitcode = pacct-&gt;ac_exitcode;
	spin_unlock_irq(&amp;amp;current-&gt;sighand-&gt;siglock);
}
*/
  do_acct_process does all actual work. Caller holds the reference to file.
 /*
static void do_acct_process(struct bsd_acct_structacct)
{
	acct_t ac;
	unsigned long flim;
	const struct credorig_cred;
	struct filefile = acct-&gt;file;

	*/
	 Accounting records are not subject to resource limits.
	 /*
	flim = current-&gt;signal-&gt;rlim[RLIMIT_FSIZE].rlim_cur;
	current-&gt;signal-&gt;rlim[RLIMIT_FSIZE].rlim_cur = RLIM_INFINITY;
	*/ Perform file operations on behalf of whoever enabled accounting /*
	orig_cred = override_creds(file-&gt;f_cred);

	*/
	 First check to see if there is enough free_space to continue
	 the process accounting system.
	 /*
	if (!check_free_space(acct))
		goto out;

	fill_ac(&amp;amp;ac);
	*/ we really need to bite the bullet and change layout /*
	ac.ac_uid = from_kuid_munged(file-&gt;f_cred-&gt;user_ns, orig_cred-&gt;uid);
	ac.ac_gid = from_kgid_munged(file-&gt;f_cred-&gt;user_ns, orig_cred-&gt;gid);
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
	*/ backward-compatible 16 bit fields /*
	ac.ac_uid16 = ac.ac_uid;
	ac.ac_gid16 = ac.ac_gid;
#endif
#if ACCT_VERSION == 3
	{
		struct pid_namespacens = acct-&gt;ns;

		ac.ac_pid = task_tgid_nr_ns(current, ns);
		rcu_read_lock();
		ac.ac_ppid = task_tgid_nr_ns(rcu_dereference(current-&gt;real_parent),
					     ns);
		rcu_read_unlock();
	}
#endif
	*/
	 Get freeze protection. If the fs is frozen, just skip the write
	 as we could deadlock the system otherwise.
	 /*
	if (file_start_write_trylock(file)) {
		*/ it's been opened O_APPEND, so position is irrelevant /*
		loff_t pos = 0;
		__kernel_write(file, (char)&amp;amp;ac, sizeof(acct_t), &amp;amp;pos);
		file_end_write(file);
	}
out:
	current-&gt;signal-&gt;rlim[RLIMIT_FSIZE].rlim_cur = flim;
	revert_creds(orig_cred);
}

*/
 acct_collect - collect accounting information into pacct_struct
 @exitcode: task exit code
 @group_dead: not 0, if this thread is the last one in the process.
 /*
void acct_collect(long exitcode, int group_dead)
{
	struct pacct_structpacct = &amp;amp;current-&gt;signal-&gt;pacct;
	cputime_t utime, stime;
	unsigned long vsize = 0;

	if (group_dead &amp;amp;&amp;amp; current-&gt;mm) {
		struct vm_area_structvma;

		down_read(&amp;amp;current-&gt;mm-&gt;mmap_sem);
		vma = current-&gt;mm-&gt;mmap;
		while (vma) {
			vsize += vma-&gt;vm_end - vma-&gt;vm_start;
			vma = vma-&gt;vm_next;
		}
		up_read(&amp;amp;current-&gt;mm-&gt;mmap_sem);
	}

	spin_lock_irq(&amp;amp;current-&gt;sighand-&gt;siglock);
	if (group_dead)
		pacct-&gt;ac_mem = vsize / 1024;
	if (thread_group_leader(current)) {
		pacct-&gt;ac_exitcode = exitcode;
		if (current-&gt;flags &amp;amp; PF_FORKNOEXEC)
			pacct-&gt;ac_flag |= AFORK;
	}
	if (current-&gt;flags &amp;amp; PF_SUPERPRIV)
		pacct-&gt;ac_flag |= ASU;
	if (current-&gt;flags &amp;amp; PF_DUMPCORE)
		pacct-&gt;ac_flag |= ACORE;
	if (current-&gt;flags &amp;amp; PF_SIGNALED)
		pacct-&gt;ac_flag |= AXSIG;
	task_cputime(current, &amp;amp;utime, &amp;amp;stime);
	pacct-&gt;ac_utime += utime;
	pacct-&gt;ac_stime += stime;
	pacct-&gt;ac_minflt += current-&gt;min_flt;
	pacct-&gt;ac_majflt += current-&gt;maj_flt;
	spin_unlock_irq(&amp;amp;current-&gt;sighand-&gt;siglock);
}

static void slow_acct_process(struct pid_namespacens)
{
	for ( ; ns; ns = ns-&gt;parent) {
		struct bsd_acct_structacct = acct_get(ns);
		if (acct) {
			do_acct_process(acct);
			mutex_unlock(&amp;amp;acct-&gt;lock);
			acct_put(acct);
		}
	}
}

*/
 acct_process

 handles process accounting for an exiting task
 /*
void acct_process(void)
{
	struct pid_namespacens;

	*/
	 This loop is safe lockless, since current is still
	 alive and holds its namespace, which in turn holds
	 its parent.
	 /*
	for (ns = task_active_pid_ns(current); ns != NULL; ns = ns-&gt;parent) {
		if (ns-&gt;bacct)
			break;
	}
	if (unlikely(ns))
		slow_acct_process(ns);
}
*/

 async.c: Asynchronous function calls for boot performance

 (C) Copyright 2009 Intel Corporation
 Author: Arjan van de Ven &lt;arjan@linux.intel.com&gt;

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*


*/

Goals and Theory of Operation

The primary goal of this feature is to reduce the kernel boot time,
by doing various independent hardware delays and discovery operations
decoupled and not strictly serialized.

More specifically, the asynchronous function call concept allows
certain operations (primarily during system boot) to happen
asynchronously, out of order, while these operations still
have their externally visible parts happen sequentially and in-order.
(not unlike how out-of-order CPUs retire their instructions in order)

Key to the asynchronous function call implementation is the concept of
a "sequence cookie" (which, although it has an abstracted type, can be
thought of as a monotonically incrementing number).

The async core will assign each scheduled event such a sequence cookie and
pass this to the called functions.

The asynchronously called function should before doing a globally visible
operation, such as registering device numbers, call the
async_synchronize_cookie() function and pass in its own cookie. The
async_synchronize_cookie() function will make sure that all asynchronous
operations that were scheduled prior to the operation corresponding with the
cookie have completed.

Subsystem/driver initialization code that scheduled asynchronous probe
functions, but which shares global resources with other drivers/subsystems
that do not use the asynchronous call feature, need to do a full
synchronization with the async_synchronize_full() function, before returning
from their init function. This is to maintain strict ordering between the
asynchronous and synchronous parts of the kernel.

/*

#include &lt;linux/async.h&gt;
#include &lt;linux/atomic.h&gt;
#include &lt;linux/ktime.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/wait.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/workqueue.h&gt;

#include "workqueue_internal.h"

static async_cookie_t next_cookie = 1;

#define MAX_WORK		32768
#define ASYNC_COOKIE_MAX	ULLONG_MAX	*/ infinity cookie /*

static LIST_HEAD(async_global_pending);	*/ pending from all registered doms /*
static ASYNC_DOMAIN(async_dfl_domain);
static DEFINE_SPINLOCK(async_lock);

struct async_entry {
	struct list_head	domain_list;
	struct list_head	global_list;
	struct work_struct	work;
	async_cookie_t		cookie;
	async_func_t		func;
	void			*data;
	struct async_domain	*domain;
};

static DECLARE_WAIT_QUEUE_HEAD(async_done);

static atomic_t entry_count;

static async_cookie_t lowest_in_progress(struct async_domaindomain)
{
	struct list_headpending;
	async_cookie_t ret = ASYNC_COOKIE_MAX;
	unsigned long flags;

	spin_lock_irqsave(&amp;amp;async_lock, flags);

	if (domain)
		pending = &amp;amp;domain-&gt;pending;
	else
		pending = &amp;amp;async_global_pending;

	if (!list_empty(pending))
		ret = list_first_entry(pending, struct async_entry,
				       domain_list)-&gt;cookie;

	spin_unlock_irqrestore(&amp;amp;async_lock, flags);
	return ret;
}

*/
 pick the first pending entry and run it
 /*
static void async_run_entry_fn(struct work_structwork)
{
	struct async_entryentry =
		container_of(work, struct async_entry, work);
	unsigned long flags;
	ktime_t uninitialized_var(calltime), delta, rettime;

	*/ 1) run (and print duration) /*
	if (initcall_debug &amp;amp;&amp;amp; system_state == SYSTEM_BOOTING) {
		pr_debug("calling  %lli_%pF @ %i\n",
			(long long)entry-&gt;cookie,
			entry-&gt;func, task_pid_nr(current));
		calltime = ktime_get();
	}
	entry-&gt;func(entry-&gt;data, entry-&gt;cookie);
	if (initcall_debug &amp;amp;&amp;amp; system_state == SYSTEM_BOOTING) {
		rettime = ktime_get();
		delta = ktime_sub(rettime, calltime);
		pr_debug("initcall %lli_%pF returned 0 after %lld usecs\n",
			(long long)entry-&gt;cookie,
			entry-&gt;func,
			(long long)ktime_to_ns(delta) &gt;&gt; 10);
	}

	*/ 2) remove self from the pending queues /*
	spin_lock_irqsave(&amp;amp;async_lock, flags);
	list_del_init(&amp;amp;entry-&gt;domain_list);
	list_del_init(&amp;amp;entry-&gt;global_list);

	*/ 3) free the entry /*
	kfree(entry);
	atomic_dec(&amp;amp;entry_count);

	spin_unlock_irqrestore(&amp;amp;async_lock, flags);

	*/ 4) wake up any waiters /*
	wake_up(&amp;amp;async_done);
}

static async_cookie_t __async_schedule(async_func_t func, voiddata, struct async_domaindomain)
{
	struct async_entryentry;
	unsigned long flags;
	async_cookie_t newcookie;

	*/ allow irq-off callers /*
	entry = kzalloc(sizeof(struct async_entry), GFP_ATOMIC);

	*/
	 If we're out of memory or if there's too much work
	 pending already, we execute synchronously.
	 /*
	if (!entry || atomic_read(&amp;amp;entry_count) &gt; MAX_WORK) {
		kfree(entry);
		spin_lock_irqsave(&amp;amp;async_lock, flags);
		newcookie = next_cookie++;
		spin_unlock_irqrestore(&amp;amp;async_lock, flags);

		*/ low on memory.. run synchronously /*
		func(data, newcookie);
		return newcookie;
	}
	INIT_LIST_HEAD(&amp;amp;entry-&gt;domain_list);
	INIT_LIST_HEAD(&amp;amp;entry-&gt;global_list);
	INIT_WORK(&amp;amp;entry-&gt;work, async_run_entry_fn);
	entry-&gt;func = func;
	entry-&gt;data = data;
	entry-&gt;domain = domain;

	spin_lock_irqsave(&amp;amp;async_lock, flags);

	*/ allocate cookie and queue /*
	newcookie = entry-&gt;cookie = next_cookie++;

	list_add_tail(&amp;amp;entry-&gt;domain_list, &amp;amp;domain-&gt;pending);
	if (domain-&gt;registered)
		list_add_tail(&amp;amp;entry-&gt;global_list, &amp;amp;async_global_pending);

	atomic_inc(&amp;amp;entry_count);
	spin_unlock_irqrestore(&amp;amp;async_lock, flags);

	*/ mark that this task has queued an async job, used by module init /*
	current-&gt;flags |= PF_USED_ASYNC;

	*/ schedule for execution /*
	queue_work(system_unbound_wq, &amp;amp;entry-&gt;work);

	return newcookie;
}

*/
 async_schedule - schedule a function for asynchronous execution
 @func: function to execute asynchronously
 @data: data pointer to pass to the function

 Returns an async_cookie_t that may be used for checkpointing later.
 Note: This function may be called from atomic or non-atomic contexts.
 /*
async_cookie_t async_schedule(async_func_t func, voiddata)
{
	return __async_schedule(func, data, &amp;amp;async_dfl_domain);
}
EXPORT_SYMBOL_GPL(async_schedule);

*/
 async_schedule_domain - schedule a function for asynchronous execution within a certain domain
 @func: function to execute asynchronously
 @data: data pointer to pass to the function
 @domain: the domain

 Returns an async_cookie_t that may be used for checkpointing later.
 @domain may be used in the async_synchronize_*_domain() functions to
 wait within a certain synchronization domain rather than globally.  A
 synchronization domain is specified via @domain.  Note: This function
 may be called from atomic or non-atomic contexts.
 /*
async_cookie_t async_schedule_domain(async_func_t func, voiddata,
				     struct async_domaindomain)
{
	return __async_schedule(func, data, domain);
}
EXPORT_SYMBOL_GPL(async_schedule_domain);

*/
 async_synchronize_full - synchronize all asynchronous function calls

 This function waits until all asynchronous function calls have been done.
 /*
void async_synchronize_full(void)
{
	async_synchronize_full_domain(NULL);
}
EXPORT_SYMBOL_GPL(async_synchronize_full);

*/
 async_unregister_domain - ensure no more anonymous waiters on this domain
 @domain: idle domain to flush out of any async_synchronize_full instances

 async_synchronize_{cookie|full}_domain() are not flushed since callers
 of these routines should know the lifetime of @domain

 Prefer ASYNC_DOMAIN_EXCLUSIVE() declarations over flushing
 /*
void async_unregister_domain(struct async_domaindomain)
{
	spin_lock_irq(&amp;amp;async_lock);
	WARN_ON(!domain-&gt;registered || !list_empty(&amp;amp;domain-&gt;pending));
	domain-&gt;registered = 0;
	spin_unlock_irq(&amp;amp;async_lock);
}
EXPORT_SYMBOL_GPL(async_unregister_domain);

*/
 async_synchronize_full_domain - synchronize all asynchronous function within a certain domain
 @domain: the domain to synchronize

 This function waits until all asynchronous function calls for the
 synchronization domain specified by @domain have been done.
 /*
void async_synchronize_full_domain(struct async_domaindomain)
{
	async_synchronize_cookie_domain(ASYNC_COOKIE_MAX, domain);
}
EXPORT_SYMBOL_GPL(async_synchronize_full_domain);

*/
 async_synchronize_cookie_domain - synchronize asynchronous function calls within a certain domain with cookie checkpointing
 @cookie: async_cookie_t to use as checkpoint
 @domain: the domain to synchronize (%NULL for all registered domains)

 This function waits until all asynchronous function calls for the
 synchronization domain specified by @domain submitted prior to @cookie
 have been done.
 /*
void async_synchronize_cookie_domain(async_cookie_t cookie, struct async_domaindomain)
{
	ktime_t uninitialized_var(starttime), delta, endtime;

	if (initcall_debug &amp;amp;&amp;amp; system_state == SYSTEM_BOOTING) {
		pr_debug("async_waiting @ %i\n", task_pid_nr(current));
		starttime = ktime_get();
	}

	wait_event(async_done, lowest_in_progress(domain) &gt;= cookie);

	if (initcall_debug &amp;amp;&amp;amp; system_state == SYSTEM_BOOTING) {
		endtime = ktime_get();
		delta = ktime_sub(endtime, starttime);

		pr_debug("async_continuing @ %i after %lli usec\n",
			task_pid_nr(current),
			(long long)ktime_to_ns(delta) &gt;&gt; 10);
	}
}
EXPORT_SYMBOL_GPL(async_synchronize_cookie_domain);

*/
 async_synchronize_cookie - synchronize asynchronous function calls with cookie checkpointing
 @cookie: async_cookie_t to use as checkpoint

 This function waits until all asynchronous function calls prior to @cookie
 have been done.
 /*
void async_synchronize_cookie(async_cookie_t cookie)
{
	async_synchronize_cookie_domain(cookie, &amp;amp;async_dfl_domain);
}
EXPORT_SYMBOL_GPL(async_synchronize_cookie);

*/
 current_is_async - is %current an async worker task?

 Returns %true if %current is an async worker task.
 /*
bool current_is_async(void)
{
	struct workerworker = current_wq_worker();

	return worker &amp;amp;&amp;amp; worker-&gt;current_func == async_run_entry_fn;
}
EXPORT_SYMBOL_GPL(current_is_async);
*/
 audit.c -- Auditing support
 Gateway between the kernel (e.g., selinux) and the user-space audit daemon.
 System-call specific features have moved to auditsc.c

 Copyright 2003-2007 Red Hat Inc., Durham, North Carolina.
 All Rights Reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Written by Rickard E. (Rik) Faith &lt;faith@redhat.com&gt;

 Goals: 1) Integrate fully with Security Modules.
	  2) Minimal run-time overhead:
	     a) Minimal when syscall auditing is disabled (audit_enable=0).
	     b) Small when syscall auditing is enabled and no audit record
		is generated (defer as much work as possible to record
		generation time):
		i) context is allocated,
		ii) names from getname are stored without a copy, and
		iii) inode information stored from path_lookup.
	  3) Ability to disable syscall auditing at boot time (audit=0).
	  4) Usable by other parts of the kernel (if audit_log* is called,
	     then a syscall record will be generated automatically for the
	     current syscall).
	  5) Netlink interface to user-space.
	  6) Support low-overhead kernel-based filtering to minimize the
	     information that must be passed to user-space.

 Example user-space utilities: http://people.redhat.com/sgrubb/audit/
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include &lt;linux/file.h&gt;
#include &lt;linux/init.h&gt;
#include &lt;linux/types.h&gt;
#include &lt;linux/atomic.h&gt;
#include &lt;linux/mm.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/err.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/kernel.h&gt;
#include &lt;linux/syscalls.h&gt;

#include &lt;linux/audit.h&gt;

#include &lt;net/sock.h&gt;
#include &lt;net/netlink.h&gt;
#include &lt;linux/skbuff.h&gt;
#ifdef CONFIG_SECURITY
#include &lt;linux/security.h&gt;
#endif
#include &lt;linux/freezer.h&gt;
#include &lt;linux/tty.h&gt;
#include &lt;linux/pid_namespace.h&gt;
#include &lt;net/netns/generic.h&gt;

#include "audit.h"

*/ No auditing will take place until audit_initialized == AUDIT_INITIALIZED.
 (Initialization happens after skb_init is called.) /*
#define AUDIT_DISABLED		-1
#define AUDIT_UNINITIALIZED	0
#define AUDIT_INITIALIZED	1
static int	audit_initialized;

#define AUDIT_OFF	0
#define AUDIT_ON	1
#define AUDIT_LOCKED	2
u32		audit_enabled;
u32		audit_ever_enabled;

EXPORT_SYMBOL_GPL(audit_enabled);

*/ Default state when kernel boots without any parameters. /*
static u32	audit_default;

*/ If auditing cannot proceed, audit_failure selects what happens. /*
static u32	audit_failure = AUDIT_FAIL_PRINTK;

*/
 If audit records are to be written to the netlink socket, audit_pid
 contains the pid of the auditd process and audit_nlk_portid contains
 the portid to use to send netlink messages to that process.
 /*
int		audit_pid;
static __u32	audit_nlk_portid;

*/ If audit_rate_limit is non-zero, limit the rate of sending audit records
 to that number per second.  This prevents DoS attacks, but results in
 audit records being dropped. /*
static u32	audit_rate_limit;

*/ Number of outstanding audit_buffers allowed.
 When set to zero, this means unlimited. /*
static u32	audit_backlog_limit = 64;
#define AUDIT_BACKLOG_WAIT_TIME (60 HZ)
static u32	audit_backlog_wait_time_master = AUDIT_BACKLOG_WAIT_TIME;
static u32	audit_backlog_wait_time = AUDIT_BACKLOG_WAIT_TIME;

*/ The identity of the user shutting down the audit system. /*
kuid_t		audit_sig_uid = INVALID_UID;
pid_t		audit_sig_pid = -1;
u32		audit_sig_sid = 0;

*/ Records can be lost in several ways:
   0) [suppressed in audit_alloc]
   1) out of memory in audit_log_start [kmalloc of struct audit_buffer]
   2) out of memory in audit_log_move [alloc_skb]
   3) suppressed due to audit_rate_limit
   4) suppressed due to audit_backlog_limit
/*
static atomic_t    audit_lost = ATOMIC_INIT(0);

*/ The netlink socket. /*
static struct sockaudit_sock;
static int audit_net_id;

*/ Hash for inode-based rules /*
struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];

*/ The audit_freelist is a list of pre-allocated audit buffers (if more
 than AUDIT_MAXFREE are in use, the audit buffer is freed instead of
 being placed on the freelist). /*
static DEFINE_SPINLOCK(audit_freelist_lock);
static int	   audit_freelist_count;
static LIST_HEAD(audit_freelist);

static struct sk_buff_head audit_skb_queue;
*/ queue of skbs to send to auditd when/if it comes back /*
static struct sk_buff_head audit_skb_hold_queue;
static struct task_structkauditd_task;
static DECLARE_WAIT_QUEUE_HEAD(kauditd_wait);
static DECLARE_WAIT_QUEUE_HEAD(audit_backlog_wait);

static struct audit_features af = {.vers = AUDIT_FEATURE_VERSION,
				   .mask = -1,
				   .features = 0,
				   .lock = 0,};

static charaudit_feature_names[2] = {
	"only_unset_loginuid",
	"loginuid_immutable",
};


*/ Serialize requests from userspace. /*
DEFINE_MUTEX(audit_cmd_mutex);

*/ AUDIT_BUFSIZ is the size of the temporary buffer used for formatting
 audit records.  Since printk uses a 1024 byte buffer, this buffer
 should be at least that large. /*
#define AUDIT_BUFSIZ 1024

*/ AUDIT_MAXFREE is the number of empty audit_buffers we keep on the
 audit_freelist.  Doing so eliminates many kmalloc/kfree calls. /*
#define AUDIT_MAXFREE  (2*NR_CPUS)

*/ The audit_buffer is used when formatting an audit record.  The caller
 locks briefly to get the record off the freelist or to allocate the
 buffer, and locks briefly to send the buffer to the netlink layer or
 to place it on a transmit queue.  Multiple audit_buffers can be in
 use simultaneously. /*
struct audit_buffer {
	struct list_head     list;
	struct sk_buff      skb;	*/ formatted skb ready to send /*
	struct audit_contextctx;	*/ NULL or associated context /*
	gfp_t		     gfp_mask;
};

struct audit_reply {
	__u32 portid;
	struct netnet;
	struct sk_buffskb;
};

static void audit_set_portid(struct audit_bufferab, __u32 portid)
{
	if (ab) {
		struct nlmsghdrnlh = nlmsg_hdr(ab-&gt;skb);
		nlh-&gt;nlmsg_pid = portid;
	}
}

void audit_panic(const charmessage)
{
	switch (audit_failure) {
	case AUDIT_FAIL_SILENT:
		break;
	case AUDIT_FAIL_PRINTK:
		if (printk_ratelimit())
			pr_err("%s\n", message);
		break;
	case AUDIT_FAIL_PANIC:
		*/ test audit_pid since printk is always losey, why bother? /*
		if (audit_pid)
			panic("audit: %s\n", message);
		break;
	}
}

static inline int audit_rate_check(void)
{
	static unsigned long	last_check = 0;
	static int		messages   = 0;
	static DEFINE_SPINLOCK(lock);
	unsigned long		flags;
	unsigned long		now;
	unsigned long		elapsed;
	int			retval	   = 0;

	if (!audit_rate_limit) return 1;

	spin_lock_irqsave(&amp;amp;lock, flags);
	if (++messages &lt; audit_rate_limit) {
		retval = 1;
	} else {
		now     = jiffies;
		elapsed = now - last_check;
		if (elapsed &gt; HZ) {
			last_check = now;
			messages   = 0;
			retval     = 1;
		}
	}
	spin_unlock_irqrestore(&amp;amp;lock, flags);

	return retval;
}

*/
 audit_log_lost - conditionally log lost audit message event
 @message: the message stating reason for lost audit message

 Emit at least 1 message per second, even if audit_rate_check is
 throttling.
 Always increment the lost messages counter.
/*
void audit_log_lost(const charmessage)
{
	static unsigned long	last_msg = 0;
	static DEFINE_SPINLOCK(lock);
	unsigned long		flags;
	unsigned long		now;
	int			print;

	atomic_inc(&amp;amp;audit_lost);

	print = (audit_failure == AUDIT_FAIL_PANIC || !audit_rate_limit);

	if (!print) {
		spin_lock_irqsave(&amp;amp;lock, flags);
		now = jiffies;
		if (now - last_msg &gt; HZ) {
			print = 1;
			last_msg = now;
		}
		spin_unlock_irqrestore(&amp;amp;lock, flags);
	}

	if (print) {
		if (printk_ratelimit())
			pr_warn("audit_lost=%u audit_rate_limit=%u audit_backlog_limit=%u\n",
				atomic_read(&amp;amp;audit_lost),
				audit_rate_limit,
				audit_backlog_limit);
		audit_panic(message);
	}
}

static int audit_log_config_change(charfunction_name, u32 new, u32 old,
				   int allow_changes)
{
	struct audit_bufferab;
	int rc = 0;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return rc;
	audit_log_format(ab, "%s=%u old=%u", function_name, new, old);
	audit_log_session_info(ab);
	rc = audit_log_task_context(ab);
	if (rc)
		allow_changes = 0;/ Something weird, deny request /*
	audit_log_format(ab, " res=%d", allow_changes);
	audit_log_end(ab);
	return rc;
}

static int audit_do_config_change(charfunction_name, u32to_change, u32 new)
{
	int allow_changes, rc = 0;
	u32 old =to_change;

	*/ check if we are locked /*
	if (audit_enabled == AUDIT_LOCKED)
		allow_changes = 0;
	else
		allow_changes = 1;

	if (audit_enabled != AUDIT_OFF) {
		rc = audit_log_config_change(function_name, new, old, allow_changes);
		if (rc)
			allow_changes = 0;
	}

	*/ If we are allowed, make the change /*
	if (allow_changes == 1)
		*to_change = new;
	*/ Not allowed, update reason /*
	else if (rc == 0)
		rc = -EPERM;
	return rc;
}

static int audit_set_rate_limit(u32 limit)
{
	return audit_do_config_change("audit_rate_limit", &amp;amp;audit_rate_limit, limit);
}

static int audit_set_backlog_limit(u32 limit)
{
	return audit_do_config_change("audit_backlog_limit", &amp;amp;audit_backlog_limit, limit);
}

static int audit_set_backlog_wait_time(u32 timeout)
{
	return audit_do_config_change("audit_backlog_wait_time",
				      &amp;amp;audit_backlog_wait_time_master, timeout);
}

static int audit_set_enabled(u32 state)
{
	int rc;
	if (state &gt; AUDIT_LOCKED)
		return -EINVAL;

	rc =  audit_do_config_change("audit_enabled", &amp;amp;audit_enabled, state);
	if (!rc)
		audit_ever_enabled |= !!state;

	return rc;
}

static int audit_set_failure(u32 state)
{
	if (state != AUDIT_FAIL_SILENT
	    &amp;amp;&amp;amp; state != AUDIT_FAIL_PRINTK
	    &amp;amp;&amp;amp; state != AUDIT_FAIL_PANIC)
		return -EINVAL;

	return audit_do_config_change("audit_failure", &amp;amp;audit_failure, state);
}

*/
 Queue skbs to be sent to auditd when/if it comes back.  These skbs should
 already have been sent via prink/syslog and so if these messages are dropped
 it is not a huge concern since we already passed the audit_log_lost()
 notification and stuff.  This is just nice to get audit messages during
 boot before auditd is running or messages generated while auditd is stopped.
 This only holds messages is audit_default is set, aka booting with audit=1
 or building your kernel that way.
 /*
static void audit_hold_skb(struct sk_buffskb)
{
	if (audit_default &amp;amp;&amp;amp;
	    (!audit_backlog_limit ||
	     skb_queue_len(&amp;amp;audit_skb_hold_queue) &lt; audit_backlog_limit))
		skb_queue_tail(&amp;amp;audit_skb_hold_queue, skb);
	else
		kfree_skb(skb);
}

*/
 For one reason or another this nlh isn't getting delivered to the userspace
 audit daemon, just send it to printk.
 /*
static void audit_printk_skb(struct sk_buffskb)
{
	struct nlmsghdrnlh = nlmsg_hdr(skb);
	chardata = nlmsg_data(nlh);

	if (nlh-&gt;nlmsg_type != AUDIT_EOE) {
		if (printk_ratelimit())
			pr_notice("type=%d %s\n", nlh-&gt;nlmsg_type, data);
		else
			audit_log_lost("printk limit exceeded");
	}

	audit_hold_skb(skb);
}

static void kauditd_send_skb(struct sk_buffskb)
{
	int err;
	int attempts = 0;
#define AUDITD_RETRIES 5

restart:
	*/ take a reference in case we can't send it and we want to hold it /*
	skb_get(skb);
	err = netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
	if (err &lt; 0) {
		pr_err("netlink_unicast sending to audit_pid=%d returned error: %d\n",
		       audit_pid, err);
		if (audit_pid) {
			if (err == -ECONNREFUSED || err == -EPERM
			    || ++attempts &gt;= AUDITD_RETRIES) {
				char s[32];

				snprintf(s, sizeof(s), "audit_pid=%d reset", audit_pid);
				audit_log_lost(s);
				audit_pid = 0;
				audit_sock = NULL;
			} else {
				pr_warn("re-scheduling(#%d) write to audit_pid=%d\n",
					attempts, audit_pid);
				set_current_state(TASK_INTERRUPTIBLE);
				schedule();
				__set_current_state(TASK_RUNNING);
				goto restart;
			}
		}
		*/ we might get lucky and get this in the next auditd /*
		audit_hold_skb(skb);
	} else
		*/ drop the extra reference if sent ok /*
		consume_skb(skb);
}

*/
 kauditd_send_multicast_skb - send the skb to multicast userspace listeners

 This function doesn't consume an skb as might be expected since it has to
 copy it anyways.
 /*
static void kauditd_send_multicast_skb(struct sk_buffskb, gfp_t gfp_mask)
{
	struct sk_buff		*copy;
	struct audit_net	*aunet = net_generic(&amp;amp;init_net, audit_net_id);
	struct sock		*sock = aunet-&gt;nlsk;

	if (!netlink_has_listeners(sock, AUDIT_NLGRP_READLOG))
		return;

	*/
	 The seemingly wasteful skb_copy() rather than bumping the refcount
	 using skb_get() is necessary because non-standard mods are made to
	 the skb by the original kaudit unicast socket send routine.  The
	 existing auditd daemon assumes this breakage.  Fixing this would
	 require co-ordinating a change in the established protocol between
	 the kaudit kernel subsystem and the auditd userspace code.  There is
	 no reason for new multicast clients to continue with this
	 non-compliance.
	 /*
	copy = skb_copy(skb, gfp_mask);
	if (!copy)
		return;

	nlmsg_multicast(sock, copy, 0, AUDIT_NLGRP_READLOG, gfp_mask);
}

*/
 flush_hold_queue - empty the hold queue if auditd appears

 If auditd just started, drain the queue of messages already
 sent to syslog/printk.  Remember loss here is ok.  We already
 called audit_log_lost() if it didn't go out normally.  so the
 race between the skb_dequeue and the next check for audit_pid
 doesn't matter.

 If you ever find kauditd to be too slow we can get a perf win
 by doing our own locking and keeping better track if there
 are messages in this queue.  I don't see the need now, but
 in 5 years when I want to play with this again I'll see this
 note and still have no friggin idea what i'm thinking today.
 /*
static void flush_hold_queue(void)
{
	struct sk_buffskb;

	if (!audit_default || !audit_pid)
		return;

	skb = skb_dequeue(&amp;amp;audit_skb_hold_queue);
	if (likely(!skb))
		return;

	while (skb &amp;amp;&amp;amp; audit_pid) {
		kauditd_send_skb(skb);
		skb = skb_dequeue(&amp;amp;audit_skb_hold_queue);
	}

	*/
	 if auditd just disappeared but we
	 dequeued an skb we need to drop ref
	 /*
	consume_skb(skb);
}

static int kauditd_thread(voiddummy)
{
	set_freezable();
	while (!kthread_should_stop()) {
		struct sk_buffskb;

		flush_hold_queue();

		skb = skb_dequeue(&amp;amp;audit_skb_queue);

		if (skb) {
			if (!audit_backlog_limit ||
			    (skb_queue_len(&amp;amp;audit_skb_queue) &lt;= audit_backlog_limit))
				wake_up(&amp;amp;audit_backlog_wait);
			if (audit_pid)
				kauditd_send_skb(skb);
			else
				audit_printk_skb(skb);
			continue;
		}

		wait_event_freezable(kauditd_wait, skb_queue_len(&amp;amp;audit_skb_queue));
	}
	return 0;
}

int audit_send_list(void_dest)
{
	struct audit_netlink_listdest = _dest;
	struct sk_buffskb;
	struct netnet = dest-&gt;net;
	struct audit_netaunet = net_generic(net, audit_net_id);

	*/ wait for parent to finish and send an ACK /*
	mutex_lock(&amp;amp;audit_cmd_mutex);
	mutex_unlock(&amp;amp;audit_cmd_mutex);

	while ((skb = __skb_dequeue(&amp;amp;dest-&gt;q)) != NULL)
		netlink_unicast(aunet-&gt;nlsk, skb, dest-&gt;portid, 0);

	put_net(net);
	kfree(dest);

	return 0;
}

struct sk_buffaudit_make_reply(__u32 portid, int seq, int type, int done,
				 int multi, const voidpayload, int size)
{
	struct sk_buff	*skb;
	struct nlmsghdr	*nlh;
	void		*data;
	int		flags = multi ? NLM_F_MULTI : 0;
	int		t     = done  ? NLMSG_DONE  : type;

	skb = nlmsg_new(size, GFP_KERNEL);
	if (!skb)
		return NULL;

	nlh	= nlmsg_put(skb, portid, seq, t, size, flags);
	if (!nlh)
		goto out_kfree_skb;
	data = nlmsg_data(nlh);
	memcpy(data, payload, size);
	return skb;

out_kfree_skb:
	kfree_skb(skb);
	return NULL;
}

static int audit_send_reply_thread(voidarg)
{
	struct audit_replyreply = (struct audit_reply)arg;
	struct netnet = reply-&gt;net;
	struct audit_netaunet = net_generic(net, audit_net_id);

	mutex_lock(&amp;amp;audit_cmd_mutex);
	mutex_unlock(&amp;amp;audit_cmd_mutex);

	*/ Ignore failure. It'll only happen if the sender goes away,
	   because our timeout is set to infinite. /*
	netlink_unicast(aunet-&gt;nlsk , reply-&gt;skb, reply-&gt;portid, 0);
	put_net(net);
	kfree(reply);
	return 0;
}
*/
 audit_send_reply - send an audit reply message via netlink
 @request_skb: skb of request we are replying to (used to target the reply)
 @seq: sequence number
 @type: audit message type
 @done: done (last) flag
 @multi: multi-part message flag
 @payload: payload data
 @size: payload size

 Allocates an skb, builds the netlink message, and sends it to the port id.
 No failure notifications.
 /*
static void audit_send_reply(struct sk_buffrequest_skb, int seq, int type, int done,
			     int multi, const voidpayload, int size)
{
	u32 portid = NETLINK_CB(request_skb).portid;
	struct netnet = sock_net(NETLINK_CB(request_skb).sk);
	struct sk_buffskb;
	struct task_structtsk;
	struct audit_replyreply = kmalloc(sizeof(struct audit_reply),
					    GFP_KERNEL);

	if (!reply)
		return;

	skb = audit_make_reply(portid, seq, type, done, multi, payload, size);
	if (!skb)
		goto out;

	reply-&gt;net = get_net(net);
	reply-&gt;portid = portid;
	reply-&gt;skb = skb;

	tsk = kthread_run(audit_send_reply_thread, reply, "audit_send_reply");
	if (!IS_ERR(tsk))
		return;
	kfree_skb(skb);
out:
	kfree(reply);
}

*/
 Check for appropriate CAP_AUDIT_ capabilities on incoming audit
 control messages.
 /*
static int audit_netlink_ok(struct sk_buffskb, u16 msg_type)
{
	int err = 0;

	*/ Only support initial user namespace for now. /*
	*/
	 We return ECONNREFUSED because it tricks userspace into thinking
	 that audit was not configured into the kernel.  Lots of users
	 configure their PAM stack (because that's what the distro does)
	 to reject login if unable to send messages to audit.  If we return
	 ECONNREFUSED the PAM stack thinks the kernel does not have audit
	 configured in and will let login proceed.  If we return EPERM
	 userspace will reject all logins.  This should be removed when we
	 support non init namespaces!!
	 /*
	if (current_user_ns() != &amp;amp;init_user_ns)
		return -ECONNREFUSED;

	switch (msg_type) {
	case AUDIT_LIST:
	case AUDIT_ADD:
	case AUDIT_DEL:
		return -EOPNOTSUPP;
	case AUDIT_GET:
	case AUDIT_SET:
	case AUDIT_GET_FEATURE:
	case AUDIT_SET_FEATURE:
	case AUDIT_LIST_RULES:
	case AUDIT_ADD_RULE:
	case AUDIT_DEL_RULE:
	case AUDIT_SIGNAL_INFO:
	case AUDIT_TTY_GET:
	case AUDIT_TTY_SET:
	case AUDIT_TRIM:
	case AUDIT_MAKE_EQUIV:
		*/ Only support auditd and auditctl in initial pid namespace
		 for now. /*
		if (task_active_pid_ns(current) != &amp;amp;init_pid_ns)
			return -EPERM;

		if (!netlink_capable(skb, CAP_AUDIT_CONTROL))
			err = -EPERM;
		break;
	case AUDIT_USER:
	case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
	case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
		if (!netlink_capable(skb, CAP_AUDIT_WRITE))
			err = -EPERM;
		break;
	default: / bad msg /*
		err = -EINVAL;
	}

	return err;
}

static void audit_log_common_recv_msg(struct audit_buffer*ab, u16 msg_type)
{
	uid_t uid = from_kuid(&amp;amp;init_user_ns, current_uid());
	pid_t pid = task_tgid_nr(current);

	if (!audit_enabled &amp;amp;&amp;amp; msg_type != AUDIT_USER_AVC) {
		*ab = NULL;
		return;
	}

	*ab = audit_log_start(NULL, GFP_KERNEL, msg_type);
	if (unlikely(!*ab))
		return;
	audit_log_format(*ab, "pid=%d uid=%u", pid, uid);
	audit_log_session_info(*ab);
	audit_log_task_context(*ab);
}

int is_audit_feature_set(int i)
{
	return af.features &amp;amp; AUDIT_FEATURE_TO_MASK(i);
}


static int audit_get_feature(struct sk_buffskb)
{
	u32 seq;

	seq = nlmsg_hdr(skb)-&gt;nlmsg_seq;

	audit_send_reply(skb, seq, AUDIT_GET_FEATURE, 0, 0, &amp;amp;af, sizeof(af));

	return 0;
}

static void audit_log_feature_change(int which, u32 old_feature, u32 new_feature,
				     u32 old_lock, u32 new_lock, int res)
{
	struct audit_bufferab;

	if (audit_enabled == AUDIT_OFF)
		return;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_FEATURE_CHANGE);
	audit_log_task_info(ab, current);
	audit_log_format(ab, " feature=%s old=%u new=%u old_lock=%u new_lock=%u res=%d",
			 audit_feature_names[which], !!old_feature, !!new_feature,
			 !!old_lock, !!new_lock, res);
	audit_log_end(ab);
}

static int audit_set_feature(struct sk_buffskb)
{
	struct audit_featuresuaf;
	int i;

	BUILD_BUG_ON(AUDIT_LAST_FEATURE + 1 &gt; ARRAY_SIZE(audit_feature_names));
	uaf = nlmsg_data(nlmsg_hdr(skb));

	*/ if there is ever a version 2 we should handle that here /*

	for (i = 0; i &lt;= AUDIT_LAST_FEATURE; i++) {
		u32 feature = AUDIT_FEATURE_TO_MASK(i);
		u32 old_feature, new_feature, old_lock, new_lock;

		*/ if we are not changing this feature, move along /*
		if (!(feature &amp;amp; uaf-&gt;mask))
			continue;

		old_feature = af.features &amp;amp; feature;
		new_feature = uaf-&gt;features &amp;amp; feature;
		new_lock = (uaf-&gt;lock | af.lock) &amp;amp; feature;
		old_lock = af.lock &amp;amp; feature;

		*/ are we changing a locked feature? /*
		if (old_lock &amp;amp;&amp;amp; (new_feature != old_feature)) {
			audit_log_feature_change(i, old_feature, new_feature,
						 old_lock, new_lock, 0);
			return -EPERM;
		}
	}
	*/ nothing invalid, do the changes /*
	for (i = 0; i &lt;= AUDIT_LAST_FEATURE; i++) {
		u32 feature = AUDIT_FEATURE_TO_MASK(i);
		u32 old_feature, new_feature, old_lock, new_lock;

		*/ if we are not changing this feature, move along /*
		if (!(feature &amp;amp; uaf-&gt;mask))
			continue;

		old_feature = af.features &amp;amp; feature;
		new_feature = uaf-&gt;features &amp;amp; feature;
		old_lock = af.lock &amp;amp; feature;
		new_lock = (uaf-&gt;lock | af.lock) &amp;amp; feature;

		if (new_feature != old_feature)
			audit_log_feature_change(i, old_feature, new_feature,
						 old_lock, new_lock, 1);

		if (new_feature)
			af.features |= feature;
		else
			af.features &amp;amp;= ~feature;
		af.lock |= new_lock;
	}

	return 0;
}

static int audit_replace(pid_t pid)
{
	struct sk_buffskb = audit_make_reply(0, 0, AUDIT_REPLACE, 0, 0,
					       &amp;amp;pid, sizeof(pid));

	if (!skb)
		return -ENOMEM;
	return netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
}

static int audit_receive_msg(struct sk_buffskb, struct nlmsghdrnlh)
{
	u32			seq;
	void			*data;
	int			err;
	struct audit_buffer	*ab;
	u16			msg_type = nlh-&gt;nlmsg_type;
	struct audit_sig_info  sig_data;
	char			*ctx = NULL;
	u32			len;

	err = audit_netlink_ok(skb, msg_type);
	if (err)
		return err;

	*/ As soon as there's any sign of userspace auditd,
	 start kauditd to talk to it /*
	if (!kauditd_task) {
		kauditd_task = kthread_run(kauditd_thread, NULL, "kauditd");
		if (IS_ERR(kauditd_task)) {
			err = PTR_ERR(kauditd_task);
			kauditd_task = NULL;
			return err;
		}
	}
	seq  = nlh-&gt;nlmsg_seq;
	data = nlmsg_data(nlh);

	switch (msg_type) {
	case AUDIT_GET: {
		struct audit_status	s;
		memset(&amp;amp;s, 0, sizeof(s));
		s.enabled		= audit_enabled;
		s.failure		= audit_failure;
		s.pid			= audit_pid;
		s.rate_limit		= audit_rate_limit;
		s.backlog_limit		= audit_backlog_limit;
		s.lost			= atomic_read(&amp;amp;audit_lost);
		s.backlog		= skb_queue_len(&amp;amp;audit_skb_queue);
		s.feature_bitmap	= AUDIT_FEATURE_BITMAP_ALL;
		s.backlog_wait_time	= audit_backlog_wait_time_master;
		audit_send_reply(skb, seq, AUDIT_GET, 0, 0, &amp;amp;s, sizeof(s));
		break;
	}
	case AUDIT_SET: {
		struct audit_status	s;
		memset(&amp;amp;s, 0, sizeof(s));
		*/ guard against past and future API changes /*
		memcpy(&amp;amp;s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
		if (s.mask &amp;amp; AUDIT_STATUS_ENABLED) {
			err = audit_set_enabled(s.enabled);
			if (err &lt; 0)
				return err;
		}
		if (s.mask &amp;amp; AUDIT_STATUS_FAILURE) {
			err = audit_set_failure(s.failure);
			if (err &lt; 0)
				return err;
		}
		if (s.mask &amp;amp; AUDIT_STATUS_PID) {
			int new_pid = s.pid;
			pid_t requesting_pid = task_tgid_vnr(current);

			if ((!new_pid) &amp;amp;&amp;amp; (requesting_pid != audit_pid)) {
				audit_log_config_change("audit_pid", new_pid, audit_pid, 0);
				return -EACCES;
			}
			if (audit_pid &amp;amp;&amp;amp; new_pid &amp;amp;&amp;amp;
			    audit_replace(requesting_pid) != -ECONNREFUSED) {
				audit_log_config_change("audit_pid", new_pid, audit_pid, 0);
				return -EEXIST;
			}
			if (audit_enabled != AUDIT_OFF)
				audit_log_config_change("audit_pid", new_pid, audit_pid, 1);
			audit_pid = new_pid;
			audit_nlk_portid = NETLINK_CB(skb).portid;
			audit_sock = skb-&gt;sk;
		}
		if (s.mask &amp;amp; AUDIT_STATUS_RATE_LIMIT) {
			err = audit_set_rate_limit(s.rate_limit);
			if (err &lt; 0)
				return err;
		}
		if (s.mask &amp;amp; AUDIT_STATUS_BACKLOG_LIMIT) {
			err = audit_set_backlog_limit(s.backlog_limit);
			if (err &lt; 0)
				return err;
		}
		if (s.mask &amp;amp; AUDIT_STATUS_BACKLOG_WAIT_TIME) {
			if (sizeof(s) &gt; (size_t)nlh-&gt;nlmsg_len)
				return -EINVAL;
			if (s.backlog_wait_time &gt; 10*AUDIT_BACKLOG_WAIT_TIME)
				return -EINVAL;
			err = audit_set_backlog_wait_time(s.backlog_wait_time);
			if (err &lt; 0)
				return err;
		}
		break;
	}
	case AUDIT_GET_FEATURE:
		err = audit_get_feature(skb);
		if (err)
			return err;
		break;
	case AUDIT_SET_FEATURE:
		err = audit_set_feature(skb);
		if (err)
			return err;
		break;
	case AUDIT_USER:
	case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
	case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
		if (!audit_enabled &amp;amp;&amp;amp; msg_type != AUDIT_USER_AVC)
			return 0;

		err = audit_filter_user(msg_type);
		if (err == 1) {/ match or error /*
			err = 0;
			if (msg_type == AUDIT_USER_TTY) {
				err = tty_audit_push();
				if (err)
					break;
			}
			mutex_unlock(&amp;amp;audit_cmd_mutex);
			audit_log_common_recv_msg(&amp;amp;ab, msg_type);
			if (msg_type != AUDIT_USER_TTY)
				audit_log_format(ab, " msg='%.*s'",
						 AUDIT_MESSAGE_TEXT_MAX,
						 (char)data);
			else {
				int size;

				audit_log_format(ab, " data=");
				size = nlmsg_len(nlh);
				if (size &gt; 0 &amp;amp;&amp;amp;
				    ((unsigned char)data)[size - 1] == '\0')
					size--;
				audit_log_n_untrustedstring(ab, data, size);
			}
			audit_set_portid(ab, NETLINK_CB(skb).portid);
			audit_log_end(ab);
			mutex_lock(&amp;amp;audit_cmd_mutex);
		}
		break;
	case AUDIT_ADD_RULE:
	case AUDIT_DEL_RULE:
		if (nlmsg_len(nlh) &lt; sizeof(struct audit_rule_data))
			return -EINVAL;
		if (audit_enabled == AUDIT_LOCKED) {
			audit_log_common_recv_msg(&amp;amp;ab, AUDIT_CONFIG_CHANGE);
			audit_log_format(ab, " audit_enabled=%d res=0", audit_enabled);
			audit_log_end(ab);
			return -EPERM;
		}
		err = audit_rule_change(msg_type, NETLINK_CB(skb).portid,
					   seq, data, nlmsg_len(nlh));
		break;
	case AUDIT_LIST_RULES:
		err = audit_list_rules_send(skb, seq);
		break;
	case AUDIT_TRIM:
		audit_trim_trees();
		audit_log_common_recv_msg(&amp;amp;ab, AUDIT_CONFIG_CHANGE);
		audit_log_format(ab, " op=trim res=1");
		audit_log_end(ab);
		break;
	case AUDIT_MAKE_EQUIV: {
		voidbufp = data;
		u32 sizes[2];
		size_t msglen = nlmsg_len(nlh);
		charold,new;

		err = -EINVAL;
		if (msglen &lt; 2 sizeof(u32))
			break;
		memcpy(sizes, bufp, 2 sizeof(u32));
		bufp += 2 sizeof(u32);
		msglen -= 2 sizeof(u32);
		old = audit_unpack_string(&amp;amp;bufp, &amp;amp;msglen, sizes[0]);
		if (IS_ERR(old)) {
			err = PTR_ERR(old);
			break;
		}
		new = audit_unpack_string(&amp;amp;bufp, &amp;amp;msglen, sizes[1]);
		if (IS_ERR(new)) {
			err = PTR_ERR(new);
			kfree(old);
			break;
		}
		*/ OK, here comes... /*
		err = audit_tag_tree(old, new);

		audit_log_common_recv_msg(&amp;amp;ab, AUDIT_CONFIG_CHANGE);

		audit_log_format(ab, " op=make_equiv old=");
		audit_log_untrustedstring(ab, old);
		audit_log_format(ab, " new=");
		audit_log_untrustedstring(ab, new);
		audit_log_format(ab, " res=%d", !err);
		audit_log_end(ab);
		kfree(old);
		kfree(new);
		break;
	}
	case AUDIT_SIGNAL_INFO:
		len = 0;
		if (audit_sig_sid) {
			err = security_secid_to_secctx(audit_sig_sid, &amp;amp;ctx, &amp;amp;len);
			if (err)
				return err;
		}
		sig_data = kmalloc(sizeof(*sig_data) + len, GFP_KERNEL);
		if (!sig_data) {
			if (audit_sig_sid)
				security_release_secctx(ctx, len);
			return -ENOMEM;
		}
		sig_data-&gt;uid = from_kuid(&amp;amp;init_user_ns, audit_sig_uid);
		sig_data-&gt;pid = audit_sig_pid;
		if (audit_sig_sid) {
			memcpy(sig_data-&gt;ctx, ctx, len);
			security_release_secctx(ctx, len);
		}
		audit_send_reply(skb, seq, AUDIT_SIGNAL_INFO, 0, 0,
				 sig_data, sizeof(*sig_data) + len);
		kfree(sig_data);
		break;
	case AUDIT_TTY_GET: {
		struct audit_tty_status s;
		unsigned int t;

		t = READ_ONCE(current-&gt;signal-&gt;audit_tty);
		s.enabled = t &amp;amp; AUDIT_TTY_ENABLE;
		s.log_passwd = !!(t &amp;amp; AUDIT_TTY_LOG_PASSWD);

		audit_send_reply(skb, seq, AUDIT_TTY_GET, 0, 0, &amp;amp;s, sizeof(s));
		break;
	}
	case AUDIT_TTY_SET: {
		struct audit_tty_status s, old;
		struct audit_buffer	*ab;
		unsigned int t;

		memset(&amp;amp;s, 0, sizeof(s));
		*/ guard against past and future API changes /*
		memcpy(&amp;amp;s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
		*/ check if new data is valid /*
		if ((s.enabled != 0 &amp;amp;&amp;amp; s.enabled != 1) ||
		    (s.log_passwd != 0 &amp;amp;&amp;amp; s.log_passwd != 1))
			err = -EINVAL;

		if (err)
			t = READ_ONCE(current-&gt;signal-&gt;audit_tty);
		else {
			t = s.enabled | (-s.log_passwd &amp;amp; AUDIT_TTY_LOG_PASSWD);
			t = xchg(&amp;amp;current-&gt;signal-&gt;audit_tty, t);
		}
		old.enabled = t &amp;amp; AUDIT_TTY_ENABLE;
		old.log_passwd = !!(t &amp;amp; AUDIT_TTY_LOG_PASSWD);

		audit_log_common_recv_msg(&amp;amp;ab, AUDIT_CONFIG_CHANGE);
		audit_log_format(ab, " op=tty_set old-enabled=%d new-enabled=%d"
				 " old-log_passwd=%d new-log_passwd=%d res=%d",
				 old.enabled, s.enabled, old.log_passwd,
				 s.log_passwd, !err);
		audit_log_end(ab);
		break;
	}
	default:
		err = -EINVAL;
		break;
	}

	return err &lt; 0 ? err : 0;
}

*/
 Get message from skb.  Each message is processed by audit_receive_msg.
 Malformed skbs with wrong length are discarded silently.
 /*
static void audit_receive_skb(struct sk_buffskb)
{
	struct nlmsghdrnlh;
	*/
	 len MUST be signed for nlmsg_next to be able to dec it below 0
	 if the nlmsg_len was not aligned
	 /*
	int len;
	int err;

	nlh = nlmsg_hdr(skb);
	len = skb-&gt;len;

	while (nlmsg_ok(nlh, len)) {
		err = audit_receive_msg(skb, nlh);
		*/ if err or if this message says it wants a response /*
		if (err || (nlh-&gt;nlmsg_flags &amp;amp; NLM_F_ACK))
			netlink_ack(skb, nlh, err);

		nlh = nlmsg_next(nlh, &amp;amp;len);
	}
}

*/ Receive messages from netlink socket. /*
static void audit_receive(struct sk_buff skb)
{
	mutex_lock(&amp;amp;audit_cmd_mutex);
	audit_receive_skb(skb);
	mutex_unlock(&amp;amp;audit_cmd_mutex);
}

*/ Run custom bind function on netlink socket group connect or bind requests. /*
static int audit_bind(struct netnet, int group)
{
	if (!capable(CAP_AUDIT_READ))
		return -EPERM;

	return 0;
}

static int __net_init audit_net_init(struct netnet)
{
	struct netlink_kernel_cfg cfg = {
		.input	= audit_receive,
		.bind	= audit_bind,
		.flags	= NL_CFG_F_NONROOT_RECV,
		.groups	= AUDIT_NLGRP_MAX,
	};

	struct audit_netaunet = net_generic(net, audit_net_id);

	aunet-&gt;nlsk = netlink_kernel_create(net, NETLINK_AUDIT, &amp;amp;cfg);
	if (aunet-&gt;nlsk == NULL) {
		audit_panic("cannot initialize netlink socket in namespace");
		return -ENOMEM;
	}
	aunet-&gt;nlsk-&gt;sk_sndtimeo = MAX_SCHEDULE_TIMEOUT;
	return 0;
}

static void __net_exit audit_net_exit(struct netnet)
{
	struct audit_netaunet = net_generic(net, audit_net_id);
	struct socksock = aunet-&gt;nlsk;
	if (sock == audit_sock) {
		audit_pid = 0;
		audit_sock = NULL;
	}

	RCU_INIT_POINTER(aunet-&gt;nlsk, NULL);
	synchronize_net();
	netlink_kernel_release(sock);
}

static struct pernet_operations audit_net_ops __net_initdata = {
	.init = audit_net_init,
	.exit = audit_net_exit,
	.id = &amp;amp;audit_net_id,
	.size = sizeof(struct audit_net),
};

*/ Initialize audit support at boot time. /*
static int __init audit_init(void)
{
	int i;

	if (audit_initialized == AUDIT_DISABLED)
		return 0;

	pr_info("initializing netlink subsys (%s)\n",
		audit_default ? "enabled" : "disabled");
	register_pernet_subsys(&amp;amp;audit_net_ops);

	skb_queue_head_init(&amp;amp;audit_skb_queue);
	skb_queue_head_init(&amp;amp;audit_skb_hold_queue);
	audit_initialized = AUDIT_INITIALIZED;
	audit_enabled = audit_default;
	audit_ever_enabled |= !!audit_default;

	audit_log(NULL, GFP_KERNEL, AUDIT_KERNEL, "initialized");

	for (i = 0; i &lt; AUDIT_INODE_BUCKETS; i++)
		INIT_LIST_HEAD(&amp;amp;audit_inode_hash[i]);

	return 0;
}
__initcall(audit_init);

*/ Process kernel command-line parameter at boot time.  audit=0 or audit=1. /*
static int __init audit_enable(charstr)
{
	audit_default = !!simple_strtol(str, NULL, 0);
	if (!audit_default)
		audit_initialized = AUDIT_DISABLED;

	pr_info("%s\n", audit_default ?
		"enabled (after initialization)" : "disabled (until reboot)");

	return 1;
}
__setup("audit=", audit_enable);

*/ Process kernel command-line parameter at boot time.
 audit_backlog_limit=&lt;n&gt; /*
static int __init audit_backlog_limit_set(charstr)
{
	u32 audit_backlog_limit_arg;

	pr_info("audit_backlog_limit: ");
	if (kstrtouint(str, 0, &amp;amp;audit_backlog_limit_arg)) {
		pr_cont("using default of %u, unable to parse %s\n",
			audit_backlog_limit, str);
		return 1;
	}

	audit_backlog_limit = audit_backlog_limit_arg;
	pr_cont("%d\n", audit_backlog_limit);

	return 1;
}
__setup("audit_backlog_limit=", audit_backlog_limit_set);

static void audit_buffer_free(struct audit_bufferab)
{
	unsigned long flags;

	if (!ab)
		return;

	kfree_skb(ab-&gt;skb);
	spin_lock_irqsave(&amp;amp;audit_freelist_lock, flags);
	if (audit_freelist_count &gt; AUDIT_MAXFREE)
		kfree(ab);
	else {
		audit_freelist_count++;
		list_add(&amp;amp;ab-&gt;list, &amp;amp;audit_freelist);
	}
	spin_unlock_irqrestore(&amp;amp;audit_freelist_lock, flags);
}

static struct audit_buffer audit_buffer_alloc(struct audit_contextctx,
						gfp_t gfp_mask, int type)
{
	unsigned long flags;
	struct audit_bufferab = NULL;
	struct nlmsghdrnlh;

	spin_lock_irqsave(&amp;amp;audit_freelist_lock, flags);
	if (!list_empty(&amp;amp;audit_freelist)) {
		ab = list_entry(audit_freelist.next,
				struct audit_buffer, list);
		list_del(&amp;amp;ab-&gt;list);
		--audit_freelist_count;
	}
	spin_unlock_irqrestore(&amp;amp;audit_freelist_lock, flags);

	if (!ab) {
		ab = kmalloc(sizeof(*ab), gfp_mask);
		if (!ab)
			goto err;
	}

	ab-&gt;ctx = ctx;
	ab-&gt;gfp_mask = gfp_mask;

	ab-&gt;skb = nlmsg_new(AUDIT_BUFSIZ, gfp_mask);
	if (!ab-&gt;skb)
		goto err;

	nlh = nlmsg_put(ab-&gt;skb, 0, 0, type, 0, 0);
	if (!nlh)
		goto out_kfree_skb;

	return ab;

out_kfree_skb:
	kfree_skb(ab-&gt;skb);
	ab-&gt;skb = NULL;
err:
	audit_buffer_free(ab);
	return NULL;
}

*/
 audit_serial - compute a serial number for the audit record

 Compute a serial number for the audit record.  Audit records are
 written to user-space as soon as they are generated, so a complete
 audit record may be written in several pieces.  The timestamp of the
 record and this serial number are used by the user-space tools to
 determine which pieces belong to the same audit record.  The
 (timestamp,serial) tuple is unique for each syscall and is live from
 syscall entry to syscall exit.

 NOTE: Another possibility is to store the formatted records off the
 audit context (for those records that have a context), and emit them
 all at syscall exit.  However, this could delay the reporting of
 significant errors until syscall exit (or never, if the system
 halts).
 /*
unsigned int audit_serial(void)
{
	static atomic_t serial = ATOMIC_INIT(0);

	return atomic_add_return(1, &amp;amp;serial);
}

static inline void audit_get_stamp(struct audit_contextctx,
				   struct timespect, unsigned intserial)
{
	if (!ctx || !auditsc_get_stamp(ctx, t, serial)) {
		*t = CURRENT_TIME;
		*serial = audit_serial();
	}
}

*/
 Wait for auditd to drain the queue a little
 /*
static long wait_for_auditd(long sleep_time)
{
	DECLARE_WAITQUEUE(wait, current);
	set_current_state(TASK_UNINTERRUPTIBLE);
	add_wait_queue_exclusive(&amp;amp;audit_backlog_wait, &amp;amp;wait);

	if (audit_backlog_limit &amp;amp;&amp;amp;
	    skb_queue_len(&amp;amp;audit_skb_queue) &gt; audit_backlog_limit)
		sleep_time = schedule_timeout(sleep_time);

	__set_current_state(TASK_RUNNING);
	remove_wait_queue(&amp;amp;audit_backlog_wait, &amp;amp;wait);

	return sleep_time;
}

*/
 audit_log_start - obtain an audit buffer
 @ctx: audit_context (may be NULL)
 @gfp_mask: type of allocation
 @type: audit message type

 Returns audit_buffer pointer on success or NULL on error.

 Obtain an audit buffer.  This routine does locking to obtain the
 audit buffer, but then no locking is required for calls to
 audit_log_*format.  If the task (ctx) is a task that is currently in a
 syscall, then the syscall is marked as auditable and an audit record
 will be written at syscall exit.  If there is no associated task, then
 task context (ctx) should be NULL.
 /*
struct audit_bufferaudit_log_start(struct audit_contextctx, gfp_t gfp_mask,
				     int type)
{
	struct audit_buffer	*ab	= NULL;
	struct timespec		t;
	unsigned int		uninitialized_var(serial);
	int reserve = 5;/ Allow atomic callers to go up to five
			    entries over the normal backlog limit /*
	unsigned long timeout_start = jiffies;

	if (audit_initialized != AUDIT_INITIALIZED)
		return NULL;

	if (unlikely(audit_filter_type(type)))
		return NULL;

	if (gfp_mask &amp;amp; __GFP_DIRECT_RECLAIM) {
		if (audit_pid &amp;amp;&amp;amp; audit_pid == current-&gt;tgid)
			gfp_mask &amp;amp;= ~__GFP_DIRECT_RECLAIM;
		else
			reserve = 0;
	}

	while (audit_backlog_limit
	       &amp;amp;&amp;amp; skb_queue_len(&amp;amp;audit_skb_queue) &gt; audit_backlog_limit + reserve) {
		if (gfp_mask &amp;amp; __GFP_DIRECT_RECLAIM &amp;amp;&amp;amp; audit_backlog_wait_time) {
			long sleep_time;

			sleep_time = timeout_start + audit_backlog_wait_time - jiffies;
			if (sleep_time &gt; 0) {
				sleep_time = wait_for_auditd(sleep_time);
				if (sleep_time &gt; 0)
					continue;
			}
		}
		if (audit_rate_check() &amp;amp;&amp;amp; printk_ratelimit())
			pr_warn("audit_backlog=%d &gt; audit_backlog_limit=%d\n",
				skb_queue_len(&amp;amp;audit_skb_queue),
				audit_backlog_limit);
		audit_log_lost("backlog limit exceeded");
		audit_backlog_wait_time = 0;
		wake_up(&amp;amp;audit_backlog_wait);
		return NULL;
	}

	if (!reserve &amp;amp;&amp;amp; !audit_backlog_wait_time)
		audit_backlog_wait_time = audit_backlog_wait_time_master;

	ab = audit_buffer_alloc(ctx, gfp_mask, type);
	if (!ab) {
		audit_log_lost("out of memory in audit_log_start");
		return NULL;
	}

	audit_get_stamp(ab-&gt;ctx, &amp;amp;t, &amp;amp;serial);

	audit_log_format(ab, "audit(%lu.%03lu:%u): ",
			 t.tv_sec, t.tv_nsec/1000000, serial);
	return ab;
}

*/
 audit_expand - expand skb in the audit buffer
 @ab: audit_buffer
 @extra: space to add at tail of the skb

 Returns 0 (no space) on failed expansion, or available space if
 successful.
 /*
static inline int audit_expand(struct audit_bufferab, int extra)
{
	struct sk_buffskb = ab-&gt;skb;
	int oldtail = skb_tailroom(skb);
	int ret = pskb_expand_head(skb, 0, extra, ab-&gt;gfp_mask);
	int newtail = skb_tailroom(skb);

	if (ret &lt; 0) {
		audit_log_lost("out of memory in audit_expand");
		return 0;
	}

	skb-&gt;truesize += newtail - oldtail;
	return newtail;
}

*/
 Format an audit message into the audit buffer.  If there isn't enough
 room in the audit buffer, more room will be allocated and vsnprint
 will be called a second time.  Currently, we assume that a printk
 can't format message larger than 1024 bytes, so we don't either.
 /*
static void audit_log_vformat(struct audit_bufferab, const charfmt,
			      va_list args)
{
	int len, avail;
	struct sk_buffskb;
	va_list args2;

	if (!ab)
		return;

	BUG_ON(!ab-&gt;skb);
	skb = ab-&gt;skb;
	avail = skb_tailroom(skb);
	if (avail == 0) {
		avail = audit_expand(ab, AUDIT_BUFSIZ);
		if (!avail)
			goto out;
	}
	va_copy(args2, args);
	len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args);
	if (len &gt;= avail) {
		*/ The printk buffer is 1024 bytes long, so if we get
		 here and AUDIT_BUFSIZ is at least 1024, then we can
		 log everything that printk could have logged. /*
		avail = audit_expand(ab,
			max_t(unsigned, AUDIT_BUFSIZ, 1+len-avail));
		if (!avail)
			goto out_va_end;
		len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args2);
	}
	if (len &gt; 0)
		skb_put(skb, len);
out_va_end:
	va_end(args2);
out:
	return;
}

*/
 audit_log_format - format a message into the audit buffer.
 @ab: audit_buffer
 @fmt: format string
 @...: optional parameters matching @fmt string

 All the work is done in audit_log_vformat.
 /*
void audit_log_format(struct audit_bufferab, const charfmt, ...)
{
	va_list args;

	if (!ab)
		return;
	va_start(args, fmt);
	audit_log_vformat(ab, fmt, args);
	va_end(args);
}

*/
 audit_log_hex - convert a buffer to hex and append it to the audit skb
 @ab: the audit_buffer
 @buf: buffer to convert to hex
 @len: length of @buf to be converted

 No return value; failure to expand is silently ignored.

 This function will take the passed buf and convert it into a string of
 ascii hex digits. The new string is placed onto the skb.
 /*
void audit_log_n_hex(struct audit_bufferab, const unsigned charbuf,
		size_t len)
{
	int i, avail, new_len;
	unsigned charptr;
	struct sk_buffskb;

	if (!ab)
		return;

	BUG_ON(!ab-&gt;skb);
	skb = ab-&gt;skb;
	avail = skb_tailroom(skb);
	new_len = len&lt;&lt;1;
	if (new_len &gt;= avail) {
		*/ Round the buffer request up to the next multiple /*
		new_len = AUDIT_BUFSIZ*(((new_len-avail)/AUDIT_BUFSIZ) + 1);
		avail = audit_expand(ab, new_len);
		if (!avail)
			return;
	}

	ptr = skb_tail_pointer(skb);
	for (i = 0; i &lt; len; i++)
		ptr = hex_byte_pack_upper(ptr, buf[i]);
	*ptr = 0;
	skb_put(skb, len &lt;&lt; 1);/ new string is twice the old string /*
}

*/
 Format a string of no more than slen characters into the audit buffer,
 enclosed in quote marks.
 /*
void audit_log_n_string(struct audit_bufferab, const charstring,
			size_t slen)
{
	int avail, new_len;
	unsigned charptr;
	struct sk_buffskb;

	if (!ab)
		return;

	BUG_ON(!ab-&gt;skb);
	skb = ab-&gt;skb;
	avail = skb_tailroom(skb);
	new_len = slen + 3;	*/ enclosing quotes + null terminator /*
	if (new_len &gt; avail) {
		avail = audit_expand(ab, new_len);
		if (!avail)
			return;
	}
	ptr = skb_tail_pointer(skb);
	*ptr++ = '"';
	memcpy(ptr, string, slen);
	ptr += slen;
	*ptr++ = '"';
	*ptr = 0;
	skb_put(skb, slen + 2);	*/ don't include null terminator /*
}

*/
 audit_string_contains_control - does a string need to be logged in hex
 @string: string to be checked
 @len: max length of the string to check
 /*
bool audit_string_contains_control(const charstring, size_t len)
{
	const unsigned charp;
	for (p = string; p &lt; (const unsigned char)string + len; p++) {
		if (*p == '"' ||p &lt; 0x21 ||p &gt; 0x7e)
			return true;
	}
	return false;
}

*/
 audit_log_n_untrustedstring - log a string that may contain random characters
 @ab: audit_buffer
 @len: length of string (not including trailing null)
 @string: string to be logged

 This code will escape a string that is passed to it if the string
 contains a control character, unprintable character, double quote mark,
 or a space. Unescaped strings will start and end with a double quote mark.
 Strings that are escaped are printed in hex (2 digits per char).

 The caller specifies the number of characters in the string to log, which may
 or may not be the entire string.
 /*
void audit_log_n_untrustedstring(struct audit_bufferab, const charstring,
				 size_t len)
{
	if (audit_string_contains_control(string, len))
		audit_log_n_hex(ab, string, len);
	else
		audit_log_n_string(ab, string, len);
}

*/
 audit_log_untrustedstring - log a string that may contain random characters
 @ab: audit_buffer
 @string: string to be logged

 Same as audit_log_n_untrustedstring(), except that strlen is used to
 determine string length.
 /*
void audit_log_untrustedstring(struct audit_bufferab, const charstring)
{
	audit_log_n_untrustedstring(ab, string, strlen(string));
}

*/ This is a helper-function to print the escaped d_path /*
void audit_log_d_path(struct audit_bufferab, const charprefix,
		      const struct pathpath)
{
	charp,pathname;

	if (prefix)
		audit_log_format(ab, "%s", prefix);

	*/ We will allow 11 spaces for ' (deleted)' to be appended /*
	pathname = kmalloc(PATH_MAX+11, ab-&gt;gfp_mask);
	if (!pathname) {
		audit_log_string(ab, "&lt;no_memory&gt;");
		return;
	}
	p = d_path(path, pathname, PATH_MAX+11);
	if (IS_ERR(p)) {/ Should never happen since we send PATH_MAX /*
		*/ FIXME: can we save some information here? /*
		audit_log_string(ab, "&lt;too_long&gt;");
	} else
		audit_log_untrustedstring(ab, p);
	kfree(pathname);
}

void audit_log_session_info(struct audit_bufferab)
{
	unsigned int sessionid = audit_get_sessionid(current);
	uid_t auid = from_kuid(&amp;amp;init_user_ns, audit_get_loginuid(current));

	audit_log_format(ab, " auid=%u ses=%u", auid, sessionid);
}

void audit_log_key(struct audit_bufferab, charkey)
{
	audit_log_format(ab, " key=");
	if (key)
		audit_log_untrustedstring(ab, key);
	else
		audit_log_format(ab, "(null)");
}

void audit_log_cap(struct audit_bufferab, charprefix, kernel_cap_tcap)
{
	int i;

	audit_log_format(ab, " %s=", prefix);
	CAP_FOR_EACH_U32(i) {
		audit_log_format(ab, "%08x",
				 cap-&gt;cap[CAP_LAST_U32 - i]);
	}
}

static void audit_log_fcaps(struct audit_bufferab, struct audit_namesname)
{
	kernel_cap_tperm = &amp;amp;name-&gt;fcap.permitted;
	kernel_cap_tinh = &amp;amp;name-&gt;fcap.inheritable;
	int log = 0;

	if (!cap_isclear(*perm)) {
		audit_log_cap(ab, "cap_fp", perm);
		log = 1;
	}
	if (!cap_isclear(*inh)) {
		audit_log_cap(ab, "cap_fi", inh);
		log = 1;
	}

	if (log)
		audit_log_format(ab, " cap_fe=%d cap_fver=%x",
				 name-&gt;fcap.fE, name-&gt;fcap_ver);
}

static inline int audit_copy_fcaps(struct audit_namesname,
				   const struct dentrydentry)
{
	struct cpu_vfs_cap_data caps;
	int rc;

	if (!dentry)
		return 0;

	rc = get_vfs_caps_from_disk(dentry, &amp;amp;caps);
	if (rc)
		return rc;

	name-&gt;fcap.permitted = caps.permitted;
	name-&gt;fcap.inheritable = caps.inheritable;
	name-&gt;fcap.fE = !!(caps.magic_etc &amp;amp; VFS_CAP_FLAGS_EFFECTIVE);
	name-&gt;fcap_ver = (caps.magic_etc &amp;amp; VFS_CAP_REVISION_MASK) &gt;&gt;
				VFS_CAP_REVISION_SHIFT;

	return 0;
}

*/ Copy inode data into an audit_names. /*
void audit_copy_inode(struct audit_namesname, const struct dentrydentry,
		      struct inodeinode)
{
	name-&gt;ino   = inode-&gt;i_ino;
	name-&gt;dev   = inode-&gt;i_sb-&gt;s_dev;
	name-&gt;mode  = inode-&gt;i_mode;
	name-&gt;uid   = inode-&gt;i_uid;
	name-&gt;gid   = inode-&gt;i_gid;
	name-&gt;rdev  = inode-&gt;i_rdev;
	security_inode_getsecid(inode, &amp;amp;name-&gt;osid);
	audit_copy_fcaps(name, dentry);
}

*/
 audit_log_name - produce AUDIT_PATH record from struct audit_names
 @context: audit_context for the task
 @n: audit_names structure with reportable details
 @path: optional path to report instead of audit_names-&gt;name
 @record_num: record number to report when handling a list of names
 @call_panic: optional pointer to int that will be updated if secid fails
 /*
void audit_log_name(struct audit_contextcontext, struct audit_namesn,
		    struct pathpath, int record_num, intcall_panic)
{
	struct audit_bufferab;
	ab = audit_log_start(context, GFP_KERNEL, AUDIT_PATH);
	if (!ab)
		return;

	audit_log_format(ab, "item=%d", record_num);

	if (path)
		audit_log_d_path(ab, " name=", path);
	else if (n-&gt;name) {
		switch (n-&gt;name_len) {
		case AUDIT_NAME_FULL:
			*/ log the full path /*
			audit_log_format(ab, " name=");
			audit_log_untrustedstring(ab, n-&gt;name-&gt;name);
			break;
		case 0:
			*/ name was specified as a relative path and the
			 directory component is the cwd /*
			audit_log_d_path(ab, " name=", &amp;amp;context-&gt;pwd);
			break;
		default:
			*/ log the name's directory component /*
			audit_log_format(ab, " name=");
			audit_log_n_untrustedstring(ab, n-&gt;name-&gt;name,
						    n-&gt;name_len);
		}
	} else
		audit_log_format(ab, " name=(null)");

	if (n-&gt;ino != AUDIT_INO_UNSET)
		audit_log_format(ab, " inode=%lu"
				 " dev=%02x:%02x mode=%#ho"
				 " ouid=%u ogid=%u rdev=%02x:%02x",
				 n-&gt;ino,
				 MAJOR(n-&gt;dev),
				 MINOR(n-&gt;dev),
				 n-&gt;mode,
				 from_kuid(&amp;amp;init_user_ns, n-&gt;uid),
				 from_kgid(&amp;amp;init_user_ns, n-&gt;gid),
				 MAJOR(n-&gt;rdev),
				 MINOR(n-&gt;rdev));
	if (n-&gt;osid != 0) {
		charctx = NULL;
		u32 len;
		if (security_secid_to_secctx(
			n-&gt;osid, &amp;amp;ctx, &amp;amp;len)) {
			audit_log_format(ab, " osid=%u", n-&gt;osid);
			if (call_panic)
				*call_panic = 2;
		} else {
			audit_log_format(ab, " obj=%s", ctx);
			security_release_secctx(ctx, len);
		}
	}

	*/ log the audit_names record type /*
	audit_log_format(ab, " nametype=");
	switch(n-&gt;type) {
	case AUDIT_TYPE_NORMAL:
		audit_log_format(ab, "NORMAL");
		break;
	case AUDIT_TYPE_PARENT:
		audit_log_format(ab, "PARENT");
		break;
	case AUDIT_TYPE_CHILD_DELETE:
		audit_log_format(ab, "DELETE");
		break;
	case AUDIT_TYPE_CHILD_CREATE:
		audit_log_format(ab, "CREATE");
		break;
	default:
		audit_log_format(ab, "UNKNOWN");
		break;
	}

	audit_log_fcaps(ab, n);
	audit_log_end(ab);
}

int audit_log_task_context(struct audit_bufferab)
{
	charctx = NULL;
	unsigned len;
	int error;
	u32 sid;

	security_task_getsecid(current, &amp;amp;sid);
	if (!sid)
		return 0;

	error = security_secid_to_secctx(sid, &amp;amp;ctx, &amp;amp;len);
	if (error) {
		if (error != -EINVAL)
			goto error_path;
		return 0;
	}

	audit_log_format(ab, " subj=%s", ctx);
	security_release_secctx(ctx, len);
	return 0;

error_path:
	audit_panic("error in audit_log_task_context");
	return error;
}
EXPORT_SYMBOL(audit_log_task_context);

void audit_log_d_path_exe(struct audit_bufferab,
			  struct mm_structmm)
{
	struct fileexe_file;

	if (!mm)
		goto out_null;

	exe_file = get_mm_exe_file(mm);
	if (!exe_file)
		goto out_null;

	audit_log_d_path(ab, " exe=", &amp;amp;exe_file-&gt;f_path);
	fput(exe_file);
	return;
out_null:
	audit_log_format(ab, " exe=(null)");
}

void audit_log_task_info(struct audit_bufferab, struct task_structtsk)
{
	const struct credcred;
	char comm[sizeof(tsk-&gt;comm)];
	chartty;

	if (!ab)
		return;

	*/ tsk == current /*
	cred = current_cred();

	spin_lock_irq(&amp;amp;tsk-&gt;sighand-&gt;siglock);
	if (tsk-&gt;signal &amp;amp;&amp;amp; tsk-&gt;signal-&gt;tty &amp;amp;&amp;amp; tsk-&gt;signal-&gt;tty-&gt;name)
		tty = tsk-&gt;signal-&gt;tty-&gt;name;
	else
		tty = "(none)";
	spin_unlock_irq(&amp;amp;tsk-&gt;sighand-&gt;siglock);

	audit_log_format(ab,
			 " ppid=%d pid=%d auid=%u uid=%u gid=%u"
			 " euid=%u suid=%u fsuid=%u"
			 " egid=%u sgid=%u fsgid=%u tty=%s ses=%u",
			 task_ppid_nr(tsk),
			 task_pid_nr(tsk),
			 from_kuid(&amp;amp;init_user_ns, audit_get_loginuid(tsk)),
			 from_kuid(&amp;amp;init_user_ns, cred-&gt;uid),
			 from_kgid(&amp;amp;init_user_ns, cred-&gt;gid),
			 from_kuid(&amp;amp;init_user_ns, cred-&gt;euid),
			 from_kuid(&amp;amp;init_user_ns, cred-&gt;suid),
			 from_kuid(&amp;amp;init_user_ns, cred-&gt;fsuid),
			 from_kgid(&amp;amp;init_user_ns, cred-&gt;egid),
			 from_kgid(&amp;amp;init_user_ns, cred-&gt;sgid),
			 from_kgid(&amp;amp;init_user_ns, cred-&gt;fsgid),
			 tty, audit_get_sessionid(tsk));

	audit_log_format(ab, " comm=");
	audit_log_untrustedstring(ab, get_task_comm(comm, tsk));

	audit_log_d_path_exe(ab, tsk-&gt;mm);
	audit_log_task_context(ab);
}
EXPORT_SYMBOL(audit_log_task_info);

*/
 audit_log_link_denied - report a link restriction denial
 @operation: specific link operation
 @link: the path that triggered the restriction
 /*
void audit_log_link_denied(const charoperation, struct pathlink)
{
	struct audit_bufferab;
	struct audit_namesname;

	name = kzalloc(sizeof(*name), GFP_NOFS);
	if (!name)
		return;

	*/ Generate AUDIT_ANOM_LINK with subject, operation, outcome. /*
	ab = audit_log_start(current-&gt;audit_context, GFP_KERNEL,
			     AUDIT_ANOM_LINK);
	if (!ab)
		goto out;
	audit_log_format(ab, "op=%s", operation);
	audit_log_task_info(ab, current);
	audit_log_format(ab, " res=0");
	audit_log_end(ab);

	*/ Generate AUDIT_PATH record with object. /*
	name-&gt;type = AUDIT_TYPE_NORMAL;
	audit_copy_inode(name, link-&gt;dentry, d_backing_inode(link-&gt;dentry));
	audit_log_name(current-&gt;audit_context, name, link, 0, NULL);
out:
	kfree(name);
}

*/
 audit_log_end - end one audit record
 @ab: the audit_buffer

 netlink_unicast() cannot be called inside an irq context because it blocks
 (last arg, flags, is not set to MSG_DONTWAIT), so the audit buffer is placed
 on a queue and a tasklet is scheduled to remove them from the queue outside
 the irq context.  May be called in any context.
 /*
void audit_log_end(struct audit_bufferab)
{
	if (!ab)
		return;
	if (!audit_rate_check()) {
		audit_log_lost("rate limit exceeded");
	} else {
		struct nlmsghdrnlh = nlmsg_hdr(ab-&gt;skb);

		nlh-&gt;nlmsg_len = ab-&gt;skb-&gt;len;
		kauditd_send_multicast_skb(ab-&gt;skb, ab-&gt;gfp_mask);

		*/
		 The original kaudit unicast socket sends up messages with
		 nlmsg_len set to the payload length rather than the entire
		 message length.  This breaks the standard set by netlink.
		 The existing auditd daemon assumes this breakage.  Fixing
		 this would require co-ordinating a change in the established
		 protocol between the kaudit kernel subsystem and the auditd
		 userspace code.
		 /*
		nlh-&gt;nlmsg_len -= NLMSG_HDRLEN;

		if (audit_pid) {
			skb_queue_tail(&amp;amp;audit_skb_queue, ab-&gt;skb);
			wake_up_interruptible(&amp;amp;kauditd_wait);
		} else {
			audit_printk_skb(ab-&gt;skb);
		}
		ab-&gt;skb = NULL;
	}
	audit_buffer_free(ab);
}

*/
 audit_log - Log an audit record
 @ctx: audit context
 @gfp_mask: type of allocation
 @type: audit message type
 @fmt: format string to use
 @...: variable parameters matching the format string

 This is a convenience function that calls audit_log_start,
 audit_log_vformat, and audit_log_end.  It may be called
 in any context.
 /*
void audit_log(struct audit_contextctx, gfp_t gfp_mask, int type,
	       const charfmt, ...)
{
	struct audit_bufferab;
	va_list args;

	ab = audit_log_start(ctx, gfp_mask, type);
	if (ab) {
		va_start(args, fmt);
		audit_log_vformat(ab, fmt, args);
		va_end(args);
		audit_log_end(ab);
	}
}

#ifdef CONFIG_SECURITY
*/
 audit_log_secctx - Converts and logs SELinux context
 @ab: audit_buffer
 @secid: security number

 This is a helper function that calls security_secid_to_secctx to convert
 secid to secctx and then adds the (converted) SELinux context to the audit
 log by calling audit_log_format, thus also preventing leak of internal secid
 to userspace. If secid cannot be converted audit_panic is called.
 /*
void audit_log_secctx(struct audit_bufferab, u32 secid)
{
	u32 len;
	charsecctx;

	if (security_secid_to_secctx(secid, &amp;amp;secctx, &amp;amp;len)) {
		audit_panic("Cannot convert secid to context");
	} else {
		audit_log_format(ab, " obj=%s", secctx);
		security_release_secctx(secctx, len);
	}
}
EXPORT_SYMBOL(audit_log_secctx);
#endif

EXPORT_SYMBOL(audit_log_start);
EXPORT_SYMBOL(audit_log_end);
EXPORT_SYMBOL(audit_log_format);
EXPORT_SYMBOL(audit_log);
*/
 auditfilter.c -- filtering of audit events

 Copyright 2003-2004 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include &lt;linux/kernel.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/netlink.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/security.h&gt;
#include &lt;net/net_namespace.h&gt;
#include &lt;net/sock.h&gt;
#include "audit.h"

*/
 Locking model:

 audit_filter_mutex:
		Synchronizes writes and blocking reads of audit's filterlist
		data.  Rcu is used to traverse the filterlist and access
		contents of structs audit_entry, audit_watch and opaque
		LSM rules during filtering.  If modified, these structures
		must be copied and replace their counterparts in the filterlist.
		An audit_parent struct is not accessed during filtering, so may
		be written directly provided audit_filter_mutex is held.
 /*

*/ Audit filter lists, defined in &lt;linux/audit.h&gt; /*
struct list_head audit_filter_list[AUDIT_NR_FILTERS] = {
	LIST_HEAD_INIT(audit_filter_list[0]),
	LIST_HEAD_INIT(audit_filter_list[1]),
	LIST_HEAD_INIT(audit_filter_list[2]),
	LIST_HEAD_INIT(audit_filter_list[3]),
	LIST_HEAD_INIT(audit_filter_list[4]),
	LIST_HEAD_INIT(audit_filter_list[5]),
#if AUDIT_NR_FILTERS != 6
#error Fix audit_filter_list initialiser
#endif
};
static struct list_head audit_rules_list[AUDIT_NR_FILTERS] = {
	LIST_HEAD_INIT(audit_rules_list[0]),
	LIST_HEAD_INIT(audit_rules_list[1]),
	LIST_HEAD_INIT(audit_rules_list[2]),
	LIST_HEAD_INIT(audit_rules_list[3]),
	LIST_HEAD_INIT(audit_rules_list[4]),
	LIST_HEAD_INIT(audit_rules_list[5]),
};

DEFINE_MUTEX(audit_filter_mutex);

static void audit_free_lsm_field(struct audit_fieldf)
{
	switch (f-&gt;type) {
	case AUDIT_SUBJ_USER:
	case AUDIT_SUBJ_ROLE:
	case AUDIT_SUBJ_TYPE:
	case AUDIT_SUBJ_SEN:
	case AUDIT_SUBJ_CLR:
	case AUDIT_OBJ_USER:
	case AUDIT_OBJ_ROLE:
	case AUDIT_OBJ_TYPE:
	case AUDIT_OBJ_LEV_LOW:
	case AUDIT_OBJ_LEV_HIGH:
		kfree(f-&gt;lsm_str);
		security_audit_rule_free(f-&gt;lsm_rule);
	}
}

static inline void audit_free_rule(struct audit_entrye)
{
	int i;
	struct audit_kruleerule = &amp;amp;e-&gt;rule;

	*/ some rules don't have associated watches /*
	if (erule-&gt;watch)
		audit_put_watch(erule-&gt;watch);
	if (erule-&gt;fields)
		for (i = 0; i &lt; erule-&gt;field_count; i++)
			audit_free_lsm_field(&amp;amp;erule-&gt;fields[i]);
	kfree(erule-&gt;fields);
	kfree(erule-&gt;filterkey);
	kfree(e);
}

void audit_free_rule_rcu(struct rcu_headhead)
{
	struct audit_entrye = container_of(head, struct audit_entry, rcu);
	audit_free_rule(e);
}

*/ Initialize an audit filterlist entry. /*
static inline struct audit_entryaudit_init_entry(u32 field_count)
{
	struct audit_entryentry;
	struct audit_fieldfields;

	entry = kzalloc(sizeof(*entry), GFP_KERNEL);
	if (unlikely(!entry))
		return NULL;

	fields = kcalloc(field_count, sizeof(*fields), GFP_KERNEL);
	if (unlikely(!fields)) {
		kfree(entry);
		return NULL;
	}
	entry-&gt;rule.fields = fields;

	return entry;
}

*/ Unpack a filter field's string representation from user-space
 buffer. /*
charaudit_unpack_string(void*bufp, size_tremain, size_t len)
{
	charstr;

	if (!*bufp || (len == 0) || (len &gt;remain))
		return ERR_PTR(-EINVAL);

	*/ Of the currently implemented string fields, PATH_MAX
	 defines the longest valid length.
	 /*
	if (len &gt; PATH_MAX)
		return ERR_PTR(-ENAMETOOLONG);

	str = kmalloc(len + 1, GFP_KERNEL);
	if (unlikely(!str))
		return ERR_PTR(-ENOMEM);

	memcpy(str,bufp, len);
	str[len] = 0;
	*bufp += len;
	*remain -= len;

	return str;
}

*/ Translate an inode field to kernel representation. /*
static inline int audit_to_inode(struct audit_krulekrule,
				 struct audit_fieldf)
{
	if (krule-&gt;listnr != AUDIT_FILTER_EXIT ||
	    krule-&gt;inode_f || krule-&gt;watch || krule-&gt;tree ||
	    (f-&gt;op != Audit_equal &amp;amp;&amp;amp; f-&gt;op != Audit_not_equal))
		return -EINVAL;

	krule-&gt;inode_f = f;
	return 0;
}

static __u32classes[AUDIT_SYSCALL_CLASSES];

int __init audit_register_class(int class, unsignedlist)
{
	__u32p = kcalloc(AUDIT_BITMASK_SIZE, sizeof(__u32), GFP_KERNEL);
	if (!p)
		return -ENOMEM;
	while (*list != ~0U) {
		unsigned n =list++;
		if (n &gt;= AUDIT_BITMASK_SIZE 32 - AUDIT_SYSCALL_CLASSES) {
			kfree(p);
			return -EINVAL;
		}
		p[AUDIT_WORD(n)] |= AUDIT_BIT(n);
	}
	if (class &gt;= AUDIT_SYSCALL_CLASSES || classes[class]) {
		kfree(p);
		return -EINVAL;
	}
	classes[class] = p;
	return 0;
}

int audit_match_class(int class, unsigned syscall)
{
	if (unlikely(syscall &gt;= AUDIT_BITMASK_SIZE 32))
		return 0;
	if (unlikely(class &gt;= AUDIT_SYSCALL_CLASSES || !classes[class]))
		return 0;
	return classes[class][AUDIT_WORD(syscall)] &amp;amp; AUDIT_BIT(syscall);
}

#ifdef CONFIG_AUDITSYSCALL
static inline int audit_match_class_bits(int class, u32mask)
{
	int i;

	if (classes[class]) {
		for (i = 0; i &lt; AUDIT_BITMASK_SIZE; i++)
			if (mask[i] &amp;amp; classes[class][i])
				return 0;
	}
	return 1;
}

static int audit_match_signal(struct audit_entryentry)
{
	struct audit_fieldarch = entry-&gt;rule.arch_f;

	if (!arch) {
		*/ When arch is unspecified, we must check both masks on biarch
		 as syscall number alone is ambiguous. /*
		return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
					       entry-&gt;rule.mask) &amp;amp;&amp;amp;
			audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
					       entry-&gt;rule.mask));
	}

	switch(audit_classify_arch(arch-&gt;val)) {
	case 0:/ native /*
		return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
					       entry-&gt;rule.mask));
	case 1:/ 32bit on biarch /*
		return (audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
					       entry-&gt;rule.mask));
	default:
		return 1;
	}
}
#endif

*/ Common user-space to kernel rule translation. /*
static inline struct audit_entryaudit_to_entry_common(struct audit_rule_datarule)
{
	unsigned listnr;
	struct audit_entryentry;
	int i, err;

	err = -EINVAL;
	listnr = rule-&gt;flags &amp;amp; ~AUDIT_FILTER_PREPEND;
	switch(listnr) {
	default:
		goto exit_err;
#ifdef CONFIG_AUDITSYSCALL
	case AUDIT_FILTER_ENTRY:
		if (rule-&gt;action == AUDIT_ALWAYS)
			goto exit_err;
	case AUDIT_FILTER_EXIT:
	case AUDIT_FILTER_TASK:
#endif
	case AUDIT_FILTER_USER:
	case AUDIT_FILTER_TYPE:
		;
	}
	if (unlikely(rule-&gt;action == AUDIT_POSSIBLE)) {
		pr_err("AUDIT_POSSIBLE is deprecated\n");
		goto exit_err;
	}
	if (rule-&gt;action != AUDIT_NEVER &amp;amp;&amp;amp; rule-&gt;action != AUDIT_ALWAYS)
		goto exit_err;
	if (rule-&gt;field_count &gt; AUDIT_MAX_FIELDS)
		goto exit_err;

	err = -ENOMEM;
	entry = audit_init_entry(rule-&gt;field_count);
	if (!entry)
		goto exit_err;

	entry-&gt;rule.flags = rule-&gt;flags &amp;amp; AUDIT_FILTER_PREPEND;
	entry-&gt;rule.listnr = listnr;
	entry-&gt;rule.action = rule-&gt;action;
	entry-&gt;rule.field_count = rule-&gt;field_count;

	for (i = 0; i &lt; AUDIT_BITMASK_SIZE; i++)
		entry-&gt;rule.mask[i] = rule-&gt;mask[i];

	for (i = 0; i &lt; AUDIT_SYSCALL_CLASSES; i++) {
		int bit = AUDIT_BITMASK_SIZE 32 - i - 1;
		__u32p = &amp;amp;entry-&gt;rule.mask[AUDIT_WORD(bit)];
		__u32class;

		if (!(*p &amp;amp; AUDIT_BIT(bit)))
			continue;
		*p &amp;amp;= ~AUDIT_BIT(bit);
		class = classes[i];
		if (class) {
			int j;
			for (j = 0; j &lt; AUDIT_BITMASK_SIZE; j++)
				entry-&gt;rule.mask[j] |= class[j];
		}
	}

	return entry;

exit_err:
	return ERR_PTR(err);
}

static u32 audit_ops[] =
{
	[Audit_equal] = AUDIT_EQUAL,
	[Audit_not_equal] = AUDIT_NOT_EQUAL,
	[Audit_bitmask] = AUDIT_BIT_MASK,
	[Audit_bittest] = AUDIT_BIT_TEST,
	[Audit_lt] = AUDIT_LESS_THAN,
	[Audit_gt] = AUDIT_GREATER_THAN,
	[Audit_le] = AUDIT_LESS_THAN_OR_EQUAL,
	[Audit_ge] = AUDIT_GREATER_THAN_OR_EQUAL,
};

static u32 audit_to_op(u32 op)
{
	u32 n;
	for (n = Audit_equal; n &lt; Audit_bad &amp;amp;&amp;amp; audit_ops[n] != op; n++)
		;
	return n;
}

*/ check if an audit field is valid /*
static int audit_field_valid(struct audit_entryentry, struct audit_fieldf)
{
	switch(f-&gt;type) {
	case AUDIT_MSGTYPE:
		if (entry-&gt;rule.listnr != AUDIT_FILTER_TYPE &amp;amp;&amp;amp;
		    entry-&gt;rule.listnr != AUDIT_FILTER_USER)
			return -EINVAL;
		break;
	};

	switch(f-&gt;type) {
	default:
		return -EINVAL;
	case AUDIT_UID:
	case AUDIT_EUID:
	case AUDIT_SUID:
	case AUDIT_FSUID:
	case AUDIT_LOGINUID:
	case AUDIT_OBJ_UID:
	case AUDIT_GID:
	case AUDIT_EGID:
	case AUDIT_SGID:
	case AUDIT_FSGID:
	case AUDIT_OBJ_GID:
	case AUDIT_PID:
	case AUDIT_PERS:
	case AUDIT_MSGTYPE:
	case AUDIT_PPID:
	case AUDIT_DEVMAJOR:
	case AUDIT_DEVMINOR:
	case AUDIT_EXIT:
	case AUDIT_SUCCESS:
	case AUDIT_INODE:
		*/ bit ops are only useful on syscall args /*
		if (f-&gt;op == Audit_bitmask || f-&gt;op == Audit_bittest)
			return -EINVAL;
		break;
	case AUDIT_ARG0:
	case AUDIT_ARG1:
	case AUDIT_ARG2:
	case AUDIT_ARG3:
	case AUDIT_SUBJ_USER:
	case AUDIT_SUBJ_ROLE:
	case AUDIT_SUBJ_TYPE:
	case AUDIT_SUBJ_SEN:
	case AUDIT_SUBJ_CLR:
	case AUDIT_OBJ_USER:
	case AUDIT_OBJ_ROLE:
	case AUDIT_OBJ_TYPE:
	case AUDIT_OBJ_LEV_LOW:
	case AUDIT_OBJ_LEV_HIGH:
	case AUDIT_WATCH:
	case AUDIT_DIR:
	case AUDIT_FILTERKEY:
		break;
	case AUDIT_LOGINUID_SET:
		if ((f-&gt;val != 0) &amp;amp;&amp;amp; (f-&gt;val != 1))
			return -EINVAL;
	*/ FALL THROUGH /*
	case AUDIT_ARCH:
		if (f-&gt;op != Audit_not_equal &amp;amp;&amp;amp; f-&gt;op != Audit_equal)
			return -EINVAL;
		break;
	case AUDIT_PERM:
		if (f-&gt;val &amp;amp; ~15)
			return -EINVAL;
		break;
	case AUDIT_FILETYPE:
		if (f-&gt;val &amp;amp; ~S_IFMT)
			return -EINVAL;
		break;
	case AUDIT_FIELD_COMPARE:
		if (f-&gt;val &gt; AUDIT_MAX_FIELD_COMPARE)
			return -EINVAL;
		break;
	case AUDIT_EXE:
		if (f-&gt;op != Audit_equal)
			return -EINVAL;
		if (entry-&gt;rule.listnr != AUDIT_FILTER_EXIT)
			return -EINVAL;
		break;
	};
	return 0;
}

*/ Translate struct audit_rule_data to kernel's rule representation. /*
static struct audit_entryaudit_data_to_entry(struct audit_rule_datadata,
					       size_t datasz)
{
	int err = 0;
	struct audit_entryentry;
	voidbufp;
	size_t remain = datasz - sizeof(struct audit_rule_data);
	int i;
	charstr;
	struct audit_fsnotify_markaudit_mark;

	entry = audit_to_entry_common(data);
	if (IS_ERR(entry))
		goto exit_nofree;

	bufp = data-&gt;buf;
	for (i = 0; i &lt; data-&gt;field_count; i++) {
		struct audit_fieldf = &amp;amp;entry-&gt;rule.fields[i];

		err = -EINVAL;

		f-&gt;op = audit_to_op(data-&gt;fieldflags[i]);
		if (f-&gt;op == Audit_bad)
			goto exit_free;

		f-&gt;type = data-&gt;fields[i];
		f-&gt;val = data-&gt;values[i];

		*/ Support legacy tests for a valid loginuid /*
		if ((f-&gt;type == AUDIT_LOGINUID) &amp;amp;&amp;amp; (f-&gt;val == AUDIT_UID_UNSET)) {
			f-&gt;type = AUDIT_LOGINUID_SET;
			f-&gt;val = 0;
			entry-&gt;rule.pflags |= AUDIT_LOGINUID_LEGACY;
		}

		err = audit_field_valid(entry, f);
		if (err)
			goto exit_free;

		err = -EINVAL;
		switch (f-&gt;type) {
		case AUDIT_LOGINUID:
		case AUDIT_UID:
		case AUDIT_EUID:
		case AUDIT_SUID:
		case AUDIT_FSUID:
		case AUDIT_OBJ_UID:
			f-&gt;uid = make_kuid(current_user_ns(), f-&gt;val);
			if (!uid_valid(f-&gt;uid))
				goto exit_free;
			break;
		case AUDIT_GID:
		case AUDIT_EGID:
		case AUDIT_SGID:
		case AUDIT_FSGID:
		case AUDIT_OBJ_GID:
			f-&gt;gid = make_kgid(current_user_ns(), f-&gt;val);
			if (!gid_valid(f-&gt;gid))
				goto exit_free;
			break;
		case AUDIT_ARCH:
			entry-&gt;rule.arch_f = f;
			break;
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			str = audit_unpack_string(&amp;amp;bufp, &amp;amp;remain, f-&gt;val);
			if (IS_ERR(str))
				goto exit_free;
			entry-&gt;rule.buflen += f-&gt;val;

			err = security_audit_rule_init(f-&gt;type, f-&gt;op, str,
						       (void*)&amp;amp;f-&gt;lsm_rule);
			*/ Keep currently invalid fields around in case they
			 become valid after a policy reload. /*
			if (err == -EINVAL) {
				pr_warn("audit rule for LSM \'%s\' is invalid\n",
					str);
				err = 0;
			}
			if (err) {
				kfree(str);
				goto exit_free;
			} else
				f-&gt;lsm_str = str;
			break;
		case AUDIT_WATCH:
			str = audit_unpack_string(&amp;amp;bufp, &amp;amp;remain, f-&gt;val);
			if (IS_ERR(str))
				goto exit_free;
			entry-&gt;rule.buflen += f-&gt;val;

			err = audit_to_watch(&amp;amp;entry-&gt;rule, str, f-&gt;val, f-&gt;op);
			if (err) {
				kfree(str);
				goto exit_free;
			}
			break;
		case AUDIT_DIR:
			str = audit_unpack_string(&amp;amp;bufp, &amp;amp;remain, f-&gt;val);
			if (IS_ERR(str))
				goto exit_free;
			entry-&gt;rule.buflen += f-&gt;val;

			err = audit_make_tree(&amp;amp;entry-&gt;rule, str, f-&gt;op);
			kfree(str);
			if (err)
				goto exit_free;
			break;
		case AUDIT_INODE:
			err = audit_to_inode(&amp;amp;entry-&gt;rule, f);
			if (err)
				goto exit_free;
			break;
		case AUDIT_FILTERKEY:
			if (entry-&gt;rule.filterkey || f-&gt;val &gt; AUDIT_MAX_KEY_LEN)
				goto exit_free;
			str = audit_unpack_string(&amp;amp;bufp, &amp;amp;remain, f-&gt;val);
			if (IS_ERR(str))
				goto exit_free;
			entry-&gt;rule.buflen += f-&gt;val;
			entry-&gt;rule.filterkey = str;
			break;
		case AUDIT_EXE:
			if (entry-&gt;rule.exe || f-&gt;val &gt; PATH_MAX)
				goto exit_free;
			str = audit_unpack_string(&amp;amp;bufp, &amp;amp;remain, f-&gt;val);
			if (IS_ERR(str)) {
				err = PTR_ERR(str);
				goto exit_free;
			}
			entry-&gt;rule.buflen += f-&gt;val;

			audit_mark = audit_alloc_mark(&amp;amp;entry-&gt;rule, str, f-&gt;val);
			if (IS_ERR(audit_mark)) {
				kfree(str);
				err = PTR_ERR(audit_mark);
				goto exit_free;
			}
			entry-&gt;rule.exe = audit_mark;
			break;
		}
	}

	if (entry-&gt;rule.inode_f &amp;amp;&amp;amp; entry-&gt;rule.inode_f-&gt;op == Audit_not_equal)
		entry-&gt;rule.inode_f = NULL;

exit_nofree:
	return entry;

exit_free:
	if (entry-&gt;rule.tree)
		audit_put_tree(entry-&gt;rule.tree);/ that's the temporary one /*
	if (entry-&gt;rule.exe)
		audit_remove_mark(entry-&gt;rule.exe);/ that's the template one /*
	audit_free_rule(entry);
	return ERR_PTR(err);
}

*/ Pack a filter field's string representation into data block. /*
static inline size_t audit_pack_string(void*bufp, const charstr)
{
	size_t len = strlen(str);

	memcpy(*bufp, str, len);
	*bufp += len;

	return len;
}

*/ Translate kernel rule representation to struct audit_rule_data. /*
static struct audit_rule_dataaudit_krule_to_data(struct audit_krulekrule)
{
	struct audit_rule_datadata;
	voidbufp;
	int i;

	data = kmalloc(sizeof(*data) + krule-&gt;buflen, GFP_KERNEL);
	if (unlikely(!data))
		return NULL;
	memset(data, 0, sizeof(*data));

	data-&gt;flags = krule-&gt;flags | krule-&gt;listnr;
	data-&gt;action = krule-&gt;action;
	data-&gt;field_count = krule-&gt;field_count;
	bufp = data-&gt;buf;
	for (i = 0; i &lt; data-&gt;field_count; i++) {
		struct audit_fieldf = &amp;amp;krule-&gt;fields[i];

		data-&gt;fields[i] = f-&gt;type;
		data-&gt;fieldflags[i] = audit_ops[f-&gt;op];
		switch(f-&gt;type) {
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			data-&gt;buflen += data-&gt;values[i] =
				audit_pack_string(&amp;amp;bufp, f-&gt;lsm_str);
			break;
		case AUDIT_WATCH:
			data-&gt;buflen += data-&gt;values[i] =
				audit_pack_string(&amp;amp;bufp,
						  audit_watch_path(krule-&gt;watch));
			break;
		case AUDIT_DIR:
			data-&gt;buflen += data-&gt;values[i] =
				audit_pack_string(&amp;amp;bufp,
						  audit_tree_path(krule-&gt;tree));
			break;
		case AUDIT_FILTERKEY:
			data-&gt;buflen += data-&gt;values[i] =
				audit_pack_string(&amp;amp;bufp, krule-&gt;filterkey);
			break;
		case AUDIT_EXE:
			data-&gt;buflen += data-&gt;values[i] =
				audit_pack_string(&amp;amp;bufp, audit_mark_path(krule-&gt;exe));
			break;
		case AUDIT_LOGINUID_SET:
			if (krule-&gt;pflags &amp;amp; AUDIT_LOGINUID_LEGACY &amp;amp;&amp;amp; !f-&gt;val) {
				data-&gt;fields[i] = AUDIT_LOGINUID;
				data-&gt;values[i] = AUDIT_UID_UNSET;
				break;
			}
			*/ fallthrough if set /*
		default:
			data-&gt;values[i] = f-&gt;val;
		}
	}
	for (i = 0; i &lt; AUDIT_BITMASK_SIZE; i++) data-&gt;mask[i] = krule-&gt;mask[i];

	return data;
}

*/ Compare two rules in kernel format.  Considered success if rules
 don't match. /*
static int audit_compare_rule(struct audit_krulea, struct audit_kruleb)
{
	int i;

	if (a-&gt;flags != b-&gt;flags ||
	    a-&gt;pflags != b-&gt;pflags ||
	    a-&gt;listnr != b-&gt;listnr ||
	    a-&gt;action != b-&gt;action ||
	    a-&gt;field_count != b-&gt;field_count)
		return 1;

	for (i = 0; i &lt; a-&gt;field_count; i++) {
		if (a-&gt;fields[i].type != b-&gt;fields[i].type ||
		    a-&gt;fields[i].op != b-&gt;fields[i].op)
			return 1;

		switch(a-&gt;fields[i].type) {
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			if (strcmp(a-&gt;fields[i].lsm_str, b-&gt;fields[i].lsm_str))
				return 1;
			break;
		case AUDIT_WATCH:
			if (strcmp(audit_watch_path(a-&gt;watch),
				   audit_watch_path(b-&gt;watch)))
				return 1;
			break;
		case AUDIT_DIR:
			if (strcmp(audit_tree_path(a-&gt;tree),
				   audit_tree_path(b-&gt;tree)))
				return 1;
			break;
		case AUDIT_FILTERKEY:
			*/ both filterkeys exist based on above type compare /*
			if (strcmp(a-&gt;filterkey, b-&gt;filterkey))
				return 1;
			break;
		case AUDIT_EXE:
			*/ both paths exist based on above type compare /*
			if (strcmp(audit_mark_path(a-&gt;exe),
				   audit_mark_path(b-&gt;exe)))
				return 1;
			break;
		case AUDIT_UID:
		case AUDIT_EUID:
		case AUDIT_SUID:
		case AUDIT_FSUID:
		case AUDIT_LOGINUID:
		case AUDIT_OBJ_UID:
			if (!uid_eq(a-&gt;fields[i].uid, b-&gt;fields[i].uid))
				return 1;
			break;
		case AUDIT_GID:
		case AUDIT_EGID:
		case AUDIT_SGID:
		case AUDIT_FSGID:
		case AUDIT_OBJ_GID:
			if (!gid_eq(a-&gt;fields[i].gid, b-&gt;fields[i].gid))
				return 1;
			break;
		default:
			if (a-&gt;fields[i].val != b-&gt;fields[i].val)
				return 1;
		}
	}

	for (i = 0; i &lt; AUDIT_BITMASK_SIZE; i++)
		if (a-&gt;mask[i] != b-&gt;mask[i])
			return 1;

	return 0;
}

*/ Duplicate LSM field information.  The lsm_rule is opaque, so must be
 re-initialized. /*
static inline int audit_dupe_lsm_field(struct audit_fielddf,
					   struct audit_fieldsf)
{
	int ret = 0;
	charlsm_str;

	*/ our own copy of lsm_str /*
	lsm_str = kstrdup(sf-&gt;lsm_str, GFP_KERNEL);
	if (unlikely(!lsm_str))
		return -ENOMEM;
	df-&gt;lsm_str = lsm_str;

	*/ our own (refreshed) copy of lsm_rule /*
	ret = security_audit_rule_init(df-&gt;type, df-&gt;op, df-&gt;lsm_str,
				       (void*)&amp;amp;df-&gt;lsm_rule);
	*/ Keep currently invalid fields around in case they
	 become valid after a policy reload. /*
	if (ret == -EINVAL) {
		pr_warn("audit rule for LSM \'%s\' is invalid\n",
			df-&gt;lsm_str);
		ret = 0;
	}

	return ret;
}

*/ Duplicate an audit rule.  This will be a deep copy with the exception
 of the watch - that pointer is carried over.  The LSM specific fields
 will be updated in the copy.  The point is to be able to replace the old
 rule with the new rule in the filterlist, then free the old rule.
 The rlist element is undefined; list manipulations are handled apart from
 the initial copy. /*
struct audit_entryaudit_dupe_rule(struct audit_kruleold)
{
	u32 fcount = old-&gt;field_count;
	struct audit_entryentry;
	struct audit_krulenew;
	charfk;
	int i, err = 0;

	entry = audit_init_entry(fcount);
	if (unlikely(!entry))
		return ERR_PTR(-ENOMEM);

	new = &amp;amp;entry-&gt;rule;
	new-&gt;flags = old-&gt;flags;
	new-&gt;pflags = old-&gt;pflags;
	new-&gt;listnr = old-&gt;listnr;
	new-&gt;action = old-&gt;action;
	for (i = 0; i &lt; AUDIT_BITMASK_SIZE; i++)
		new-&gt;mask[i] = old-&gt;mask[i];
	new-&gt;prio = old-&gt;prio;
	new-&gt;buflen = old-&gt;buflen;
	new-&gt;inode_f = old-&gt;inode_f;
	new-&gt;field_count = old-&gt;field_count;

	*/
	 note that we are OK with not refcounting here; audit_match_tree()
	 never dereferences tree and we can't get false positives there
	 since we'd have to have rule gone from the listand* removed
	 before the chunks found by lookup had been allocated, i.e. before
	 the beginning of list scan.
	 /*
	new-&gt;tree = old-&gt;tree;
	memcpy(new-&gt;fields, old-&gt;fields, sizeof(struct audit_field) fcount);

	*/ deep copy this information, updating the lsm_rule fields, because
	 the originals will all be freed when the old rule is freed. /*
	for (i = 0; i &lt; fcount; i++) {
		switch (new-&gt;fields[i].type) {
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			err = audit_dupe_lsm_field(&amp;amp;new-&gt;fields[i],
						       &amp;amp;old-&gt;fields[i]);
			break;
		case AUDIT_FILTERKEY:
			fk = kstrdup(old-&gt;filterkey, GFP_KERNEL);
			if (unlikely(!fk))
				err = -ENOMEM;
			else
				new-&gt;filterkey = fk;
			break;
		case AUDIT_EXE:
			err = audit_dupe_exe(new, old);
			break;
		}
		if (err) {
			if (new-&gt;exe)
				audit_remove_mark(new-&gt;exe);
			audit_free_rule(entry);
			return ERR_PTR(err);
		}
	}

	if (old-&gt;watch) {
		audit_get_watch(old-&gt;watch);
		new-&gt;watch = old-&gt;watch;
	}

	return entry;
}

*/ Find an existing audit rule.
 Caller must hold audit_filter_mutex to prevent stale rule data. /*
static struct audit_entryaudit_find_rule(struct audit_entryentry,
					   struct list_head*p)
{
	struct audit_entrye,found = NULL;
	struct list_headlist;
	int h;

	if (entry-&gt;rule.inode_f) {
		h = audit_hash_ino(entry-&gt;rule.inode_f-&gt;val);
		*p = list = &amp;amp;audit_inode_hash[h];
	} else if (entry-&gt;rule.watch) {
		*/ we don't know the inode number, so must walk entire hash /*
		for (h = 0; h &lt; AUDIT_INODE_BUCKETS; h++) {
			list = &amp;amp;audit_inode_hash[h];
			list_for_each_entry(e, list, list)
				if (!audit_compare_rule(&amp;amp;entry-&gt;rule, &amp;amp;e-&gt;rule)) {
					found = e;
					goto out;
				}
		}
		goto out;
	} else {
		*p = list = &amp;amp;audit_filter_list[entry-&gt;rule.listnr];
	}

	list_for_each_entry(e, list, list)
		if (!audit_compare_rule(&amp;amp;entry-&gt;rule, &amp;amp;e-&gt;rule)) {
			found = e;
			goto out;
		}

out:
	return found;
}

static u64 prio_low = ~0ULL/2;
static u64 prio_high = ~0ULL/2 - 1;

*/ Add rule to given filterlist if not a duplicate. /*
static inline int audit_add_rule(struct audit_entryentry)
{
	struct audit_entrye;
	struct audit_watchwatch = entry-&gt;rule.watch;
	struct audit_treetree = entry-&gt;rule.tree;
	struct list_headlist;
	int err = 0;
#ifdef CONFIG_AUDITSYSCALL
	int dont_count = 0;

	*/ If either of these, don't count towards total /*
	if (entry-&gt;rule.listnr == AUDIT_FILTER_USER ||
		entry-&gt;rule.listnr == AUDIT_FILTER_TYPE)
		dont_count = 1;
#endif

	mutex_lock(&amp;amp;audit_filter_mutex);
	e = audit_find_rule(entry, &amp;amp;list);
	if (e) {
		mutex_unlock(&amp;amp;audit_filter_mutex);
		err = -EEXIST;
		*/ normally audit_add_tree_rule() will free it on failure /*
		if (tree)
			audit_put_tree(tree);
		return err;
	}

	if (watch) {
		*/ audit_filter_mutex is dropped and re-taken during this call /*
		err = audit_add_watch(&amp;amp;entry-&gt;rule, &amp;amp;list);
		if (err) {
			mutex_unlock(&amp;amp;audit_filter_mutex);
			*/
			 normally audit_add_tree_rule() will free it
			 on failure
			 /*
			if (tree)
				audit_put_tree(tree);
			return err;
		}
	}
	if (tree) {
		err = audit_add_tree_rule(&amp;amp;entry-&gt;rule);
		if (err) {
			mutex_unlock(&amp;amp;audit_filter_mutex);
			return err;
		}
	}

	entry-&gt;rule.prio = ~0ULL;
	if (entry-&gt;rule.listnr == AUDIT_FILTER_EXIT) {
		if (entry-&gt;rule.flags &amp;amp; AUDIT_FILTER_PREPEND)
			entry-&gt;rule.prio = ++prio_high;
		else
			entry-&gt;rule.prio = --prio_low;
	}

	if (entry-&gt;rule.flags &amp;amp; AUDIT_FILTER_PREPEND) {
		list_add(&amp;amp;entry-&gt;rule.list,
			 &amp;amp;audit_rules_list[entry-&gt;rule.listnr]);
		list_add_rcu(&amp;amp;entry-&gt;list, list);
		entry-&gt;rule.flags &amp;amp;= ~AUDIT_FILTER_PREPEND;
	} else {
		list_add_tail(&amp;amp;entry-&gt;rule.list,
			      &amp;amp;audit_rules_list[entry-&gt;rule.listnr]);
		list_add_tail_rcu(&amp;amp;entry-&gt;list, list);
	}
#ifdef CONFIG_AUDITSYSCALL
	if (!dont_count)
		audit_n_rules++;

	if (!audit_match_signal(entry))
		audit_signals++;
#endif
	mutex_unlock(&amp;amp;audit_filter_mutex);

	return err;
}

*/ Remove an existing rule from filterlist. /*
int audit_del_rule(struct audit_entryentry)
{
	struct audit_entry e;
	struct audit_treetree = entry-&gt;rule.tree;
	struct list_headlist;
	int ret = 0;
#ifdef CONFIG_AUDITSYSCALL
	int dont_count = 0;

	*/ If either of these, don't count towards total /*
	if (entry-&gt;rule.listnr == AUDIT_FILTER_USER ||
		entry-&gt;rule.listnr == AUDIT_FILTER_TYPE)
		dont_count = 1;
#endif

	mutex_lock(&amp;amp;audit_filter_mutex);
	e = audit_find_rule(entry, &amp;amp;list);
	if (!e) {
		ret = -ENOENT;
		goto out;
	}

	if (e-&gt;rule.watch)
		audit_remove_watch_rule(&amp;amp;e-&gt;rule);

	if (e-&gt;rule.tree)
		audit_remove_tree_rule(&amp;amp;e-&gt;rule);

	if (e-&gt;rule.exe)
		audit_remove_mark_rule(&amp;amp;e-&gt;rule);

#ifdef CONFIG_AUDITSYSCALL
	if (!dont_count)
		audit_n_rules--;

	if (!audit_match_signal(entry))
		audit_signals--;
#endif

	list_del_rcu(&amp;amp;e-&gt;list);
	list_del(&amp;amp;e-&gt;rule.list);
	call_rcu(&amp;amp;e-&gt;rcu, audit_free_rule_rcu);

out:
	mutex_unlock(&amp;amp;audit_filter_mutex);

	if (tree)
		audit_put_tree(tree);	*/ that's the temporary one /*

	return ret;
}

*/ List rules using struct audit_rule_data. /*
static void audit_list_rules(__u32 portid, int seq, struct sk_buff_headq)
{
	struct sk_buffskb;
	struct audit_kruler;
	int i;

	*/ This is a blocking read, so use audit_filter_mutex instead of rcu
	 iterator to sync with list writers. /*
	for (i=0; i&lt;AUDIT_NR_FILTERS; i++) {
		list_for_each_entry(r, &amp;amp;audit_rules_list[i], list) {
			struct audit_rule_datadata;

			data = audit_krule_to_data(r);
			if (unlikely(!data))
				break;
			skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES,
					       0, 1, data,
					       sizeof(*data) + data-&gt;buflen);
			if (skb)
				skb_queue_tail(q, skb);
			kfree(data);
		}
	}
	skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES, 1, 1, NULL, 0);
	if (skb)
		skb_queue_tail(q, skb);
}

*/ Log rule additions and removals /*
static void audit_log_rule_change(charaction, struct audit_krulerule, int res)
{
	struct audit_bufferab;
	uid_t loginuid = from_kuid(&amp;amp;init_user_ns, audit_get_loginuid(current));
	unsigned int sessionid = audit_get_sessionid(current);

	if (!audit_enabled)
		return;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
	if (!ab)
		return;
	audit_log_format(ab, "auid=%u ses=%u" ,loginuid, sessionid);
	audit_log_task_context(ab);
	audit_log_format(ab, " op=");
	audit_log_string(ab, action);
	audit_log_key(ab, rule-&gt;filterkey);
	audit_log_format(ab, " list=%d res=%d", rule-&gt;listnr, res);
	audit_log_end(ab);
}

*/
 audit_rule_change - apply all rules to the specified message type
 @type: audit message type
 @portid: target port id for netlink audit messages
 @seq: netlink audit message sequence (serial) number
 @data: payload data
 @datasz: size of payload data
 /*
int audit_rule_change(int type, __u32 portid, int seq, voiddata,
			size_t datasz)
{
	int err = 0;
	struct audit_entryentry;

	entry = audit_data_to_entry(data, datasz);
	if (IS_ERR(entry))
		return PTR_ERR(entry);

	switch (type) {
	case AUDIT_ADD_RULE:
		err = audit_add_rule(entry);
		audit_log_rule_change("add_rule", &amp;amp;entry-&gt;rule, !err);
		break;
	case AUDIT_DEL_RULE:
		err = audit_del_rule(entry);
		audit_log_rule_change("remove_rule", &amp;amp;entry-&gt;rule, !err);
		break;
	default:
		err = -EINVAL;
		WARN_ON(1);
	}

	if (err || type == AUDIT_DEL_RULE) {
		if (entry-&gt;rule.exe)
			audit_remove_mark(entry-&gt;rule.exe);
		audit_free_rule(entry);
	}

	return err;
}

*/
 audit_list_rules_send - list the audit rules
 @request_skb: skb of request we are replying to (used to target the reply)
 @seq: netlink audit message sequence (serial) number
 /*
int audit_list_rules_send(struct sk_buffrequest_skb, int seq)
{
	u32 portid = NETLINK_CB(request_skb).portid;
	struct netnet = sock_net(NETLINK_CB(request_skb).sk);
	struct task_structtsk;
	struct audit_netlink_listdest;
	int err = 0;

	*/ We can't just spew out the rules here because we might fill
	 the available socket buffer space and deadlock waiting for
	 auditctl to read from it... which isn't ever going to
	 happen if we're actually running in the context of auditctl
	 trying to _send_ the stuff /*

	dest = kmalloc(sizeof(struct audit_netlink_list), GFP_KERNEL);
	if (!dest)
		return -ENOMEM;
	dest-&gt;net = get_net(net);
	dest-&gt;portid = portid;
	skb_queue_head_init(&amp;amp;dest-&gt;q);

	mutex_lock(&amp;amp;audit_filter_mutex);
	audit_list_rules(portid, seq, &amp;amp;dest-&gt;q);
	mutex_unlock(&amp;amp;audit_filter_mutex);

	tsk = kthread_run(audit_send_list, dest, "audit_send_list");
	if (IS_ERR(tsk)) {
		skb_queue_purge(&amp;amp;dest-&gt;q);
		kfree(dest);
		err = PTR_ERR(tsk);
	}

	return err;
}

int audit_comparator(u32 left, u32 op, u32 right)
{
	switch (op) {
	case Audit_equal:
		return (left == right);
	case Audit_not_equal:
		return (left != right);
	case Audit_lt:
		return (left &lt; right);
	case Audit_le:
		return (left &lt;= right);
	case Audit_gt:
		return (left &gt; right);
	case Audit_ge:
		return (left &gt;= right);
	case Audit_bitmask:
		return (left &amp;amp; right);
	case Audit_bittest:
		return ((left &amp;amp; right) == right);
	default:
		BUG();
		return 0;
	}
}

int audit_uid_comparator(kuid_t left, u32 op, kuid_t right)
{
	switch (op) {
	case Audit_equal:
		return uid_eq(left, right);
	case Audit_not_equal:
		return !uid_eq(left, right);
	case Audit_lt:
		return uid_lt(left, right);
	case Audit_le:
		return uid_lte(left, right);
	case Audit_gt:
		return uid_gt(left, right);
	case Audit_ge:
		return uid_gte(left, right);
	case Audit_bitmask:
	case Audit_bittest:
	default:
		BUG();
		return 0;
	}
}

int audit_gid_comparator(kgid_t left, u32 op, kgid_t right)
{
	switch (op) {
	case Audit_equal:
		return gid_eq(left, right);
	case Audit_not_equal:
		return !gid_eq(left, right);
	case Audit_lt:
		return gid_lt(left, right);
	case Audit_le:
		return gid_lte(left, right);
	case Audit_gt:
		return gid_gt(left, right);
	case Audit_ge:
		return gid_gte(left, right);
	case Audit_bitmask:
	case Audit_bittest:
	default:
		BUG();
		return 0;
	}
}

*/
 parent_len - find the length of the parent portion of a pathname
 @path: pathname of which to determine length
 /*
int parent_len(const charpath)
{
	int plen;
	const charp;

	plen = strlen(path);

	if (plen == 0)
		return plen;

	*/ disregard trailing slashes /*
	p = path + plen - 1;
	while ((*p == '/') &amp;amp;&amp;amp; (p &gt; path))
		p--;

	*/ walk backward until we find the next slash or hit beginning /*
	while ((*p != '/') &amp;amp;&amp;amp; (p &gt; path))
		p--;

	*/ did we find a slash? Then increment to include it in path /*
	if (*p == '/')
		p++;

	return p - path;
}

*/
 audit_compare_dname_path - compare given dentry name with last component in
 			      given path. Return of 0 indicates a match.
 @dname:	dentry name that we're comparing
 @path:	full pathname that we're comparing
 @parentlen:	length of the parent if known. Passing in AUDIT_NAME_FULL
 		here indicates that we must compute this value.
 /*
int audit_compare_dname_path(const chardname, const charpath, int parentlen)
{
	int dlen, pathlen;
	const charp;

	dlen = strlen(dname);
	pathlen = strlen(path);
	if (pathlen &lt; dlen)
		return 1;

	parentlen = parentlen == AUDIT_NAME_FULL ? parent_len(path) : parentlen;
	if (pathlen - parentlen != dlen)
		return 1;

	p = path + parentlen;

	return strncmp(p, dname, dlen);
}

static int audit_filter_user_rules(struct audit_krulerule, int type,
				   enum audit_statestate)
{
	int i;

	for (i = 0; i &lt; rule-&gt;field_count; i++) {
		struct audit_fieldf = &amp;amp;rule-&gt;fields[i];
		pid_t pid;
		int result = 0;
		u32 sid;

		switch (f-&gt;type) {
		case AUDIT_PID:
			pid = task_pid_nr(current);
			result = audit_comparator(pid, f-&gt;op, f-&gt;val);
			break;
		case AUDIT_UID:
			result = audit_uid_comparator(current_uid(), f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_GID:
			result = audit_gid_comparator(current_gid(), f-&gt;op, f-&gt;gid);
			break;
		case AUDIT_LOGINUID:
			result = audit_uid_comparator(audit_get_loginuid(current),
						  f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_LOGINUID_SET:
			result = audit_comparator(audit_loginuid_set(current),
						  f-&gt;op, f-&gt;val);
			break;
		case AUDIT_MSGTYPE:
			result = audit_comparator(type, f-&gt;op, f-&gt;val);
			break;
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
			if (f-&gt;lsm_rule) {
				security_task_getsecid(current, &amp;amp;sid);
				result = security_audit_rule_match(sid,
								   f-&gt;type,
								   f-&gt;op,
								   f-&gt;lsm_rule,
								   NULL);
			}
			break;
		}

		if (!result)
			return 0;
	}
	switch (rule-&gt;action) {
	case AUDIT_NEVER:   state = AUDIT_DISABLED;	    break;
	case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
	}
	return 1;
}

int audit_filter_user(int type)
{
	enum audit_state state = AUDIT_DISABLED;
	struct audit_entrye;
	int rc, ret;

	ret = 1;/ Audit by default /*

	rcu_read_lock();
	list_for_each_entry_rcu(e, &amp;amp;audit_filter_list[AUDIT_FILTER_USER], list) {
		rc = audit_filter_user_rules(&amp;amp;e-&gt;rule, type, &amp;amp;state);
		if (rc) {
			if (rc &gt; 0 &amp;amp;&amp;amp; state == AUDIT_DISABLED)
				ret = 0;
			break;
		}
	}
	rcu_read_unlock();

	return ret;
}

int audit_filter_type(int type)
{
	struct audit_entrye;
	int result = 0;

	rcu_read_lock();
	if (list_empty(&amp;amp;audit_filter_list[AUDIT_FILTER_TYPE]))
		goto unlock_and_return;

	list_for_each_entry_rcu(e, &amp;amp;audit_filter_list[AUDIT_FILTER_TYPE],
				list) {
		int i;
		for (i = 0; i &lt; e-&gt;rule.field_count; i++) {
			struct audit_fieldf = &amp;amp;e-&gt;rule.fields[i];
			if (f-&gt;type == AUDIT_MSGTYPE) {
				result = audit_comparator(type, f-&gt;op, f-&gt;val);
				if (!result)
					break;
			}
		}
		if (result)
			goto unlock_and_return;
	}
unlock_and_return:
	rcu_read_unlock();
	return result;
}

static int update_lsm_rule(struct audit_kruler)
{
	struct audit_entryentry = container_of(r, struct audit_entry, rule);
	struct audit_entrynentry;
	int err = 0;

	if (!security_audit_rule_known(r))
		return 0;

	nentry = audit_dupe_rule(r);
	if (entry-&gt;rule.exe)
		audit_remove_mark(entry-&gt;rule.exe);
	if (IS_ERR(nentry)) {
		*/ save the first error encountered for the
		 return value /*
		err = PTR_ERR(nentry);
		audit_panic("error updating LSM filters");
		if (r-&gt;watch)
			list_del(&amp;amp;r-&gt;rlist);
		list_del_rcu(&amp;amp;entry-&gt;list);
		list_del(&amp;amp;r-&gt;list);
	} else {
		if (r-&gt;watch || r-&gt;tree)
			list_replace_init(&amp;amp;r-&gt;rlist, &amp;amp;nentry-&gt;rule.rlist);
		list_replace_rcu(&amp;amp;entry-&gt;list, &amp;amp;nentry-&gt;list);
		list_replace(&amp;amp;r-&gt;list, &amp;amp;nentry-&gt;rule.list);
	}
	call_rcu(&amp;amp;entry-&gt;rcu, audit_free_rule_rcu);

	return err;
}

*/ This function will re-initialize the lsm_rule field of all applicable rules.
 It will traverse the filter lists serarching for rules that contain LSM
 specific filter fields.  When such a rule is found, it is copied, the
 LSM field is re-initialized, and the old rule is replaced with the
 updated rule. /*
int audit_update_lsm_rules(void)
{
	struct audit_kruler,n;
	int i, err = 0;

	*/ audit_filter_mutex synchronizes the writers /*
	mutex_lock(&amp;amp;audit_filter_mutex);

	for (i = 0; i &lt; AUDIT_NR_FILTERS; i++) {
		list_for_each_entry_safe(r, n, &amp;amp;audit_rules_list[i], list) {
			int res = update_lsm_rule(r);
			if (!err)
				err = res;
		}
	}
	mutex_unlock(&amp;amp;audit_filter_mutex);

	return err;
}
*/
 audit_fsnotify.c -- tracking inodes

 Copyright 2003-2009,2014-2015 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 /*

#include &lt;linux/kernel.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/fsnotify_backend.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/netlink.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/security.h&gt;
#include "audit.h"

*/
 this mark lives on the parent directory of the inode in question.
 but dev, ino, and path are about the child
 /*
struct audit_fsnotify_mark {
	dev_t dev;		*/ associated superblock device /*
	unsigned long ino;	*/ associated inode number /*
	charpath;		*/ insertion path /*
	struct fsnotify_mark mark;/ fsnotify mark on the inode /*
	struct audit_krulerule;
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_fsnotify_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_EVENTS (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
			 FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_fsnotify_mark_free(struct audit_fsnotify_markaudit_mark)
{
	kfree(audit_mark-&gt;path);
	kfree(audit_mark);
}

static void audit_fsnotify_free_mark(struct fsnotify_markmark)
{
	struct audit_fsnotify_markaudit_mark;

	audit_mark = container_of(mark, struct audit_fsnotify_mark, mark);
	audit_fsnotify_mark_free(audit_mark);
}

charaudit_mark_path(struct audit_fsnotify_markmark)
{
	return mark-&gt;path;
}

int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev)
{
	if (mark-&gt;ino == AUDIT_INO_UNSET)
		return 0;
	return (mark-&gt;ino == ino) &amp;amp;&amp;amp; (mark-&gt;dev == dev);
}

static void audit_update_mark(struct audit_fsnotify_markaudit_mark,
			     struct inodeinode)
{
	audit_mark-&gt;dev = inode ? inode-&gt;i_sb-&gt;s_dev : AUDIT_DEV_UNSET;
	audit_mark-&gt;ino = inode ? inode-&gt;i_ino : AUDIT_INO_UNSET;
}

struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len)
{
	struct audit_fsnotify_markaudit_mark;
	struct path path;
	struct dentrydentry;
	struct inodeinode;
	int ret;

	if (pathname[0] != '/' || pathname[len-1] == '/')
		return ERR_PTR(-EINVAL);

	dentry = kern_path_locked(pathname, &amp;amp;path);
	if (IS_ERR(dentry))
		return (void)dentry;/ returning an error /*
	inode = path.dentry-&gt;d_inode;
	inode_unlock(inode);

	audit_mark = kzalloc(sizeof(*audit_mark), GFP_KERNEL);
	if (unlikely(!audit_mark)) {
		audit_mark = ERR_PTR(-ENOMEM);
		goto out;
	}

	fsnotify_init_mark(&amp;amp;audit_mark-&gt;mark, audit_fsnotify_free_mark);
	audit_mark-&gt;mark.mask = AUDIT_FS_EVENTS;
	audit_mark-&gt;path = pathname;
	audit_update_mark(audit_mark, dentry-&gt;d_inode);
	audit_mark-&gt;rule = krule;

	ret = fsnotify_add_mark(&amp;amp;audit_mark-&gt;mark, audit_fsnotify_group, inode, NULL, true);
	if (ret &lt; 0) {
		audit_fsnotify_mark_free(audit_mark);
		audit_mark = ERR_PTR(ret);
	}
out:
	dput(dentry);
	path_put(&amp;amp;path);
	return audit_mark;
}

static void audit_mark_log_rule_change(struct audit_fsnotify_markaudit_mark, charop)
{
	struct audit_bufferab;
	struct audit_krulerule = audit_mark-&gt;rule;

	if (!audit_enabled)
		return;
	ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return;
	audit_log_format(ab, "auid=%u ses=%u op=",
			 from_kuid(&amp;amp;init_user_ns, audit_get_loginuid(current)),
			 audit_get_sessionid(current));
	audit_log_string(ab, op);
	audit_log_format(ab, " path=");
	audit_log_untrustedstring(ab, audit_mark-&gt;path);
	audit_log_key(ab, rule-&gt;filterkey);
	audit_log_format(ab, " list=%d res=1", rule-&gt;listnr);
	audit_log_end(ab);
}

void audit_remove_mark(struct audit_fsnotify_markaudit_mark)
{
	fsnotify_destroy_mark(&amp;amp;audit_mark-&gt;mark, audit_fsnotify_group);
	fsnotify_put_mark(&amp;amp;audit_mark-&gt;mark);
}

void audit_remove_mark_rule(struct audit_krulekrule)
{
	struct audit_fsnotify_markmark = krule-&gt;exe;

	audit_remove_mark(mark);
}

static void audit_autoremove_mark_rule(struct audit_fsnotify_markaudit_mark)
{
	struct audit_krulerule = audit_mark-&gt;rule;
	struct audit_entryentry = container_of(rule, struct audit_entry, rule);

	audit_mark_log_rule_change(audit_mark, "autoremove_rule");
	audit_del_rule(entry);
}

*/ Update mark data in audit rules based on fsnotify events. /*
static int audit_mark_handle_event(struct fsnotify_groupgroup,
				    struct inodeto_tell,
				    struct fsnotify_markinode_mark,
				    struct fsnotify_markvfsmount_mark,
				    u32 mask, voiddata, int data_type,
				    const unsigned chardname, u32 cookie)
{
	struct audit_fsnotify_markaudit_mark;
	struct inodeinode = NULL;

	audit_mark = container_of(inode_mark, struct audit_fsnotify_mark, mark);

	BUG_ON(group != audit_fsnotify_group);

	switch (data_type) {
	case (FSNOTIFY_EVENT_PATH):
		inode = ((struct path)data)-&gt;dentry-&gt;d_inode;
		break;
	case (FSNOTIFY_EVENT_INODE):
		inode = (struct inode)data;
		break;
	default:
		BUG();
		return 0;
	};

	if (mask &amp;amp; (FS_CREATE|FS_MOVED_TO|FS_DELETE|FS_MOVED_FROM)) {
		if (audit_compare_dname_path(dname, audit_mark-&gt;path, AUDIT_NAME_FULL))
			return 0;
		audit_update_mark(audit_mark, inode);
	} else if (mask &amp;amp; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
		audit_autoremove_mark_rule(audit_mark);

	return 0;
}

static const struct fsnotify_ops audit_mark_fsnotify_ops = {
	.handle_event =	audit_mark_handle_event,
};

static int __init audit_fsnotify_init(void)
{
	audit_fsnotify_group = fsnotify_alloc_group(&amp;amp;audit_mark_fsnotify_ops);
	if (IS_ERR(audit_fsnotify_group)) {
		audit_fsnotify_group = NULL;
		audit_panic("cannot create audit fsnotify group");
	}
	return 0;
}
device_initcall(audit_fsnotify_init);
*/
 audit -- definition of audit_context structure and supporting types 

 Copyright 2003-2004 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include &lt;linux/fs.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/skbuff.h&gt;
#include &lt;uapi/linux/mqueue.h&gt;

*/ AUDIT_NAMES is the number of slots we reserve in the audit_context
 for saving names from getname().  If we get more names we will allocate
 a name dynamically and also add those to the list anchored by names_list. /*
#define AUDIT_NAMES	5

*/ At task start time, the audit_state is set in the audit_context using
   a per-task filter.  At syscall entry, the audit_state is augmented by
   the syscall filter. /*
enum audit_state {
	AUDIT_DISABLED,		*/ Do not create per-task audit_context.
				 No syscall-specific audit records can
				 be generated. /*
	AUDIT_BUILD_CONTEXT,	*/ Create the per-task audit_context,
				 and fill it in at syscall
				 entry time.  This makes a full
				 syscall record available if some
				 other part of the kernel decides it
				 should be recorded. /*
	AUDIT_RECORD_CONTEXT	*/ Create the per-task audit_context,
				 always fill it in at syscall entry
				 time, and always write out the audit
				 record at syscall exit time.  /*
};

*/ Rule lists /*
struct audit_watch;
struct audit_fsnotify_mark;
struct audit_tree;
struct audit_chunk;

struct audit_entry {
	struct list_head	list;
	struct rcu_head		rcu;
	struct audit_krule	rule;
};

struct audit_cap_data {
	kernel_cap_t		permitted;
	kernel_cap_t		inheritable;
	union {
		unsigned int	fE;		*/ effective bit of file cap /*
		kernel_cap_t	effective;	*/ effective set of process /*
	};
};

*/ When fs/namei.c:getname() is called, we store the pointer in name and bump
 the refcnt in the associated filename struct.

 Further, in fs/namei.c:path_lookup() we store the inode and device.
 /*
struct audit_names {
	struct list_head	list;		*/ audit_context-&gt;names_list /*

	struct filename		*name;
	int			name_len;	*/ number of chars to log /*
	bool			hidden;		*/ don't log this record /*

	unsigned long		ino;
	dev_t			dev;
	umode_t			mode;
	kuid_t			uid;
	kgid_t			gid;
	dev_t			rdev;
	u32			osid;
	struct audit_cap_data	fcap;
	unsigned int		fcap_ver;
	unsigned char		type;		*/ record type /*
	*/
	 This was an allocated audit_names and not from the array of
	 names allocated in the task audit context.  Thus this name
	 should be freed on syscall exit.
	 /*
	bool			should_free;
};

struct audit_proctitle {
	int	len;	*/ length of the cmdline field. /*
	char	*value;	*/ the cmdline field /*
};

*/ The per-task audit context. /*
struct audit_context {
	int		    dummy;	*/ must be the first element /*
	int		    in_syscall;	*/ 1 if task is in a syscall /*
	enum audit_state    state, current_state;
	unsigned int	    serial;    / serial number for record /*
	int		    major;     / syscall number /*
	struct timespec	    ctime;     / time of syscall entry /*
	unsigned long	    argv[4];   / syscall arguments /*
	long		    return_code;*/ syscall return code /*
	u64		    prio;
	int		    return_valid;/ return code is valid /*
	*/
	 The names_list is the list of all audit_names collected during this
	 syscall.  The first AUDIT_NAMES entries in the names_list will
	 actually be from the preallocated_names array for performance
	 reasons.  Except during allocation they should never be referenced
	 through the preallocated_names array and should only be found/used
	 by running the names_list.
	 /*
	struct audit_names  preallocated_names[AUDIT_NAMES];
	int		    name_count;/ total records in names_list /*
	struct list_head    names_list;	*/ struct audit_names-&gt;list anchor /*
	char		   filterkey;	*/ key for rule that triggered record /*
	struct path	    pwd;
	struct audit_aux_dataaux;
	struct audit_aux_dataaux_pids;
	struct sockaddr_storagesockaddr;
	size_t sockaddr_len;
				*/ Save things to print about task_struct /*
	pid_t		    pid, ppid;
	kuid_t		    uid, euid, suid, fsuid;
	kgid_t		    gid, egid, sgid, fsgid;
	unsigned long	    personality;
	int		    arch;

	pid_t		    target_pid;
	kuid_t		    target_auid;
	kuid_t		    target_uid;
	unsigned int	    target_sessionid;
	u32		    target_sid;
	char		    target_comm[TASK_COMM_LEN];

	struct audit_tree_refstrees,first_trees;
	struct list_head killed_trees;
	int tree_count;

	int type;
	union {
		struct {
			int nargs;
			long args[6];
		} socketcall;
		struct {
			kuid_t			uid;
			kgid_t			gid;
			umode_t			mode;
			u32			osid;
			int			has_perm;
			uid_t			perm_uid;
			gid_t			perm_gid;
			umode_t			perm_mode;
			unsigned long		qbytes;
		} ipc;
		struct {
			mqd_t			mqdes;
			struct mq_attr		mqstat;
		} mq_getsetattr;
		struct {
			mqd_t			mqdes;
			int			sigev_signo;
		} mq_notify;
		struct {
			mqd_t			mqdes;
			size_t			msg_len;
			unsigned int		msg_prio;
			struct timespec		abs_timeout;
		} mq_sendrecv;
		struct {
			int			oflag;
			umode_t			mode;
			struct mq_attr		attr;
		} mq_open;
		struct {
			pid_t			pid;
			struct audit_cap_data	cap;
		} capset;
		struct {
			int			fd;
			int			flags;
		} mmap;
		struct {
			int			argc;
		} execve;
	};
	int fds[2];
	struct audit_proctitle proctitle;
};

extern u32 audit_ever_enabled;

extern void audit_copy_inode(struct audit_namesname,
			     const struct dentrydentry,
			     struct inodeinode);
extern void audit_log_cap(struct audit_bufferab, charprefix,
			  kernel_cap_tcap);
extern void audit_log_name(struct audit_contextcontext,
			   struct audit_namesn, struct pathpath,
			   int record_num, intcall_panic);

extern int audit_pid;

#define AUDIT_INODE_BUCKETS	32
extern struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];

static inline int audit_hash_ino(u32 ino)
{
	return (ino &amp;amp; (AUDIT_INODE_BUCKETS-1));
}

*/ Indicates that audit should log the full pathname. /*
#define AUDIT_NAME_FULL -1

extern int audit_match_class(int class, unsigned syscall);
extern int audit_comparator(const u32 left, const u32 op, const u32 right);
extern int audit_uid_comparator(kuid_t left, u32 op, kuid_t right);
extern int audit_gid_comparator(kgid_t left, u32 op, kgid_t right);
extern int parent_len(const charpath);
extern int audit_compare_dname_path(const chardname, const charpath, int plen);
extern struct sk_buffaudit_make_reply(__u32 portid, int seq, int type,
					int done, int multi,
					const voidpayload, int size);
extern void		    audit_panic(const charmessage);

struct audit_netlink_list {
	__u32 portid;
	struct netnet;
	struct sk_buff_head q;
};

int audit_send_list(void);

struct audit_net {
	struct socknlsk;
};

extern int selinux_audit_rule_update(void);

extern struct mutex audit_filter_mutex;
extern int audit_del_rule(struct audit_entry);
extern void audit_free_rule_rcu(struct rcu_head);
extern struct list_head audit_filter_list[];

extern struct audit_entryaudit_dupe_rule(struct audit_kruleold);

extern void audit_log_d_path_exe(struct audit_bufferab,
				 struct mm_structmm);

*/ audit watch functions /*
#ifdef CONFIG_AUDIT_WATCH
extern void audit_put_watch(struct audit_watchwatch);
extern void audit_get_watch(struct audit_watchwatch);
extern int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op);
extern int audit_add_watch(struct audit_krulekrule, struct list_head*list);
extern void audit_remove_watch_rule(struct audit_krulekrule);
extern charaudit_watch_path(struct audit_watchwatch);
extern int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev);

extern struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len);
extern charaudit_mark_path(struct audit_fsnotify_markmark);
extern void audit_remove_mark(struct audit_fsnotify_markaudit_mark);
extern void audit_remove_mark_rule(struct audit_krulekrule);
extern int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev);
extern int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold);
extern int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark);

#else
#define audit_put_watch(w) {}
#define audit_get_watch(w) {}
#define audit_to_watch(k, p, l, o) (-EINVAL)
#define audit_add_watch(k, l) (-EINVAL)
#define audit_remove_watch_rule(k) BUG()
#define audit_watch_path(w) ""
#define audit_watch_compare(w, i, d) 0

#define audit_alloc_mark(k, p, l) (ERR_PTR(-EINVAL))
#define audit_mark_path(m) ""
#define audit_remove_mark(m)
#define audit_remove_mark_rule(k)
#define audit_mark_compare(m, i, d) 0
#define audit_exe_compare(t, m) (-EINVAL)
#define audit_dupe_exe(n, o) (-EINVAL)
#endif */ CONFIG_AUDIT_WATCH /*

#ifdef CONFIG_AUDIT_TREE
extern struct audit_chunkaudit_tree_lookup(const struct inode);
extern void audit_put_chunk(struct audit_chunk);
extern bool audit_tree_match(struct audit_chunk, struct audit_tree);
extern int audit_make_tree(struct audit_krule, char, u32);
extern int audit_add_tree_rule(struct audit_krule);
extern int audit_remove_tree_rule(struct audit_krule);
extern void audit_trim_trees(void);
extern int audit_tag_tree(charold, charnew);
extern const charaudit_tree_path(struct audit_tree);
extern void audit_put_tree(struct audit_tree);
extern void audit_kill_trees(struct list_head);
#else
#define audit_remove_tree_rule(rule) BUG()
#define audit_add_tree_rule(rule) -EINVAL
#define audit_make_tree(rule, str, op) -EINVAL
#define audit_trim_trees() (void)0
#define audit_put_tree(tree) (void)0
#define audit_tag_tree(old, new) -EINVAL
#define audit_tree_path(rule) ""	*/ never called /*
#define audit_kill_trees(list) BUG()
#endif

extern charaudit_unpack_string(void*, size_t, size_t);

extern pid_t audit_sig_pid;
extern kuid_t audit_sig_uid;
extern u32 audit_sig_sid;

#ifdef CONFIG_AUDITSYSCALL
extern int __audit_signal_info(int sig, struct task_structt);
static inline int audit_signal_info(int sig, struct task_structt)
{
	if (unlikely((audit_pid &amp;amp;&amp;amp; t-&gt;tgid == audit_pid) ||
		     (audit_signals &amp;amp;&amp;amp; !audit_dummy_context())))
		return __audit_signal_info(sig, t);
	return 0;
}
extern void audit_filter_inodes(struct task_struct, struct audit_context);
extern struct list_headaudit_killed_trees(void);
#else
#define audit_signal_info(s,t) AUDIT_DISABLED
#define audit_filter_inodes(t,c) AUDIT_DISABLED
#endif

extern struct mutex audit_cmd_mutex;
*/
 auditsc.c -- System-call auditing support
 Handles all system-call specific auditing features.

 Copyright 2003-2004 Red Hat Inc., Durham, North Carolina.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright (C) 2005, 2006 IBM Corporation
 All Rights Reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Written by Rickard E. (Rik) Faith &lt;faith@redhat.com&gt;

 Many of the ideas implemented here are from Stephen C. Tweedie,
 especially the idea of avoiding a copy by using getname.

 The method for actual interception of syscall entry and exit (not in
 this file -- see entry.S) is based on a GPL'd patch written by
 okir@suse.de and Copyright 2003 SuSE Linux AG.

 POSIX message queue support added by George Wilson &lt;ltcgcw@us.ibm.com&gt;,
 2006.

 The support of additional filter rules compares (&gt;, &lt;, &gt;=, &lt;=) was
 added by Dustin Kirkland &lt;dustin.kirkland@us.ibm.com&gt;, 2005.

 Modified by Amy Griffis &lt;amy.griffis@hp.com&gt; to collect additional
 filesystem information.

 Subject and object context labeling support added by &lt;danjones@us.ibm.com&gt;
 and &lt;dustin.kirkland@us.ibm.com&gt; for LSPP certification compliance.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include &lt;linux/init.h&gt;
#include &lt;asm/types.h&gt;
#include &lt;linux/atomic.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/mm.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/mount.h&gt;
#include &lt;linux/socket.h&gt;
#include &lt;linux/mqueue.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/personality.h&gt;
#include &lt;linux/time.h&gt;
#include &lt;linux/netlink.h&gt;
#include &lt;linux/compiler.h&gt;
#include &lt;asm/unistd.h&gt;
#include &lt;linux/security.h&gt;
#include &lt;linux/list.h&gt;
#include &lt;linux/tty.h&gt;
#include &lt;linux/binfmts.h&gt;
#include &lt;linux/highmem.h&gt;
#include &lt;linux/syscalls.h&gt;
#include &lt;asm/syscall.h&gt;
#include &lt;linux/capability.h&gt;
#include &lt;linux/fs_struct.h&gt;
#include &lt;linux/compat.h&gt;
#include &lt;linux/ctype.h&gt;
#include &lt;linux/string.h&gt;
#include &lt;uapi/linux/limits.h&gt;

#include "audit.h"

*/ flags stating the success for a syscall /*
#define AUDITSC_INVALID 0
#define AUDITSC_SUCCESS 1
#define AUDITSC_FAILURE 2

*/ no execve audit message should be longer than this (userspace limits) /*
#define MAX_EXECVE_AUDIT_LEN 7500

*/ max length to print of cmdline/proctitle value during audit /*
#define MAX_PROCTITLE_AUDIT_LEN 128

*/ number of audit rules /*
int audit_n_rules;

*/ determines whether we collect data for signals sent /*
int audit_signals;

struct audit_aux_data {
	struct audit_aux_data	*next;
	int			type;
};

#define AUDIT_AUX_IPCPERM	0

*/ Number of target pids per aux struct. /*
#define AUDIT_AUX_PIDS	16

struct audit_aux_data_pids {
	struct audit_aux_data	d;
	pid_t			target_pid[AUDIT_AUX_PIDS];
	kuid_t			target_auid[AUDIT_AUX_PIDS];
	kuid_t			target_uid[AUDIT_AUX_PIDS];
	unsigned int		target_sessionid[AUDIT_AUX_PIDS];
	u32			target_sid[AUDIT_AUX_PIDS];
	char 			target_comm[AUDIT_AUX_PIDS][TASK_COMM_LEN];
	int			pid_count;
};

struct audit_aux_data_bprm_fcaps {
	struct audit_aux_data	d;
	struct audit_cap_data	fcap;
	unsigned int		fcap_ver;
	struct audit_cap_data	old_pcap;
	struct audit_cap_data	new_pcap;
};

struct audit_tree_refs {
	struct audit_tree_refsnext;
	struct audit_chunkc[31];
};

static int audit_match_perm(struct audit_contextctx, int mask)
{
	unsigned n;
	if (unlikely(!ctx))
		return 0;
	n = ctx-&gt;major;

	switch (audit_classify_syscall(ctx-&gt;arch, n)) {
	case 0:	*/ native /*
		if ((mask &amp;amp; AUDIT_PERM_WRITE) &amp;amp;&amp;amp;
		     audit_match_class(AUDIT_CLASS_WRITE, n))
			return 1;
		if ((mask &amp;amp; AUDIT_PERM_READ) &amp;amp;&amp;amp;
		     audit_match_class(AUDIT_CLASS_READ, n))
			return 1;
		if ((mask &amp;amp; AUDIT_PERM_ATTR) &amp;amp;&amp;amp;
		     audit_match_class(AUDIT_CLASS_CHATTR, n))
			return 1;
		return 0;
	case 1:/ 32bit on biarch /*
		if ((mask &amp;amp; AUDIT_PERM_WRITE) &amp;amp;&amp;amp;
		     audit_match_class(AUDIT_CLASS_WRITE_32, n))
			return 1;
		if ((mask &amp;amp; AUDIT_PERM_READ) &amp;amp;&amp;amp;
		     audit_match_class(AUDIT_CLASS_READ_32, n))
			return 1;
		if ((mask &amp;amp; AUDIT_PERM_ATTR) &amp;amp;&amp;amp;
		     audit_match_class(AUDIT_CLASS_CHATTR_32, n))
			return 1;
		return 0;
	case 2:/ open /*
		return mask &amp;amp; ACC_MODE(ctx-&gt;argv[1]);
	case 3:/ openat /*
		return mask &amp;amp; ACC_MODE(ctx-&gt;argv[2]);
	case 4:/ socketcall /*
		return ((mask &amp;amp; AUDIT_PERM_WRITE) &amp;amp;&amp;amp; ctx-&gt;argv[0] == SYS_BIND);
	case 5:/ execve /*
		return mask &amp;amp; AUDIT_PERM_EXEC;
	default:
		return 0;
	}
}

static int audit_match_filetype(struct audit_contextctx, int val)
{
	struct audit_namesn;
	umode_t mode = (umode_t)val;

	if (unlikely(!ctx))
		return 0;

	list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
		if ((n-&gt;ino != AUDIT_INO_UNSET) &amp;amp;&amp;amp;
		    ((n-&gt;mode &amp;amp; S_IFMT) == mode))
			return 1;
	}

	return 0;
}

*/
 We keep a linked list of fixed-sized (31 pointer) arrays of audit_chunk;
 -&gt;first_trees points to its beginning, -&gt;trees - to the current end of data.
 -&gt;tree_count is the number of free entries in array pointed to by -&gt;trees.
 Original condition is (NULL, NULL, 0); as soon as it grows we never revert to NULL,
 "empty" becomes (p, p, 31) afterwards.  We don't shrink the list (and seriously,
 it's going to remain 1-element for almost any setup) until we free context itself.
 References in it _are_ dropped - at the same time we free/drop aux stuff.
 /*

#ifdef CONFIG_AUDIT_TREE
static void audit_set_auditable(struct audit_contextctx)
{
	if (!ctx-&gt;prio) {
		ctx-&gt;prio = 1;
		ctx-&gt;current_state = AUDIT_RECORD_CONTEXT;
	}
}

static int put_tree_ref(struct audit_contextctx, struct audit_chunkchunk)
{
	struct audit_tree_refsp = ctx-&gt;trees;
	int left = ctx-&gt;tree_count;
	if (likely(left)) {
		p-&gt;c[--left] = chunk;
		ctx-&gt;tree_count = left;
		return 1;
	}
	if (!p)
		return 0;
	p = p-&gt;next;
	if (p) {
		p-&gt;c[30] = chunk;
		ctx-&gt;trees = p;
		ctx-&gt;tree_count = 30;
		return 1;
	}
	return 0;
}

static int grow_tree_refs(struct audit_contextctx)
{
	struct audit_tree_refsp = ctx-&gt;trees;
	ctx-&gt;trees = kzalloc(sizeof(struct audit_tree_refs), GFP_KERNEL);
	if (!ctx-&gt;trees) {
		ctx-&gt;trees = p;
		return 0;
	}
	if (p)
		p-&gt;next = ctx-&gt;trees;
	else
		ctx-&gt;first_trees = ctx-&gt;trees;
	ctx-&gt;tree_count = 31;
	return 1;
}
#endif

static void unroll_tree_refs(struct audit_contextctx,
		      struct audit_tree_refsp, int count)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_tree_refsq;
	int n;
	if (!p) {
		*/ we started with empty chain /*
		p = ctx-&gt;first_trees;
		count = 31;
		*/ if the very first allocation has failed, nothing to do /*
		if (!p)
			return;
	}
	n = count;
	for (q = p; q != ctx-&gt;trees; q = q-&gt;next, n = 31) {
		while (n--) {
			audit_put_chunk(q-&gt;c[n]);
			q-&gt;c[n] = NULL;
		}
	}
	while (n-- &gt; ctx-&gt;tree_count) {
		audit_put_chunk(q-&gt;c[n]);
		q-&gt;c[n] = NULL;
	}
	ctx-&gt;trees = p;
	ctx-&gt;tree_count = count;
#endif
}

static void free_tree_refs(struct audit_contextctx)
{
	struct audit_tree_refsp,q;
	for (p = ctx-&gt;first_trees; p; p = q) {
		q = p-&gt;next;
		kfree(p);
	}
}

static int match_tree_refs(struct audit_contextctx, struct audit_treetree)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_tree_refsp;
	int n;
	if (!tree)
		return 0;
	*/ full ones /*
	for (p = ctx-&gt;first_trees; p != ctx-&gt;trees; p = p-&gt;next) {
		for (n = 0; n &lt; 31; n++)
			if (audit_tree_match(p-&gt;c[n], tree))
				return 1;
	}
	*/ partial /*
	if (p) {
		for (n = ctx-&gt;tree_count; n &lt; 31; n++)
			if (audit_tree_match(p-&gt;c[n], tree))
				return 1;
	}
#endif
	return 0;
}

static int audit_compare_uid(kuid_t uid,
			     struct audit_namesname,
			     struct audit_fieldf,
			     struct audit_contextctx)
{
	struct audit_namesn;
	int rc;
 
	if (name) {
		rc = audit_uid_comparator(uid, f-&gt;op, name-&gt;uid);
		if (rc)
			return rc;
	}
 
	if (ctx) {
		list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
			rc = audit_uid_comparator(uid, f-&gt;op, n-&gt;uid);
			if (rc)
				return rc;
		}
	}
	return 0;
}

static int audit_compare_gid(kgid_t gid,
			     struct audit_namesname,
			     struct audit_fieldf,
			     struct audit_contextctx)
{
	struct audit_namesn;
	int rc;
 
	if (name) {
		rc = audit_gid_comparator(gid, f-&gt;op, name-&gt;gid);
		if (rc)
			return rc;
	}
 
	if (ctx) {
		list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
			rc = audit_gid_comparator(gid, f-&gt;op, n-&gt;gid);
			if (rc)
				return rc;
		}
	}
	return 0;
}

static int audit_field_compare(struct task_structtsk,
			       const struct credcred,
			       struct audit_fieldf,
			       struct audit_contextctx,
			       struct audit_namesname)
{
	switch (f-&gt;val) {
	*/ process to file object comparisons /*
	case AUDIT_COMPARE_UID_TO_OBJ_UID:
		return audit_compare_uid(cred-&gt;uid, name, f, ctx);
	case AUDIT_COMPARE_GID_TO_OBJ_GID:
		return audit_compare_gid(cred-&gt;gid, name, f, ctx);
	case AUDIT_COMPARE_EUID_TO_OBJ_UID:
		return audit_compare_uid(cred-&gt;euid, name, f, ctx);
	case AUDIT_COMPARE_EGID_TO_OBJ_GID:
		return audit_compare_gid(cred-&gt;egid, name, f, ctx);
	case AUDIT_COMPARE_AUID_TO_OBJ_UID:
		return audit_compare_uid(tsk-&gt;loginuid, name, f, ctx);
	case AUDIT_COMPARE_SUID_TO_OBJ_UID:
		return audit_compare_uid(cred-&gt;suid, name, f, ctx);
	case AUDIT_COMPARE_SGID_TO_OBJ_GID:
		return audit_compare_gid(cred-&gt;sgid, name, f, ctx);
	case AUDIT_COMPARE_FSUID_TO_OBJ_UID:
		return audit_compare_uid(cred-&gt;fsuid, name, f, ctx);
	case AUDIT_COMPARE_FSGID_TO_OBJ_GID:
		return audit_compare_gid(cred-&gt;fsgid, name, f, ctx);
	*/ uid comparisons /*
	case AUDIT_COMPARE_UID_TO_AUID:
		return audit_uid_comparator(cred-&gt;uid, f-&gt;op, tsk-&gt;loginuid);
	case AUDIT_COMPARE_UID_TO_EUID:
		return audit_uid_comparator(cred-&gt;uid, f-&gt;op, cred-&gt;euid);
	case AUDIT_COMPARE_UID_TO_SUID:
		return audit_uid_comparator(cred-&gt;uid, f-&gt;op, cred-&gt;suid);
	case AUDIT_COMPARE_UID_TO_FSUID:
		return audit_uid_comparator(cred-&gt;uid, f-&gt;op, cred-&gt;fsuid);
	*/ auid comparisons /*
	case AUDIT_COMPARE_AUID_TO_EUID:
		return audit_uid_comparator(tsk-&gt;loginuid, f-&gt;op, cred-&gt;euid);
	case AUDIT_COMPARE_AUID_TO_SUID:
		return audit_uid_comparator(tsk-&gt;loginuid, f-&gt;op, cred-&gt;suid);
	case AUDIT_COMPARE_AUID_TO_FSUID:
		return audit_uid_comparator(tsk-&gt;loginuid, f-&gt;op, cred-&gt;fsuid);
	*/ euid comparisons /*
	case AUDIT_COMPARE_EUID_TO_SUID:
		return audit_uid_comparator(cred-&gt;euid, f-&gt;op, cred-&gt;suid);
	case AUDIT_COMPARE_EUID_TO_FSUID:
		return audit_uid_comparator(cred-&gt;euid, f-&gt;op, cred-&gt;fsuid);
	*/ suid comparisons /*
	case AUDIT_COMPARE_SUID_TO_FSUID:
		return audit_uid_comparator(cred-&gt;suid, f-&gt;op, cred-&gt;fsuid);
	*/ gid comparisons /*
	case AUDIT_COMPARE_GID_TO_EGID:
		return audit_gid_comparator(cred-&gt;gid, f-&gt;op, cred-&gt;egid);
	case AUDIT_COMPARE_GID_TO_SGID:
		return audit_gid_comparator(cred-&gt;gid, f-&gt;op, cred-&gt;sgid);
	case AUDIT_COMPARE_GID_TO_FSGID:
		return audit_gid_comparator(cred-&gt;gid, f-&gt;op, cred-&gt;fsgid);
	*/ egid comparisons /*
	case AUDIT_COMPARE_EGID_TO_SGID:
		return audit_gid_comparator(cred-&gt;egid, f-&gt;op, cred-&gt;sgid);
	case AUDIT_COMPARE_EGID_TO_FSGID:
		return audit_gid_comparator(cred-&gt;egid, f-&gt;op, cred-&gt;fsgid);
	*/ sgid comparison /*
	case AUDIT_COMPARE_SGID_TO_FSGID:
		return audit_gid_comparator(cred-&gt;sgid, f-&gt;op, cred-&gt;fsgid);
	default:
		WARN(1, "Missing AUDIT_COMPARE define.  Report as a bug\n");
		return 0;
	}
	return 0;
}

*/ Determine if any context name data matches a rule's watch data /*
*/ Compare a task_struct with an audit_rule.  Return 1 on match, 0
 otherwise.

 If task_creation is true, this is an explicit indication that we are
 filtering a task rule at task creation time.  This and tsk == current are
 the only situations where tsk-&gt;cred may be accessed without an rcu read lock.
 /*
static int audit_filter_rules(struct task_structtsk,
			      struct audit_krulerule,
			      struct audit_contextctx,
			      struct audit_namesname,
			      enum audit_statestate,
			      bool task_creation)
{
	const struct credcred;
	int i, need_sid = 1;
	u32 sid;

	cred = rcu_dereference_check(tsk-&gt;cred, tsk == current || task_creation);

	for (i = 0; i &lt; rule-&gt;field_count; i++) {
		struct audit_fieldf = &amp;amp;rule-&gt;fields[i];
		struct audit_namesn;
		int result = 0;
		pid_t pid;

		switch (f-&gt;type) {
		case AUDIT_PID:
			pid = task_pid_nr(tsk);
			result = audit_comparator(pid, f-&gt;op, f-&gt;val);
			break;
		case AUDIT_PPID:
			if (ctx) {
				if (!ctx-&gt;ppid)
					ctx-&gt;ppid = task_ppid_nr(tsk);
				result = audit_comparator(ctx-&gt;ppid, f-&gt;op, f-&gt;val);
			}
			break;
		case AUDIT_EXE:
			result = audit_exe_compare(tsk, rule-&gt;exe);
			break;
		case AUDIT_UID:
			result = audit_uid_comparator(cred-&gt;uid, f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_EUID:
			result = audit_uid_comparator(cred-&gt;euid, f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_SUID:
			result = audit_uid_comparator(cred-&gt;suid, f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_FSUID:
			result = audit_uid_comparator(cred-&gt;fsuid, f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_GID:
			result = audit_gid_comparator(cred-&gt;gid, f-&gt;op, f-&gt;gid);
			if (f-&gt;op == Audit_equal) {
				if (!result)
					result = in_group_p(f-&gt;gid);
			} else if (f-&gt;op == Audit_not_equal) {
				if (result)
					result = !in_group_p(f-&gt;gid);
			}
			break;
		case AUDIT_EGID:
			result = audit_gid_comparator(cred-&gt;egid, f-&gt;op, f-&gt;gid);
			if (f-&gt;op == Audit_equal) {
				if (!result)
					result = in_egroup_p(f-&gt;gid);
			} else if (f-&gt;op == Audit_not_equal) {
				if (result)
					result = !in_egroup_p(f-&gt;gid);
			}
			break;
		case AUDIT_SGID:
			result = audit_gid_comparator(cred-&gt;sgid, f-&gt;op, f-&gt;gid);
			break;
		case AUDIT_FSGID:
			result = audit_gid_comparator(cred-&gt;fsgid, f-&gt;op, f-&gt;gid);
			break;
		case AUDIT_PERS:
			result = audit_comparator(tsk-&gt;personality, f-&gt;op, f-&gt;val);
			break;
		case AUDIT_ARCH:
			if (ctx)
				result = audit_comparator(ctx-&gt;arch, f-&gt;op, f-&gt;val);
			break;

		case AUDIT_EXIT:
			if (ctx &amp;amp;&amp;amp; ctx-&gt;return_valid)
				result = audit_comparator(ctx-&gt;return_code, f-&gt;op, f-&gt;val);
			break;
		case AUDIT_SUCCESS:
			if (ctx &amp;amp;&amp;amp; ctx-&gt;return_valid) {
				if (f-&gt;val)
					result = audit_comparator(ctx-&gt;return_valid, f-&gt;op, AUDITSC_SUCCESS);
				else
					result = audit_comparator(ctx-&gt;return_valid, f-&gt;op, AUDITSC_FAILURE);
			}
			break;
		case AUDIT_DEVMAJOR:
			if (name) {
				if (audit_comparator(MAJOR(name-&gt;dev), f-&gt;op, f-&gt;val) ||
				    audit_comparator(MAJOR(name-&gt;rdev), f-&gt;op, f-&gt;val))
					++result;
			} else if (ctx) {
				list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
					if (audit_comparator(MAJOR(n-&gt;dev), f-&gt;op, f-&gt;val) ||
					    audit_comparator(MAJOR(n-&gt;rdev), f-&gt;op, f-&gt;val)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_DEVMINOR:
			if (name) {
				if (audit_comparator(MINOR(name-&gt;dev), f-&gt;op, f-&gt;val) ||
				    audit_comparator(MINOR(name-&gt;rdev), f-&gt;op, f-&gt;val))
					++result;
			} else if (ctx) {
				list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
					if (audit_comparator(MINOR(n-&gt;dev), f-&gt;op, f-&gt;val) ||
					    audit_comparator(MINOR(n-&gt;rdev), f-&gt;op, f-&gt;val)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_INODE:
			if (name)
				result = audit_comparator(name-&gt;ino, f-&gt;op, f-&gt;val);
			else if (ctx) {
				list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
					if (audit_comparator(n-&gt;ino, f-&gt;op, f-&gt;val)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_OBJ_UID:
			if (name) {
				result = audit_uid_comparator(name-&gt;uid, f-&gt;op, f-&gt;uid);
			} else if (ctx) {
				list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
					if (audit_uid_comparator(n-&gt;uid, f-&gt;op, f-&gt;uid)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_OBJ_GID:
			if (name) {
				result = audit_gid_comparator(name-&gt;gid, f-&gt;op, f-&gt;gid);
			} else if (ctx) {
				list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
					if (audit_gid_comparator(n-&gt;gid, f-&gt;op, f-&gt;gid)) {
						++result;
						break;
					}
				}
			}
			break;
		case AUDIT_WATCH:
			if (name)
				result = audit_watch_compare(rule-&gt;watch, name-&gt;ino, name-&gt;dev);
			break;
		case AUDIT_DIR:
			if (ctx)
				result = match_tree_refs(ctx, rule-&gt;tree);
			break;
		case AUDIT_LOGINUID:
			result = audit_uid_comparator(tsk-&gt;loginuid, f-&gt;op, f-&gt;uid);
			break;
		case AUDIT_LOGINUID_SET:
			result = audit_comparator(audit_loginuid_set(tsk), f-&gt;op, f-&gt;val);
			break;
		case AUDIT_SUBJ_USER:
		case AUDIT_SUBJ_ROLE:
		case AUDIT_SUBJ_TYPE:
		case AUDIT_SUBJ_SEN:
		case AUDIT_SUBJ_CLR:
			*/ NOTE: this may return negative values indicating
			   a temporary error.  We simply treat this as a
			   match for now to avoid losing information that
			   may be wanted.   An error message will also be
			   logged upon error /*
			if (f-&gt;lsm_rule) {
				if (need_sid) {
					security_task_getsecid(tsk, &amp;amp;sid);
					need_sid = 0;
				}
				result = security_audit_rule_match(sid, f-&gt;type,
				                                  f-&gt;op,
				                                  f-&gt;lsm_rule,
				                                  ctx);
			}
			break;
		case AUDIT_OBJ_USER:
		case AUDIT_OBJ_ROLE:
		case AUDIT_OBJ_TYPE:
		case AUDIT_OBJ_LEV_LOW:
		case AUDIT_OBJ_LEV_HIGH:
			*/ The above note for AUDIT_SUBJ_USER...AUDIT_SUBJ_CLR
			   also applies here /*
			if (f-&gt;lsm_rule) {
				*/ Find files that match /*
				if (name) {
					result = security_audit_rule_match(
					           name-&gt;osid, f-&gt;type, f-&gt;op,
					           f-&gt;lsm_rule, ctx);
				} else if (ctx) {
					list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
						if (security_audit_rule_match(n-&gt;osid, f-&gt;type,
									      f-&gt;op, f-&gt;lsm_rule,
									      ctx)) {
							++result;
							break;
						}
					}
				}
				*/ Find ipc objects that match /*
				if (!ctx || ctx-&gt;type != AUDIT_IPC)
					break;
				if (security_audit_rule_match(ctx-&gt;ipc.osid,
							      f-&gt;type, f-&gt;op,
							      f-&gt;lsm_rule, ctx))
					++result;
			}
			break;
		case AUDIT_ARG0:
		case AUDIT_ARG1:
		case AUDIT_ARG2:
		case AUDIT_ARG3:
			if (ctx)
				result = audit_comparator(ctx-&gt;argv[f-&gt;type-AUDIT_ARG0], f-&gt;op, f-&gt;val);
			break;
		case AUDIT_FILTERKEY:
			*/ ignore this field for filtering /*
			result = 1;
			break;
		case AUDIT_PERM:
			result = audit_match_perm(ctx, f-&gt;val);
			break;
		case AUDIT_FILETYPE:
			result = audit_match_filetype(ctx, f-&gt;val);
			break;
		case AUDIT_FIELD_COMPARE:
			result = audit_field_compare(tsk, cred, f, ctx, name);
			break;
		}
		if (!result)
			return 0;
	}

	if (ctx) {
		if (rule-&gt;prio &lt;= ctx-&gt;prio)
			return 0;
		if (rule-&gt;filterkey) {
			kfree(ctx-&gt;filterkey);
			ctx-&gt;filterkey = kstrdup(rule-&gt;filterkey, GFP_ATOMIC);
		}
		ctx-&gt;prio = rule-&gt;prio;
	}
	switch (rule-&gt;action) {
	case AUDIT_NEVER:   state = AUDIT_DISABLED;	    break;
	case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
	}
	return 1;
}

*/ At process creation time, we can determine if system-call auditing is
 completely disabled for this task.  Since we only have the task
 structure at this point, we can only check uid and gid.
 /*
static enum audit_state audit_filter_task(struct task_structtsk, char*key)
{
	struct audit_entrye;
	enum audit_state   state;

	rcu_read_lock();
	list_for_each_entry_rcu(e, &amp;amp;audit_filter_list[AUDIT_FILTER_TASK], list) {
		if (audit_filter_rules(tsk, &amp;amp;e-&gt;rule, NULL, NULL,
				       &amp;amp;state, true)) {
			if (state == AUDIT_RECORD_CONTEXT)
				*key = kstrdup(e-&gt;rule.filterkey, GFP_ATOMIC);
			rcu_read_unlock();
			return state;
		}
	}
	rcu_read_unlock();
	return AUDIT_BUILD_CONTEXT;
}

static int audit_in_mask(const struct audit_krulerule, unsigned long val)
{
	int word, bit;

	if (val &gt; 0xffffffff)
		return false;

	word = AUDIT_WORD(val);
	if (word &gt;= AUDIT_BITMASK_SIZE)
		return false;

	bit = AUDIT_BIT(val);

	return rule-&gt;mask[word] &amp;amp; bit;
}

*/ At syscall entry and exit time, this filter is called if the
 audit_state is not low enough that auditing cannot take place, but is
 also not high enough that we already know we have to write an audit
 record (i.e., the state is AUDIT_SETUP_CONTEXT or AUDIT_BUILD_CONTEXT).
 /*
static enum audit_state audit_filter_syscall(struct task_structtsk,
					     struct audit_contextctx,
					     struct list_headlist)
{
	struct audit_entrye;
	enum audit_state state;

	if (audit_pid &amp;amp;&amp;amp; tsk-&gt;tgid == audit_pid)
		return AUDIT_DISABLED;

	rcu_read_lock();
	if (!list_empty(list)) {
		list_for_each_entry_rcu(e, list, list) {
			if (audit_in_mask(&amp;amp;e-&gt;rule, ctx-&gt;major) &amp;amp;&amp;amp;
			    audit_filter_rules(tsk, &amp;amp;e-&gt;rule, ctx, NULL,
					       &amp;amp;state, false)) {
				rcu_read_unlock();
				ctx-&gt;current_state = state;
				return state;
			}
		}
	}
	rcu_read_unlock();
	return AUDIT_BUILD_CONTEXT;
}

*/
 Given an audit_name check the inode hash table to see if they match.
 Called holding the rcu read lock to protect the use of audit_inode_hash
 /*
static int audit_filter_inode_name(struct task_structtsk,
				   struct audit_namesn,
				   struct audit_contextctx) {
	int h = audit_hash_ino((u32)n-&gt;ino);
	struct list_headlist = &amp;amp;audit_inode_hash[h];
	struct audit_entrye;
	enum audit_state state;

	if (list_empty(list))
		return 0;

	list_for_each_entry_rcu(e, list, list) {
		if (audit_in_mask(&amp;amp;e-&gt;rule, ctx-&gt;major) &amp;amp;&amp;amp;
		    audit_filter_rules(tsk, &amp;amp;e-&gt;rule, ctx, n, &amp;amp;state, false)) {
			ctx-&gt;current_state = state;
			return 1;
		}
	}

	return 0;
}

*/ At syscall exit time, this filter is called if any audit_names have been
 collected during syscall processing.  We only check rules in sublists at hash
 buckets applicable to the inode numbers in audit_names.
 Regarding audit_state, same rules apply as for audit_filter_syscall().
 /*
void audit_filter_inodes(struct task_structtsk, struct audit_contextctx)
{
	struct audit_namesn;

	if (audit_pid &amp;amp;&amp;amp; tsk-&gt;tgid == audit_pid)
		return;

	rcu_read_lock();

	list_for_each_entry(n, &amp;amp;ctx-&gt;names_list, list) {
		if (audit_filter_inode_name(tsk, n, ctx))
			break;
	}
	rcu_read_unlock();
}

*/ Transfer the audit context pointer to the caller, clearing it in the tsk's struct /*
static inline struct audit_contextaudit_take_context(struct task_structtsk,
						      int return_valid,
						      long return_code)
{
	struct audit_contextcontext = tsk-&gt;audit_context;

	if (!context)
		return NULL;
	context-&gt;return_valid = return_valid;

	*/
	 we need to fix up the return code in the audit logs if the actual
	 return codes are later going to be fixed up by the arch specific
	 signal handlers
	
	 This is actually a test for:
	 (rc == ERESTARTSYS ) || (rc == ERESTARTNOINTR) ||
	 (rc == ERESTARTNOHAND) || (rc == ERESTART_RESTARTBLOCK)
	
	 but is faster than a bunch of ||
	 /*
	if (unlikely(return_code &lt;= -ERESTARTSYS) &amp;amp;&amp;amp;
	    (return_code &gt;= -ERESTART_RESTARTBLOCK) &amp;amp;&amp;amp;
	    (return_code != -ENOIOCTLCMD))
		context-&gt;return_code = -EINTR;
	else
		context-&gt;return_code  = return_code;

	if (context-&gt;in_syscall &amp;amp;&amp;amp; !context-&gt;dummy) {
		audit_filter_syscall(tsk, context, &amp;amp;audit_filter_list[AUDIT_FILTER_EXIT]);
		audit_filter_inodes(tsk, context);
	}

	tsk-&gt;audit_context = NULL;
	return context;
}

static inline void audit_proctitle_free(struct audit_contextcontext)
{
	kfree(context-&gt;proctitle.value);
	context-&gt;proctitle.value = NULL;
	context-&gt;proctitle.len = 0;
}

static inline void audit_free_names(struct audit_contextcontext)
{
	struct audit_namesn,next;

	list_for_each_entry_safe(n, next, &amp;amp;context-&gt;names_list, list) {
		list_del(&amp;amp;n-&gt;list);
		if (n-&gt;name)
			putname(n-&gt;name);
		if (n-&gt;should_free)
			kfree(n);
	}
	context-&gt;name_count = 0;
	path_put(&amp;amp;context-&gt;pwd);
	context-&gt;pwd.dentry = NULL;
	context-&gt;pwd.mnt = NULL;
}

static inline void audit_free_aux(struct audit_contextcontext)
{
	struct audit_aux_dataaux;

	while ((aux = context-&gt;aux)) {
		context-&gt;aux = aux-&gt;next;
		kfree(aux);
	}
	while ((aux = context-&gt;aux_pids)) {
		context-&gt;aux_pids = aux-&gt;next;
		kfree(aux);
	}
}

static inline struct audit_contextaudit_alloc_context(enum audit_state state)
{
	struct audit_contextcontext;

	context = kzalloc(sizeof(*context), GFP_KERNEL);
	if (!context)
		return NULL;
	context-&gt;state = state;
	context-&gt;prio = state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;
	INIT_LIST_HEAD(&amp;amp;context-&gt;killed_trees);
	INIT_LIST_HEAD(&amp;amp;context-&gt;names_list);
	return context;
}

*/
 audit_alloc - allocate an audit context block for a task
 @tsk: task

 Filter on the task information and allocate a per-task audit context
 if necessary.  Doing so turns on system call auditing for the
 specified task.  This is called from copy_process, so no lock is
 needed.
 /*
int audit_alloc(struct task_structtsk)
{
	struct audit_contextcontext;
	enum audit_state     state;
	charkey = NULL;

	if (likely(!audit_ever_enabled))
		return 0;/ Return if not auditing. /*

	state = audit_filter_task(tsk, &amp;amp;key);
	if (state == AUDIT_DISABLED) {
		clear_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
		return 0;
	}

	if (!(context = audit_alloc_context(state))) {
		kfree(key);
		audit_log_lost("out of memory in audit_alloc");
		return -ENOMEM;
	}
	context-&gt;filterkey = key;

	tsk-&gt;audit_context  = context;
	set_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
	return 0;
}

static inline void audit_free_context(struct audit_contextcontext)
{
	audit_free_names(context);
	unroll_tree_refs(context, NULL, 0);
	free_tree_refs(context);
	audit_free_aux(context);
	kfree(context-&gt;filterkey);
	kfree(context-&gt;sockaddr);
	audit_proctitle_free(context);
	kfree(context);
}

static int audit_log_pid_context(struct audit_contextcontext, pid_t pid,
				 kuid_t auid, kuid_t uid, unsigned int sessionid,
				 u32 sid, charcomm)
{
	struct audit_bufferab;
	charctx = NULL;
	u32 len;
	int rc = 0;

	ab = audit_log_start(context, GFP_KERNEL, AUDIT_OBJ_PID);
	if (!ab)
		return rc;

	audit_log_format(ab, "opid=%d oauid=%d ouid=%d oses=%d", pid,
			 from_kuid(&amp;amp;init_user_ns, auid),
			 from_kuid(&amp;amp;init_user_ns, uid), sessionid);
	if (sid) {
		if (security_secid_to_secctx(sid, &amp;amp;ctx, &amp;amp;len)) {
			audit_log_format(ab, " obj=(none)");
			rc = 1;
		} else {
			audit_log_format(ab, " obj=%s", ctx);
			security_release_secctx(ctx, len);
		}
	}
	audit_log_format(ab, " ocomm=");
	audit_log_untrustedstring(ab, comm);
	audit_log_end(ab);

	return rc;
}

*/
 to_send and len_sent accounting are very loose estimates.  We aren't
 really worried about a hard cap to MAX_EXECVE_AUDIT_LEN so much as being
 within about 500 bytes (next page boundary)

 why snprintf?  an int is up to 12 digits long.  if we just assumed when
 logging that a[%d]= was going to be 16 characters long we would be wasting
 space in every audit message.  In one 7500 byte message we can log up to
 about 1000 min size arguments.  That comes down to about 50% waste of space
 if we didn't do the snprintf to find out how long arg_num_len was.
 /*
static int audit_log_single_execve_arg(struct audit_contextcontext,
					struct audit_buffer*ab,
					int arg_num,
					size_tlen_sent,
					const char __userp,
					charbuf)
{
	char arg_num_len_buf[12];
	const char __usertmp_p = p;
	*/ how many digits are in arg_num? 5 is the length of ' a=""' /*
	size_t arg_num_len = snprintf(arg_num_len_buf, 12, "%d", arg_num) + 5;
	size_t len, len_left, to_send;
	size_t max_execve_audit_len = MAX_EXECVE_AUDIT_LEN;
	unsigned int i, has_cntl = 0, too_long = 0;
	int ret;

	*/ strnlen_user includes the null we don't want to send /*
	len_left = len = strnlen_user(p, MAX_ARG_STRLEN) - 1;

	*/
	 We just created this mm, if we can't find the strings
	 we just copied into it something is _very_ wrong. Similar
	 for strings that are too long, we should not have created
	 any.
	 /*
	if (WARN_ON_ONCE(len &lt; 0 || len &gt; MAX_ARG_STRLEN - 1)) {
		send_sig(SIGKILL, current, 0);
		return -1;
	}

	*/ walk the whole argument looking for non-ascii chars /*
	do {
		if (len_left &gt; MAX_EXECVE_AUDIT_LEN)
			to_send = MAX_EXECVE_AUDIT_LEN;
		else
			to_send = len_left;
		ret = copy_from_user(buf, tmp_p, to_send);
		*/
		 There is no reason for this copy to be short. We just
		 copied them here, and the mm hasn't been exposed to user-
		 space yet.
		 /*
		if (ret) {
			WARN_ON(1);
			send_sig(SIGKILL, current, 0);
			return -1;
		}
		buf[to_send] = '\0';
		has_cntl = audit_string_contains_control(buf, to_send);
		if (has_cntl) {
			*/
			 hex messages get logged as 2 bytes, so we can only
			 send half as much in each message
			 /*
			max_execve_audit_len = MAX_EXECVE_AUDIT_LEN / 2;
			break;
		}
		len_left -= to_send;
		tmp_p += to_send;
	} while (len_left &gt; 0);

	len_left = len;

	if (len &gt; max_execve_audit_len)
		too_long = 1;

	*/ rewalk the argument actually logging the message /*
	for (i = 0; len_left &gt; 0; i++) {
		int room_left;

		if (len_left &gt; max_execve_audit_len)
			to_send = max_execve_audit_len;
		else
			to_send = len_left;

		*/ do we have space left to send this argument in this ab? /*
		room_left = MAX_EXECVE_AUDIT_LEN - arg_num_len -len_sent;
		if (has_cntl)
			room_left -= (to_send 2);
		else
			room_left -= to_send;
		if (room_left &lt; 0) {
			*len_sent = 0;
			audit_log_end(*ab);
			*ab = audit_log_start(context, GFP_KERNEL, AUDIT_EXECVE);
			if (!*ab)
				return 0;
		}

		*/
		 first record needs to say how long the original string was
		 so we can be sure nothing was lost.
		 /*
		if ((i == 0) &amp;amp;&amp;amp; (too_long))
			audit_log_format(*ab, " a%d_len=%zu", arg_num,
					 has_cntl ? 2*len : len);

		*/
		 normally arguments are small enough to fit and we already
		 filled buf above when we checked for control characters
		 so don't bother with another copy_from_user
		 /*
		if (len &gt;= max_execve_audit_len)
			ret = copy_from_user(buf, p, to_send);
		else
			ret = 0;
		if (ret) {
			WARN_ON(1);
			send_sig(SIGKILL, current, 0);
			return -1;
		}
		buf[to_send] = '\0';

		*/ actually log it /*
		audit_log_format(*ab, " a%d", arg_num);
		if (too_long)
			audit_log_format(*ab, "[%d]", i);
		audit_log_format(*ab, "=");
		if (has_cntl)
			audit_log_n_hex(*ab, buf, to_send);
		else
			audit_log_string(*ab, buf);

		p += to_send;
		len_left -= to_send;
		*len_sent += arg_num_len;
		if (has_cntl)
			*len_sent += to_send 2;
		else
			*len_sent += to_send;
	}
	*/ include the null we didn't log /*
	return len + 1;
}

static void audit_log_execve_info(struct audit_contextcontext,
				  struct audit_buffer*ab)
{
	int i, len;
	size_t len_sent = 0;
	const char __userp;
	charbuf;

	p = (const char __user)current-&gt;mm-&gt;arg_start;

	audit_log_format(*ab, "argc=%d", context-&gt;execve.argc);

	*/
	 we need some kernel buffer to hold the userspace args.  Just
	 allocate one big one rather than allocating one of the right size
	 for every single argument inside audit_log_single_execve_arg()
	 should be &lt;8k allocation so should be pretty safe.
	 /*
	buf = kmalloc(MAX_EXECVE_AUDIT_LEN + 1, GFP_KERNEL);
	if (!buf) {
		audit_panic("out of memory for argv string");
		return;
	}

	for (i = 0; i &lt; context-&gt;execve.argc; i++) {
		len = audit_log_single_execve_arg(context, ab, i,
						  &amp;amp;len_sent, p, buf);
		if (len &lt;= 0)
			break;
		p += len;
	}
	kfree(buf);
}

static void show_special(struct audit_contextcontext, intcall_panic)
{
	struct audit_bufferab;
	int i;

	ab = audit_log_start(context, GFP_KERNEL, context-&gt;type);
	if (!ab)
		return;

	switch (context-&gt;type) {
	case AUDIT_SOCKETCALL: {
		int nargs = context-&gt;socketcall.nargs;
		audit_log_format(ab, "nargs=%d", nargs);
		for (i = 0; i &lt; nargs; i++)
			audit_log_format(ab, " a%d=%lx", i,
				context-&gt;socketcall.args[i]);
		break; }
	case AUDIT_IPC: {
		u32 osid = context-&gt;ipc.osid;

		audit_log_format(ab, "ouid=%u ogid=%u mode=%#ho",
				 from_kuid(&amp;amp;init_user_ns, context-&gt;ipc.uid),
				 from_kgid(&amp;amp;init_user_ns, context-&gt;ipc.gid),
				 context-&gt;ipc.mode);
		if (osid) {
			charctx = NULL;
			u32 len;
			if (security_secid_to_secctx(osid, &amp;amp;ctx, &amp;amp;len)) {
				audit_log_format(ab, " osid=%u", osid);
				*call_panic = 1;
			} else {
				audit_log_format(ab, " obj=%s", ctx);
				security_release_secctx(ctx, len);
			}
		}
		if (context-&gt;ipc.has_perm) {
			audit_log_end(ab);
			ab = audit_log_start(context, GFP_KERNEL,
					     AUDIT_IPC_SET_PERM);
			if (unlikely(!ab))
				return;
			audit_log_format(ab,
				"qbytes=%lx ouid=%u ogid=%u mode=%#ho",
				context-&gt;ipc.qbytes,
				context-&gt;ipc.perm_uid,
				context-&gt;ipc.perm_gid,
				context-&gt;ipc.perm_mode);
		}
		break; }
	case AUDIT_MQ_OPEN: {
		audit_log_format(ab,
			"oflag=0x%x mode=%#ho mq_flags=0x%lx mq_maxmsg=%ld "
			"mq_msgsize=%ld mq_curmsgs=%ld",
			context-&gt;mq_open.oflag, context-&gt;mq_open.mode,
			context-&gt;mq_open.attr.mq_flags,
			context-&gt;mq_open.attr.mq_maxmsg,
			context-&gt;mq_open.attr.mq_msgsize,
			context-&gt;mq_open.attr.mq_curmsgs);
		break; }
	case AUDIT_MQ_SENDRECV: {
		audit_log_format(ab,
			"mqdes=%d msg_len=%zd msg_prio=%u "
			"abs_timeout_sec=%ld abs_timeout_nsec=%ld",
			context-&gt;mq_sendrecv.mqdes,
			context-&gt;mq_sendrecv.msg_len,
			context-&gt;mq_sendrecv.msg_prio,
			context-&gt;mq_sendrecv.abs_timeout.tv_sec,
			context-&gt;mq_sendrecv.abs_timeout.tv_nsec);
		break; }
	case AUDIT_MQ_NOTIFY: {
		audit_log_format(ab, "mqdes=%d sigev_signo=%d",
				context-&gt;mq_notify.mqdes,
				context-&gt;mq_notify.sigev_signo);
		break; }
	case AUDIT_MQ_GETSETATTR: {
		struct mq_attrattr = &amp;amp;context-&gt;mq_getsetattr.mqstat;
		audit_log_format(ab,
			"mqdes=%d mq_flags=0x%lx mq_maxmsg=%ld mq_msgsize=%ld "
			"mq_curmsgs=%ld ",
			context-&gt;mq_getsetattr.mqdes,
			attr-&gt;mq_flags, attr-&gt;mq_maxmsg,
			attr-&gt;mq_msgsize, attr-&gt;mq_curmsgs);
		break; }
	case AUDIT_CAPSET: {
		audit_log_format(ab, "pid=%d", context-&gt;capset.pid);
		audit_log_cap(ab, "cap_pi", &amp;amp;context-&gt;capset.cap.inheritable);
		audit_log_cap(ab, "cap_pp", &amp;amp;context-&gt;capset.cap.permitted);
		audit_log_cap(ab, "cap_pe", &amp;amp;context-&gt;capset.cap.effective);
		break; }
	case AUDIT_MMAP: {
		audit_log_format(ab, "fd=%d flags=0x%x", context-&gt;mmap.fd,
				 context-&gt;mmap.flags);
		break; }
	case AUDIT_EXECVE: {
		audit_log_execve_info(context, &amp;amp;ab);
		break; }
	}
	audit_log_end(ab);
}

static inline int audit_proctitle_rtrim(charproctitle, int len)
{
	charend = proctitle + len - 1;
	while (end &gt; proctitle &amp;amp;&amp;amp; !isprint(*end))
		end--;

	*/ catch the case where proctitle is only 1 non-print character /*
	len = end - proctitle + 1;
	len -= isprint(proctitle[len-1]) == 0;
	return len;
}

static void audit_log_proctitle(struct task_structtsk,
			 struct audit_contextcontext)
{
	int res;
	charbuf;
	charmsg = "(null)";
	int len = strlen(msg);
	struct audit_bufferab;

	ab = audit_log_start(context, GFP_KERNEL, AUDIT_PROCTITLE);
	if (!ab)
		return;	*/ audit_panic or being filtered /*

	audit_log_format(ab, "proctitle=");

	*/ Not  cached /*
	if (!context-&gt;proctitle.value) {
		buf = kmalloc(MAX_PROCTITLE_AUDIT_LEN, GFP_KERNEL);
		if (!buf)
			goto out;
		*/ Historically called this from procfs naming /*
		res = get_cmdline(tsk, buf, MAX_PROCTITLE_AUDIT_LEN);
		if (res == 0) {
			kfree(buf);
			goto out;
		}
		res = audit_proctitle_rtrim(buf, res);
		if (res == 0) {
			kfree(buf);
			goto out;
		}
		context-&gt;proctitle.value = buf;
		context-&gt;proctitle.len = res;
	}
	msg = context-&gt;proctitle.value;
	len = context-&gt;proctitle.len;
out:
	audit_log_n_untrustedstring(ab, msg, len);
	audit_log_end(ab);
}

static void audit_log_exit(struct audit_contextcontext, struct task_structtsk)
{
	int i, call_panic = 0;
	struct audit_bufferab;
	struct audit_aux_dataaux;
	struct audit_namesn;

	*/ tsk == current /*
	context-&gt;personality = tsk-&gt;personality;

	ab = audit_log_start(context, GFP_KERNEL, AUDIT_SYSCALL);
	if (!ab)
		return;		*/ audit_panic has been called /*
	audit_log_format(ab, "arch=%x syscall=%d",
			 context-&gt;arch, context-&gt;major);
	if (context-&gt;personality != PER_LINUX)
		audit_log_format(ab, " per=%lx", context-&gt;personality);
	if (context-&gt;return_valid)
		audit_log_format(ab, " success=%s exit=%ld",
				 (context-&gt;return_valid==AUDITSC_SUCCESS)?"yes":"no",
				 context-&gt;return_code);

	audit_log_format(ab,
			 " a0=%lx a1=%lx a2=%lx a3=%lx items=%d",
			 context-&gt;argv[0],
			 context-&gt;argv[1],
			 context-&gt;argv[2],
			 context-&gt;argv[3],
			 context-&gt;name_count);

	audit_log_task_info(ab, tsk);
	audit_log_key(ab, context-&gt;filterkey);
	audit_log_end(ab);

	for (aux = context-&gt;aux; aux; aux = aux-&gt;next) {

		ab = audit_log_start(context, GFP_KERNEL, aux-&gt;type);
		if (!ab)
			continue;/ audit_panic has been called /*

		switch (aux-&gt;type) {

		case AUDIT_BPRM_FCAPS: {
			struct audit_aux_data_bprm_fcapsaxs = (void)aux;
			audit_log_format(ab, "fver=%x", axs-&gt;fcap_ver);
			audit_log_cap(ab, "fp", &amp;amp;axs-&gt;fcap.permitted);
			audit_log_cap(ab, "fi", &amp;amp;axs-&gt;fcap.inheritable);
			audit_log_format(ab, " fe=%d", axs-&gt;fcap.fE);
			audit_log_cap(ab, "old_pp", &amp;amp;axs-&gt;old_pcap.permitted);
			audit_log_cap(ab, "old_pi", &amp;amp;axs-&gt;old_pcap.inheritable);
			audit_log_cap(ab, "old_pe", &amp;amp;axs-&gt;old_pcap.effective);
			audit_log_cap(ab, "new_pp", &amp;amp;axs-&gt;new_pcap.permitted);
			audit_log_cap(ab, "new_pi", &amp;amp;axs-&gt;new_pcap.inheritable);
			audit_log_cap(ab, "new_pe", &amp;amp;axs-&gt;new_pcap.effective);
			break; }

		}
		audit_log_end(ab);
	}

	if (context-&gt;type)
		show_special(context, &amp;amp;call_panic);

	if (context-&gt;fds[0] &gt;= 0) {
		ab = audit_log_start(context, GFP_KERNEL, AUDIT_FD_PAIR);
		if (ab) {
			audit_log_format(ab, "fd0=%d fd1=%d",
					context-&gt;fds[0], context-&gt;fds[1]);
			audit_log_end(ab);
		}
	}

	if (context-&gt;sockaddr_len) {
		ab = audit_log_start(context, GFP_KERNEL, AUDIT_SOCKADDR);
		if (ab) {
			audit_log_format(ab, "saddr=");
			audit_log_n_hex(ab, (void)context-&gt;sockaddr,
					context-&gt;sockaddr_len);
			audit_log_end(ab);
		}
	}

	for (aux = context-&gt;aux_pids; aux; aux = aux-&gt;next) {
		struct audit_aux_data_pidsaxs = (void)aux;

		for (i = 0; i &lt; axs-&gt;pid_count; i++)
			if (audit_log_pid_context(context, axs-&gt;target_pid[i],
						  axs-&gt;target_auid[i],
						  axs-&gt;target_uid[i],
						  axs-&gt;target_sessionid[i],
						  axs-&gt;target_sid[i],
						  axs-&gt;target_comm[i]))
				call_panic = 1;
	}

	if (context-&gt;target_pid &amp;amp;&amp;amp;
	    audit_log_pid_context(context, context-&gt;target_pid,
				  context-&gt;target_auid, context-&gt;target_uid,
				  context-&gt;target_sessionid,
				  context-&gt;target_sid, context-&gt;target_comm))
			call_panic = 1;

	if (context-&gt;pwd.dentry &amp;amp;&amp;amp; context-&gt;pwd.mnt) {
		ab = audit_log_start(context, GFP_KERNEL, AUDIT_CWD);
		if (ab) {
			audit_log_d_path(ab, " cwd=", &amp;amp;context-&gt;pwd);
			audit_log_end(ab);
		}
	}

	i = 0;
	list_for_each_entry(n, &amp;amp;context-&gt;names_list, list) {
		if (n-&gt;hidden)
			continue;
		audit_log_name(context, n, NULL, i++, &amp;amp;call_panic);
	}

	audit_log_proctitle(tsk, context);

	*/ Send end of event record to help user space know we are finished /*
	ab = audit_log_start(context, GFP_KERNEL, AUDIT_EOE);
	if (ab)
		audit_log_end(ab);
	if (call_panic)
		audit_panic("error converting sid to string");
}

*/
 audit_free - free a per-task audit context
 @tsk: task whose audit context block to free

 Called from copy_process and do_exit
 /*
void __audit_free(struct task_structtsk)
{
	struct audit_contextcontext;

	context = audit_take_context(tsk, 0, 0);
	if (!context)
		return;

	*/ Check for system calls that do not go through the exit
	 function (e.g., exit_group), then free context block.
	 We use GFP_ATOMIC here because we might be doing this
	 in the context of the idle thread /*
	*/ that can happen only if we are called from do_exit() /*
	if (context-&gt;in_syscall &amp;amp;&amp;amp; context-&gt;current_state == AUDIT_RECORD_CONTEXT)
		audit_log_exit(context, tsk);
	if (!list_empty(&amp;amp;context-&gt;killed_trees))
		audit_kill_trees(&amp;amp;context-&gt;killed_trees);

	audit_free_context(context);
}

*/
 audit_syscall_entry - fill in an audit record at syscall entry
 @major: major syscall type (function)
 @a1: additional syscall register 1
 @a2: additional syscall register 2
 @a3: additional syscall register 3
 @a4: additional syscall register 4

 Fill in audit context at syscall entry.  This only happens if the
 audit context was created when the task was created and the state or
 filters demand the audit context be built.  If the state from the
 per-task filter or from the per-syscall filter is AUDIT_RECORD_CONTEXT,
 then the record will be written at syscall exit time (otherwise, it
 will only be written if another part of the kernel requests that it
 be written).
 /*
void __audit_syscall_entry(int major, unsigned long a1, unsigned long a2,
			   unsigned long a3, unsigned long a4)
{
	struct task_structtsk = current;
	struct audit_contextcontext = tsk-&gt;audit_context;
	enum audit_state     state;

	if (!context)
		return;

	BUG_ON(context-&gt;in_syscall || context-&gt;name_count);

	if (!audit_enabled)
		return;

	context-&gt;arch	    = syscall_get_arch();
	context-&gt;major      = major;
	context-&gt;argv[0]    = a1;
	context-&gt;argv[1]    = a2;
	context-&gt;argv[2]    = a3;
	context-&gt;argv[3]    = a4;

	state = context-&gt;state;
	context-&gt;dummy = !audit_n_rules;
	if (!context-&gt;dummy &amp;amp;&amp;amp; state == AUDIT_BUILD_CONTEXT) {
		context-&gt;prio = 0;
		state = audit_filter_syscall(tsk, context, &amp;amp;audit_filter_list[AUDIT_FILTER_ENTRY]);
	}
	if (state == AUDIT_DISABLED)
		return;

	context-&gt;serial     = 0;
	context-&gt;ctime      = CURRENT_TIME;
	context-&gt;in_syscall = 1;
	context-&gt;current_state  = state;
	context-&gt;ppid       = 0;
}

*/
 audit_syscall_exit - deallocate audit context after a system call
 @success: success value of the syscall
 @return_code: return value of the syscall

 Tear down after system call.  If the audit context has been marked as
 auditable (either because of the AUDIT_RECORD_CONTEXT state from
 filtering, or because some other part of the kernel wrote an audit
 message), then write out the syscall information.  In call cases,
 free the names stored from getname().
 /*
void __audit_syscall_exit(int success, long return_code)
{
	struct task_structtsk = current;
	struct audit_contextcontext;

	if (success)
		success = AUDITSC_SUCCESS;
	else
		success = AUDITSC_FAILURE;

	context = audit_take_context(tsk, success, return_code);
	if (!context)
		return;

	if (context-&gt;in_syscall &amp;amp;&amp;amp; context-&gt;current_state == AUDIT_RECORD_CONTEXT)
		audit_log_exit(context, tsk);

	context-&gt;in_syscall = 0;
	context-&gt;prio = context-&gt;state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;

	if (!list_empty(&amp;amp;context-&gt;killed_trees))
		audit_kill_trees(&amp;amp;context-&gt;killed_trees);

	audit_free_names(context);
	unroll_tree_refs(context, NULL, 0);
	audit_free_aux(context);
	context-&gt;aux = NULL;
	context-&gt;aux_pids = NULL;
	context-&gt;target_pid = 0;
	context-&gt;target_sid = 0;
	context-&gt;sockaddr_len = 0;
	context-&gt;type = 0;
	context-&gt;fds[0] = -1;
	if (context-&gt;state != AUDIT_RECORD_CONTEXT) {
		kfree(context-&gt;filterkey);
		context-&gt;filterkey = NULL;
	}
	tsk-&gt;audit_context = context;
}

static inline void handle_one(const struct inodeinode)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_contextcontext;
	struct audit_tree_refsp;
	struct audit_chunkchunk;
	int count;
	if (likely(hlist_empty(&amp;amp;inode-&gt;i_fsnotify_marks)))
		return;
	context = current-&gt;audit_context;
	p = context-&gt;trees;
	count = context-&gt;tree_count;
	rcu_read_lock();
	chunk = audit_tree_lookup(inode);
	rcu_read_unlock();
	if (!chunk)
		return;
	if (likely(put_tree_ref(context, chunk)))
		return;
	if (unlikely(!grow_tree_refs(context))) {
		pr_warn("out of memory, audit has lost a tree reference\n");
		audit_set_auditable(context);
		audit_put_chunk(chunk);
		unroll_tree_refs(context, p, count);
		return;
	}
	put_tree_ref(context, chunk);
#endif
}

static void handle_path(const struct dentrydentry)
{
#ifdef CONFIG_AUDIT_TREE
	struct audit_contextcontext;
	struct audit_tree_refsp;
	const struct dentryd,parent;
	struct audit_chunkdrop;
	unsigned long seq;
	int count;

	context = current-&gt;audit_context;
	p = context-&gt;trees;
	count = context-&gt;tree_count;
retry:
	drop = NULL;
	d = dentry;
	rcu_read_lock();
	seq = read_seqbegin(&amp;amp;rename_lock);
	for(;;) {
		struct inodeinode = d_backing_inode(d);
		if (inode &amp;amp;&amp;amp; unlikely(!hlist_empty(&amp;amp;inode-&gt;i_fsnotify_marks))) {
			struct audit_chunkchunk;
			chunk = audit_tree_lookup(inode);
			if (chunk) {
				if (unlikely(!put_tree_ref(context, chunk))) {
					drop = chunk;
					break;
				}
			}
		}
		parent = d-&gt;d_parent;
		if (parent == d)
			break;
		d = parent;
	}
	if (unlikely(read_seqretry(&amp;amp;rename_lock, seq) || drop)) { / in this order /*
		rcu_read_unlock();
		if (!drop) {
			*/ just a race with rename /*
			unroll_tree_refs(context, p, count);
			goto retry;
		}
		audit_put_chunk(drop);
		if (grow_tree_refs(context)) {
			*/ OK, got more space /*
			unroll_tree_refs(context, p, count);
			goto retry;
		}
		*/ too bad /*
		pr_warn("out of memory, audit has lost a tree reference\n");
		unroll_tree_refs(context, p, count);
		audit_set_auditable(context);
		return;
	}
	rcu_read_unlock();
#endif
}

static struct audit_namesaudit_alloc_name(struct audit_contextcontext,
						unsigned char type)
{
	struct audit_namesaname;

	if (context-&gt;name_count &lt; AUDIT_NAMES) {
		aname = &amp;amp;context-&gt;preallocated_names[context-&gt;name_count];
		memset(aname, 0, sizeof(*aname));
	} else {
		aname = kzalloc(sizeof(*aname), GFP_NOFS);
		if (!aname)
			return NULL;
		aname-&gt;should_free = true;
	}

	aname-&gt;ino = AUDIT_INO_UNSET;
	aname-&gt;type = type;
	list_add_tail(&amp;amp;aname-&gt;list, &amp;amp;context-&gt;names_list);

	context-&gt;name_count++;
	return aname;
}

*/
 audit_reusename - fill out filename with info from existing entry
 @uptr: userland ptr to pathname

 Search the audit_names list for the current audit context. If there is an
 existing entry with a matching "uptr" then return the filename
 associated with that audit_name. If not, return NULL.
 /*
struct filename
__audit_reusename(const __user charuptr)
{
	struct audit_contextcontext = current-&gt;audit_context;
	struct audit_namesn;

	list_for_each_entry(n, &amp;amp;context-&gt;names_list, list) {
		if (!n-&gt;name)
			continue;
		if (n-&gt;name-&gt;uptr == uptr) {
			n-&gt;name-&gt;refcnt++;
			return n-&gt;name;
		}
	}
	return NULL;
}

*/
 audit_getname - add a name to the list
 @name: name to add

 Add a name to the list of audit names for this context.
 Called from fs/namei.c:getname().
 /*
void __audit_getname(struct filenamename)
{
	struct audit_contextcontext = current-&gt;audit_context;
	struct audit_namesn;

	if (!context-&gt;in_syscall)
		return;

	n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
	if (!n)
		return;

	n-&gt;name = name;
	n-&gt;name_len = AUDIT_NAME_FULL;
	name-&gt;aname = n;
	name-&gt;refcnt++;

	if (!context-&gt;pwd.dentry)
		get_fs_pwd(current-&gt;fs, &amp;amp;context-&gt;pwd);
}

*/
 __audit_inode - store the inode and device from a lookup
 @name: name being audited
 @dentry: dentry being audited
 @flags: attributes for this particular entry
 /*
void __audit_inode(struct filenamename, const struct dentrydentry,
		   unsigned int flags)
{
	struct audit_contextcontext = current-&gt;audit_context;
	struct inodeinode = d_backing_inode(dentry);
	struct audit_namesn;
	bool parent = flags &amp;amp; AUDIT_INODE_PARENT;

	if (!context-&gt;in_syscall)
		return;

	if (!name)
		goto out_alloc;

	*/
	 If we have a pointer to an audit_names entry already, then we can
	 just use it directly if the type is correct.
	 /*
	n = name-&gt;aname;
	if (n) {
		if (parent) {
			if (n-&gt;type == AUDIT_TYPE_PARENT ||
			    n-&gt;type == AUDIT_TYPE_UNKNOWN)
				goto out;
		} else {
			if (n-&gt;type != AUDIT_TYPE_PARENT)
				goto out;
		}
	}

	list_for_each_entry_reverse(n, &amp;amp;context-&gt;names_list, list) {
		if (n-&gt;ino) {
			*/ valid inode number, use that for the comparison /*
			if (n-&gt;ino != inode-&gt;i_ino ||
			    n-&gt;dev != inode-&gt;i_sb-&gt;s_dev)
				continue;
		} else if (n-&gt;name) {
			*/ inode number has not been set, check the name /*
			if (strcmp(n-&gt;name-&gt;name, name-&gt;name))
				continue;
		} else
			*/ no inode and no name (?!) ... this is odd ... /*
			continue;

		*/ match the correct record type /*
		if (parent) {
			if (n-&gt;type == AUDIT_TYPE_PARENT ||
			    n-&gt;type == AUDIT_TYPE_UNKNOWN)
				goto out;
		} else {
			if (n-&gt;type != AUDIT_TYPE_PARENT)
				goto out;
		}
	}

out_alloc:
	*/ unable to find an entry with both a matching name and type /*
	n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
	if (!n)
		return;
	if (name) {
		n-&gt;name = name;
		name-&gt;refcnt++;
	}

out:
	if (parent) {
		n-&gt;name_len = n-&gt;name ? parent_len(n-&gt;name-&gt;name) : AUDIT_NAME_FULL;
		n-&gt;type = AUDIT_TYPE_PARENT;
		if (flags &amp;amp; AUDIT_INODE_HIDDEN)
			n-&gt;hidden = true;
	} else {
		n-&gt;name_len = AUDIT_NAME_FULL;
		n-&gt;type = AUDIT_TYPE_NORMAL;
	}
	handle_path(dentry);
	audit_copy_inode(n, dentry, inode);
}

void __audit_file(const struct filefile)
{
	__audit_inode(NULL, file-&gt;f_path.dentry, 0);
}

*/
 __audit_inode_child - collect inode info for created/removed objects
 @parent: inode of dentry parent
 @dentry: dentry being audited
 @type:   AUDIT_TYPE_* value that we're looking for

 For syscalls that create or remove filesystem objects, audit_inode
 can only collect information for the filesystem object's parent.
 This call updates the audit context with the child's information.
 Syscalls that create a new filesystem object must be hooked after
 the object is created.  Syscalls that remove a filesystem object
 must be hooked prior, in order to capture the target inode during
 unsuccessful attempts.
 /*
void __audit_inode_child(struct inodeparent,
			 const struct dentrydentry,
			 const unsigned char type)
{
	struct audit_contextcontext = current-&gt;audit_context;
	struct inodeinode = d_backing_inode(dentry);
	const chardname = dentry-&gt;d_name.name;
	struct audit_namesn,found_parent = NULL,found_child = NULL;

	if (!context-&gt;in_syscall)
		return;

	if (inode)
		handle_one(inode);

	*/ look for a parent entry first /*
	list_for_each_entry(n, &amp;amp;context-&gt;names_list, list) {
		if (!n-&gt;name ||
		    (n-&gt;type != AUDIT_TYPE_PARENT &amp;amp;&amp;amp;
		     n-&gt;type != AUDIT_TYPE_UNKNOWN))
			continue;

		if (n-&gt;ino == parent-&gt;i_ino &amp;amp;&amp;amp; n-&gt;dev == parent-&gt;i_sb-&gt;s_dev &amp;amp;&amp;amp;
		    !audit_compare_dname_path(dname,
					      n-&gt;name-&gt;name, n-&gt;name_len)) {
			if (n-&gt;type == AUDIT_TYPE_UNKNOWN)
				n-&gt;type = AUDIT_TYPE_PARENT;
			found_parent = n;
			break;
		}
	}

	*/ is there a matching child entry? /*
	list_for_each_entry(n, &amp;amp;context-&gt;names_list, list) {
		*/ can only match entries that have a name /*
		if (!n-&gt;name ||
		    (n-&gt;type != type &amp;amp;&amp;amp; n-&gt;type != AUDIT_TYPE_UNKNOWN))
			continue;

		if (!strcmp(dname, n-&gt;name-&gt;name) ||
		    !audit_compare_dname_path(dname, n-&gt;name-&gt;name,
						found_parent ?
						found_parent-&gt;name_len :
						AUDIT_NAME_FULL)) {
			if (n-&gt;type == AUDIT_TYPE_UNKNOWN)
				n-&gt;type = type;
			found_child = n;
			break;
		}
	}

	if (!found_parent) {
		*/ create a new, "anonymous" parent record /*
		n = audit_alloc_name(context, AUDIT_TYPE_PARENT);
		if (!n)
			return;
		audit_copy_inode(n, NULL, parent);
	}

	if (!found_child) {
		found_child = audit_alloc_name(context, type);
		if (!found_child)
			return;

		*/ Re-use the name belonging to the slot for a matching parent
		 directory. All names for this context are relinquished in
		 audit_free_names() /*
		if (found_parent) {
			found_child-&gt;name = found_parent-&gt;name;
			found_child-&gt;name_len = AUDIT_NAME_FULL;
			found_child-&gt;name-&gt;refcnt++;
		}
	}

	if (inode)
		audit_copy_inode(found_child, dentry, inode);
	else
		found_child-&gt;ino = AUDIT_INO_UNSET;
}
EXPORT_SYMBOL_GPL(__audit_inode_child);

*/
 auditsc_get_stamp - get local copies of audit_context values
 @ctx: audit_context for the task
 @t: timespec to store time recorded in the audit_context
 @serial: serial value that is recorded in the audit_context

 Also sets the context as auditable.
 /*
int auditsc_get_stamp(struct audit_contextctx,
		       struct timespect, unsigned intserial)
{
	if (!ctx-&gt;in_syscall)
		return 0;
	if (!ctx-&gt;serial)
		ctx-&gt;serial = audit_serial();
	t-&gt;tv_sec  = ctx-&gt;ctime.tv_sec;
	t-&gt;tv_nsec = ctx-&gt;ctime.tv_nsec;
	*serial    = ctx-&gt;serial;
	if (!ctx-&gt;prio) {
		ctx-&gt;prio = 1;
		ctx-&gt;current_state = AUDIT_RECORD_CONTEXT;
	}
	return 1;
}

*/ global counter which is incremented every time something logs in /*
static atomic_t session_id = ATOMIC_INIT(0);

static int audit_set_loginuid_perm(kuid_t loginuid)
{
	*/ if we are unset, we don't need privs /*
	if (!audit_loginuid_set(current))
		return 0;
	*/ if AUDIT_FEATURE_LOGINUID_IMMUTABLE means never ever allow a change/*
	if (is_audit_feature_set(AUDIT_FEATURE_LOGINUID_IMMUTABLE))
		return -EPERM;
	*/ it is set, you need permission /*
	if (!capable(CAP_AUDIT_CONTROL))
		return -EPERM;
	*/ reject if this is not an unset and we don't allow that /*
	if (is_audit_feature_set(AUDIT_FEATURE_ONLY_UNSET_LOGINUID) &amp;amp;&amp;amp; uid_valid(loginuid))
		return -EPERM;
	return 0;
}

static void audit_log_set_loginuid(kuid_t koldloginuid, kuid_t kloginuid,
				   unsigned int oldsessionid, unsigned int sessionid,
				   int rc)
{
	struct audit_bufferab;
	uid_t uid, oldloginuid, loginuid;

	if (!audit_enabled)
		return;

	uid = from_kuid(&amp;amp;init_user_ns, task_uid(current));
	oldloginuid = from_kuid(&amp;amp;init_user_ns, koldloginuid);
	loginuid = from_kuid(&amp;amp;init_user_ns, kloginuid),

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_LOGIN);
	if (!ab)
		return;
	audit_log_format(ab, "pid=%d uid=%u", task_pid_nr(current), uid);
	audit_log_task_context(ab);
	audit_log_format(ab, " old-auid=%u auid=%u old-ses=%u ses=%u res=%d",
			 oldloginuid, loginuid, oldsessionid, sessionid, !rc);
	audit_log_end(ab);
}

*/
 audit_set_loginuid - set current task's audit_context loginuid
 @loginuid: loginuid value

 Returns 0.

 Called (set) from fs/proc/base.c::proc_loginuid_write().
 /*
int audit_set_loginuid(kuid_t loginuid)
{
	struct task_structtask = current;
	unsigned int oldsessionid, sessionid = (unsigned int)-1;
	kuid_t oldloginuid;
	int rc;

	oldloginuid = audit_get_loginuid(current);
	oldsessionid = audit_get_sessionid(current);

	rc = audit_set_loginuid_perm(loginuid);
	if (rc)
		goto out;

	*/ are we setting or clearing? /*
	if (uid_valid(loginuid))
		sessionid = (unsigned int)atomic_inc_return(&amp;amp;session_id);

	task-&gt;sessionid = sessionid;
	task-&gt;loginuid = loginuid;
out:
	audit_log_set_loginuid(oldloginuid, loginuid, oldsessionid, sessionid, rc);
	return rc;
}

*/
 __audit_mq_open - record audit data for a POSIX MQ open
 @oflag: open flag
 @mode: mode bits
 @attr: queue attributes

 /*
void __audit_mq_open(int oflag, umode_t mode, struct mq_attrattr)
{
	struct audit_contextcontext = current-&gt;audit_context;

	if (attr)
		memcpy(&amp;amp;context-&gt;mq_open.attr, attr, sizeof(struct mq_attr));
	else
		memset(&amp;amp;context-&gt;mq_open.attr, 0, sizeof(struct mq_attr));

	context-&gt;mq_open.oflag = oflag;
	context-&gt;mq_open.mode = mode;

	context-&gt;type = AUDIT_MQ_OPEN;
}

*/
 __audit_mq_sendrecv - record audit data for a POSIX MQ timed send/receive
 @mqdes: MQ descriptor
 @msg_len: Message length
 @msg_prio: Message priority
 @abs_timeout: Message timeout in absolute time

 /*
void __audit_mq_sendrecv(mqd_t mqdes, size_t msg_len, unsigned int msg_prio,
			const struct timespecabs_timeout)
{
	struct audit_contextcontext = current-&gt;audit_context;
	struct timespecp = &amp;amp;context-&gt;mq_sendrecv.abs_timeout;

	if (abs_timeout)
		memcpy(p, abs_timeout, sizeof(struct timespec));
	else
		memset(p, 0, sizeof(struct timespec));

	context-&gt;mq_sendrecv.mqdes = mqdes;
	context-&gt;mq_sendrecv.msg_len = msg_len;
	context-&gt;mq_sendrecv.msg_prio = msg_prio;

	context-&gt;type = AUDIT_MQ_SENDRECV;
}

*/
 __audit_mq_notify - record audit data for a POSIX MQ notify
 @mqdes: MQ descriptor
 @notification: Notification event

 /*

void __audit_mq_notify(mqd_t mqdes, const struct sigeventnotification)
{
	struct audit_contextcontext = current-&gt;audit_context;

	if (notification)
		context-&gt;mq_notify.sigev_signo = notification-&gt;sigev_signo;
	else
		context-&gt;mq_notify.sigev_signo = 0;

	context-&gt;mq_notify.mqdes = mqdes;
	context-&gt;type = AUDIT_MQ_NOTIFY;
}

*/
 __audit_mq_getsetattr - record audit data for a POSIX MQ get/set attribute
 @mqdes: MQ descriptor
 @mqstat: MQ flags

 /*
void __audit_mq_getsetattr(mqd_t mqdes, struct mq_attrmqstat)
{
	struct audit_contextcontext = current-&gt;audit_context;
	context-&gt;mq_getsetattr.mqdes = mqdes;
	context-&gt;mq_getsetattr.mqstat =mqstat;
	context-&gt;type = AUDIT_MQ_GETSETATTR;
}

*/
 audit_ipc_obj - record audit data for ipc object
 @ipcp: ipc permissions

 /*
void __audit_ipc_obj(struct kern_ipc_permipcp)
{
	struct audit_contextcontext = current-&gt;audit_context;
	context-&gt;ipc.uid = ipcp-&gt;uid;
	context-&gt;ipc.gid = ipcp-&gt;gid;
	context-&gt;ipc.mode = ipcp-&gt;mode;
	context-&gt;ipc.has_perm = 0;
	security_ipc_getsecid(ipcp, &amp;amp;context-&gt;ipc.osid);
	context-&gt;type = AUDIT_IPC;
}

*/
 audit_ipc_set_perm - record audit data for new ipc permissions
 @qbytes: msgq bytes
 @uid: msgq user id
 @gid: msgq group id
 @mode: msgq mode (permissions)

 Called only after audit_ipc_obj().
 /*
void __audit_ipc_set_perm(unsigned long qbytes, uid_t uid, gid_t gid, umode_t mode)
{
	struct audit_contextcontext = current-&gt;audit_context;

	context-&gt;ipc.qbytes = qbytes;
	context-&gt;ipc.perm_uid = uid;
	context-&gt;ipc.perm_gid = gid;
	context-&gt;ipc.perm_mode = mode;
	context-&gt;ipc.has_perm = 1;
}

void __audit_bprm(struct linux_binprmbprm)
{
	struct audit_contextcontext = current-&gt;audit_context;

	context-&gt;type = AUDIT_EXECVE;
	context-&gt;execve.argc = bprm-&gt;argc;
}


*/
 audit_socketcall - record audit data for sys_socketcall
 @nargs: number of args, which should not be more than AUDITSC_ARGS.
 @args: args array

 /*
int __audit_socketcall(int nargs, unsigned longargs)
{
	struct audit_contextcontext = current-&gt;audit_context;

	if (nargs &lt;= 0 || nargs &gt; AUDITSC_ARGS || !args)
		return -EINVAL;
	context-&gt;type = AUDIT_SOCKETCALL;
	context-&gt;socketcall.nargs = nargs;
	memcpy(context-&gt;socketcall.args, args, nargs sizeof(unsigned long));
	return 0;
}

*/
 __audit_fd_pair - record audit data for pipe and socketpair
 @fd1: the first file descriptor
 @fd2: the second file descriptor

 /*
void __audit_fd_pair(int fd1, int fd2)
{
	struct audit_contextcontext = current-&gt;audit_context;
	context-&gt;fds[0] = fd1;
	context-&gt;fds[1] = fd2;
}

*/
 audit_sockaddr - record audit data for sys_bind, sys_connect, sys_sendto
 @len: data length in user space
 @a: data address in kernel space

 Returns 0 for success or NULL context or &lt; 0 on error.
 /*
int __audit_sockaddr(int len, voida)
{
	struct audit_contextcontext = current-&gt;audit_context;

	if (!context-&gt;sockaddr) {
		voidp = kmalloc(sizeof(struct sockaddr_storage), GFP_KERNEL);
		if (!p)
			return -ENOMEM;
		context-&gt;sockaddr = p;
	}

	context-&gt;sockaddr_len = len;
	memcpy(context-&gt;sockaddr, a, len);
	return 0;
}

void __audit_ptrace(struct task_structt)
{
	struct audit_contextcontext = current-&gt;audit_context;

	context-&gt;target_pid = task_pid_nr(t);
	context-&gt;target_auid = audit_get_loginuid(t);
	context-&gt;target_uid = task_uid(t);
	context-&gt;target_sessionid = audit_get_sessionid(t);
	security_task_getsecid(t, &amp;amp;context-&gt;target_sid);
	memcpy(context-&gt;target_comm, t-&gt;comm, TASK_COMM_LEN);
}

*/
 audit_signal_info - record signal info for shutting down audit subsystem
 @sig: signal value
 @t: task being signaled

 If the audit subsystem is being terminated, record the task (pid)
 and uid that is doing that.
 /*
int __audit_signal_info(int sig, struct task_structt)
{
	struct audit_aux_data_pidsaxp;
	struct task_structtsk = current;
	struct audit_contextctx = tsk-&gt;audit_context;
	kuid_t uid = current_uid(), t_uid = task_uid(t);

	if (audit_pid &amp;amp;&amp;amp; t-&gt;tgid == audit_pid) {
		if (sig == SIGTERM || sig == SIGHUP || sig == SIGUSR1 || sig == SIGUSR2) {
			audit_sig_pid = task_pid_nr(tsk);
			if (uid_valid(tsk-&gt;loginuid))
				audit_sig_uid = tsk-&gt;loginuid;
			else
				audit_sig_uid = uid;
			security_task_getsecid(tsk, &amp;amp;audit_sig_sid);
		}
		if (!audit_signals || audit_dummy_context())
			return 0;
	}

	*/ optimize the common case by putting first signal recipient directly
	 in audit_context /*
	if (!ctx-&gt;target_pid) {
		ctx-&gt;target_pid = task_tgid_nr(t);
		ctx-&gt;target_auid = audit_get_loginuid(t);
		ctx-&gt;target_uid = t_uid;
		ctx-&gt;target_sessionid = audit_get_sessionid(t);
		security_task_getsecid(t, &amp;amp;ctx-&gt;target_sid);
		memcpy(ctx-&gt;target_comm, t-&gt;comm, TASK_COMM_LEN);
		return 0;
	}

	axp = (void)ctx-&gt;aux_pids;
	if (!axp || axp-&gt;pid_count == AUDIT_AUX_PIDS) {
		axp = kzalloc(sizeof(*axp), GFP_ATOMIC);
		if (!axp)
			return -ENOMEM;

		axp-&gt;d.type = AUDIT_OBJ_PID;
		axp-&gt;d.next = ctx-&gt;aux_pids;
		ctx-&gt;aux_pids = (void)axp;
	}
	BUG_ON(axp-&gt;pid_count &gt;= AUDIT_AUX_PIDS);

	axp-&gt;target_pid[axp-&gt;pid_count] = task_tgid_nr(t);
	axp-&gt;target_auid[axp-&gt;pid_count] = audit_get_loginuid(t);
	axp-&gt;target_uid[axp-&gt;pid_count] = t_uid;
	axp-&gt;target_sessionid[axp-&gt;pid_count] = audit_get_sessionid(t);
	security_task_getsecid(t, &amp;amp;axp-&gt;target_sid[axp-&gt;pid_count]);
	memcpy(axp-&gt;target_comm[axp-&gt;pid_count], t-&gt;comm, TASK_COMM_LEN);
	axp-&gt;pid_count++;

	return 0;
}

*/
 __audit_log_bprm_fcaps - store information about a loading bprm and relevant fcaps
 @bprm: pointer to the bprm being processed
 @new: the proposed new credentials
 @old: the old credentials

 Simply check if the proc already has the caps given by the file and if not
 store the priv escalation info for later auditing at the end of the syscall

 -Eric
 /*
int __audit_log_bprm_fcaps(struct linux_binprmbprm,
			   const struct crednew, const struct credold)
{
	struct audit_aux_data_bprm_fcapsax;
	struct audit_contextcontext = current-&gt;audit_context;
	struct cpu_vfs_cap_data vcaps;

	ax = kmalloc(sizeof(*ax), GFP_KERNEL);
	if (!ax)
		return -ENOMEM;

	ax-&gt;d.type = AUDIT_BPRM_FCAPS;
	ax-&gt;d.next = context-&gt;aux;
	context-&gt;aux = (void)ax;

	get_vfs_caps_from_disk(bprm-&gt;file-&gt;f_path.dentry, &amp;amp;vcaps);

	ax-&gt;fcap.permitted = vcaps.permitted;
	ax-&gt;fcap.inheritable = vcaps.inheritable;
	ax-&gt;fcap.fE = !!(vcaps.magic_etc &amp;amp; VFS_CAP_FLAGS_EFFECTIVE);
	ax-&gt;fcap_ver = (vcaps.magic_etc &amp;amp; VFS_CAP_REVISION_MASK) &gt;&gt; VFS_CAP_REVISION_SHIFT;

	ax-&gt;old_pcap.permitted   = old-&gt;cap_permitted;
	ax-&gt;old_pcap.inheritable = old-&gt;cap_inheritable;
	ax-&gt;old_pcap.effective   = old-&gt;cap_effective;

	ax-&gt;new_pcap.permitted   = new-&gt;cap_permitted;
	ax-&gt;new_pcap.inheritable = new-&gt;cap_inheritable;
	ax-&gt;new_pcap.effective   = new-&gt;cap_effective;
	return 0;
}

*/
 __audit_log_capset - store information about the arguments to the capset syscall
 @new: the new credentials
 @old: the old (current) credentials

 Record the arguments userspace sent to sys_capset for later printing by the
 audit system if applicable
 /*
void __audit_log_capset(const struct crednew, const struct credold)
{
	struct audit_contextcontext = current-&gt;audit_context;
	context-&gt;capset.pid = task_pid_nr(current);
	context-&gt;capset.cap.effective   = new-&gt;cap_effective;
	context-&gt;capset.cap.inheritable = new-&gt;cap_effective;
	context-&gt;capset.cap.permitted   = new-&gt;cap_permitted;
	context-&gt;type = AUDIT_CAPSET;
}

void __audit_mmap_fd(int fd, int flags)
{
	struct audit_contextcontext = current-&gt;audit_context;
	context-&gt;mmap.fd = fd;
	context-&gt;mmap.flags = flags;
	context-&gt;type = AUDIT_MMAP;
}

static void audit_log_task(struct audit_bufferab)
{
	kuid_t auid, uid;
	kgid_t gid;
	unsigned int sessionid;
	char comm[sizeof(current-&gt;comm)];

	auid = audit_get_loginuid(current);
	sessionid = audit_get_sessionid(current);
	current_uid_gid(&amp;amp;uid, &amp;amp;gid);

	audit_log_format(ab, "auid=%u uid=%u gid=%u ses=%u",
			 from_kuid(&amp;amp;init_user_ns, auid),
			 from_kuid(&amp;amp;init_user_ns, uid),
			 from_kgid(&amp;amp;init_user_ns, gid),
			 sessionid);
	audit_log_task_context(ab);
	audit_log_format(ab, " pid=%d comm=", task_pid_nr(current));
	audit_log_untrustedstring(ab, get_task_comm(comm, current));
	audit_log_d_path_exe(ab, current-&gt;mm);
}

*/
 audit_core_dumps - record information about processes that end abnormally
 @signr: signal value

 If a process ends with a core dump, something fishy is going on and we
 should record the event for investigation.
 /*
void audit_core_dumps(long signr)
{
	struct audit_bufferab;

	if (!audit_enabled)
		return;

	if (signr == SIGQUIT)	*/ don't care for those /*
		return;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_ANOM_ABEND);
	if (unlikely(!ab))
		return;
	audit_log_task(ab);
	audit_log_format(ab, " sig=%ld", signr);
	audit_log_end(ab);
}

void __audit_seccomp(unsigned long syscall, long signr, int code)
{
	struct audit_bufferab;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_SECCOMP);
	if (unlikely(!ab))
		return;
	audit_log_task(ab);
	audit_log_format(ab, " sig=%ld arch=%x syscall=%ld compat=%d ip=0x%lx code=0x%x",
			 signr, syscall_get_arch(), syscall,
			 in_compat_syscall(), KSTK_EIP(current), code);
	audit_log_end(ab);
}

struct list_headaudit_killed_trees(void)
{
	struct audit_contextctx = current-&gt;audit_context;
	if (likely(!ctx || !ctx-&gt;in_syscall))
		return NULL;
	return &amp;amp;ctx-&gt;killed_trees;
}
*/
/*
#include "audit.h"
#include &lt;linux/fsnotify_backend.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/mount.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/slab.h&gt;

struct audit_tree;
struct audit_chunk;

struct audit_tree {
	atomic_t count;
	int goner;
	struct audit_chunkroot;
	struct list_head chunks;
	struct list_head rules;
	struct list_head list;
	struct list_head same_root;
	struct rcu_head head;
	char pathname[];
};

struct audit_chunk {
	struct list_head hash;
	struct fsnotify_mark mark;
	struct list_head trees;		*/ with root here /*
	int dead;
	int count;
	atomic_long_t refs;
	struct rcu_head head;
	struct node {
		struct list_head list;
		struct audit_treeowner;
		unsigned index;		*/ index; upper bit indicates 'will prune' /*
	} owners[];
};

static LIST_HEAD(tree_list);
static LIST_HEAD(prune_list);
static struct task_structprune_thread;

*/
 One struct chunk is attached to each inode of interest.
 We replace struct chunk on tagging/untagging.
 Rules have pointer to struct audit_tree.
 Rules have struct list_head rlist forming a list of rules over
 the same tree.
 References to struct chunk are collected at audit_inode{,_child}()
 time and used in AUDIT_TREE rule matching.
 These references are dropped at the same time we are calling
 audit_free_names(), etc.

 Cyclic lists galore:
 tree.chunks anchors chunk.owners[].list			hash_lock
 tree.rules anchors rule.rlist				audit_filter_mutex
 chunk.trees anchors tree.same_root				hash_lock
 chunk.hash is a hash with middle bits of watch.inode as
 a hash function.						RCU, hash_lock

 tree is refcounted; one reference for "some rules on rules_list refer to
 it", one for each chunk with pointer to it.

 chunk is refcounted by embedded fsnotify_mark + .refs (non-zero refcount
 of watch contributes 1 to .refs).

 node.index allows to get from node.list to containing chunk.
 MSB of that sucker is stolen to mark taggings that we might have to
 revert - several operations have very unpleasant cleanup logics and
 that makes a difference.  Some.
 /*

static struct fsnotify_groupaudit_tree_group;

static struct audit_treealloc_tree(const chars)
{
	struct audit_treetree;

	tree = kmalloc(sizeof(struct audit_tree) + strlen(s) + 1, GFP_KERNEL);
	if (tree) {
		atomic_set(&amp;amp;tree-&gt;count, 1);
		tree-&gt;goner = 0;
		INIT_LIST_HEAD(&amp;amp;tree-&gt;chunks);
		INIT_LIST_HEAD(&amp;amp;tree-&gt;rules);
		INIT_LIST_HEAD(&amp;amp;tree-&gt;list);
		INIT_LIST_HEAD(&amp;amp;tree-&gt;same_root);
		tree-&gt;root = NULL;
		strcpy(tree-&gt;pathname, s);
	}
	return tree;
}

static inline void get_tree(struct audit_treetree)
{
	atomic_inc(&amp;amp;tree-&gt;count);
}

static inline void put_tree(struct audit_treetree)
{
	if (atomic_dec_and_test(&amp;amp;tree-&gt;count))
		kfree_rcu(tree, head);
}

*/ to avoid bringing the entire thing in audit.h /*
const charaudit_tree_path(struct audit_treetree)
{
	return tree-&gt;pathname;
}

static void free_chunk(struct audit_chunkchunk)
{
	int i;

	for (i = 0; i &lt; chunk-&gt;count; i++) {
		if (chunk-&gt;owners[i].owner)
			put_tree(chunk-&gt;owners[i].owner);
	}
	kfree(chunk);
}

void audit_put_chunk(struct audit_chunkchunk)
{
	if (atomic_long_dec_and_test(&amp;amp;chunk-&gt;refs))
		free_chunk(chunk);
}

static void __put_chunk(struct rcu_headrcu)
{
	struct audit_chunkchunk = container_of(rcu, struct audit_chunk, head);
	audit_put_chunk(chunk);
}

static void audit_tree_destroy_watch(struct fsnotify_markentry)
{
	struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);
	call_rcu(&amp;amp;chunk-&gt;head, __put_chunk);
}

static struct audit_chunkalloc_chunk(int count)
{
	struct audit_chunkchunk;
	size_t size;
	int i;

	size = offsetof(struct audit_chunk, owners) + count sizeof(struct node);
	chunk = kzalloc(size, GFP_KERNEL);
	if (!chunk)
		return NULL;

	INIT_LIST_HEAD(&amp;amp;chunk-&gt;hash);
	INIT_LIST_HEAD(&amp;amp;chunk-&gt;trees);
	chunk-&gt;count = count;
	atomic_long_set(&amp;amp;chunk-&gt;refs, 1);
	for (i = 0; i &lt; count; i++) {
		INIT_LIST_HEAD(&amp;amp;chunk-&gt;owners[i].list);
		chunk-&gt;owners[i].index = i;
	}
	fsnotify_init_mark(&amp;amp;chunk-&gt;mark, audit_tree_destroy_watch);
	chunk-&gt;mark.mask = FS_IN_IGNORED;
	return chunk;
}

enum {HASH_SIZE = 128};
static struct list_head chunk_hash_heads[HASH_SIZE];
static __cacheline_aligned_in_smp DEFINE_SPINLOCK(hash_lock);

static inline struct list_headchunk_hash(const struct inodeinode)
{
	unsigned long n = (unsigned long)inode / L1_CACHE_BYTES;
	return chunk_hash_heads + n % HASH_SIZE;
}

*/ hash_lock &amp;amp; entry-&gt;lock is held by caller /*
static void insert_hash(struct audit_chunkchunk)
{
	struct fsnotify_markentry = &amp;amp;chunk-&gt;mark;
	struct list_headlist;

	if (!entry-&gt;inode)
		return;
	list = chunk_hash(entry-&gt;inode);
	list_add_rcu(&amp;amp;chunk-&gt;hash, list);
}

*/ called under rcu_read_lock /*
struct audit_chunkaudit_tree_lookup(const struct inodeinode)
{
	struct list_headlist = chunk_hash(inode);
	struct audit_chunkp;

	list_for_each_entry_rcu(p, list, hash) {
		*/ mark.inode may have gone NULL, but who cares? /*
		if (p-&gt;mark.inode == inode) {
			atomic_long_inc(&amp;amp;p-&gt;refs);
			return p;
		}
	}
	return NULL;
}

bool audit_tree_match(struct audit_chunkchunk, struct audit_treetree)
{
	int n;
	for (n = 0; n &lt; chunk-&gt;count; n++)
		if (chunk-&gt;owners[n].owner == tree)
			return true;
	return false;
}

*/ tagging and untagging inodes with trees /*

static struct audit_chunkfind_chunk(struct nodep)
{
	int index = p-&gt;index &amp;amp; ~(1U&lt;&lt;31);
	p -= index;
	return container_of(p, struct audit_chunk, owners[0]);
}

static void untag_chunk(struct nodep)
{
	struct audit_chunkchunk = find_chunk(p);
	struct fsnotify_markentry = &amp;amp;chunk-&gt;mark;
	struct audit_chunknew = NULL;
	struct audit_treeowner;
	int size = chunk-&gt;count - 1;
	int i, j;

	fsnotify_get_mark(entry);

	spin_unlock(&amp;amp;hash_lock);

	if (size)
		new = alloc_chunk(size);

	spin_lock(&amp;amp;entry-&gt;lock);
	if (chunk-&gt;dead || !entry-&gt;inode) {
		spin_unlock(&amp;amp;entry-&gt;lock);
		if (new)
			free_chunk(new);
		goto out;
	}

	owner = p-&gt;owner;

	if (!size) {
		chunk-&gt;dead = 1;
		spin_lock(&amp;amp;hash_lock);
		list_del_init(&amp;amp;chunk-&gt;trees);
		if (owner-&gt;root == chunk)
			owner-&gt;root = NULL;
		list_del_init(&amp;amp;p-&gt;list);
		list_del_rcu(&amp;amp;chunk-&gt;hash);
		spin_unlock(&amp;amp;hash_lock);
		spin_unlock(&amp;amp;entry-&gt;lock);
		fsnotify_destroy_mark(entry, audit_tree_group);
		goto out;
	}

	if (!new)
		goto Fallback;

	fsnotify_duplicate_mark(&amp;amp;new-&gt;mark, entry);
	if (fsnotify_add_mark(&amp;amp;new-&gt;mark, new-&gt;mark.group, new-&gt;mark.inode, NULL, 1)) {
		fsnotify_put_mark(&amp;amp;new-&gt;mark);
		goto Fallback;
	}

	chunk-&gt;dead = 1;
	spin_lock(&amp;amp;hash_lock);
	list_replace_init(&amp;amp;chunk-&gt;trees, &amp;amp;new-&gt;trees);
	if (owner-&gt;root == chunk) {
		list_del_init(&amp;amp;owner-&gt;same_root);
		owner-&gt;root = NULL;
	}

	for (i = j = 0; j &lt;= size; i++, j++) {
		struct audit_trees;
		if (&amp;amp;chunk-&gt;owners[j] == p) {
			list_del_init(&amp;amp;p-&gt;list);
			i--;
			continue;
		}
		s = chunk-&gt;owners[j].owner;
		new-&gt;owners[i].owner = s;
		new-&gt;owners[i].index = chunk-&gt;owners[j].index - j + i;
		if (!s)/ result of earlier fallback /*
			continue;
		get_tree(s);
		list_replace_init(&amp;amp;chunk-&gt;owners[j].list, &amp;amp;new-&gt;owners[i].list);
	}

	list_replace_rcu(&amp;amp;chunk-&gt;hash, &amp;amp;new-&gt;hash);
	list_for_each_entry(owner, &amp;amp;new-&gt;trees, same_root)
		owner-&gt;root = new;
	spin_unlock(&amp;amp;hash_lock);
	spin_unlock(&amp;amp;entry-&gt;lock);
	fsnotify_destroy_mark(entry, audit_tree_group);
	fsnotify_put_mark(&amp;amp;new-&gt;mark);	*/ drop initial reference /*
	goto out;

Fallback:
	// do the best we can
	spin_lock(&amp;amp;hash_lock);
	if (owner-&gt;root == chunk) {
		list_del_init(&amp;amp;owner-&gt;same_root);
		owner-&gt;root = NULL;
	}
	list_del_init(&amp;amp;p-&gt;list);
	p-&gt;owner = NULL;
	put_tree(owner);
	spin_unlock(&amp;amp;hash_lock);
	spin_unlock(&amp;amp;entry-&gt;lock);
out:
	fsnotify_put_mark(entry);
	spin_lock(&amp;amp;hash_lock);
}

static int create_chunk(struct inodeinode, struct audit_treetree)
{
	struct fsnotify_markentry;
	struct audit_chunkchunk = alloc_chunk(1);
	if (!chunk)
		return -ENOMEM;

	entry = &amp;amp;chunk-&gt;mark;
	if (fsnotify_add_mark(entry, audit_tree_group, inode, NULL, 0)) {
		fsnotify_put_mark(entry);
		return -ENOSPC;
	}

	spin_lock(&amp;amp;entry-&gt;lock);
	spin_lock(&amp;amp;hash_lock);
	if (tree-&gt;goner) {
		spin_unlock(&amp;amp;hash_lock);
		chunk-&gt;dead = 1;
		spin_unlock(&amp;amp;entry-&gt;lock);
		fsnotify_destroy_mark(entry, audit_tree_group);
		fsnotify_put_mark(entry);
		return 0;
	}
	chunk-&gt;owners[0].index = (1U &lt;&lt; 31);
	chunk-&gt;owners[0].owner = tree;
	get_tree(tree);
	list_add(&amp;amp;chunk-&gt;owners[0].list, &amp;amp;tree-&gt;chunks);
	if (!tree-&gt;root) {
		tree-&gt;root = chunk;
		list_add(&amp;amp;tree-&gt;same_root, &amp;amp;chunk-&gt;trees);
	}
	insert_hash(chunk);
	spin_unlock(&amp;amp;hash_lock);
	spin_unlock(&amp;amp;entry-&gt;lock);
	fsnotify_put_mark(entry);	*/ drop initial reference /*
	return 0;
}

*/ the first tagged inode becomes root of tree /*
static int tag_chunk(struct inodeinode, struct audit_treetree)
{
	struct fsnotify_markold_entry,chunk_entry;
	struct audit_treeowner;
	struct audit_chunkchunk,old;
	struct nodep;
	int n;

	old_entry = fsnotify_find_inode_mark(audit_tree_group, inode);
	if (!old_entry)
		return create_chunk(inode, tree);

	old = container_of(old_entry, struct audit_chunk, mark);

	*/ are we already there? /*
	spin_lock(&amp;amp;hash_lock);
	for (n = 0; n &lt; old-&gt;count; n++) {
		if (old-&gt;owners[n].owner == tree) {
			spin_unlock(&amp;amp;hash_lock);
			fsnotify_put_mark(old_entry);
			return 0;
		}
	}
	spin_unlock(&amp;amp;hash_lock);

	chunk = alloc_chunk(old-&gt;count + 1);
	if (!chunk) {
		fsnotify_put_mark(old_entry);
		return -ENOMEM;
	}

	chunk_entry = &amp;amp;chunk-&gt;mark;

	spin_lock(&amp;amp;old_entry-&gt;lock);
	if (!old_entry-&gt;inode) {
		*/ old_entry is being shot, lets just lie /*
		spin_unlock(&amp;amp;old_entry-&gt;lock);
		fsnotify_put_mark(old_entry);
		free_chunk(chunk);
		return -ENOENT;
	}

	fsnotify_duplicate_mark(chunk_entry, old_entry);
	if (fsnotify_add_mark(chunk_entry, chunk_entry-&gt;group, chunk_entry-&gt;inode, NULL, 1)) {
		spin_unlock(&amp;amp;old_entry-&gt;lock);
		fsnotify_put_mark(chunk_entry);
		fsnotify_put_mark(old_entry);
		return -ENOSPC;
	}

	*/ even though we hold old_entry-&gt;lock, this is safe since chunk_entry-&gt;lock could NEVER have been grabbed before /*
	spin_lock(&amp;amp;chunk_entry-&gt;lock);
	spin_lock(&amp;amp;hash_lock);

	*/ we now hold old_entry-&gt;lock, chunk_entry-&gt;lock, and hash_lock /*
	if (tree-&gt;goner) {
		spin_unlock(&amp;amp;hash_lock);
		chunk-&gt;dead = 1;
		spin_unlock(&amp;amp;chunk_entry-&gt;lock);
		spin_unlock(&amp;amp;old_entry-&gt;lock);

		fsnotify_destroy_mark(chunk_entry, audit_tree_group);

		fsnotify_put_mark(chunk_entry);
		fsnotify_put_mark(old_entry);
		return 0;
	}
	list_replace_init(&amp;amp;old-&gt;trees, &amp;amp;chunk-&gt;trees);
	for (n = 0, p = chunk-&gt;owners; n &lt; old-&gt;count; n++, p++) {
		struct audit_trees = old-&gt;owners[n].owner;
		p-&gt;owner = s;
		p-&gt;index = old-&gt;owners[n].index;
		if (!s)/ result of fallback in untag /*
			continue;
		get_tree(s);
		list_replace_init(&amp;amp;old-&gt;owners[n].list, &amp;amp;p-&gt;list);
	}
	p-&gt;index = (chunk-&gt;count - 1) | (1U&lt;&lt;31);
	p-&gt;owner = tree;
	get_tree(tree);
	list_add(&amp;amp;p-&gt;list, &amp;amp;tree-&gt;chunks);
	list_replace_rcu(&amp;amp;old-&gt;hash, &amp;amp;chunk-&gt;hash);
	list_for_each_entry(owner, &amp;amp;chunk-&gt;trees, same_root)
		owner-&gt;root = chunk;
	old-&gt;dead = 1;
	if (!tree-&gt;root) {
		tree-&gt;root = chunk;
		list_add(&amp;amp;tree-&gt;same_root, &amp;amp;chunk-&gt;trees);
	}
	spin_unlock(&amp;amp;hash_lock);
	spin_unlock(&amp;amp;chunk_entry-&gt;lock);
	spin_unlock(&amp;amp;old_entry-&gt;lock);
	fsnotify_destroy_mark(old_entry, audit_tree_group);
	fsnotify_put_mark(chunk_entry);	*/ drop initial reference /*
	fsnotify_put_mark(old_entry);/ pair to fsnotify_find mark_entry /*
	return 0;
}

static void audit_tree_log_remove_rule(struct audit_krulerule)
{
	struct audit_bufferab;

	ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return;
	audit_log_format(ab, "op=");
	audit_log_string(ab, "remove_rule");
	audit_log_format(ab, " dir=");
	audit_log_untrustedstring(ab, rule-&gt;tree-&gt;pathname);
	audit_log_key(ab, rule-&gt;filterkey);
	audit_log_format(ab, " list=%d res=1", rule-&gt;listnr);
	audit_log_end(ab);
}

static void kill_rules(struct audit_treetree)
{
	struct audit_krulerule,next;
	struct audit_entryentry;

	list_for_each_entry_safe(rule, next, &amp;amp;tree-&gt;rules, rlist) {
		entry = container_of(rule, struct audit_entry, rule);

		list_del_init(&amp;amp;rule-&gt;rlist);
		if (rule-&gt;tree) {
			*/ not a half-baked one /*
			audit_tree_log_remove_rule(rule);
			if (entry-&gt;rule.exe)
				audit_remove_mark(entry-&gt;rule.exe);
			rule-&gt;tree = NULL;
			list_del_rcu(&amp;amp;entry-&gt;list);
			list_del(&amp;amp;entry-&gt;rule.list);
			call_rcu(&amp;amp;entry-&gt;rcu, audit_free_rule_rcu);
		}
	}
}

*/
 finish killing struct audit_tree
 /*
static void prune_one(struct audit_treevictim)
{
	spin_lock(&amp;amp;hash_lock);
	while (!list_empty(&amp;amp;victim-&gt;chunks)) {
		struct nodep;

		p = list_entry(victim-&gt;chunks.next, struct node, list);

		untag_chunk(p);
	}
	spin_unlock(&amp;amp;hash_lock);
	put_tree(victim);
}

*/ trim the uncommitted chunks from tree /*

static void trim_marked(struct audit_treetree)
{
	struct list_headp,q;
	spin_lock(&amp;amp;hash_lock);
	if (tree-&gt;goner) {
		spin_unlock(&amp;amp;hash_lock);
		return;
	}
	*/ reorder /*
	for (p = tree-&gt;chunks.next; p != &amp;amp;tree-&gt;chunks; p = q) {
		struct nodenode = list_entry(p, struct node, list);
		q = p-&gt;next;
		if (node-&gt;index &amp;amp; (1U&lt;&lt;31)) {
			list_del_init(p);
			list_add(p, &amp;amp;tree-&gt;chunks);
		}
	}

	while (!list_empty(&amp;amp;tree-&gt;chunks)) {
		struct nodenode;

		node = list_entry(tree-&gt;chunks.next, struct node, list);

		*/ have we run out of marked? /*
		if (!(node-&gt;index &amp;amp; (1U&lt;&lt;31)))
			break;

		untag_chunk(node);
	}
	if (!tree-&gt;root &amp;amp;&amp;amp; !tree-&gt;goner) {
		tree-&gt;goner = 1;
		spin_unlock(&amp;amp;hash_lock);
		mutex_lock(&amp;amp;audit_filter_mutex);
		kill_rules(tree);
		list_del_init(&amp;amp;tree-&gt;list);
		mutex_unlock(&amp;amp;audit_filter_mutex);
		prune_one(tree);
	} else {
		spin_unlock(&amp;amp;hash_lock);
	}
}

static void audit_schedule_prune(void);

*/ called with audit_filter_mutex /*
int audit_remove_tree_rule(struct audit_krulerule)
{
	struct audit_treetree;
	tree = rule-&gt;tree;
	if (tree) {
		spin_lock(&amp;amp;hash_lock);
		list_del_init(&amp;amp;rule-&gt;rlist);
		if (list_empty(&amp;amp;tree-&gt;rules) &amp;amp;&amp;amp; !tree-&gt;goner) {
			tree-&gt;root = NULL;
			list_del_init(&amp;amp;tree-&gt;same_root);
			tree-&gt;goner = 1;
			list_move(&amp;amp;tree-&gt;list, &amp;amp;prune_list);
			rule-&gt;tree = NULL;
			spin_unlock(&amp;amp;hash_lock);
			audit_schedule_prune();
			return 1;
		}
		rule-&gt;tree = NULL;
		spin_unlock(&amp;amp;hash_lock);
		return 1;
	}
	return 0;
}

static int compare_root(struct vfsmountmnt, voidarg)
{
	return d_backing_inode(mnt-&gt;mnt_root) == arg;
}

void audit_trim_trees(void)
{
	struct list_head cursor;

	mutex_lock(&amp;amp;audit_filter_mutex);
	list_add(&amp;amp;cursor, &amp;amp;tree_list);
	while (cursor.next != &amp;amp;tree_list) {
		struct audit_treetree;
		struct path path;
		struct vfsmountroot_mnt;
		struct nodenode;
		int err;

		tree = container_of(cursor.next, struct audit_tree, list);
		get_tree(tree);
		list_del(&amp;amp;cursor);
		list_add(&amp;amp;cursor, &amp;amp;tree-&gt;list);
		mutex_unlock(&amp;amp;audit_filter_mutex);

		err = kern_path(tree-&gt;pathname, 0, &amp;amp;path);
		if (err)
			goto skip_it;

		root_mnt = collect_mounts(&amp;amp;path);
		path_put(&amp;amp;path);
		if (IS_ERR(root_mnt))
			goto skip_it;

		spin_lock(&amp;amp;hash_lock);
		list_for_each_entry(node, &amp;amp;tree-&gt;chunks, list) {
			struct audit_chunkchunk = find_chunk(node);
			*/ this could be NULL if the watch is dying else where... /*
			struct inodeinode = chunk-&gt;mark.inode;
			node-&gt;index |= 1U&lt;&lt;31;
			if (iterate_mounts(compare_root, inode, root_mnt))
				node-&gt;index &amp;amp;= ~(1U&lt;&lt;31);
		}
		spin_unlock(&amp;amp;hash_lock);
		trim_marked(tree);
		drop_collected_mounts(root_mnt);
skip_it:
		put_tree(tree);
		mutex_lock(&amp;amp;audit_filter_mutex);
	}
	list_del(&amp;amp;cursor);
	mutex_unlock(&amp;amp;audit_filter_mutex);
}

int audit_make_tree(struct audit_krulerule, charpathname, u32 op)
{

	if (pathname[0] != '/' ||
	    rule-&gt;listnr != AUDIT_FILTER_EXIT ||
	    op != Audit_equal ||
	    rule-&gt;inode_f || rule-&gt;watch || rule-&gt;tree)
		return -EINVAL;
	rule-&gt;tree = alloc_tree(pathname);
	if (!rule-&gt;tree)
		return -ENOMEM;
	return 0;
}

void audit_put_tree(struct audit_treetree)
{
	put_tree(tree);
}

static int tag_mount(struct vfsmountmnt, voidarg)
{
	return tag_chunk(d_backing_inode(mnt-&gt;mnt_root), arg);
}

*/
 That gets run when evict_chunk() ends up needing to kill audit_tree.
 Runs from a separate thread.
 /*
static int prune_tree_thread(voidunused)
{
	for (;;) {
		set_current_state(TASK_INTERRUPTIBLE);
		if (list_empty(&amp;amp;prune_list))
			schedule();
		__set_current_state(TASK_RUNNING);

		mutex_lock(&amp;amp;audit_cmd_mutex);
		mutex_lock(&amp;amp;audit_filter_mutex);

		while (!list_empty(&amp;amp;prune_list)) {
			struct audit_treevictim;

			victim = list_entry(prune_list.next,
					struct audit_tree, list);
			list_del_init(&amp;amp;victim-&gt;list);

			mutex_unlock(&amp;amp;audit_filter_mutex);

			prune_one(victim);

			mutex_lock(&amp;amp;audit_filter_mutex);
		}

		mutex_unlock(&amp;amp;audit_filter_mutex);
		mutex_unlock(&amp;amp;audit_cmd_mutex);
	}
	return 0;
}

static int audit_launch_prune(void)
{
	if (prune_thread)
		return 0;
	prune_thread = kthread_create(prune_tree_thread, NULL,
				"audit_prune_tree");
	if (IS_ERR(prune_thread)) {
		pr_err("cannot start thread audit_prune_tree");
		prune_thread = NULL;
		return -ENOMEM;
	} else {
		wake_up_process(prune_thread);
		return 0;
	}
}

*/ called with audit_filter_mutex /*
int audit_add_tree_rule(struct audit_krulerule)
{
	struct audit_treeseed = rule-&gt;tree,tree;
	struct path path;
	struct vfsmountmnt;
	int err;

	rule-&gt;tree = NULL;
	list_for_each_entry(tree, &amp;amp;tree_list, list) {
		if (!strcmp(seed-&gt;pathname, tree-&gt;pathname)) {
			put_tree(seed);
			rule-&gt;tree = tree;
			list_add(&amp;amp;rule-&gt;rlist, &amp;amp;tree-&gt;rules);
			return 0;
		}
	}
	tree = seed;
	list_add(&amp;amp;tree-&gt;list, &amp;amp;tree_list);
	list_add(&amp;amp;rule-&gt;rlist, &amp;amp;tree-&gt;rules);
	*/ do not set rule-&gt;tree yet /*
	mutex_unlock(&amp;amp;audit_filter_mutex);

	if (unlikely(!prune_thread)) {
		err = audit_launch_prune();
		if (err)
			goto Err;
	}

	err = kern_path(tree-&gt;pathname, 0, &amp;amp;path);
	if (err)
		goto Err;
	mnt = collect_mounts(&amp;amp;path);
	path_put(&amp;amp;path);
	if (IS_ERR(mnt)) {
		err = PTR_ERR(mnt);
		goto Err;
	}

	get_tree(tree);
	err = iterate_mounts(tag_mount, tree, mnt);
	drop_collected_mounts(mnt);

	if (!err) {
		struct nodenode;
		spin_lock(&amp;amp;hash_lock);
		list_for_each_entry(node, &amp;amp;tree-&gt;chunks, list)
			node-&gt;index &amp;amp;= ~(1U&lt;&lt;31);
		spin_unlock(&amp;amp;hash_lock);
	} else {
		trim_marked(tree);
		goto Err;
	}

	mutex_lock(&amp;amp;audit_filter_mutex);
	if (list_empty(&amp;amp;rule-&gt;rlist)) {
		put_tree(tree);
		return -ENOENT;
	}
	rule-&gt;tree = tree;
	put_tree(tree);

	return 0;
Err:
	mutex_lock(&amp;amp;audit_filter_mutex);
	list_del_init(&amp;amp;tree-&gt;list);
	list_del_init(&amp;amp;tree-&gt;rules);
	put_tree(tree);
	return err;
}

int audit_tag_tree(charold, charnew)
{
	struct list_head cursor, barrier;
	int failed = 0;
	struct path path1, path2;
	struct vfsmounttagged;
	int err;

	err = kern_path(new, 0, &amp;amp;path2);
	if (err)
		return err;
	tagged = collect_mounts(&amp;amp;path2);
	path_put(&amp;amp;path2);
	if (IS_ERR(tagged))
		return PTR_ERR(tagged);

	err = kern_path(old, 0, &amp;amp;path1);
	if (err) {
		drop_collected_mounts(tagged);
		return err;
	}

	mutex_lock(&amp;amp;audit_filter_mutex);
	list_add(&amp;amp;barrier, &amp;amp;tree_list);
	list_add(&amp;amp;cursor, &amp;amp;barrier);

	while (cursor.next != &amp;amp;tree_list) {
		struct audit_treetree;
		int good_one = 0;

		tree = container_of(cursor.next, struct audit_tree, list);
		get_tree(tree);
		list_del(&amp;amp;cursor);
		list_add(&amp;amp;cursor, &amp;amp;tree-&gt;list);
		mutex_unlock(&amp;amp;audit_filter_mutex);

		err = kern_path(tree-&gt;pathname, 0, &amp;amp;path2);
		if (!err) {
			good_one = path_is_under(&amp;amp;path1, &amp;amp;path2);
			path_put(&amp;amp;path2);
		}

		if (!good_one) {
			put_tree(tree);
			mutex_lock(&amp;amp;audit_filter_mutex);
			continue;
		}

		failed = iterate_mounts(tag_mount, tree, tagged);
		if (failed) {
			put_tree(tree);
			mutex_lock(&amp;amp;audit_filter_mutex);
			break;
		}

		mutex_lock(&amp;amp;audit_filter_mutex);
		spin_lock(&amp;amp;hash_lock);
		if (!tree-&gt;goner) {
			list_del(&amp;amp;tree-&gt;list);
			list_add(&amp;amp;tree-&gt;list, &amp;amp;tree_list);
		}
		spin_unlock(&amp;amp;hash_lock);
		put_tree(tree);
	}

	while (barrier.prev != &amp;amp;tree_list) {
		struct audit_treetree;

		tree = container_of(barrier.prev, struct audit_tree, list);
		get_tree(tree);
		list_del(&amp;amp;tree-&gt;list);
		list_add(&amp;amp;tree-&gt;list, &amp;amp;barrier);
		mutex_unlock(&amp;amp;audit_filter_mutex);

		if (!failed) {
			struct nodenode;
			spin_lock(&amp;amp;hash_lock);
			list_for_each_entry(node, &amp;amp;tree-&gt;chunks, list)
				node-&gt;index &amp;amp;= ~(1U&lt;&lt;31);
			spin_unlock(&amp;amp;hash_lock);
		} else {
			trim_marked(tree);
		}

		put_tree(tree);
		mutex_lock(&amp;amp;audit_filter_mutex);
	}
	list_del(&amp;amp;barrier);
	list_del(&amp;amp;cursor);
	mutex_unlock(&amp;amp;audit_filter_mutex);
	path_put(&amp;amp;path1);
	drop_collected_mounts(tagged);
	return failed;
}


static void audit_schedule_prune(void)
{
	wake_up_process(prune_thread);
}

*/
 ... and that one is done if evict_chunk() decides to delay until the end
 of syscall.  Runs synchronously.
 /*
void audit_kill_trees(struct list_headlist)
{
	mutex_lock(&amp;amp;audit_cmd_mutex);
	mutex_lock(&amp;amp;audit_filter_mutex);

	while (!list_empty(list)) {
		struct audit_treevictim;

		victim = list_entry(list-&gt;next, struct audit_tree, list);
		kill_rules(victim);
		list_del_init(&amp;amp;victim-&gt;list);

		mutex_unlock(&amp;amp;audit_filter_mutex);

		prune_one(victim);

		mutex_lock(&amp;amp;audit_filter_mutex);
	}

	mutex_unlock(&amp;amp;audit_filter_mutex);
	mutex_unlock(&amp;amp;audit_cmd_mutex);
}

*/
  Here comes the stuff asynchronous to auditctl operations
 /*

static void evict_chunk(struct audit_chunkchunk)
{
	struct audit_treeowner;
	struct list_headpostponed = audit_killed_trees();
	int need_prune = 0;
	int n;

	if (chunk-&gt;dead)
		return;

	chunk-&gt;dead = 1;
	mutex_lock(&amp;amp;audit_filter_mutex);
	spin_lock(&amp;amp;hash_lock);
	while (!list_empty(&amp;amp;chunk-&gt;trees)) {
		owner = list_entry(chunk-&gt;trees.next,
				   struct audit_tree, same_root);
		owner-&gt;goner = 1;
		owner-&gt;root = NULL;
		list_del_init(&amp;amp;owner-&gt;same_root);
		spin_unlock(&amp;amp;hash_lock);
		if (!postponed) {
			kill_rules(owner);
			list_move(&amp;amp;owner-&gt;list, &amp;amp;prune_list);
			need_prune = 1;
		} else {
			list_move(&amp;amp;owner-&gt;list, postponed);
		}
		spin_lock(&amp;amp;hash_lock);
	}
	list_del_rcu(&amp;amp;chunk-&gt;hash);
	for (n = 0; n &lt; chunk-&gt;count; n++)
		list_del_init(&amp;amp;chunk-&gt;owners[n].list);
	spin_unlock(&amp;amp;hash_lock);
	mutex_unlock(&amp;amp;audit_filter_mutex);
	if (need_prune)
		audit_schedule_prune();
}

static int audit_tree_handle_event(struct fsnotify_groupgroup,
				   struct inodeto_tell,
				   struct fsnotify_markinode_mark,
				   struct fsnotify_markvfsmount_mark,
				   u32 mask, voiddata, int data_type,
				   const unsigned charfile_name, u32 cookie)
{
	return 0;
}

static void audit_tree_freeing_mark(struct fsnotify_markentry, struct fsnotify_groupgroup)
{
	struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);

	evict_chunk(chunk);

	*/
	 We are guaranteed to have at least one reference to the mark from
	 either the inode or the caller of fsnotify_destroy_mark().
	 /*
	BUG_ON(atomic_read(&amp;amp;entry-&gt;refcnt) &lt; 1);
}

static const struct fsnotify_ops audit_tree_ops = {
	.handle_event = audit_tree_handle_event,
	.freeing_mark = audit_tree_freeing_mark,
};

static int __init audit_tree_init(void)
{
	int i;

	audit_tree_group = fsnotify_alloc_group(&amp;amp;audit_tree_ops);
	if (IS_ERR(audit_tree_group))
		audit_panic("cannot initialize fsnotify group for rectree watches");

	for (i = 0; i &lt; HASH_SIZE; i++)
		INIT_LIST_HEAD(&amp;amp;chunk_hash_heads[i]);

	return 0;
}
__initcall(audit_tree_init);
*/
 audit_watch.c -- watching inodes

 Copyright 2003-2009 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include &lt;linux/kernel.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/fsnotify_backend.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/netlink.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/security.h&gt;
#include "audit.h"

*/
 Reference counting:

 audit_parent: lifetime is from audit_init_parent() to receipt of an FS_IGNORED
 	event.  Each audit_watch holds a reference to its associated parent.

 audit_watch: if added to lists, lifetime is from audit_init_watch() to
 	audit_remove_watch().  Additionally, an audit_watch may exist
 	temporarily to assist in searching existing filter data.  Each
 	audit_krule holds a reference to its associated watch.
 /*

struct audit_watch {
	atomic_t		count;	*/ reference count /*
	dev_t			dev;	*/ associated superblock device /*
	char			*path;	*/ insertion path /*
	unsigned long		ino;	*/ associated inode number /*
	struct audit_parent	*parent;/ associated parent /*
	struct list_head	wlist;	*/ entry in parent-&gt;watches list /*
	struct list_head	rules;	*/ anchor for krule-&gt;rlist /*
};

struct audit_parent {
	struct list_head	watches;/ anchor for audit_watch-&gt;wlist /*
	struct fsnotify_mark mark;/ fsnotify mark on the inode /*
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_watch_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_WATCH (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
			FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_free_parent(struct audit_parentparent)
{
	WARN_ON(!list_empty(&amp;amp;parent-&gt;watches));
	kfree(parent);
}

static void audit_watch_free_mark(struct fsnotify_markentry)
{
	struct audit_parentparent;

	parent = container_of(entry, struct audit_parent, mark);
	audit_free_parent(parent);
}

static void audit_get_parent(struct audit_parentparent)
{
	if (likely(parent))
		fsnotify_get_mark(&amp;amp;parent-&gt;mark);
}

static void audit_put_parent(struct audit_parentparent)
{
	if (likely(parent))
		fsnotify_put_mark(&amp;amp;parent-&gt;mark);
}

*/
 Find and return the audit_parent on the given inode.  If found a reference
 is taken on this parent.
 /*
static inline struct audit_parentaudit_find_parent(struct inodeinode)
{
	struct audit_parentparent = NULL;
	struct fsnotify_markentry;

	entry = fsnotify_find_inode_mark(audit_watch_group, inode);
	if (entry)
		parent = container_of(entry, struct audit_parent, mark);

	return parent;
}

void audit_get_watch(struct audit_watchwatch)
{
	atomic_inc(&amp;amp;watch-&gt;count);
}

void audit_put_watch(struct audit_watchwatch)
{
	if (atomic_dec_and_test(&amp;amp;watch-&gt;count)) {
		WARN_ON(watch-&gt;parent);
		WARN_ON(!list_empty(&amp;amp;watch-&gt;rules));
		kfree(watch-&gt;path);
		kfree(watch);
	}
}

static void audit_remove_watch(struct audit_watchwatch)
{
	list_del(&amp;amp;watch-&gt;wlist);
	audit_put_parent(watch-&gt;parent);
	watch-&gt;parent = NULL;
	audit_put_watch(watch);/ match initial get /*
}

charaudit_watch_path(struct audit_watchwatch)
{
	return watch-&gt;path;
}

int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev)
{
	return (watch-&gt;ino != AUDIT_INO_UNSET) &amp;amp;&amp;amp;
		(watch-&gt;ino == ino) &amp;amp;&amp;amp;
		(watch-&gt;dev == dev);
}

*/ Initialize a parent watch entry. /*
static struct audit_parentaudit_init_parent(struct pathpath)
{
	struct inodeinode = d_backing_inode(path-&gt;dentry);
	struct audit_parentparent;
	int ret;

	parent = kzalloc(sizeof(*parent), GFP_KERNEL);
	if (unlikely(!parent))
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&amp;amp;parent-&gt;watches);

	fsnotify_init_mark(&amp;amp;parent-&gt;mark, audit_watch_free_mark);
	parent-&gt;mark.mask = AUDIT_FS_WATCH;
	ret = fsnotify_add_mark(&amp;amp;parent-&gt;mark, audit_watch_group, inode, NULL, 0);
	if (ret &lt; 0) {
		audit_free_parent(parent);
		return ERR_PTR(ret);
	}

	return parent;
}

*/ Initialize a watch entry. /*
static struct audit_watchaudit_init_watch(charpath)
{
	struct audit_watchwatch;

	watch = kzalloc(sizeof(*watch), GFP_KERNEL);
	if (unlikely(!watch))
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&amp;amp;watch-&gt;rules);
	atomic_set(&amp;amp;watch-&gt;count, 1);
	watch-&gt;path = path;
	watch-&gt;dev = AUDIT_DEV_UNSET;
	watch-&gt;ino = AUDIT_INO_UNSET;

	return watch;
}

*/ Translate a watch string to kernel representation. /*
int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op)
{
	struct audit_watchwatch;

	if (!audit_watch_group)
		return -EOPNOTSUPP;

	if (path[0] != '/' || path[len-1] == '/' ||
	    krule-&gt;listnr != AUDIT_FILTER_EXIT ||
	    op != Audit_equal ||
	    krule-&gt;inode_f || krule-&gt;watch || krule-&gt;tree)
		return -EINVAL;

	watch = audit_init_watch(path);
	if (IS_ERR(watch))
		return PTR_ERR(watch);

	krule-&gt;watch = watch;

	return 0;
}

*/ Duplicate the given audit watch.  The new watch's rules list is initialized
 to an empty list and wlist is undefined. /*
static struct audit_watchaudit_dupe_watch(struct audit_watchold)
{
	charpath;
	struct audit_watchnew;

	path = kstrdup(old-&gt;path, GFP_KERNEL);
	if (unlikely(!path))
		return ERR_PTR(-ENOMEM);

	new = audit_init_watch(path);
	if (IS_ERR(new)) {
		kfree(path);
		goto out;
	}

	new-&gt;dev = old-&gt;dev;
	new-&gt;ino = old-&gt;ino;
	audit_get_parent(old-&gt;parent);
	new-&gt;parent = old-&gt;parent;

out:
	return new;
}

static void audit_watch_log_rule_change(struct audit_kruler, struct audit_watchw, charop)
{
	if (audit_enabled) {
		struct audit_bufferab;
		ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
		if (unlikely(!ab))
			return;
		audit_log_format(ab, "auid=%u ses=%u op=",
				 from_kuid(&amp;amp;init_user_ns, audit_get_loginuid(current)),
				 audit_get_sessionid(current));
		audit_log_string(ab, op);
		audit_log_format(ab, " path=");
		audit_log_untrustedstring(ab, w-&gt;path);
		audit_log_key(ab, r-&gt;filterkey);
		audit_log_format(ab, " list=%d res=1", r-&gt;listnr);
		audit_log_end(ab);
	}
}

*/ Update inode info in audit rules based on filesystem event. /*
static void audit_update_watch(struct audit_parentparent,
			       const chardname, dev_t dev,
			       unsigned long ino, unsigned invalidating)
{
	struct audit_watchowatch,nwatch,nextw;
	struct audit_kruler,nextr;
	struct audit_entryoentry,nentry;

	mutex_lock(&amp;amp;audit_filter_mutex);
	*/ Run all of the watches on this parent looking for the one that
	 matches the given dname /*
	list_for_each_entry_safe(owatch, nextw, &amp;amp;parent-&gt;watches, wlist) {
		if (audit_compare_dname_path(dname, owatch-&gt;path,
					     AUDIT_NAME_FULL))
			continue;

		*/ If the update involves invalidating rules, do the inode-based
		 filtering now, so we don't omit records. /*
		if (invalidating &amp;amp;&amp;amp; !audit_dummy_context())
			audit_filter_inodes(current, current-&gt;audit_context);

		*/ updating ino will likely change which audit_hash_list we
		 are on so we need a new watch for the new list /*
		nwatch = audit_dupe_watch(owatch);
		if (IS_ERR(nwatch)) {
			mutex_unlock(&amp;amp;audit_filter_mutex);
			audit_panic("error updating watch, skipping");
			return;
		}
		nwatch-&gt;dev = dev;
		nwatch-&gt;ino = ino;

		list_for_each_entry_safe(r, nextr, &amp;amp;owatch-&gt;rules, rlist) {

			oentry = container_of(r, struct audit_entry, rule);
			list_del(&amp;amp;oentry-&gt;rule.rlist);
			list_del_rcu(&amp;amp;oentry-&gt;list);

			nentry = audit_dupe_rule(&amp;amp;oentry-&gt;rule);
			if (IS_ERR(nentry)) {
				list_del(&amp;amp;oentry-&gt;rule.list);
				audit_panic("error updating watch, removing");
			} else {
				int h = audit_hash_ino((u32)ino);

				*/
				 nentry-&gt;rule.watch == oentry-&gt;rule.watch so
				 we must drop that reference and set it to our
				 new watch.
				 /*
				audit_put_watch(nentry-&gt;rule.watch);
				audit_get_watch(nwatch);
				nentry-&gt;rule.watch = nwatch;
				list_add(&amp;amp;nentry-&gt;rule.rlist, &amp;amp;nwatch-&gt;rules);
				list_add_rcu(&amp;amp;nentry-&gt;list, &amp;amp;audit_inode_hash[h]);
				list_replace(&amp;amp;oentry-&gt;rule.list,
					     &amp;amp;nentry-&gt;rule.list);
			}
			if (oentry-&gt;rule.exe)
				audit_remove_mark(oentry-&gt;rule.exe);

			audit_watch_log_rule_change(r, owatch, "updated_rules");

			call_rcu(&amp;amp;oentry-&gt;rcu, audit_free_rule_rcu);
		}

		audit_remove_watch(owatch);
		goto add_watch_to_parent;/ event applies to a single watch /*
	}
	mutex_unlock(&amp;amp;audit_filter_mutex);
	return;

add_watch_to_parent:
	list_add(&amp;amp;nwatch-&gt;wlist, &amp;amp;parent-&gt;watches);
	mutex_unlock(&amp;amp;audit_filter_mutex);
	return;
}

*/ Remove all watches &amp;amp; rules associated with a parent that is going away. /*
static void audit_remove_parent_watches(struct audit_parentparent)
{
	struct audit_watchw,nextw;
	struct audit_kruler,nextr;
	struct audit_entrye;

	mutex_lock(&amp;amp;audit_filter_mutex);
	list_for_each_entry_safe(w, nextw, &amp;amp;parent-&gt;watches, wlist) {
		list_for_each_entry_safe(r, nextr, &amp;amp;w-&gt;rules, rlist) {
			e = container_of(r, struct audit_entry, rule);
			audit_watch_log_rule_change(r, w, "remove_rule");
			if (e-&gt;rule.exe)
				audit_remove_mark(e-&gt;rule.exe);
			list_del(&amp;amp;r-&gt;rlist);
			list_del(&amp;amp;r-&gt;list);
			list_del_rcu(&amp;amp;e-&gt;list);
			call_rcu(&amp;amp;e-&gt;rcu, audit_free_rule_rcu);
		}
		audit_remove_watch(w);
	}
	mutex_unlock(&amp;amp;audit_filter_mutex);

	fsnotify_destroy_mark(&amp;amp;parent-&gt;mark, audit_watch_group);
}

*/ Get path information necessary for adding watches. /*
static int audit_get_nd(struct audit_watchwatch, struct pathparent)
{
	struct dentryd = kern_path_locked(watch-&gt;path, parent);
	if (IS_ERR(d))
		return PTR_ERR(d);
	inode_unlock(d_backing_inode(parent-&gt;dentry));
	if (d_is_positive(d)) {
		*/ update watch filter fields /*
		watch-&gt;dev = d_backing_inode(d)-&gt;i_sb-&gt;s_dev;
		watch-&gt;ino = d_backing_inode(d)-&gt;i_ino;
	}
	dput(d);
	return 0;
}

*/ Associate the given rule with an existing parent.
 Caller must hold audit_filter_mutex. /*
static void audit_add_to_parent(struct audit_krulekrule,
				struct audit_parentparent)
{
	struct audit_watchw,watch = krule-&gt;watch;
	int watch_found = 0;

	BUG_ON(!mutex_is_locked(&amp;amp;audit_filter_mutex));

	list_for_each_entry(w, &amp;amp;parent-&gt;watches, wlist) {
		if (strcmp(watch-&gt;path, w-&gt;path))
			continue;

		watch_found = 1;

		*/ put krule's ref to temporary watch /*
		audit_put_watch(watch);

		audit_get_watch(w);
		krule-&gt;watch = watch = w;

		audit_put_parent(parent);
		break;
	}

	if (!watch_found) {
		watch-&gt;parent = parent;

		audit_get_watch(watch);
		list_add(&amp;amp;watch-&gt;wlist, &amp;amp;parent-&gt;watches);
	}
	list_add(&amp;amp;krule-&gt;rlist, &amp;amp;watch-&gt;rules);
}

*/ Find a matching watch entry, or add this one.
 Caller must hold audit_filter_mutex. /*
int audit_add_watch(struct audit_krulekrule, struct list_head*list)
{
	struct audit_watchwatch = krule-&gt;watch;
	struct audit_parentparent;
	struct path parent_path;
	int h, ret = 0;

	mutex_unlock(&amp;amp;audit_filter_mutex);

	*/ Avoid calling path_lookup under audit_filter_mutex. /*
	ret = audit_get_nd(watch, &amp;amp;parent_path);

	*/ caller expects mutex locked /*
	mutex_lock(&amp;amp;audit_filter_mutex);

	if (ret)
		return ret;

	*/ either find an old parent or attach a new one /*
	parent = audit_find_parent(d_backing_inode(parent_path.dentry));
	if (!parent) {
		parent = audit_init_parent(&amp;amp;parent_path);
		if (IS_ERR(parent)) {
			ret = PTR_ERR(parent);
			goto error;
		}
	}

	audit_add_to_parent(krule, parent);

	h = audit_hash_ino((u32)watch-&gt;ino);
	*list = &amp;amp;audit_inode_hash[h];
error:
	path_put(&amp;amp;parent_path);
	return ret;
}

void audit_remove_watch_rule(struct audit_krulekrule)
{
	struct audit_watchwatch = krule-&gt;watch;
	struct audit_parentparent = watch-&gt;parent;

	list_del(&amp;amp;krule-&gt;rlist);

	if (list_empty(&amp;amp;watch-&gt;rules)) {
		audit_remove_watch(watch);

		if (list_empty(&amp;amp;parent-&gt;watches)) {
			audit_get_parent(parent);
			fsnotify_destroy_mark(&amp;amp;parent-&gt;mark, audit_watch_group);
			audit_put_parent(parent);
		}
	}
}

*/ Update watch data in audit rules based on fsnotify events. /*
static int audit_watch_handle_event(struct fsnotify_groupgroup,
				    struct inodeto_tell,
				    struct fsnotify_markinode_mark,
				    struct fsnotify_markvfsmount_mark,
				    u32 mask, voiddata, int data_type,
				    const unsigned chardname, u32 cookie)
{
	struct inodeinode;
	struct audit_parentparent;

	parent = container_of(inode_mark, struct audit_parent, mark);

	BUG_ON(group != audit_watch_group);

	switch (data_type) {
	case (FSNOTIFY_EVENT_PATH):
		inode = d_backing_inode(((struct path)data)-&gt;dentry);
		break;
	case (FSNOTIFY_EVENT_INODE):
		inode = (struct inode)data;
		break;
	default:
		BUG();
		inode = NULL;
		break;
	};

	if (mask &amp;amp; (FS_CREATE|FS_MOVED_TO) &amp;amp;&amp;amp; inode)
		audit_update_watch(parent, dname, inode-&gt;i_sb-&gt;s_dev, inode-&gt;i_ino, 0);
	else if (mask &amp;amp; (FS_DELETE|FS_MOVED_FROM))
		audit_update_watch(parent, dname, AUDIT_DEV_UNSET, AUDIT_INO_UNSET, 1);
	else if (mask &amp;amp; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
		audit_remove_parent_watches(parent);

	return 0;
}

static const struct fsnotify_ops audit_watch_fsnotify_ops = {
	.handle_event = 	audit_watch_handle_event,
};

static int __init audit_watch_init(void)
{
	audit_watch_group = fsnotify_alloc_group(&amp;amp;audit_watch_fsnotify_ops);
	if (IS_ERR(audit_watch_group)) {
		audit_watch_group = NULL;
		audit_panic("cannot create audit fsnotify group");
	}
	return 0;
}
device_initcall(audit_watch_init);

int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold)
{
	struct audit_fsnotify_markaudit_mark;
	charpathname;

	pathname = kstrdup(audit_mark_path(old-&gt;exe), GFP_KERNEL);
	if (!pathname)
		return -ENOMEM;

	audit_mark = audit_alloc_mark(new, pathname, strlen(pathname));
	if (IS_ERR(audit_mark)) {
		kfree(pathname);
		return PTR_ERR(audit_mark);
	}
	new-&gt;exe = audit_mark;

	return 0;
}

int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark)
{
	struct fileexe_file;
	unsigned long ino;
	dev_t dev;

	rcu_read_lock();
	exe_file = rcu_dereference(tsk-&gt;mm-&gt;exe_file);
	ino = exe_file-&gt;f_inode-&gt;i_ino;
	dev = exe_file-&gt;f_inode-&gt;i_sb-&gt;s_dev;
	rcu_read_unlock();
	return audit_mark_compare(mark, ino, dev);
}
*/

 Simple stack backtrace regression test module

 (C) Copyright 2008 Intel Corporation
 Author: Arjan van de Ven &lt;arjan@linux.intel.com&gt;

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*

#include &lt;linux/completion.h&gt;
#include &lt;linux/delay.h&gt;
#include &lt;linux/interrupt.h&gt;
#include &lt;linux/module.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/stacktrace.h&gt;

static void backtrace_test_normal(void)
{
	pr_info("Testing a backtrace from process context.\n");
	pr_info("The following trace is a kernel self test and not a bug!\n");

	dump_stack();
}

static DECLARE_COMPLETION(backtrace_work);

static void backtrace_test_irq_callback(unsigned long data)
{
	dump_stack();
	complete(&amp;amp;backtrace_work);
}

static DECLARE_TASKLET(backtrace_tasklet, &amp;amp;backtrace_test_irq_callback, 0);

static void backtrace_test_irq(void)
{
	pr_info("Testing a backtrace from irq context.\n");
	pr_info("The following trace is a kernel self test and not a bug!\n");

	init_completion(&amp;amp;backtrace_work);
	tasklet_schedule(&amp;amp;backtrace_tasklet);
	wait_for_completion(&amp;amp;backtrace_work);
}

#ifdef CONFIG_STACKTRACE
static void backtrace_test_saved(void)
{
	struct stack_trace trace;
	unsigned long entries[8];

	pr_info("Testing a saved backtrace.\n");
	pr_info("The following trace is a kernel self test and not a bug!\n");

	trace.nr_entries = 0;
	trace.max_entries = ARRAY_SIZE(entries);
	trace.entries = entries;
	trace.skip = 0;

	save_stack_trace(&amp;amp;trace);
	print_stack_trace(&amp;amp;trace, 0);
}
#else
static void backtrace_test_saved(void)
{
	pr_info("Saved backtrace test skipped.\n");
}
#endif

static int backtrace_regression_test(void)
{
	pr_info("====[ backtrace testing ]===========\n");

	backtrace_test_normal();
	backtrace_test_irq();
	backtrace_test_saved();

	pr_info("====[ end of backtrace testing ]====\n");
	return 0;
}

static void exitf(void)
{
}

module_init(backtrace_regression_test);
module_exit(exitf);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Arjan van de Ven &lt;arjan@linux.intel.com&gt;");
*/
