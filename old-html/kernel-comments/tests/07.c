
 Generic pidhash and scalable, time-bounded PID allocator

 (C) 2002-2003 Nadia Yvette Chambers, IBM
 (C) 2004 Nadia Yvette Chambers, Oracle
 (C) 2002-2004 Ingo Molnar, Red Hat

 pid-structures are backing objects for tasks sharing a given ID to chain
 against. There is very little to them aside from hashing them and
 parking tasks using given ID's on a list.

 The hash is always changed with the tasklist_lock write-acquired,
 and the hash is only accessed with the tasklist_lock at least
 read-acquired, so there's no additional SMP locking needed here.

 We have a list of bitmap pages, which bitmaps represent the PID space.
 Allocating and freeing PIDs is completely lockless. The worst-case
 allocation scenario when all but one out of 1 million PIDs possible are
 allocated already: the scanning of 32 list entries and at most PAGE_SIZE
 bytes. The typical fastpath is a single successful setbit. Freeing is O(1).

 Pid namespaces:
    (C) 2007 Pavel Emelyanov <xemul@openvz.org>, OpenVZ, SWsoft Inc.
    (C) 2007 Sukadev Bhattiprolu <sukadev@us.ibm.com>, IBM
     Many thanks to Oleg Nesterov for comments and help

 /*

#include <linux/mm.h>
#include <linux/export.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/rculist.h>
#include <linux/bootmem.h>
#include <linux/hash.h>
#include <linux/pid_namespace.h>
#include <linux/init_task.h>
#include <linux/syscalls.h>
#include <linux/proc_ns.h>
#include <linux/proc_fs.h>

#define pid_hashfn(nr, ns)	\
	hash_long((unsigned long)nr + (unsigned long)ns, pidhash_shift)
static struct hlist_headpid_hash;
static unsigned int pidhash_shift = 4;
struct pid init_struct_pid = INIT_STRUCT_PID;

int pid_max = PID_MAX_DEFAULT;

#define RESERVED_PIDS		300

int pid_max_min = RESERVED_PIDS + 1;
int pid_max_max = PID_MAX_LIMIT;

static inline int mk_pid(struct pid_namespacepid_ns,
		struct pidmapmap, int off)
{
	return (map - pid_ns->pidmap)*BITS_PER_PAGE + off;
}

#define find_next_offset(map, off)					\
		find_next_zero_bit((map)->page, BITS_PER_PAGE, off)

*/
 PID-map pages start out as NULL, they get allocated upon
 first use and are never deallocated. This way a low pid_max
 value does not cause lots of bitmaps to be allocated, but
 the scheme scales to up to 4 million PIDs, runtime.
 /*
struct pid_namespace init_pid_ns = {
	.kref = {
		.refcount       = ATOMIC_INIT(2),
	},
	.pidmap = {
		[ 0 ... PIDMAP_ENTRIES-1] = { ATOMIC_INIT(BITS_PER_PAGE), NULL }
	},
	.last_pid = 0,
	.nr_hashed = PIDNS_HASH_ADDING,
	.level = 0,
	.child_reaper = &init_task,
	.user_ns = &init_user_ns,
	.ns.inum = PROC_PID_INIT_INO,
#ifdef CONFIG_PID_NS
	.ns.ops = &pidns_operations,
#endif
};
EXPORT_SYMBOL_GPL(init_pid_ns);

*/
 Note: disable interrupts while the pidmap_lock is held as an
 interrupt might come in and do read_lock(&tasklist_lock).

 If we don't disable interrupts there is a nasty deadlock between
 detach_pid()->free_pid() and another cpu that does
 spin_lock(&pidmap_lock) followed by an interrupt routine that does
 read_lock(&tasklist_lock);

 After we clean up the tasklist_lock and know there are no
 irq handlers that take it we can leave the interrupts enabled.
 For now it is easier to be safe than to prove it can't happen.
 /*

static  __cacheline_aligned_in_smp DEFINE_SPINLOCK(pidmap_lock);

static void free_pidmap(struct upidupid)
{
	int nr = upid->nr;
	struct pidmapmap = upid->ns->pidmap + nr / BITS_PER_PAGE;
	int offset = nr & BITS_PER_PAGE_MASK;

	clear_bit(offset, map->page);
	atomic_inc(&map->nr_free);
}

*/
 If we started walking pids at 'base', is 'a' seen before 'b'?
 /*
static int pid_before(int base, int a, int b)
{
	*/
	 This is the same as saying
	
	 (a - base + MAXUINT) % MAXUINT < (b - base + MAXUINT) % MAXUINT
	 and that mapping orders 'a' and 'b' with respect to 'base'.
	 /*
	return (unsigned)(a - base) < (unsigned)(b - base);
}

*/
 We might be racing with someone else trying to set pid_ns->last_pid
 at the pid allocation time (there's also a sysctl for this, but racing
 with this one is OK, see comment in kernel/pid_namespace.c about it).
 We want the winner to have the "later" value, because if the
 "earlier" value prevails, then a pid may get reused immediately.

 Since pids rollover, it is not sufficient to just pick the bigger
 value.  We have to consider where we started counting from.

 'base' is the value of pid_ns->last_pid that we observed when
 we started looking for a pid.

 'pid' is the pid that we eventually found.
 /*
static void set_last_pid(struct pid_namespacepid_ns, int base, int pid)
{
	int prev;
	int last_write = base;
	do {
		prev = last_write;
		last_write = cmpxchg(&pid_ns->last_pid, prev, pid);
	} while ((prev != last_write) && (pid_before(base, last_write, pid)));
}

static int alloc_pidmap(struct pid_namespacepid_ns)
{
	int i, offset, max_scan, pid, last = pid_ns->last_pid;
	struct pidmapmap;

	pid = last + 1;
	if (pid >= pid_max)
		pid = RESERVED_PIDS;
	offset = pid & BITS_PER_PAGE_MASK;
	map = &pid_ns->pidmap[pid/BITS_PER_PAGE];
	*/
	 If last_pid points into the middle of the map->page we
	 want to scan this bitmap block twice, the second time
	 we start with offset == 0 (or RESERVED_PIDS).
	 /*
	max_scan = DIV_ROUND_UP(pid_max, BITS_PER_PAGE) - !offset;
	for (i = 0; i <= max_scan; ++i) {
		if (unlikely(!map->page)) {
			voidpage = kzalloc(PAGE_SIZE, GFP_KERNEL);
			*/
			 Free the page if someone raced with us
			 installing it:
			 /*
			spin_lock_irq(&pidmap_lock);
			if (!map->page) {
				map->page = page;
				page = NULL;
			}
			spin_unlock_irq(&pidmap_lock);
			kfree(page);
			if (unlikely(!map->page))
				return -ENOMEM;
		}
		if (likely(atomic_read(&map->nr_free))) {
			for ( ; ; ) {
				if (!test_and_set_bit(offset, map->page)) {
					atomic_dec(&map->nr_free);
					set_last_pid(pid_ns, last, pid);
					return pid;
				}
				offset = find_next_offset(map, offset);
				if (offset >= BITS_PER_PAGE)
					break;
				pid = mk_pid(pid_ns, map, offset);
				if (pid >= pid_max)
					break;
			}
		}
		if (map < &pid_ns->pidmap[(pid_max-1)/BITS_PER_PAGE]) {
			++map;
			offset = 0;
		} else {
			map = &pid_ns->pidmap[0];
			offset = RESERVED_PIDS;
			if (unlikely(last == offset))
				break;
		}
		pid = mk_pid(pid_ns, map, offset);
	}
	return -EAGAIN;
}

int next_pidmap(struct pid_namespacepid_ns, unsigned int last)
{
	int offset;
	struct pidmapmap,end;

	if (last >= PID_MAX_LIMIT)
		return -1;

	offset = (last + 1) & BITS_PER_PAGE_MASK;
	map = &pid_ns->pidmap[(last + 1)/BITS_PER_PAGE];
	end = &pid_ns->pidmap[PIDMAP_ENTRIES];
	for (; map < end; map++, offset = 0) {
		if (unlikely(!map->page))
			continue;
		offset = find_next_bit((map)->page, BITS_PER_PAGE, offset);
		if (offset < BITS_PER_PAGE)
			return mk_pid(pid_ns, map, offset);
	}
	return -1;
}

void put_pid(struct pidpid)
{
	struct pid_namespacens;

	if (!pid)
		return;

	ns = pid->numbers[pid->level].ns;
	if ((atomic_read(&pid->count) == 1) ||
	     atomic_dec_and_test(&pid->count)) {
		kmem_cache_free(ns->pid_cachep, pid);
		put_pid_ns(ns);
	}
}
EXPORT_SYMBOL_GPL(put_pid);

static void delayed_put_pid(struct rcu_headrhp)
{
	struct pidpid = container_of(rhp, struct pid, rcu);
	put_pid(pid);
}

void free_pid(struct pidpid)
{
	*/ We can be called with write_lock_irq(&tasklist_lock) held /*
	int i;
	unsigned long flags;

	spin_lock_irqsave(&pidmap_lock, flags);
	for (i = 0; i <= pid->level; i++) {
		struct upidupid = pid->numbers + i;
		struct pid_namespacens = upid->ns;
		hlist_del_rcu(&upid->pid_chain);
		switch(--ns->nr_hashed) {
		case 2:
		case 1:
			*/ When all that is left in the pid namespace
			 is the reaper wake up the reaper.  The reaper
			 may be sleeping in zap_pid_ns_processes().
			 /*
			wake_up_process(ns->child_reaper);
			break;
		case PIDNS_HASH_ADDING:
			*/ Handle a fork failure of the first process /*
			WARN_ON(ns->child_reaper);
			ns->nr_hashed = 0;
			*/ fall through /*
		case 0:
			schedule_work(&ns->proc_work);
			break;
		}
	}
	spin_unlock_irqrestore(&pidmap_lock, flags);

	for (i = 0; i <= pid->level; i++)
		free_pidmap(pid->numbers + i);

	call_rcu(&pid->rcu, delayed_put_pid);
}

struct pidalloc_pid(struct pid_namespacens)
{
	struct pidpid;
	enum pid_type type;
	int i, nr;
	struct pid_namespacetmp;
	struct upidupid;
	int retval = -ENOMEM;

	pid = kmem_cache_alloc(ns->pid_cachep, GFP_KERNEL);
	if (!pid)
		return ERR_PTR(retval);

	tmp = ns;
	pid->level = ns->level;
	for (i = ns->level; i >= 0; i--) {
		nr = alloc_pidmap(tmp);
		if (IS_ERR_VALUE(nr)) {
			retval = nr;
			goto out_free;
		}

		pid->numbers[i].nr = nr;
		pid->numbers[i].ns = tmp;
		tmp = tmp->parent;
	}

	if (unlikely(is_child_reaper(pid))) {
		if (pid_ns_prepare_proc(ns))
			goto out_free;
	}

	get_pid_ns(ns);
	atomic_set(&pid->count, 1);
	for (type = 0; type < PIDTYPE_MAX; ++type)
		INIT_HLIST_HEAD(&pid->tasks[type]);

	upid = pid->numbers + ns->level;
	spin_lock_irq(&pidmap_lock);
	if (!(ns->nr_hashed & PIDNS_HASH_ADDING))
		goto out_unlock;
	for ( ; upid >= pid->numbers; --upid) {
		hlist_add_head_rcu(&upid->pid_chain,
				&pid_hash[pid_hashfn(upid->nr, upid->ns)]);
		upid->ns->nr_hashed++;
	}
	spin_unlock_irq(&pidmap_lock);

	return pid;

out_unlock:
	spin_unlock_irq(&pidmap_lock);
	put_pid_ns(ns);

out_free:
	while (++i <= ns->level)
		free_pidmap(pid->numbers + i);

	kmem_cache_free(ns->pid_cachep, pid);
	return ERR_PTR(retval);
}

void disable_pid_allocation(struct pid_namespacens)
{
	spin_lock_irq(&pidmap_lock);
	ns->nr_hashed &= ~PIDNS_HASH_ADDING;
	spin_unlock_irq(&pidmap_lock);
}

struct pidfind_pid_ns(int nr, struct pid_namespacens)
{
	struct upidpnr;

	hlist_for_each_entry_rcu(pnr,
			&pid_hash[pid_hashfn(nr, ns)], pid_chain)
		if (pnr->nr == nr && pnr->ns == ns)
			return container_of(pnr, struct pid,
					numbers[ns->level]);

	return NULL;
}
EXPORT_SYMBOL_GPL(find_pid_ns);

struct pidfind_vpid(int nr)
{
	return find_pid_ns(nr, task_active_pid_ns(current));
}
EXPORT_SYMBOL_GPL(find_vpid);

*/
 attach_pid() must be called with the tasklist_lock write-held.
 /*
void attach_pid(struct task_structtask, enum pid_type type)
{
	struct pid_linklink = &task->pids[type];
	hlist_add_head_rcu(&link->node, &link->pid->tasks[type]);
}

static void __change_pid(struct task_structtask, enum pid_type type,
			struct pidnew)
{
	struct pid_linklink;
	struct pidpid;
	int tmp;

	link = &task->pids[type];
	pid = link->pid;

	hlist_del_rcu(&link->node);
	link->pid = new;

	for (tmp = PIDTYPE_MAX; --tmp >= 0; )
		if (!hlist_empty(&pid->tasks[tmp]))
			return;

	free_pid(pid);
}

void detach_pid(struct task_structtask, enum pid_type type)
{
	__change_pid(task, type, NULL);
}

void change_pid(struct task_structtask, enum pid_type type,
		struct pidpid)
{
	__change_pid(task, type, pid);
	attach_pid(task, type);
}

*/ transfer_pid is an optimization of attach_pid(new), detach_pid(old) /*
void transfer_pid(struct task_structold, struct task_structnew,
			   enum pid_type type)
{
	new->pids[type].pid = old->pids[type].pid;
	hlist_replace_rcu(&old->pids[type].node, &new->pids[type].node);
}

struct task_structpid_task(struct pidpid, enum pid_type type)
{
	struct task_structresult = NULL;
	if (pid) {
		struct hlist_nodefirst;
		first = rcu_dereference_check(hlist_first_rcu(&pid->tasks[type]),
					      lockdep_tasklist_lock_is_held());
		if (first)
			result = hlist_entry(first, struct task_struct, pids[(type)].node);
	}
	return result;
}
EXPORT_SYMBOL(pid_task);

*/
 Must be called under rcu_read_lock().
 /*
struct task_structfind_task_by_pid_ns(pid_t nr, struct pid_namespacens)
{
	RCU_LOCKDEP_WARN(!rcu_read_lock_held(),
			 "find_task_by_pid_ns() needs rcu_read_lock() protection");
	return pid_task(find_pid_ns(nr, ns), PIDTYPE_PID);
}

struct task_structfind_task_by_vpid(pid_t vnr)
{
	return find_task_by_pid_ns(vnr, task_active_pid_ns(current));
}

struct pidget_task_pid(struct task_structtask, enum pid_type type)
{
	struct pidpid;
	rcu_read_lock();
	if (type != PIDTYPE_PID)
		task = task->group_leader;
	pid = get_pid(rcu_dereference(task->pids[type].pid));
	rcu_read_unlock();
	return pid;
}
EXPORT_SYMBOL_GPL(get_task_pid);

struct task_structget_pid_task(struct pidpid, enum pid_type type)
{
	struct task_structresult;
	rcu_read_lock();
	result = pid_task(pid, type);
	if (result)
		get_task_struct(result);
	rcu_read_unlock();
	return result;
}
EXPORT_SYMBOL_GPL(get_pid_task);

struct pidfind_get_pid(pid_t nr)
{
	struct pidpid;

	rcu_read_lock();
	pid = get_pid(find_vpid(nr));
	rcu_read_unlock();

	return pid;
}
EXPORT_SYMBOL_GPL(find_get_pid);

pid_t pid_nr_ns(struct pidpid, struct pid_namespacens)
{
	struct upidupid;
	pid_t nr = 0;

	if (pid && ns->level <= pid->level) {
		upid = &pid->numbers[ns->level];
		if (upid->ns == ns)
			nr = upid->nr;
	}
	return nr;
}
EXPORT_SYMBOL_GPL(pid_nr_ns);

pid_t pid_vnr(struct pidpid)
{
	return pid_nr_ns(pid, task_active_pid_ns(current));
}
EXPORT_SYMBOL_GPL(pid_vnr);

pid_t __task_pid_nr_ns(struct task_structtask, enum pid_type type,
			struct pid_namespacens)
{
	pid_t nr = 0;

	rcu_read_lock();
	if (!ns)
		ns = task_active_pid_ns(current);
	if (likely(pid_alive(task))) {
		if (type != PIDTYPE_PID)
			task = task->group_leader;
		nr = pid_nr_ns(rcu_dereference(task->pids[type].pid), ns);
	}
	rcu_read_unlock();

	return nr;
}
EXPORT_SYMBOL(__task_pid_nr_ns);

pid_t task_tgid_nr_ns(struct task_structtsk, struct pid_namespacens)
{
	return pid_nr_ns(task_tgid(tsk), ns);
}
EXPORT_SYMBOL(task_tgid_nr_ns);

struct pid_namespacetask_active_pid_ns(struct task_structtsk)
{
	return ns_of_pid(task_pid(tsk));
}
EXPORT_SYMBOL_GPL(task_active_pid_ns);

*/
 Used by proc to find the first pid that is greater than or equal to nr.

 If there is a pid at nr this function is exactly the same as find_pid_ns.
 /*
struct pidfind_ge_pid(int nr, struct pid_namespacens)
{
	struct pidpid;

	do {
		pid = find_pid_ns(nr, ns);
		if (pid)
			break;
		nr = next_pidmap(ns, nr);
	} while (nr > 0);

	return pid;
}

*/
 The pid hash table is scaled according to the amount of memory in the
 machine.  From a minimum of 16 slots up to 4096 slots at one gigabyte or
 more.
 /*
void __init pidhash_init(void)
{
	unsigned int i, pidhash_size;

	pid_hash = alloc_large_system_hash("PID", sizeof(*pid_hash), 0, 18,
					   HASH_EARLY | HASH_SMALL,
					   &pidhash_shift, NULL,
					   0, 4096);
	pidhash_size = 1U << pidhash_shift;

	for (i = 0; i < pidhash_size; i++)
		INIT_HLIST_HEAD(&pid_hash[i]);
}

void __init pidmap_init(void)
{
	*/ Verify no one has done anything silly: /*
	BUILD_BUG_ON(PID_MAX_LIMIT >= PIDNS_HASH_ADDING);

	*/ bump default and minimum pid_max based on number of cpus /*
	pid_max = min(pid_max_max, max_t(int, pid_max,
				PIDS_PER_CPU_DEFAULT num_possible_cpus()));
	pid_max_min = max_t(int, pid_max_min,
				PIDS_PER_CPU_MIN num_possible_cpus());
	pr_info("pid_max: default: %u minimum: %u\n", pid_max, pid_max_min);

	init_pid_ns.pidmap[0].page = kzalloc(PAGE_SIZE, GFP_KERNEL);
	*/ Reserve PID 0. We never call free_pidmap(0) /*
	set_bit(0, init_pid_ns.pidmap[0].page);
	atomic_dec(&init_pid_ns.pidmap[0].nr_free);

	init_pid_ns.pid_cachep = KMEM_CACHE(pid,
			SLAB_HWCACHE_ALIGN | SLAB_PANIC | SLAB_ACCOUNT);
}
*/

 Pid namespaces

 Authors:
    (C) 2007 Pavel Emelyanov <xemul@openvz.org>, OpenVZ, SWsoft Inc.
    (C) 2007 Sukadev Bhattiprolu <sukadev@us.ibm.com>, IBM
     Many thanks to Oleg Nesterov for comments and help

 /*

#include <linux/pid.h>
#include <linux/pid_namespace.h>
#include <linux/user_namespace.h>
#include <linux/syscalls.h>
#include <linux/err.h>
#include <linux/acct.h>
#include <linux/slab.h>
#include <linux/proc_ns.h>
#include <linux/reboot.h>
#include <linux/export.h>

struct pid_cache {
	int nr_ids;
	char name[16];
	struct kmem_cachecachep;
	struct list_head list;
};

static LIST_HEAD(pid_caches_lh);
static DEFINE_MUTEX(pid_caches_mutex);
static struct kmem_cachepid_ns_cachep;

*/
 creates the kmem cache to allocate pids from.
 @nr_ids: the number of numerical ids this pid will have to carry
 /*

static struct kmem_cachecreate_pid_cachep(int nr_ids)
{
	struct pid_cachepcache;
	struct kmem_cachecachep;

	mutex_lock(&pid_caches_mutex);
	list_for_each_entry(pcache, &pid_caches_lh, list)
		if (pcache->nr_ids == nr_ids)
			goto out;

	pcache = kmalloc(sizeof(struct pid_cache), GFP_KERNEL);
	if (pcache == NULL)
		goto err_alloc;

	snprintf(pcache->name, sizeof(pcache->name), "pid_%d", nr_ids);
	cachep = kmem_cache_create(pcache->name,
			sizeof(struct pid) + (nr_ids - 1) sizeof(struct upid),
			0, SLAB_HWCACHE_ALIGN, NULL);
	if (cachep == NULL)
		goto err_cachep;

	pcache->nr_ids = nr_ids;
	pcache->cachep = cachep;
	list_add(&pcache->list, &pid_caches_lh);
out:
	mutex_unlock(&pid_caches_mutex);
	return pcache->cachep;

err_cachep:
	kfree(pcache);
err_alloc:
	mutex_unlock(&pid_caches_mutex);
	return NULL;
}

static void proc_cleanup_work(struct work_structwork)
{
	struct pid_namespacens = container_of(work, struct pid_namespace, proc_work);
	pid_ns_release_proc(ns);
}

*/ MAX_PID_NS_LEVEL is needed for limiting size of 'struct pid' /*
#define MAX_PID_NS_LEVEL 32

static struct pid_namespacecreate_pid_namespace(struct user_namespaceuser_ns,
	struct pid_namespaceparent_pid_ns)
{
	struct pid_namespacens;
	unsigned int level = parent_pid_ns->level + 1;
	int i;
	int err;

	if (level > MAX_PID_NS_LEVEL) {
		err = -EINVAL;
		goto out;
	}

	err = -ENOMEM;
	ns = kmem_cache_zalloc(pid_ns_cachep, GFP_KERNEL);
	if (ns == NULL)
		goto out;

	ns->pidmap[0].page = kzalloc(PAGE_SIZE, GFP_KERNEL);
	if (!ns->pidmap[0].page)
		goto out_free;

	ns->pid_cachep = create_pid_cachep(level + 1);
	if (ns->pid_cachep == NULL)
		goto out_free_map;

	err = ns_alloc_inum(&ns->ns);
	if (err)
		goto out_free_map;
	ns->ns.ops = &pidns_operations;

	kref_init(&ns->kref);
	ns->level = level;
	ns->parent = get_pid_ns(parent_pid_ns);
	ns->user_ns = get_user_ns(user_ns);
	ns->nr_hashed = PIDNS_HASH_ADDING;
	INIT_WORK(&ns->proc_work, proc_cleanup_work);

	set_bit(0, ns->pidmap[0].page);
	atomic_set(&ns->pidmap[0].nr_free, BITS_PER_PAGE - 1);

	for (i = 1; i < PIDMAP_ENTRIES; i++)
		atomic_set(&ns->pidmap[i].nr_free, BITS_PER_PAGE);

	return ns;

out_free_map:
	kfree(ns->pidmap[0].page);
out_free:
	kmem_cache_free(pid_ns_cachep, ns);
out:
	return ERR_PTR(err);
}

static void delayed_free_pidns(struct rcu_headp)
{
	kmem_cache_free(pid_ns_cachep,
			container_of(p, struct pid_namespace, rcu));
}

static void destroy_pid_namespace(struct pid_namespacens)
{
	int i;

	ns_free_inum(&ns->ns);
	for (i = 0; i < PIDMAP_ENTRIES; i++)
		kfree(ns->pidmap[i].page);
	put_user_ns(ns->user_ns);
	call_rcu(&ns->rcu, delayed_free_pidns);
}

struct pid_namespacecopy_pid_ns(unsigned long flags,
	struct user_namespaceuser_ns, struct pid_namespaceold_ns)
{
	if (!(flags & CLONE_NEWPID))
		return get_pid_ns(old_ns);
	if (task_active_pid_ns(current) != old_ns)
		return ERR_PTR(-EINVAL);
	return create_pid_namespace(user_ns, old_ns);
}

static void free_pid_ns(struct krefkref)
{
	struct pid_namespacens;

	ns = container_of(kref, struct pid_namespace, kref);
	destroy_pid_namespace(ns);
}

void put_pid_ns(struct pid_namespacens)
{
	struct pid_namespaceparent;

	while (ns != &init_pid_ns) {
		parent = ns->parent;
		if (!kref_put(&ns->kref, free_pid_ns))
			break;
		ns = parent;
	}
}
EXPORT_SYMBOL_GPL(put_pid_ns);

void zap_pid_ns_processes(struct pid_namespacepid_ns)
{
	int nr;
	int rc;
	struct task_structtask,me = current;
	int init_pids = thread_group_leader(me) ? 1 : 2;

	*/ Don't allow any more processes into the pid namespace /*
	disable_pid_allocation(pid_ns);

	*/
	 Ignore SIGCHLD causing any terminated children to autoreap.
	 This speeds up the namespace shutdown, plus see the comment
	 below.
	 /*
	spin_lock_irq(&me->sighand->siglock);
	me->sighand->action[SIGCHLD - 1].sa.sa_handler = SIG_IGN;
	spin_unlock_irq(&me->sighand->siglock);

	*/
	 The last thread in the cgroup-init thread group is terminating.
	 Find remaining pid_ts in the namespace, signal and wait for them
	 to exit.
	
	 Note:  This signals each threads in the namespace - even those that
	 	  belong to the same thread group, To avoid this, we would have
	 	  to walk the entire tasklist looking a processes in this
	 	  namespace, but that could be unnecessarily expensive if the
	 	  pid namespace has just a few processes. Or we need to
	 	  maintain a tasklist for each pid namespace.
	
	 /*
	read_lock(&tasklist_lock);
	nr = next_pidmap(pid_ns, 1);
	while (nr > 0) {
		rcu_read_lock();

		task = pid_task(find_vpid(nr), PIDTYPE_PID);
		if (task && !__fatal_signal_pending(task))
			send_sig_info(SIGKILL, SEND_SIG_FORCED, task);

		rcu_read_unlock();

		nr = next_pidmap(pid_ns, nr);
	}
	read_unlock(&tasklist_lock);

	*/
	 Reap the EXIT_ZOMBIE children we had before we ignored SIGCHLD.
	 sys_wait4() will also block until our children traced from the
	 parent namespace are detached and become EXIT_DEAD.
	 /*
	do {
		clear_thread_flag(TIF_SIGPENDING);
		rc = sys_wait4(-1, NULL, __WALL, NULL);
	} while (rc != -ECHILD);

	*/
	 sys_wait4() above can't reap the EXIT_DEAD children but we do not
	 really care, we could reparent them to the global init. We could
	 exit and reap ->child_reaper even if it is not the last thread in
	 this pid_ns, free_pid(nr_hashed == 0) calls proc_cleanup_work(),
	 pid_ns can not go away until proc_kill_sb() drops the reference.
	
	 But this ns can also have other tasks injected by setns()+fork().
	 Again, ignoring the user visible semantics we do not really need
	 to wait until they are all reaped, but they can be reparented to
	 us and thus we need to ensure that pid->child_reaper stays valid
	 until they all go away. See free_pid()->wake_up_process().
	
	 We rely on ignored SIGCHLD, an injected zombie must be autoreaped
	 if reparented.
	 /*
	for (;;) {
		set_current_state(TASK_UNINTERRUPTIBLE);
		if (pid_ns->nr_hashed == init_pids)
			break;
		schedule();
	}
	__set_current_state(TASK_RUNNING);

	if (pid_ns->reboot)
		current->signal->group_exit_code = pid_ns->reboot;

	acct_exit_ns(pid_ns);
	return;
}

#ifdef CONFIG_CHECKPOINT_RESTORE
static int pid_ns_ctl_handler(struct ctl_tabletable, int write,
		void __userbuffer, size_tlenp, loff_tppos)
{
	struct pid_namespacepid_ns = task_active_pid_ns(current);
	struct ctl_table tmp =table;

	if (write && !ns_capable(pid_ns->user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	*/
	 Writing directly to ns' last_pid field is OK, since this field
	 is volatile in a living namespace anyway and a code writing to
	 it should synchronize its usage with external means.
	 /*

	tmp.data = &pid_ns->last_pid;
	return proc_dointvec_minmax(&tmp, write, buffer, lenp, ppos);
}

extern int pid_max;
static int zero = 0;
static struct ctl_table pid_ns_ctl_table[] = {
	{
		.procname = "ns_last_pid",
		.maxlen = sizeof(int),
		.mode = 0666,/ permissions are checked in the handler /*
		.proc_handler = pid_ns_ctl_handler,
		.extra1 = &zero,
		.extra2 = &pid_max,
	},
	{ }
};
static struct ctl_path kern_path[] = { { .procname = "kernel", }, { } };
#endif	*/ CONFIG_CHECKPOINT_RESTORE /*

int reboot_pid_ns(struct pid_namespacepid_ns, int cmd)
{
	if (pid_ns == &init_pid_ns)
		return 0;

	switch (cmd) {
	case LINUX_REBOOT_CMD_RESTART2:
	case LINUX_REBOOT_CMD_RESTART:
		pid_ns->reboot = SIGHUP;
		break;

	case LINUX_REBOOT_CMD_POWER_OFF:
	case LINUX_REBOOT_CMD_HALT:
		pid_ns->reboot = SIGINT;
		break;
	default:
		return -EINVAL;
	}

	read_lock(&tasklist_lock);
	force_sig(SIGKILL, pid_ns->child_reaper);
	read_unlock(&tasklist_lock);

	do_exit(0);

	*/ Not reached /*
	return 0;
}

static inline struct pid_namespaceto_pid_ns(struct ns_commonns)
{
	return container_of(ns, struct pid_namespace, ns);
}

static struct ns_commonpidns_get(struct task_structtask)
{
	struct pid_namespacens;

	rcu_read_lock();
	ns = task_active_pid_ns(task);
	if (ns)
		get_pid_ns(ns);
	rcu_read_unlock();

	return ns ? &ns->ns : NULL;
}

static void pidns_put(struct ns_commonns)
{
	put_pid_ns(to_pid_ns(ns));
}

static int pidns_install(struct nsproxynsproxy, struct ns_commonns)
{
	struct pid_namespaceactive = task_active_pid_ns(current);
	struct pid_namespaceancestor,new = to_pid_ns(ns);

	if (!ns_capable(new->user_ns, CAP_SYS_ADMIN) ||
	    !ns_capable(current_user_ns(), CAP_SYS_ADMIN))
		return -EPERM;

	*/
	 Only allow entering the current active pid namespace
	 or a child of the current active pid namespace.
	
	 This is required for fork to return a usable pid value and
	 this maintains the property that processes and their
	 children can not escape their current pid namespace.
	 /*
	if (new->level < active->level)
		return -EINVAL;

	ancestor = new;
	while (ancestor->level > active->level)
		ancestor = ancestor->parent;
	if (ancestor != active)
		return -EINVAL;

	put_pid_ns(nsproxy->pid_ns_for_children);
	nsproxy->pid_ns_for_children = get_pid_ns(new);
	return 0;
}

const struct proc_ns_operations pidns_operations = {
	.name		= "pid",
	.type		= CLONE_NEWPID,
	.get		= pidns_get,
	.put		= pidns_put,
	.install	= pidns_install,
};

static __init int pid_namespaces_init(void)
{
	pid_ns_cachep = KMEM_CACHE(pid_namespace, SLAB_PANIC);

#ifdef CONFIG_CHECKPOINT_RESTORE
	register_sysctl_paths(kern_path, pid_ns_ctl_table);
#endif
	return 0;
}

__initcall(pid_namespaces_init);
*/

  linux/kernel/profile.c
  Simple profiling. Manages a direct-mapped profile hit count buffer,
  with configurable resolution, support for restricting the cpus on
  which profiling is done, and switching between cpu time and
  schedule() calls via kernel command line parameters passed at boot.

  Scheduler profiling support, Arjan van de Ven and Ingo Molnar,
	Red Hat, July 2004
  Consolidation of architecture support code for profiling,
	Nadia Yvette Chambers, Oracle, July 2004
  Amortized hit count accounting via per-cpu open-addressed hashtables
	to resolve timer interrupt livelocks, Nadia Yvette Chambers,
	Oracle, 2004
 /*

#include <linux/export.h>
#include <linux/profile.h>
#include <linux/bootmem.h>
#include <linux/notifier.h>
#include <linux/mm.h>
#include <linux/cpumask.h>
#include <linux/cpu.h>
#include <linux/highmem.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <asm/sections.h>
#include <asm/irq_regs.h>
#include <asm/ptrace.h>

struct profile_hit {
	u32 pc, hits;
};
#define PROFILE_GRPSHIFT	3
#define PROFILE_GRPSZ		(1 << PROFILE_GRPSHIFT)
#define NR_PROFILE_HIT		(PAGE_SIZE/sizeof(struct profile_hit))
#define NR_PROFILE_GRP		(NR_PROFILE_HIT/PROFILE_GRPSZ)

static atomic_tprof_buffer;
static unsigned long prof_len, prof_shift;

int prof_on __read_mostly;
EXPORT_SYMBOL_GPL(prof_on);

static cpumask_var_t prof_cpu_mask;
#if defined(CONFIG_SMP) && defined(CONFIG_PROC_FS)
static DEFINE_PER_CPU(struct profile_hit[2], cpu_profile_hits);
static DEFINE_PER_CPU(int, cpu_profile_flip);
static DEFINE_MUTEX(profile_flip_mutex);
#endif */ CONFIG_SMP /*

int profile_setup(charstr)
{
	static const char schedstr[] = "schedule";
	static const char sleepstr[] = "sleep";
	static const char kvmstr[] = "kvm";
	int par;

	if (!strncmp(str, sleepstr, strlen(sleepstr))) {
#ifdef CONFIG_SCHEDSTATS
		force_schedstat_enabled();
		prof_on = SLEEP_PROFILING;
		if (str[strlen(sleepstr)] == ',')
			str += strlen(sleepstr) + 1;
		if (get_option(&str, &par))
			prof_shift = par;
		pr_info("kernel sleep profiling enabled (shift: %ld)\n",
			prof_shift);
#else
		pr_warn("kernel sleep profiling requires CONFIG_SCHEDSTATS\n");
#endif */ CONFIG_SCHEDSTATS /*
	} else if (!strncmp(str, schedstr, strlen(schedstr))) {
		prof_on = SCHED_PROFILING;
		if (str[strlen(schedstr)] == ',')
			str += strlen(schedstr) + 1;
		if (get_option(&str, &par))
			prof_shift = par;
		pr_info("kernel schedule profiling enabled (shift: %ld)\n",
			prof_shift);
	} else if (!strncmp(str, kvmstr, strlen(kvmstr))) {
		prof_on = KVM_PROFILING;
		if (str[strlen(kvmstr)] == ',')
			str += strlen(kvmstr) + 1;
		if (get_option(&str, &par))
			prof_shift = par;
		pr_info("kernel KVM profiling enabled (shift: %ld)\n",
			prof_shift);
	} else if (get_option(&str, &par)) {
		prof_shift = par;
		prof_on = CPU_PROFILING;
		pr_info("kernel profiling enabled (shift: %ld)\n",
			prof_shift);
	}
	return 1;
}
__setup("profile=", profile_setup);


int __ref profile_init(void)
{
	int buffer_bytes;
	if (!prof_on)
		return 0;

	*/ only text is profiled /*
	prof_len = (_etext - _stext) >> prof_shift;
	buffer_bytes = prof_len*sizeof(atomic_t);

	if (!alloc_cpumask_var(&prof_cpu_mask, GFP_KERNEL))
		return -ENOMEM;

	cpumask_copy(prof_cpu_mask, cpu_possible_mask);

	prof_buffer = kzalloc(buffer_bytes, GFP_KERNEL|__GFP_NOWARN);
	if (prof_buffer)
		return 0;

	prof_buffer = alloc_pages_exact(buffer_bytes,
					GFP_KERNEL|__GFP_ZERO|__GFP_NOWARN);
	if (prof_buffer)
		return 0;

	prof_buffer = vzalloc(buffer_bytes);
	if (prof_buffer)
		return 0;

	free_cpumask_var(prof_cpu_mask);
	return -ENOMEM;
}

*/ Profile event notifications /*

static BLOCKING_NOTIFIER_HEAD(task_exit_notifier);
static ATOMIC_NOTIFIER_HEAD(task_free_notifier);
static BLOCKING_NOTIFIER_HEAD(munmap_notifier);

void profile_task_exit(struct task_structtask)
{
	blocking_notifier_call_chain(&task_exit_notifier, 0, task);
}

int profile_handoff_task(struct task_structtask)
{
	int ret;
	ret = atomic_notifier_call_chain(&task_free_notifier, 0, task);
	return (ret == NOTIFY_OK) ? 1 : 0;
}

void profile_munmap(unsigned long addr)
{
	blocking_notifier_call_chain(&munmap_notifier, 0, (void)addr);
}

int task_handoff_register(struct notifier_blockn)
{
	return atomic_notifier_chain_register(&task_free_notifier, n);
}
EXPORT_SYMBOL_GPL(task_handoff_register);

int task_handoff_unregister(struct notifier_blockn)
{
	return atomic_notifier_chain_unregister(&task_free_notifier, n);
}
EXPORT_SYMBOL_GPL(task_handoff_unregister);

int profile_event_register(enum profile_type type, struct notifier_blockn)
{
	int err = -EINVAL;

	switch (type) {
	case PROFILE_TASK_EXIT:
		err = blocking_notifier_chain_register(
				&task_exit_notifier, n);
		break;
	case PROFILE_MUNMAP:
		err = blocking_notifier_chain_register(
				&munmap_notifier, n);
		break;
	}

	return err;
}
EXPORT_SYMBOL_GPL(profile_event_register);

int profile_event_unregister(enum profile_type type, struct notifier_blockn)
{
	int err = -EINVAL;

	switch (type) {
	case PROFILE_TASK_EXIT:
		err = blocking_notifier_chain_unregister(
				&task_exit_notifier, n);
		break;
	case PROFILE_MUNMAP:
		err = blocking_notifier_chain_unregister(
				&munmap_notifier, n);
		break;
	}

	return err;
}
EXPORT_SYMBOL_GPL(profile_event_unregister);

#if defined(CONFIG_SMP) && defined(CONFIG_PROC_FS)
*/
 Each cpu has a pair of open-addressed hashtables for pending
 profile hits. read_profile() IPI's all cpus to request them
 to flip buffers and flushes their contents to prof_buffer itself.
 Flip requests are serialized by the profile_flip_mutex. The sole
 use of having a second hashtable is for avoiding cacheline
 contention that would otherwise happen during flushes of pending
 profile hits required for the accuracy of reported profile hits
 and so resurrect the interrupt livelock issue.

 The open-addressed hashtables are indexed by profile buffer slot
 and hold the number of pending hits to that profile buffer slot on
 a cpu in an entry. When the hashtable overflows, all pending hits
 are accounted to their corresponding profile buffer slots with
 atomic_add() and the hashtable emptied. As numerous pending hits
 may be accounted to a profile buffer slot in a hashtable entry,
 this amortizes a number of atomic profile buffer increments likely
 to be far larger than the number of entries in the hashtable,
 particularly given that the number of distinct profile buffer
 positions to which hits are accounted during short intervals (e.g.
 several seconds) is usually very small. Exclusion from buffer
 flipping is provided by interrupt disablement (note that for
 SCHED_PROFILING or SLEEP_PROFILING profile_hit() may be called from
 process context).
 The hash function is meant to be lightweight as opposed to strong,
 and was vaguely inspired by ppc64 firmware-supported inverted
 pagetable hash functions, but uses a full hashtable full of finite
 collision chains, not just pairs of them.

 -- nyc
 /*
static void __profile_flip_buffers(voidunused)
{
	int cpu = smp_processor_id();

	per_cpu(cpu_profile_flip, cpu) = !per_cpu(cpu_profile_flip, cpu);
}

static void profile_flip_buffers(void)
{
	int i, j, cpu;

	mutex_lock(&profile_flip_mutex);
	j = per_cpu(cpu_profile_flip, get_cpu());
	put_cpu();
	on_each_cpu(__profile_flip_buffers, NULL, 1);
	for_each_online_cpu(cpu) {
		struct profile_hithits = per_cpu(cpu_profile_hits, cpu)[j];
		for (i = 0; i < NR_PROFILE_HIT; ++i) {
			if (!hits[i].hits) {
				if (hits[i].pc)
					hits[i].pc = 0;
				continue;
			}
			atomic_add(hits[i].hits, &prof_buffer[hits[i].pc]);
			hits[i].hits = hits[i].pc = 0;
		}
	}
	mutex_unlock(&profile_flip_mutex);
}

static void profile_discard_flip_buffers(void)
{
	int i, cpu;

	mutex_lock(&profile_flip_mutex);
	i = per_cpu(cpu_profile_flip, get_cpu());
	put_cpu();
	on_each_cpu(__profile_flip_buffers, NULL, 1);
	for_each_online_cpu(cpu) {
		struct profile_hithits = per_cpu(cpu_profile_hits, cpu)[i];
		memset(hits, 0, NR_PROFILE_HIT*sizeof(struct profile_hit));
	}
	mutex_unlock(&profile_flip_mutex);
}

static void do_profile_hits(int type, void__pc, unsigned int nr_hits)
{
	unsigned long primary, secondary, flags, pc = (unsigned long)__pc;
	int i, j, cpu;
	struct profile_hithits;

	pc = min((pc - (unsigned long)_stext) >> prof_shift, prof_len - 1);
	i = primary = (pc & (NR_PROFILE_GRP - 1)) << PROFILE_GRPSHIFT;
	secondary = (~(pc << 1) & (NR_PROFILE_GRP - 1)) << PROFILE_GRPSHIFT;
	cpu = get_cpu();
	hits = per_cpu(cpu_profile_hits, cpu)[per_cpu(cpu_profile_flip, cpu)];
	if (!hits) {
		put_cpu();
		return;
	}
	*/
	 We buffer the global profiler buffer into a per-CPU
	 queue and thus reduce the number of global (and possibly
	 NUMA-alien) accesses. The write-queue is self-coalescing:
	 /*
	local_irq_save(flags);
	do {
		for (j = 0; j < PROFILE_GRPSZ; ++j) {
			if (hits[i + j].pc == pc) {
				hits[i + j].hits += nr_hits;
				goto out;
			} else if (!hits[i + j].hits) {
				hits[i + j].pc = pc;
				hits[i + j].hits = nr_hits;
				goto out;
			}
		}
		i = (i + secondary) & (NR_PROFILE_HIT - 1);
	} while (i != primary);

	*/
	 Add the current hit(s) and flush the write-queue out
	 to the global buffer:
	 /*
	atomic_add(nr_hits, &prof_buffer[pc]);
	for (i = 0; i < NR_PROFILE_HIT; ++i) {
		atomic_add(hits[i].hits, &prof_buffer[hits[i].pc]);
		hits[i].pc = hits[i].hits = 0;
	}
out:
	local_irq_restore(flags);
	put_cpu();
}

static int profile_cpu_callback(struct notifier_blockinfo,
					unsigned long action, void__cpu)
{
	int node, cpu = (unsigned long)__cpu;
	struct pagepage;

	switch (action) {
	case CPU_UP_PREPARE:
	case CPU_UP_PREPARE_FROZEN:
		node = cpu_to_mem(cpu);
		per_cpu(cpu_profile_flip, cpu) = 0;
		if (!per_cpu(cpu_profile_hits, cpu)[1]) {
			page = __alloc_pages_node(node,
					GFP_KERNEL | __GFP_ZERO,
					0);
			if (!page)
				return notifier_from_errno(-ENOMEM);
			per_cpu(cpu_profile_hits, cpu)[1] = page_address(page);
		}
		if (!per_cpu(cpu_profile_hits, cpu)[0]) {
			page = __alloc_pages_node(node,
					GFP_KERNEL | __GFP_ZERO,
					0);
			if (!page)
				goto out_free;
			per_cpu(cpu_profile_hits, cpu)[0] = page_address(page);
		}
		break;
out_free:
		page = virt_to_page(per_cpu(cpu_profile_hits, cpu)[1]);
		per_cpu(cpu_profile_hits, cpu)[1] = NULL;
		__free_page(page);
		return notifier_from_errno(-ENOMEM);
	case CPU_ONLINE:
	case CPU_ONLINE_FROZEN:
		if (prof_cpu_mask != NULL)
			cpumask_set_cpu(cpu, prof_cpu_mask);
		break;
	case CPU_UP_CANCELED:
	case CPU_UP_CANCELED_FROZEN:
	case CPU_DEAD:
	case CPU_DEAD_FROZEN:
		if (prof_cpu_mask != NULL)
			cpumask_clear_cpu(cpu, prof_cpu_mask);
		if (per_cpu(cpu_profile_hits, cpu)[0]) {
			page = virt_to_page(per_cpu(cpu_profile_hits, cpu)[0]);
			per_cpu(cpu_profile_hits, cpu)[0] = NULL;
			__free_page(page);
		}
		if (per_cpu(cpu_profile_hits, cpu)[1]) {
			page = virt_to_page(per_cpu(cpu_profile_hits, cpu)[1]);
			per_cpu(cpu_profile_hits, cpu)[1] = NULL;
			__free_page(page);
		}
		break;
	}
	return NOTIFY_OK;
}
#else */ !CONFIG_SMP /*
#define profile_flip_buffers()		do { } while (0)
#define profile_discard_flip_buffers()	do { } while (0)
#define profile_cpu_callback		NULL

static void do_profile_hits(int type, void__pc, unsigned int nr_hits)
{
	unsigned long pc;
	pc = ((unsigned long)__pc - (unsigned long)_stext) >> prof_shift;
	atomic_add(nr_hits, &prof_buffer[min(pc, prof_len - 1)]);
}
#endif */ !CONFIG_SMP /*

void profile_hits(int type, void__pc, unsigned int nr_hits)
{
	if (prof_on != type || !prof_buffer)
		return;
	do_profile_hits(type, __pc, nr_hits);
}
EXPORT_SYMBOL_GPL(profile_hits);

void profile_tick(int type)
{
	struct pt_regsregs = get_irq_regs();

	if (!user_mode(regs) && prof_cpu_mask != NULL &&
	    cpumask_test_cpu(smp_processor_id(), prof_cpu_mask))
		profile_hit(type, (void)profile_pc(regs));
}

#ifdef CONFIG_PROC_FS
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>

static int prof_cpu_mask_proc_show(struct seq_filem, voidv)
{
	seq_printf(m, "%*pb\n", cpumask_pr_args(prof_cpu_mask));
	return 0;
}

static int prof_cpu_mask_proc_open(struct inodeinode, struct filefile)
{
	return single_open(file, prof_cpu_mask_proc_show, NULL);
}

static ssize_t prof_cpu_mask_proc_write(struct filefile,
	const char __userbuffer, size_t count, loff_tpos)
{
	cpumask_var_t new_value;
	int err;

	if (!alloc_cpumask_var(&new_value, GFP_KERNEL))
		return -ENOMEM;

	err = cpumask_parse_user(buffer, count, new_value);
	if (!err) {
		cpumask_copy(prof_cpu_mask, new_value);
		err = count;
	}
	free_cpumask_var(new_value);
	return err;
}

static const struct file_operations prof_cpu_mask_proc_fops = {
	.open		= prof_cpu_mask_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
	.write		= prof_cpu_mask_proc_write,
};

void create_prof_cpu_mask(void)
{
	*/ create /proc/irq/prof_cpu_mask /*
	proc_create("irq/prof_cpu_mask", 0600, NULL, &prof_cpu_mask_proc_fops);
}

*/
 This function accesses profiling information. The returned data is
 binary: the sampling step and the actual contents of the profile
 buffer. Use of the program readprofile is recommended in order to
 get meaningful info out of these data.
 /*
static ssize_t
read_profile(struct filefile, char __userbuf, size_t count, loff_tppos)
{
	unsigned long p =ppos;
	ssize_t read;
	charpnt;
	unsigned int sample_step = 1 << prof_shift;

	profile_flip_buffers();
	if (p >= (prof_len+1)*sizeof(unsigned int))
		return 0;
	if (count > (prof_len+1)*sizeof(unsigned int) - p)
		count = (prof_len+1)*sizeof(unsigned int) - p;
	read = 0;

	while (p < sizeof(unsigned int) && count > 0) {
		if (put_user(*((char)(&sample_step)+p), buf))
			return -EFAULT;
		buf++; p++; count--; read++;
	}
	pnt = (char)prof_buffer + p - sizeof(atomic_t);
	if (copy_to_user(buf, (void)pnt, count))
		return -EFAULT;
	read += count;
	*ppos += read;
	return read;
}

*/
 Writing to /proc/profile resets the counters

 Writing a 'profiling multiplier' value into it also re-sets the profiling
 interrupt frequency, on architectures that support this.
 /*
static ssize_t write_profile(struct filefile, const char __userbuf,
			     size_t count, loff_tppos)
{
#ifdef CONFIG_SMP
	extern int setup_profiling_timer(unsigned int multiplier);

	if (count == sizeof(int)) {
		unsigned int multiplier;

		if (copy_from_user(&multiplier, buf, sizeof(int)))
			return -EFAULT;

		if (setup_profiling_timer(multiplier))
			return -EINVAL;
	}
#endif
	profile_discard_flip_buffers();
	memset(prof_buffer, 0, prof_len sizeof(atomic_t));
	return count;
}

static const struct file_operations proc_profile_operations = {
	.read		= read_profile,
	.write		= write_profile,
	.llseek		= default_llseek,
};

#ifdef CONFIG_SMP
static void profile_nop(voidunused)
{
}

static int create_hash_tables(void)
{
	int cpu;

	for_each_online_cpu(cpu) {
		int node = cpu_to_mem(cpu);
		struct pagepage;

		page = __alloc_pages_node(node,
				GFP_KERNEL | __GFP_ZERO | __GFP_THISNODE,
				0);
		if (!page)
			goto out_cleanup;
		per_cpu(cpu_profile_hits, cpu)[1]
				= (struct profile_hit)page_address(page);
		page = __alloc_pages_node(node,
				GFP_KERNEL | __GFP_ZERO | __GFP_THISNODE,
				0);
		if (!page)
			goto out_cleanup;
		per_cpu(cpu_profile_hits, cpu)[0]
				= (struct profile_hit)page_address(page);
	}
	return 0;
out_cleanup:
	prof_on = 0;
	smp_mb();
	on_each_cpu(profile_nop, NULL, 1);
	for_each_online_cpu(cpu) {
		struct pagepage;

		if (per_cpu(cpu_profile_hits, cpu)[0]) {
			page = virt_to_page(per_cpu(cpu_profile_hits, cpu)[0]);
			per_cpu(cpu_profile_hits, cpu)[0] = NULL;
			__free_page(page);
		}
		if (per_cpu(cpu_profile_hits, cpu)[1]) {
			page = virt_to_page(per_cpu(cpu_profile_hits, cpu)[1]);
			per_cpu(cpu_profile_hits, cpu)[1] = NULL;
			__free_page(page);
		}
	}
	return -1;
}
#else
#define create_hash_tables()			({ 0; })
#endif

int __ref create_proc_profile(void)/ false positive from hotcpu_notifier /*
{
	struct proc_dir_entryentry;
	int err = 0;

	if (!prof_on)
		return 0;

	cpu_notifier_register_begin();

	if (create_hash_tables()) {
		err = -ENOMEM;
		goto out;
	}

	entry = proc_create("profile", S_IWUSR | S_IRUGO,
			    NULL, &proc_profile_operations);
	if (!entry)
		goto out;
	proc_set_size(entry, (1 + prof_len) sizeof(atomic_t));
	__hotcpu_notifier(profile_cpu_callback, 0);

out:
	cpu_notifier_register_done();
	return err;
}
subsys_initcall(create_proc_profile);
#endif*/ CONFIG_PROC_FS

 linux/kernel/ptrace.c

 (C) Copyright 1999 Linus Torvalds

 Common interfaces for "ptrace()" which we do not want
 to continually duplicate across every architecture.
 /*

#include <linux/capability.h>
#include <linux/export.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/highmem.h>
#include <linux/pagemap.h>
#include <linux/ptrace.h>
#include <linux/security.h>
#include <linux/signal.h>
#include <linux/uio.h>
#include <linux/audit.h>
#include <linux/pid_namespace.h>
#include <linux/syscalls.h>
#include <linux/uaccess.h>
#include <linux/regset.h>
#include <linux/hw_breakpoint.h>
#include <linux/cn_proc.h>
#include <linux/compat.h>


*/
 ptrace a task: make the debugger its new parent and
 move it to the ptrace list.

 Must be called with the tasklist lock write-held.
 /*
void __ptrace_link(struct task_structchild, struct task_structnew_parent)
{
	BUG_ON(!list_empty(&child->ptrace_entry));
	list_add(&child->ptrace_entry, &new_parent->ptraced);
	child->parent = new_parent;
}

*/
 __ptrace_unlink - unlink ptracee and restore its execution state
 @child: ptracee to be unlinked

 Remove @child from the ptrace list, move it back to the original parent,
 and restore the execution state so that it conforms to the group stop
 state.

 Unlinking can happen via two paths - explicit PTRACE_DETACH or ptracer
 exiting.  For PTRACE_DETACH, unless the ptracee has been killed between
 ptrace_check_attach() and here, it's guaranteed to be in TASK_TRACED.
 If the ptracer is exiting, the ptracee can be in any state.

 After detach, the ptracee should be in a state which conforms to the
 group stop.  If the group is stopped or in the process of stopping, the
 ptracee should be put into TASK_STOPPED; otherwise, it should be woken
 up from TASK_TRACED.

 If the ptracee is in TASK_TRACED and needs to be moved to TASK_STOPPED,
 it goes through TRACED -> RUNNING -> STOPPED transition which is similar
 to but in the opposite direction of what happens while attaching to a
 stopped task.  However, in this direction, the intermediate RUNNING
 state is not hidden even from the current ptracer and if it immediately
 re-attaches and performs a WNOHANG wait(2), it may fail.

 CONTEXT:
 write_lock_irq(tasklist_lock)
 /*
void __ptrace_unlink(struct task_structchild)
{
	BUG_ON(!child->ptrace);

	child->parent = child->real_parent;
	list_del_init(&child->ptrace_entry);

	spin_lock(&child->sighand->siglock);
	child->ptrace = 0;
	*/
	 Clear all pending traps and TRAPPING.  TRAPPING should be
	 cleared regardless of JOBCTL_STOP_PENDING.  Do it explicitly.
	 /*
	task_clear_jobctl_pending(child, JOBCTL_TRAP_MASK);
	task_clear_jobctl_trapping(child);

	*/
	 Reinstate JOBCTL_STOP_PENDING if group stop is in effect and
	 @child isn't dead.
	 /*
	if (!(child->flags & PF_EXITING) &&
	    (child->signal->flags & SIGNAL_STOP_STOPPED ||
	     child->signal->group_stop_count)) {
		child->jobctl |= JOBCTL_STOP_PENDING;

		*/
		 This is only possible if this thread was cloned by the
		 traced task running in the stopped group, set the signal
		 for the future reports.
		 FIXME: we should change ptrace_init_task() to handle this
		 case.
		 /*
		if (!(child->jobctl & JOBCTL_STOP_SIGMASK))
			child->jobctl |= SIGSTOP;
	}

	*/
	 If transition to TASK_STOPPED is pending or in TASK_TRACED, kick
	 @child in the butt.  Note that @resume should be used iff @child
	 is in TASK_TRACED; otherwise, we might unduly disrupt
	 TASK_KILLABLE sleeps.
	 /*
	if (child->jobctl & JOBCTL_STOP_PENDING || task_is_traced(child))
		ptrace_signal_wake_up(child, true);

	spin_unlock(&child->sighand->siglock);
}

*/ Ensure that nothing can wake it up, even SIGKILL /*
static bool ptrace_freeze_traced(struct task_structtask)
{
	bool ret = false;

	*/ Lockless, nobody but us can set this flag /*
	if (task->jobctl & JOBCTL_LISTENING)
		return ret;

	spin_lock_irq(&task->sighand->siglock);
	if (task_is_traced(task) && !__fatal_signal_pending(task)) {
		task->state = __TASK_TRACED;
		ret = true;
	}
	spin_unlock_irq(&task->sighand->siglock);

	return ret;
}

static void ptrace_unfreeze_traced(struct task_structtask)
{
	if (task->state != __TASK_TRACED)
		return;

	WARN_ON(!task->ptrace || task->parent != current);

	spin_lock_irq(&task->sighand->siglock);
	if (__fatal_signal_pending(task))
		wake_up_state(task, __TASK_TRACED);
	else
		task->state = TASK_TRACED;
	spin_unlock_irq(&task->sighand->siglock);
}

*/
 ptrace_check_attach - check whether ptracee is ready for ptrace operation
 @child: ptracee to check for
 @ignore_state: don't check whether @child is currently %TASK_TRACED

 Check whether @child is being ptraced by %current and ready for further
 ptrace operations.  If @ignore_state is %false, @child also should be in
 %TASK_TRACED state and on return the child is guaranteed to be traced
 and not executing.  If @ignore_state is %true, @child can be in any
 state.

 CONTEXT:
 Grabs and releases tasklist_lock and @child->sighand->siglock.

 RETURNS:
 0 on success, -ESRCH if %child is not ready.
 /*
static int ptrace_check_attach(struct task_structchild, bool ignore_state)
{
	int ret = -ESRCH;

	*/
	 We take the read lock around doing both checks to close a
	 possible race where someone else was tracing our child and
	 detached between these two checks.  After this locked check,
	 we are sure that this is our traced child and that can only
	 be changed by us so it's not changing right after this.
	 /*
	read_lock(&tasklist_lock);
	if (child->ptrace && child->parent == current) {
		WARN_ON(child->state == __TASK_TRACED);
		*/
		 child->sighand can't be NULL, release_task()
		 does ptrace_unlink() before __exit_signal().
		 /*
		if (ignore_state || ptrace_freeze_traced(child))
			ret = 0;
	}
	read_unlock(&tasklist_lock);

	if (!ret && !ignore_state) {
		if (!wait_task_inactive(child, __TASK_TRACED)) {
			*/
			 This can only happen if may_ptrace_stop() fails and
			 ptrace_stop() changes ->state back to TASK_RUNNING,
			 so we should not worry about leaking __TASK_TRACED.
			 /*
			WARN_ON(child->state == __TASK_TRACED);
			ret = -ESRCH;
		}
	}

	return ret;
}

static int ptrace_has_cap(struct user_namespacens, unsigned int mode)
{
	if (mode & PTRACE_MODE_NOAUDIT)
		return has_ns_capability_noaudit(current, ns, CAP_SYS_PTRACE);
	else
		return has_ns_capability(current, ns, CAP_SYS_PTRACE);
}

*/ Returns 0 on success, -errno on denial. /*
static int __ptrace_may_access(struct task_structtask, unsigned int mode)
{
	const struct credcred = current_cred(),tcred;
	int dumpable = 0;
	kuid_t caller_uid;
	kgid_t caller_gid;

	if (!(mode & PTRACE_MODE_FSCREDS) == !(mode & PTRACE_MODE_REALCREDS)) {
		WARN(1, "denying ptrace access check without PTRACE_MODE_*CREDS\n");
		return -EPERM;
	}

	*/ May we inspect the given task?
	 This check is used both for attaching with ptrace
	 and for allowing access to sensitive information in /proc.
	
	 ptrace_attach denies several cases that /proc allows
	 because setting up the necessary parent/child relationship
	 or halting the specified task is impossible.
	 /*

	*/ Don't let security modules deny introspection /*
	if (same_thread_group(task, current))
		return 0;
	rcu_read_lock();
	if (mode & PTRACE_MODE_FSCREDS) {
		caller_uid = cred->fsuid;
		caller_gid = cred->fsgid;
	} else {
		*/
		 Using the euid would make more sense here, but something
		 in userland might rely on the old behavior, and this
		 shouldn't be a security problem since
		 PTRACE_MODE_REALCREDS implies that the caller explicitly
		 used a syscall that requests access to another process
		 (and not a filesystem syscall to procfs).
		 /*
		caller_uid = cred->uid;
		caller_gid = cred->gid;
	}
	tcred = __task_cred(task);
	if (uid_eq(caller_uid, tcred->euid) &&
	    uid_eq(caller_uid, tcred->suid) &&
	    uid_eq(caller_uid, tcred->uid)  &&
	    gid_eq(caller_gid, tcred->egid) &&
	    gid_eq(caller_gid, tcred->sgid) &&
	    gid_eq(caller_gid, tcred->gid))
		goto ok;
	if (ptrace_has_cap(tcred->user_ns, mode))
		goto ok;
	rcu_read_unlock();
	return -EPERM;
ok:
	rcu_read_unlock();
	smp_rmb();
	if (task->mm)
		dumpable = get_dumpable(task->mm);
	rcu_read_lock();
	if (dumpable != SUID_DUMP_USER &&
	    !ptrace_has_cap(__task_cred(task)->user_ns, mode)) {
		rcu_read_unlock();
		return -EPERM;
	}
	rcu_read_unlock();

	return security_ptrace_access_check(task, mode);
}

bool ptrace_may_access(struct task_structtask, unsigned int mode)
{
	int err;
	task_lock(task);
	err = __ptrace_may_access(task, mode);
	task_unlock(task);
	return !err;
}

static int ptrace_attach(struct task_structtask, long request,
			 unsigned long addr,
			 unsigned long flags)
{
	bool seize = (request == PTRACE_SEIZE);
	int retval;

	retval = -EIO;
	if (seize) {
		if (addr != 0)
			goto out;
		if (flags & ~(unsigned long)PTRACE_O_MASK)
			goto out;
		flags = PT_PTRACED | PT_SEIZED | (flags << PT_OPT_FLAG_SHIFT);
	} else {
		flags = PT_PTRACED;
	}

	audit_ptrace(task);

	retval = -EPERM;
	if (unlikely(task->flags & PF_KTHREAD))
		goto out;
	if (same_thread_group(task, current))
		goto out;

	*/
	 Protect exec's credential calculations against our interference;
	 SUID, SGID and LSM creds get determined differently
	 under ptrace.
	 /*
	retval = -ERESTARTNOINTR;
	if (mutex_lock_interruptible(&task->signal->cred_guard_mutex))
		goto out;

	task_lock(task);
	retval = __ptrace_may_access(task, PTRACE_MODE_ATTACH_REALCREDS);
	task_unlock(task);
	if (retval)
		goto unlock_creds;

	write_lock_irq(&tasklist_lock);
	retval = -EPERM;
	if (unlikely(task->exit_state))
		goto unlock_tasklist;
	if (task->ptrace)
		goto unlock_tasklist;

	if (seize)
		flags |= PT_SEIZED;
	rcu_read_lock();
	if (ns_capable(__task_cred(task)->user_ns, CAP_SYS_PTRACE))
		flags |= PT_PTRACE_CAP;
	rcu_read_unlock();
	task->ptrace = flags;

	__ptrace_link(task, current);

	*/ SEIZE doesn't trap tracee on attach /*
	if (!seize)
		send_sig_info(SIGSTOP, SEND_SIG_FORCED, task);

	spin_lock(&task->sighand->siglock);

	*/
	 If the task is already STOPPED, set JOBCTL_TRAP_STOP and
	 TRAPPING, and kick it so that it transits to TRACED.  TRAPPING
	 will be cleared if the child completes the transition or any
	 event which clears the group stop states happens.  We'll wait
	 for the transition to complete before returning from this
	 function.
	
	 This hides STOPPED -> RUNNING -> TRACED transition from the
	 attaching thread but a different thread in the same group can
	 still observe the transient RUNNING state.  IOW, if another
	 thread's WNOHANG wait(2) on the stopped tracee races against
	 ATTACH, the wait(2) may fail due to the transient RUNNING.
	
	 The following task_is_stopped() test is safe as both transitions
	 in and out of STOPPED are protected by siglock.
	 /*
	if (task_is_stopped(task) &&
	    task_set_jobctl_pending(task, JOBCTL_TRAP_STOP | JOBCTL_TRAPPING))
		signal_wake_up_state(task, __TASK_STOPPED);

	spin_unlock(&task->sighand->siglock);

	retval = 0;
unlock_tasklist:
	write_unlock_irq(&tasklist_lock);
unlock_creds:
	mutex_unlock(&task->signal->cred_guard_mutex);
out:
	if (!retval) {
		*/
		 We do not bother to change retval or clear JOBCTL_TRAPPING
		 if wait_on_bit() was interrupted by SIGKILL. The tracer will
		 not return to user-mode, it will exit and clear this bit in
		 __ptrace_unlink() if it wasn't already cleared by the tracee;
		 and until then nobody can ptrace this task.
		 /*
		wait_on_bit(&task->jobctl, JOBCTL_TRAPPING_BIT, TASK_KILLABLE);
		proc_ptrace_connector(task, PTRACE_ATTACH);
	}

	return retval;
}

*/
 ptrace_traceme  --  helper for PTRACE_TRACEME

 Performs checks and sets PT_PTRACED.
 Should be used by all ptrace implementations for PTRACE_TRACEME.
 /*
static int ptrace_traceme(void)
{
	int ret = -EPERM;

	write_lock_irq(&tasklist_lock);
	*/ Are we already being traced? /*
	if (!current->ptrace) {
		ret = security_ptrace_traceme(current->parent);
		*/
		 Check PF_EXITING to ensure ->real_parent has not passed
		 exit_ptrace(). Otherwise we don't report the error but
		 pretend ->real_parent untraces us right after return.
		 /*
		if (!ret && !(current->real_parent->flags & PF_EXITING)) {
			current->ptrace = PT_PTRACED;
			__ptrace_link(current, current->real_parent);
		}
	}
	write_unlock_irq(&tasklist_lock);

	return ret;
}

*/
 Called with irqs disabled, returns true if childs should reap themselves.
 /*
static int ignoring_children(struct sighand_structsigh)
{
	int ret;
	spin_lock(&sigh->siglock);
	ret = (sigh->action[SIGCHLD-1].sa.sa_handler == SIG_IGN) ||
	      (sigh->action[SIGCHLD-1].sa.sa_flags & SA_NOCLDWAIT);
	spin_unlock(&sigh->siglock);
	return ret;
}

*/
 Called with tasklist_lock held for writing.
 Unlink a traced task, and clean it up if it was a traced zombie.
 Return true if it needs to be reaped with release_task().
 (We can't call release_task() here because we already hold tasklist_lock.)

 If it's a zombie, our attachedness prevented normal parent notification
 or self-reaping.  Do notification now if it would have happened earlier.
 If it should reap itself, return true.

 If it's our own child, there is no notification to do. But if our normal
 children self-reap, then this child was prevented by ptrace and we must
 reap it now, in that case we must also wake up sub-threads sleeping in
 do_wait().
 /*
static bool __ptrace_detach(struct task_structtracer, struct task_structp)
{
	bool dead;

	__ptrace_unlink(p);

	if (p->exit_state != EXIT_ZOMBIE)
		return false;

	dead = !thread_group_leader(p);

	if (!dead && thread_group_empty(p)) {
		if (!same_thread_group(p->real_parent, tracer))
			dead = do_notify_parent(p, p->exit_signal);
		else if (ignoring_children(tracer->sighand)) {
			__wake_up_parent(p, tracer);
			dead = true;
		}
	}
	*/ Mark it as in the process of being reaped. /*
	if (dead)
		p->exit_state = EXIT_DEAD;
	return dead;
}

static int ptrace_detach(struct task_structchild, unsigned int data)
{
	if (!valid_signal(data))
		return -EIO;

	*/ Architecture-specific hardware disable .. /*
	ptrace_disable(child);
	clear_tsk_thread_flag(child, TIF_SYSCALL_TRACE);

	write_lock_irq(&tasklist_lock);
	*/
	 We rely on ptrace_freeze_traced(). It can't be killed and
	 untraced by another thread, it can't be a zombie.
	 /*
	WARN_ON(!child->ptrace || child->exit_state);
	*/
	 tasklist_lock avoids the race with wait_task_stopped(), see
	 the comment in ptrace_resume().
	 /*
	child->exit_code = data;
	__ptrace_detach(current, child);
	write_unlock_irq(&tasklist_lock);

	proc_ptrace_connector(child, PTRACE_DETACH);

	return 0;
}

*/
 Detach all tasks we were using ptrace on. Called with tasklist held
 for writing.
 /*
void exit_ptrace(struct task_structtracer, struct list_headdead)
{
	struct task_structp,n;

	list_for_each_entry_safe(p, n, &tracer->ptraced, ptrace_entry) {
		if (unlikely(p->ptrace & PT_EXITKILL))
			send_sig_info(SIGKILL, SEND_SIG_FORCED, p);

		if (__ptrace_detach(tracer, p))
			list_add(&p->ptrace_entry, dead);
	}
}

int ptrace_readdata(struct task_structtsk, unsigned long src, char __userdst, int len)
{
	int copied = 0;

	while (len > 0) {
		char buf[128];
		int this_len, retval;

		this_len = (len > sizeof(buf)) ? sizeof(buf) : len;
		retval = access_process_vm(tsk, src, buf, this_len, 0);
		if (!retval) {
			if (copied)
				break;
			return -EIO;
		}
		if (copy_to_user(dst, buf, retval))
			return -EFAULT;
		copied += retval;
		src += retval;
		dst += retval;
		len -= retval;
	}
	return copied;
}

int ptrace_writedata(struct task_structtsk, char __usersrc, unsigned long dst, int len)
{
	int copied = 0;

	while (len > 0) {
		char buf[128];
		int this_len, retval;

		this_len = (len > sizeof(buf)) ? sizeof(buf) : len;
		if (copy_from_user(buf, src, this_len))
			return -EFAULT;
		retval = access_process_vm(tsk, dst, buf, this_len, 1);
		if (!retval) {
			if (copied)
				break;
			return -EIO;
		}
		copied += retval;
		src += retval;
		dst += retval;
		len -= retval;
	}
	return copied;
}

static int ptrace_setoptions(struct task_structchild, unsigned long data)
{
	unsigned flags;

	if (data & ~(unsigned long)PTRACE_O_MASK)
		return -EINVAL;

	if (unlikely(data & PTRACE_O_SUSPEND_SECCOMP)) {
		if (!config_enabled(CONFIG_CHECKPOINT_RESTORE) ||
		    !config_enabled(CONFIG_SECCOMP))
			return -EINVAL;

		if (!capable(CAP_SYS_ADMIN))
			return -EPERM;

		if (seccomp_mode(&current->seccomp) != SECCOMP_MODE_DISABLED ||
		    current->ptrace & PT_SUSPEND_SECCOMP)
			return -EPERM;
	}

	*/ Avoid intermediate state when all opts are cleared /*
	flags = child->ptrace;
	flags &= ~(PTRACE_O_MASK << PT_OPT_FLAG_SHIFT);
	flags |= (data << PT_OPT_FLAG_SHIFT);
	child->ptrace = flags;

	return 0;
}

static int ptrace_getsiginfo(struct task_structchild, siginfo_tinfo)
{
	unsigned long flags;
	int error = -ESRCH;

	if (lock_task_sighand(child, &flags)) {
		error = -EINVAL;
		if (likely(child->last_siginfo != NULL)) {
			*info =child->last_siginfo;
			error = 0;
		}
		unlock_task_sighand(child, &flags);
	}
	return error;
}

static int ptrace_setsiginfo(struct task_structchild, const siginfo_tinfo)
{
	unsigned long flags;
	int error = -ESRCH;

	if (lock_task_sighand(child, &flags)) {
		error = -EINVAL;
		if (likely(child->last_siginfo != NULL)) {
			*child->last_siginfo =info;
			error = 0;
		}
		unlock_task_sighand(child, &flags);
	}
	return error;
}

static int ptrace_peek_siginfo(struct task_structchild,
				unsigned long addr,
				unsigned long data)
{
	struct ptrace_peeksiginfo_args arg;
	struct sigpendingpending;
	struct sigqueueq;
	int ret, i;

	ret = copy_from_user(&arg, (void __user) addr,
				sizeof(struct ptrace_peeksiginfo_args));
	if (ret)
		return -EFAULT;

	if (arg.flags & ~PTRACE_PEEKSIGINFO_SHARED)
		return -EINVAL;/ unknown flags /*

	if (arg.nr < 0)
		return -EINVAL;

	if (arg.flags & PTRACE_PEEKSIGINFO_SHARED)
		pending = &child->signal->shared_pending;
	else
		pending = &child->pending;

	for (i = 0; i < arg.nr; ) {
		siginfo_t info;
		s32 off = arg.off + i;

		spin_lock_irq(&child->sighand->siglock);
		list_for_each_entry(q, &pending->list, list) {
			if (!off--) {
				copy_siginfo(&info, &q->info);
				break;
			}
		}
		spin_unlock_irq(&child->sighand->siglock);

		if (off >= 0)/ beyond the end of the list /*
			break;

#ifdef CONFIG_COMPAT
		if (unlikely(in_compat_syscall())) {
			compat_siginfo_t __useruinfo = compat_ptr(data);

			if (copy_siginfo_to_user32(uinfo, &info) ||
			    __put_user(info.si_code, &uinfo->si_code)) {
				ret = -EFAULT;
				break;
			}

		} else
#endif
		{
			siginfo_t __useruinfo = (siginfo_t __user) data;

			if (copy_siginfo_to_user(uinfo, &info) ||
			    __put_user(info.si_code, &uinfo->si_code)) {
				ret = -EFAULT;
				break;
			}
		}

		data += sizeof(siginfo_t);
		i++;

		if (signal_pending(current))
			break;

		cond_resched();
	}

	if (i > 0)
		return i;

	return ret;
}

#ifdef PTRACE_SINGLESTEP
#define is_singlestep(request)		((request) == PTRACE_SINGLESTEP)
#else
#define is_singlestep(request)		0
#endif

#ifdef PTRACE_SINGLEBLOCK
#define is_singleblock(request)		((request) == PTRACE_SINGLEBLOCK)
#else
#define is_singleblock(request)		0
#endif

#ifdef PTRACE_SYSEMU
#define is_sysemu_singlestep(request)	((request) == PTRACE_SYSEMU_SINGLESTEP)
#else
#define is_sysemu_singlestep(request)	0
#endif

static int ptrace_resume(struct task_structchild, long request,
			 unsigned long data)
{
	bool need_siglock;

	if (!valid_signal(data))
		return -EIO;

	if (request == PTRACE_SYSCALL)
		set_tsk_thread_flag(child, TIF_SYSCALL_TRACE);
	else
		clear_tsk_thread_flag(child, TIF_SYSCALL_TRACE);

#ifdef TIF_SYSCALL_EMU
	if (request == PTRACE_SYSEMU || request == PTRACE_SYSEMU_SINGLESTEP)
		set_tsk_thread_flag(child, TIF_SYSCALL_EMU);
	else
		clear_tsk_thread_flag(child, TIF_SYSCALL_EMU);
#endif

	if (is_singleblock(request)) {
		if (unlikely(!arch_has_block_step()))
			return -EIO;
		user_enable_block_step(child);
	} else if (is_singlestep(request) || is_sysemu_singlestep(request)) {
		if (unlikely(!arch_has_single_step()))
			return -EIO;
		user_enable_single_step(child);
	} else {
		user_disable_single_step(child);
	}

	*/
	 Change ->exit_code and ->state under siglock to avoid the race
	 with wait_task_stopped() in between; a non-zero ->exit_code will
	 wrongly look like another report from tracee.
	
	 Note that we need siglock even if ->exit_code == data and/or this
	 status was not reported yet, the new status must not be cleared by
	 wait_task_stopped() after resume.
	
	 If data == 0 we do not care if wait_task_stopped() reports the old
	 status and clears the code too; this can't race with the tracee, it
	 takes siglock after resume.
	 /*
	need_siglock = data && !thread_group_empty(current);
	if (need_siglock)
		spin_lock_irq(&child->sighand->siglock);
	child->exit_code = data;
	wake_up_state(child, __TASK_TRACED);
	if (need_siglock)
		spin_unlock_irq(&child->sighand->siglock);

	return 0;
}

#ifdef CONFIG_HAVE_ARCH_TRACEHOOK

static const struct user_regset
find_regset(const struct user_regset_viewview, unsigned int type)
{
	const struct user_regsetregset;
	int n;

	for (n = 0; n < view->n; ++n) {
		regset = view->regsets + n;
		if (regset->core_note_type == type)
			return regset;
	}

	return NULL;
}

static int ptrace_regset(struct task_structtask, int req, unsigned int type,
			 struct ioveckiov)
{
	const struct user_regset_viewview = task_user_regset_view(task);
	const struct user_regsetregset = find_regset(view, type);
	int regset_no;

	if (!regset || (kiov->iov_len % regset->size) != 0)
		return -EINVAL;

	regset_no = regset - view->regsets;
	kiov->iov_len = min(kiov->iov_len,
			    (__kernel_size_t) (regset->n regset->size));

	if (req == PTRACE_GETREGSET)
		return copy_regset_to_user(task, view, regset_no, 0,
					   kiov->iov_len, kiov->iov_base);
	else
		return copy_regset_from_user(task, view, regset_no, 0,
					     kiov->iov_len, kiov->iov_base);
}

*/
 This is declared in linux/regset.h and defined in machine-dependent
 code.  We put the export here, near the primary machine-neutral use,
 to ensure no machine forgets it.
 /*
EXPORT_SYMBOL_GPL(task_user_regset_view);
#endif

int ptrace_request(struct task_structchild, long request,
		   unsigned long addr, unsigned long data)
{
	bool seized = child->ptrace & PT_SEIZED;
	int ret = -EIO;
	siginfo_t siginfo,si;
	void __userdatavp = (void __user) data;
	unsigned long __userdatalp = datavp;
	unsigned long flags;

	switch (request) {
	case PTRACE_PEEKTEXT:
	case PTRACE_PEEKDATA:
		return generic_ptrace_peekdata(child, addr, data);
	case PTRACE_POKETEXT:
	case PTRACE_POKEDATA:
		return generic_ptrace_pokedata(child, addr, data);

#ifdef PTRACE_OLDSETOPTIONS
	case PTRACE_OLDSETOPTIONS:
#endif
	case PTRACE_SETOPTIONS:
		ret = ptrace_setoptions(child, data);
		break;
	case PTRACE_GETEVENTMSG:
		ret = put_user(child->ptrace_message, datalp);
		break;

	case PTRACE_PEEKSIGINFO:
		ret = ptrace_peek_siginfo(child, addr, data);
		break;

	case PTRACE_GETSIGINFO:
		ret = ptrace_getsiginfo(child, &siginfo);
		if (!ret)
			ret = copy_siginfo_to_user(datavp, &siginfo);
		break;

	case PTRACE_SETSIGINFO:
		if (copy_from_user(&siginfo, datavp, sizeof siginfo))
			ret = -EFAULT;
		else
			ret = ptrace_setsiginfo(child, &siginfo);
		break;

	case PTRACE_GETSIGMASK:
		if (addr != sizeof(sigset_t)) {
			ret = -EINVAL;
			break;
		}

		if (copy_to_user(datavp, &child->blocked, sizeof(sigset_t)))
			ret = -EFAULT;
		else
			ret = 0;

		break;

	case PTRACE_SETSIGMASK: {
		sigset_t new_set;

		if (addr != sizeof(sigset_t)) {
			ret = -EINVAL;
			break;
		}

		if (copy_from_user(&new_set, datavp, sizeof(sigset_t))) {
			ret = -EFAULT;
			break;
		}

		sigdelsetmask(&new_set, sigmask(SIGKILL)|sigmask(SIGSTOP));

		*/
		 Every thread does recalc_sigpending() after resume, so
		 retarget_shared_pending() and recalc_sigpending() are not
		 called here.
		 /*
		spin_lock_irq(&child->sighand->siglock);
		child->blocked = new_set;
		spin_unlock_irq(&child->sighand->siglock);

		ret = 0;
		break;
	}

	case PTRACE_INTERRUPT:
		*/
		 Stop tracee without any side-effect on signal or job
		 control.  At least one trap is guaranteed to happen
		 after this request.  If @child is already trapped, the
		 current trap is not disturbed and another trap will
		 happen after the current trap is ended with PTRACE_CONT.
		
		 The actual trap might not be PTRACE_EVENT_STOP trap but
		 the pending condition is cleared regardless.
		 /*
		if (unlikely(!seized || !lock_task_sighand(child, &flags)))
			break;

		*/
		 INTERRUPT doesn't disturb existing trap sans one
		 exception.  If ptracer issued LISTEN for the current
		 STOP, this INTERRUPT should clear LISTEN and re-trap
		 tracee into STOP.
		 /*
		if (likely(task_set_jobctl_pending(child, JOBCTL_TRAP_STOP)))
			ptrace_signal_wake_up(child, child->jobctl & JOBCTL_LISTENING);

		unlock_task_sighand(child, &flags);
		ret = 0;
		break;

	case PTRACE_LISTEN:
		*/
		 Listen for events.  Tracee must be in STOP.  It's not
		 resumed per-se but is not considered to be in TRACED by
		 wait(2) or ptrace(2).  If an async event (e.g. group
		 stop state change) happens, tracee will enter STOP trap
		 again.  Alternatively, ptracer can issue INTERRUPT to
		 finish listening and re-trap tracee into STOP.
		 /*
		if (unlikely(!seized || !lock_task_sighand(child, &flags)))
			break;

		si = child->last_siginfo;
		if (likely(si && (si->si_code >> 8) == PTRACE_EVENT_STOP)) {
			child->jobctl |= JOBCTL_LISTENING;
			*/
			 If NOTIFY is set, it means event happened between
			 start of this trap and now.  Trigger re-trap.
			 /*
			if (child->jobctl & JOBCTL_TRAP_NOTIFY)
				ptrace_signal_wake_up(child, true);
			ret = 0;
		}
		unlock_task_sighand(child, &flags);
		break;

	case PTRACE_DETACH:	/ detach a process that was attached. /*
		ret = ptrace_detach(child, data);
		break;

#ifdef CONFIG_BINFMT_ELF_FDPIC
	case PTRACE_GETFDPIC: {
		struct mm_structmm = get_task_mm(child);
		unsigned long tmp = 0;

		ret = -ESRCH;
		if (!mm)
			break;

		switch (addr) {
		case PTRACE_GETFDPIC_EXEC:
			tmp = mm->context.exec_fdpic_loadmap;
			break;
		case PTRACE_GETFDPIC_INTERP:
			tmp = mm->context.interp_fdpic_loadmap;
			break;
		default:
			break;
		}
		mmput(mm);

		ret = put_user(tmp, datalp);
		break;
	}
#endif

#ifdef PTRACE_SINGLESTEP
	case PTRACE_SINGLESTEP:
#endif
#ifdef PTRACE_SINGLEBLOCK
	case PTRACE_SINGLEBLOCK:
#endif
#ifdef PTRACE_SYSEMU
	case PTRACE_SYSEMU:
	case PTRACE_SYSEMU_SINGLESTEP:
#endif
	case PTRACE_SYSCALL:
	case PTRACE_CONT:
		return ptrace_resume(child, request, data);

	case PTRACE_KILL:
		if (child->exit_state)	*/ already dead /*
			return 0;
		return ptrace_resume(child, request, SIGKILL);

#ifdef CONFIG_HAVE_ARCH_TRACEHOOK
	case PTRACE_GETREGSET:
	case PTRACE_SETREGSET: {
		struct iovec kiov;
		struct iovec __useruiov = datavp;

		if (!access_ok(VERIFY_WRITE, uiov, sizeof(*uiov)))
			return -EFAULT;

		if (__get_user(kiov.iov_base, &uiov->iov_base) ||
		    __get_user(kiov.iov_len, &uiov->iov_len))
			return -EFAULT;

		ret = ptrace_regset(child, request, addr, &kiov);
		if (!ret)
			ret = __put_user(kiov.iov_len, &uiov->iov_len);
		break;
	}
#endif

	case PTRACE_SECCOMP_GET_FILTER:
		ret = seccomp_get_filter(child, addr, datavp);
		break;

	default:
		break;
	}

	return ret;
}

static struct task_structptrace_get_task_struct(pid_t pid)
{
	struct task_structchild;

	rcu_read_lock();
	child = find_task_by_vpid(pid);
	if (child)
		get_task_struct(child);
	rcu_read_unlock();

	if (!child)
		return ERR_PTR(-ESRCH);
	return child;
}

#ifndef arch_ptrace_attach
#define arch_ptrace_attach(child)	do { } while (0)
#endif

SYSCALL_DEFINE4(ptrace, long, request, long, pid, unsigned long, addr,
		unsigned long, data)
{
	struct task_structchild;
	long ret;

	if (request == PTRACE_TRACEME) {
		ret = ptrace_traceme();
		if (!ret)
			arch_ptrace_attach(current);
		goto out;
	}

	child = ptrace_get_task_struct(pid);
	if (IS_ERR(child)) {
		ret = PTR_ERR(child);
		goto out;
	}

	if (request == PTRACE_ATTACH || request == PTRACE_SEIZE) {
		ret = ptrace_attach(child, request, addr, data);
		*/
		 Some architectures need to do book-keeping after
		 a ptrace attach.
		 /*
		if (!ret)
			arch_ptrace_attach(child);
		goto out_put_task_struct;
	}

	ret = ptrace_check_attach(child, request == PTRACE_KILL ||
				  request == PTRACE_INTERRUPT);
	if (ret < 0)
		goto out_put_task_struct;

	ret = arch_ptrace(child, request, addr, data);
	if (ret || request != PTRACE_DETACH)
		ptrace_unfreeze_traced(child);

 out_put_task_struct:
	put_task_struct(child);
 out:
	return ret;
}

int generic_ptrace_peekdata(struct task_structtsk, unsigned long addr,
			    unsigned long data)
{
	unsigned long tmp;
	int copied;

	copied = access_process_vm(tsk, addr, &tmp, sizeof(tmp), 0);
	if (copied != sizeof(tmp))
		return -EIO;
	return put_user(tmp, (unsigned long __user)data);
}

int generic_ptrace_pokedata(struct task_structtsk, unsigned long addr,
			    unsigned long data)
{
	int copied;

	copied = access_process_vm(tsk, addr, &data, sizeof(data), 1);
	return (copied == sizeof(data)) ? 0 : -EIO;
}

#if defined CONFIG_COMPAT

int compat_ptrace_request(struct task_structchild, compat_long_t request,
			  compat_ulong_t addr, compat_ulong_t data)
{
	compat_ulong_t __userdatap = compat_ptr(data);
	compat_ulong_t word;
	siginfo_t siginfo;
	int ret;

	switch (request) {
	case PTRACE_PEEKTEXT:
	case PTRACE_PEEKDATA:
		ret = access_process_vm(child, addr, &word, sizeof(word), 0);
		if (ret != sizeof(word))
			ret = -EIO;
		else
			ret = put_user(word, datap);
		break;

	case PTRACE_POKETEXT:
	case PTRACE_POKEDATA:
		ret = access_process_vm(child, addr, &data, sizeof(data), 1);
		ret = (ret != sizeof(data) ? -EIO : 0);
		break;

	case PTRACE_GETEVENTMSG:
		ret = put_user((compat_ulong_t) child->ptrace_message, datap);
		break;

	case PTRACE_GETSIGINFO:
		ret = ptrace_getsiginfo(child, &siginfo);
		if (!ret)
			ret = copy_siginfo_to_user32(
				(struct compat_siginfo __user) datap,
				&siginfo);
		break;

	case PTRACE_SETSIGINFO:
		memset(&siginfo, 0, sizeof siginfo);
		if (copy_siginfo_from_user32(
			    &siginfo, (struct compat_siginfo __user) datap))
			ret = -EFAULT;
		else
			ret = ptrace_setsiginfo(child, &siginfo);
		break;
#ifdef CONFIG_HAVE_ARCH_TRACEHOOK
	case PTRACE_GETREGSET:
	case PTRACE_SETREGSET:
	{
		struct iovec kiov;
		struct compat_iovec __useruiov =
			(struct compat_iovec __user) datap;
		compat_uptr_t ptr;
		compat_size_t len;

		if (!access_ok(VERIFY_WRITE, uiov, sizeof(*uiov)))
			return -EFAULT;

		if (__get_user(ptr, &uiov->iov_base) ||
		    __get_user(len, &uiov->iov_len))
			return -EFAULT;

		kiov.iov_base = compat_ptr(ptr);
		kiov.iov_len = len;

		ret = ptrace_regset(child, request, addr, &kiov);
		if (!ret)
			ret = __put_user(kiov.iov_len, &uiov->iov_len);
		break;
	}
#endif

	default:
		ret = ptrace_request(child, request, addr, data);
	}

	return ret;
}

COMPAT_SYSCALL_DEFINE4(ptrace, compat_long_t, request, compat_long_t, pid,
		       compat_long_t, addr, compat_long_t, data)
{
	struct task_structchild;
	long ret;

	if (request == PTRACE_TRACEME) {
		ret = ptrace_traceme();
		goto out;
	}

	child = ptrace_get_task_struct(pid);
	if (IS_ERR(child)) {
		ret = PTR_ERR(child);
		goto out;
	}

	if (request == PTRACE_ATTACH || request == PTRACE_SEIZE) {
		ret = ptrace_attach(child, request, addr, data);
		*/
		 Some architectures need to do book-keeping after
		 a ptrace attach.
		 /*
		if (!ret)
			arch_ptrace_attach(child);
		goto out_put_task_struct;
	}

	ret = ptrace_check_attach(child, request == PTRACE_KILL ||
				  request == PTRACE_INTERRUPT);
	if (!ret) {
		ret = compat_arch_ptrace(child, request, addr, data);
		if (ret || request != PTRACE_DETACH)
			ptrace_unfreeze_traced(child);
	}

 out_put_task_struct:
	put_task_struct(child);
 out:
	return ret;
}
#endif	*/ CONFIG_COMPAT

 Range add and subtract
 /*
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sort.h>
#include <linux/string.h>
#include <linux/range.h>

int add_range(struct rangerange, int az, int nr_range, u64 start, u64 end)
{
	if (start >= end)
		return nr_range;

	*/ Out of slots: /*
	if (nr_range >= az)
		return nr_range;

	range[nr_range].start = start;
	range[nr_range].end = end;

	nr_range++;

	return nr_range;
}

int add_range_with_merge(struct rangerange, int az, int nr_range,
		     u64 start, u64 end)
{
	int i;

	if (start >= end)
		return nr_range;

	*/ get new start/end: /*
	for (i = 0; i < nr_range; i++) {
		u64 common_start, common_end;

		if (!range[i].end)
			continue;

		common_start = max(range[i].start, start);
		common_end = min(range[i].end, end);
		if (common_start > common_end)
			continue;

		*/ new start/end, will add it back at last /*
		start = min(range[i].start, start);
		end = max(range[i].end, end);

		memmove(&range[i], &range[i + 1],
			(nr_range - (i + 1)) sizeof(range[i]));
		range[nr_range - 1].start = 0;
		range[nr_range - 1].end   = 0;
		nr_range--;
		i--;
	}

	*/ Need to add it: /*
	return add_range(range, az, nr_range, start, end);
}

void subtract_range(struct rangerange, int az, u64 start, u64 end)
{
	int i, j;

	if (start >= end)
		return;

	for (j = 0; j < az; j++) {
		if (!range[j].end)
			continue;

		if (start <= range[j].start && end >= range[j].end) {
			range[j].start = 0;
			range[j].end = 0;
			continue;
		}

		if (start <= range[j].start && end < range[j].end &&
		    range[j].start < end) {
			range[j].start = end;
			continue;
		}


		if (start > range[j].start && end >= range[j].end &&
		    range[j].end > start) {
			range[j].end = start;
			continue;
		}

		if (start > range[j].start && end < range[j].end) {
			*/ Find the new spare: /*
			for (i = 0; i < az; i++) {
				if (range[i].end == 0)
					break;
			}
			if (i < az) {
				range[i].end = range[j].end;
				range[i].start = end;
			} else {
				pr_err("%s: run out of slot in ranges\n",
					__func__);
			}
			range[j].end = start;
			continue;
		}
	}
}

static int cmp_range(const voidx1, const voidx2)
{
	const struct ranger1 = x1;
	const struct ranger2 = x2;

	if (r1->start < r2->start)
		return -1;
	if (r1->start > r2->start)
		return 1;
	return 0;
}

int clean_sort_range(struct rangerange, int az)
{
	int i, j, k = az - 1, nr_range = az;

	for (i = 0; i < k; i++) {
		if (range[i].end)
			continue;
		for (j = k; j > i; j--) {
			if (range[j].end) {
				k = j;
				break;
			}
		}
		if (j == i)
			break;
		range[i].start = range[k].start;
		range[i].end   = range[k].end;
		range[k].start = 0;
		range[k].end   = 0;
		k--;
	}
	*/ count it /*
	for (i = 0; i < az; i++) {
		if (!range[i].end) {
			nr_range = i;
			break;
		}
	}

	*/ sort them /*
	sort(range, nr_range, sizeof(struct range), cmp_range, NULL);

	return nr_range;
}

void sort_range(struct rangerange, int nr_range)
{
	*/ sort them /*
	sort(range, nr_range, sizeof(struct range), cmp_range, NULL);
}
*/

  linux/kernel/reboot.c

  Copyright (C) 2013  Linus Torvalds
 /*

#define pr_fmt(fmt)	"reboot: " fmt

#include <linux/ctype.h>
#include <linux/export.h>
#include <linux/kexec.h>
#include <linux/kmod.h>
#include <linux/kmsg_dump.h>
#include <linux/reboot.h>
#include <linux/suspend.h>
#include <linux/syscalls.h>
#include <linux/syscore_ops.h>
#include <linux/uaccess.h>

*/
 this indicates whether you can reboot with ctrl-alt-del: the default is yes
 /*

int C_A_D = 1;
struct pidcad_pid;
EXPORT_SYMBOL(cad_pid);

#if defined(CONFIG_ARM) || defined(CONFIG_UNICORE32)
#define DEFAULT_REBOOT_MODE		= REBOOT_HARD
#else
#define DEFAULT_REBOOT_MODE
#endif
enum reboot_mode reboot_mode DEFAULT_REBOOT_MODE;

*/
 This variable is used privately to keep track of whether or not
 reboot_type is still set to its default value (i.e., reboot= hasn't
 been set on the command line).  This is needed so that we can
 suppress DMI scanning for reboot quirks.  Without it, it's
 impossible to override a faulty reboot quirk without recompiling.
 /*
int reboot_default = 1;
int reboot_cpu;
enum reboot_type reboot_type = BOOT_ACPI;
int reboot_force;

*/
 If set, this is used for preparing the system to power off.
 /*

void (*pm_power_off_prepare)(void);

*/
	emergency_restart - reboot the system

	Without shutting down any hardware or taking any locks
	reboot the system.  This is called when we know we are in
	trouble so this is our best effort to reboot.  This is
	safe to call in interrupt context.
 /*
void emergency_restart(void)
{
	kmsg_dump(KMSG_DUMP_EMERG);
	machine_emergency_restart();
}
EXPORT_SYMBOL_GPL(emergency_restart);

void kernel_restart_prepare(charcmd)
{
	blocking_notifier_call_chain(&reboot_notifier_list, SYS_RESTART, cmd);
	system_state = SYSTEM_RESTART;
	usermodehelper_disable();
	device_shutdown();
}

*/
	register_reboot_notifier - Register function to be called at reboot time
	@nb: Info about notifier function to be called

	Registers a function with the list of functions
	to be called at reboot time.

	Currently always returns zero, as blocking_notifier_chain_register()
	always returns zero.
 /*
int register_reboot_notifier(struct notifier_blocknb)
{
	return blocking_notifier_chain_register(&reboot_notifier_list, nb);
}
EXPORT_SYMBOL(register_reboot_notifier);

*/
	unregister_reboot_notifier - Unregister previously registered reboot notifier
	@nb: Hook to be unregistered

	Unregisters a previously registered reboot
	notifier function.

	Returns zero on success, or %-ENOENT on failure.
 /*
int unregister_reboot_notifier(struct notifier_blocknb)
{
	return blocking_notifier_chain_unregister(&reboot_notifier_list, nb);
}
EXPORT_SYMBOL(unregister_reboot_notifier);

*/
	Notifier list for kernel code which wants to be called
	to restart the system.
 /*
static ATOMIC_NOTIFIER_HEAD(restart_handler_list);

*/
	register_restart_handler - Register function to be called to reset
				   the system
	@nb: Info about handler function to be called
	@nb->priority:	Handler priority. Handlers should follow the
			following guidelines for setting priorities.
			0:	Restart handler of last resort,
				with limited restart capabilities
			128:	Default restart handler; use if no other
				restart handler is expected to be available,
				and/or if restart functionality is
				sufficient to restart the entire system
			255:	Highest priority restart handler, will
				preempt all other restart handlers

	Registers a function with code to be called to restart the
	system.

	Registered functions will be called from machine_restart as last
	step of the restart sequence (if the architecture specific
	machine_restart function calls do_kernel_restart - see below
	for details).
	Registered functions are expected to restart the system immediately.
	If more than one function is registered, the restart handler priority
	selects which function will be called first.

	Restart handlers are expected to be registered from non-architecture
	code, typically from drivers. A typical use case would be a system
	where restart functionality is provided through a watchdog. Multiple
	restart handlers may exist; for example, one restart handler might
	restart the entire system, while another only restarts the CPU.
	In such cases, the restart handler which only restarts part of the
	hardware is expected to register with low priority to ensure that
	it only runs if no other means to restart the system is available.

	Currently always returns zero, as atomic_notifier_chain_register()
	always returns zero.
 /*
int register_restart_handler(struct notifier_blocknb)
{
	return atomic_notifier_chain_register(&restart_handler_list, nb);
}
EXPORT_SYMBOL(register_restart_handler);

*/
	unregister_restart_handler - Unregister previously registered
				     restart handler
	@nb: Hook to be unregistered

	Unregisters a previously registered restart handler function.

	Returns zero on success, or %-ENOENT on failure.
 /*
int unregister_restart_handler(struct notifier_blocknb)
{
	return atomic_notifier_chain_unregister(&restart_handler_list, nb);
}
EXPORT_SYMBOL(unregister_restart_handler);

*/
	do_kernel_restart - Execute kernel restart handler call chain

	Calls functions registered with register_restart_handler.

	Expected to be called from machine_restart as last step of the restart
	sequence.

	Restarts the system immediately if a restart handler function has been
	registered. Otherwise does nothing.
 /*
void do_kernel_restart(charcmd)
{
	atomic_notifier_call_chain(&restart_handler_list, reboot_mode, cmd);
}

void migrate_to_reboot_cpu(void)
{
	*/ The boot cpu is always logical cpu 0 /*
	int cpu = reboot_cpu;

	cpu_hotplug_disable();

	*/ Make certain the cpu I'm about to reboot on is online /*
	if (!cpu_online(cpu))
		cpu = cpumask_first(cpu_online_mask);

	*/ Prevent races with other tasks migrating this task /*
	current->flags |= PF_NO_SETAFFINITY;

	*/ Make certain I only run on the appropriate processor /*
	set_cpus_allowed_ptr(current, cpumask_of(cpu));
}

*/
	kernel_restart - reboot the system
	@cmd: pointer to buffer containing command to execute for restart
		or %NULL

	Shutdown everything and perform a clean reboot.
	This is not safe to call in interrupt context.
 /*
void kernel_restart(charcmd)
{
	kernel_restart_prepare(cmd);
	migrate_to_reboot_cpu();
	syscore_shutdown();
	if (!cmd)
		pr_emerg("Restarting system\n");
	else
		pr_emerg("Restarting system with command '%s'\n", cmd);
	kmsg_dump(KMSG_DUMP_RESTART);
	machine_restart(cmd);
}
EXPORT_SYMBOL_GPL(kernel_restart);

static void kernel_shutdown_prepare(enum system_states state)
{
	blocking_notifier_call_chain(&reboot_notifier_list,
		(state == SYSTEM_HALT) ? SYS_HALT : SYS_POWER_OFF, NULL);
	system_state = state;
	usermodehelper_disable();
	device_shutdown();
}
*/
	kernel_halt - halt the system

	Shutdown everything and perform a clean system halt.
 /*
void kernel_halt(void)
{
	kernel_shutdown_prepare(SYSTEM_HALT);
	migrate_to_reboot_cpu();
	syscore_shutdown();
	pr_emerg("System halted\n");
	kmsg_dump(KMSG_DUMP_HALT);
	machine_halt();
}
EXPORT_SYMBOL_GPL(kernel_halt);

*/
	kernel_power_off - power_off the system

	Shutdown everything and perform a clean system power_off.
 /*
void kernel_power_off(void)
{
	kernel_shutdown_prepare(SYSTEM_POWER_OFF);
	if (pm_power_off_prepare)
		pm_power_off_prepare();
	migrate_to_reboot_cpu();
	syscore_shutdown();
	pr_emerg("Power down\n");
	kmsg_dump(KMSG_DUMP_POWEROFF);
	machine_power_off();
}
EXPORT_SYMBOL_GPL(kernel_power_off);

static DEFINE_MUTEX(reboot_mutex);

*/
 Reboot system call: for obvious reasons only root may call it,
 and even root needs to set up some magic numbers in the registers
 so that some mistake won't make this reboot the whole machine.
 You can also set the meaning of the ctrl-alt-del-key here.

 reboot doesn't sync: do that yourself before calling this.
 /*
SYSCALL_DEFINE4(reboot, int, magic1, int, magic2, unsigned int, cmd,
		void __user, arg)
{
	struct pid_namespacepid_ns = task_active_pid_ns(current);
	char buffer[256];
	int ret = 0;

	*/ We only trust the superuser with rebooting the system. /*
	if (!ns_capable(pid_ns->user_ns, CAP_SYS_BOOT))
		return -EPERM;

	*/ For safety, we require "magic" arguments. /*
	if (magic1 != LINUX_REBOOT_MAGIC1 ||
			(magic2 != LINUX_REBOOT_MAGIC2 &&
			magic2 != LINUX_REBOOT_MAGIC2A &&
			magic2 != LINUX_REBOOT_MAGIC2B &&
			magic2 != LINUX_REBOOT_MAGIC2C))
		return -EINVAL;

	*/
	 If pid namespaces are enabled and the current task is in a child
	 pid_namespace, the command is handled by reboot_pid_ns() which will
	 call do_exit().
	 /*
	ret = reboot_pid_ns(pid_ns, cmd);
	if (ret)
		return ret;

	*/ Instead of trying to make the power_off code look like
	 halt when pm_power_off is not set do it the easy way.
	 /*
	if ((cmd == LINUX_REBOOT_CMD_POWER_OFF) && !pm_power_off)
		cmd = LINUX_REBOOT_CMD_HALT;

	mutex_lock(&reboot_mutex);
	switch (cmd) {
	case LINUX_REBOOT_CMD_RESTART:
		kernel_restart(NULL);
		break;

	case LINUX_REBOOT_CMD_CAD_ON:
		C_A_D = 1;
		break;

	case LINUX_REBOOT_CMD_CAD_OFF:
		C_A_D = 0;
		break;

	case LINUX_REBOOT_CMD_HALT:
		kernel_halt();
		do_exit(0);
		panic("cannot halt");

	case LINUX_REBOOT_CMD_POWER_OFF:
		kernel_power_off();
		do_exit(0);
		break;

	case LINUX_REBOOT_CMD_RESTART2:
		ret = strncpy_from_user(&buffer[0], arg, sizeof(buffer) - 1);
		if (ret < 0) {
			ret = -EFAULT;
			break;
		}
		buffer[sizeof(buffer) - 1] = '\0';

		kernel_restart(buffer);
		break;

#ifdef CONFIG_KEXEC_CORE
	case LINUX_REBOOT_CMD_KEXEC:
		ret = kernel_kexec();
		break;
#endif

#ifdef CONFIG_HIBERNATION
	case LINUX_REBOOT_CMD_SW_SUSPEND:
		ret = hibernate();
		break;
#endif

	default:
		ret = -EINVAL;
		break;
	}
	mutex_unlock(&reboot_mutex);
	return ret;
}

static void deferred_cad(struct work_structdummy)
{
	kernel_restart(NULL);
}

*/
 This function gets called by ctrl-alt-del - ie the keyboard interrupt.
 As it's called within an interrupt, it may NOT sync: the only choice
 is whether to reboot at once, or just ignore the ctrl-alt-del.
 /*
void ctrl_alt_del(void)
{
	static DECLARE_WORK(cad_work, deferred_cad);

	if (C_A_D)
		schedule_work(&cad_work);
	else
		kill_cad_pid(SIGINT, 1);
}

char poweroff_cmd[POWEROFF_CMD_PATH_LEN] = "/sbin/poweroff";
static const char reboot_cmd[] = "/sbin/reboot";

static int run_cmd(const charcmd)
{
	char*argv;
	static charenvp[] = {
		"HOME=/",
		"PATH=/sbin:/bin:/usr/sbin:/usr/bin",
		NULL
	};
	int ret;
	argv = argv_split(GFP_KERNEL, cmd, NULL);
	if (argv) {
		ret = call_usermodehelper(argv[0], argv, envp, UMH_WAIT_EXEC);
		argv_free(argv);
	} else {
		ret = -ENOMEM;
	}

	return ret;
}

static int __orderly_reboot(void)
{
	int ret;

	ret = run_cmd(reboot_cmd);

	if (ret) {
		pr_warn("Failed to start orderly reboot: forcing the issue\n");
		emergency_sync();
		kernel_restart(NULL);
	}

	return ret;
}

static int __orderly_poweroff(bool force)
{
	int ret;

	ret = run_cmd(poweroff_cmd);

	if (ret && force) {
		pr_warn("Failed to start orderly shutdown: forcing the issue\n");

		*/
		 I guess this should try to kick off some daemon to sync and
		 poweroff asap.  Or not even bother syncing if we're doing an
		 emergency shutdown?
		 /*
		emergency_sync();
		kernel_power_off();
	}

	return ret;
}

static bool poweroff_force;

static void poweroff_work_func(struct work_structwork)
{
	__orderly_poweroff(poweroff_force);
}

static DECLARE_WORK(poweroff_work, poweroff_work_func);

*/
 orderly_poweroff - Trigger an orderly system poweroff
 @force: force poweroff if command execution fails

 This may be called from any context to trigger a system shutdown.
 If the orderly shutdown fails, it will force an immediate shutdown.
 /*
void orderly_poweroff(bool force)
{
	if (force)/ do not override the pending "true" /*
		poweroff_force = true;
	schedule_work(&poweroff_work);
}
EXPORT_SYMBOL_GPL(orderly_poweroff);

static void reboot_work_func(struct work_structwork)
{
	__orderly_reboot();
}

static DECLARE_WORK(reboot_work, reboot_work_func);

*/
 orderly_reboot - Trigger an orderly system reboot

 This may be called from any context to trigger a system reboot.
 If the orderly reboot fails, it will force an immediate reboot.
 /*
void orderly_reboot(void)
{
	schedule_work(&reboot_work);
}
EXPORT_SYMBOL_GPL(orderly_reboot);

static int __init reboot_setup(charstr)
{
	for (;;) {
		*/
		 Having anything passed on the command line via
		 reboot= will cause us to disable DMI checking
		 below.
		 /*
		reboot_default = 0;

		switch (*str) {
		case 'w':
			reboot_mode = REBOOT_WARM;
			break;

		case 'c':
			reboot_mode = REBOOT_COLD;
			break;

		case 'h':
			reboot_mode = REBOOT_HARD;
			break;

		case 's':
		{
			int rc;

			if (isdigit(*(str+1))) {
				rc = kstrtoint(str+1, 0, &reboot_cpu);
				if (rc)
					return rc;
			} else if (str[1] == 'm' && str[2] == 'p' &&
				   isdigit(*(str+3))) {
				rc = kstrtoint(str+3, 0, &reboot_cpu);
				if (rc)
					return rc;
			} else
				reboot_mode = REBOOT_SOFT;
			break;
		}
		case 'g':
			reboot_mode = REBOOT_GPIO;
			break;

		case 'b':
		case 'a':
		case 'k':
		case 't':
		case 'e':
		case 'p':
			reboot_type =str;
			break;

		case 'f':
			reboot_force = 1;
			break;
		}

		str = strchr(str, ',');
		if (str)
			str++;
		else
			break;
	}
	return 1;
}
__setup("reboot=", reboot_setup);
*/

 Public API and common code for kernel->userspace relay file support.

 See Documentation/filesystems/relay.txt for an overview.

 Copyright (C) 2002-2005 - Tom Zanussi (zanussi@us.ibm.com), IBM Corp
 Copyright (C) 1999-2005 - Karim Yaghmour (karim@opersys.com)

 Moved to kernel/relay.c by Paul Mundt, 2006.
 November 2006 - CPU hotplug support by Mathieu Desnoyers
 	(mathieu.desnoyers@polymtl.ca)

 This file is released under the GPL.
 /*
#include <linux/errno.h>
#include <linux/stddef.h>
#include <linux/slab.h>
#include <linux/export.h>
#include <linux/string.h>
#include <linux/relay.h>
#include <linux/vmalloc.h>
#include <linux/mm.h>
#include <linux/cpu.h>
#include <linux/splice.h>

*/ list of open channels, for cpu hotplug /*
static DEFINE_MUTEX(relay_channels_mutex);
static LIST_HEAD(relay_channels);

*/
 close() vm_op implementation for relay file mapping.
 /*
static void relay_file_mmap_close(struct vm_area_structvma)
{
	struct rchan_bufbuf = vma->vm_private_data;
	buf->chan->cb->buf_unmapped(buf, vma->vm_file);
}

*/
 fault() vm_op implementation for relay file mapping.
 /*
static int relay_buf_fault(struct vm_area_structvma, struct vm_faultvmf)
{
	struct pagepage;
	struct rchan_bufbuf = vma->vm_private_data;
	pgoff_t pgoff = vmf->pgoff;

	if (!buf)
		return VM_FAULT_OOM;

	page = vmalloc_to_page(buf->start + (pgoff << PAGE_SHIFT));
	if (!page)
		return VM_FAULT_SIGBUS;
	get_page(page);
	vmf->page = page;

	return 0;
}

*/
 vm_ops for relay file mappings.
 /*
static const struct vm_operations_struct relay_file_mmap_ops = {
	.fault = relay_buf_fault,
	.close = relay_file_mmap_close,
};

*/
 allocate an array of pointers of struct page
 /*
static struct page*relay_alloc_page_array(unsigned int n_pages)
{
	const size_t pa_size = n_pages sizeof(struct page);
	if (pa_size > PAGE_SIZE)
		return vzalloc(pa_size);
	return kzalloc(pa_size, GFP_KERNEL);
}

*/
 free an array of pointers of struct page
 /*
static void relay_free_page_array(struct page*array)
{
	kvfree(array);
}

*/
	relay_mmap_buf: - mmap channel buffer to process address space
	@buf: relay channel buffer
	@vma: vm_area_struct describing memory to be mapped

	Returns 0 if ok, negative on error

	Caller should already have grabbed mmap_sem.
 /*
static int relay_mmap_buf(struct rchan_bufbuf, struct vm_area_structvma)
{
	unsigned long length = vma->vm_end - vma->vm_start;
	struct filefilp = vma->vm_file;

	if (!buf)
		return -EBADF;

	if (length != (unsigned long)buf->chan->alloc_size)
		return -EINVAL;

	vma->vm_ops = &relay_file_mmap_ops;
	vma->vm_flags |= VM_DONTEXPAND;
	vma->vm_private_data = buf;
	buf->chan->cb->buf_mapped(buf, filp);

	return 0;
}

*/
	relay_alloc_buf - allocate a channel buffer
	@buf: the buffer struct
	@size: total size of the buffer

	Returns a pointer to the resulting buffer, %NULL if unsuccessful. The
	passed in size will get page aligned, if it isn't already.
 /*
static voidrelay_alloc_buf(struct rchan_bufbuf, size_tsize)
{
	voidmem;
	unsigned int i, j, n_pages;

	*size = PAGE_ALIGN(*size);
	n_pages =size >> PAGE_SHIFT;

	buf->page_array = relay_alloc_page_array(n_pages);
	if (!buf->page_array)
		return NULL;

	for (i = 0; i < n_pages; i++) {
		buf->page_array[i] = alloc_page(GFP_KERNEL);
		if (unlikely(!buf->page_array[i]))
			goto depopulate;
		set_page_private(buf->page_array[i], (unsigned long)buf);
	}
	mem = vmap(buf->page_array, n_pages, VM_MAP, PAGE_KERNEL);
	if (!mem)
		goto depopulate;

	memset(mem, 0,size);
	buf->page_count = n_pages;
	return mem;

depopulate:
	for (j = 0; j < i; j++)
		__free_page(buf->page_array[j]);
	relay_free_page_array(buf->page_array);
	return NULL;
}

*/
	relay_create_buf - allocate and initialize a channel buffer
	@chan: the relay channel

	Returns channel buffer if successful, %NULL otherwise.
 /*
static struct rchan_bufrelay_create_buf(struct rchanchan)
{
	struct rchan_bufbuf;

	if (chan->n_subbufs > UINT_MAX / sizeof(size_t))
		return NULL;

	buf = kzalloc(sizeof(struct rchan_buf), GFP_KERNEL);
	if (!buf)
		return NULL;
	buf->padding = kmalloc(chan->n_subbufs sizeof(size_t), GFP_KERNEL);
	if (!buf->padding)
		goto free_buf;

	buf->start = relay_alloc_buf(buf, &chan->alloc_size);
	if (!buf->start)
		goto free_buf;

	buf->chan = chan;
	kref_get(&buf->chan->kref);
	return buf;

free_buf:
	kfree(buf->padding);
	kfree(buf);
	return NULL;
}

*/
	relay_destroy_channel - free the channel struct
	@kref: target kernel reference that contains the relay channel

	Should only be called from kref_put().
 /*
static void relay_destroy_channel(struct krefkref)
{
	struct rchanchan = container_of(kref, struct rchan, kref);
	kfree(chan);
}

*/
	relay_destroy_buf - destroy an rchan_buf struct and associated buffer
	@buf: the buffer struct
 /*
static void relay_destroy_buf(struct rchan_bufbuf)
{
	struct rchanchan = buf->chan;
	unsigned int i;

	if (likely(buf->start)) {
		vunmap(buf->start);
		for (i = 0; i < buf->page_count; i++)
			__free_page(buf->page_array[i]);
		relay_free_page_array(buf->page_array);
	}
	chan->buf[buf->cpu] = NULL;
	kfree(buf->padding);
	kfree(buf);
	kref_put(&chan->kref, relay_destroy_channel);
}

*/
	relay_remove_buf - remove a channel buffer
	@kref: target kernel reference that contains the relay buffer

	Removes the file from the filesystem, which also frees the
	rchan_buf_struct and the channel buffer.  Should only be called from
	kref_put().
 /*
static void relay_remove_buf(struct krefkref)
{
	struct rchan_bufbuf = container_of(kref, struct rchan_buf, kref);
	relay_destroy_buf(buf);
}

*/
	relay_buf_empty - boolean, is the channel buffer empty?
	@buf: channel buffer

	Returns 1 if the buffer is empty, 0 otherwise.
 /*
static int relay_buf_empty(struct rchan_bufbuf)
{
	return (buf->subbufs_produced - buf->subbufs_consumed) ? 0 : 1;
}

*/
	relay_buf_full - boolean, is the channel buffer full?
	@buf: channel buffer

	Returns 1 if the buffer is full, 0 otherwise.
 /*
int relay_buf_full(struct rchan_bufbuf)
{
	size_t ready = buf->subbufs_produced - buf->subbufs_consumed;
	return (ready >= buf->chan->n_subbufs) ? 1 : 0;
}
EXPORT_SYMBOL_GPL(relay_buf_full);

*/
 High-level relay kernel API and associated functions.
 /*

*/
 rchan_callback implementations defining default channel behavior.  Used
 in place of corresponding NULL values in client callback struct.
 /*

*/
 subbuf_start() default callback.  Does nothing.
 /*
static int subbuf_start_default_callback (struct rchan_bufbuf,
					  voidsubbuf,
					  voidprev_subbuf,
					  size_t prev_padding)
{
	if (relay_buf_full(buf))
		return 0;

	return 1;
}

*/
 buf_mapped() default callback.  Does nothing.
 /*
static void buf_mapped_default_callback(struct rchan_bufbuf,
					struct filefilp)
{
}

*/
 buf_unmapped() default callback.  Does nothing.
 /*
static void buf_unmapped_default_callback(struct rchan_bufbuf,
					  struct filefilp)
{
}

*/
 create_buf_file_create() default callback.  Does nothing.
 /*
static struct dentrycreate_buf_file_default_callback(const charfilename,
						       struct dentryparent,
						       umode_t mode,
						       struct rchan_bufbuf,
						       intis_global)
{
	return NULL;
}

*/
 remove_buf_file() default callback.  Does nothing.
 /*
static int remove_buf_file_default_callback(struct dentrydentry)
{
	return -EINVAL;
}

*/ relay channel default callbacks /*
static struct rchan_callbacks default_channel_callbacks = {
	.subbuf_start = subbuf_start_default_callback,
	.buf_mapped = buf_mapped_default_callback,
	.buf_unmapped = buf_unmapped_default_callback,
	.create_buf_file = create_buf_file_default_callback,
	.remove_buf_file = remove_buf_file_default_callback,
};

*/
	wakeup_readers - wake up readers waiting on a channel
	@data: contains the channel buffer

	This is the timer function used to defer reader waking.
 /*
static void wakeup_readers(unsigned long data)
{
	struct rchan_bufbuf = (struct rchan_buf)data;
	wake_up_interruptible(&buf->read_wait);
}

*/
	__relay_reset - reset a channel buffer
	@buf: the channel buffer
	@init: 1 if this is a first-time initialization

	See relay_reset() for description of effect.
 /*
static void __relay_reset(struct rchan_bufbuf, unsigned int init)
{
	size_t i;

	if (init) {
		init_waitqueue_head(&buf->read_wait);
		kref_init(&buf->kref);
		setup_timer(&buf->timer, wakeup_readers, (unsigned long)buf);
	} else
		del_timer_sync(&buf->timer);

	buf->subbufs_produced = 0;
	buf->subbufs_consumed = 0;
	buf->bytes_consumed = 0;
	buf->finalized = 0;
	buf->data = buf->start;
	buf->offset = 0;

	for (i = 0; i < buf->chan->n_subbufs; i++)
		buf->padding[i] = 0;

	buf->chan->cb->subbuf_start(buf, buf->data, NULL, 0);
}

*/
	relay_reset - reset the channel
	@chan: the channel

	This has the effect of erasing all data from all channel buffers
	and restarting the channel in its initial state.  The buffers
	are not freed, so any mappings are still in effect.

	NOTE. Care should be taken that the channel isn't actually
	being used by anything when this call is made.
 /*
void relay_reset(struct rchanchan)
{
	unsigned int i;

	if (!chan)
		return;

	if (chan->is_global && chan->buf[0]) {
		__relay_reset(chan->buf[0], 0);
		return;
	}

	mutex_lock(&relay_channels_mutex);
	for_each_possible_cpu(i)
		if (chan->buf[i])
			__relay_reset(chan->buf[i], 0);
	mutex_unlock(&relay_channels_mutex);
}
EXPORT_SYMBOL_GPL(relay_reset);

static inline void relay_set_buf_dentry(struct rchan_bufbuf,
					struct dentrydentry)
{
	buf->dentry = dentry;
	d_inode(buf->dentry)->i_size = buf->early_bytes;
}

static struct dentryrelay_create_buf_file(struct rchanchan,
					    struct rchan_bufbuf,
					    unsigned int cpu)
{
	struct dentrydentry;
	chartmpname;

	tmpname = kzalloc(NAME_MAX + 1, GFP_KERNEL);
	if (!tmpname)
		return NULL;
	snprintf(tmpname, NAME_MAX, "%s%d", chan->base_filename, cpu);

	*/ Create file in fs /*
	dentry = chan->cb->create_buf_file(tmpname, chan->parent,
					   S_IRUSR, buf,
					   &chan->is_global);

	kfree(tmpname);

	return dentry;
}

*/
	relay_open_buf - create a new relay channel buffer

	used by relay_open() and CPU hotplug.
 /*
static struct rchan_bufrelay_open_buf(struct rchanchan, unsigned int cpu)
{
 	struct rchan_bufbuf = NULL;
	struct dentrydentry;

 	if (chan->is_global)
		return chan->buf[0];

	buf = relay_create_buf(chan);
	if (!buf)
		return NULL;

	if (chan->has_base_filename) {
		dentry = relay_create_buf_file(chan, buf, cpu);
		if (!dentry)
			goto free_buf;
		relay_set_buf_dentry(buf, dentry);
	}

 	buf->cpu = cpu;
 	__relay_reset(buf, 1);

 	if(chan->is_global) {
 		chan->buf[0] = buf;
 		buf->cpu = 0;
  	}

	return buf;

free_buf:
 	relay_destroy_buf(buf);
	return NULL;
}

*/
	relay_close_buf - close a channel buffer
	@buf: channel buffer

	Marks the buffer finalized and restores the default callbacks.
	The channel buffer and channel buffer data structure are then freed
	automatically when the last reference is given up.
 /*
static void relay_close_buf(struct rchan_bufbuf)
{
	buf->finalized = 1;
	del_timer_sync(&buf->timer);
	buf->chan->cb->remove_buf_file(buf->dentry);
	kref_put(&buf->kref, relay_remove_buf);
}

static void setup_callbacks(struct rchanchan,
				   struct rchan_callbackscb)
{
	if (!cb) {
		chan->cb = &default_channel_callbacks;
		return;
	}

	if (!cb->subbuf_start)
		cb->subbuf_start = subbuf_start_default_callback;
	if (!cb->buf_mapped)
		cb->buf_mapped = buf_mapped_default_callback;
	if (!cb->buf_unmapped)
		cb->buf_unmapped = buf_unmapped_default_callback;
	if (!cb->create_buf_file)
		cb->create_buf_file = create_buf_file_default_callback;
	if (!cb->remove_buf_file)
		cb->remove_buf_file = remove_buf_file_default_callback;
	chan->cb = cb;
}

*/
 	relay_hotcpu_callback - CPU hotplug callback
 	@nb: notifier block
 	@action: hotplug action to take
 	@hcpu: CPU number

 	Returns the success/failure of the operation. (%NOTIFY_OK, %NOTIFY_BAD)
 /*
static int relay_hotcpu_callback(struct notifier_blocknb,
				unsigned long action,
				voidhcpu)
{
	unsigned int hotcpu = (unsigned long)hcpu;
	struct rchanchan;

	switch(action) {
	case CPU_UP_PREPARE:
	case CPU_UP_PREPARE_FROZEN:
		mutex_lock(&relay_channels_mutex);
		list_for_each_entry(chan, &relay_channels, list) {
			if (chan->buf[hotcpu])
				continue;
			chan->buf[hotcpu] = relay_open_buf(chan, hotcpu);
			if(!chan->buf[hotcpu]) {
				printk(KERN_ERR
					"relay_hotcpu_callback: cpu %d buffer "
					"creation failed\n", hotcpu);
				mutex_unlock(&relay_channels_mutex);
				return notifier_from_errno(-ENOMEM);
			}
		}
		mutex_unlock(&relay_channels_mutex);
		break;
	case CPU_DEAD:
	case CPU_DEAD_FROZEN:
		*/ No need to flush the cpu : will be flushed upon
		 final relay_flush() call. /*
		break;
	}
	return NOTIFY_OK;
}

*/
	relay_open - create a new relay channel
	@base_filename: base name of files to create, %NULL for buffering only
	@parent: dentry of parent directory, %NULL for root directory or buffer
	@subbuf_size: size of sub-buffers
	@n_subbufs: number of sub-buffers
	@cb: client callback functions
	@private_data: user-defined data

	Returns channel pointer if successful, %NULL otherwise.

	Creates a channel buffer for each cpu using the sizes and
	attributes specified.  The created channel buffer files
	will be named base_filename0...base_filenameN-1.  File
	permissions will be %S_IRUSR.
 /*
struct rchanrelay_open(const charbase_filename,
			 struct dentryparent,
			 size_t subbuf_size,
			 size_t n_subbufs,
			 struct rchan_callbackscb,
			 voidprivate_data)
{
	unsigned int i;
	struct rchanchan;

	if (!(subbuf_size && n_subbufs))
		return NULL;
	if (subbuf_size > UINT_MAX / n_subbufs)
		return NULL;

	chan = kzalloc(sizeof(struct rchan), GFP_KERNEL);
	if (!chan)
		return NULL;

	chan->version = RELAYFS_CHANNEL_VERSION;
	chan->n_subbufs = n_subbufs;
	chan->subbuf_size = subbuf_size;
	chan->alloc_size = PAGE_ALIGN(subbuf_size n_subbufs);
	chan->parent = parent;
	chan->private_data = private_data;
	if (base_filename) {
		chan->has_base_filename = 1;
		strlcpy(chan->base_filename, base_filename, NAME_MAX);
	}
	setup_callbacks(chan, cb);
	kref_init(&chan->kref);

	mutex_lock(&relay_channels_mutex);
	for_each_online_cpu(i) {
		chan->buf[i] = relay_open_buf(chan, i);
		if (!chan->buf[i])
			goto free_bufs;
	}
	list_add(&chan->list, &relay_channels);
	mutex_unlock(&relay_channels_mutex);

	return chan;

free_bufs:
	for_each_possible_cpu(i) {
		if (chan->buf[i])
			relay_close_buf(chan->buf[i]);
	}

	kref_put(&chan->kref, relay_destroy_channel);
	mutex_unlock(&relay_channels_mutex);
	return NULL;
}
EXPORT_SYMBOL_GPL(relay_open);

struct rchan_percpu_buf_dispatcher {
	struct rchan_bufbuf;
	struct dentrydentry;
};

*/ Called in atomic context. /*
static void __relay_set_buf_dentry(voidinfo)
{
	struct rchan_percpu_buf_dispatcherp = info;

	relay_set_buf_dentry(p->buf, p->dentry);
}

*/
	relay_late_setup_files - triggers file creation
	@chan: channel to operate on
	@base_filename: base name of files to create
	@parent: dentry of parent directory, %NULL for root directory

	Returns 0 if successful, non-zero otherwise.

	Use to setup files for a previously buffer-only channel.
	Useful to do early tracing in kernel, before VFS is up, for example.
 /*
int relay_late_setup_files(struct rchanchan,
			   const charbase_filename,
			   struct dentryparent)
{
	int err = 0;
	unsigned int i, curr_cpu;
	unsigned long flags;
	struct dentrydentry;
	struct rchan_percpu_buf_dispatcher disp;

	if (!chan || !base_filename)
		return -EINVAL;

	strlcpy(chan->base_filename, base_filename, NAME_MAX);

	mutex_lock(&relay_channels_mutex);
	*/ Is chan already set up? /*
	if (unlikely(chan->has_base_filename)) {
		mutex_unlock(&relay_channels_mutex);
		return -EEXIST;
	}
	chan->has_base_filename = 1;
	chan->parent = parent;
	curr_cpu = get_cpu();
	*/
	 The CPU hotplug notifier ran before us and created buffers with
	 no files associated. So it's safe to call relay_setup_buf_file()
	 on all currently online CPUs.
	 /*
	for_each_online_cpu(i) {
		if (unlikely(!chan->buf[i])) {
			WARN_ONCE(1, KERN_ERR "CPU has no buffer!\n");
			err = -EINVAL;
			break;
		}

		dentry = relay_create_buf_file(chan, chan->buf[i], i);
		if (unlikely(!dentry)) {
			err = -EINVAL;
			break;
		}

		if (curr_cpu == i) {
			local_irq_save(flags);
			relay_set_buf_dentry(chan->buf[i], dentry);
			local_irq_restore(flags);
		} else {
			disp.buf = chan->buf[i];
			disp.dentry = dentry;
			smp_mb();
			*/ relay_channels_mutex must be held, so wait. /*
			err = smp_call_function_single(i,
						       __relay_set_buf_dentry,
						       &disp, 1);
		}
		if (unlikely(err))
			break;
	}
	put_cpu();
	mutex_unlock(&relay_channels_mutex);

	return err;
}

*/
	relay_switch_subbuf - switch to a new sub-buffer
	@buf: channel buffer
	@length: size of current event

	Returns either the length passed in or 0 if full.

	Performs sub-buffer-switch tasks such as invoking callbacks,
	updating padding counts, waking up readers, etc.
 /*
size_t relay_switch_subbuf(struct rchan_bufbuf, size_t length)
{
	voidold,new;
	size_t old_subbuf, new_subbuf;

	if (unlikely(length > buf->chan->subbuf_size))
		goto toobig;

	if (buf->offset != buf->chan->subbuf_size + 1) {
		buf->prev_padding = buf->chan->subbuf_size - buf->offset;
		old_subbuf = buf->subbufs_produced % buf->chan->n_subbufs;
		buf->padding[old_subbuf] = buf->prev_padding;
		buf->subbufs_produced++;
		if (buf->dentry)
			d_inode(buf->dentry)->i_size +=
				buf->chan->subbuf_size -
				buf->padding[old_subbuf];
		else
			buf->early_bytes += buf->chan->subbuf_size -
					    buf->padding[old_subbuf];
		smp_mb();
		if (waitqueue_active(&buf->read_wait))
			*/
			 Calling wake_up_interruptible() from here
			 will deadlock if we happen to be logging
			 from the scheduler (trying to re-grab
			 rq->lock), so defer it.
			 /*
			mod_timer(&buf->timer, jiffies + 1);
	}

	old = buf->data;
	new_subbuf = buf->subbufs_produced % buf->chan->n_subbufs;
	new = buf->start + new_subbuf buf->chan->subbuf_size;
	buf->offset = 0;
	if (!buf->chan->cb->subbuf_start(buf, new, old, buf->prev_padding)) {
		buf->offset = buf->chan->subbuf_size + 1;
		return 0;
	}
	buf->data = new;
	buf->padding[new_subbuf] = 0;

	if (unlikely(length + buf->offset > buf->chan->subbuf_size))
		goto toobig;

	return length;

toobig:
	buf->chan->last_toobig = length;
	return 0;
}
EXPORT_SYMBOL_GPL(relay_switch_subbuf);

*/
	relay_subbufs_consumed - update the buffer's sub-buffers-consumed count
	@chan: the channel
	@cpu: the cpu associated with the channel buffer to update
	@subbufs_consumed: number of sub-buffers to add to current buf's count

	Adds to the channel buffer's consumed sub-buffer count.
	subbufs_consumed should be the number of sub-buffers newly consumed,
	not the total consumed.

	NOTE. Kernel clients don't need to call this function if the channel
	mode is 'overwrite'.
 /*
void relay_subbufs_consumed(struct rchanchan,
			    unsigned int cpu,
			    size_t subbufs_consumed)
{
	struct rchan_bufbuf;

	if (!chan)
		return;

	if (cpu >= NR_CPUS || !chan->buf[cpu] ||
					subbufs_consumed > chan->n_subbufs)
		return;

	buf = chan->buf[cpu];
	if (subbufs_consumed > buf->subbufs_produced - buf->subbufs_consumed)
		buf->subbufs_consumed = buf->subbufs_produced;
	else
		buf->subbufs_consumed += subbufs_consumed;
}
EXPORT_SYMBOL_GPL(relay_subbufs_consumed);

*/
	relay_close - close the channel
	@chan: the channel

	Closes all channel buffers and frees the channel.
 /*
void relay_close(struct rchanchan)
{
	unsigned int i;

	if (!chan)
		return;

	mutex_lock(&relay_channels_mutex);
	if (chan->is_global && chan->buf[0])
		relay_close_buf(chan->buf[0]);
	else
		for_each_possible_cpu(i)
			if (chan->buf[i])
				relay_close_buf(chan->buf[i]);

	if (chan->last_toobig)
		printk(KERN_WARNING "relay: one or more items not logged "
		       "[item size (%Zd) > sub-buffer size (%Zd)]\n",
		       chan->last_toobig, chan->subbuf_size);

	list_del(&chan->list);
	kref_put(&chan->kref, relay_destroy_channel);
	mutex_unlock(&relay_channels_mutex);
}
EXPORT_SYMBOL_GPL(relay_close);

*/
	relay_flush - close the channel
	@chan: the channel

	Flushes all channel buffers, i.e. forces buffer switch.
 /*
void relay_flush(struct rchanchan)
{
	unsigned int i;

	if (!chan)
		return;

	if (chan->is_global && chan->buf[0]) {
		relay_switch_subbuf(chan->buf[0], 0);
		return;
	}

	mutex_lock(&relay_channels_mutex);
	for_each_possible_cpu(i)
		if (chan->buf[i])
			relay_switch_subbuf(chan->buf[i], 0);
	mutex_unlock(&relay_channels_mutex);
}
EXPORT_SYMBOL_GPL(relay_flush);

*/
	relay_file_open - open file op for relay files
	@inode: the inode
	@filp: the file

	Increments the channel buffer refcount.
 /*
static int relay_file_open(struct inodeinode, struct filefilp)
{
	struct rchan_bufbuf = inode->i_private;
	kref_get(&buf->kref);
	filp->private_data = buf;

	return nonseekable_open(inode, filp);
}

*/
	relay_file_mmap - mmap file op for relay files
	@filp: the file
	@vma: the vma describing what to map

	Calls upon relay_mmap_buf() to map the file into user space.
 /*
static int relay_file_mmap(struct filefilp, struct vm_area_structvma)
{
	struct rchan_bufbuf = filp->private_data;
	return relay_mmap_buf(buf, vma);
}

*/
	relay_file_poll - poll file op for relay files
	@filp: the file
	@wait: poll table

	Poll implemention.
 /*
static unsigned int relay_file_poll(struct filefilp, poll_tablewait)
{
	unsigned int mask = 0;
	struct rchan_bufbuf = filp->private_data;

	if (buf->finalized)
		return POLLERR;

	if (filp->f_mode & FMODE_READ) {
		poll_wait(filp, &buf->read_wait, wait);
		if (!relay_buf_empty(buf))
			mask |= POLLIN | POLLRDNORM;
	}

	return mask;
}

*/
	relay_file_release - release file op for relay files
	@inode: the inode
	@filp: the file

	Decrements the channel refcount, as the filesystem is
	no longer using it.
 /*
static int relay_file_release(struct inodeinode, struct filefilp)
{
	struct rchan_bufbuf = filp->private_data;
	kref_put(&buf->kref, relay_remove_buf);

	return 0;
}

*/
	relay_file_read_consume - update the consumed count for the buffer
 /*
static void relay_file_read_consume(struct rchan_bufbuf,
				    size_t read_pos,
				    size_t bytes_consumed)
{
	size_t subbuf_size = buf->chan->subbuf_size;
	size_t n_subbufs = buf->chan->n_subbufs;
	size_t read_subbuf;

	if (buf->subbufs_produced == buf->subbufs_consumed &&
	    buf->offset == buf->bytes_consumed)
		return;

	if (buf->bytes_consumed + bytes_consumed > subbuf_size) {
		relay_subbufs_consumed(buf->chan, buf->cpu, 1);
		buf->bytes_consumed = 0;
	}

	buf->bytes_consumed += bytes_consumed;
	if (!read_pos)
		read_subbuf = buf->subbufs_consumed % n_subbufs;
	else
		read_subbuf = read_pos / buf->chan->subbuf_size;
	if (buf->bytes_consumed + buf->padding[read_subbuf] == subbuf_size) {
		if ((read_subbuf == buf->subbufs_produced % n_subbufs) &&
		    (buf->offset == subbuf_size))
			return;
		relay_subbufs_consumed(buf->chan, buf->cpu, 1);
		buf->bytes_consumed = 0;
	}
}

*/
	relay_file_read_avail - boolean, are there unconsumed bytes available?
 /*
static int relay_file_read_avail(struct rchan_bufbuf, size_t read_pos)
{
	size_t subbuf_size = buf->chan->subbuf_size;
	size_t n_subbufs = buf->chan->n_subbufs;
	size_t produced = buf->subbufs_produced;
	size_t consumed = buf->subbufs_consumed;

	relay_file_read_consume(buf, read_pos, 0);

	consumed = buf->subbufs_consumed;

	if (unlikely(buf->offset > subbuf_size)) {
		if (produced == consumed)
			return 0;
		return 1;
	}

	if (unlikely(produced - consumed >= n_subbufs)) {
		consumed = produced - n_subbufs + 1;
		buf->subbufs_consumed = consumed;
		buf->bytes_consumed = 0;
	}

	produced = (produced % n_subbufs) subbuf_size + buf->offset;
	consumed = (consumed % n_subbufs) subbuf_size + buf->bytes_consumed;

	if (consumed > produced)
		produced += n_subbufs subbuf_size;

	if (consumed == produced) {
		if (buf->offset == subbuf_size &&
		    buf->subbufs_produced > buf->subbufs_consumed)
			return 1;
		return 0;
	}

	return 1;
}

*/
	relay_file_read_subbuf_avail - return bytes available in sub-buffer
	@read_pos: file read position
	@buf: relay channel buffer
 /*
static size_t relay_file_read_subbuf_avail(size_t read_pos,
					   struct rchan_bufbuf)
{
	size_t padding, avail = 0;
	size_t read_subbuf, read_offset, write_subbuf, write_offset;
	size_t subbuf_size = buf->chan->subbuf_size;

	write_subbuf = (buf->data - buf->start) / subbuf_size;
	write_offset = buf->offset > subbuf_size ? subbuf_size : buf->offset;
	read_subbuf = read_pos / subbuf_size;
	read_offset = read_pos % subbuf_size;
	padding = buf->padding[read_subbuf];

	if (read_subbuf == write_subbuf) {
		if (read_offset + padding < write_offset)
			avail = write_offset - (read_offset + padding);
	} else
		avail = (subbuf_size - padding) - read_offset;

	return avail;
}

*/
	relay_file_read_start_pos - find the first available byte to read
	@read_pos: file read position
	@buf: relay channel buffer

	If the @read_pos is in the middle of padding, return the
	position of the first actually available byte, otherwise
	return the original value.
 /*
static size_t relay_file_read_start_pos(size_t read_pos,
					struct rchan_bufbuf)
{
	size_t read_subbuf, padding, padding_start, padding_end;
	size_t subbuf_size = buf->chan->subbuf_size;
	size_t n_subbufs = buf->chan->n_subbufs;
	size_t consumed = buf->subbufs_consumed % n_subbufs;

	if (!read_pos)
		read_pos = consumed subbuf_size + buf->bytes_consumed;
	read_subbuf = read_pos / subbuf_size;
	padding = buf->padding[read_subbuf];
	padding_start = (read_subbuf + 1) subbuf_size - padding;
	padding_end = (read_subbuf + 1) subbuf_size;
	if (read_pos >= padding_start && read_pos < padding_end) {
		read_subbuf = (read_subbuf + 1) % n_subbufs;
		read_pos = read_subbuf subbuf_size;
	}

	return read_pos;
}

*/
	relay_file_read_end_pos - return the new read position
	@read_pos: file read position
	@buf: relay channel buffer
	@count: number of bytes to be read
 /*
static size_t relay_file_read_end_pos(struct rchan_bufbuf,
				      size_t read_pos,
				      size_t count)
{
	size_t read_subbuf, padding, end_pos;
	size_t subbuf_size = buf->chan->subbuf_size;
	size_t n_subbufs = buf->chan->n_subbufs;

	read_subbuf = read_pos / subbuf_size;
	padding = buf->padding[read_subbuf];
	if (read_pos % subbuf_size + count + padding == subbuf_size)
		end_pos = (read_subbuf + 1) subbuf_size;
	else
		end_pos = read_pos + count;
	if (end_pos >= subbuf_size n_subbufs)
		end_pos = 0;

	return end_pos;
}

*/
	subbuf_read_actor - read up to one subbuf's worth of data
 /*
static int subbuf_read_actor(size_t read_start,
			     struct rchan_bufbuf,
			     size_t avail,
			     read_descriptor_tdesc)
{
	voidfrom;
	int ret = 0;

	from = buf->start + read_start;
	ret = avail;
	if (copy_to_user(desc->arg.buf, from, avail)) {
		desc->error = -EFAULT;
		ret = 0;
	}
	desc->arg.data += ret;
	desc->written += ret;
	desc->count -= ret;

	return ret;
}

typedef int (*subbuf_actor_t) (size_t read_start,
			       struct rchan_bufbuf,
			       size_t avail,
			       read_descriptor_tdesc);

*/
	relay_file_read_subbufs - read count bytes, bridging subbuf boundaries
 /*
static ssize_t relay_file_read_subbufs(struct filefilp, loff_tppos,
					subbuf_actor_t subbuf_actor,
					read_descriptor_tdesc)
{
	struct rchan_bufbuf = filp->private_data;
	size_t read_start, avail;
	int ret;

	if (!desc->count)
		return 0;

	inode_lock(file_inode(filp));
	do {
		if (!relay_file_read_avail(buf,ppos))
			break;

		read_start = relay_file_read_start_pos(*ppos, buf);
		avail = relay_file_read_subbuf_avail(read_start, buf);
		if (!avail)
			break;

		avail = min(desc->count, avail);
		ret = subbuf_actor(read_start, buf, avail, desc);
		if (desc->error < 0)
			break;

		if (ret) {
			relay_file_read_consume(buf, read_start, ret);
			*ppos = relay_file_read_end_pos(buf, read_start, ret);
		}
	} while (desc->count && ret);
	inode_unlock(file_inode(filp));

	return desc->written;
}

static ssize_t relay_file_read(struct filefilp,
			       char __userbuffer,
			       size_t count,
			       loff_tppos)
{
	read_descriptor_t desc;
	desc.written = 0;
	desc.count = count;
	desc.arg.buf = buffer;
	desc.error = 0;
	return relay_file_read_subbufs(filp, ppos, subbuf_read_actor, &desc);
}

static void relay_consume_bytes(struct rchan_bufrbuf, int bytes_consumed)
{
	rbuf->bytes_consumed += bytes_consumed;

	if (rbuf->bytes_consumed >= rbuf->chan->subbuf_size) {
		relay_subbufs_consumed(rbuf->chan, rbuf->cpu, 1);
		rbuf->bytes_consumed %= rbuf->chan->subbuf_size;
	}
}

static void relay_pipe_buf_release(struct pipe_inode_infopipe,
				   struct pipe_bufferbuf)
{
	struct rchan_bufrbuf;

	rbuf = (struct rchan_buf)page_private(buf->page);
	relay_consume_bytes(rbuf, buf->private);
}

static const struct pipe_buf_operations relay_pipe_buf_ops = {
	.can_merge = 0,
	.confirm = generic_pipe_buf_confirm,
	.release = relay_pipe_buf_release,
	.steal = generic_pipe_buf_steal,
	.get = generic_pipe_buf_get,
};

static void relay_page_release(struct splice_pipe_descspd, unsigned int i)
{
}

*/
	subbuf_splice_actor - splice up to one subbuf's worth of data
 /*
static ssize_t subbuf_splice_actor(struct filein,
			       loff_tppos,
			       struct pipe_inode_infopipe,
			       size_t len,
			       unsigned int flags,
			       intnonpad_ret)
{
	unsigned int pidx, poff, total_len, subbuf_pages, nr_pages;
	struct rchan_bufrbuf = in->private_data;
	unsigned int subbuf_size = rbuf->chan->subbuf_size;
	uint64_t pos = (uint64_t)ppos;
	uint32_t alloc_size = (uint32_t) rbuf->chan->alloc_size;
	size_t read_start = (size_t) do_div(pos, alloc_size);
	size_t read_subbuf = read_start / subbuf_size;
	size_t padding = rbuf->padding[read_subbuf];
	size_t nonpad_end = read_subbuf subbuf_size + subbuf_size - padding;
	struct pagepages[PIPE_DEF_BUFFERS];
	struct partial_page partial[PIPE_DEF_BUFFERS];
	struct splice_pipe_desc spd = {
		.pages = pages,
		.nr_pages = 0,
		.nr_pages_max = PIPE_DEF_BUFFERS,
		.partial = partial,
		.flags = flags,
		.ops = &relay_pipe_buf_ops,
		.spd_release = relay_page_release,
	};
	ssize_t ret;

	if (rbuf->subbufs_produced == rbuf->subbufs_consumed)
		return 0;
	if (splice_grow_spd(pipe, &spd))
		return -ENOMEM;

	*/
	 Adjust read len, if longer than what is available
	 /*
	if (len > (subbuf_size - read_start % subbuf_size))
		len = subbuf_size - read_start % subbuf_size;

	subbuf_pages = rbuf->chan->alloc_size >> PAGE_SHIFT;
	pidx = (read_start / PAGE_SIZE) % subbuf_pages;
	poff = read_start & ~PAGE_MASK;
	nr_pages = min_t(unsigned int, subbuf_pages, spd.nr_pages_max);

	for (total_len = 0; spd.nr_pages < nr_pages; spd.nr_pages++) {
		unsigned int this_len, this_end, private;
		unsigned int cur_pos = read_start + total_len;

		if (!len)
			break;

		this_len = min_t(unsigned long, len, PAGE_SIZE - poff);
		private = this_len;

		spd.pages[spd.nr_pages] = rbuf->page_array[pidx];
		spd.partial[spd.nr_pages].offset = poff;

		this_end = cur_pos + this_len;
		if (this_end >= nonpad_end) {
			this_len = nonpad_end - cur_pos;
			private = this_len + padding;
		}
		spd.partial[spd.nr_pages].len = this_len;
		spd.partial[spd.nr_pages].private = private;

		len -= this_len;
		total_len += this_len;
		poff = 0;
		pidx = (pidx + 1) % subbuf_pages;

		if (this_end >= nonpad_end) {
			spd.nr_pages++;
			break;
		}
	}

	ret = 0;
	if (!spd.nr_pages)
		goto out;

	ret =nonpad_ret = splice_to_pipe(pipe, &spd);
	if (ret < 0 || ret < total_len)
		goto out;

        if (read_start + ret == nonpad_end)
                ret += padding;

out:
	splice_shrink_spd(&spd);
	return ret;
}

static ssize_t relay_file_splice_read(struct filein,
				      loff_tppos,
				      struct pipe_inode_infopipe,
				      size_t len,
				      unsigned int flags)
{
	ssize_t spliced;
	int ret;
	int nonpad_ret = 0;

	ret = 0;
	spliced = 0;

	while (len && !spliced) {
		ret = subbuf_splice_actor(in, ppos, pipe, len, flags, &nonpad_ret);
		if (ret < 0)
			break;
		else if (!ret) {
			if (flags & SPLICE_F_NONBLOCK)
				ret = -EAGAIN;
			break;
		}

		*ppos += ret;
		if (ret > len)
			len = 0;
		else
			len -= ret;
		spliced += nonpad_ret;
		nonpad_ret = 0;
	}

	if (spliced)
		return spliced;

	return ret;
}

const struct file_operations relay_file_operations = {
	.open		= relay_file_open,
	.poll		= relay_file_poll,
	.mmap		= relay_file_mmap,
	.read		= relay_file_read,
	.llseek		= no_llseek,
	.release	= relay_file_release,
	.splice_read	= relay_file_splice_read,
};
EXPORT_SYMBOL_GPL(relay_file_operations);

static __init int relay_init(void)
{

	hotcpu_notifier(relay_hotcpu_callback, 0);
	return 0;
}

early_initcall(relay_init);
*/

	linux/kernel/resource.c

 Copyright (C) 1999	Linus Torvalds
 Copyright (C) 1999	Martin Mares <mj@ucw.cz>

 Arbitrary resource management.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/export.h>
#include <linux/errno.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/device.h>
#include <linux/pfn.h>
#include <linux/mm.h>
#include <linux/resource_ext.h>
#include <asm/io.h>


struct resource ioport_resource = {
	.name	= "PCI IO",
	.start	= 0,
	.end	= IO_SPACE_LIMIT,
	.flags	= IORESOURCE_IO,
};
EXPORT_SYMBOL(ioport_resource);

struct resource iomem_resource = {
	.name	= "PCI mem",
	.start	= 0,
	.end	= -1,
	.flags	= IORESOURCE_MEM,
};
EXPORT_SYMBOL(iomem_resource);

*/ constraints to be met while allocating resources /*
struct resource_constraint {
	resource_size_t min, max, align;
	resource_size_t (*alignf)(void, const struct resource,
			resource_size_t, resource_size_t);
	voidalignf_data;
};

static DEFINE_RWLOCK(resource_lock);

*/
 For memory hotplug, there is no way to free resource entries allocated
 by boot mem after the system is up. So for reusing the resource entry
 we need to remember the resource.
 /*
static struct resourcebootmem_resource_free;
static DEFINE_SPINLOCK(bootmem_resource_lock);

static struct resourcenext_resource(struct resourcep, bool sibling_only)
{
	*/ Caller wants to traverse through siblings only /*
	if (sibling_only)
		return p->sibling;

	if (p->child)
		return p->child;
	while (!p->sibling && p->parent)
		p = p->parent;
	return p->sibling;
}

static voidr_next(struct seq_filem, voidv, loff_tpos)
{
	struct resourcep = v;
	(*pos)++;
	return (void)next_resource(p, false);
}

#ifdef CONFIG_PROC_FS

enum { MAX_IORES_LEVEL = 5 };

static voidr_start(struct seq_filem, loff_tpos)
	__acquires(resource_lock)
{
	struct resourcep = m->private;
	loff_t l = 0;
	read_lock(&resource_lock);
	for (p = p->child; p && l <pos; p = r_next(m, p, &l))
		;
	return p;
}

static void r_stop(struct seq_filem, voidv)
	__releases(resource_lock)
{
	read_unlock(&resource_lock);
}

static int r_show(struct seq_filem, voidv)
{
	struct resourceroot = m->private;
	struct resourcer = v,p;
	unsigned long long start, end;
	int width = root->end < 0x10000 ? 4 : 8;
	int depth;

	for (depth = 0, p = r; depth < MAX_IORES_LEVEL; depth++, p = p->parent)
		if (p->parent == root)
			break;

	if (file_ns_capable(m->file, &init_user_ns, CAP_SYS_ADMIN)) {
		start = r->start;
		end = r->end;
	} else {
		start = end = 0;
	}

	seq_printf(m, "%*s%0*llx-%0*llx : %s\n",
			depth 2, "",
			width, start,
			width, end,
			r->name ? r->name : "<BAD>");
	return 0;
}

static const struct seq_operations resource_op = {
	.start	= r_start,
	.next	= r_next,
	.stop	= r_stop,
	.show	= r_show,
};

static int ioports_open(struct inodeinode, struct filefile)
{
	int res = seq_open(file, &resource_op);
	if (!res) {
		struct seq_filem = file->private_data;
		m->private = &ioport_resource;
	}
	return res;
}

static int iomem_open(struct inodeinode, struct filefile)
{
	int res = seq_open(file, &resource_op);
	if (!res) {
		struct seq_filem = file->private_data;
		m->private = &iomem_resource;
	}
	return res;
}

static const struct file_operations proc_ioports_operations = {
	.open		= ioports_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
};

static const struct file_operations proc_iomem_operations = {
	.open		= iomem_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
};

static int __init ioresources_init(void)
{
	proc_create("ioports", 0, NULL, &proc_ioports_operations);
	proc_create("iomem", 0, NULL, &proc_iomem_operations);
	return 0;
}
__initcall(ioresources_init);

#endif */ CONFIG_PROC_FS /*

static void free_resource(struct resourceres)
{
	if (!res)
		return;

	if (!PageSlab(virt_to_head_page(res))) {
		spin_lock(&bootmem_resource_lock);
		res->sibling = bootmem_resource_free;
		bootmem_resource_free = res;
		spin_unlock(&bootmem_resource_lock);
	} else {
		kfree(res);
	}
}

static struct resourcealloc_resource(gfp_t flags)
{
	struct resourceres = NULL;

	spin_lock(&bootmem_resource_lock);
	if (bootmem_resource_free) {
		res = bootmem_resource_free;
		bootmem_resource_free = res->sibling;
	}
	spin_unlock(&bootmem_resource_lock);

	if (res)
		memset(res, 0, sizeof(struct resource));
	else
		res = kzalloc(sizeof(struct resource), flags);

	return res;
}

*/ Return the conflict entry if you can't request it /*
static struct resource __request_resource(struct resourceroot, struct resourcenew)
{
	resource_size_t start = new->start;
	resource_size_t end = new->end;
	struct resourcetmp,*p;

	if (end < start)
		return root;
	if (start < root->start)
		return root;
	if (end > root->end)
		return root;
	p = &root->child;
	for (;;) {
		tmp =p;
		if (!tmp || tmp->start > end) {
			new->sibling = tmp;
			*p = new;
			new->parent = root;
			return NULL;
		}
		p = &tmp->sibling;
		if (tmp->end < start)
			continue;
		return tmp;
	}
}

static int __release_resource(struct resourceold, bool release_child)
{
	struct resourcetmp,*p,chd;

	p = &old->parent->child;
	for (;;) {
		tmp =p;
		if (!tmp)
			break;
		if (tmp == old) {
			if (release_child || !(tmp->child)) {
				*p = tmp->sibling;
			} else {
				for (chd = tmp->child;; chd = chd->sibling) {
					chd->parent = tmp->parent;
					if (!(chd->sibling))
						break;
				}
				*p = tmp->child;
				chd->sibling = tmp->sibling;
			}
			old->parent = NULL;
			return 0;
		}
		p = &tmp->sibling;
	}
	return -EINVAL;
}

static void __release_child_resources(struct resourcer)
{
	struct resourcetmp,p;
	resource_size_t size;

	p = r->child;
	r->child = NULL;
	while (p) {
		tmp = p;
		p = p->sibling;

		tmp->parent = NULL;
		tmp->sibling = NULL;
		__release_child_resources(tmp);

		printk(KERN_DEBUG "release child resource %pR\n", tmp);
		*/ need to restore size, and keep flags /*
		size = resource_size(tmp);
		tmp->start = 0;
		tmp->end = size - 1;
	}
}

void release_child_resources(struct resourcer)
{
	write_lock(&resource_lock);
	__release_child_resources(r);
	write_unlock(&resource_lock);
}

*/
 request_resource_conflict - request and reserve an I/O or memory resource
 @root: root resource descriptor
 @new: resource descriptor desired by caller

 Returns 0 for success, conflict resource on error.
 /*
struct resourcerequest_resource_conflict(struct resourceroot, struct resourcenew)
{
	struct resourceconflict;

	write_lock(&resource_lock);
	conflict = __request_resource(root, new);
	write_unlock(&resource_lock);
	return conflict;
}

*/
 request_resource - request and reserve an I/O or memory resource
 @root: root resource descriptor
 @new: resource descriptor desired by caller

 Returns 0 for success, negative error code on error.
 /*
int request_resource(struct resourceroot, struct resourcenew)
{
	struct resourceconflict;

	conflict = request_resource_conflict(root, new);
	return conflict ? -EBUSY : 0;
}

EXPORT_SYMBOL(request_resource);

*/
 release_resource - release a previously reserved resource
 @old: resource pointer
 /*
int release_resource(struct resourceold)
{
	int retval;

	write_lock(&resource_lock);
	retval = __release_resource(old, true);
	write_unlock(&resource_lock);
	return retval;
}

EXPORT_SYMBOL(release_resource);

*/
 Finds the lowest iomem resource existing within [res->start.res->end).
 The caller must specify res->start, res->end, res->flags, and optionally
 desc.  If found, returns 0, res is overwritten, if not found, returns -1.
 This function walks the whole tree and not just first level children until
 and unless first_level_children_only is true.
 /*
static int find_next_iomem_res(struct resourceres, unsigned long desc,
			       bool first_level_children_only)
{
	resource_size_t start, end;
	struct resourcep;
	bool sibling_only = false;

	BUG_ON(!res);

	start = res->start;
	end = res->end;
	BUG_ON(start >= end);

	if (first_level_children_only)
		sibling_only = true;

	read_lock(&resource_lock);

	for (p = iomem_resource.child; p; p = next_resource(p, sibling_only)) {
		if ((p->flags & res->flags) != res->flags)
			continue;
		if ((desc != IORES_DESC_NONE) && (desc != p->desc))
			continue;
		if (p->start > end) {
			p = NULL;
			break;
		}
		if ((p->end >= start) && (p->start < end))
			break;
	}

	read_unlock(&resource_lock);
	if (!p)
		return -1;
	*/ copy data /*
	if (res->start < p->start)
		res->start = p->start;
	if (res->end > p->end)
		res->end = p->end;
	return 0;
}

*/
 Walks through iomem resources and calls func() with matching resource
 ranges. This walks through whole tree and not just first level children.
 All the memory ranges which overlap start,end and also match flags and
 desc are valid candidates.

 @desc: I/O resource descriptor. Use IORES_DESC_NONE to skip @desc check.
 @flags: I/O resource flags
 @start: start addr
 @end: end addr

 NOTE: For a new descriptor search, define a new IORES_DESC in
 <linux/ioport.h> and set it in 'desc' of a target resource entry.
 /*
int walk_iomem_res_desc(unsigned long desc, unsigned long flags, u64 start,
		u64 end, voidarg, int (*func)(u64, u64, void))
{
	struct resource res;
	u64 orig_end;
	int ret = -1;

	res.start = start;
	res.end = end;
	res.flags = flags;
	orig_end = res.end;

	while ((res.start < res.end) &&
		(!find_next_iomem_res(&res, desc, false))) {

		ret = (*func)(res.start, res.end, arg);
		if (ret)
			break;

		res.start = res.end + 1;
		res.end = orig_end;
	}

	return ret;
}

*/
 This function calls the @func callback against all memory ranges of type
 System RAM which are marked as IORESOURCE_SYSTEM_RAM and IORESOUCE_BUSY.
 Now, this function is only for System RAM, it deals with full ranges and
 not PFNs. If resources are not PFN-aligned, dealing with PFNs can truncate
 ranges.
 /*
int walk_system_ram_res(u64 start, u64 end, voidarg,
				int (*func)(u64, u64, void))
{
	struct resource res;
	u64 orig_end;
	int ret = -1;

	res.start = start;
	res.end = end;
	res.flags = IORESOURCE_SYSTEM_RAM | IORESOURCE_BUSY;
	orig_end = res.end;
	while ((res.start < res.end) &&
		(!find_next_iomem_res(&res, IORES_DESC_NONE, true))) {
		ret = (*func)(res.start, res.end, arg);
		if (ret)
			break;
		res.start = res.end + 1;
		res.end = orig_end;
	}
	return ret;
}

#if !defined(CONFIG_ARCH_HAS_WALK_MEMORY)

*/
 This function calls the @func callback against all memory ranges of type
 System RAM which are marked as IORESOURCE_SYSTEM_RAM and IORESOUCE_BUSY.
 It is to be used only for System RAM.
 /*
int walk_system_ram_range(unsigned long start_pfn, unsigned long nr_pages,
		voidarg, int (*func)(unsigned long, unsigned long, void))
{
	struct resource res;
	unsigned long pfn, end_pfn;
	u64 orig_end;
	int ret = -1;

	res.start = (u64) start_pfn << PAGE_SHIFT;
	res.end = ((u64)(start_pfn + nr_pages) << PAGE_SHIFT) - 1;
	res.flags = IORESOURCE_SYSTEM_RAM | IORESOURCE_BUSY;
	orig_end = res.end;
	while ((res.start < res.end) &&
		(find_next_iomem_res(&res, IORES_DESC_NONE, true) >= 0)) {
		pfn = (res.start + PAGE_SIZE - 1) >> PAGE_SHIFT;
		end_pfn = (res.end + 1) >> PAGE_SHIFT;
		if (end_pfn > pfn)
			ret = (*func)(pfn, end_pfn - pfn, arg);
		if (ret)
			break;
		res.start = res.end + 1;
		res.end = orig_end;
	}
	return ret;
}

#endif

static int __is_ram(unsigned long pfn, unsigned long nr_pages, voidarg)
{
	return 1;
}
*/
 This generic page_is_ram() returns true if specified address is
 registered as System RAM in iomem_resource list.
 /*
int __weak page_is_ram(unsigned long pfn)
{
	return walk_system_ram_range(pfn, 1, NULL, __is_ram) == 1;
}
EXPORT_SYMBOL_GPL(page_is_ram);

*/
 region_intersects() - determine intersection of region with known resources
 @start: region start address
 @size: size of region
 @flags: flags of resource (in iomem_resource)
 @desc: descriptor of resource (in iomem_resource) or IORES_DESC_NONE

 Check if the specified region partially overlaps or fully eclipses a
 resource identified by @flags and @desc (optional with IORES_DESC_NONE).
 Return REGION_DISJOINT if the region does not overlap @flags/@desc,
 return REGION_MIXED if the region overlaps @flags/@desc and another
 resource, and return REGION_INTERSECTS if the region overlaps @flags/@desc
 and no other defined resource. Note that REGION_INTERSECTS is also
 returned in the case when the specified region overlaps RAM and undefined
 memory holes.

 region_intersect() is used by memory remapping functions to ensure
 the user is not remapping RAM and is a vast speed up over walking
 through the resource table page by page.
 /*
int region_intersects(resource_size_t start, size_t size, unsigned long flags,
		      unsigned long desc)
{
	resource_size_t end = start + size - 1;
	int type = 0; int other = 0;
	struct resourcep;

	read_lock(&resource_lock);
	for (p = iomem_resource.child; p ; p = p->sibling) {
		bool is_type = (((p->flags & flags) == flags) &&
				((desc == IORES_DESC_NONE) ||
				 (desc == p->desc)));

		if (start >= p->start && start <= p->end)
			is_type ? type++ : other++;
		if (end >= p->start && end <= p->end)
			is_type ? type++ : other++;
		if (p->start >= start && p->end <= end)
			is_type ? type++ : other++;
	}
	read_unlock(&resource_lock);

	if (other == 0)
		return type ? REGION_INTERSECTS : REGION_DISJOINT;

	if (type)
		return REGION_MIXED;

	return REGION_DISJOINT;
}
EXPORT_SYMBOL_GPL(region_intersects);

void __weak arch_remove_reservations(struct resourceavail)
{
}

static resource_size_t simple_align_resource(voiddata,
					     const struct resourceavail,
					     resource_size_t size,
					     resource_size_t align)
{
	return avail->start;
}

static void resource_clip(struct resourceres, resource_size_t min,
			  resource_size_t max)
{
	if (res->start < min)
		res->start = min;
	if (res->end > max)
		res->end = max;
}

*/
 Find empty slot in the resource tree with the given range and
 alignment constraints
 /*
static int __find_resource(struct resourceroot, struct resourceold,
			 struct resourcenew,
			 resource_size_t  size,
			 struct resource_constraintconstraint)
{
	struct resourcethis = root->child;
	struct resource tmp =new, avail, alloc;

	tmp.start = root->start;
	*/
	 Skip past an allocated resource that starts at 0, since the assignment
	 of this->start - 1 to tmp->end below would cause an underflow.
	 /*
	if (this && this->start == root->start) {
		tmp.start = (this == old) ? old->start : this->end + 1;
		this = this->sibling;
	}
	for(;;) {
		if (this)
			tmp.end = (this == old) ?  this->end : this->start - 1;
		else
			tmp.end = root->end;

		if (tmp.end < tmp.start)
			goto next;

		resource_clip(&tmp, constraint->min, constraint->max);
		arch_remove_reservations(&tmp);

		*/ Check for overflow after ALIGN() /*
		avail.start = ALIGN(tmp.start, constraint->align);
		avail.end = tmp.end;
		avail.flags = new->flags & ~IORESOURCE_UNSET;
		if (avail.start >= tmp.start) {
			alloc.flags = avail.flags;
			alloc.start = constraint->alignf(constraint->alignf_data, &avail,
					size, constraint->align);
			alloc.end = alloc.start + size - 1;
			if (resource_contains(&avail, &alloc)) {
				new->start = alloc.start;
				new->end = alloc.end;
				return 0;
			}
		}

next:		if (!this || this->end == root->end)
			break;

		if (this != old)
			tmp.start = this->end + 1;
		this = this->sibling;
	}
	return -EBUSY;
}

*/
 Find empty slot in the resource tree given range and alignment.
 /*
static int find_resource(struct resourceroot, struct resourcenew,
			resource_size_t size,
			struct resource_constraint constraint)
{
	return  __find_resource(root, NULL, new, size, constraint);
}

*/
 reallocate_resource - allocate a slot in the resource tree given range & alignment.
	The resource will be relocated if the new size cannot be reallocated in the
	current location.

 @root: root resource descriptor
 @old:  resource descriptor desired by caller
 @newsize: new size of the resource descriptor
 @constraint: the size and alignment constraints to be met.
 /*
static int reallocate_resource(struct resourceroot, struct resourceold,
			resource_size_t newsize,
			struct resource_constraint constraint)
{
	int err=0;
	struct resource new =old;
	struct resourceconflict;

	write_lock(&resource_lock);

	if ((err = __find_resource(root, old, &new, newsize, constraint)))
		goto out;

	if (resource_contains(&new, old)) {
		old->start = new.start;
		old->end = new.end;
		goto out;
	}

	if (old->child) {
		err = -EBUSY;
		goto out;
	}

	if (resource_contains(old, &new)) {
		old->start = new.start;
		old->end = new.end;
	} else {
		__release_resource(old, true);
		*old = new;
		conflict = __request_resource(root, old);
		BUG_ON(conflict);
	}
out:
	write_unlock(&resource_lock);
	return err;
}


*/
 allocate_resource - allocate empty slot in the resource tree given range & alignment.
 	The resource will be reallocated with a new size if it was already allocated
 @root: root resource descriptor
 @new: resource descriptor desired by caller
 @size: requested resource region size
 @min: minimum boundary to allocate
 @max: maximum boundary to allocate
 @align: alignment requested, in bytes
 @alignf: alignment function, optional, called if not NULL
 @alignf_data: arbitrary data to pass to the @alignf function
 /*
int allocate_resource(struct resourceroot, struct resourcenew,
		      resource_size_t size, resource_size_t min,
		      resource_size_t max, resource_size_t align,
		      resource_size_t (*alignf)(void,
						const struct resource,
						resource_size_t,
						resource_size_t),
		      voidalignf_data)
{
	int err;
	struct resource_constraint constraint;

	if (!alignf)
		alignf = simple_align_resource;

	constraint.min = min;
	constraint.max = max;
	constraint.align = align;
	constraint.alignf = alignf;
	constraint.alignf_data = alignf_data;

	if ( new->parent ) {
		*/ resource is already allocated, try reallocating with
		   the new constraints /*
		return reallocate_resource(root, new, size, &constraint);
	}

	write_lock(&resource_lock);
	err = find_resource(root, new, size, &constraint);
	if (err >= 0 && __request_resource(root, new))
		err = -EBUSY;
	write_unlock(&resource_lock);
	return err;
}

EXPORT_SYMBOL(allocate_resource);

*/
 lookup_resource - find an existing resource by a resource start address
 @root: root resource descriptor
 @start: resource start address

 Returns a pointer to the resource if found, NULL otherwise
 /*
struct resourcelookup_resource(struct resourceroot, resource_size_t start)
{
	struct resourceres;

	read_lock(&resource_lock);
	for (res = root->child; res; res = res->sibling) {
		if (res->start == start)
			break;
	}
	read_unlock(&resource_lock);

	return res;
}

*/
 Insert a resource into the resource tree. If successful, return NULL,
 otherwise return the conflicting resource (compare to __request_resource())
 /*
static struct resource __insert_resource(struct resourceparent, struct resourcenew)
{
	struct resourcefirst,next;

	for (;; parent = first) {
		first = __request_resource(parent, new);
		if (!first)
			return first;

		if (first == parent)
			return first;
		if (WARN_ON(first == new))	*/ duplicated insertion /*
			return first;

		if ((first->start > new->start) || (first->end < new->end))
			break;
		if ((first->start == new->start) && (first->end == new->end))
			break;
	}

	for (next = first; ; next = next->sibling) {
		*/ Partial overlap? Bad, and unfixable /*
		if (next->start < new->start || next->end > new->end)
			return next;
		if (!next->sibling)
			break;
		if (next->sibling->start > new->end)
			break;
	}

	new->parent = parent;
	new->sibling = next->sibling;
	new->child = first;

	next->sibling = NULL;
	for (next = first; next; next = next->sibling)
		next->parent = new;

	if (parent->child == first) {
		parent->child = new;
	} else {
		next = parent->child;
		while (next->sibling != first)
			next = next->sibling;
		next->sibling = new;
	}
	return NULL;
}

*/
 insert_resource_conflict - Inserts resource in the resource tree
 @parent: parent of the new resource
 @new: new resource to insert

 Returns 0 on success, conflict resource if the resource can't be inserted.

 This function is equivalent to request_resource_conflict when no conflict
 happens. If a conflict happens, and the conflicting resources
 entirely fit within the range of the new resource, then the new
 resource is inserted and the conflicting resources become children of
 the new resource.

 This function is intended for producers of resources, such as FW modules
 and bus drivers.
 /*
struct resourceinsert_resource_conflict(struct resourceparent, struct resourcenew)
{
	struct resourceconflict;

	write_lock(&resource_lock);
	conflict = __insert_resource(parent, new);
	write_unlock(&resource_lock);
	return conflict;
}

*/
 insert_resource - Inserts a resource in the resource tree
 @parent: parent of the new resource
 @new: new resource to insert

 Returns 0 on success, -EBUSY if the resource can't be inserted.

 This function is intended for producers of resources, such as FW modules
 and bus drivers.
 /*
int insert_resource(struct resourceparent, struct resourcenew)
{
	struct resourceconflict;

	conflict = insert_resource_conflict(parent, new);
	return conflict ? -EBUSY : 0;
}
EXPORT_SYMBOL_GPL(insert_resource);

*/
 insert_resource_expand_to_fit - Insert a resource into the resource tree
 @root: root resource descriptor
 @new: new resource to insert

 Insert a resource into the resource tree, possibly expanding it in order
 to make it encompass any conflicting resources.
 /*
void insert_resource_expand_to_fit(struct resourceroot, struct resourcenew)
{
	if (new->parent)
		return;

	write_lock(&resource_lock);
	for (;;) {
		struct resourceconflict;

		conflict = __insert_resource(root, new);
		if (!conflict)
			break;
		if (conflict == root)
			break;

		*/ Ok, expand resource to cover the conflict, then try again .. /*
		if (conflict->start < new->start)
			new->start = conflict->start;
		if (conflict->end > new->end)
			new->end = conflict->end;

		printk("Expanded resource %s due to conflict with %s\n", new->name, conflict->name);
	}
	write_unlock(&resource_lock);
}

*/
 remove_resource - Remove a resource in the resource tree
 @old: resource to remove

 Returns 0 on success, -EINVAL if the resource is not valid.

 This function removes a resource previously inserted by insert_resource()
 or insert_resource_conflict(), and moves the children (if any) up to
 where they were before.  insert_resource() and insert_resource_conflict()
 insert a new resource, and move any conflicting resources down to the
 children of the new resource.

 insert_resource(), insert_resource_conflict() and remove_resource() are
 intended for producers of resources, such as FW modules and bus drivers.
 /*
int remove_resource(struct resourceold)
{
	int retval;

	write_lock(&resource_lock);
	retval = __release_resource(old, false);
	write_unlock(&resource_lock);
	return retval;
}
EXPORT_SYMBOL_GPL(remove_resource);

static int __adjust_resource(struct resourceres, resource_size_t start,
				resource_size_t size)
{
	struct resourcetmp,parent = res->parent;
	resource_size_t end = start + size - 1;
	int result = -EBUSY;

	if (!parent)
		goto skip;

	if ((start < parent->start) || (end > parent->end))
		goto out;

	if (res->sibling && (res->sibling->start <= end))
		goto out;

	tmp = parent->child;
	if (tmp != res) {
		while (tmp->sibling != res)
			tmp = tmp->sibling;
		if (start <= tmp->end)
			goto out;
	}

skip:
	for (tmp = res->child; tmp; tmp = tmp->sibling)
		if ((tmp->start < start) || (tmp->end > end))
			goto out;

	res->start = start;
	res->end = end;
	result = 0;

 out:
	return result;
}

*/
 adjust_resource - modify a resource's start and size
 @res: resource to modify
 @start: new start value
 @size: new size

 Given an existing resource, change its start and size to match the
 arguments.  Returns 0 on success, -EBUSY if it can't fit.
 Existing children of the resource are assumed to be immutable.
 /*
int adjust_resource(struct resourceres, resource_size_t start,
			resource_size_t size)
{
	int result;

	write_lock(&resource_lock);
	result = __adjust_resource(res, start, size);
	write_unlock(&resource_lock);
	return result;
}
EXPORT_SYMBOL(adjust_resource);

static void __init __reserve_region_with_split(struct resourceroot,
		resource_size_t start, resource_size_t end,
		const charname)
{
	struct resourceparent = root;
	struct resourceconflict;
	struct resourceres = alloc_resource(GFP_ATOMIC);
	struct resourcenext_res = NULL;

	if (!res)
		return;

	res->name = name;
	res->start = start;
	res->end = end;
	res->flags = IORESOURCE_BUSY;
	res->desc = IORES_DESC_NONE;

	while (1) {

		conflict = __request_resource(parent, res);
		if (!conflict) {
			if (!next_res)
				break;
			res = next_res;
			next_res = NULL;
			continue;
		}

		*/ conflict covered whole area /*
		if (conflict->start <= res->start &&
				conflict->end >= res->end) {
			free_resource(res);
			WARN_ON(next_res);
			break;
		}

		*/ failed, split and try again /*
		if (conflict->start > res->start) {
			end = res->end;
			res->end = conflict->start - 1;
			if (conflict->end < end) {
				next_res = alloc_resource(GFP_ATOMIC);
				if (!next_res) {
					free_resource(res);
					break;
				}
				next_res->name = name;
				next_res->start = conflict->end + 1;
				next_res->end = end;
				next_res->flags = IORESOURCE_BUSY;
				next_res->desc = IORES_DESC_NONE;
			}
		} else {
			res->start = conflict->end + 1;
		}
	}

}

void __init reserve_region_with_split(struct resourceroot,
		resource_size_t start, resource_size_t end,
		const charname)
{
	int abort = 0;

	write_lock(&resource_lock);
	if (root->start > start || root->end < end) {
		pr_err("requested range [0x%llx-0x%llx] not in root %pr\n",
		       (unsigned long long)start, (unsigned long long)end,
		       root);
		if (start > root->end || end < root->start)
			abort = 1;
		else {
			if (end > root->end)
				end = root->end;
			if (start < root->start)
				start = root->start;
			pr_err("fixing request to [0x%llx-0x%llx]\n",
			       (unsigned long long)start,
			       (unsigned long long)end);
		}
		dump_stack();
	}
	if (!abort)
		__reserve_region_with_split(root, start, end, name);
	write_unlock(&resource_lock);
}

*/
 resource_alignment - calculate resource's alignment
 @res: resource pointer

 Returns alignment on success, 0 (invalid alignment) on failure.
 /*
resource_size_t resource_alignment(struct resourceres)
{
	switch (res->flags & (IORESOURCE_SIZEALIGN | IORESOURCE_STARTALIGN)) {
	case IORESOURCE_SIZEALIGN:
		return resource_size(res);
	case IORESOURCE_STARTALIGN:
		return res->start;
	default:
		return 0;
	}
}

*/
 This is compatibility stuff for IO resources.

 Note how this, unlike the above, knows about
 the IO flag meanings (busy etc).

 request_region creates a new busy region.

 release_region releases a matching busy region.
 /*

static DECLARE_WAIT_QUEUE_HEAD(muxed_resource_wait);

*/
 __request_region - create a new busy resource region
 @parent: parent resource descriptor
 @start: resource start address
 @n: resource region size
 @name: reserving caller's ID string
 @flags: IO resource flags
 /*
struct resource __request_region(struct resourceparent,
				   resource_size_t start, resource_size_t n,
				   const charname, int flags)
{
	DECLARE_WAITQUEUE(wait, current);
	struct resourceres = alloc_resource(GFP_KERNEL);

	if (!res)
		return NULL;

	res->name = name;
	res->start = start;
	res->end = start + n - 1;

	write_lock(&resource_lock);

	for (;;) {
		struct resourceconflict;

		res->flags = resource_type(parent) | resource_ext_type(parent);
		res->flags |= IORESOURCE_BUSY | flags;
		res->desc = parent->desc;

		conflict = __request_resource(parent, res);
		if (!conflict)
			break;
		if (conflict != parent) {
			if (!(conflict->flags & IORESOURCE_BUSY)) {
				parent = conflict;
				continue;
			}
		}
		if (conflict->flags & flags & IORESOURCE_MUXED) {
			add_wait_queue(&muxed_resource_wait, &wait);
			write_unlock(&resource_lock);
			set_current_state(TASK_UNINTERRUPTIBLE);
			schedule();
			remove_wait_queue(&muxed_resource_wait, &wait);
			write_lock(&resource_lock);
			continue;
		}
		*/ Uhhuh, that didn't work out.. /*
		free_resource(res);
		res = NULL;
		break;
	}
	write_unlock(&resource_lock);
	return res;
}
EXPORT_SYMBOL(__request_region);

*/
 __release_region - release a previously reserved resource region
 @parent: parent resource descriptor
 @start: resource start address
 @n: resource region size

 The described resource region must match a currently busy region.
 /*
void __release_region(struct resourceparent, resource_size_t start,
			resource_size_t n)
{
	struct resource*p;
	resource_size_t end;

	p = &parent->child;
	end = start + n - 1;

	write_lock(&resource_lock);

	for (;;) {
		struct resourceres =p;

		if (!res)
			break;
		if (res->start <= start && res->end >= end) {
			if (!(res->flags & IORESOURCE_BUSY)) {
				p = &res->child;
				continue;
			}
			if (res->start != start || res->end != end)
				break;
			*p = res->sibling;
			write_unlock(&resource_lock);
			if (res->flags & IORESOURCE_MUXED)
				wake_up(&muxed_resource_wait);
			free_resource(res);
			return;
		}
		p = &res->sibling;
	}

	write_unlock(&resource_lock);

	printk(KERN_WARNING "Trying to free nonexistent resource "
		"<%016llx-%016llx>\n", (unsigned long long)start,
		(unsigned long long)end);
}
EXPORT_SYMBOL(__release_region);

#ifdef CONFIG_MEMORY_HOTREMOVE
*/
 release_mem_region_adjustable - release a previously reserved memory region
 @parent: parent resource descriptor
 @start: resource start address
 @size: resource region size

 This interface is intended for memory hot-delete.  The requested region
 is released from a currently busy memory resource.  The requested region
 must either match exactly or fit into a single busy resource entry.  In
 the latter case, the remaining resource is adjusted accordingly.
 Existing children of the busy memory resource must be immutable in the
 request.

 Note:
 - Additional release conditions, such as overlapping region, can be
   supported after they are confirmed as valid cases.
 - When a busy memory resource gets split into two entries, the code
   assumes that all children remain in the lower address entry for
   simplicity.  Enhance this logic when necessary.
 /*
int release_mem_region_adjustable(struct resourceparent,
			resource_size_t start, resource_size_t size)
{
	struct resource*p;
	struct resourceres;
	struct resourcenew_res;
	resource_size_t end;
	int ret = -EINVAL;

	end = start + size - 1;
	if ((start < parent->start) || (end > parent->end))
		return ret;

	*/ The alloc_resource() result gets checked later /*
	new_res = alloc_resource(GFP_KERNEL);

	p = &parent->child;
	write_lock(&resource_lock);

	while ((res =p)) {
		if (res->start >= end)
			break;

		*/ look for the next resource if it does not fit into /*
		if (res->start > start || res->end < end) {
			p = &res->sibling;
			continue;
		}

		if (!(res->flags & IORESOURCE_MEM))
			break;

		if (!(res->flags & IORESOURCE_BUSY)) {
			p = &res->child;
			continue;
		}

		*/ found the target resource; let's adjust accordingly /*
		if (res->start == start && res->end == end) {
			*/ free the whole entry /*
			*p = res->sibling;
			free_resource(res);
			ret = 0;
		} else if (res->start == start && res->end != end) {
			*/ adjust the start /*
			ret = __adjust_resource(res, end + 1,
						res->end - end);
		} else if (res->start != start && res->end == end) {
			*/ adjust the end /*
			ret = __adjust_resource(res, res->start,
						start - res->start);
		} else {
			*/ split into two entries /*
			if (!new_res) {
				ret = -ENOMEM;
				break;
			}
			new_res->name = res->name;
			new_res->start = end + 1;
			new_res->end = res->end;
			new_res->flags = res->flags;
			new_res->desc = res->desc;
			new_res->parent = res->parent;
			new_res->sibling = res->sibling;
			new_res->child = NULL;

			ret = __adjust_resource(res, res->start,
						start - res->start);
			if (ret)
				break;
			res->sibling = new_res;
			new_res = NULL;
		}

		break;
	}

	write_unlock(&resource_lock);
	free_resource(new_res);
	return ret;
}
#endif	*/ CONFIG_MEMORY_HOTREMOVE /*

*/
 Managed region resource
 /*
static void devm_resource_release(struct devicedev, voidptr)
{
	struct resource*r = ptr;

	release_resource(*r);
}

*/
 devm_request_resource() - request and reserve an I/O or memory resource
 @dev: device for which to request the resource
 @root: root of the resource tree from which to request the resource
 @new: descriptor of the resource to request

 This is a device-managed version of request_resource(). There is usually
 no need to release resources requested by this function explicitly since
 that will be taken care of when the device is unbound from its driver.
 If for some reason the resource needs to be released explicitly, because
 of ordering issues for example, drivers must call devm_release_resource()
 rather than the regular release_resource().

 When a conflict is detected between any existing resources and the newly
 requested resource, an error message will be printed.

 Returns 0 on success or a negative error code on failure.
 /*
int devm_request_resource(struct devicedev, struct resourceroot,
			  struct resourcenew)
{
	struct resourceconflict,*ptr;

	ptr = devres_alloc(devm_resource_release, sizeof(*ptr), GFP_KERNEL);
	if (!ptr)
		return -ENOMEM;

	*ptr = new;

	conflict = request_resource_conflict(root, new);
	if (conflict) {
		dev_err(dev, "resource collision: %pR conflicts with %s %pR\n",
			new, conflict->name, conflict);
		devres_free(ptr);
		return -EBUSY;
	}

	devres_add(dev, ptr);
	return 0;
}
EXPORT_SYMBOL(devm_request_resource);

static int devm_resource_match(struct devicedev, voidres, voiddata)
{
	struct resource*ptr = res;

	returnptr == data;
}

*/
 devm_release_resource() - release a previously requested resource
 @dev: device for which to release the resource
 @new: descriptor of the resource to release

 Releases a resource previously requested using devm_request_resource().
 /*
void devm_release_resource(struct devicedev, struct resourcenew)
{
	WARN_ON(devres_release(dev, devm_resource_release, devm_resource_match,
			       new));
}
EXPORT_SYMBOL(devm_release_resource);

struct region_devres {
	struct resourceparent;
	resource_size_t start;
	resource_size_t n;
};

static void devm_region_release(struct devicedev, voidres)
{
	struct region_devresthis = res;

	__release_region(this->parent, this->start, this->n);
}

static int devm_region_match(struct devicedev, voidres, voidmatch_data)
{
	struct region_devresthis = res,match = match_data;

	return this->parent == match->parent &&
		this->start == match->start && this->n == match->n;
}

struct resource __devm_request_region(struct devicedev,
				struct resourceparent, resource_size_t start,
				resource_size_t n, const charname)
{
	struct region_devresdr = NULL;
	struct resourceres;

	dr = devres_alloc(devm_region_release, sizeof(struct region_devres),
			  GFP_KERNEL);
	if (!dr)
		return NULL;

	dr->parent = parent;
	dr->start = start;
	dr->n = n;

	res = __request_region(parent, start, n, name, 0);
	if (res)
		devres_add(dev, dr);
	else
		devres_free(dr);

	return res;
}
EXPORT_SYMBOL(__devm_request_region);

void __devm_release_region(struct devicedev, struct resourceparent,
			   resource_size_t start, resource_size_t n)
{
	struct region_devres match_data = { parent, start, n };

	__release_region(parent, start, n);
	WARN_ON(devres_destroy(dev, devm_region_release, devm_region_match,
			       &match_data));
}
EXPORT_SYMBOL(__devm_release_region);

*/
 Called from init/main.c to reserve IO ports.
 /*
#define MAXRESERVE 4
static int __init reserve_setup(charstr)
{
	static int reserved;
	static struct resource reserve[MAXRESERVE];

	for (;;) {
		unsigned int io_start, io_num;
		int x = reserved;

		if (get_option (&str, &io_start) != 2)
			break;
		if (get_option (&str, &io_num)   == 0)
			break;
		if (x < MAXRESERVE) {
			struct resourceres = reserve + x;
			res->name = "reserved";
			res->start = io_start;
			res->end = io_start + io_num - 1;
			res->flags = IORESOURCE_BUSY;
			res->desc = IORES_DESC_NONE;
			res->child = NULL;
			if (request_resource(res->start >= 0x10000 ? &iomem_resource : &ioport_resource, res) == 0)
				reserved = x+1;
		}
	}
	return 1;
}

__setup("reserve=", reserve_setup);

*/
 Check if the requested addr and size spans more than any slot in the
 iomem resource tree.
 /*
int iomem_map_sanity_check(resource_size_t addr, unsigned long size)
{
	struct resourcep = &iomem_resource;
	int err = 0;
	loff_t l;

	read_lock(&resource_lock);
	for (p = p->child; p ; p = r_next(NULL, p, &l)) {
		*/
		 We can probably skip the resources without
		 IORESOURCE_IO attribute?
		 /*
		if (p->start >= addr + size)
			continue;
		if (p->end < addr)
			continue;
		if (PFN_DOWN(p->start) <= PFN_DOWN(addr) &&
		    PFN_DOWN(p->end) >= PFN_DOWN(addr + size - 1))
			continue;
		*/
		 if a resource is "BUSY", it's not a hardware resource
		 but a driver mapping of such a resource; we don't want
		 to warn for those; some drivers legitimately map only
		 partial hardware resources. (example: vesafb)
		 /*
		if (p->flags & IORESOURCE_BUSY)
			continue;

		printk(KERN_WARNING "resource sanity check: requesting [mem %#010llx-%#010llx], which spans more than %s %pR\n",
		       (unsigned long long)addr,
		       (unsigned long long)(addr + size - 1),
		       p->name, p);
		err = -1;
		break;
	}
	read_unlock(&resource_lock);

	return err;
}

#ifdef CONFIG_STRICT_DEVMEM
static int strict_iomem_checks = 1;
#else
static int strict_iomem_checks;
#endif

*/
 check if an address is reserved in the iomem resource tree
 returns 1 if reserved, 0 if not reserved.
 /*
int iomem_is_exclusive(u64 addr)
{
	struct resourcep = &iomem_resource;
	int err = 0;
	loff_t l;
	int size = PAGE_SIZE;

	if (!strict_iomem_checks)
		return 0;

	addr = addr & PAGE_MASK;

	read_lock(&resource_lock);
	for (p = p->child; p ; p = r_next(NULL, p, &l)) {
		*/
		 We can probably skip the resources without
		 IORESOURCE_IO attribute?
		 /*
		if (p->start >= addr + size)
			break;
		if (p->end < addr)
			continue;
		*/
		 A resource is exclusive if IORESOURCE_EXCLUSIVE is set
		 or CONFIG_IO_STRICT_DEVMEM is enabled and the
		 resource is busy.
		 /*
		if ((p->flags & IORESOURCE_BUSY) == 0)
			continue;
		if (IS_ENABLED(CONFIG_IO_STRICT_DEVMEM)
				|| p->flags & IORESOURCE_EXCLUSIVE) {
			err = 1;
			break;
		}
	}
	read_unlock(&resource_lock);

	return err;
}

struct resource_entryresource_list_create_entry(struct resourceres,
						  size_t extra_size)
{
	struct resource_entryentry;

	entry = kzalloc(sizeof(*entry) + extra_size, GFP_KERNEL);
	if (entry) {
		INIT_LIST_HEAD(&entry->node);
		entry->res = res ? res : &entry->__res;
	}

	return entry;
}
EXPORT_SYMBOL(resource_list_create_entry);

void resource_list_free(struct list_headhead)
{
	struct resource_entryentry,tmp;

	list_for_each_entry_safe(entry, tmp, head, node)
		resource_list_destroy_entry(entry);
}
EXPORT_SYMBOL(resource_list_free);

static int __init strict_iomem(charstr)
{
	if (strstr(str, "relaxed"))
		strict_iomem_checks = 0;
	if (strstr(str, "strict"))
		strict_iomem_checks = 1;
	return 1;
}

__setup("iomem=", strict_iomem);
*/

 linux/kernel/seccomp.c

 Copyright 2004-2005  Andrea Arcangeli <andrea@cpushare.com>

 Copyright (C) 2012 Google, Inc.
 Will Drewry <wad@chromium.org>

 This defines a simple but solid secure-computing facility.

 Mode 1 uses a fixed list of allowed system calls.
 Mode 2 allows user-defined system call filters in the form
        of Berkeley Packet Filters/Linux Socket Filters.
 /*

#include <linux/atomic.h>
#include <linux/audit.h>
#include <linux/compat.h>
#include <linux/sched.h>
#include <linux/seccomp.h>
#include <linux/slab.h>
#include <linux/syscalls.h>

#ifdef CONFIG_HAVE_ARCH_SECCOMP_FILTER
#include <asm/syscall.h>
#endif

#ifdef CONFIG_SECCOMP_FILTER
#include <linux/filter.h>
#include <linux/pid.h>
#include <linux/ptrace.h>
#include <linux/security.h>
#include <linux/tracehook.h>
#include <linux/uaccess.h>

*/
 struct seccomp_filter - container for seccomp BPF programs

 @usage: reference count to manage the object lifetime.
         get/put helpers should be used when accessing an instance
         outside of a lifetime-guarded section.  In general, this
         is only needed for handling filters shared across tasks.
 @prev: points to a previously installed, or inherited, filter
 @len: the number of instructions in the program
 @insnsi: the BPF program instructions to evaluate

 seccomp_filter objects are organized in a tree linked via the @prev
 pointer.  For any task, it appears to be a singly-linked list starting
 with current->seccomp.filter, the most recently attached or inherited filter.
 However, multiple filters may share a @prev node, by way of fork(), which
 results in a unidirectional tree existing in memory.  This is similar to
 how namespaces work.

 seccomp_filter objects should never be modified after being attached
 to a task_struct (other than @usage).
 /*
struct seccomp_filter {
	atomic_t usage;
	struct seccomp_filterprev;
	struct bpf_progprog;
};

*/ Limit any path through the tree to 256KB worth of instructions. /*
#define MAX_INSNS_PER_PATH ((1 << 18) / sizeof(struct sock_filter))

*/
 Endianness is explicitly ignored and left for BPF program authors to manage
 as per the specific architecture.
 /*
static void populate_seccomp_data(struct seccomp_datasd)
{
	struct task_structtask = current;
	struct pt_regsregs = task_pt_regs(task);
	unsigned long args[6];

	sd->nr = syscall_get_nr(task, regs);
	sd->arch = syscall_get_arch();
	syscall_get_arguments(task, regs, 0, 6, args);
	sd->args[0] = args[0];
	sd->args[1] = args[1];
	sd->args[2] = args[2];
	sd->args[3] = args[3];
	sd->args[4] = args[4];
	sd->args[5] = args[5];
	sd->instruction_pointer = KSTK_EIP(task);
}

*/
	seccomp_check_filter - verify seccomp filter code
	@filter: filter to verify
	@flen: length of filter

 Takes a previously checked filter (by bpf_check_classic) and
 redirects all filter code that loads struct sk_buff data
 and related data through seccomp_bpf_load.  It also
 enforces length and alignment checking of those loads.

 Returns 0 if the rule set is legal or -EINVAL if not.
 /*
static int seccomp_check_filter(struct sock_filterfilter, unsigned int flen)
{
	int pc;
	for (pc = 0; pc < flen; pc++) {
		struct sock_filterftest = &filter[pc];
		u16 code = ftest->code;
		u32 k = ftest->k;

		switch (code) {
		case BPF_LD | BPF_W | BPF_ABS:
			ftest->code = BPF_LDX | BPF_W | BPF_ABS;
			*/ 32-bit aligned and not out of bounds. /*
			if (k >= sizeof(struct seccomp_data) || k & 3)
				return -EINVAL;
			continue;
		case BPF_LD | BPF_W | BPF_LEN:
			ftest->code = BPF_LD | BPF_IMM;
			ftest->k = sizeof(struct seccomp_data);
			continue;
		case BPF_LDX | BPF_W | BPF_LEN:
			ftest->code = BPF_LDX | BPF_IMM;
			ftest->k = sizeof(struct seccomp_data);
			continue;
		*/ Explicitly include allowed calls. /*
		case BPF_RET | BPF_K:
		case BPF_RET | BPF_A:
		case BPF_ALU | BPF_ADD | BPF_K:
		case BPF_ALU | BPF_ADD | BPF_X:
		case BPF_ALU | BPF_SUB | BPF_K:
		case BPF_ALU | BPF_SUB | BPF_X:
		case BPF_ALU | BPF_MUL | BPF_K:
		case BPF_ALU | BPF_MUL | BPF_X:
		case BPF_ALU | BPF_DIV | BPF_K:
		case BPF_ALU | BPF_DIV | BPF_X:
		case BPF_ALU | BPF_AND | BPF_K:
		case BPF_ALU | BPF_AND | BPF_X:
		case BPF_ALU | BPF_OR | BPF_K:
		case BPF_ALU | BPF_OR | BPF_X:
		case BPF_ALU | BPF_XOR | BPF_K:
		case BPF_ALU | BPF_XOR | BPF_X:
		case BPF_ALU | BPF_LSH | BPF_K:
		case BPF_ALU | BPF_LSH | BPF_X:
		case BPF_ALU | BPF_RSH | BPF_K:
		case BPF_ALU | BPF_RSH | BPF_X:
		case BPF_ALU | BPF_NEG:
		case BPF_LD | BPF_IMM:
		case BPF_LDX | BPF_IMM:
		case BPF_MISC | BPF_TAX:
		case BPF_MISC | BPF_TXA:
		case BPF_LD | BPF_MEM:
		case BPF_LDX | BPF_MEM:
		case BPF_ST:
		case BPF_STX:
		case BPF_JMP | BPF_JA:
		case BPF_JMP | BPF_JEQ | BPF_K:
		case BPF_JMP | BPF_JEQ | BPF_X:
		case BPF_JMP | BPF_JGE | BPF_K:
		case BPF_JMP | BPF_JGE | BPF_X:
		case BPF_JMP | BPF_JGT | BPF_K:
		case BPF_JMP | BPF_JGT | BPF_X:
		case BPF_JMP | BPF_JSET | BPF_K:
		case BPF_JMP | BPF_JSET | BPF_X:
			continue;
		default:
			return -EINVAL;
		}
	}
	return 0;
}

*/
 seccomp_run_filters - evaluates all seccomp filters against @syscall
 @syscall: number of the current system call

 Returns valid seccomp BPF response codes.
 /*
static u32 seccomp_run_filters(struct seccomp_datasd)
{
	struct seccomp_data sd_local;
	u32 ret = SECCOMP_RET_ALLOW;
	*/ Make sure cross-thread synced filter points somewhere sane. /*
	struct seccomp_filterf =
			lockless_dereference(current->seccomp.filter);

	*/ Ensure unexpected behavior doesn't result in failing open. /*
	if (unlikely(WARN_ON(f == NULL)))
		return SECCOMP_RET_KILL;

	if (!sd) {
		populate_seccomp_data(&sd_local);
		sd = &sd_local;
	}

	*/
	 All filters in the list are evaluated and the lowest BPF return
	 value always takes priority (ignoring the DATA).
	 /*
	for (; f; f = f->prev) {
		u32 cur_ret = BPF_PROG_RUN(f->prog, (void)sd);

		if ((cur_ret & SECCOMP_RET_ACTION) < (ret & SECCOMP_RET_ACTION))
			ret = cur_ret;
	}
	return ret;
}
#endif */ CONFIG_SECCOMP_FILTER /*

static inline bool seccomp_may_assign_mode(unsigned long seccomp_mode)
{
	assert_spin_locked(&current->sighand->siglock);

	if (current->seccomp.mode && current->seccomp.mode != seccomp_mode)
		return false;

	return true;
}

static inline void seccomp_assign_mode(struct task_structtask,
				       unsigned long seccomp_mode)
{
	assert_spin_locked(&task->sighand->siglock);

	task->seccomp.mode = seccomp_mode;
	*/
	 Make sure TIF_SECCOMP cannot be set before the mode (and
	 filter) is set.
	 /*
	smp_mb__before_atomic();
	set_tsk_thread_flag(task, TIF_SECCOMP);
}

#ifdef CONFIG_SECCOMP_FILTER
*/ Returns 1 if the parent is an ancestor of the child. /*
static int is_ancestor(struct seccomp_filterparent,
		       struct seccomp_filterchild)
{
	*/ NULL is the root ancestor. /*
	if (parent == NULL)
		return 1;
	for (; child; child = child->prev)
		if (child == parent)
			return 1;
	return 0;
}

*/
 seccomp_can_sync_threads: checks if all threads can be synchronized

 Expects sighand and cred_guard_mutex locks to be held.

 Returns 0 on success, -ve on error, or the pid of a thread which was
 either not in the correct seccomp mode or it did not have an ancestral
 seccomp filter.
 /*
static inline pid_t seccomp_can_sync_threads(void)
{
	struct task_structthread,caller;

	BUG_ON(!mutex_is_locked(&current->signal->cred_guard_mutex));
	assert_spin_locked(&current->sighand->siglock);

	*/ Validate all threads being eligible for synchronization. /*
	caller = current;
	for_each_thread(caller, thread) {
		pid_t failed;

		*/ Skip current, since it is initiating the sync. /*
		if (thread == caller)
			continue;

		if (thread->seccomp.mode == SECCOMP_MODE_DISABLED ||
		    (thread->seccomp.mode == SECCOMP_MODE_FILTER &&
		     is_ancestor(thread->seccomp.filter,
				 caller->seccomp.filter)))
			continue;

		*/ Return the first thread that cannot be synchronized. /*
		failed = task_pid_vnr(thread);
		*/ If the pid cannot be resolved, then return -ESRCH /*
		if (unlikely(WARN_ON(failed == 0)))
			failed = -ESRCH;
		return failed;
	}

	return 0;
}

*/
 seccomp_sync_threads: sets all threads to use current's filter

 Expects sighand and cred_guard_mutex locks to be held, and for
 seccomp_can_sync_threads() to have returned success already
 without dropping the locks.

 /*
static inline void seccomp_sync_threads(void)
{
	struct task_structthread,caller;

	BUG_ON(!mutex_is_locked(&current->signal->cred_guard_mutex));
	assert_spin_locked(&current->sighand->siglock);

	*/ Synchronize all threads. /*
	caller = current;
	for_each_thread(caller, thread) {
		*/ Skip current, since it needs no changes. /*
		if (thread == caller)
			continue;

		*/ Get a task reference for the new leaf node. /*
		get_seccomp_filter(caller);
		*/
		 Drop the task reference to the shared ancestor since
		 current's path will hold a reference.  (This also
		 allows a put before the assignment.)
		 /*
		put_seccomp_filter(thread);
		smp_store_release(&thread->seccomp.filter,
				  caller->seccomp.filter);

		*/
		 Don't let an unprivileged task work around
		 the no_new_privs restriction by creating
		 a thread that sets it up, enters seccomp,
		 then dies.
		 /*
		if (task_no_new_privs(caller))
			task_set_no_new_privs(thread);

		*/
		 Opt the other thread into seccomp if needed.
		 As threads are considered to be trust-realm
		 equivalent (see ptrace_may_access), it is safe to
		 allow one thread to transition the other.
		 /*
		if (thread->seccomp.mode == SECCOMP_MODE_DISABLED)
			seccomp_assign_mode(thread, SECCOMP_MODE_FILTER);
	}
}

*/
 seccomp_prepare_filter: Prepares a seccomp filter for use.
 @fprog: BPF program to install

 Returns filter on success or an ERR_PTR on failure.
 /*
static struct seccomp_filterseccomp_prepare_filter(struct sock_fprogfprog)
{
	struct seccomp_filtersfilter;
	int ret;
	const bool save_orig = config_enabled(CONFIG_CHECKPOINT_RESTORE);

	if (fprog->len == 0 || fprog->len > BPF_MAXINSNS)
		return ERR_PTR(-EINVAL);

	BUG_ON(INT_MAX / fprog->len < sizeof(struct sock_filter));

	*/
	 Installing a seccomp filter requires that the task has
	 CAP_SYS_ADMIN in its namespace or be running with no_new_privs.
	 This avoids scenarios where unprivileged tasks can affect the
	 behavior of privileged children.
	 /*
	if (!task_no_new_privs(current) &&
	    security_capable_noaudit(current_cred(), current_user_ns(),
				     CAP_SYS_ADMIN) != 0)
		return ERR_PTR(-EACCES);

	*/ Allocate a new seccomp_filter /*
	sfilter = kzalloc(sizeof(*sfilter), GFP_KERNEL | __GFP_NOWARN);
	if (!sfilter)
		return ERR_PTR(-ENOMEM);

	ret = bpf_prog_create_from_user(&sfilter->prog, fprog,
					seccomp_check_filter, save_orig);
	if (ret < 0) {
		kfree(sfilter);
		return ERR_PTR(ret);
	}

	atomic_set(&sfilter->usage, 1);

	return sfilter;
}

*/
 seccomp_prepare_user_filter - prepares a user-supplied sock_fprog
 @user_filter: pointer to the user data containing a sock_fprog.

 Returns 0 on success and non-zero otherwise.
 /*
static struct seccomp_filter
seccomp_prepare_user_filter(const char __useruser_filter)
{
	struct sock_fprog fprog;
	struct seccomp_filterfilter = ERR_PTR(-EFAULT);

#ifdef CONFIG_COMPAT
	if (in_compat_syscall()) {
		struct compat_sock_fprog fprog32;
		if (copy_from_user(&fprog32, user_filter, sizeof(fprog32)))
			goto out;
		fprog.len = fprog32.len;
		fprog.filter = compat_ptr(fprog32.filter);
	} else/ falls through to the if below. /*
#endif
	if (copy_from_user(&fprog, user_filter, sizeof(fprog)))
		goto out;
	filter = seccomp_prepare_filter(&fprog);
out:
	return filter;
}

*/
 seccomp_attach_filter: validate and attach filter
 @flags:  flags to change filter behavior
 @filter: seccomp filter to add to the current process

 Caller must be holding current->sighand->siglock lock.

 Returns 0 on success, -ve on error.
 /*
static long seccomp_attach_filter(unsigned int flags,
				  struct seccomp_filterfilter)
{
	unsigned long total_insns;
	struct seccomp_filterwalker;

	assert_spin_locked(&current->sighand->siglock);

	*/ Validate resulting filter length. /*
	total_insns = filter->prog->len;
	for (walker = current->seccomp.filter; walker; walker = walker->prev)
		total_insns += walker->prog->len + 4; / 4 instr penalty /*
	if (total_insns > MAX_INSNS_PER_PATH)
		return -ENOMEM;

	*/ If thread sync has been requested, check that it is possible. /*
	if (flags & SECCOMP_FILTER_FLAG_TSYNC) {
		int ret;

		ret = seccomp_can_sync_threads();
		if (ret)
			return ret;
	}

	*/
	 If there is an existing filter, make it the prev and don't drop its
	 task reference.
	 /*
	filter->prev = current->seccomp.filter;
	current->seccomp.filter = filter;

	*/ Now that the new filter is in place, synchronize to all threads. /*
	if (flags & SECCOMP_FILTER_FLAG_TSYNC)
		seccomp_sync_threads();

	return 0;
}

*/ get_seccomp_filter - increments the reference count of the filter on @tsk /*
void get_seccomp_filter(struct task_structtsk)
{
	struct seccomp_filterorig = tsk->seccomp.filter;
	if (!orig)
		return;
	*/ Reference count is bounded by the number of total processes. /*
	atomic_inc(&orig->usage);
}

static inline void seccomp_filter_free(struct seccomp_filterfilter)
{
	if (filter) {
		bpf_prog_destroy(filter->prog);
		kfree(filter);
	}
}

*/ put_seccomp_filter - decrements the ref count of tsk->seccomp.filter /*
void put_seccomp_filter(struct task_structtsk)
{
	struct seccomp_filterorig = tsk->seccomp.filter;
	*/ Clean up single-reference branches iteratively. /*
	while (orig && atomic_dec_and_test(&orig->usage)) {
		struct seccomp_filterfreeme = orig;
		orig = orig->prev;
		seccomp_filter_free(freeme);
	}
}

*/
 seccomp_send_sigsys - signals the task to allow in-process syscall emulation
 @syscall: syscall number to send to userland
 @reason: filter-supplied reason code to send to userland (via si_errno)

 Forces a SIGSYS with a code of SYS_SECCOMP and related sigsys info.
 /*
static void seccomp_send_sigsys(int syscall, int reason)
{
	struct siginfo info;
	memset(&info, 0, sizeof(info));
	info.si_signo = SIGSYS;
	info.si_code = SYS_SECCOMP;
	info.si_call_addr = (void __user)KSTK_EIP(current);
	info.si_errno = reason;
	info.si_arch = syscall_get_arch();
	info.si_syscall = syscall;
	force_sig_info(SIGSYS, &info, current);
}
#endif	*/ CONFIG_SECCOMP_FILTER /*

*/
 Secure computing mode 1 allows only read/write/exit/sigreturn.
 To be fully secure this must be combined with rlimit
 to limit the stack allocations too.
 /*
static int mode1_syscalls[] = {
	__NR_seccomp_read, __NR_seccomp_write, __NR_seccomp_exit, __NR_seccomp_sigreturn,
	0,/ null terminated /*
};

#ifdef CONFIG_COMPAT
static int mode1_syscalls_32[] = {
	__NR_seccomp_read_32, __NR_seccomp_write_32, __NR_seccomp_exit_32, __NR_seccomp_sigreturn_32,
	0,/ null terminated /*
};
#endif

static void __secure_computing_strict(int this_syscall)
{
	intsyscall_whitelist = mode1_syscalls;
#ifdef CONFIG_COMPAT
	if (in_compat_syscall())
		syscall_whitelist = mode1_syscalls_32;
#endif
	do {
		if (*syscall_whitelist == this_syscall)
			return;
	} while (*++syscall_whitelist);

#ifdef SECCOMP_DEBUG
	dump_stack();
#endif
	audit_seccomp(this_syscall, SIGKILL, SECCOMP_RET_KILL);
	do_exit(SIGKILL);
}

#ifndef CONFIG_HAVE_ARCH_SECCOMP_FILTER
void secure_computing_strict(int this_syscall)
{
	int mode = current->seccomp.mode;

	if (config_enabled(CONFIG_CHECKPOINT_RESTORE) &&
	    unlikely(current->ptrace & PT_SUSPEND_SECCOMP))
		return;

	if (mode == SECCOMP_MODE_DISABLED)
		return;
	else if (mode == SECCOMP_MODE_STRICT)
		__secure_computing_strict(this_syscall);
	else
		BUG();
}
#else
int __secure_computing(void)
{
	u32 phase1_result = seccomp_phase1(NULL);

	if (likely(phase1_result == SECCOMP_PHASE1_OK))
		return 0;
	else if (likely(phase1_result == SECCOMP_PHASE1_SKIP))
		return -1;
	else
		return seccomp_phase2(phase1_result);
}

#ifdef CONFIG_SECCOMP_FILTER
static u32 __seccomp_phase1_filter(int this_syscall, struct seccomp_datasd)
{
	u32 filter_ret, action;
	int data;

	*/
	 Make sure that any changes to mode from another thread have
	 been seen after TIF_SECCOMP was seen.
	 /*
	rmb();

	filter_ret = seccomp_run_filters(sd);
	data = filter_ret & SECCOMP_RET_DATA;
	action = filter_ret & SECCOMP_RET_ACTION;

	switch (action) {
	case SECCOMP_RET_ERRNO:
		*/ Set low-order bits as an errno, capped at MAX_ERRNO. /*
		if (data > MAX_ERRNO)
			data = MAX_ERRNO;
		syscall_set_return_value(current, task_pt_regs(current),
					 -data, 0);
		goto skip;

	case SECCOMP_RET_TRAP:
		*/ Show the handler the original registers. /*
		syscall_rollback(current, task_pt_regs(current));
		*/ Let the filter pass back 16 bits of data. /*
		seccomp_send_sigsys(this_syscall, data);
		goto skip;

	case SECCOMP_RET_TRACE:
		return filter_ret; / Save the rest for phase 2. /*

	case SECCOMP_RET_ALLOW:
		return SECCOMP_PHASE1_OK;

	case SECCOMP_RET_KILL:
	default:
		audit_seccomp(this_syscall, SIGSYS, action);
		do_exit(SIGSYS);
	}

	unreachable();

skip:
	audit_seccomp(this_syscall, 0, action);
	return SECCOMP_PHASE1_SKIP;
}
#endif

*/
 seccomp_phase1() - run fast path seccomp checks on the current syscall
 @arg sd: The seccomp_data or NULL

 This only reads pt_regs via the syscall_xyz helpers.  The only change
 it will make to pt_regs is via syscall_set_return_value, and it will
 only do that if it returns SECCOMP_PHASE1_SKIP.

 If sd is provided, it will not read pt_regs at all.

 It may also call do_exit or force a signal; these actions must be
 safe.

 If it returns SECCOMP_PHASE1_OK, the syscall passes checks and should
 be processed normally.

 If it returns SECCOMP_PHASE1_SKIP, then the syscall should not be
 invoked.  In this case, seccomp_phase1 will have set the return value
 using syscall_set_return_value.

 If it returns anything else, then the return value should be passed
 to seccomp_phase2 from a context in which ptrace hooks are safe.
 /*
u32 seccomp_phase1(struct seccomp_datasd)
{
	int mode = current->seccomp.mode;
	int this_syscall = sd ? sd->nr :
		syscall_get_nr(current, task_pt_regs(current));

	if (config_enabled(CONFIG_CHECKPOINT_RESTORE) &&
	    unlikely(current->ptrace & PT_SUSPEND_SECCOMP))
		return SECCOMP_PHASE1_OK;

	switch (mode) {
	case SECCOMP_MODE_STRICT:
		__secure_computing_strict(this_syscall); / may call do_exit /*
		return SECCOMP_PHASE1_OK;
#ifdef CONFIG_SECCOMP_FILTER
	case SECCOMP_MODE_FILTER:
		return __seccomp_phase1_filter(this_syscall, sd);
#endif
	default:
		BUG();
	}
}

*/
 seccomp_phase2() - finish slow path seccomp work for the current syscall
 @phase1_result: The return value from seccomp_phase1()

 This must be called from a context in which ptrace hooks can be used.

 Returns 0 if the syscall should be processed or -1 to skip the syscall.
 /*
int seccomp_phase2(u32 phase1_result)
{
	struct pt_regsregs = task_pt_regs(current);
	u32 action = phase1_result & SECCOMP_RET_ACTION;
	int data = phase1_result & SECCOMP_RET_DATA;

	BUG_ON(action != SECCOMP_RET_TRACE);

	audit_seccomp(syscall_get_nr(current, regs), 0, action);

	*/ Skip these calls if there is no tracer. /*
	if (!ptrace_event_enabled(current, PTRACE_EVENT_SECCOMP)) {
		syscall_set_return_value(current, regs,
					 -ENOSYS, 0);
		return -1;
	}

	*/ Allow the BPF to provide the event message /*
	ptrace_event(PTRACE_EVENT_SECCOMP, data);
	*/
	 The delivery of a fatal signal during event
	 notification may silently skip tracer notification.
	 Terminating the task now avoids executing a system
	 call that may not be intended.
	 /*
	if (fatal_signal_pending(current))
		do_exit(SIGSYS);
	if (syscall_get_nr(current, regs) < 0)
		return -1; / Explicit request to skip. /*

	return 0;
}
#endif */ CONFIG_HAVE_ARCH_SECCOMP_FILTER /*

long prctl_get_seccomp(void)
{
	return current->seccomp.mode;
}

*/
 seccomp_set_mode_strict: internal function for setting strict seccomp

 Once current->seccomp.mode is non-zero, it may not be changed.

 Returns 0 on success or -EINVAL on failure.
 /*
static long seccomp_set_mode_strict(void)
{
	const unsigned long seccomp_mode = SECCOMP_MODE_STRICT;
	long ret = -EINVAL;

	spin_lock_irq(&current->sighand->siglock);

	if (!seccomp_may_assign_mode(seccomp_mode))
		goto out;

#ifdef TIF_NOTSC
	disable_TSC();
#endif
	seccomp_assign_mode(current, seccomp_mode);
	ret = 0;

out:
	spin_unlock_irq(&current->sighand->siglock);

	return ret;
}

#ifdef CONFIG_SECCOMP_FILTER
*/
 seccomp_set_mode_filter: internal function for setting seccomp filter
 @flags:  flags to change filter behavior
 @filter: struct sock_fprog containing filter

 This function may be called repeatedly to install additional filters.
 Every filter successfully installed will be evaluated (in reverse order)
 for each system call the task makes.

 Once current->seccomp.mode is non-zero, it may not be changed.

 Returns 0 on success or -EINVAL on failure.
 /*
static long seccomp_set_mode_filter(unsigned int flags,
				    const char __userfilter)
{
	const unsigned long seccomp_mode = SECCOMP_MODE_FILTER;
	struct seccomp_filterprepared = NULL;
	long ret = -EINVAL;

	*/ Validate flags. /*
	if (flags & ~SECCOMP_FILTER_FLAG_MASK)
		return -EINVAL;

	*/ Prepare the new filter before holding any locks. /*
	prepared = seccomp_prepare_user_filter(filter);
	if (IS_ERR(prepared))
		return PTR_ERR(prepared);

	*/
	 Make sure we cannot change seccomp or nnp state via TSYNC
	 while another thread is in the middle of calling exec.
	 /*
	if (flags & SECCOMP_FILTER_FLAG_TSYNC &&
	    mutex_lock_killable(&current->signal->cred_guard_mutex))
		goto out_free;

	spin_lock_irq(&current->sighand->siglock);

	if (!seccomp_may_assign_mode(seccomp_mode))
		goto out;

	ret = seccomp_attach_filter(flags, prepared);
	if (ret)
		goto out;
	*/ Do not free the successfully attached filter. /*
	prepared = NULL;

	seccomp_assign_mode(current, seccomp_mode);
out:
	spin_unlock_irq(&current->sighand->siglock);
	if (flags & SECCOMP_FILTER_FLAG_TSYNC)
		mutex_unlock(&current->signal->cred_guard_mutex);
out_free:
	seccomp_filter_free(prepared);
	return ret;
}
#else
static inline long seccomp_set_mode_filter(unsigned int flags,
					   const char __userfilter)
{
	return -EINVAL;
}
#endif

*/ Common entry point for both prctl and syscall. /*
static long do_seccomp(unsigned int op, unsigned int flags,
		       const char __useruargs)
{
	switch (op) {
	case SECCOMP_SET_MODE_STRICT:
		if (flags != 0 || uargs != NULL)
			return -EINVAL;
		return seccomp_set_mode_strict();
	case SECCOMP_SET_MODE_FILTER:
		return seccomp_set_mode_filter(flags, uargs);
	default:
		return -EINVAL;
	}
}

SYSCALL_DEFINE3(seccomp, unsigned int, op, unsigned int, flags,
			 const char __user, uargs)
{
	return do_seccomp(op, flags, uargs);
}

*/
 prctl_set_seccomp: configures current->seccomp.mode
 @seccomp_mode: requested mode to use
 @filter: optional struct sock_fprog for use with SECCOMP_MODE_FILTER

 Returns 0 on success or -EINVAL on failure.
 /*
long prctl_set_seccomp(unsigned long seccomp_mode, char __userfilter)
{
	unsigned int op;
	char __useruargs;

	switch (seccomp_mode) {
	case SECCOMP_MODE_STRICT:
		op = SECCOMP_SET_MODE_STRICT;
		*/
		 Setting strict mode through prctl always ignored filter,
		 so make sure it is always NULL here to pass the internal
		 check in do_seccomp().
		 /*
		uargs = NULL;
		break;
	case SECCOMP_MODE_FILTER:
		op = SECCOMP_SET_MODE_FILTER;
		uargs = filter;
		break;
	default:
		return -EINVAL;
	}

	*/ prctl interface doesn't have flags, so they are always zero. /*
	return do_seccomp(op, 0, uargs);
}

#if defined(CONFIG_SECCOMP_FILTER) && defined(CONFIG_CHECKPOINT_RESTORE)
long seccomp_get_filter(struct task_structtask, unsigned long filter_off,
			void __userdata)
{
	struct seccomp_filterfilter;
	struct sock_fprog_kernfprog;
	long ret;
	unsigned long count = 0;

	if (!capable(CAP_SYS_ADMIN) ||
	    current->seccomp.mode != SECCOMP_MODE_DISABLED) {
		return -EACCES;
	}

	spin_lock_irq(&task->sighand->siglock);
	if (task->seccomp.mode != SECCOMP_MODE_FILTER) {
		ret = -EINVAL;
		goto out;
	}

	filter = task->seccomp.filter;
	while (filter) {
		filter = filter->prev;
		count++;
	}

	if (filter_off >= count) {
		ret = -ENOENT;
		goto out;
	}
	count -= filter_off;

	filter = task->seccomp.filter;
	while (filter && count > 1) {
		filter = filter->prev;
		count--;
	}

	if (WARN_ON(count != 1 || !filter)) {
		*/ The filter tree shouldn't shrink while we're using it. /*
		ret = -ENOENT;
		goto out;
	}

	fprog = filter->prog->orig_prog;
	if (!fprog) {
		*/ This must be a new non-cBPF filter, since we save every
		 every cBPF filter's orig_prog above when
		 CONFIG_CHECKPOINT_RESTORE is enabled.
		 /*
		ret = -EMEDIUMTYPE;
		goto out;
	}

	ret = fprog->len;
	if (!data)
		goto out;

	get_seccomp_filter(task);
	spin_unlock_irq(&task->sighand->siglock);

	if (copy_to_user(data, fprog->filter, bpf_classic_proglen(fprog)))
		ret = -EFAULT;

	put_seccomp_filter(task);
	return ret;

out:
	spin_unlock_irq(&task->sighand->siglock);
	return ret;
}
#endif
*/

  linux/kernel/signal.c

  Copyright (C) 1991, 1992  Linus Torvalds

  1997-11-02  Modified for POSIX.1b signals by Richard Henderson

  2003-06-02  Jim Houston - Concurrent Computer Corp.
		Changes to use preallocated sigqueue structures
		to allow signals to be sent reliably.
 /*

#include <linux/slab.h>
#include <linux/export.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/tty.h>
#include <linux/binfmts.h>
#include <linux/coredump.h>
#include <linux/security.h>
#include <linux/syscalls.h>
#include <linux/ptrace.h>
#include <linux/signal.h>
#include <linux/signalfd.h>
#include <linux/ratelimit.h>
#include <linux/tracehook.h>
#include <linux/capability.h>
#include <linux/freezer.h>
#include <linux/pid_namespace.h>
#include <linux/nsproxy.h>
#include <linux/user_namespace.h>
#include <linux/uprobes.h>
#include <linux/compat.h>
#include <linux/cn_proc.h>
#include <linux/compiler.h>

#define CREATE_TRACE_POINTS
#include <trace/events/signal.h>

#include <asm/param.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <asm/siginfo.h>
#include <asm/cacheflush.h>
#include "audit.h"	*/ audit_signal_info() /*

*/
 SLAB caches for signal bits.
 /*

static struct kmem_cachesigqueue_cachep;

int print_fatal_signals __read_mostly;

static void __usersig_handler(struct task_structt, int sig)
{
	return t->sighand->action[sig - 1].sa.sa_handler;
}

static int sig_handler_ignored(void __userhandler, int sig)
{
	*/ Is it explicitly or implicitly ignored? /*
	return handler == SIG_IGN ||
		(handler == SIG_DFL && sig_kernel_ignore(sig));
}

static int sig_task_ignored(struct task_structt, int sig, bool force)
{
	void __userhandler;

	handler = sig_handler(t, sig);

	if (unlikely(t->signal->flags & SIGNAL_UNKILLABLE) &&
			handler == SIG_DFL && !force)
		return 1;

	return sig_handler_ignored(handler, sig);
}

static int sig_ignored(struct task_structt, int sig, bool force)
{
	*/
	 Blocked signals are never ignored, since the
	 signal handler may change by the time it is
	 unblocked.
	 /*
	if (sigismember(&t->blocked, sig) || sigismember(&t->real_blocked, sig))
		return 0;

	if (!sig_task_ignored(t, sig, force))
		return 0;

	*/
	 Tracers may want to know about even ignored signals.
	 /*
	return !t->ptrace;
}

*/
 Re-calculate pending state from the set of locally pending
 signals, globally pending signals, and blocked signals.
 /*
static inline int has_pending_signals(sigset_tsignal, sigset_tblocked)
{
	unsigned long ready;
	long i;

	switch (_NSIG_WORDS) {
	default:
		for (i = _NSIG_WORDS, ready = 0; --i >= 0 ;)
			ready |= signal->sig[i] &~ blocked->sig[i];
		break;

	case 4: ready  = signal->sig[3] &~ blocked->sig[3];
		ready |= signal->sig[2] &~ blocked->sig[2];
		ready |= signal->sig[1] &~ blocked->sig[1];
		ready |= signal->sig[0] &~ blocked->sig[0];
		break;

	case 2: ready  = signal->sig[1] &~ blocked->sig[1];
		ready |= signal->sig[0] &~ blocked->sig[0];
		break;

	case 1: ready  = signal->sig[0] &~ blocked->sig[0];
	}
	return ready !=	0;
}

#define PENDING(p,b) has_pending_signals(&(p)->signal, (b))

static int recalc_sigpending_tsk(struct task_structt)
{
	if ((t->jobctl & JOBCTL_PENDING_MASK) ||
	    PENDING(&t->pending, &t->blocked) ||
	    PENDING(&t->signal->shared_pending, &t->blocked)) {
		set_tsk_thread_flag(t, TIF_SIGPENDING);
		return 1;
	}
	*/
	 We must never clear the flag in another thread, or in current
	 when it's possible the current syscall is returning -ERESTART*.
	 So we don't clear it here, and only callers who know they should do.
	 /*
	return 0;
}

*/
 After recalculating TIF_SIGPENDING, we need to make sure the task wakes up.
 This is superfluous when called on current, the wakeup is a harmless no-op.
 /*
void recalc_sigpending_and_wake(struct task_structt)
{
	if (recalc_sigpending_tsk(t))
		signal_wake_up(t, 0);
}

void recalc_sigpending(void)
{
	if (!recalc_sigpending_tsk(current) && !freezing(current))
		clear_thread_flag(TIF_SIGPENDING);

}

*/ Given the mask, find the first available signal that should be serviced. /*

#define SYNCHRONOUS_MASK \
	(sigmask(SIGSEGV) | sigmask(SIGBUS) | sigmask(SIGILL) | \
	 sigmask(SIGTRAP) | sigmask(SIGFPE) | sigmask(SIGSYS))

int next_signal(struct sigpendingpending, sigset_tmask)
{
	unsigned long i,s,m, x;
	int sig = 0;

	s = pending->signal.sig;
	m = mask->sig;

	*/
	 Handle the first word specially: it contains the
	 synchronous signals that need to be dequeued first.
	 /*
	x =s &~m;
	if (x) {
		if (x & SYNCHRONOUS_MASK)
			x &= SYNCHRONOUS_MASK;
		sig = ffz(~x) + 1;
		return sig;
	}

	switch (_NSIG_WORDS) {
	default:
		for (i = 1; i < _NSIG_WORDS; ++i) {
			x =++s &~++m;
			if (!x)
				continue;
			sig = ffz(~x) + i*_NSIG_BPW + 1;
			break;
		}
		break;

	case 2:
		x = s[1] &~ m[1];
		if (!x)
			break;
		sig = ffz(~x) + _NSIG_BPW + 1;
		break;

	case 1:
		*/ Nothing to do /*
		break;
	}

	return sig;
}

static inline void print_dropped_signal(int sig)
{
	static DEFINE_RATELIMIT_STATE(ratelimit_state, 5 HZ, 10);

	if (!print_fatal_signals)
		return;

	if (!__ratelimit(&ratelimit_state))
		return;

	printk(KERN_INFO "%s/%d: reached RLIMIT_SIGPENDING, dropped signal %d\n",
				current->comm, current->pid, sig);
}

*/
 task_set_jobctl_pending - set jobctl pending bits
 @task: target task
 @mask: pending bits to set

 Clear @mask from @task->jobctl.  @mask must be subset of
 %JOBCTL_PENDING_MASK | %JOBCTL_STOP_CONSUME | %JOBCTL_STOP_SIGMASK |
 %JOBCTL_TRAPPING.  If stop signo is being set, the existing signo is
 cleared.  If @task is already being killed or exiting, this function
 becomes noop.

 CONTEXT:
 Must be called with @task->sighand->siglock held.

 RETURNS:
 %true if @mask is set, %false if made noop because @task was dying.
 /*
bool task_set_jobctl_pending(struct task_structtask, unsigned long mask)
{
	BUG_ON(mask & ~(JOBCTL_PENDING_MASK | JOBCTL_STOP_CONSUME |
			JOBCTL_STOP_SIGMASK | JOBCTL_TRAPPING));
	BUG_ON((mask & JOBCTL_TRAPPING) && !(mask & JOBCTL_PENDING_MASK));

	if (unlikely(fatal_signal_pending(task) || (task->flags & PF_EXITING)))
		return false;

	if (mask & JOBCTL_STOP_SIGMASK)
		task->jobctl &= ~JOBCTL_STOP_SIGMASK;

	task->jobctl |= mask;
	return true;
}

*/
 task_clear_jobctl_trapping - clear jobctl trapping bit
 @task: target task

 If JOBCTL_TRAPPING is set, a ptracer is waiting for us to enter TRACED.
 Clear it and wake up the ptracer.  Note that we don't need any further
 locking.  @task->siglock guarantees that @task->parent points to the
 ptracer.

 CONTEXT:
 Must be called with @task->sighand->siglock held.
 /*
void task_clear_jobctl_trapping(struct task_structtask)
{
	if (unlikely(task->jobctl & JOBCTL_TRAPPING)) {
		task->jobctl &= ~JOBCTL_TRAPPING;
		smp_mb();	*/ advised by wake_up_bit() /*
		wake_up_bit(&task->jobctl, JOBCTL_TRAPPING_BIT);
	}
}

*/
 task_clear_jobctl_pending - clear jobctl pending bits
 @task: target task
 @mask: pending bits to clear

 Clear @mask from @task->jobctl.  @mask must be subset of
 %JOBCTL_PENDING_MASK.  If %JOBCTL_STOP_PENDING is being cleared, other
 STOP bits are cleared together.

 If clearing of @mask leaves no stop or trap pending, this function calls
 task_clear_jobctl_trapping().

 CONTEXT:
 Must be called with @task->sighand->siglock held.
 /*
void task_clear_jobctl_pending(struct task_structtask, unsigned long mask)
{
	BUG_ON(mask & ~JOBCTL_PENDING_MASK);

	if (mask & JOBCTL_STOP_PENDING)
		mask |= JOBCTL_STOP_CONSUME | JOBCTL_STOP_DEQUEUED;

	task->jobctl &= ~mask;

	if (!(task->jobctl & JOBCTL_PENDING_MASK))
		task_clear_jobctl_trapping(task);
}

*/
 task_participate_group_stop - participate in a group stop
 @task: task participating in a group stop

 @task has %JOBCTL_STOP_PENDING set and is participating in a group stop.
 Group stop states are cleared and the group stop count is consumed if
 %JOBCTL_STOP_CONSUME was set.  If the consumption completes the group
 stop, the appropriate %SIGNAL_* flags are set.

 CONTEXT:
 Must be called with @task->sighand->siglock held.

 RETURNS:
 %true if group stop completion should be notified to the parent, %false
 otherwise.
 /*
static bool task_participate_group_stop(struct task_structtask)
{
	struct signal_structsig = task->signal;
	bool consume = task->jobctl & JOBCTL_STOP_CONSUME;

	WARN_ON_ONCE(!(task->jobctl & JOBCTL_STOP_PENDING));

	task_clear_jobctl_pending(task, JOBCTL_STOP_PENDING);

	if (!consume)
		return false;

	if (!WARN_ON_ONCE(sig->group_stop_count == 0))
		sig->group_stop_count--;

	*/
	 Tell the caller to notify completion iff we are entering into a
	 fresh group stop.  Read comment in do_signal_stop() for details.
	 /*
	if (!sig->group_stop_count && !(sig->flags & SIGNAL_STOP_STOPPED)) {
		sig->flags = SIGNAL_STOP_STOPPED;
		return true;
	}
	return false;
}

*/
 allocate a new signal queue record
 - this may be called without locks if and only if t == current, otherwise an
   appropriate lock must be held to stop the target task from exiting
 /*
static struct sigqueue
__sigqueue_alloc(int sig, struct task_structt, gfp_t flags, int override_rlimit)
{
	struct sigqueueq = NULL;
	struct user_structuser;

	*/
	 Protect access to @t credentials. This can go away when all
	 callers hold rcu read lock.
	 /*
	rcu_read_lock();
	user = get_uid(__task_cred(t)->user);
	atomic_inc(&user->sigpending);
	rcu_read_unlock();

	if (override_rlimit ||
	    atomic_read(&user->sigpending) <=
			task_rlimit(t, RLIMIT_SIGPENDING)) {
		q = kmem_cache_alloc(sigqueue_cachep, flags);
	} else {
		print_dropped_signal(sig);
	}

	if (unlikely(q == NULL)) {
		atomic_dec(&user->sigpending);
		free_uid(user);
	} else {
		INIT_LIST_HEAD(&q->list);
		q->flags = 0;
		q->user = user;
	}

	return q;
}

static void __sigqueue_free(struct sigqueueq)
{
	if (q->flags & SIGQUEUE_PREALLOC)
		return;
	atomic_dec(&q->user->sigpending);
	free_uid(q->user);
	kmem_cache_free(sigqueue_cachep, q);
}

void flush_sigqueue(struct sigpendingqueue)
{
	struct sigqueueq;

	sigemptyset(&queue->signal);
	while (!list_empty(&queue->list)) {
		q = list_entry(queue->list.next, struct sigqueue , list);
		list_del_init(&q->list);
		__sigqueue_free(q);
	}
}

*/
 Flush all pending signals for this kthread.
 /*
void flush_signals(struct task_structt)
{
	unsigned long flags;

	spin_lock_irqsave(&t->sighand->siglock, flags);
	clear_tsk_thread_flag(t, TIF_SIGPENDING);
	flush_sigqueue(&t->pending);
	flush_sigqueue(&t->signal->shared_pending);
	spin_unlock_irqrestore(&t->sighand->siglock, flags);
}

static void __flush_itimer_signals(struct sigpendingpending)
{
	sigset_t signal, retain;
	struct sigqueueq,n;

	signal = pending->signal;
	sigemptyset(&retain);

	list_for_each_entry_safe(q, n, &pending->list, list) {
		int sig = q->info.si_signo;

		if (likely(q->info.si_code != SI_TIMER)) {
			sigaddset(&retain, sig);
		} else {
			sigdelset(&signal, sig);
			list_del_init(&q->list);
			__sigqueue_free(q);
		}
	}

	sigorsets(&pending->signal, &signal, &retain);
}

void flush_itimer_signals(void)
{
	struct task_structtsk = current;
	unsigned long flags;

	spin_lock_irqsave(&tsk->sighand->siglock, flags);
	__flush_itimer_signals(&tsk->pending);
	__flush_itimer_signals(&tsk->signal->shared_pending);
	spin_unlock_irqrestore(&tsk->sighand->siglock, flags);
}

void ignore_signals(struct task_structt)
{
	int i;

	for (i = 0; i < _NSIG; ++i)
		t->sighand->action[i].sa.sa_handler = SIG_IGN;

	flush_signals(t);
}

*/
 Flush all handlers for a task.
 /*

void
flush_signal_handlers(struct task_structt, int force_default)
{
	int i;
	struct k_sigactionka = &t->sighand->action[0];
	for (i = _NSIG ; i != 0 ; i--) {
		if (force_default || ka->sa.sa_handler != SIG_IGN)
			ka->sa.sa_handler = SIG_DFL;
		ka->sa.sa_flags = 0;
#ifdef __ARCH_HAS_SA_RESTORER
		ka->sa.sa_restorer = NULL;
#endif
		sigemptyset(&ka->sa.sa_mask);
		ka++;
	}
}

int unhandled_signal(struct task_structtsk, int sig)
{
	void __userhandler = tsk->sighand->action[sig-1].sa.sa_handler;
	if (is_global_init(tsk))
		return 1;
	if (handler != SIG_IGN && handler != SIG_DFL)
		return 0;
	*/ if ptraced, let the tracer determine /*
	return !tsk->ptrace;
}

static void collect_signal(int sig, struct sigpendinglist, siginfo_tinfo)
{
	struct sigqueueq,first = NULL;

	*/
	 Collect the siginfo appropriate to this signal.  Check if
	 there is another siginfo for the same signal.
	/*
	list_for_each_entry(q, &list->list, list) {
		if (q->info.si_signo == sig) {
			if (first)
				goto still_pending;
			first = q;
		}
	}

	sigdelset(&list->signal, sig);

	if (first) {
still_pending:
		list_del_init(&first->list);
		copy_siginfo(info, &first->info);
		__sigqueue_free(first);
	} else {
		*/
		 Ok, it wasn't in the queue.  This must be
		 a fast-pathed signal or we must have been
		 out of queue space.  So zero out the info.
		 /*
		info->si_signo = sig;
		info->si_errno = 0;
		info->si_code = SI_USER;
		info->si_pid = 0;
		info->si_uid = 0;
	}
}

static int __dequeue_signal(struct sigpendingpending, sigset_tmask,
			siginfo_tinfo)
{
	int sig = next_signal(pending, mask);

	if (sig)
		collect_signal(sig, pending, info);
	return sig;
}

*/
 Dequeue a signal and return the element to the caller, which is
 expected to free it.

 All callers have to hold the siglock.
 /*
int dequeue_signal(struct task_structtsk, sigset_tmask, siginfo_tinfo)
{
	int signr;

	*/ We only dequeue private signals from ourselves, we don't let
	 signalfd steal them
	 /*
	signr = __dequeue_signal(&tsk->pending, mask, info);
	if (!signr) {
		signr = __dequeue_signal(&tsk->signal->shared_pending,
					 mask, info);
		*/
		 itimer signal ?
		
		 itimers are process shared and we restart periodic
		 itimers in the signal delivery path to prevent DoS
		 attacks in the high resolution timer case. This is
		 compliant with the old way of self-restarting
		 itimers, as the SIGALRM is a legacy signal and only
		 queued once. Changing the restart behaviour to
		 restart the timer in the signal dequeue path is
		 reducing the timer noise on heavy loaded !highres
		 systems too.
		 /*
		if (unlikely(signr == SIGALRM)) {
			struct hrtimertmr = &tsk->signal->real_timer;

			if (!hrtimer_is_queued(tmr) &&
			    tsk->signal->it_real_incr.tv64 != 0) {
				hrtimer_forward(tmr, tmr->base->get_time(),
						tsk->signal->it_real_incr);
				hrtimer_restart(tmr);
			}
		}
	}

	recalc_sigpending();
	if (!signr)
		return 0;

	if (unlikely(sig_kernel_stop(signr))) {
		*/
		 Set a marker that we have dequeued a stop signal.  Our
		 caller might release the siglock and then the pending
		 stop signal it is about to process is no longer in the
		 pending bitmasks, but must still be cleared by a SIGCONT
		 (and overruled by a SIGKILL).  So those cases clear this
		 shared flag after we've set it.  Note that this flag may
		 remain set after the signal we return is ignored or
		 handled.  That doesn't matter because its only purpose
		 is to alert stop-signal processing code when another
		 processor has come along and cleared the flag.
		 /*
		current->jobctl |= JOBCTL_STOP_DEQUEUED;
	}
	if ((info->si_code & __SI_MASK) == __SI_TIMER && info->si_sys_private) {
		*/
		 Release the siglock to ensure proper locking order
		 of timer locks outside of siglocks.  Note, we leave
		 irqs disabled here, since the posix-timers code is
		 about to disable them again anyway.
		 /*
		spin_unlock(&tsk->sighand->siglock);
		do_schedule_next_timer(info);
		spin_lock(&tsk->sighand->siglock);
	}
	return signr;
}

*/
 Tell a process that it has a new active signal..

 NOTE! we rely on the previous spin_lock to
 lock interrupts for us! We can only be called with
 "siglock" held, and the local interrupt must
 have been disabled when that got acquired!

 No need to set need_resched since signal event passing
 goes through ->blocked
 /*
void signal_wake_up_state(struct task_structt, unsigned int state)
{
	set_tsk_thread_flag(t, TIF_SIGPENDING);
	*/
	 TASK_WAKEKILL also means wake it up in the stopped/traced/killable
	 case. We don't check t->state here because there is a race with it
	 executing another processor and just now entering stopped state.
	 By using wake_up_state, we ensure the process will wake up and
	 handle its death signal.
	 /*
	if (!wake_up_state(t, state | TASK_INTERRUPTIBLE))
		kick_process(t);
}

*/
 Remove signals in mask from the pending set and queue.
 Returns 1 if any signals were found.

 All callers must be holding the siglock.
 /*
static int flush_sigqueue_mask(sigset_tmask, struct sigpendings)
{
	struct sigqueueq,n;
	sigset_t m;

	sigandsets(&m, mask, &s->signal);
	if (sigisemptyset(&m))
		return 0;

	sigandnsets(&s->signal, &s->signal, mask);
	list_for_each_entry_safe(q, n, &s->list, list) {
		if (sigismember(mask, q->info.si_signo)) {
			list_del_init(&q->list);
			__sigqueue_free(q);
		}
	}
	return 1;
}

static inline int is_si_special(const struct siginfoinfo)
{
	return info <= SEND_SIG_FORCED;
}

static inline bool si_fromuser(const struct siginfoinfo)
{
	return info == SEND_SIG_NOINFO ||
		(!is_si_special(info) && SI_FROMUSER(info));
}

*/
 called with RCU read lock from check_kill_permission()
 /*
static int kill_ok_by_cred(struct task_structt)
{
	const struct credcred = current_cred();
	const struct credtcred = __task_cred(t);

	if (uid_eq(cred->euid, tcred->suid) ||
	    uid_eq(cred->euid, tcred->uid)  ||
	    uid_eq(cred->uid,  tcred->suid) ||
	    uid_eq(cred->uid,  tcred->uid))
		return 1;

	if (ns_capable(tcred->user_ns, CAP_KILL))
		return 1;

	return 0;
}

*/
 Bad permissions for sending the signal
 - the caller must hold the RCU read lock
 /*
static int check_kill_permission(int sig, struct siginfoinfo,
				 struct task_structt)
{
	struct pidsid;
	int error;

	if (!valid_signal(sig))
		return -EINVAL;

	if (!si_fromuser(info))
		return 0;

	error = audit_signal_info(sig, t);/ Let audit system see the signal /*
	if (error)
		return error;

	if (!same_thread_group(current, t) &&
	    !kill_ok_by_cred(t)) {
		switch (sig) {
		case SIGCONT:
			sid = task_session(t);
			*/
			 We don't return the error if sid == NULL. The
			 task was unhashed, the caller must notice this.
			 /*
			if (!sid || sid == task_session(current))
				break;
		default:
			return -EPERM;
		}
	}

	return security_task_kill(t, info, sig, 0);
}

*/
 ptrace_trap_notify - schedule trap to notify ptracer
 @t: tracee wanting to notify tracer

 This function schedules sticky ptrace trap which is cleared on the next
 TRAP_STOP to notify ptracer of an event.  @t must have been seized by
 ptracer.

 If @t is running, STOP trap will be taken.  If trapped for STOP and
 ptracer is listening for events, tracee is woken up so that it can
 re-trap for the new event.  If trapped otherwise, STOP trap will be
 eventually taken without returning to userland after the existing traps
 are finished by PTRACE_CONT.

 CONTEXT:
 Must be called with @task->sighand->siglock held.
 /*
static void ptrace_trap_notify(struct task_structt)
{
	WARN_ON_ONCE(!(t->ptrace & PT_SEIZED));
	assert_spin_locked(&t->sighand->siglock);

	task_set_jobctl_pending(t, JOBCTL_TRAP_NOTIFY);
	ptrace_signal_wake_up(t, t->jobctl & JOBCTL_LISTENING);
}

*/
 Handle magic process-wide effects of stop/continue signals. Unlike
 the signal actions, these happen immediately at signal-generation
 time regardless of blocking, ignoring, or handling.  This does the
 actual continuing for SIGCONT, but not the actual stopping for stop
 signals. The process stop is done as a signal action for SIG_DFL.

 Returns true if the signal should be actually delivered, otherwise
 it should be dropped.
 /*
static bool prepare_signal(int sig, struct task_structp, bool force)
{
	struct signal_structsignal = p->signal;
	struct task_structt;
	sigset_t flush;

	if (signal->flags & (SIGNAL_GROUP_EXIT | SIGNAL_GROUP_COREDUMP)) {
		if (!(signal->flags & SIGNAL_GROUP_EXIT))
			return sig == SIGKILL;
		*/
		 The process is in the middle of dying, nothing to do.
		 /*
	} else if (sig_kernel_stop(sig)) {
		*/
		 This is a stop signal.  Remove SIGCONT from all queues.
		 /*
		siginitset(&flush, sigmask(SIGCONT));
		flush_sigqueue_mask(&flush, &signal->shared_pending);
		for_each_thread(p, t)
			flush_sigqueue_mask(&flush, &t->pending);
	} else if (sig == SIGCONT) {
		unsigned int why;
		*/
		 Remove all stop signals from all queues, wake all threads.
		 /*
		siginitset(&flush, SIG_KERNEL_STOP_MASK);
		flush_sigqueue_mask(&flush, &signal->shared_pending);
		for_each_thread(p, t) {
			flush_sigqueue_mask(&flush, &t->pending);
			task_clear_jobctl_pending(t, JOBCTL_STOP_PENDING);
			if (likely(!(t->ptrace & PT_SEIZED)))
				wake_up_state(t, __TASK_STOPPED);
			else
				ptrace_trap_notify(t);
		}

		*/
		 Notify the parent with CLD_CONTINUED if we were stopped.
		
		 If we were in the middle of a group stop, we pretend it
		 was already finished, and then continued. Since SIGCHLD
		 doesn't queue we report only CLD_STOPPED, as if the next
		 CLD_CONTINUED was dropped.
		 /*
		why = 0;
		if (signal->flags & SIGNAL_STOP_STOPPED)
			why |= SIGNAL_CLD_CONTINUED;
		else if (signal->group_stop_count)
			why |= SIGNAL_CLD_STOPPED;

		if (why) {
			*/
			 The first thread which returns from do_signal_stop()
			 will take ->siglock, notice SIGNAL_CLD_MASK, and
			 notify its parent. See get_signal_to_deliver().
			 /*
			signal->flags = why | SIGNAL_STOP_CONTINUED;
			signal->group_stop_count = 0;
			signal->group_exit_code = 0;
		}
	}

	return !sig_ignored(p, sig, force);
}

*/
 Test if P wants to take SIG.  After we've checked all threads with this,
 it's equivalent to finding no threads not blocking SIG.  Any threads not
 blocking SIG were ruled out because they are not running and already
 have pending signals.  Such threads will dequeue from the shared queue
 as soon as they're available, so putting the signal on the shared queue
 will be equivalent to sending it to one such thread.
 /*
static inline int wants_signal(int sig, struct task_structp)
{
	if (sigismember(&p->blocked, sig))
		return 0;
	if (p->flags & PF_EXITING)
		return 0;
	if (sig == SIGKILL)
		return 1;
	if (task_is_stopped_or_traced(p))
		return 0;
	return task_curr(p) || !signal_pending(p);
}

static void complete_signal(int sig, struct task_structp, int group)
{
	struct signal_structsignal = p->signal;
	struct task_structt;

	*/
	 Now find a thread we can wake up to take the signal off the queue.
	
	 If the main thread wants the signal, it gets first crack.
	 Probably the least surprising to the average bear.
	 /*
	if (wants_signal(sig, p))
		t = p;
	else if (!group || thread_group_empty(p))
		*/
		 There is just one thread and it does not need to be woken.
		 It will dequeue unblocked signals before it runs again.
		 /*
		return;
	else {
		*/
		 Otherwise try to find a suitable thread.
		 /*
		t = signal->curr_target;
		while (!wants_signal(sig, t)) {
			t = next_thread(t);
			if (t == signal->curr_target)
				*/
				 No thread needs to be woken.
				 Any eligible threads will see
				 the signal in the queue soon.
				 /*
				return;
		}
		signal->curr_target = t;
	}

	*/
	 Found a killable thread.  If the signal will be fatal,
	 then start taking the whole group down immediately.
	 /*
	if (sig_fatal(p, sig) &&
	    !(signal->flags & (SIGNAL_UNKILLABLE | SIGNAL_GROUP_EXIT)) &&
	    !sigismember(&t->real_blocked, sig) &&
	    (sig == SIGKILL || !t->ptrace)) {
		*/
		 This signal will be fatal to the whole group.
		 /*
		if (!sig_kernel_coredump(sig)) {
			*/
			 Start a group exit and wake everybody up.
			 This way we don't have other threads
			 running and doing things after a slower
			 thread has the fatal signal pending.
			 /*
			signal->flags = SIGNAL_GROUP_EXIT;
			signal->group_exit_code = sig;
			signal->group_stop_count = 0;
			t = p;
			do {
				task_clear_jobctl_pending(t, JOBCTL_PENDING_MASK);
				sigaddset(&t->pending.signal, SIGKILL);
				signal_wake_up(t, 1);
			} while_each_thread(p, t);
			return;
		}
	}

	*/
	 The signal is already in the shared-pending queue.
	 Tell the chosen thread to wake up and dequeue it.
	 /*
	signal_wake_up(t, sig == SIGKILL);
	return;
}

static inline int legacy_queue(struct sigpendingsignals, int sig)
{
	return (sig < SIGRTMIN) && sigismember(&signals->signal, sig);
}

#ifdef CONFIG_USER_NS
static inline void userns_fixup_signal_uid(struct siginfoinfo, struct task_structt)
{
	if (current_user_ns() == task_cred_xxx(t, user_ns))
		return;

	if (SI_FROMKERNEL(info))
		return;

	rcu_read_lock();
	info->si_uid = from_kuid_munged(task_cred_xxx(t, user_ns),
					make_kuid(current_user_ns(), info->si_uid));
	rcu_read_unlock();
}
#else
static inline void userns_fixup_signal_uid(struct siginfoinfo, struct task_structt)
{
	return;
}
#endif

static int __send_signal(int sig, struct siginfoinfo, struct task_structt,
			int group, int from_ancestor_ns)
{
	struct sigpendingpending;
	struct sigqueueq;
	int override_rlimit;
	int ret = 0, result;

	assert_spin_locked(&t->sighand->siglock);

	result = TRACE_SIGNAL_IGNORED;
	if (!prepare_signal(sig, t,
			from_ancestor_ns || (info == SEND_SIG_FORCED)))
		goto ret;

	pending = group ? &t->signal->shared_pending : &t->pending;
	*/
	 Short-circuit ignored signals and support queuing
	 exactly one non-rt signal, so that we can get more
	 detailed information about the cause of the signal.
	 /*
	result = TRACE_SIGNAL_ALREADY_PENDING;
	if (legacy_queue(pending, sig))
		goto ret;

	result = TRACE_SIGNAL_DELIVERED;
	*/
	 fast-pathed signals for kernel-internal things like SIGSTOP
	 or SIGKILL.
	 /*
	if (info == SEND_SIG_FORCED)
		goto out_set;

	*/
	 Real-time signals must be queued if sent by sigqueue, or
	 some other real-time mechanism.  It is implementation
	 defined whether kill() does so.  We attempt to do so, on
	 the principle of least surprise, but since kill is not
	 allowed to fail with EAGAIN when low on memory we just
	 make sure at least one signal gets delivered and don't
	 pass on the info struct.
	 /*
	if (sig < SIGRTMIN)
		override_rlimit = (is_si_special(info) || info->si_code >= 0);
	else
		override_rlimit = 0;

	q = __sigqueue_alloc(sig, t, GFP_ATOMIC | __GFP_NOTRACK_FALSE_POSITIVE,
		override_rlimit);
	if (q) {
		list_add_tail(&q->list, &pending->list);
		switch ((unsigned long) info) {
		case (unsigned long) SEND_SIG_NOINFO:
			q->info.si_signo = sig;
			q->info.si_errno = 0;
			q->info.si_code = SI_USER;
			q->info.si_pid = task_tgid_nr_ns(current,
							task_active_pid_ns(t));
			q->info.si_uid = from_kuid_munged(current_user_ns(), current_uid());
			break;
		case (unsigned long) SEND_SIG_PRIV:
			q->info.si_signo = sig;
			q->info.si_errno = 0;
			q->info.si_code = SI_KERNEL;
			q->info.si_pid = 0;
			q->info.si_uid = 0;
			break;
		default:
			copy_siginfo(&q->info, info);
			if (from_ancestor_ns)
				q->info.si_pid = 0;
			break;
		}

		userns_fixup_signal_uid(&q->info, t);

	} else if (!is_si_special(info)) {
		if (sig >= SIGRTMIN && info->si_code != SI_USER) {
			*/
			 Queue overflow, abort.  We may abort if the
			 signal was rt and sent by user using something
			 other than kill().
			 /*
			result = TRACE_SIGNAL_OVERFLOW_FAIL;
			ret = -EAGAIN;
			goto ret;
		} else {
			*/
			 This is a silent loss of information.  We still
			 send the signal, but theinfo bits are lost.
			 /*
			result = TRACE_SIGNAL_LOSE_INFO;
		}
	}

out_set:
	signalfd_notify(t, sig);
	sigaddset(&pending->signal, sig);
	complete_signal(sig, t, group);
ret:
	trace_signal_generate(sig, info, t, group, result);
	return ret;
}

static int send_signal(int sig, struct siginfoinfo, struct task_structt,
			int group)
{
	int from_ancestor_ns = 0;

#ifdef CONFIG_PID_NS
	from_ancestor_ns = si_fromuser(info) &&
			   !task_pid_nr_ns(current, task_active_pid_ns(t));
#endif

	return __send_signal(sig, info, t, group, from_ancestor_ns);
}

static void print_fatal_signal(int signr)
{
	struct pt_regsregs = signal_pt_regs();
	printk(KERN_INFO "potentially unexpected fatal signal %d.\n", signr);

#if defined(__i386__) && !defined(__arch_um__)
	printk(KERN_INFO "code at %08lx: ", regs->ip);
	{
		int i;
		for (i = 0; i < 16; i++) {
			unsigned char insn;

			if (get_user(insn, (unsigned char)(regs->ip + i)))
				break;
			printk(KERN_CONT "%02x ", insn);
		}
	}
	printk(KERN_CONT "\n");
#endif
	preempt_disable();
	show_regs(regs);
	preempt_enable();
}

static int __init setup_print_fatal_signals(charstr)
{
	get_option (&str, &print_fatal_signals);

	return 1;
}

__setup("print-fatal-signals=", setup_print_fatal_signals);

int
__group_send_sig_info(int sig, struct siginfoinfo, struct task_structp)
{
	return send_signal(sig, info, p, 1);
}

static int
specific_send_sig_info(int sig, struct siginfoinfo, struct task_structt)
{
	return send_signal(sig, info, t, 0);
}

int do_send_sig_info(int sig, struct siginfoinfo, struct task_structp,
			bool group)
{
	unsigned long flags;
	int ret = -ESRCH;

	if (lock_task_sighand(p, &flags)) {
		ret = send_signal(sig, info, p, group);
		unlock_task_sighand(p, &flags);
	}

	return ret;
}

*/
 Force a signal that the process can't ignore: if necessary
 we unblock the signal and change any SIG_IGN to SIG_DFL.

 Note: If we unblock the signal, we always reset it to SIG_DFL,
 since we do not want to have a signal handler that was blocked
 be invoked when user space had explicitly blocked it.

 We don't want to have recursive SIGSEGV's etc, for example,
 that is why we also clear SIGNAL_UNKILLABLE.
 /*
int
force_sig_info(int sig, struct siginfoinfo, struct task_structt)
{
	unsigned long int flags;
	int ret, blocked, ignored;
	struct k_sigactionaction;

	spin_lock_irqsave(&t->sighand->siglock, flags);
	action = &t->sighand->action[sig-1];
	ignored = action->sa.sa_handler == SIG_IGN;
	blocked = sigismember(&t->blocked, sig);
	if (blocked || ignored) {
		action->sa.sa_handler = SIG_DFL;
		if (blocked) {
			sigdelset(&t->blocked, sig);
			recalc_sigpending_and_wake(t);
		}
	}
	if (action->sa.sa_handler == SIG_DFL)
		t->signal->flags &= ~SIGNAL_UNKILLABLE;
	ret = specific_send_sig_info(sig, info, t);
	spin_unlock_irqrestore(&t->sighand->siglock, flags);

	return ret;
}

*/
 Nuke all other threads in the group.
 /*
int zap_other_threads(struct task_structp)
{
	struct task_structt = p;
	int count = 0;

	p->signal->group_stop_count = 0;

	while_each_thread(p, t) {
		task_clear_jobctl_pending(t, JOBCTL_PENDING_MASK);
		count++;

		*/ Don't bother with already dead threads /*
		if (t->exit_state)
			continue;
		sigaddset(&t->pending.signal, SIGKILL);
		signal_wake_up(t, 1);
	}

	return count;
}

struct sighand_struct__lock_task_sighand(struct task_structtsk,
					   unsigned longflags)
{
	struct sighand_structsighand;

	for (;;) {
		*/
		 Disable interrupts early to avoid deadlocks.
		 See rcu_read_unlock() comment header for details.
		 /*
		local_irq_save(*flags);
		rcu_read_lock();
		sighand = rcu_dereference(tsk->sighand);
		if (unlikely(sighand == NULL)) {
			rcu_read_unlock();
			local_irq_restore(*flags);
			break;
		}
		*/
		 This sighand can be already freed and even reused, but
		 we rely on SLAB_DESTROY_BY_RCU and sighand_ctor() which
		 initializes ->siglock: this slab can't go away, it has
		 the same object type, ->siglock can't be reinitialized.
		
		 We need to ensure that tsk->sighand is still the same
		 after we take the lock, we can race with de_thread() or
		 __exit_signal(). In the latter case the next iteration
		 must see ->sighand == NULL.
		 /*
		spin_lock(&sighand->siglock);
		if (likely(sighand == tsk->sighand)) {
			rcu_read_unlock();
			break;
		}
		spin_unlock(&sighand->siglock);
		rcu_read_unlock();
		local_irq_restore(*flags);
	}

	return sighand;
}

*/
 send signal info to all the members of a group
 /*
int group_send_sig_info(int sig, struct siginfoinfo, struct task_structp)
{
	int ret;

	rcu_read_lock();
	ret = check_kill_permission(sig, info, p);
	rcu_read_unlock();

	if (!ret && sig)
		ret = do_send_sig_info(sig, info, p, true);

	return ret;
}

*/
 __kill_pgrp_info() sends a signal to a process group: this is what the tty
 control characters do (^C, ^Z etc)
 - the caller must hold at least a readlock on tasklist_lock
 /*
int __kill_pgrp_info(int sig, struct siginfoinfo, struct pidpgrp)
{
	struct task_structp = NULL;
	int retval, success;

	success = 0;
	retval = -ESRCH;
	do_each_pid_task(pgrp, PIDTYPE_PGID, p) {
		int err = group_send_sig_info(sig, info, p);
		success |= !err;
		retval = err;
	} while_each_pid_task(pgrp, PIDTYPE_PGID, p);
	return success ? 0 : retval;
}

int kill_pid_info(int sig, struct siginfoinfo, struct pidpid)
{
	int error = -ESRCH;
	struct task_structp;

	for (;;) {
		rcu_read_lock();
		p = pid_task(pid, PIDTYPE_PID);
		if (p)
			error = group_send_sig_info(sig, info, p);
		rcu_read_unlock();
		if (likely(!p || error != -ESRCH))
			return error;

		*/
		 The task was unhashed in between, try again.  If it
		 is dead, pid_task() will return NULL, if we race with
		 de_thread() it will find the new leader.
		 /*
	}
}

int kill_proc_info(int sig, struct siginfoinfo, pid_t pid)
{
	int error;
	rcu_read_lock();
	error = kill_pid_info(sig, info, find_vpid(pid));
	rcu_read_unlock();
	return error;
}

static int kill_as_cred_perm(const struct credcred,
			     struct task_structtarget)
{
	const struct credpcred = __task_cred(target);
	if (!uid_eq(cred->euid, pcred->suid) && !uid_eq(cred->euid, pcred->uid) &&
	    !uid_eq(cred->uid,  pcred->suid) && !uid_eq(cred->uid,  pcred->uid))
		return 0;
	return 1;
}

*/ like kill_pid_info(), but doesn't use uid/euid of "current" /*
int kill_pid_info_as_cred(int sig, struct siginfoinfo, struct pidpid,
			 const struct credcred, u32 secid)
{
	int ret = -EINVAL;
	struct task_structp;
	unsigned long flags;

	if (!valid_signal(sig))
		return ret;

	rcu_read_lock();
	p = pid_task(pid, PIDTYPE_PID);
	if (!p) {
		ret = -ESRCH;
		goto out_unlock;
	}
	if (si_fromuser(info) && !kill_as_cred_perm(cred, p)) {
		ret = -EPERM;
		goto out_unlock;
	}
	ret = security_task_kill(p, info, sig, secid);
	if (ret)
		goto out_unlock;

	if (sig) {
		if (lock_task_sighand(p, &flags)) {
			ret = __send_signal(sig, info, p, 1, 0);
			unlock_task_sighand(p, &flags);
		} else
			ret = -ESRCH;
	}
out_unlock:
	rcu_read_unlock();
	return ret;
}
EXPORT_SYMBOL_GPL(kill_pid_info_as_cred);

*/
 kill_something_info() interprets pid in interesting ways just like kill(2).

 POSIX specifies that kill(-1,sig) is unspecified, but what we have
 is probably wrong.  Should make it like BSD or SYSV.
 /*

static int kill_something_info(int sig, struct siginfoinfo, pid_t pid)
{
	int ret;

	if (pid > 0) {
		rcu_read_lock();
		ret = kill_pid_info(sig, info, find_vpid(pid));
		rcu_read_unlock();
		return ret;
	}

	read_lock(&tasklist_lock);
	if (pid != -1) {
		ret = __kill_pgrp_info(sig, info,
				pid ? find_vpid(-pid) : task_pgrp(current));
	} else {
		int retval = 0, count = 0;
		struct task_struct p;

		for_each_process(p) {
			if (task_pid_vnr(p) > 1 &&
					!same_thread_group(p, current)) {
				int err = group_send_sig_info(sig, info, p);
				++count;
				if (err != -EPERM)
					retval = err;
			}
		}
		ret = count ? retval : -ESRCH;
	}
	read_unlock(&tasklist_lock);

	return ret;
}

*/
 These are for backward compatibility with the rest of the kernel source.
 /*

int send_sig_info(int sig, struct siginfoinfo, struct task_structp)
{
	*/
	 Make sure legacy kernel users don't send in bad values
	 (normal paths check this in check_kill_permission).
	 /*
	if (!valid_signal(sig))
		return -EINVAL;

	return do_send_sig_info(sig, info, p, false);
}

#define __si_special(priv) \
	((priv) ? SEND_SIG_PRIV : SEND_SIG_NOINFO)

int
send_sig(int sig, struct task_structp, int priv)
{
	return send_sig_info(sig, __si_special(priv), p);
}

void
force_sig(int sig, struct task_structp)
{
	force_sig_info(sig, SEND_SIG_PRIV, p);
}

*/
 When things go south during signal handling, we
 will force a SIGSEGV. And if the signal that caused
 the problem was already a SIGSEGV, we'll want to
 make sure we don't even try to deliver the signal..
 /*
int
force_sigsegv(int sig, struct task_structp)
{
	if (sig == SIGSEGV) {
		unsigned long flags;
		spin_lock_irqsave(&p->sighand->siglock, flags);
		p->sighand->action[sig - 1].sa.sa_handler = SIG_DFL;
		spin_unlock_irqrestore(&p->sighand->siglock, flags);
	}
	force_sig(SIGSEGV, p);
	return 0;
}

int kill_pgrp(struct pidpid, int sig, int priv)
{
	int ret;

	read_lock(&tasklist_lock);
	ret = __kill_pgrp_info(sig, __si_special(priv), pid);
	read_unlock(&tasklist_lock);

	return ret;
}
EXPORT_SYMBOL(kill_pgrp);

int kill_pid(struct pidpid, int sig, int priv)
{
	return kill_pid_info(sig, __si_special(priv), pid);
}
EXPORT_SYMBOL(kill_pid);

*/
 These functions support sending signals using preallocated sigqueue
 structures.  This is needed "because realtime applications cannot
 afford to lose notifications of asynchronous events, like timer
 expirations or I/O completions".  In the case of POSIX Timers
 we allocate the sigqueue structure from the timer_create.  If this
 allocation fails we are able to report the failure to the application
 with an EAGAIN error.
 /*
struct sigqueuesigqueue_alloc(void)
{
	struct sigqueueq = __sigqueue_alloc(-1, current, GFP_KERNEL, 0);

	if (q)
		q->flags |= SIGQUEUE_PREALLOC;

	return q;
}

void sigqueue_free(struct sigqueueq)
{
	unsigned long flags;
	spinlock_tlock = &current->sighand->siglock;

	BUG_ON(!(q->flags & SIGQUEUE_PREALLOC));
	*/
	 We must hold ->siglock while testing q->list
	 to serialize with collect_signal() or with
	 __exit_signal()->flush_sigqueue().
	 /*
	spin_lock_irqsave(lock, flags);
	q->flags &= ~SIGQUEUE_PREALLOC;
	*/
	 If it is queued it will be freed when dequeued,
	 like the "regular" sigqueue.
	 /*
	if (!list_empty(&q->list))
		q = NULL;
	spin_unlock_irqrestore(lock, flags);

	if (q)
		__sigqueue_free(q);
}

int send_sigqueue(struct sigqueueq, struct task_structt, int group)
{
	int sig = q->info.si_signo;
	struct sigpendingpending;
	unsigned long flags;
	int ret, result;

	BUG_ON(!(q->flags & SIGQUEUE_PREALLOC));

	ret = -1;
	if (!likely(lock_task_sighand(t, &flags)))
		goto ret;

	ret = 1;/ the signal is ignored /*
	result = TRACE_SIGNAL_IGNORED;
	if (!prepare_signal(sig, t, false))
		goto out;

	ret = 0;
	if (unlikely(!list_empty(&q->list))) {
		*/
		 If an SI_TIMER entry is already queue just increment
		 the overrun count.
		 /*
		BUG_ON(q->info.si_code != SI_TIMER);
		q->info.si_overrun++;
		result = TRACE_SIGNAL_ALREADY_PENDING;
		goto out;
	}
	q->info.si_overrun = 0;

	signalfd_notify(t, sig);
	pending = group ? &t->signal->shared_pending : &t->pending;
	list_add_tail(&q->list, &pending->list);
	sigaddset(&pending->signal, sig);
	complete_signal(sig, t, group);
	result = TRACE_SIGNAL_DELIVERED;
out:
	trace_signal_generate(sig, &q->info, t, group, result);
	unlock_task_sighand(t, &flags);
ret:
	return ret;
}

*/
 Let a parent know about the death of a child.
 For a stopped/continued status change, use do_notify_parent_cldstop instead.

 Returns true if our parent ignored us and so we've switched to
 self-reaping.
 /*
bool do_notify_parent(struct task_structtsk, int sig)
{
	struct siginfo info;
	unsigned long flags;
	struct sighand_structpsig;
	bool autoreap = false;
	cputime_t utime, stime;

	BUG_ON(sig == -1);

 	*/ do_notify_parent_cldstop should have been called instead.  /*
 	BUG_ON(task_is_stopped_or_traced(tsk));

	BUG_ON(!tsk->ptrace &&
	       (tsk->group_leader != tsk || !thread_group_empty(tsk)));

	if (sig != SIGCHLD) {
		*/
		 This is only possible if parent == real_parent.
		 Check if it has changed security domain.
		 /*
		if (tsk->parent_exec_id != tsk->parent->self_exec_id)
			sig = SIGCHLD;
	}

	info.si_signo = sig;
	info.si_errno = 0;
	*/
	 We are under tasklist_lock here so our parent is tied to
	 us and cannot change.
	
	 task_active_pid_ns will always return the same pid namespace
	 until a task passes through release_task.
	
	 write_lock() currently calls preempt_disable() which is the
	 same as rcu_read_lock(), but according to Oleg, this is not
	 correct to rely on this
	 /*
	rcu_read_lock();
	info.si_pid = task_pid_nr_ns(tsk, task_active_pid_ns(tsk->parent));
	info.si_uid = from_kuid_munged(task_cred_xxx(tsk->parent, user_ns),
				       task_uid(tsk));
	rcu_read_unlock();

	task_cputime(tsk, &utime, &stime);
	info.si_utime = cputime_to_clock_t(utime + tsk->signal->utime);
	info.si_stime = cputime_to_clock_t(stime + tsk->signal->stime);

	info.si_status = tsk->exit_code & 0x7f;
	if (tsk->exit_code & 0x80)
		info.si_code = CLD_DUMPED;
	else if (tsk->exit_code & 0x7f)
		info.si_code = CLD_KILLED;
	else {
		info.si_code = CLD_EXITED;
		info.si_status = tsk->exit_code >> 8;
	}

	psig = tsk->parent->sighand;
	spin_lock_irqsave(&psig->siglock, flags);
	if (!tsk->ptrace && sig == SIGCHLD &&
	    (psig->action[SIGCHLD-1].sa.sa_handler == SIG_IGN ||
	     (psig->action[SIGCHLD-1].sa.sa_flags & SA_NOCLDWAIT))) {
		*/
		 We are exiting and our parent doesn't care.  POSIX.1
		 defines special semantics for setting SIGCHLD to SIG_IGN
		 or setting the SA_NOCLDWAIT flag: we should be reaped
		 automatically and not left for our parent's wait4 call.
		 Rather than having the parent do it as a magic kind of
		 signal handler, we just set this to tell do_exit that we
		 can be cleaned up without becoming a zombie.  Note that
		 we still call __wake_up_parent in this case, because a
		 blocked sys_wait4 might now return -ECHILD.
		
		 Whether we send SIGCHLD or not for SA_NOCLDWAIT
		 is implementation-defined: we do (if you don't want
		 it, just use SIG_IGN instead).
		 /*
		autoreap = true;
		if (psig->action[SIGCHLD-1].sa.sa_handler == SIG_IGN)
			sig = 0;
	}
	if (valid_signal(sig) && sig)
		__group_send_sig_info(sig, &info, tsk->parent);
	__wake_up_parent(tsk, tsk->parent);
	spin_unlock_irqrestore(&psig->siglock, flags);

	return autoreap;
}

*/
 do_notify_parent_cldstop - notify parent of stopped/continued state change
 @tsk: task reporting the state change
 @for_ptracer: the notification is for ptracer
 @why: CLD_{CONTINUED|STOPPED|TRAPPED} to report

 Notify @tsk's parent that the stopped/continued state has changed.  If
 @for_ptracer is %false, @tsk's group leader notifies to its real parent.
 If %true, @tsk reports to @tsk->parent which should be the ptracer.

 CONTEXT:
 Must be called with tasklist_lock at least read locked.
 /*
static void do_notify_parent_cldstop(struct task_structtsk,
				     bool for_ptracer, int why)
{
	struct siginfo info;
	unsigned long flags;
	struct task_structparent;
	struct sighand_structsighand;
	cputime_t utime, stime;

	if (for_ptracer) {
		parent = tsk->parent;
	} else {
		tsk = tsk->group_leader;
		parent = tsk->real_parent;
	}

	info.si_signo = SIGCHLD;
	info.si_errno = 0;
	*/
	 see comment in do_notify_parent() about the following 4 lines
	 /*
	rcu_read_lock();
	info.si_pid = task_pid_nr_ns(tsk, task_active_pid_ns(parent));
	info.si_uid = from_kuid_munged(task_cred_xxx(parent, user_ns), task_uid(tsk));
	rcu_read_unlock();

	task_cputime(tsk, &utime, &stime);
	info.si_utime = cputime_to_clock_t(utime);
	info.si_stime = cputime_to_clock_t(stime);

 	info.si_code = why;
 	switch (why) {
 	case CLD_CONTINUED:
 		info.si_status = SIGCONT;
 		break;
 	case CLD_STOPPED:
 		info.si_status = tsk->signal->group_exit_code & 0x7f;
 		break;
 	case CLD_TRAPPED:
 		info.si_status = tsk->exit_code & 0x7f;
 		break;
 	default:
 		BUG();
 	}

	sighand = parent->sighand;
	spin_lock_irqsave(&sighand->siglock, flags);
	if (sighand->action[SIGCHLD-1].sa.sa_handler != SIG_IGN &&
	    !(sighand->action[SIGCHLD-1].sa.sa_flags & SA_NOCLDSTOP))
		__group_send_sig_info(SIGCHLD, &info, parent);
	*/
	 Even if SIGCHLD is not generated, we must wake up wait4 calls.
	 /*
	__wake_up_parent(tsk, parent);
	spin_unlock_irqrestore(&sighand->siglock, flags);
}

static inline int may_ptrace_stop(void)
{
	if (!likely(current->ptrace))
		return 0;
	*/
	 Are we in the middle of do_coredump?
	 If so and our tracer is also part of the coredump stopping
	 is a deadlock situation, and pointless because our tracer
	 is dead so don't allow us to stop.
	 If SIGKILL was already sent before the caller unlocked
	 ->siglock we must see ->core_state != NULL. Otherwise it
	 is safe to enter schedule().
	
	 This is almost outdated, a task with the pending SIGKILL can't
	 block in TASK_TRACED. But PTRACE_EVENT_EXIT can be reported
	 after SIGKILL was already dequeued.
	 /*
	if (unlikely(current->mm->core_state) &&
	    unlikely(current->mm == current->parent->mm))
		return 0;

	return 1;
}

*/
 Return non-zero if there is a SIGKILL that should be waking us up.
 Called with the siglock held.
 /*
static int sigkill_pending(struct task_structtsk)
{
	return	sigismember(&tsk->pending.signal, SIGKILL) ||
		sigismember(&tsk->signal->shared_pending.signal, SIGKILL);
}

*/
 This must be called with current->sighand->siglock held.

 This should be the path for all ptrace stops.
 We always set current->last_siginfo while stopped here.
 That makes it a way to test a stopped process for
 being ptrace-stopped vs being job-control-stopped.

 If we actually decide not to stop at all because the tracer
 is gone, we keep current->exit_code unless clear_code.
 /*
static void ptrace_stop(int exit_code, int why, int clear_code, siginfo_tinfo)
	__releases(&current->sighand->siglock)
	__acquires(&current->sighand->siglock)
{
	bool gstop_done = false;

	if (arch_ptrace_stop_needed(exit_code, info)) {
		*/
		 The arch code has something special to do before a
		 ptrace stop.  This is allowed to block, e.g. for faults
		 on user stack pages.  We can't keep the siglock while
		 calling arch_ptrace_stop, so we must release it now.
		 To preserve proper semantics, we must do this before
		 any signal bookkeeping like checking group_stop_count.
		 Meanwhile, a SIGKILL could come in before we retake the
		 siglock.  That must prevent us from sleeping in TASK_TRACED.
		 So after regaining the lock, we must check for SIGKILL.
		 /*
		spin_unlock_irq(&current->sighand->siglock);
		arch_ptrace_stop(exit_code, info);
		spin_lock_irq(&current->sighand->siglock);
		if (sigkill_pending(current))
			return;
	}

	*/
	 We're committing to trapping.  TRACED should be visible before
	 TRAPPING is cleared; otherwise, the tracer might fail do_wait().
	 Also, transition to TRACED and updates to ->jobctl should be
	 atomic with respect to siglock and should be done after the arch
	 hook as siglock is released and regrabbed across it.
	 /*
	set_current_state(TASK_TRACED);

	current->last_siginfo = info;
	current->exit_code = exit_code;

	*/
	 If @why is CLD_STOPPED, we're trapping to participate in a group
	 stop.  Do the bookkeeping.  Note that if SIGCONT was delievered
	 across siglock relocks since INTERRUPT was scheduled, PENDING
	 could be clear now.  We act as if SIGCONT is received after
	 TASK_TRACED is entered - ignore it.
	 /*
	if (why == CLD_STOPPED && (current->jobctl & JOBCTL_STOP_PENDING))
		gstop_done = task_participate_group_stop(current);

	*/ any trap clears pending STOP trap, STOP trap clears NOTIFY /*
	task_clear_jobctl_pending(current, JOBCTL_TRAP_STOP);
	if (info && info->si_code >> 8 == PTRACE_EVENT_STOP)
		task_clear_jobctl_pending(current, JOBCTL_TRAP_NOTIFY);

	*/ entering a trap, clear TRAPPING /*
	task_clear_jobctl_trapping(current);

	spin_unlock_irq(&current->sighand->siglock);
	read_lock(&tasklist_lock);
	if (may_ptrace_stop()) {
		*/
		 Notify parents of the stop.
		
		 While ptraced, there are two parents - the ptracer and
		 the real_parent of the group_leader.  The ptracer should
		 know about every stop while the real parent is only
		 interested in the completion of group stop.  The states
		 for the two don't interact with each other.  Notify
		 separately unless they're gonna be duplicates.
		 /*
		do_notify_parent_cldstop(current, true, why);
		if (gstop_done && ptrace_reparented(current))
			do_notify_parent_cldstop(current, false, why);

		*/
		 Don't want to allow preemption here, because
		 sys_ptrace() needs this task to be inactive.
		
		 XXX: implement read_unlock_no_resched().
		 /*
		preempt_disable();
		read_unlock(&tasklist_lock);
		preempt_enable_no_resched();
		freezable_schedule();
	} else {
		*/
		 By the time we got the lock, our tracer went away.
		 Don't drop the lock yet, another tracer may come.
		
		 If @gstop_done, the ptracer went away between group stop
		 completion and here.  During detach, it would have set
		 JOBCTL_STOP_PENDING on us and we'll re-enter
		 TASK_STOPPED in do_signal_stop() on return, so notifying
		 the real parent of the group stop completion is enough.
		 /*
		if (gstop_done)
			do_notify_parent_cldstop(current, false, why);

		*/ tasklist protects us from ptrace_freeze_traced() /*
		__set_current_state(TASK_RUNNING);
		if (clear_code)
			current->exit_code = 0;
		read_unlock(&tasklist_lock);
	}

	*/
	 We are back.  Now reacquire the siglock before touching
	 last_siginfo, so that we are sure to have synchronized with
	 any signal-sending on another CPU that wants to examine it.
	 /*
	spin_lock_irq(&current->sighand->siglock);
	current->last_siginfo = NULL;

	*/ LISTENING can be set only during STOP traps, clear it /*
	current->jobctl &= ~JOBCTL_LISTENING;

	*/
	 Queued signals ignored us while we were stopped for tracing.
	 So check for any that we should take before resuming user mode.
	 This sets TIF_SIGPENDING, but never clears it.
	 /*
	recalc_sigpending_tsk(current);
}

static void ptrace_do_notify(int signr, int exit_code, int why)
{
	siginfo_t info;

	memset(&info, 0, sizeof info);
	info.si_signo = signr;
	info.si_code = exit_code;
	info.si_pid = task_pid_vnr(current);
	info.si_uid = from_kuid_munged(current_user_ns(), current_uid());

	*/ Let the debugger run.  /*
	ptrace_stop(exit_code, why, 1, &info);
}

void ptrace_notify(int exit_code)
{
	BUG_ON((exit_code & (0x7f | ~0xffff)) != SIGTRAP);
	if (unlikely(current->task_works))
		task_work_run();

	spin_lock_irq(&current->sighand->siglock);
	ptrace_do_notify(SIGTRAP, exit_code, CLD_TRAPPED);
	spin_unlock_irq(&current->sighand->siglock);
}

*/
 do_signal_stop - handle group stop for SIGSTOP and other stop signals
 @signr: signr causing group stop if initiating

 If %JOBCTL_STOP_PENDING is not set yet, initiate group stop with @signr
 and participate in it.  If already set, participate in the existing
 group stop.  If participated in a group stop (and thus slept), %true is
 returned with siglock released.

 If ptraced, this function doesn't handle stop itself.  Instead,
 %JOBCTL_TRAP_STOP is scheduled and %false is returned with siglock
 untouched.  The caller must ensure that INTERRUPT trap handling takes
 places afterwards.

 CONTEXT:
 Must be called with @current->sighand->siglock held, which is released
 on %true return.

 RETURNS:
 %false if group stop is already cancelled or ptrace trap is scheduled.
 %true if participated in group stop.
 /*
static bool do_signal_stop(int signr)
	__releases(&current->sighand->siglock)
{
	struct signal_structsig = current->signal;

	if (!(current->jobctl & JOBCTL_STOP_PENDING)) {
		unsigned long gstop = JOBCTL_STOP_PENDING | JOBCTL_STOP_CONSUME;
		struct task_structt;

		*/ signr will be recorded in task->jobctl for retries /*
		WARN_ON_ONCE(signr & ~JOBCTL_STOP_SIGMASK);

		if (!likely(current->jobctl & JOBCTL_STOP_DEQUEUED) ||
		    unlikely(signal_group_exit(sig)))
			return false;
		*/
		 There is no group stop already in progress.  We must
		 initiate one now.
		
		 While ptraced, a task may be resumed while group stop is
		 still in effect and then receive a stop signal and
		 initiate another group stop.  This deviates from the
		 usual behavior as two consecutive stop signals can't
		 cause two group stops when !ptraced.  That is why we
		 also check !task_is_stopped(t) below.
		
		 The condition can be distinguished by testing whether
		 SIGNAL_STOP_STOPPED is already set.  Don't generate
		 group_exit_code in such case.
		
		 This is not necessary for SIGNAL_STOP_CONTINUED because
		 an intervening stop signal is required to cause two
		 continued events regardless of ptrace.
		 /*
		if (!(sig->flags & SIGNAL_STOP_STOPPED))
			sig->group_exit_code = signr;

		sig->group_stop_count = 0;

		if (task_set_jobctl_pending(current, signr | gstop))
			sig->group_stop_count++;

		t = current;
		while_each_thread(current, t) {
			*/
			 Setting state to TASK_STOPPED for a group
			 stop is always done with the siglock held,
			 so this check has no races.
			 /*
			if (!task_is_stopped(t) &&
			    task_set_jobctl_pending(t, signr | gstop)) {
				sig->group_stop_count++;
				if (likely(!(t->ptrace & PT_SEIZED)))
					signal_wake_up(t, 0);
				else
					ptrace_trap_notify(t);
			}
		}
	}

	if (likely(!current->ptrace)) {
		int notify = 0;

		*/
		 If there are no other threads in the group, or if there
		 is a group stop in progress and we are the last to stop,
		 report to the parent.
		 /*
		if (task_participate_group_stop(current))
			notify = CLD_STOPPED;

		__set_current_state(TASK_STOPPED);
		spin_unlock_irq(&current->sighand->siglock);

		*/
		 Notify the parent of the group stop completion.  Because
		 we're not holding either the siglock or tasklist_lock
		 here, ptracer may attach inbetween; however, this is for
		 group stop and should always be delivered to the real
		 parent of the group leader.  The new ptracer will get
		 its notification when this task transitions into
		 TASK_TRACED.
		 /*
		if (notify) {
			read_lock(&tasklist_lock);
			do_notify_parent_cldstop(current, false, notify);
			read_unlock(&tasklist_lock);
		}

		*/ Now we don't run again until woken by SIGCONT or SIGKILL /*
		freezable_schedule();
		return true;
	} else {
		*/
		 While ptraced, group stop is handled by STOP trap.
		 Schedule it and let the caller deal with it.
		 /*
		task_set_jobctl_pending(current, JOBCTL_TRAP_STOP);
		return false;
	}
}

*/
 do_jobctl_trap - take care of ptrace jobctl traps

 When PT_SEIZED, it's used for both group stop and explicit
 SEIZE/INTERRUPT traps.  Both generate PTRACE_EVENT_STOP trap with
 accompanying siginfo.  If stopped, lower eight bits of exit_code contain
 the stop signal; otherwise, %SIGTRAP.

 When !PT_SEIZED, it's used only for group stop trap with stop signal
 number as exit_code and no siginfo.

 CONTEXT:
 Must be called with @current->sighand->siglock held, which may be
 released and re-acquired before returning with intervening sleep.
 /*
static void do_jobctl_trap(void)
{
	struct signal_structsignal = current->signal;
	int signr = current->jobctl & JOBCTL_STOP_SIGMASK;

	if (current->ptrace & PT_SEIZED) {
		if (!signal->group_stop_count &&
		    !(signal->flags & SIGNAL_STOP_STOPPED))
			signr = SIGTRAP;
		WARN_ON_ONCE(!signr);
		ptrace_do_notify(signr, signr | (PTRACE_EVENT_STOP << 8),
				 CLD_STOPPED);
	} else {
		WARN_ON_ONCE(!signr);
		ptrace_stop(signr, CLD_STOPPED, 0, NULL);
		current->exit_code = 0;
	}
}

static int ptrace_signal(int signr, siginfo_tinfo)
{
	ptrace_signal_deliver();
	*/
	 We do not check sig_kernel_stop(signr) but set this marker
	 unconditionally because we do not know whether debugger will
	 change signr. This flag has no meaning unless we are going
	 to stop after return from ptrace_stop(). In this case it will
	 be checked in do_signal_stop(), we should only stop if it was
	 not cleared by SIGCONT while we were sleeping. See also the
	 comment in dequeue_signal().
	 /*
	current->jobctl |= JOBCTL_STOP_DEQUEUED;
	ptrace_stop(signr, CLD_TRAPPED, 0, info);

	*/ We're back.  Did the debugger cancel the sig?  /*
	signr = current->exit_code;
	if (signr == 0)
		return signr;

	current->exit_code = 0;

	*/
	 Update the siginfo structure if the signal has
	 changed.  If the debugger wanted something
	 specific in the siginfo structure then it should
	 have updatedinfo via PTRACE_SETSIGINFO.
	 /*
	if (signr != info->si_signo) {
		info->si_signo = signr;
		info->si_errno = 0;
		info->si_code = SI_USER;
		rcu_read_lock();
		info->si_pid = task_pid_vnr(current->parent);
		info->si_uid = from_kuid_munged(current_user_ns(),
						task_uid(current->parent));
		rcu_read_unlock();
	}

	*/ If the (new) signal is now blocked, requeue it.  /*
	if (sigismember(&current->blocked, signr)) {
		specific_send_sig_info(signr, info, current);
		signr = 0;
	}

	return signr;
}

int get_signal(struct ksignalksig)
{
	struct sighand_structsighand = current->sighand;
	struct signal_structsignal = current->signal;
	int signr;

	if (unlikely(current->task_works))
		task_work_run();

	if (unlikely(uprobe_deny_signal()))
		return 0;

	*/
	 Do this once, we can't return to user-mode if freezing() == T.
	 do_signal_stop() and ptrace_stop() do freezable_schedule() and
	 thus do not need another check after return.
	 /*
	try_to_freeze();

relock:
	spin_lock_irq(&sighand->siglock);
	*/
	 Every stopped thread goes here after wakeup. Check to see if
	 we should notify the parent, prepare_signal(SIGCONT) encodes
	 the CLD_ si_code into SIGNAL_CLD_MASK bits.
	 /*
	if (unlikely(signal->flags & SIGNAL_CLD_MASK)) {
		int why;

		if (signal->flags & SIGNAL_CLD_CONTINUED)
			why = CLD_CONTINUED;
		else
			why = CLD_STOPPED;

		signal->flags &= ~SIGNAL_CLD_MASK;

		spin_unlock_irq(&sighand->siglock);

		*/
		 Notify the parent that we're continuing.  This event is
		 always per-process and doesn't make whole lot of sense
		 for ptracers, who shouldn't consume the state via
		 wait(2) either, but, for backward compatibility, notify
		 the ptracer of the group leader too unless it's gonna be
		 a duplicate.
		 /*
		read_lock(&tasklist_lock);
		do_notify_parent_cldstop(current, false, why);

		if (ptrace_reparented(current->group_leader))
			do_notify_parent_cldstop(current->group_leader,
						true, why);
		read_unlock(&tasklist_lock);

		goto relock;
	}

	for (;;) {
		struct k_sigactionka;

		if (unlikely(current->jobctl & JOBCTL_STOP_PENDING) &&
		    do_signal_stop(0))
			goto relock;

		if (unlikely(current->jobctl & JOBCTL_TRAP_MASK)) {
			do_jobctl_trap();
			spin_unlock_irq(&sighand->siglock);
			goto relock;
		}

		signr = dequeue_signal(current, &current->blocked, &ksig->info);

		if (!signr)
			break;/ will return 0 /*

		if (unlikely(current->ptrace) && signr != SIGKILL) {
			signr = ptrace_signal(signr, &ksig->info);
			if (!signr)
				continue;
		}

		ka = &sighand->action[signr-1];

		*/ Trace actually delivered signals. /*
		trace_signal_deliver(signr, &ksig->info, ka);

		if (ka->sa.sa_handler == SIG_IGN)/ Do nothing.  /*
			continue;
		if (ka->sa.sa_handler != SIG_DFL) {
			*/ Run the handler.  /*
			ksig->ka =ka;

			if (ka->sa.sa_flags & SA_ONESHOT)
				ka->sa.sa_handler = SIG_DFL;

			break;/ will return non-zero "signr" value /*
		}

		*/
		 Now we are doing the default action for this signal.
		 /*
		if (sig_kernel_ignore(signr))/ Default is nothing. /*
			continue;

		*/
		 Global init gets no signals it doesn't want.
		 Container-init gets no signals it doesn't want from same
		 container.
		
		 Note that if global/container-init sees a sig_kernel_only()
		 signal here, the signal must have been generated internally
		 or must have come from an ancestor namespace. In either
		 case, the signal cannot be dropped.
		 /*
		if (unlikely(signal->flags & SIGNAL_UNKILLABLE) &&
				!sig_kernel_only(signr))
			continue;

		if (sig_kernel_stop(signr)) {
			*/
			 The default action is to stop all threads in
			 the thread group.  The job control signals
			 do nothing in an orphaned pgrp, but SIGSTOP
			 always works.  Note that siglock needs to be
			 dropped during the call to is_orphaned_pgrp()
			 because of lock ordering with tasklist_lock.
			 This allows an intervening SIGCONT to be posted.
			 We need to check for that and bail out if necessary.
			 /*
			if (signr != SIGSTOP) {
				spin_unlock_irq(&sighand->siglock);

				*/ signals can be posted during this window /*

				if (is_current_pgrp_orphaned())
					goto relock;

				spin_lock_irq(&sighand->siglock);
			}

			if (likely(do_signal_stop(ksig->info.si_signo))) {
				*/ It released the siglock.  /*
				goto relock;
			}

			*/
			 We didn't actually stop, due to a race
			 with SIGCONT or something like that.
			 /*
			continue;
		}

		spin_unlock_irq(&sighand->siglock);

		*/
		 Anything else is fatal, maybe with a core dump.
		 /*
		current->flags |= PF_SIGNALED;

		if (sig_kernel_coredump(signr)) {
			if (print_fatal_signals)
				print_fatal_signal(ksig->info.si_signo);
			proc_coredump_connector(current);
			*/
			 If it was able to dump core, this kills all
			 other threads in the group and synchronizes with
			 their demise.  If we lost the race with another
			 thread getting here, it set group_exit_code
			 first and our do_group_exit call below will use
			 that value and ignore the one we pass it.
			 /*
			do_coredump(&ksig->info);
		}

		*/
		 Death signals, no core dump.
		 /*
		do_group_exit(ksig->info.si_signo);
		*/ NOTREACHED /*
	}
	spin_unlock_irq(&sighand->siglock);

	ksig->sig = signr;
	return ksig->sig > 0;
}

*/
 signal_delivered - 
 @ksig:		kernel signal struct
 @stepping:		nonzero if debugger single-step or block-step in use

 This function should be called when a signal has successfully been
 delivered. It updates the blocked signals accordingly (@ksig->ka.sa.sa_mask
 is always blocked, and the signal itself is blocked unless %SA_NODEFER
 is set in @ksig->ka.sa.sa_flags.  Tracing is notified.
 /*
static void signal_delivered(struct ksignalksig, int stepping)
{
	sigset_t blocked;

	*/ A signal was successfully delivered, and the
	   saved sigmask was stored on the signal frame,
	   and will be restored by sigreturn.  So we can
	   simply clear the restore sigmask flag.  /*
	clear_restore_sigmask();

	sigorsets(&blocked, &current->blocked, &ksig->ka.sa.sa_mask);
	if (!(ksig->ka.sa.sa_flags & SA_NODEFER))
		sigaddset(&blocked, ksig->sig);
	set_current_blocked(&blocked);
	tracehook_signal_handler(stepping);
}

void signal_setup_done(int failed, struct ksignalksig, int stepping)
{
	if (failed)
		force_sigsegv(ksig->sig, current);
	else
		signal_delivered(ksig, stepping);
}

*/
 It could be that complete_signal() picked us to notify about the
 group-wide signal. Other threads should be notified now to take
 the shared signals in @which since we will not.
 /*
static void retarget_shared_pending(struct task_structtsk, sigset_twhich)
{
	sigset_t retarget;
	struct task_structt;

	sigandsets(&retarget, &tsk->signal->shared_pending.signal, which);
	if (sigisemptyset(&retarget))
		return;

	t = tsk;
	while_each_thread(tsk, t) {
		if (t->flags & PF_EXITING)
			continue;

		if (!has_pending_signals(&retarget, &t->blocked))
			continue;
		*/ Remove the signals this thread can handle. /*
		sigandsets(&retarget, &retarget, &t->blocked);

		if (!signal_pending(t))
			signal_wake_up(t, 0);

		if (sigisemptyset(&retarget))
			break;
	}
}

void exit_signals(struct task_structtsk)
{
	int group_stop = 0;
	sigset_t unblocked;

	*/
	 @tsk is about to have PF_EXITING set - lock out users which
	 expect stable threadgroup.
	 /*
	threadgroup_change_begin(tsk);

	if (thread_group_empty(tsk) || signal_group_exit(tsk->signal)) {
		tsk->flags |= PF_EXITING;
		threadgroup_change_end(tsk);
		return;
	}

	spin_lock_irq(&tsk->sighand->siglock);
	*/
	 From now this task is not visible for group-wide signals,
	 see wants_signal(), do_signal_stop().
	 /*
	tsk->flags |= PF_EXITING;

	threadgroup_change_end(tsk);

	if (!signal_pending(tsk))
		goto out;

	unblocked = tsk->blocked;
	signotset(&unblocked);
	retarget_shared_pending(tsk, &unblocked);

	if (unlikely(tsk->jobctl & JOBCTL_STOP_PENDING) &&
	    task_participate_group_stop(tsk))
		group_stop = CLD_STOPPED;
out:
	spin_unlock_irq(&tsk->sighand->siglock);

	*/
	 If group stop has completed, deliver the notification.  This
	 should always go to the real parent of the group leader.
	 /*
	if (unlikely(group_stop)) {
		read_lock(&tasklist_lock);
		do_notify_parent_cldstop(tsk, false, group_stop);
		read_unlock(&tasklist_lock);
	}
}

EXPORT_SYMBOL(recalc_sigpending);
EXPORT_SYMBOL_GPL(dequeue_signal);
EXPORT_SYMBOL(flush_signals);
EXPORT_SYMBOL(force_sig);
EXPORT_SYMBOL(send_sig);
EXPORT_SYMBOL(send_sig_info);
EXPORT_SYMBOL(sigprocmask);

*/
 System call entry points.
 /*

*/
  sys_restart_syscall - restart a system call
 /*
SYSCALL_DEFINE0(restart_syscall)
{
	struct restart_blockrestart = &current->restart_block;
	return restart->fn(restart);
}

long do_no_restart_syscall(struct restart_blockparam)
{
	return -EINTR;
}

static void __set_task_blocked(struct task_structtsk, const sigset_tnewset)
{
	if (signal_pending(tsk) && !thread_group_empty(tsk)) {
		sigset_t newblocked;
		*/ A set of now blocked but previously unblocked signals. /*
		sigandnsets(&newblocked, newset, &current->blocked);
		retarget_shared_pending(tsk, &newblocked);
	}
	tsk->blocked =newset;
	recalc_sigpending();
}

*/
 set_current_blocked - change current->blocked mask
 @newset: new mask

 It is wrong to change ->blocked directly, this helper should be used
 to ensure the process can't miss a shared signal we are going to block.
 /*
void set_current_blocked(sigset_tnewset)
{
	sigdelsetmask(newset, sigmask(SIGKILL) | sigmask(SIGSTOP));
	__set_current_blocked(newset);
}

void __set_current_blocked(const sigset_tnewset)
{
	struct task_structtsk = current;

	spin_lock_irq(&tsk->sighand->siglock);
	__set_task_blocked(tsk, newset);
	spin_unlock_irq(&tsk->sighand->siglock);
}

*/
 This is also useful for kernel threads that want to temporarily
 (or permanently) block certain signals.

 NOTE! Unlike the user-mode sys_sigprocmask(), the kernel
 interface happily blocks "unblockable" signals like SIGKILL
 and friends.
 /*
int sigprocmask(int how, sigset_tset, sigset_toldset)
{
	struct task_structtsk = current;
	sigset_t newset;

	*/ Lockless, only current can change ->blocked, never from irq /*
	if (oldset)
		*oldset = tsk->blocked;

	switch (how) {
	case SIG_BLOCK:
		sigorsets(&newset, &tsk->blocked, set);
		break;
	case SIG_UNBLOCK:
		sigandnsets(&newset, &tsk->blocked, set);
		break;
	case SIG_SETMASK:
		newset =set;
		break;
	default:
		return -EINVAL;
	}

	__set_current_blocked(&newset);
	return 0;
}

*/
  sys_rt_sigprocmask - change the list of currently blocked signals
  @how: whether to add, remove, or set signals
  @nset: stores pending signals
  @oset: previous value of signal mask if non-null
  @sigsetsize: size of sigset_t type
 /*
SYSCALL_DEFINE4(rt_sigprocmask, int, how, sigset_t __user, nset,
		sigset_t __user, oset, size_t, sigsetsize)
{
	sigset_t old_set, new_set;
	int error;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	old_set = current->blocked;

	if (nset) {
		if (copy_from_user(&new_set, nset, sizeof(sigset_t)))
			return -EFAULT;
		sigdelsetmask(&new_set, sigmask(SIGKILL)|sigmask(SIGSTOP));

		error = sigprocmask(how, &new_set, NULL);
		if (error)
			return error;
	}

	if (oset) {
		if (copy_to_user(oset, &old_set, sizeof(sigset_t)))
			return -EFAULT;
	}

	return 0;
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE4(rt_sigprocmask, int, how, compat_sigset_t __user, nset,
		compat_sigset_t __user, oset, compat_size_t, sigsetsize)
{
#ifdef __BIG_ENDIAN
	sigset_t old_set = current->blocked;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (nset) {
		compat_sigset_t new32;
		sigset_t new_set;
		int error;
		if (copy_from_user(&new32, nset, sizeof(compat_sigset_t)))
			return -EFAULT;

		sigset_from_compat(&new_set, &new32);
		sigdelsetmask(&new_set, sigmask(SIGKILL)|sigmask(SIGSTOP));

		error = sigprocmask(how, &new_set, NULL);
		if (error)
			return error;
	}
	if (oset) {
		compat_sigset_t old32;
		sigset_to_compat(&old32, &old_set);
		if (copy_to_user(oset, &old32, sizeof(compat_sigset_t)))
			return -EFAULT;
	}
	return 0;
#else
	return sys_rt_sigprocmask(how, (sigset_t __user)nset,
				  (sigset_t __user)oset, sigsetsize);
#endif
}
#endif

static int do_sigpending(voidset, unsigned long sigsetsize)
{
	if (sigsetsize > sizeof(sigset_t))
		return -EINVAL;

	spin_lock_irq(&current->sighand->siglock);
	sigorsets(set, &current->pending.signal,
		  &current->signal->shared_pending.signal);
	spin_unlock_irq(&current->sighand->siglock);

	*/ Outside the lock because only this thread touches it.  /*
	sigandsets(set, &current->blocked, set);
	return 0;
}

*/
  sys_rt_sigpending - examine a pending signal that has been raised
			while blocked
  @uset: stores pending signals
  @sigsetsize: size of sigset_t type or larger
 /*
SYSCALL_DEFINE2(rt_sigpending, sigset_t __user, uset, size_t, sigsetsize)
{
	sigset_t set;
	int err = do_sigpending(&set, sigsetsize);
	if (!err && copy_to_user(uset, &set, sigsetsize))
		err = -EFAULT;
	return err;
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE2(rt_sigpending, compat_sigset_t __user, uset,
		compat_size_t, sigsetsize)
{
#ifdef __BIG_ENDIAN
	sigset_t set;
	int err = do_sigpending(&set, sigsetsize);
	if (!err) {
		compat_sigset_t set32;
		sigset_to_compat(&set32, &set);
		*/ we can get here only if sigsetsize <= sizeof(set) /*
		if (copy_to_user(uset, &set32, sigsetsize))
			err = -EFAULT;
	}
	return err;
#else
	return sys_rt_sigpending((sigset_t __user)uset, sigsetsize);
#endif
}
#endif

#ifndef HAVE_ARCH_COPY_SIGINFO_TO_USER

int copy_siginfo_to_user(siginfo_t __userto, const siginfo_tfrom)
{
	int err;

	if (!access_ok (VERIFY_WRITE, to, sizeof(siginfo_t)))
		return -EFAULT;
	if (from->si_code < 0)
		return __copy_to_user(to, from, sizeof(siginfo_t))
			? -EFAULT : 0;
	*/
	 If you change siginfo_t structure, please be sure
	 this code is fixed accordingly.
	 Please remember to update the signalfd_copyinfo() function
	 inside fs/signalfd.c too, in case siginfo_t changes.
	 It should never copy any pad contained in the structure
	 to avoid security leaks, but must copy the generic
	 3 ints plus the relevant union member.
	 /*
	err = __put_user(from->si_signo, &to->si_signo);
	err |= __put_user(from->si_errno, &to->si_errno);
	err |= __put_user((short)from->si_code, &to->si_code);
	switch (from->si_code & __SI_MASK) {
	case __SI_KILL:
		err |= __put_user(from->si_pid, &to->si_pid);
		err |= __put_user(from->si_uid, &to->si_uid);
		break;
	case __SI_TIMER:
		 err |= __put_user(from->si_tid, &to->si_tid);
		 err |= __put_user(from->si_overrun, &to->si_overrun);
		 err |= __put_user(from->si_ptr, &to->si_ptr);
		break;
	case __SI_POLL:
		err |= __put_user(from->si_band, &to->si_band);
		err |= __put_user(from->si_fd, &to->si_fd);
		break;
	case __SI_FAULT:
		err |= __put_user(from->si_addr, &to->si_addr);
#ifdef __ARCH_SI_TRAPNO
		err |= __put_user(from->si_trapno, &to->si_trapno);
#endif
#ifdef BUS_MCEERR_AO
		*/
		 Other callers might not initialize the si_lsb field,
		 so check explicitly for the right codes here.
		 /*
		if (from->si_signo == SIGBUS &&
		    (from->si_code == BUS_MCEERR_AR || from->si_code == BUS_MCEERR_AO))
			err |= __put_user(from->si_addr_lsb, &to->si_addr_lsb);
#endif
#ifdef SEGV_BNDERR
		if (from->si_signo == SIGSEGV && from->si_code == SEGV_BNDERR) {
			err |= __put_user(from->si_lower, &to->si_lower);
			err |= __put_user(from->si_upper, &to->si_upper);
		}
#endif
#ifdef SEGV_PKUERR
		if (from->si_signo == SIGSEGV && from->si_code == SEGV_PKUERR)
			err |= __put_user(from->si_pkey, &to->si_pkey);
#endif
		break;
	case __SI_CHLD:
		err |= __put_user(from->si_pid, &to->si_pid);
		err |= __put_user(from->si_uid, &to->si_uid);
		err |= __put_user(from->si_status, &to->si_status);
		err |= __put_user(from->si_utime, &to->si_utime);
		err |= __put_user(from->si_stime, &to->si_stime);
		break;
	case __SI_RT:/ This is not generated by the kernel as of now. /*
	case __SI_MESGQ:/ But this is /*
		err |= __put_user(from->si_pid, &to->si_pid);
		err |= __put_user(from->si_uid, &to->si_uid);
		err |= __put_user(from->si_ptr, &to->si_ptr);
		break;
#ifdef __ARCH_SIGSYS
	case __SI_SYS:
		err |= __put_user(from->si_call_addr, &to->si_call_addr);
		err |= __put_user(from->si_syscall, &to->si_syscall);
		err |= __put_user(from->si_arch, &to->si_arch);
		break;
#endif
	default:/ this is just in case for now ... /*
		err |= __put_user(from->si_pid, &to->si_pid);
		err |= __put_user(from->si_uid, &to->si_uid);
		break;
	}
	return err;
}

#endif

*/
  do_sigtimedwait - wait for queued signals specified in @which
  @which: queued signals to wait for
  @info: if non-null, the signal's siginfo is returned here
  @ts: upper bound on process time suspension
 /*
int do_sigtimedwait(const sigset_twhich, siginfo_tinfo,
			const struct timespects)
{
	struct task_structtsk = current;
	long timeout = MAX_SCHEDULE_TIMEOUT;
	sigset_t mask =which;
	int sig;

	if (ts) {
		if (!timespec_valid(ts))
			return -EINVAL;
		timeout = timespec_to_jiffies(ts);
		*/
		 We can be close to the next tick, add another one
		 to ensure we will wait at least the time asked for.
		 /*
		if (ts->tv_sec || ts->tv_nsec)
			timeout++;
	}

	*/
	 Invert the set of allowed signals to get those we want to block.
	 /*
	sigdelsetmask(&mask, sigmask(SIGKILL) | sigmask(SIGSTOP));
	signotset(&mask);

	spin_lock_irq(&tsk->sighand->siglock);
	sig = dequeue_signal(tsk, &mask, info);
	if (!sig && timeout) {
		*/
		 None ready, temporarily unblock those we're interested
		 while we are sleeping in so that we'll be awakened when
		 they arrive. Unblocking is always fine, we can avoid
		 set_current_blocked().
		 /*
		tsk->real_blocked = tsk->blocked;
		sigandsets(&tsk->blocked, &tsk->blocked, &mask);
		recalc_sigpending();
		spin_unlock_irq(&tsk->sighand->siglock);

		timeout = freezable_schedule_timeout_interruptible(timeout);

		spin_lock_irq(&tsk->sighand->siglock);
		__set_task_blocked(tsk, &tsk->real_blocked);
		sigemptyset(&tsk->real_blocked);
		sig = dequeue_signal(tsk, &mask, info);
	}
	spin_unlock_irq(&tsk->sighand->siglock);

	if (sig)
		return sig;
	return timeout ? -EINTR : -EAGAIN;
}

*/
  sys_rt_sigtimedwait - synchronously wait for queued signals specified
			in @uthese
  @uthese: queued signals to wait for
  @uinfo: if non-null, the signal's siginfo is returned here
  @uts: upper bound on process time suspension
  @sigsetsize: size of sigset_t type
 /*
SYSCALL_DEFINE4(rt_sigtimedwait, const sigset_t __user, uthese,
		siginfo_t __user, uinfo, const struct timespec __user, uts,
		size_t, sigsetsize)
{
	sigset_t these;
	struct timespec ts;
	siginfo_t info;
	int ret;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (copy_from_user(&these, uthese, sizeof(these)))
		return -EFAULT;

	if (uts) {
		if (copy_from_user(&ts, uts, sizeof(ts)))
			return -EFAULT;
	}

	ret = do_sigtimedwait(&these, &info, uts ? &ts : NULL);

	if (ret > 0 && uinfo) {
		if (copy_siginfo_to_user(uinfo, &info))
			ret = -EFAULT;
	}

	return ret;
}

*/
  sys_kill - send a signal to a process
  @pid: the PID of the process
  @sig: signal to be sent
 /*
SYSCALL_DEFINE2(kill, pid_t, pid, int, sig)
{
	struct siginfo info;

	info.si_signo = sig;
	info.si_errno = 0;
	info.si_code = SI_USER;
	info.si_pid = task_tgid_vnr(current);
	info.si_uid = from_kuid_munged(current_user_ns(), current_uid());

	return kill_something_info(sig, &info, pid);
}

static int
do_send_specific(pid_t tgid, pid_t pid, int sig, struct siginfoinfo)
{
	struct task_structp;
	int error = -ESRCH;

	rcu_read_lock();
	p = find_task_by_vpid(pid);
	if (p && (tgid <= 0 || task_tgid_vnr(p) == tgid)) {
		error = check_kill_permission(sig, info, p);
		*/
		 The null signal is a permissions and process existence
		 probe.  No signal is actually delivered.
		 /*
		if (!error && sig) {
			error = do_send_sig_info(sig, info, p, false);
			*/
			 If lock_task_sighand() failed we pretend the task
			 dies after receiving the signal. The window is tiny,
			 and the signal is private anyway.
			 /*
			if (unlikely(error == -ESRCH))
				error = 0;
		}
	}
	rcu_read_unlock();

	return error;
}

static int do_tkill(pid_t tgid, pid_t pid, int sig)
{
	struct siginfo info = {};

	info.si_signo = sig;
	info.si_errno = 0;
	info.si_code = SI_TKILL;
	info.si_pid = task_tgid_vnr(current);
	info.si_uid = from_kuid_munged(current_user_ns(), current_uid());

	return do_send_specific(tgid, pid, sig, &info);
}

*/
  sys_tgkill - send signal to one specific thread
  @tgid: the thread group ID of the thread
  @pid: the PID of the thread
  @sig: signal to be sent

  This syscall also checks the @tgid and returns -ESRCH even if the PID
  exists but it's not belonging to the target process anymore. This
  method solves the problem of threads exiting and PIDs getting reused.
 /*
SYSCALL_DEFINE3(tgkill, pid_t, tgid, pid_t, pid, int, sig)
{
	*/ This is only valid for single tasks /*
	if (pid <= 0 || tgid <= 0)
		return -EINVAL;

	return do_tkill(tgid, pid, sig);
}

*/
  sys_tkill - send signal to one specific task
  @pid: the PID of the task
  @sig: signal to be sent

  Send a signal to only one task, even if it's a CLONE_THREAD task.
 /*
SYSCALL_DEFINE2(tkill, pid_t, pid, int, sig)
{
	*/ This is only valid for single tasks /*
	if (pid <= 0)
		return -EINVAL;

	return do_tkill(0, pid, sig);
}

static int do_rt_sigqueueinfo(pid_t pid, int sig, siginfo_tinfo)
{
	*/ Not even root can pretend to send signals from the kernel.
	 Nor can they impersonate a kill()/tgkill(), which adds source info.
	 /*
	if ((info->si_code >= 0 || info->si_code == SI_TKILL) &&
	    (task_pid_vnr(current) != pid))
		return -EPERM;

	info->si_signo = sig;

	*/ POSIX.1b doesn't mention process groups.  /*
	return kill_proc_info(sig, info, pid);
}

*/
  sys_rt_sigqueueinfo - send signal information to a signal
  @pid: the PID of the thread
  @sig: signal to be sent
  @uinfo: signal info to be sent
 /*
SYSCALL_DEFINE3(rt_sigqueueinfo, pid_t, pid, int, sig,
		siginfo_t __user, uinfo)
{
	siginfo_t info;
	if (copy_from_user(&info, uinfo, sizeof(siginfo_t)))
		return -EFAULT;
	return do_rt_sigqueueinfo(pid, sig, &info);
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE3(rt_sigqueueinfo,
			compat_pid_t, pid,
			int, sig,
			struct compat_siginfo __user, uinfo)
{
	siginfo_t info = {};
	int ret = copy_siginfo_from_user32(&info, uinfo);
	if (unlikely(ret))
		return ret;
	return do_rt_sigqueueinfo(pid, sig, &info);
}
#endif

static int do_rt_tgsigqueueinfo(pid_t tgid, pid_t pid, int sig, siginfo_tinfo)
{
	*/ This is only valid for single tasks /*
	if (pid <= 0 || tgid <= 0)
		return -EINVAL;

	*/ Not even root can pretend to send signals from the kernel.
	 Nor can they impersonate a kill()/tgkill(), which adds source info.
	 /*
	if ((info->si_code >= 0 || info->si_code == SI_TKILL) &&
	    (task_pid_vnr(current) != pid))
		return -EPERM;

	info->si_signo = sig;

	return do_send_specific(tgid, pid, sig, info);
}

SYSCALL_DEFINE4(rt_tgsigqueueinfo, pid_t, tgid, pid_t, pid, int, sig,
		siginfo_t __user, uinfo)
{
	siginfo_t info;

	if (copy_from_user(&info, uinfo, sizeof(siginfo_t)))
		return -EFAULT;

	return do_rt_tgsigqueueinfo(tgid, pid, sig, &info);
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE4(rt_tgsigqueueinfo,
			compat_pid_t, tgid,
			compat_pid_t, pid,
			int, sig,
			struct compat_siginfo __user, uinfo)
{
	siginfo_t info = {};

	if (copy_siginfo_from_user32(&info, uinfo))
		return -EFAULT;
	return do_rt_tgsigqueueinfo(tgid, pid, sig, &info);
}
#endif

*/
 For kthreads only, must not be used if cloned with CLONE_SIGHAND
 /*
void kernel_sigaction(int sig, __sighandler_t action)
{
	spin_lock_irq(&current->sighand->siglock);
	current->sighand->action[sig - 1].sa.sa_handler = action;
	if (action == SIG_IGN) {
		sigset_t mask;

		sigemptyset(&mask);
		sigaddset(&mask, sig);

		flush_sigqueue_mask(&mask, &current->signal->shared_pending);
		flush_sigqueue_mask(&mask, &current->pending);
		recalc_sigpending();
	}
	spin_unlock_irq(&current->sighand->siglock);
}
EXPORT_SYMBOL(kernel_sigaction);

int do_sigaction(int sig, struct k_sigactionact, struct k_sigactionoact)
{
	struct task_structp = current,t;
	struct k_sigactionk;
	sigset_t mask;

	if (!valid_signal(sig) || sig < 1 || (act && sig_kernel_only(sig)))
		return -EINVAL;

	k = &p->sighand->action[sig-1];

	spin_lock_irq(&p->sighand->siglock);
	if (oact)
		*oact =k;

	if (act) {
		sigdelsetmask(&act->sa.sa_mask,
			      sigmask(SIGKILL) | sigmask(SIGSTOP));
		*k =act;
		*/
		 POSIX 3.3.1.3:
		  "Setting a signal action to SIG_IGN for a signal that is
		   pending shall cause the pending signal to be discarded,
		   whether or not it is blocked."
		
		  "Setting a signal action to SIG_DFL for a signal that is
		   pending and whose default action is to ignore the signal
		   (for example, SIGCHLD), shall cause the pending signal to
		   be discarded, whether or not it is blocked"
		 /*
		if (sig_handler_ignored(sig_handler(p, sig), sig)) {
			sigemptyset(&mask);
			sigaddset(&mask, sig);
			flush_sigqueue_mask(&mask, &p->signal->shared_pending);
			for_each_thread(p, t)
				flush_sigqueue_mask(&mask, &t->pending);
		}
	}

	spin_unlock_irq(&p->sighand->siglock);
	return 0;
}

static int
do_sigaltstack (const stack_t __useruss, stack_t __useruoss, unsigned long sp)
{
	stack_t oss;
	int error;

	oss.ss_sp = (void __user) current->sas_ss_sp;
	oss.ss_size = current->sas_ss_size;
	oss.ss_flags = sas_ss_flags(sp);

	if (uss) {
		void __userss_sp;
		size_t ss_size;
		int ss_flags;

		error = -EFAULT;
		if (!access_ok(VERIFY_READ, uss, sizeof(*uss)))
			goto out;
		error = __get_user(ss_sp, &uss->ss_sp) |
			__get_user(ss_flags, &uss->ss_flags) |
			__get_user(ss_size, &uss->ss_size);
		if (error)
			goto out;

		error = -EPERM;
		if (on_sig_stack(sp))
			goto out;

		error = -EINVAL;
		*/
		 Note - this code used to test ss_flags incorrectly:
		  	  old code may have been written using ss_flags==0
			  to mean ss_flags==SS_ONSTACK (as this was the only
			  way that worked) - this fix preserves that older
			  mechanism.
		 /*
		if (ss_flags != SS_DISABLE && ss_flags != SS_ONSTACK && ss_flags != 0)
			goto out;

		if (ss_flags == SS_DISABLE) {
			ss_size = 0;
			ss_sp = NULL;
		} else {
			error = -ENOMEM;
			if (ss_size < MINSIGSTKSZ)
				goto out;
		}

		current->sas_ss_sp = (unsigned long) ss_sp;
		current->sas_ss_size = ss_size;
	}

	error = 0;
	if (uoss) {
		error = -EFAULT;
		if (!access_ok(VERIFY_WRITE, uoss, sizeof(*uoss)))
			goto out;
		error = __put_user(oss.ss_sp, &uoss->ss_sp) |
			__put_user(oss.ss_size, &uoss->ss_size) |
			__put_user(oss.ss_flags, &uoss->ss_flags);
	}

out:
	return error;
}
SYSCALL_DEFINE2(sigaltstack,const stack_t __user,uss, stack_t __user,uoss)
{
	return do_sigaltstack(uss, uoss, current_user_stack_pointer());
}

int restore_altstack(const stack_t __useruss)
{
	int err = do_sigaltstack(uss, NULL, current_user_stack_pointer());
	*/ squash all but EFAULT for now /*
	return err == -EFAULT ? err : 0;
}

int __save_altstack(stack_t __useruss, unsigned long sp)
{
	struct task_structt = current;
	return  __put_user((void __user)t->sas_ss_sp, &uss->ss_sp) |
		__put_user(sas_ss_flags(sp), &uss->ss_flags) |
		__put_user(t->sas_ss_size, &uss->ss_size);
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE2(sigaltstack,
			const compat_stack_t __user, uss_ptr,
			compat_stack_t __user, uoss_ptr)
{
	stack_t uss, uoss;
	int ret;
	mm_segment_t seg;

	if (uss_ptr) {
		compat_stack_t uss32;

		memset(&uss, 0, sizeof(stack_t));
		if (copy_from_user(&uss32, uss_ptr, sizeof(compat_stack_t)))
			return -EFAULT;
		uss.ss_sp = compat_ptr(uss32.ss_sp);
		uss.ss_flags = uss32.ss_flags;
		uss.ss_size = uss32.ss_size;
	}
	seg = get_fs();
	set_fs(KERNEL_DS);
	ret = do_sigaltstack((stack_t __force __user) (uss_ptr ? &uss : NULL),
			     (stack_t __force __user) &uoss,
			     compat_user_stack_pointer());
	set_fs(seg);
	if (ret >= 0 && uoss_ptr)  {
		if (!access_ok(VERIFY_WRITE, uoss_ptr, sizeof(compat_stack_t)) ||
		    __put_user(ptr_to_compat(uoss.ss_sp), &uoss_ptr->ss_sp) ||
		    __put_user(uoss.ss_flags, &uoss_ptr->ss_flags) ||
		    __put_user(uoss.ss_size, &uoss_ptr->ss_size))
			ret = -EFAULT;
	}
	return ret;
}

int compat_restore_altstack(const compat_stack_t __useruss)
{
	int err = compat_sys_sigaltstack(uss, NULL);
	*/ squash all but -EFAULT for now /*
	return err == -EFAULT ? err : 0;
}

int __compat_save_altstack(compat_stack_t __useruss, unsigned long sp)
{
	struct task_structt = current;
	return  __put_user(ptr_to_compat((void __user)t->sas_ss_sp), &uss->ss_sp) |
		__put_user(sas_ss_flags(sp), &uss->ss_flags) |
		__put_user(t->sas_ss_size, &uss->ss_size);
}
#endif

#ifdef __ARCH_WANT_SYS_SIGPENDING

*/
  sys_sigpending - examine pending signals
  @set: where mask of pending signal is returned
 /*
SYSCALL_DEFINE1(sigpending, old_sigset_t __user, set)
{
	return sys_rt_sigpending((sigset_t __user)set, sizeof(old_sigset_t)); 
}

#endif

#ifdef __ARCH_WANT_SYS_SIGPROCMASK
*/
  sys_sigprocmask - examine and change blocked signals
  @how: whether to add, remove, or set signals
  @nset: signals to add or remove (if non-null)
  @oset: previous value of signal mask if non-null

 Some platforms have their own version with special arguments;
 others support only sys_rt_sigprocmask.
 /*

SYSCALL_DEFINE3(sigprocmask, int, how, old_sigset_t __user, nset,
		old_sigset_t __user, oset)
{
	old_sigset_t old_set, new_set;
	sigset_t new_blocked;

	old_set = current->blocked.sig[0];

	if (nset) {
		if (copy_from_user(&new_set, nset, sizeof(*nset)))
			return -EFAULT;

		new_blocked = current->blocked;

		switch (how) {
		case SIG_BLOCK:
			sigaddsetmask(&new_blocked, new_set);
			break;
		case SIG_UNBLOCK:
			sigdelsetmask(&new_blocked, new_set);
			break;
		case SIG_SETMASK:
			new_blocked.sig[0] = new_set;
			break;
		default:
			return -EINVAL;
		}

		set_current_blocked(&new_blocked);
	}

	if (oset) {
		if (copy_to_user(oset, &old_set, sizeof(*oset)))
			return -EFAULT;
	}

	return 0;
}
#endif */ __ARCH_WANT_SYS_SIGPROCMASK /*

#ifndef CONFIG_ODD_RT_SIGACTION
*/
  sys_rt_sigaction - alter an action taken by a process
  @sig: signal to be sent
  @act: new sigaction
  @oact: used to save the previous sigaction
  @sigsetsize: size of sigset_t type
 /*
SYSCALL_DEFINE4(rt_sigaction, int, sig,
		const struct sigaction __user, act,
		struct sigaction __user, oact,
		size_t, sigsetsize)
{
	struct k_sigaction new_sa, old_sa;
	int ret = -EINVAL;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(sigset_t))
		goto out;

	if (act) {
		if (copy_from_user(&new_sa.sa, act, sizeof(new_sa.sa)))
			return -EFAULT;
	}

	ret = do_sigaction(sig, act ? &new_sa : NULL, oact ? &old_sa : NULL);

	if (!ret && oact) {
		if (copy_to_user(oact, &old_sa.sa, sizeof(old_sa.sa)))
			return -EFAULT;
	}
out:
	return ret;
}
#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE4(rt_sigaction, int, sig,
		const struct compat_sigaction __user, act,
		struct compat_sigaction __user, oact,
		compat_size_t, sigsetsize)
{
	struct k_sigaction new_ka, old_ka;
	compat_sigset_t mask;
#ifdef __ARCH_HAS_SA_RESTORER
	compat_uptr_t restorer;
#endif
	int ret;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(compat_sigset_t))
		return -EINVAL;

	if (act) {
		compat_uptr_t handler;
		ret = get_user(handler, &act->sa_handler);
		new_ka.sa.sa_handler = compat_ptr(handler);
#ifdef __ARCH_HAS_SA_RESTORER
		ret |= get_user(restorer, &act->sa_restorer);
		new_ka.sa.sa_restorer = compat_ptr(restorer);
#endif
		ret |= copy_from_user(&mask, &act->sa_mask, sizeof(mask));
		ret |= get_user(new_ka.sa.sa_flags, &act->sa_flags);
		if (ret)
			return -EFAULT;
		sigset_from_compat(&new_ka.sa.sa_mask, &mask);
	}

	ret = do_sigaction(sig, act ? &new_ka : NULL, oact ? &old_ka : NULL);
	if (!ret && oact) {
		sigset_to_compat(&mask, &old_ka.sa.sa_mask);
		ret = put_user(ptr_to_compat(old_ka.sa.sa_handler), 
			       &oact->sa_handler);
		ret |= copy_to_user(&oact->sa_mask, &mask, sizeof(mask));
		ret |= put_user(old_ka.sa.sa_flags, &oact->sa_flags);
#ifdef __ARCH_HAS_SA_RESTORER
		ret |= put_user(ptr_to_compat(old_ka.sa.sa_restorer),
				&oact->sa_restorer);
#endif
	}
	return ret;
}
#endif
#endif */ !CONFIG_ODD_RT_SIGACTION /*

#ifdef CONFIG_OLD_SIGACTION
SYSCALL_DEFINE3(sigaction, int, sig,
		const struct old_sigaction __user, act,
	        struct old_sigaction __user, oact)
{
	struct k_sigaction new_ka, old_ka;
	int ret;

	if (act) {
		old_sigset_t mask;
		if (!access_ok(VERIFY_READ, act, sizeof(*act)) ||
		    __get_user(new_ka.sa.sa_handler, &act->sa_handler) ||
		    __get_user(new_ka.sa.sa_restorer, &act->sa_restorer) ||
		    __get_user(new_ka.sa.sa_flags, &act->sa_flags) ||
		    __get_user(mask, &act->sa_mask))
			return -EFAULT;
#ifdef __ARCH_HAS_KA_RESTORER
		new_ka.ka_restorer = NULL;
#endif
		siginitset(&new_ka.sa.sa_mask, mask);
	}

	ret = do_sigaction(sig, act ? &new_ka : NULL, oact ? &old_ka : NULL);

	if (!ret && oact) {
		if (!access_ok(VERIFY_WRITE, oact, sizeof(*oact)) ||
		    __put_user(old_ka.sa.sa_handler, &oact->sa_handler) ||
		    __put_user(old_ka.sa.sa_restorer, &oact->sa_restorer) ||
		    __put_user(old_ka.sa.sa_flags, &oact->sa_flags) ||
		    __put_user(old_ka.sa.sa_mask.sig[0], &oact->sa_mask))
			return -EFAULT;
	}

	return ret;
}
#endif
#ifdef CONFIG_COMPAT_OLD_SIGACTION
COMPAT_SYSCALL_DEFINE3(sigaction, int, sig,
		const struct compat_old_sigaction __user, act,
	        struct compat_old_sigaction __user, oact)
{
	struct k_sigaction new_ka, old_ka;
	int ret;
	compat_old_sigset_t mask;
	compat_uptr_t handler, restorer;

	if (act) {
		if (!access_ok(VERIFY_READ, act, sizeof(*act)) ||
		    __get_user(handler, &act->sa_handler) ||
		    __get_user(restorer, &act->sa_restorer) ||
		    __get_user(new_ka.sa.sa_flags, &act->sa_flags) ||
		    __get_user(mask, &act->sa_mask))
			return -EFAULT;

#ifdef __ARCH_HAS_KA_RESTORER
		new_ka.ka_restorer = NULL;
#endif
		new_ka.sa.sa_handler = compat_ptr(handler);
		new_ka.sa.sa_restorer = compat_ptr(restorer);
		siginitset(&new_ka.sa.sa_mask, mask);
	}

	ret = do_sigaction(sig, act ? &new_ka : NULL, oact ? &old_ka : NULL);

	if (!ret && oact) {
		if (!access_ok(VERIFY_WRITE, oact, sizeof(*oact)) ||
		    __put_user(ptr_to_compat(old_ka.sa.sa_handler),
			       &oact->sa_handler) ||
		    __put_user(ptr_to_compat(old_ka.sa.sa_restorer),
			       &oact->sa_restorer) ||
		    __put_user(old_ka.sa.sa_flags, &oact->sa_flags) ||
		    __put_user(old_ka.sa.sa_mask.sig[0], &oact->sa_mask))
			return -EFAULT;
	}
	return ret;
}
#endif

#ifdef CONFIG_SGETMASK_SYSCALL

*/
 For backwards compatibility.  Functionality superseded by sigprocmask.
 /*
SYSCALL_DEFINE0(sgetmask)
{
	*/ SMP safe /*
	return current->blocked.sig[0];
}

SYSCALL_DEFINE1(ssetmask, int, newmask)
{
	int old = current->blocked.sig[0];
	sigset_t newset;

	siginitset(&newset, newmask);
	set_current_blocked(&newset);

	return old;
}
#endif */ CONFIG_SGETMASK_SYSCALL /*

#ifdef __ARCH_WANT_SYS_SIGNAL
*/
 For backwards compatibility.  Functionality superseded by sigaction.
 /*
SYSCALL_DEFINE2(signal, int, sig, __sighandler_t, handler)
{
	struct k_sigaction new_sa, old_sa;
	int ret;

	new_sa.sa.sa_handler = handler;
	new_sa.sa.sa_flags = SA_ONESHOT | SA_NOMASK;
	sigemptyset(&new_sa.sa.sa_mask);

	ret = do_sigaction(sig, &new_sa, &old_sa);

	return ret ? ret : (unsigned long)old_sa.sa.sa_handler;
}
#endif */ __ARCH_WANT_SYS_SIGNAL /*

#ifdef __ARCH_WANT_SYS_PAUSE

SYSCALL_DEFINE0(pause)
{
	while (!signal_pending(current)) {
		__set_current_state(TASK_INTERRUPTIBLE);
		schedule();
	}
	return -ERESTARTNOHAND;
}

#endif

static int sigsuspend(sigset_tset)
{
	current->saved_sigmask = current->blocked;
	set_current_blocked(set);

	while (!signal_pending(current)) {
		__set_current_state(TASK_INTERRUPTIBLE);
		schedule();
	}
	set_restore_sigmask();
	return -ERESTARTNOHAND;
}

*/
  sys_rt_sigsuspend - replace the signal mask for a value with the
	@unewset value until a signal is received
  @unewset: new signal mask value
  @sigsetsize: size of sigset_t type
 /*
SYSCALL_DEFINE2(rt_sigsuspend, sigset_t __user, unewset, size_t, sigsetsize)
{
	sigset_t newset;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (copy_from_user(&newset, unewset, sizeof(newset)))
		return -EFAULT;
	return sigsuspend(&newset);
}
 
#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE2(rt_sigsuspend, compat_sigset_t __user, unewset, compat_size_t, sigsetsize)
{
#ifdef __BIG_ENDIAN
	sigset_t newset;
	compat_sigset_t newset32;

	*/ XXX: Don't preclude handling different sized sigset_t's.  /*
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (copy_from_user(&newset32, unewset, sizeof(compat_sigset_t)))
		return -EFAULT;
	sigset_from_compat(&newset, &newset32);
	return sigsuspend(&newset);
#else
	*/ on little-endian bitmaps don't care about granularity /*
	return sys_rt_sigsuspend((sigset_t __user)unewset, sigsetsize);
#endif
}
#endif

#ifdef CONFIG_OLD_SIGSUSPEND
SYSCALL_DEFINE1(sigsuspend, old_sigset_t, mask)
{
	sigset_t blocked;
	siginitset(&blocked, mask);
	return sigsuspend(&blocked);
}
#endif
#ifdef CONFIG_OLD_SIGSUSPEND3
SYSCALL_DEFINE3(sigsuspend, int, unused1, int, unused2, old_sigset_t, mask)
{
	sigset_t blocked;
	siginitset(&blocked, mask);
	return sigsuspend(&blocked);
}
#endif

__weak const chararch_vma_name(struct vm_area_structvma)
{
	return NULL;
}

void __init signals_init(void)
{
	*/ If this check fails, the __ARCH_SI_PREAMBLE_SIZE value is wrong! /*
	BUILD_BUG_ON(__ARCH_SI_PREAMBLE_SIZE
		!= offsetof(struct siginfo, _sifields._pad));

	sigqueue_cachep = KMEM_CACHE(sigqueue, SLAB_PANIC);
}

#ifdef CONFIG_KGDB_KDB
#include <linux/kdb.h>
*/
 kdb_send_sig_info - Allows kdb to send signals without exposing
 signal internals.  This function checks if the required locks are
 available before calling the main signal code, to avoid kdb
 deadlocks.
 /*
void
kdb_send_sig_info(struct task_structt, struct siginfoinfo)
{
	static struct task_structkdb_prev_t;
	int sig, new_t;
	if (!spin_trylock(&t->sighand->siglock)) {
		kdb_printf("Can't do kill command now.\n"
			   "The sigmask lock is held somewhere else in "
			   "kernel, try again later\n");
		return;
	}
	spin_unlock(&t->sighand->siglock);
	new_t = kdb_prev_t != t;
	kdb_prev_t = t;
	if (t->state != TASK_RUNNING && new_t) {
		kdb_printf("Process is not RUNNING, sending a signal from "
			   "kdb risks deadlock\n"
			   "on the run queue locks. "
			   "The signal has _not_ been sent.\n"
			   "Reissue the kill command if you want to risk "
			   "the deadlock.\n");
		return;
	}
	sig = info->si_signo;
	if (send_sig_info(sig, info, t))
		kdb_printf("Fail to deliver Signal %d to process %d.\n",
			   sig, t->pid);
	else
		kdb_printf("Signal %d is sent to process %d.\n", sig, t->pid);
}
#endif	*/ CONFIG_KGDB_KDB
