
 Generate definitions needed by the preprocessor.
 This code generates raw asm output which is post-processed
 to extract and format the required data.
 /*

#define __GENERATING_BOUNDS_H
*/ Include headers that define the enum constants of interest /*
#include <linux/page-flags.h>
#include <linux/mmzone.h>
#include <linux/kbuild.h>
#include <linux/log2.h>
#include <linux/spinlock_types.h>

void foo(void)
{
	*/ The enum constants to put into include/generated/bounds.h /*
	DEFINE(NR_PAGEFLAGS, __NR_PAGEFLAGS);
	DEFINE(MAX_NR_ZONES, __MAX_NR_ZONES);
#ifdef CONFIG_SMP
	DEFINE(NR_CPUS_BITS, ilog2(CONFIG_NR_CPUS));
#endif
	DEFINE(SPINLOCK_SIZE, sizeof(spinlock_t));
	*/ End of constants /*
}
*/

 linux/kernel/capability.c

 Copyright (C) 1997  Andrew Main <zefram@fysh.org>

 Integrated into 2.1.97+,  Andrew G. Morgan <morgan@kernel.org>
 30 May 2002:	Cleanup, Robert M. Love <rml@tech9.net>
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/audit.h>
#include <linux/capability.h>
#include <linux/mm.h>
#include <linux/export.h>
#include <linux/security.h>
#include <linux/syscalls.h>
#include <linux/pid_namespace.h>
#include <linux/user_namespace.h>
#include <asm/uaccess.h>

*/
 Leveraged for setting/resetting capabilities
 /*

const kernel_cap_t __cap_empty_set = CAP_EMPTY_SET;
EXPORT_SYMBOL(__cap_empty_set);

int file_caps_enabled = 1;

static int __init file_caps_disable(charstr)
{
	file_caps_enabled = 0;
	return 1;
}
__setup("no_file_caps", file_caps_disable);

#ifdef CONFIG_MULTIUSER
*/
 More recent versions of libcap are available from:

   http://www.kernel.org/pub/linux/libs/security/linux-privs/
 /*

static void warn_legacy_capability_use(void)
{
	char name[sizeof(current->comm)];

	pr_info_once("warning: `%s' uses 32-bit capabilities (legacy support in use)\n",
		     get_task_comm(name, current));
}

*/
 Version 2 capabilities worked fine, but the linux/capability.h file
 that accompanied their introduction encouraged their use without
 the necessary user-space source code changes. As such, we have
 created a version 3 with equivalent functionality to version 2, but
 with a header change to protect legacy source code from using
 version 2 when it wanted to use version 1. If your system has code
 that trips the following warning, it is using version 2 specific
 capabilities and may be doing so insecurely.

 The remedy is to either upgrade your version of libcap (to 2.10+,
 if the application is linked against it), or recompile your
 application with modern kernel headers and this warning will go
 away.
 /*

static void warn_deprecated_v2(void)
{
	char name[sizeof(current->comm)];

	pr_info_once("warning: `%s' uses deprecated v2 capabilities in a way that may be insecure\n",
		     get_task_comm(name, current));
}

*/
 Version check. Return the number of u32s in each capability flag
 array, or a negative value on error.
 /*
static int cap_validate_magic(cap_user_header_t header, unsignedtocopy)
{
	__u32 version;

	if (get_user(version, &header->version))
		return -EFAULT;

	switch (version) {
	case _LINUX_CAPABILITY_VERSION_1:
		warn_legacy_capability_use();
		*tocopy = _LINUX_CAPABILITY_U32S_1;
		break;
	case _LINUX_CAPABILITY_VERSION_2:
		warn_deprecated_v2();
		*/
		 fall through - v3 is otherwise equivalent to v2.
		 /*
	case _LINUX_CAPABILITY_VERSION_3:
		*tocopy = _LINUX_CAPABILITY_U32S_3;
		break;
	default:
		if (put_user((u32)_KERNEL_CAPABILITY_VERSION, &header->version))
			return -EFAULT;
		return -EINVAL;
	}

	return 0;
}

*/
 The only thing that can change the capabilities of the current
 process is the current process. As such, we can't be in this code
 at the same time as we are in the process of setting capabilities
 in this process. The net result is that we can limit our use of
 locks to when we are reading the caps of another process.
 /*
static inline int cap_get_target_pid(pid_t pid, kernel_cap_tpEp,
				     kernel_cap_tpIp, kernel_cap_tpPp)
{
	int ret;

	if (pid && (pid != task_pid_vnr(current))) {
		struct task_structtarget;

		rcu_read_lock();

		target = find_task_by_vpid(pid);
		if (!target)
			ret = -ESRCH;
		else
			ret = security_capget(target, pEp, pIp, pPp);

		rcu_read_unlock();
	} else
		ret = security_capget(current, pEp, pIp, pPp);

	return ret;
}

*/
 sys_capget - get the capabilities of a given process.
 @header: pointer to struct that contains capability version and
	target pid data
 @dataptr: pointer to struct that contains the effective, permitted,
	and inheritable capabilities that are returned

 Returns 0 on success and < 0 on error.
 /*
SYSCALL_DEFINE2(capget, cap_user_header_t, header, cap_user_data_t, dataptr)
{
	int ret = 0;
	pid_t pid;
	unsigned tocopy;
	kernel_cap_t pE, pI, pP;

	ret = cap_validate_magic(header, &tocopy);
	if ((dataptr == NULL) || (ret != 0))
		return ((dataptr == NULL) && (ret == -EINVAL)) ? 0 : ret;

	if (get_user(pid, &header->pid))
		return -EFAULT;

	if (pid < 0)
		return -EINVAL;

	ret = cap_get_target_pid(pid, &pE, &pI, &pP);
	if (!ret) {
		struct __user_cap_data_struct kdata[_KERNEL_CAPABILITY_U32S];
		unsigned i;

		for (i = 0; i < tocopy; i++) {
			kdata[i].effective = pE.cap[i];
			kdata[i].permitted = pP.cap[i];
			kdata[i].inheritable = pI.cap[i];
		}

		*/
		 Note, in the case, tocopy < _KERNEL_CAPABILITY_U32S,
		 we silently drop the upper capabilities here. This
		 has the effect of making older libcap
		 implementations implicitly drop upper capability
		 bits when they perform a: capget/modify/capset
		 sequence.
		
		 This behavior is considered fail-safe
		 behavior. Upgrading the application to a newer
		 version of libcap will enable access to the newer
		 capabilities.
		
		 An alternative would be to return an error here
		 (-ERANGE), but that causes legacy applications to
		 unexpectedly fail; the capget/modify/capset aborts
		 before modification is attempted and the application
		 fails.
		 /*
		if (copy_to_user(dataptr, kdata, tocopy
				 sizeof(struct __user_cap_data_struct))) {
			return -EFAULT;
		}
	}

	return ret;
}

*/
 sys_capset - set capabilities for a process or (*) a group of processes
 @header: pointer to struct that contains capability version and
	target pid data
 @data: pointer to struct that contains the effective, permitted,
	and inheritable capabilities

 Set capabilities for the current process only.  The ability to any other
 process(es) has been deprecated and removed.

 The restrictions on setting capabilities are specified as:

 I: any raised capabilities must be a subset of the old permitted
 P: any raised capabilities must be a subset of the old permitted
 E: must be set to a subset of new permitted

 Returns 0 on success and < 0 on error.
 /*
SYSCALL_DEFINE2(capset, cap_user_header_t, header, const cap_user_data_t, data)
{
	struct __user_cap_data_struct kdata[_KERNEL_CAPABILITY_U32S];
	unsigned i, tocopy, copybytes;
	kernel_cap_t inheritable, permitted, effective;
	struct crednew;
	int ret;
	pid_t pid;

	ret = cap_validate_magic(header, &tocopy);
	if (ret != 0)
		return ret;

	if (get_user(pid, &header->pid))
		return -EFAULT;

	*/ may only affect current now /*
	if (pid != 0 && pid != task_pid_vnr(current))
		return -EPERM;

	copybytes = tocopy sizeof(struct __user_cap_data_struct);
	if (copybytes > sizeof(kdata))
		return -EFAULT;

	if (copy_from_user(&kdata, data, copybytes))
		return -EFAULT;

	for (i = 0; i < tocopy; i++) {
		effective.cap[i] = kdata[i].effective;
		permitted.cap[i] = kdata[i].permitted;
		inheritable.cap[i] = kdata[i].inheritable;
	}
	while (i < _KERNEL_CAPABILITY_U32S) {
		effective.cap[i] = 0;
		permitted.cap[i] = 0;
		inheritable.cap[i] = 0;
		i++;
	}

	effective.cap[CAP_LAST_U32] &= CAP_LAST_U32_VALID_MASK;
	permitted.cap[CAP_LAST_U32] &= CAP_LAST_U32_VALID_MASK;
	inheritable.cap[CAP_LAST_U32] &= CAP_LAST_U32_VALID_MASK;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;

	ret = security_capset(new, current_cred(),
			      &effective, &inheritable, &permitted);
	if (ret < 0)
		goto error;

	audit_log_capset(new, current_cred());

	return commit_creds(new);

error:
	abort_creds(new);
	return ret;
}

*/
 has_ns_capability - Does a task have a capability in a specific user ns
 @t: The task in question
 @ns: target user namespace
 @cap: The capability to be tested for

 Return true if the specified task has the given superior capability
 currently in effect to the specified user namespace, false if not.

 Note that this does not set PF_SUPERPRIV on the task.
 /*
bool has_ns_capability(struct task_structt,
		       struct user_namespacens, int cap)
{
	int ret;

	rcu_read_lock();
	ret = security_capable(__task_cred(t), ns, cap);
	rcu_read_unlock();

	return (ret == 0);
}

*/
 has_capability - Does a task have a capability in init_user_ns
 @t: The task in question
 @cap: The capability to be tested for

 Return true if the specified task has the given superior capability
 currently in effect to the initial user namespace, false if not.

 Note that this does not set PF_SUPERPRIV on the task.
 /*
bool has_capability(struct task_structt, int cap)
{
	return has_ns_capability(t, &init_user_ns, cap);
}

*/
 has_ns_capability_noaudit - Does a task have a capability (unaudited)
 in a specific user ns.
 @t: The task in question
 @ns: target user namespace
 @cap: The capability to be tested for

 Return true if the specified task has the given superior capability
 currently in effect to the specified user namespace, false if not.
 Do not write an audit message for the check.

 Note that this does not set PF_SUPERPRIV on the task.
 /*
bool has_ns_capability_noaudit(struct task_structt,
			       struct user_namespacens, int cap)
{
	int ret;

	rcu_read_lock();
	ret = security_capable_noaudit(__task_cred(t), ns, cap);
	rcu_read_unlock();

	return (ret == 0);
}

*/
 has_capability_noaudit - Does a task have a capability (unaudited) in the
 initial user ns
 @t: The task in question
 @cap: The capability to be tested for

 Return true if the specified task has the given superior capability
 currently in effect to init_user_ns, false if not.  Don't write an
 audit message for the check.

 Note that this does not set PF_SUPERPRIV on the task.
 /*
bool has_capability_noaudit(struct task_structt, int cap)
{
	return has_ns_capability_noaudit(t, &init_user_ns, cap);
}

*/
 ns_capable - Determine if the current task has a superior capability in effect
 @ns:  The usernamespace we want the capability in
 @cap: The capability to be tested for

 Return true if the current task has the given superior capability currently
 available for use, false if not.

 This sets PF_SUPERPRIV on the task if the capability is available on the
 assumption that it's about to be used.
 /*
bool ns_capable(struct user_namespacens, int cap)
{
	if (unlikely(!cap_valid(cap))) {
		pr_crit("capable() called with invalid cap=%u\n", cap);
		BUG();
	}

	if (security_capable(current_cred(), ns, cap) == 0) {
		current->flags |= PF_SUPERPRIV;
		return true;
	}
	return false;
}
EXPORT_SYMBOL(ns_capable);


*/
 capable - Determine if the current task has a superior capability in effect
 @cap: The capability to be tested for

 Return true if the current task has the given superior capability currently
 available for use, false if not.

 This sets PF_SUPERPRIV on the task if the capability is available on the
 assumption that it's about to be used.
 /*
bool capable(int cap)
{
	return ns_capable(&init_user_ns, cap);
}
EXPORT_SYMBOL(capable);
#endif */ CONFIG_MULTIUSER /*

*/
 file_ns_capable - Determine if the file's opener had a capability in effect
 @file:  The file we want to check
 @ns:  The usernamespace we want the capability in
 @cap: The capability to be tested for

 Return true if task that opened the file had a capability in effect
 when the file was opened.

 This does not set PF_SUPERPRIV because the caller may not
 actually be privileged.
 /*
bool file_ns_capable(const struct filefile, struct user_namespacens,
		     int cap)
{
	if (WARN_ON_ONCE(!cap_valid(cap)))
		return false;

	if (security_capable(file->f_cred, ns, cap) == 0)
		return true;

	return false;
}
EXPORT_SYMBOL(file_ns_capable);

*/
 capable_wrt_inode_uidgid - Check nsown_capable and uid and gid mapped
 @inode: The inode in question
 @cap: The capability in question

 Return true if the current task has the given capability targeted at
 its own user namespace and that the given inode's uid and gid are
 mapped into the current user namespace.
 /*
bool capable_wrt_inode_uidgid(const struct inodeinode, int cap)
{
	struct user_namespacens = current_user_ns();

	return ns_capable(ns, cap) && kuid_has_mapping(ns, inode->i_uid) &&
		kgid_has_mapping(ns, inode->i_gid);
}
EXPORT_SYMBOL(capable_wrt_inode_uidgid);
*/

  Generic process-grouping system.

  Based originally on the cpuset system, extracted by Paul Menage
  Copyright (C) 2006 Google, Inc

  Notifications support
  Copyright (C) 2009 Nokia Corporation
  Author: Kirill A. Shutemov

  Copyright notices from the original cpuset code:
  --------------------------------------------------
  Copyright (C) 2003 BULL SA.
  Copyright (C) 2004-2006 Silicon Graphics, Inc.

  Portions derived from Patrick Mochel's sysfs code.
  sysfs is Copyright (c) 2001-3 Patrick Mochel

  2003-10-10 Written by Simon Derr.
  2003-10-22 Updates by Stephen Hemminger.
  2004 May-July Rework by Paul Jackson.
  ---------------------------------------------------

  This file is subject to the terms and conditions of the GNU General Public
  License.  See the file COPYING in the main directory of the Linux
  distribution for more details.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/cgroup.h>
#include <linux/cred.h>
#include <linux/ctype.h>
#include <linux/errno.h>
#include <linux/init_task.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/magic.h>
#include <linux/mm.h>
#include <linux/mutex.h>
#include <linux/mount.h>
#include <linux/pagemap.h>
#include <linux/proc_fs.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/percpu-rwsem.h>
#include <linux/string.h>
#include <linux/sort.h>
#include <linux/kmod.h>
#include <linux/delayacct.h>
#include <linux/cgroupstats.h>
#include <linux/hashtable.h>
#include <linux/pid_namespace.h>
#include <linux/idr.h>
#include <linux/vmalloc.h>/ TODO: replace with more sophisticated array /*
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/atomic.h>
#include <linux/cpuset.h>
#include <linux/proc_ns.h>
#include <linux/nsproxy.h>
#include <linux/proc_ns.h>
#include <net/sock.h>

*/
 pidlists linger the following amount before being destroyed.  The goal
 is avoiding frequent destruction in the middle of consecutive read calls
 Expiring in the middle is a performance problem not a correctness one.
 1 sec should be enough.
 /*
#define CGROUP_PIDLIST_DESTROY_DELAY	HZ

#define CGROUP_FILE_NAME_MAX		(MAX_CGROUP_TYPE_NAMELEN +	\
					 MAX_CFTYPE_NAME + 2)

*/
 cgroup_mutex is the master lock.  Any modification to cgroup or its
 hierarchy must be performed while holding it.

 css_set_lock protects task->cgroups pointer, the list of css_set
 objects, and the chain of tasks off each css_set.

 These locks are exported if CONFIG_PROVE_RCU so that accessors in
 cgroup.h can use them for lockdep annotations.
 /*
#ifdef CONFIG_PROVE_RCU
DEFINE_MUTEX(cgroup_mutex);
DEFINE_SPINLOCK(css_set_lock);
EXPORT_SYMBOL_GPL(cgroup_mutex);
EXPORT_SYMBOL_GPL(css_set_lock);
#else
static DEFINE_MUTEX(cgroup_mutex);
static DEFINE_SPINLOCK(css_set_lock);
#endif

*/
 Protects cgroup_idr and css_idr so that IDs can be released without
 grabbing cgroup_mutex.
 /*
static DEFINE_SPINLOCK(cgroup_idr_lock);

*/
 Protects cgroup_file->kn for !self csses.  It synchronizes notifications
 against file removal/re-creation across css hiding.
 /*
static DEFINE_SPINLOCK(cgroup_file_kn_lock);

*/
 Protects cgroup_subsys->release_agent_path.  Modifying it also requires
 cgroup_mutex.  Reading requires either cgroup_mutex or this spinlock.
 /*
static DEFINE_SPINLOCK(release_agent_path_lock);

struct percpu_rw_semaphore cgroup_threadgroup_rwsem;

#define cgroup_assert_mutex_or_rcu_locked()				\
	RCU_LOCKDEP_WARN(!rcu_read_lock_held() &&			\
			   !lockdep_is_held(&cgroup_mutex),		\
			   "cgroup_mutex or RCU read lock required");

*/
 cgroup destruction makes heavy use of work items and there can be a lot
 of concurrent destructions.  Use a separate workqueue so that cgroup
 destruction work items don't end up filling up max_active of system_wq
 which may lead to deadlock.
 /*
static struct workqueue_structcgroup_destroy_wq;

*/
 pidlist destructions need to be flushed on cgroup destruction.  Use a
 separate workqueue as flush domain.
 /*
static struct workqueue_structcgroup_pidlist_destroy_wq;

*/ generate an array of cgroup subsystem pointers /*
#define SUBSYS(_x) [_x ## _cgrp_id] = &_x ## _cgrp_subsys,
static struct cgroup_subsyscgroup_subsys[] = {
#include <linux/cgroup_subsys.h>
};
#undef SUBSYS

*/ array of cgroup subsystem names /*
#define SUBSYS(_x) [_x ## _cgrp_id] = #_x,
static const charcgroup_subsys_name[] = {
#include <linux/cgroup_subsys.h>
};
#undef SUBSYS

*/ array of static_keys for cgroup_subsys_enabled() and cgroup_subsys_on_dfl() /*
#define SUBSYS(_x)								\
	DEFINE_STATIC_KEY_TRUE(_x ## _cgrp_subsys_enabled_key);			\
	DEFINE_STATIC_KEY_TRUE(_x ## _cgrp_subsys_on_dfl_key);			\
	EXPORT_SYMBOL_GPL(_x ## _cgrp_subsys_enabled_key);			\
	EXPORT_SYMBOL_GPL(_x ## _cgrp_subsys_on_dfl_key);
#include <linux/cgroup_subsys.h>
#undef SUBSYS

#define SUBSYS(_x) [_x ## _cgrp_id] = &_x ## _cgrp_subsys_enabled_key,
static struct static_key_truecgroup_subsys_enabled_key[] = {
#include <linux/cgroup_subsys.h>
};
#undef SUBSYS

#define SUBSYS(_x) [_x ## _cgrp_id] = &_x ## _cgrp_subsys_on_dfl_key,
static struct static_key_truecgroup_subsys_on_dfl_key[] = {
#include <linux/cgroup_subsys.h>
};
#undef SUBSYS

*/
 The default hierarchy, reserved for the subsystems that are otherwise
 unattached - it never has more than a single cgroup, and all tasks are
 part of that cgroup.
 /*
struct cgroup_root cgrp_dfl_root;
EXPORT_SYMBOL_GPL(cgrp_dfl_root);

*/
 The default hierarchy always exists but is hidden until mounted for the
 first time.  This is for backward compatibility.
 /*
static bool cgrp_dfl_visible;

*/ Controllers blocked by the commandline in v1 /*
static u16 cgroup_no_v1_mask;

*/ some controllers are not supported in the default hierarchy /*
static u16 cgrp_dfl_inhibit_ss_mask;

*/ some controllers are implicitly enabled on the default hierarchy /*
static unsigned long cgrp_dfl_implicit_ss_mask;

*/ The list of hierarchy roots /*

static LIST_HEAD(cgroup_roots);
static int cgroup_root_count;

*/ hierarchy ID allocation and mapping, protected by cgroup_mutex /*
static DEFINE_IDR(cgroup_hierarchy_idr);

*/
 Assign a monotonically increasing serial number to csses.  It guarantees
 cgroups with bigger numbers are newer than those with smaller numbers.
 Also, as csses are always appended to the parent's ->children list, it
 guarantees that sibling csses are always sorted in the ascending serial
 number order on the list.  Protected by cgroup_mutex.
 /*
static u64 css_serial_nr_next = 1;

*/
 These bitmask flags indicate whether tasks in the fork and exit paths have
 fork/exit handlers to call. This avoids us having to do extra work in the
 fork/exit path to check which subsystems have fork/exit callbacks.
 /*
static u16 have_fork_callback __read_mostly;
static u16 have_exit_callback __read_mostly;
static u16 have_free_callback __read_mostly;

*/ cgroup namespace for init task /*
struct cgroup_namespace init_cgroup_ns = {
	.count		= { .counter = 2, },
	.user_ns	= &init_user_ns,
	.ns.ops		= &cgroupns_operations,
	.ns.inum	= PROC_CGROUP_INIT_INO,
	.root_cset	= &init_css_set,
};

*/ Ditto for the can_fork callback. /*
static u16 have_canfork_callback __read_mostly;

static struct file_system_type cgroup2_fs_type;
static struct cftype cgroup_dfl_base_files[];
static struct cftype cgroup_legacy_base_files[];

static int rebind_subsystems(struct cgroup_rootdst_root, u16 ss_mask);
static void cgroup_lock_and_drain_offline(struct cgroupcgrp);
static int cgroup_apply_control(struct cgroupcgrp);
static void cgroup_finalize_control(struct cgroupcgrp, int ret);
static void css_task_iter_advance(struct css_task_iterit);
static int cgroup_destroy_locked(struct cgroupcgrp);
static struct cgroup_subsys_statecss_create(struct cgroupcgrp,
					      struct cgroup_subsysss);
static void css_release(struct percpu_refref);
static void kill_css(struct cgroup_subsys_statecss);
static int cgroup_addrm_files(struct cgroup_subsys_statecss,
			      struct cgroupcgrp, struct cftype cfts[],
			      bool is_add);

*/
 cgroup_ssid_enabled - cgroup subsys enabled test by subsys ID
 @ssid: subsys ID of interest

 cgroup_subsys_enabled() can only be used with literal subsys names which
 is fine for individual subsystems but unsuitable for cgroup core.  This
 is slower static_key_enabled() based test indexed by @ssid.
 /*
static bool cgroup_ssid_enabled(int ssid)
{
	if (CGROUP_SUBSYS_COUNT == 0)
		return false;

	return static_key_enabled(cgroup_subsys_enabled_key[ssid]);
}

static bool cgroup_ssid_no_v1(int ssid)
{
	return cgroup_no_v1_mask & (1 << ssid);
}

*/
 cgroup_on_dfl - test whether a cgroup is on the default hierarchy
 @cgrp: the cgroup of interest

 The default hierarchy is the v2 interface of cgroup and this function
 can be used to test whether a cgroup is on the default hierarchy for
 cases where a subsystem should behave differnetly depending on the
 interface version.

 The set of behaviors which change on the default hierarchy are still
 being determined and the mount option is prefixed with __DEVEL__.

 List of changed behaviors:

 - Mount options "noprefix", "xattr", "clone_children", "release_agent"
   and "name" are disallowed.

 - When mounting an existing superblock, mount options should match.

 - Remount is disallowed.

 - rename(2) is disallowed.

 - "tasks" is removed.  Everything should be at process granularity.  Use
   "cgroup.procs" instead.

 - "cgroup.procs" is not sorted.  pids will be unique unless they got
   recycled inbetween reads.

 - "release_agent" and "notify_on_release" are removed.  Replacement
   notification mechanism will be implemented.

 - "cgroup.clone_children" is removed.

 - "cgroup.subtree_populated" is available.  Its value is 0 if the cgroup
   and its descendants contain no task; otherwise, 1.  The file also
   generates kernfs notification which can be monitored through poll and
   [di]notify when the value of the file changes.

 - cpuset: tasks will be kept in empty cpusets when hotplug happens and
   take masks of ancestors with non-empty cpus/mems, instead of being
   moved to an ancestor.

 - cpuset: a task can be moved into an empty cpuset, and again it takes
   masks of ancestors.

 - memcg: use_hierarchy is on by default and the cgroup file for the flag
   is not created.

 - blkcg: blk-throttle becomes properly hierarchical.

 - debug: disallowed on the default hierarchy.
 /*
static bool cgroup_on_dfl(const struct cgroupcgrp)
{
	return cgrp->root == &cgrp_dfl_root;
}

*/ IDR wrappers which synchronize using cgroup_idr_lock /*
static int cgroup_idr_alloc(struct idridr, voidptr, int start, int end,
			    gfp_t gfp_mask)
{
	int ret;

	idr_preload(gfp_mask);
	spin_lock_bh(&cgroup_idr_lock);
	ret = idr_alloc(idr, ptr, start, end, gfp_mask & ~__GFP_DIRECT_RECLAIM);
	spin_unlock_bh(&cgroup_idr_lock);
	idr_preload_end();
	return ret;
}

static voidcgroup_idr_replace(struct idridr, voidptr, int id)
{
	voidret;

	spin_lock_bh(&cgroup_idr_lock);
	ret = idr_replace(idr, ptr, id);
	spin_unlock_bh(&cgroup_idr_lock);
	return ret;
}

static void cgroup_idr_remove(struct idridr, int id)
{
	spin_lock_bh(&cgroup_idr_lock);
	idr_remove(idr, id);
	spin_unlock_bh(&cgroup_idr_lock);
}

static struct cgroupcgroup_parent(struct cgroupcgrp)
{
	struct cgroup_subsys_stateparent_css = cgrp->self.parent;

	if (parent_css)
		return container_of(parent_css, struct cgroup, self);
	return NULL;
}

*/ subsystems visibly enabled on a cgroup /*
static u16 cgroup_control(struct cgroupcgrp)
{
	struct cgroupparent = cgroup_parent(cgrp);
	u16 root_ss_mask = cgrp->root->subsys_mask;

	if (parent)
		return parent->subtree_control;

	if (cgroup_on_dfl(cgrp))
		root_ss_mask &= ~(cgrp_dfl_inhibit_ss_mask |
				  cgrp_dfl_implicit_ss_mask);
	return root_ss_mask;
}

*/ subsystems enabled on a cgroup /*
static u16 cgroup_ss_mask(struct cgroupcgrp)
{
	struct cgroupparent = cgroup_parent(cgrp);

	if (parent)
		return parent->subtree_ss_mask;

	return cgrp->root->subsys_mask;
}

*/
 cgroup_css - obtain a cgroup's css for the specified subsystem
 @cgrp: the cgroup of interest
 @ss: the subsystem of interest (%NULL returns @cgrp->self)

 Return @cgrp's css (cgroup_subsys_state) associated with @ss.  This
 function must be called either under cgroup_mutex or rcu_read_lock() and
 the caller is responsible for pinning the returned css if it wants to
 keep accessing it outside the said locks.  This function may return
 %NULL if @cgrp doesn't have @subsys_id enabled.
 /*
static struct cgroup_subsys_statecgroup_css(struct cgroupcgrp,
					      struct cgroup_subsysss)
{
	if (ss)
		return rcu_dereference_check(cgrp->subsys[ss->id],
					lockdep_is_held(&cgroup_mutex));
	else
		return &cgrp->self;
}

*/
 cgroup_e_css - obtain a cgroup's effective css for the specified subsystem
 @cgrp: the cgroup of interest
 @ss: the subsystem of interest (%NULL returns @cgrp->self)

 Similar to cgroup_css() but returns the effective css, which is defined
 as the matching css of the nearest ancestor including self which has @ss
 enabled.  If @ss is associated with the hierarchy @cgrp is on, this
 function is guaranteed to return non-NULL css.
 /*
static struct cgroup_subsys_statecgroup_e_css(struct cgroupcgrp,
						struct cgroup_subsysss)
{
	lockdep_assert_held(&cgroup_mutex);

	if (!ss)
		return &cgrp->self;

	*/
	 This function is used while updating css associations and thus
	 can't test the csses directly.  Test ss_mask.
	 /*
	while (!(cgroup_ss_mask(cgrp) & (1 << ss->id))) {
		cgrp = cgroup_parent(cgrp);
		if (!cgrp)
			return NULL;
	}

	return cgroup_css(cgrp, ss);
}

*/
 cgroup_get_e_css - get a cgroup's effective css for the specified subsystem
 @cgrp: the cgroup of interest
 @ss: the subsystem of interest

 Find and get the effective css of @cgrp for @ss.  The effective css is
 defined as the matching css of the nearest ancestor including self which
 has @ss enabled.  If @ss is not mounted on the hierarchy @cgrp is on,
 the root css is returned, so this function always returns a valid css.
 The returned css must be put using css_put().
 /*
struct cgroup_subsys_statecgroup_get_e_css(struct cgroupcgrp,
					     struct cgroup_subsysss)
{
	struct cgroup_subsys_statecss;

	rcu_read_lock();

	do {
		css = cgroup_css(cgrp, ss);

		if (css && css_tryget_online(css))
			goto out_unlock;
		cgrp = cgroup_parent(cgrp);
	} while (cgrp);

	css = init_css_set.subsys[ss->id];
	css_get(css);
out_unlock:
	rcu_read_unlock();
	return css;
}

*/ convenient tests for these bits /*
static inline bool cgroup_is_dead(const struct cgroupcgrp)
{
	return !(cgrp->self.flags & CSS_ONLINE);
}

static void cgroup_get(struct cgroupcgrp)
{
	WARN_ON_ONCE(cgroup_is_dead(cgrp));
	css_get(&cgrp->self);
}

static bool cgroup_tryget(struct cgroupcgrp)
{
	return css_tryget(&cgrp->self);
}

struct cgroup_subsys_stateof_css(struct kernfs_open_fileof)
{
	struct cgroupcgrp = of->kn->parent->priv;
	struct cftypecft = of_cft(of);

	*/
	 This is open and unprotected implementation of cgroup_css().
	 seq_css() is only called from a kernfs file operation which has
	 an active reference on the file.  Because all the subsystem
	 files are drained before a css is disassociated with a cgroup,
	 the matching css from the cgroup's subsys table is guaranteed to
	 be and stay valid until the enclosing operation is complete.
	 /*
	if (cft->ss)
		return rcu_dereference_raw(cgrp->subsys[cft->ss->id]);
	else
		return &cgrp->self;
}
EXPORT_SYMBOL_GPL(of_css);

static int notify_on_release(const struct cgroupcgrp)
{
	return test_bit(CGRP_NOTIFY_ON_RELEASE, &cgrp->flags);
}

*/
 for_each_css - iterate all css's of a cgroup
 @css: the iteration cursor
 @ssid: the index of the subsystem, CGROUP_SUBSYS_COUNT after reaching the end
 @cgrp: the target cgroup to iterate css's of

 Should be called under cgroup_[tree_]mutex.
 /*
#define for_each_css(css, ssid, cgrp)					\
	for ((ssid) = 0; (ssid) < CGROUP_SUBSYS_COUNT; (ssid)++)	\
		if (!((css) = rcu_dereference_check(			\
				(cgrp)->subsys[(ssid)],			\
				lockdep_is_held(&cgroup_mutex)))) { }	\
		else

*/
 for_each_e_css - iterate all effective css's of a cgroup
 @css: the iteration cursor
 @ssid: the index of the subsystem, CGROUP_SUBSYS_COUNT after reaching the end
 @cgrp: the target cgroup to iterate css's of

 Should be called under cgroup_[tree_]mutex.
 /*
#define for_each_e_css(css, ssid, cgrp)					\
	for ((ssid) = 0; (ssid) < CGROUP_SUBSYS_COUNT; (ssid)++)	\
		if (!((css) = cgroup_e_css(cgrp, cgroup_subsys[(ssid)]))) \
			;						\
		else

*/
 for_each_subsys - iterate all enabled cgroup subsystems
 @ss: the iteration cursor
 @ssid: the index of @ss, CGROUP_SUBSYS_COUNT after reaching the end
 /*
#define for_each_subsys(ss, ssid)					\
	for ((ssid) = 0; (ssid) < CGROUP_SUBSYS_COUNT &&		\
	     (((ss) = cgroup_subsys[ssid]) || true); (ssid)++)

*/
 do_each_subsys_mask - filter for_each_subsys with a bitmask
 @ss: the iteration cursor
 @ssid: the index of @ss, CGROUP_SUBSYS_COUNT after reaching the end
 @ss_mask: the bitmask

 The block will only run for cases where the ssid-th bit (1 << ssid) of
 @ss_mask is set.
 /*
#define do_each_subsys_mask(ss, ssid, ss_mask) do {			\
	unsigned long __ss_mask = (ss_mask);				\
	if (!CGROUP_SUBSYS_COUNT) {/ to avoid spurious gcc warning /*	\
		(ssid) = 0;						\
		break;							\
	}								\
	for_each_set_bit(ssid, &__ss_mask, CGROUP_SUBSYS_COUNT) {	\
		(ss) = cgroup_subsys[ssid];				\
		{

#define while_each_subsys_mask()					\
		}							\
	}								\
} while (false)

*/ iterate across the hierarchies /*
#define for_each_root(root)						\
	list_for_each_entry((root), &cgroup_roots, root_list)

*/ iterate over child cgrps, lock should be held throughout iteration /*
#define cgroup_for_each_live_child(child, cgrp)				\
	list_for_each_entry((child), &(cgrp)->self.children, self.sibling) \
		if (({ lockdep_assert_held(&cgroup_mutex);		\
		       cgroup_is_dead(child); }))			\
			;						\
		else

*/ walk live descendants in preorder /*
#define cgroup_for_each_live_descendant_pre(dsct, d_css, cgrp)		\
	css_for_each_descendant_pre((d_css), cgroup_css((cgrp), NULL))	\
		if (({ lockdep_assert_held(&cgroup_mutex);		\
		       (dsct) = (d_css)->cgroup;			\
		       cgroup_is_dead(dsct); }))			\
			;						\
		else

*/ walk live descendants in postorder /*
#define cgroup_for_each_live_descendant_post(dsct, d_css, cgrp)		\
	css_for_each_descendant_post((d_css), cgroup_css((cgrp), NULL))	\
		if (({ lockdep_assert_held(&cgroup_mutex);		\
		       (dsct) = (d_css)->cgroup;			\
		       cgroup_is_dead(dsct); }))			\
			;						\
		else

static void cgroup_release_agent(struct work_structwork);
static void check_for_release(struct cgroupcgrp);

*/
 A cgroup can be associated with multiple css_sets as different tasks may
 belong to different cgroups on different hierarchies.  In the other
 direction, a css_set is naturally associated with multiple cgroups.
 This M:N relationship is represented by the following link structure
 which exists for each association and allows traversing the associations
 from both sides.
 /*
struct cgrp_cset_link {
	*/ the cgroup and css_set this link associates /*
	struct cgroup		*cgrp;
	struct css_set		*cset;

	*/ list of cgrp_cset_links anchored at cgrp->cset_links /*
	struct list_head	cset_link;

	*/ list of cgrp_cset_links anchored at css_set->cgrp_links /*
	struct list_head	cgrp_link;
};

*/
 The default css_set - used by init and its children prior to any
 hierarchies being mounted. It contains a pointer to the root state
 for each subsystem. Also used to anchor the list of css_sets. Not
 reference-counted, to improve performance when child cgroups
 haven't been created.
 /*
struct css_set init_css_set = {
	.refcount		= ATOMIC_INIT(1),
	.cgrp_links		= LIST_HEAD_INIT(init_css_set.cgrp_links),
	.tasks			= LIST_HEAD_INIT(init_css_set.tasks),
	.mg_tasks		= LIST_HEAD_INIT(init_css_set.mg_tasks),
	.mg_preload_node	= LIST_HEAD_INIT(init_css_set.mg_preload_node),
	.mg_node		= LIST_HEAD_INIT(init_css_set.mg_node),
	.task_iters		= LIST_HEAD_INIT(init_css_set.task_iters),
};

static int css_set_count	= 1;	*/ 1 for init_css_set /*

*/
 css_set_populated - does a css_set contain any tasks?
 @cset: target css_set
 /*
static bool css_set_populated(struct css_setcset)
{
	lockdep_assert_held(&css_set_lock);

	return !list_empty(&cset->tasks) || !list_empty(&cset->mg_tasks);
}

*/
 cgroup_update_populated - updated populated count of a cgroup
 @cgrp: the target cgroup
 @populated: inc or dec populated count

 One of the css_sets associated with @cgrp is either getting its first
 task or losing the last.  Update @cgrp->populated_cnt accordingly.  The
 count is propagated towards root so that a given cgroup's populated_cnt
 is zero iff the cgroup and all its descendants don't contain any tasks.

 @cgrp's interface file "cgroup.populated" is zero if
 @cgrp->populated_cnt is zero and 1 otherwise.  When @cgrp->populated_cnt
 changes from or to zero, userland is notified that the content of the
 interface file has changed.  This can be used to detect when @cgrp and
 its descendants become populated or empty.
 /*
static void cgroup_update_populated(struct cgroupcgrp, bool populated)
{
	lockdep_assert_held(&css_set_lock);

	do {
		bool trigger;

		if (populated)
			trigger = !cgrp->populated_cnt++;
		else
			trigger = !--cgrp->populated_cnt;

		if (!trigger)
			break;

		check_for_release(cgrp);
		cgroup_file_notify(&cgrp->events_file);

		cgrp = cgroup_parent(cgrp);
	} while (cgrp);
}

*/
 css_set_update_populated - update populated state of a css_set
 @cset: target css_set
 @populated: whether @cset is populated or depopulated

 @cset is either getting the first task or losing the last.  Update the
 ->populated_cnt of all associated cgroups accordingly.
 /*
static void css_set_update_populated(struct css_setcset, bool populated)
{
	struct cgrp_cset_linklink;

	lockdep_assert_held(&css_set_lock);

	list_for_each_entry(link, &cset->cgrp_links, cgrp_link)
		cgroup_update_populated(link->cgrp, populated);
}

*/
 css_set_move_task - move a task from one css_set to another
 @task: task being moved
 @from_cset: css_set @task currently belongs to (may be NULL)
 @to_cset: new css_set @task is being moved to (may be NULL)
 @use_mg_tasks: move to @to_cset->mg_tasks instead of ->tasks

 Move @task from @from_cset to @to_cset.  If @task didn't belong to any
 css_set, @from_cset can be NULL.  If @task is being disassociated
 instead of moved, @to_cset can be NULL.

 This function automatically handles populated_cnt updates and
 css_task_iter adjustments but the caller is responsible for managing
 @from_cset and @to_cset's reference counts.
 /*
static void css_set_move_task(struct task_structtask,
			      struct css_setfrom_cset, struct css_setto_cset,
			      bool use_mg_tasks)
{
	lockdep_assert_held(&css_set_lock);

	if (to_cset && !css_set_populated(to_cset))
		css_set_update_populated(to_cset, true);

	if (from_cset) {
		struct css_task_iterit,pos;

		WARN_ON_ONCE(list_empty(&task->cg_list));

		*/
		 @task is leaving, advance task iterators which are
		 pointing to it so that they can resume at the next
		 position.  Advancing an iterator might remove it from
		 the list, use safe walk.  See css_task_iter_advance*()
		 for details.
		 /*
		list_for_each_entry_safe(it, pos, &from_cset->task_iters,
					 iters_node)
			if (it->task_pos == &task->cg_list)
				css_task_iter_advance(it);

		list_del_init(&task->cg_list);
		if (!css_set_populated(from_cset))
			css_set_update_populated(from_cset, false);
	} else {
		WARN_ON_ONCE(!list_empty(&task->cg_list));
	}

	if (to_cset) {
		*/
		 We are synchronized through cgroup_threadgroup_rwsem
		 against PF_EXITING setting such that we can't race
		 against cgroup_exit() changing the css_set to
		 init_css_set and dropping the old one.
		 /*
		WARN_ON_ONCE(task->flags & PF_EXITING);

		rcu_assign_pointer(task->cgroups, to_cset);
		list_add_tail(&task->cg_list, use_mg_tasks ? &to_cset->mg_tasks :
							     &to_cset->tasks);
	}
}

*/
 hash table for cgroup groups. This improves the performance to find
 an existing css_set. This hash doesn't (currently) take into
 account cgroups in empty hierarchies.
 /*
#define CSS_SET_HASH_BITS	7
static DEFINE_HASHTABLE(css_set_table, CSS_SET_HASH_BITS);

static unsigned long css_set_hash(struct cgroup_subsys_statecss[])
{
	unsigned long key = 0UL;
	struct cgroup_subsysss;
	int i;

	for_each_subsys(ss, i)
		key += (unsigned long)css[i];
	key = (key >> 16) ^ key;

	return key;
}

static void put_css_set_locked(struct css_setcset)
{
	struct cgrp_cset_linklink,tmp_link;
	struct cgroup_subsysss;
	int ssid;

	lockdep_assert_held(&css_set_lock);

	if (!atomic_dec_and_test(&cset->refcount))
		return;

	*/ This css_set is dead. unlink it and release cgroup and css refs /*
	for_each_subsys(ss, ssid) {
		list_del(&cset->e_cset_node[ssid]);
		css_put(cset->subsys[ssid]);
	}
	hash_del(&cset->hlist);
	css_set_count--;

	list_for_each_entry_safe(link, tmp_link, &cset->cgrp_links, cgrp_link) {
		list_del(&link->cset_link);
		list_del(&link->cgrp_link);
		if (cgroup_parent(link->cgrp))
			cgroup_put(link->cgrp);
		kfree(link);
	}

	kfree_rcu(cset, rcu_head);
}

static void put_css_set(struct css_setcset)
{
	*/
	 Ensure that the refcount doesn't hit zero while any readers
	 can see it. Similar to atomic_dec_and_lock(), but for an
	 rwlock
	 /*
	if (atomic_add_unless(&cset->refcount, -1, 1))
		return;

	spin_lock_bh(&css_set_lock);
	put_css_set_locked(cset);
	spin_unlock_bh(&css_set_lock);
}

*/
 refcounted get/put for css_set objects
 /*
static inline void get_css_set(struct css_setcset)
{
	atomic_inc(&cset->refcount);
}

*/
 compare_css_sets - helper function for find_existing_css_set().
 @cset: candidate css_set being tested
 @old_cset: existing css_set for a task
 @new_cgrp: cgroup that's being entered by the task
 @template: desired set of css pointers in css_set (pre-calculated)

 Returns true if "cset" matches "old_cset" except for the hierarchy
 which "new_cgrp" belongs to, for which it should match "new_cgrp".
 /*
static bool compare_css_sets(struct css_setcset,
			     struct css_setold_cset,
			     struct cgroupnew_cgrp,
			     struct cgroup_subsys_statetemplate[])
{
	struct list_headl1,l2;

	*/
	 On the default hierarchy, there can be csets which are
	 associated with the same set of cgroups but different csses.
	 Let's first ensure that csses match.
	 /*
	if (memcmp(template, cset->subsys, sizeof(cset->subsys)))
		return false;

	*/
	 Compare cgroup pointers in order to distinguish between
	 different cgroups in hierarchies.  As different cgroups may
	 share the same effective css, this comparison is always
	 necessary.
	 /*
	l1 = &cset->cgrp_links;
	l2 = &old_cset->cgrp_links;
	while (1) {
		struct cgrp_cset_linklink1,link2;
		struct cgroupcgrp1,cgrp2;

		l1 = l1->next;
		l2 = l2->next;
		*/ See if we reached the end - both lists are equal length. /*
		if (l1 == &cset->cgrp_links) {
			BUG_ON(l2 != &old_cset->cgrp_links);
			break;
		} else {
			BUG_ON(l2 == &old_cset->cgrp_links);
		}
		*/ Locate the cgroups associated with these links. /*
		link1 = list_entry(l1, struct cgrp_cset_link, cgrp_link);
		link2 = list_entry(l2, struct cgrp_cset_link, cgrp_link);
		cgrp1 = link1->cgrp;
		cgrp2 = link2->cgrp;
		*/ Hierarchies should be linked in the same order. /*
		BUG_ON(cgrp1->root != cgrp2->root);

		*/
		 If this hierarchy is the hierarchy of the cgroup
		 that's changing, then we need to check that this
		 css_set points to the new cgroup; if it's any other
		 hierarchy, then this css_set should point to the
		 same cgroup as the old css_set.
		 /*
		if (cgrp1->root == new_cgrp->root) {
			if (cgrp1 != new_cgrp)
				return false;
		} else {
			if (cgrp1 != cgrp2)
				return false;
		}
	}
	return true;
}

*/
 find_existing_css_set - init css array and find the matching css_set
 @old_cset: the css_set that we're using before the cgroup transition
 @cgrp: the cgroup that we're moving into
 @template: out param for the new set of csses, should be clear on entry
 /*
static struct css_setfind_existing_css_set(struct css_setold_cset,
					struct cgroupcgrp,
					struct cgroup_subsys_statetemplate[])
{
	struct cgroup_rootroot = cgrp->root;
	struct cgroup_subsysss;
	struct css_setcset;
	unsigned long key;
	int i;

	*/
	 Build the set of subsystem state objects that we want to see in the
	 new css_set. while subsystems can change globally, the entries here
	 won't change, so no need for locking.
	 /*
	for_each_subsys(ss, i) {
		if (root->subsys_mask & (1UL << i)) {
			*/
			 @ss is in this hierarchy, so we want the
			 effective css from @cgrp.
			 /*
			template[i] = cgroup_e_css(cgrp, ss);
		} else {
			*/
			 @ss is not in this hierarchy, so we don't want
			 to change the css.
			 /*
			template[i] = old_cset->subsys[i];
		}
	}

	key = css_set_hash(template);
	hash_for_each_possible(css_set_table, cset, hlist, key) {
		if (!compare_css_sets(cset, old_cset, cgrp, template))
			continue;

		*/ This css_set matches what we need /*
		return cset;
	}

	*/ No existing cgroup group matched /*
	return NULL;
}

static void free_cgrp_cset_links(struct list_headlinks_to_free)
{
	struct cgrp_cset_linklink,tmp_link;

	list_for_each_entry_safe(link, tmp_link, links_to_free, cset_link) {
		list_del(&link->cset_link);
		kfree(link);
	}
}

*/
 allocate_cgrp_cset_links - allocate cgrp_cset_links
 @count: the number of links to allocate
 @tmp_links: list_head the allocated links are put on

 Allocate @count cgrp_cset_link structures and chain them on @tmp_links
 through ->cset_link.  Returns 0 on success or -errno.
 /*
static int allocate_cgrp_cset_links(int count, struct list_headtmp_links)
{
	struct cgrp_cset_linklink;
	int i;

	INIT_LIST_HEAD(tmp_links);

	for (i = 0; i < count; i++) {
		link = kzalloc(sizeof(*link), GFP_KERNEL);
		if (!link) {
			free_cgrp_cset_links(tmp_links);
			return -ENOMEM;
		}
		list_add(&link->cset_link, tmp_links);
	}
	return 0;
}

*/
 link_css_set - a helper function to link a css_set to a cgroup
 @tmp_links: cgrp_cset_link objects allocated by allocate_cgrp_cset_links()
 @cset: the css_set to be linked
 @cgrp: the destination cgroup
 /*
static void link_css_set(struct list_headtmp_links, struct css_setcset,
			 struct cgroupcgrp)
{
	struct cgrp_cset_linklink;

	BUG_ON(list_empty(tmp_links));

	if (cgroup_on_dfl(cgrp))
		cset->dfl_cgrp = cgrp;

	link = list_first_entry(tmp_links, struct cgrp_cset_link, cset_link);
	link->cset = cset;
	link->cgrp = cgrp;

	*/
	 Always add links to the tail of the lists so that the lists are
	 in choronological order.
	 /*
	list_move_tail(&link->cset_link, &cgrp->cset_links);
	list_add_tail(&link->cgrp_link, &cset->cgrp_links);

	if (cgroup_parent(cgrp))
		cgroup_get(cgrp);
}

*/
 find_css_set - return a new css_set with one cgroup updated
 @old_cset: the baseline css_set
 @cgrp: the cgroup to be updated

 Return a new css_set that's equivalent to @old_cset, but with @cgrp
 substituted into the appropriate hierarchy.
 /*
static struct css_setfind_css_set(struct css_setold_cset,
				    struct cgroupcgrp)
{
	struct cgroup_subsys_statetemplate[CGROUP_SUBSYS_COUNT] = { };
	struct css_setcset;
	struct list_head tmp_links;
	struct cgrp_cset_linklink;
	struct cgroup_subsysss;
	unsigned long key;
	int ssid;

	lockdep_assert_held(&cgroup_mutex);

	*/ First see if we already have a cgroup group that matches
	 the desired set /*
	spin_lock_bh(&css_set_lock);
	cset = find_existing_css_set(old_cset, cgrp, template);
	if (cset)
		get_css_set(cset);
	spin_unlock_bh(&css_set_lock);

	if (cset)
		return cset;

	cset = kzalloc(sizeof(*cset), GFP_KERNEL);
	if (!cset)
		return NULL;

	*/ Allocate all the cgrp_cset_link objects that we'll need /*
	if (allocate_cgrp_cset_links(cgroup_root_count, &tmp_links) < 0) {
		kfree(cset);
		return NULL;
	}

	atomic_set(&cset->refcount, 1);
	INIT_LIST_HEAD(&cset->cgrp_links);
	INIT_LIST_HEAD(&cset->tasks);
	INIT_LIST_HEAD(&cset->mg_tasks);
	INIT_LIST_HEAD(&cset->mg_preload_node);
	INIT_LIST_HEAD(&cset->mg_node);
	INIT_LIST_HEAD(&cset->task_iters);
	INIT_HLIST_NODE(&cset->hlist);

	*/ Copy the set of subsystem state objects generated in
	 find_existing_css_set() /*
	memcpy(cset->subsys, template, sizeof(cset->subsys));

	spin_lock_bh(&css_set_lock);
	*/ Add reference counts and links from the new css_set. /*
	list_for_each_entry(link, &old_cset->cgrp_links, cgrp_link) {
		struct cgroupc = link->cgrp;

		if (c->root == cgrp->root)
			c = cgrp;
		link_css_set(&tmp_links, cset, c);
	}

	BUG_ON(!list_empty(&tmp_links));

	css_set_count++;

	*/ Add @cset to the hash table /*
	key = css_set_hash(cset->subsys);
	hash_add(css_set_table, &cset->hlist, key);

	for_each_subsys(ss, ssid) {
		struct cgroup_subsys_statecss = cset->subsys[ssid];

		list_add_tail(&cset->e_cset_node[ssid],
			      &css->cgroup->e_csets[ssid]);
		css_get(css);
	}

	spin_unlock_bh(&css_set_lock);

	return cset;
}

static struct cgroup_rootcgroup_root_from_kf(struct kernfs_rootkf_root)
{
	struct cgrouproot_cgrp = kf_root->kn->priv;

	return root_cgrp->root;
}

static int cgroup_init_root_id(struct cgroup_rootroot)
{
	int id;

	lockdep_assert_held(&cgroup_mutex);

	id = idr_alloc_cyclic(&cgroup_hierarchy_idr, root, 0, 0, GFP_KERNEL);
	if (id < 0)
		return id;

	root->hierarchy_id = id;
	return 0;
}

static void cgroup_exit_root_id(struct cgroup_rootroot)
{
	lockdep_assert_held(&cgroup_mutex);

	if (root->hierarchy_id) {
		idr_remove(&cgroup_hierarchy_idr, root->hierarchy_id);
		root->hierarchy_id = 0;
	}
}

static void cgroup_free_root(struct cgroup_rootroot)
{
	if (root) {
		*/ hierarchy ID should already have been released /*
		WARN_ON_ONCE(root->hierarchy_id);

		idr_destroy(&root->cgroup_idr);
		kfree(root);
	}
}

static void cgroup_destroy_root(struct cgroup_rootroot)
{
	struct cgroupcgrp = &root->cgrp;
	struct cgrp_cset_linklink,tmp_link;

	cgroup_lock_and_drain_offline(&cgrp_dfl_root.cgrp);

	BUG_ON(atomic_read(&root->nr_cgrps));
	BUG_ON(!list_empty(&cgrp->self.children));

	*/ Rebind all subsystems back to the default hierarchy /*
	WARN_ON(rebind_subsystems(&cgrp_dfl_root, root->subsys_mask));

	*/
	 Release all the links from cset_links to this hierarchy's
	 root cgroup
	 /*
	spin_lock_bh(&css_set_lock);

	list_for_each_entry_safe(link, tmp_link, &cgrp->cset_links, cset_link) {
		list_del(&link->cset_link);
		list_del(&link->cgrp_link);
		kfree(link);
	}

	spin_unlock_bh(&css_set_lock);

	if (!list_empty(&root->root_list)) {
		list_del(&root->root_list);
		cgroup_root_count--;
	}

	cgroup_exit_root_id(root);

	mutex_unlock(&cgroup_mutex);

	kernfs_destroy_root(root->kf_root);
	cgroup_free_root(root);
}

*/ look up cgroup associated with given css_set on the specified hierarchy /*
static struct cgroupcset_cgroup_from_root(struct css_setcset,
					    struct cgroup_rootroot)
{
	struct cgroupres = NULL;

	lockdep_assert_held(&cgroup_mutex);
	lockdep_assert_held(&css_set_lock);

	if (cset == &init_css_set) {
		res = &root->cgrp;
	} else {
		struct cgrp_cset_linklink;

		list_for_each_entry(link, &cset->cgrp_links, cgrp_link) {
			struct cgroupc = link->cgrp;

			if (c->root == root) {
				res = c;
				break;
			}
		}
	}

	BUG_ON(!res);
	return res;
}

*/
 Return the cgroup for "task" from the given hierarchy. Must be
 called with cgroup_mutex and css_set_lock held.
 /*
static struct cgrouptask_cgroup_from_root(struct task_structtask,
					    struct cgroup_rootroot)
{
	*/
	 No need to lock the task - since we hold cgroup_mutex the
	 task can't change groups, so the only thing that can happen
	 is that it exits and its css is set back to init_css_set.
	 /*
	return cset_cgroup_from_root(task_css_set(task), root);
}

*/
 A task must hold cgroup_mutex to modify cgroups.

 Any task can increment and decrement the count field without lock.
 So in general, code holding cgroup_mutex can't rely on the count
 field not changing.  However, if the count goes to zero, then only
 cgroup_attach_task() can increment it again.  Because a count of zero
 means that no tasks are currently attached, therefore there is no
 way a task attached to that cgroup can fork (the other way to
 increment the count).  So code holding cgroup_mutex can safely
 assume that if the count is zero, it will stay zero. Similarly, if
 a task holds cgroup_mutex on a cgroup with zero count, it
 knows that the cgroup won't be removed, as cgroup_rmdir()
 needs that mutex.

 A cgroup can only be deleted if both its 'count' of using tasks
 is zero, and its list of 'children' cgroups is empty.  Since all
 tasks in the system use _some_ cgroup, and since there is always at
 least one task in the system (init, pid == 1), therefore, root cgroup
 always has either children cgroups and/or using tasks.  So we don't
 need a special hack to ensure that root cgroup cannot be deleted.

 P.S.  One more locking exception.  RCU is used to guard the
 update of a tasks cgroup pointer by cgroup_attach_task()
 /*

static struct kernfs_syscall_ops cgroup_kf_syscall_ops;
static const struct file_operations proc_cgroupstats_operations;

static charcgroup_file_name(struct cgroupcgrp, const struct cftypecft,
			      charbuf)
{
	struct cgroup_subsysss = cft->ss;

	if (cft->ss && !(cft->flags & CFTYPE_NO_PREFIX) &&
	    !(cgrp->root->flags & CGRP_ROOT_NOPREFIX))
		snprintf(buf, CGROUP_FILE_NAME_MAX, "%s.%s",
			 cgroup_on_dfl(cgrp) ? ss->name : ss->legacy_name,
			 cft->name);
	else
		strncpy(buf, cft->name, CGROUP_FILE_NAME_MAX);
	return buf;
}

*/
 cgroup_file_mode - deduce file mode of a control file
 @cft: the control file in question

 S_IRUGO for read, S_IWUSR for write.
 /*
static umode_t cgroup_file_mode(const struct cftypecft)
{
	umode_t mode = 0;

	if (cft->read_u64 || cft->read_s64 || cft->seq_show)
		mode |= S_IRUGO;

	if (cft->write_u64 || cft->write_s64 || cft->write) {
		if (cft->flags & CFTYPE_WORLD_WRITABLE)
			mode |= S_IWUGO;
		else
			mode |= S_IWUSR;
	}

	return mode;
}

*/
 cgroup_calc_subtree_ss_mask - calculate subtree_ss_mask
 @subtree_control: the new subtree_control mask to consider
 @this_ss_mask: available subsystems

 On the default hierarchy, a subsystem may request other subsystems to be
 enabled together through its ->depends_on mask.  In such cases, more
 subsystems than specified in "cgroup.subtree_control" may be enabled.

 This function calculates which subsystems need to be enabled if
 @subtree_control is to be applied while restricted to @this_ss_mask.
 /*
static u16 cgroup_calc_subtree_ss_mask(u16 subtree_control, u16 this_ss_mask)
{
	u16 cur_ss_mask = subtree_control;
	struct cgroup_subsysss;
	int ssid;

	lockdep_assert_held(&cgroup_mutex);

	cur_ss_mask |= cgrp_dfl_implicit_ss_mask;

	while (true) {
		u16 new_ss_mask = cur_ss_mask;

		do_each_subsys_mask(ss, ssid, cur_ss_mask) {
			new_ss_mask |= ss->depends_on;
		} while_each_subsys_mask();

		*/
		 Mask out subsystems which aren't available.  This can
		 happen only if some depended-upon subsystems were bound
		 to non-default hierarchies.
		 /*
		new_ss_mask &= this_ss_mask;

		if (new_ss_mask == cur_ss_mask)
			break;
		cur_ss_mask = new_ss_mask;
	}

	return cur_ss_mask;
}

*/
 cgroup_kn_unlock - unlocking helper for cgroup kernfs methods
 @kn: the kernfs_node being serviced

 This helper undoes cgroup_kn_lock_live() and should be invoked before
 the method finishes if locking succeeded.  Note that once this function
 returns the cgroup returned by cgroup_kn_lock_live() may become
 inaccessible any time.  If the caller intends to continue to access the
 cgroup, it should pin it before invoking this function.
 /*
static void cgroup_kn_unlock(struct kernfs_nodekn)
{
	struct cgroupcgrp;

	if (kernfs_type(kn) == KERNFS_DIR)
		cgrp = kn->priv;
	else
		cgrp = kn->parent->priv;

	mutex_unlock(&cgroup_mutex);

	kernfs_unbreak_active_protection(kn);
	cgroup_put(cgrp);
}

*/
 cgroup_kn_lock_live - locking helper for cgroup kernfs methods
 @kn: the kernfs_node being serviced
 @drain_offline: perform offline draining on the cgroup

 This helper is to be used by a cgroup kernfs method currently servicing
 @kn.  It breaks the active protection, performs cgroup locking and
 verifies that the associated cgroup is alive.  Returns the cgroup if
 alive; otherwise, %NULL.  A successful return should be undone by a
 matching cgroup_kn_unlock() invocation.  If @drain_offline is %true, the
 cgroup is drained of offlining csses before return.

 Any cgroup kernfs method implementation which requires locking the
 associated cgroup should use this helper.  It avoids nesting cgroup
 locking under kernfs active protection and allows all kernfs operations
 including self-removal.
 /*
static struct cgroupcgroup_kn_lock_live(struct kernfs_nodekn,
					  bool drain_offline)
{
	struct cgroupcgrp;

	if (kernfs_type(kn) == KERNFS_DIR)
		cgrp = kn->priv;
	else
		cgrp = kn->parent->priv;

	*/
	 We're gonna grab cgroup_mutex which nests outside kernfs
	 active_ref.  cgroup liveliness check alone provides enough
	 protection against removal.  Ensure @cgrp stays accessible and
	 break the active_ref protection.
	 /*
	if (!cgroup_tryget(cgrp))
		return NULL;
	kernfs_break_active_protection(kn);

	if (drain_offline)
		cgroup_lock_and_drain_offline(cgrp);
	else
		mutex_lock(&cgroup_mutex);

	if (!cgroup_is_dead(cgrp))
		return cgrp;

	cgroup_kn_unlock(kn);
	return NULL;
}

static void cgroup_rm_file(struct cgroupcgrp, const struct cftypecft)
{
	char name[CGROUP_FILE_NAME_MAX];

	lockdep_assert_held(&cgroup_mutex);

	if (cft->file_offset) {
		struct cgroup_subsys_statecss = cgroup_css(cgrp, cft->ss);
		struct cgroup_filecfile = (void)css + cft->file_offset;

		spin_lock_irq(&cgroup_file_kn_lock);
		cfile->kn = NULL;
		spin_unlock_irq(&cgroup_file_kn_lock);
	}

	kernfs_remove_by_name(cgrp->kn, cgroup_file_name(cgrp, cft, name));
}

*/
 css_clear_dir - remove subsys files in a cgroup directory
 @css: taget css
 /*
static void css_clear_dir(struct cgroup_subsys_statecss)
{
	struct cgroupcgrp = css->cgroup;
	struct cftypecfts;

	if (!(css->flags & CSS_VISIBLE))
		return;

	css->flags &= ~CSS_VISIBLE;

	list_for_each_entry(cfts, &css->ss->cfts, node)
		cgroup_addrm_files(css, cgrp, cfts, false);
}

*/
 css_populate_dir - create subsys files in a cgroup directory
 @css: target css

 On failure, no file is added.
 /*
static int css_populate_dir(struct cgroup_subsys_statecss)
{
	struct cgroupcgrp = css->cgroup;
	struct cftypecfts,failed_cfts;
	int ret;

	if ((css->flags & CSS_VISIBLE) || !cgrp->kn)
		return 0;

	if (!css->ss) {
		if (cgroup_on_dfl(cgrp))
			cfts = cgroup_dfl_base_files;
		else
			cfts = cgroup_legacy_base_files;

		return cgroup_addrm_files(&cgrp->self, cgrp, cfts, true);
	}

	list_for_each_entry(cfts, &css->ss->cfts, node) {
		ret = cgroup_addrm_files(css, cgrp, cfts, true);
		if (ret < 0) {
			failed_cfts = cfts;
			goto err;
		}
	}

	css->flags |= CSS_VISIBLE;

	return 0;
err:
	list_for_each_entry(cfts, &css->ss->cfts, node) {
		if (cfts == failed_cfts)
			break;
		cgroup_addrm_files(css, cgrp, cfts, false);
	}
	return ret;
}

static int rebind_subsystems(struct cgroup_rootdst_root, u16 ss_mask)
{
	struct cgroupdcgrp = &dst_root->cgrp;
	struct cgroup_subsysss;
	int ssid, i, ret;

	lockdep_assert_held(&cgroup_mutex);

	do_each_subsys_mask(ss, ssid, ss_mask) {
		*/
		 If @ss has non-root csses attached to it, can't move.
		 If @ss is an implicit controller, it is exempt from this
		 rule and can be stolen.
		 /*
		if (css_next_child(NULL, cgroup_css(&ss->root->cgrp, ss)) &&
		    !ss->implicit_on_dfl)
			return -EBUSY;

		*/ can't move between two non-dummy roots either /*
		if (ss->root != &cgrp_dfl_root && dst_root != &cgrp_dfl_root)
			return -EBUSY;
	} while_each_subsys_mask();

	do_each_subsys_mask(ss, ssid, ss_mask) {
		struct cgroup_rootsrc_root = ss->root;
		struct cgroupscgrp = &src_root->cgrp;
		struct cgroup_subsys_statecss = cgroup_css(scgrp, ss);
		struct css_setcset;

		WARN_ON(!css || cgroup_css(dcgrp, ss));

		*/ disable from the source /*
		src_root->subsys_mask &= ~(1 << ssid);
		WARN_ON(cgroup_apply_control(scgrp));
		cgroup_finalize_control(scgrp, 0);

		*/ rebind /*
		RCU_INIT_POINTER(scgrp->subsys[ssid], NULL);
		rcu_assign_pointer(dcgrp->subsys[ssid], css);
		ss->root = dst_root;
		css->cgroup = dcgrp;

		spin_lock_bh(&css_set_lock);
		hash_for_each(css_set_table, i, cset, hlist)
			list_move_tail(&cset->e_cset_node[ss->id],
				       &dcgrp->e_csets[ss->id]);
		spin_unlock_bh(&css_set_lock);

		*/ default hierarchy doesn't enable controllers by default /*
		dst_root->subsys_mask |= 1 << ssid;
		if (dst_root == &cgrp_dfl_root) {
			static_branch_enable(cgroup_subsys_on_dfl_key[ssid]);
		} else {
			dcgrp->subtree_control |= 1 << ssid;
			static_branch_disable(cgroup_subsys_on_dfl_key[ssid]);
		}

		ret = cgroup_apply_control(dcgrp);
		if (ret)
			pr_warn("partial failure to rebind %s controller (err=%d)\n",
				ss->name, ret);

		if (ss->bind)
			ss->bind(css);
	} while_each_subsys_mask();

	kernfs_activate(dcgrp->kn);
	return 0;
}

static int cgroup_show_options(struct seq_fileseq,
			       struct kernfs_rootkf_root)
{
	struct cgroup_rootroot = cgroup_root_from_kf(kf_root);
	struct cgroup_subsysss;
	int ssid;

	if (root != &cgrp_dfl_root)
		for_each_subsys(ss, ssid)
			if (root->subsys_mask & (1 << ssid))
				seq_show_option(seq, ss->legacy_name, NULL);
	if (root->flags & CGRP_ROOT_NOPREFIX)
		seq_puts(seq, ",noprefix");
	if (root->flags & CGRP_ROOT_XATTR)
		seq_puts(seq, ",xattr");

	spin_lock(&release_agent_path_lock);
	if (strlen(root->release_agent_path))
		seq_show_option(seq, "release_agent",
				root->release_agent_path);
	spin_unlock(&release_agent_path_lock);

	if (test_bit(CGRP_CPUSET_CLONE_CHILDREN, &root->cgrp.flags))
		seq_puts(seq, ",clone_children");
	if (strlen(root->name))
		seq_show_option(seq, "name", root->name);
	return 0;
}

struct cgroup_sb_opts {
	u16 subsys_mask;
	unsigned int flags;
	charrelease_agent;
	bool cpuset_clone_children;
	charname;
	*/ User explicitly requested empty subsystem /*
	bool none;
};

static int parse_cgroupfs_options(chardata, struct cgroup_sb_optsopts)
{
	chartoken,o = data;
	bool all_ss = false, one_ss = false;
	u16 mask = U16_MAX;
	struct cgroup_subsysss;
	int nr_opts = 0;
	int i;

#ifdef CONFIG_CPUSETS
	mask = ~((u16)1 << cpuset_cgrp_id);
#endif

	memset(opts, 0, sizeof(*opts));

	while ((token = strsep(&o, ",")) != NULL) {
		nr_opts++;

		if (!*token)
			return -EINVAL;
		if (!strcmp(token, "none")) {
			*/ Explicitly have no subsystems /*
			opts->none = true;
			continue;
		}
		if (!strcmp(token, "all")) {
			*/ Mutually exclusive option 'all' + subsystem name /*
			if (one_ss)
				return -EINVAL;
			all_ss = true;
			continue;
		}
		if (!strcmp(token, "noprefix")) {
			opts->flags |= CGRP_ROOT_NOPREFIX;
			continue;
		}
		if (!strcmp(token, "clone_children")) {
			opts->cpuset_clone_children = true;
			continue;
		}
		if (!strcmp(token, "xattr")) {
			opts->flags |= CGRP_ROOT_XATTR;
			continue;
		}
		if (!strncmp(token, "release_agent=", 14)) {
			*/ Specifying two release agents is forbidden /*
			if (opts->release_agent)
				return -EINVAL;
			opts->release_agent =
				kstrndup(token + 14, PATH_MAX - 1, GFP_KERNEL);
			if (!opts->release_agent)
				return -ENOMEM;
			continue;
		}
		if (!strncmp(token, "name=", 5)) {
			const charname = token + 5;
			*/ Can't specify an empty name /*
			if (!strlen(name))
				return -EINVAL;
			*/ Must match [\w.-]+ /*
			for (i = 0; i < strlen(name); i++) {
				char c = name[i];
				if (isalnum(c))
					continue;
				if ((c == '.') || (c == '-') || (c == '_'))
					continue;
				return -EINVAL;
			}
			*/ Specifying two names is forbidden /*
			if (opts->name)
				return -EINVAL;
			opts->name = kstrndup(name,
					      MAX_CGROUP_ROOT_NAMELEN - 1,
					      GFP_KERNEL);
			if (!opts->name)
				return -ENOMEM;

			continue;
		}

		for_each_subsys(ss, i) {
			if (strcmp(token, ss->legacy_name))
				continue;
			if (!cgroup_ssid_enabled(i))
				continue;
			if (cgroup_ssid_no_v1(i))
				continue;

			*/ Mutually exclusive option 'all' + subsystem name /*
			if (all_ss)
				return -EINVAL;
			opts->subsys_mask |= (1 << i);
			one_ss = true;

			break;
		}
		if (i == CGROUP_SUBSYS_COUNT)
			return -ENOENT;
	}

	*/
	 If the 'all' option was specified select all the subsystems,
	 otherwise if 'none', 'name=' and a subsystem name options were
	 not specified, let's default to 'all'
	 /*
	if (all_ss || (!one_ss && !opts->none && !opts->name))
		for_each_subsys(ss, i)
			if (cgroup_ssid_enabled(i) && !cgroup_ssid_no_v1(i))
				opts->subsys_mask |= (1 << i);

	*/
	 We either have to specify by name or by subsystems. (So all
	 empty hierarchies must have a name).
	 /*
	if (!opts->subsys_mask && !opts->name)
		return -EINVAL;

	*/
	 Option noprefix was introduced just for backward compatibility
	 with the old cpuset, so we allow noprefix only if mounting just
	 the cpuset subsystem.
	 /*
	if ((opts->flags & CGRP_ROOT_NOPREFIX) && (opts->subsys_mask & mask))
		return -EINVAL;

	*/ Can't specify "none" and some subsystems /*
	if (opts->subsys_mask && opts->none)
		return -EINVAL;

	return 0;
}

static int cgroup_remount(struct kernfs_rootkf_root, intflags, chardata)
{
	int ret = 0;
	struct cgroup_rootroot = cgroup_root_from_kf(kf_root);
	struct cgroup_sb_opts opts;
	u16 added_mask, removed_mask;

	if (root == &cgrp_dfl_root) {
		pr_err("remount is not allowed\n");
		return -EINVAL;
	}

	cgroup_lock_and_drain_offline(&cgrp_dfl_root.cgrp);

	*/ See what subsystems are wanted /*
	ret = parse_cgroupfs_options(data, &opts);
	if (ret)
		goto out_unlock;

	if (opts.subsys_mask != root->subsys_mask || opts.release_agent)
		pr_warn("option changes via remount are deprecated (pid=%d comm=%s)\n",
			task_tgid_nr(current), current->comm);

	added_mask = opts.subsys_mask & ~root->subsys_mask;
	removed_mask = root->subsys_mask & ~opts.subsys_mask;

	*/ Don't allow flags or name to change at remount /*
	if ((opts.flags ^ root->flags) ||
	    (opts.name && strcmp(opts.name, root->name))) {
		pr_err("option or name mismatch, new: 0x%x \"%s\", old: 0x%x \"%s\"\n",
		       opts.flags, opts.name ?: "", root->flags, root->name);
		ret = -EINVAL;
		goto out_unlock;
	}

	*/ remounting is not allowed for populated hierarchies /*
	if (!list_empty(&root->cgrp.self.children)) {
		ret = -EBUSY;
		goto out_unlock;
	}

	ret = rebind_subsystems(root, added_mask);
	if (ret)
		goto out_unlock;

	WARN_ON(rebind_subsystems(&cgrp_dfl_root, removed_mask));

	if (opts.release_agent) {
		spin_lock(&release_agent_path_lock);
		strcpy(root->release_agent_path, opts.release_agent);
		spin_unlock(&release_agent_path_lock);
	}
 out_unlock:
	kfree(opts.release_agent);
	kfree(opts.name);
	mutex_unlock(&cgroup_mutex);
	return ret;
}

*/
 To reduce the fork() overhead for systems that are not actually using
 their cgroups capability, we don't maintain the lists running through
 each css_set to its tasks until we see the list actually used - in other
 words after the first mount.
 /*
static bool use_task_css_set_links __read_mostly;

static void cgroup_enable_task_cg_lists(void)
{
	struct task_structp,g;

	spin_lock_bh(&css_set_lock);

	if (use_task_css_set_links)
		goto out_unlock;

	use_task_css_set_links = true;

	*/
	 We need tasklist_lock because RCU is not safe against
	 while_each_thread(). Besides, a forking task that has passed
	 cgroup_post_fork() without seeing use_task_css_set_links = 1
	 is not guaranteed to have its child immediately visible in the
	 tasklist if we walk through it with RCU.
	 /*
	read_lock(&tasklist_lock);
	do_each_thread(g, p) {
		WARN_ON_ONCE(!list_empty(&p->cg_list) ||
			     task_css_set(p) != &init_css_set);

		*/
		 We should check if the process is exiting, otherwise
		 it will race with cgroup_exit() in that the list
		 entry won't be deleted though the process has exited.
		 Do it while holding siglock so that we don't end up
		 racing against cgroup_exit().
		 /*
		spin_lock_irq(&p->sighand->siglock);
		if (!(p->flags & PF_EXITING)) {
			struct css_setcset = task_css_set(p);

			if (!css_set_populated(cset))
				css_set_update_populated(cset, true);
			list_add_tail(&p->cg_list, &cset->tasks);
			get_css_set(cset);
		}
		spin_unlock_irq(&p->sighand->siglock);
	} while_each_thread(g, p);
	read_unlock(&tasklist_lock);
out_unlock:
	spin_unlock_bh(&css_set_lock);
}

static void init_cgroup_housekeeping(struct cgroupcgrp)
{
	struct cgroup_subsysss;
	int ssid;

	INIT_LIST_HEAD(&cgrp->self.sibling);
	INIT_LIST_HEAD(&cgrp->self.children);
	INIT_LIST_HEAD(&cgrp->cset_links);
	INIT_LIST_HEAD(&cgrp->pidlists);
	mutex_init(&cgrp->pidlist_mutex);
	cgrp->self.cgroup = cgrp;
	cgrp->self.flags |= CSS_ONLINE;

	for_each_subsys(ss, ssid)
		INIT_LIST_HEAD(&cgrp->e_csets[ssid]);

	init_waitqueue_head(&cgrp->offline_waitq);
	INIT_WORK(&cgrp->release_agent_work, cgroup_release_agent);
}

static void init_cgroup_root(struct cgroup_rootroot,
			     struct cgroup_sb_optsopts)
{
	struct cgroupcgrp = &root->cgrp;

	INIT_LIST_HEAD(&root->root_list);
	atomic_set(&root->nr_cgrps, 1);
	cgrp->root = root;
	init_cgroup_housekeeping(cgrp);
	idr_init(&root->cgroup_idr);

	root->flags = opts->flags;
	if (opts->release_agent)
		strcpy(root->release_agent_path, opts->release_agent);
	if (opts->name)
		strcpy(root->name, opts->name);
	if (opts->cpuset_clone_children)
		set_bit(CGRP_CPUSET_CLONE_CHILDREN, &root->cgrp.flags);
}

static int cgroup_setup_root(struct cgroup_rootroot, u16 ss_mask)
{
	LIST_HEAD(tmp_links);
	struct cgrouproot_cgrp = &root->cgrp;
	struct css_setcset;
	int i, ret;

	lockdep_assert_held(&cgroup_mutex);

	ret = cgroup_idr_alloc(&root->cgroup_idr, root_cgrp, 1, 2, GFP_KERNEL);
	if (ret < 0)
		goto out;
	root_cgrp->id = ret;
	root_cgrp->ancestor_ids[0] = ret;

	ret = percpu_ref_init(&root_cgrp->self.refcnt, css_release, 0,
			      GFP_KERNEL);
	if (ret)
		goto out;

	*/
	 We're accessing css_set_count without locking css_set_lock here,
	 but that's OK - it can only be increased by someone holding
	 cgroup_lock, and that's us.  Later rebinding may disable
	 controllers on the default hierarchy and thus create new csets,
	 which can't be more than the existing ones.  Allocate 2x.
	 /*
	ret = allocate_cgrp_cset_links(2 css_set_count, &tmp_links);
	if (ret)
		goto cancel_ref;

	ret = cgroup_init_root_id(root);
	if (ret)
		goto cancel_ref;

	root->kf_root = kernfs_create_root(&cgroup_kf_syscall_ops,
					   KERNFS_ROOT_CREATE_DEACTIVATED,
					   root_cgrp);
	if (IS_ERR(root->kf_root)) {
		ret = PTR_ERR(root->kf_root);
		goto exit_root_id;
	}
	root_cgrp->kn = root->kf_root->kn;

	ret = css_populate_dir(&root_cgrp->self);
	if (ret)
		goto destroy_root;

	ret = rebind_subsystems(root, ss_mask);
	if (ret)
		goto destroy_root;

	*/
	 There must be no failure case after here, since rebinding takes
	 care of subsystems' refcounts, which are explicitly dropped in
	 the failure exit path.
	 /*
	list_add(&root->root_list, &cgroup_roots);
	cgroup_root_count++;

	*/
	 Link the root cgroup in this hierarchy into all the css_set
	 objects.
	 /*
	spin_lock_bh(&css_set_lock);
	hash_for_each(css_set_table, i, cset, hlist) {
		link_css_set(&tmp_links, cset, root_cgrp);
		if (css_set_populated(cset))
			cgroup_update_populated(root_cgrp, true);
	}
	spin_unlock_bh(&css_set_lock);

	BUG_ON(!list_empty(&root_cgrp->self.children));
	BUG_ON(atomic_read(&root->nr_cgrps) != 1);

	kernfs_activate(root_cgrp->kn);
	ret = 0;
	goto out;

destroy_root:
	kernfs_destroy_root(root->kf_root);
	root->kf_root = NULL;
exit_root_id:
	cgroup_exit_root_id(root);
cancel_ref:
	percpu_ref_exit(&root_cgrp->self.refcnt);
out:
	free_cgrp_cset_links(&tmp_links);
	return ret;
}

static struct dentrycgroup_mount(struct file_system_typefs_type,
			 int flags, const charunused_dev_name,
			 voiddata)
{
	bool is_v2 = fs_type == &cgroup2_fs_type;
	struct super_blockpinned_sb = NULL;
	struct cgroup_namespacens = current->nsproxy->cgroup_ns;
	struct cgroup_subsysss;
	struct cgroup_rootroot;
	struct cgroup_sb_opts opts;
	struct dentrydentry;
	int ret;
	int i;
	bool new_sb;

	get_cgroup_ns(ns);

	*/ Check if the caller has permission to mount. /*
	if (!ns_capable(ns->user_ns, CAP_SYS_ADMIN)) {
		put_cgroup_ns(ns);
		return ERR_PTR(-EPERM);
	}

	*/
	 The first time anyone tries to mount a cgroup, enable the list
	 linking each css_set to its tasks and fix up all existing tasks.
	 /*
	if (!use_task_css_set_links)
		cgroup_enable_task_cg_lists();

	if (is_v2) {
		if (data) {
			pr_err("cgroup2: unknown option \"%s\"\n", (char)data);
			put_cgroup_ns(ns);
			return ERR_PTR(-EINVAL);
		}
		cgrp_dfl_visible = true;
		root = &cgrp_dfl_root;
		cgroup_get(&root->cgrp);
		goto out_mount;
	}

	cgroup_lock_and_drain_offline(&cgrp_dfl_root.cgrp);

	*/ First find the desired set of subsystems /*
	ret = parse_cgroupfs_options(data, &opts);
	if (ret)
		goto out_unlock;

	*/
	 Destruction of cgroup root is asynchronous, so subsystems may
	 still be dying after the previous unmount.  Let's drain the
	 dying subsystems.  We just need to ensure that the ones
	 unmounted previously finish dying and don't care about new ones
	 starting.  Testing ref liveliness is good enough.
	 /*
	for_each_subsys(ss, i) {
		if (!(opts.subsys_mask & (1 << i)) ||
		    ss->root == &cgrp_dfl_root)
			continue;

		if (!percpu_ref_tryget_live(&ss->root->cgrp.self.refcnt)) {
			mutex_unlock(&cgroup_mutex);
			msleep(10);
			ret = restart_syscall();
			goto out_free;
		}
		cgroup_put(&ss->root->cgrp);
	}

	for_each_root(root) {
		bool name_match = false;

		if (root == &cgrp_dfl_root)
			continue;

		*/
		 If we asked for a name then it must match.  Also, if
		 name matches but sybsys_mask doesn't, we should fail.
		 Remember whether name matched.
		 /*
		if (opts.name) {
			if (strcmp(opts.name, root->name))
				continue;
			name_match = true;
		}

		*/
		 If we asked for subsystems (or explicitly for no
		 subsystems) then they must match.
		 /*
		if ((opts.subsys_mask || opts.none) &&
		    (opts.subsys_mask != root->subsys_mask)) {
			if (!name_match)
				continue;
			ret = -EBUSY;
			goto out_unlock;
		}

		if (root->flags ^ opts.flags)
			pr_warn("new mount options do not match the existing superblock, will be ignored\n");

		*/
		 We want to reuse @root whose lifetime is governed by its
		 ->cgrp.  Let's check whether @root is alive and keep it
		 that way.  As cgroup_kill_sb() can happen anytime, we
		 want to block it by pinning the sb so that @root doesn't
		 get killed before mount is complete.
		
		 With the sb pinned, tryget_live can reliably indicate
		 whether @root can be reused.  If it's being killed,
		 drain it.  We can use wait_queue for the wait but this
		 path is super cold.  Let's just sleep a bit and retry.
		 /*
		pinned_sb = kernfs_pin_sb(root->kf_root, NULL);
		if (IS_ERR(pinned_sb) ||
		    !percpu_ref_tryget_live(&root->cgrp.self.refcnt)) {
			mutex_unlock(&cgroup_mutex);
			if (!IS_ERR_OR_NULL(pinned_sb))
				deactivate_super(pinned_sb);
			msleep(10);
			ret = restart_syscall();
			goto out_free;
		}

		ret = 0;
		goto out_unlock;
	}

	*/
	 No such thing, create a new one.  name= matching without subsys
	 specification is allowed for already existing hierarchies but we
	 can't create new one without subsys specification.
	 /*
	if (!opts.subsys_mask && !opts.none) {
		ret = -EINVAL;
		goto out_unlock;
	}

	*/
	 We know this subsystem has not yet been bound.  Users in a non-init
	 user namespace may only mount hierarchies with no bound subsystems,
	 i.e. 'none,name=user1'
	 /*
	if (!opts.none && !capable(CAP_SYS_ADMIN)) {
		ret = -EPERM;
		goto out_unlock;
	}

	root = kzalloc(sizeof(*root), GFP_KERNEL);
	if (!root) {
		ret = -ENOMEM;
		goto out_unlock;
	}

	init_cgroup_root(root, &opts);

	ret = cgroup_setup_root(root, opts.subsys_mask);
	if (ret)
		cgroup_free_root(root);

out_unlock:
	mutex_unlock(&cgroup_mutex);
out_free:
	kfree(opts.release_agent);
	kfree(opts.name);

	if (ret) {
		put_cgroup_ns(ns);
		return ERR_PTR(ret);
	}
out_mount:
	dentry = kernfs_mount(fs_type, flags, root->kf_root,
			      is_v2 ? CGROUP2_SUPER_MAGIC : CGROUP_SUPER_MAGIC,
			      &new_sb);

	*/
	 In non-init cgroup namespace, instead of root cgroup's
	 dentry, we return the dentry corresponding to the
	 cgroupns->root_cgrp.
	 /*
	if (!IS_ERR(dentry) && ns != &init_cgroup_ns) {
		struct dentrynsdentry;
		struct cgroupcgrp;

		mutex_lock(&cgroup_mutex);
		spin_lock_bh(&css_set_lock);

		cgrp = cset_cgroup_from_root(ns->root_cset, root);

		spin_unlock_bh(&css_set_lock);
		mutex_unlock(&cgroup_mutex);

		nsdentry = kernfs_node_dentry(cgrp->kn, dentry->d_sb);
		dput(dentry);
		dentry = nsdentry;
	}

	if (IS_ERR(dentry) || !new_sb)
		cgroup_put(&root->cgrp);

	*/
	 If @pinned_sb, we're reusing an existing root and holding an
	 extra ref on its sb.  Mount is complete.  Put the extra ref.
	 /*
	if (pinned_sb) {
		WARN_ON(new_sb);
		deactivate_super(pinned_sb);
	}

	put_cgroup_ns(ns);
	return dentry;
}

static void cgroup_kill_sb(struct super_blocksb)
{
	struct kernfs_rootkf_root = kernfs_root_from_sb(sb);
	struct cgroup_rootroot = cgroup_root_from_kf(kf_root);

	*/
	 If @root doesn't have any mounts or children, start killing it.
	 This prevents new mounts by disabling percpu_ref_tryget_live().
	 cgroup_mount() may wait for @root's release.
	
	 And don't kill the default root.
	 /*
	if (!list_empty(&root->cgrp.self.children) ||
	    root == &cgrp_dfl_root)
		cgroup_put(&root->cgrp);
	else
		percpu_ref_kill(&root->cgrp.self.refcnt);

	kernfs_kill_sb(sb);
}

static struct file_system_type cgroup_fs_type = {
	.name = "cgroup",
	.mount = cgroup_mount,
	.kill_sb = cgroup_kill_sb,
	.fs_flags = FS_USERNS_MOUNT,
};

static struct file_system_type cgroup2_fs_type = {
	.name = "cgroup2",
	.mount = cgroup_mount,
	.kill_sb = cgroup_kill_sb,
	.fs_flags = FS_USERNS_MOUNT,
};

static charcgroup_path_ns_locked(struct cgroupcgrp, charbuf, size_t buflen,
				   struct cgroup_namespacens)
{
	struct cgrouproot = cset_cgroup_from_root(ns->root_cset, cgrp->root);
	int ret;

	ret = kernfs_path_from_node(cgrp->kn, root->kn, buf, buflen);
	if (ret < 0 || ret >= buflen)
		return NULL;
	return buf;
}

charcgroup_path_ns(struct cgroupcgrp, charbuf, size_t buflen,
		     struct cgroup_namespacens)
{
	charret;

	mutex_lock(&cgroup_mutex);
	spin_lock_bh(&css_set_lock);

	ret = cgroup_path_ns_locked(cgrp, buf, buflen, ns);

	spin_unlock_bh(&css_set_lock);
	mutex_unlock(&cgroup_mutex);

	return ret;
}
EXPORT_SYMBOL_GPL(cgroup_path_ns);

*/
 task_cgroup_path - cgroup path of a task in the first cgroup hierarchy
 @task: target task
 @buf: the buffer to write the path into
 @buflen: the length of the buffer

 Determine @task's cgroup on the first (the one with the lowest non-zero
 hierarchy_id) cgroup hierarchy and copy its path into @buf.  This
 function grabs cgroup_mutex and shouldn't be used inside locks used by
 cgroup controller callbacks.

 Return value is the same as kernfs_path().
 /*
chartask_cgroup_path(struct task_structtask, charbuf, size_t buflen)
{
	struct cgroup_rootroot;
	struct cgroupcgrp;
	int hierarchy_id = 1;
	charpath = NULL;

	mutex_lock(&cgroup_mutex);
	spin_lock_bh(&css_set_lock);

	root = idr_get_next(&cgroup_hierarchy_idr, &hierarchy_id);

	if (root) {
		cgrp = task_cgroup_from_root(task, root);
		path = cgroup_path_ns_locked(cgrp, buf, buflen, &init_cgroup_ns);
	} else {
		*/ if no hierarchy exists, everyone is in "/" /*
		if (strlcpy(buf, "/", buflen) < buflen)
			path = buf;
	}

	spin_unlock_bh(&css_set_lock);
	mutex_unlock(&cgroup_mutex);
	return path;
}
EXPORT_SYMBOL_GPL(task_cgroup_path);

*/ used to track tasks and other necessary states during migration /*
struct cgroup_taskset {
	*/ the src and dst cset list running through cset->mg_node /*
	struct list_head	src_csets;
	struct list_head	dst_csets;

	*/ the subsys currently being processed /*
	int			ssid;

	*/
	 Fields for cgroup_taskset_*() iteration.
	
	 Before migration is committed, the target migration tasks are on
	 ->mg_tasks of the csets on ->src_csets.  After, on ->mg_tasks of
	 the csets on ->dst_csets.  ->csets point to either ->src_csets
	 or ->dst_csets depending on whether migration is committed.
	
	 ->cur_csets and ->cur_task point to the current task position
	 during iteration.
	 /*
	struct list_head	*csets;
	struct css_set		*cur_cset;
	struct task_struct	*cur_task;
};

#define CGROUP_TASKSET_INIT(tset)	(struct cgroup_taskset){	\
	.src_csets		= LIST_HEAD_INIT(tset.src_csets),	\
	.dst_csets		= LIST_HEAD_INIT(tset.dst_csets),	\
	.csets			= &tset.src_csets,			\
}

*/
 cgroup_taskset_add - try to add a migration target task to a taskset
 @task: target task
 @tset: target taskset

 Add @task, which is a migration target, to @tset.  This function becomes
 noop if @task doesn't need to be migrated.  @task's css_set should have
 been added as a migration source and @task->cg_list will be moved from
 the css_set's tasks list to mg_tasks one.
 /*
static void cgroup_taskset_add(struct task_structtask,
			       struct cgroup_tasksettset)
{
	struct css_setcset;

	lockdep_assert_held(&css_set_lock);

	*/ @task either already exited or can't exit until the end /*
	if (task->flags & PF_EXITING)
		return;

	*/ leave @task alone if post_fork() hasn't linked it yet /*
	if (list_empty(&task->cg_list))
		return;

	cset = task_css_set(task);
	if (!cset->mg_src_cgrp)
		return;

	list_move_tail(&task->cg_list, &cset->mg_tasks);
	if (list_empty(&cset->mg_node))
		list_add_tail(&cset->mg_node, &tset->src_csets);
	if (list_empty(&cset->mg_dst_cset->mg_node))
		list_move_tail(&cset->mg_dst_cset->mg_node,
			       &tset->dst_csets);
}

*/
 cgroup_taskset_first - reset taskset and return the first task
 @tset: taskset of interest
 @dst_cssp: output variable for the destination css

 @tset iteration is initialized and the first task is returned.
 /*
struct task_structcgroup_taskset_first(struct cgroup_tasksettset,
					 struct cgroup_subsys_state*dst_cssp)
{
	tset->cur_cset = list_first_entry(tset->csets, struct css_set, mg_node);
	tset->cur_task = NULL;

	return cgroup_taskset_next(tset, dst_cssp);
}

*/
 cgroup_taskset_next - iterate to the next task in taskset
 @tset: taskset of interest
 @dst_cssp: output variable for the destination css

 Return the next task in @tset.  Iteration must have been initialized
 with cgroup_taskset_first().
 /*
struct task_structcgroup_taskset_next(struct cgroup_tasksettset,
					struct cgroup_subsys_state*dst_cssp)
{
	struct css_setcset = tset->cur_cset;
	struct task_structtask = tset->cur_task;

	while (&cset->mg_node != tset->csets) {
		if (!task)
			task = list_first_entry(&cset->mg_tasks,
						struct task_struct, cg_list);
		else
			task = list_next_entry(task, cg_list);

		if (&task->cg_list != &cset->mg_tasks) {
			tset->cur_cset = cset;
			tset->cur_task = task;

			*/
			 This function may be called both before and
			 after cgroup_taskset_migrate().  The two cases
			 can be distinguished by looking at whether @cset
			 has its ->mg_dst_cset set.
			 /*
			if (cset->mg_dst_cset)
				*dst_cssp = cset->mg_dst_cset->subsys[tset->ssid];
			else
				*dst_cssp = cset->subsys[tset->ssid];

			return task;
		}

		cset = list_next_entry(cset, mg_node);
		task = NULL;
	}

	return NULL;
}

*/
 cgroup_taskset_migrate - migrate a taskset
 @tset: taget taskset
 @root: cgroup root the migration is taking place on

 Migrate tasks in @tset as setup by migration preparation functions.
 This function fails iff one of the ->can_attach callbacks fails and
 guarantees that either all or none of the tasks in @tset are migrated.
 @tset is consumed regardless of success.
 /*
static int cgroup_taskset_migrate(struct cgroup_tasksettset,
				  struct cgroup_rootroot)
{
	struct cgroup_subsysss;
	struct task_structtask,tmp_task;
	struct css_setcset,tmp_cset;
	int ssid, failed_ssid, ret;

	*/ methods shouldn't be called if no task is actually migrating /*
	if (list_empty(&tset->src_csets))
		return 0;

	*/ check that we can legitimately attach to the cgroup /*
	do_each_subsys_mask(ss, ssid, root->subsys_mask) {
		if (ss->can_attach) {
			tset->ssid = ssid;
			ret = ss->can_attach(tset);
			if (ret) {
				failed_ssid = ssid;
				goto out_cancel_attach;
			}
		}
	} while_each_subsys_mask();

	*/
	 Now that we're guaranteed success, proceed to move all tasks to
	 the new cgroup.  There are no failure cases after here, so this
	 is the commit point.
	 /*
	spin_lock_bh(&css_set_lock);
	list_for_each_entry(cset, &tset->src_csets, mg_node) {
		list_for_each_entry_safe(task, tmp_task, &cset->mg_tasks, cg_list) {
			struct css_setfrom_cset = task_css_set(task);
			struct css_setto_cset = cset->mg_dst_cset;

			get_css_set(to_cset);
			css_set_move_task(task, from_cset, to_cset, true);
			put_css_set_locked(from_cset);
		}
	}
	spin_unlock_bh(&css_set_lock);

	*/
	 Migration is committed, all target tasks are now on dst_csets.
	 Nothing is sensitive to fork() after this point.  Notify
	 controllers that migration is complete.
	 /*
	tset->csets = &tset->dst_csets;

	do_each_subsys_mask(ss, ssid, root->subsys_mask) {
		if (ss->attach) {
			tset->ssid = ssid;
			ss->attach(tset);
		}
	} while_each_subsys_mask();

	ret = 0;
	goto out_release_tset;

out_cancel_attach:
	do_each_subsys_mask(ss, ssid, root->subsys_mask) {
		if (ssid == failed_ssid)
			break;
		if (ss->cancel_attach) {
			tset->ssid = ssid;
			ss->cancel_attach(tset);
		}
	} while_each_subsys_mask();
out_release_tset:
	spin_lock_bh(&css_set_lock);
	list_splice_init(&tset->dst_csets, &tset->src_csets);
	list_for_each_entry_safe(cset, tmp_cset, &tset->src_csets, mg_node) {
		list_splice_tail_init(&cset->mg_tasks, &cset->tasks);
		list_del_init(&cset->mg_node);
	}
	spin_unlock_bh(&css_set_lock);
	return ret;
}

*/
 cgroup_may_migrate_to - verify whether a cgroup can be migration destination
 @dst_cgrp: destination cgroup to test

 On the default hierarchy, except for the root, subtree_control must be
 zero for migration destination cgroups with tasks so that child cgroups
 don't compete against tasks.
 /*
static bool cgroup_may_migrate_to(struct cgroupdst_cgrp)
{
	return !cgroup_on_dfl(dst_cgrp) || !cgroup_parent(dst_cgrp) ||
		!dst_cgrp->subtree_control;
}

*/
 cgroup_migrate_finish - cleanup after attach
 @preloaded_csets: list of preloaded css_sets

 Undo cgroup_migrate_add_src() and cgroup_migrate_prepare_dst().  See
 those functions for details.
 /*
static void cgroup_migrate_finish(struct list_headpreloaded_csets)
{
	struct css_setcset,tmp_cset;

	lockdep_assert_held(&cgroup_mutex);

	spin_lock_bh(&css_set_lock);
	list_for_each_entry_safe(cset, tmp_cset, preloaded_csets, mg_preload_node) {
		cset->mg_src_cgrp = NULL;
		cset->mg_dst_cgrp = NULL;
		cset->mg_dst_cset = NULL;
		list_del_init(&cset->mg_preload_node);
		put_css_set_locked(cset);
	}
	spin_unlock_bh(&css_set_lock);
}

*/
 cgroup_migrate_add_src - add a migration source css_set
 @src_cset: the source css_set to add
 @dst_cgrp: the destination cgroup
 @preloaded_csets: list of preloaded css_sets

 Tasks belonging to @src_cset are about to be migrated to @dst_cgrp.  Pin
 @src_cset and add it to @preloaded_csets, which should later be cleaned
 up by cgroup_migrate_finish().

 This function may be called without holding cgroup_threadgroup_rwsem
 even if the target is a process.  Threads may be created and destroyed
 but as long as cgroup_mutex is not dropped, no new css_set can be put
 into play and the preloaded css_sets are guaranteed to cover all
 migrations.
 /*
static void cgroup_migrate_add_src(struct css_setsrc_cset,
				   struct cgroupdst_cgrp,
				   struct list_headpreloaded_csets)
{
	struct cgroupsrc_cgrp;

	lockdep_assert_held(&cgroup_mutex);
	lockdep_assert_held(&css_set_lock);

	*/
	 If ->dead, @src_set is associated with one or more dead cgroups
	 and doesn't contain any migratable tasks.  Ignore it early so
	 that the rest of migration path doesn't get confused by it.
	 /*
	if (src_cset->dead)
		return;

	src_cgrp = cset_cgroup_from_root(src_cset, dst_cgrp->root);

	if (!list_empty(&src_cset->mg_preload_node))
		return;

	WARN_ON(src_cset->mg_src_cgrp);
	WARN_ON(src_cset->mg_dst_cgrp);
	WARN_ON(!list_empty(&src_cset->mg_tasks));
	WARN_ON(!list_empty(&src_cset->mg_node));

	src_cset->mg_src_cgrp = src_cgrp;
	src_cset->mg_dst_cgrp = dst_cgrp;
	get_css_set(src_cset);
	list_add(&src_cset->mg_preload_node, preloaded_csets);
}

*/
 cgroup_migrate_prepare_dst - prepare destination css_sets for migration
 @preloaded_csets: list of preloaded source css_sets

 Tasks are about to be moved and all the source css_sets have been
 preloaded to @preloaded_csets.  This function looks up and pins all
 destination css_sets, links each to its source, and append them to
 @preloaded_csets.

 This function must be called after cgroup_migrate_add_src() has been
 called on each migration source css_set.  After migration is performed
 using cgroup_migrate(), cgroup_migrate_finish() must be called on
 @preloaded_csets.
 /*
static int cgroup_migrate_prepare_dst(struct list_headpreloaded_csets)
{
	LIST_HEAD(csets);
	struct css_setsrc_cset,tmp_cset;

	lockdep_assert_held(&cgroup_mutex);

	*/ look up the dst cset for each src cset and link it to src /*
	list_for_each_entry_safe(src_cset, tmp_cset, preloaded_csets, mg_preload_node) {
		struct css_setdst_cset;

		dst_cset = find_css_set(src_cset, src_cset->mg_dst_cgrp);
		if (!dst_cset)
			goto err;

		WARN_ON_ONCE(src_cset->mg_dst_cset || dst_cset->mg_dst_cset);

		*/
		 If src cset equals dst, it's noop.  Drop the src.
		 cgroup_migrate() will skip the cset too.  Note that we
		 can't handle src == dst as some nodes are used by both.
		 /*
		if (src_cset == dst_cset) {
			src_cset->mg_src_cgrp = NULL;
			src_cset->mg_dst_cgrp = NULL;
			list_del_init(&src_cset->mg_preload_node);
			put_css_set(src_cset);
			put_css_set(dst_cset);
			continue;
		}

		src_cset->mg_dst_cset = dst_cset;

		if (list_empty(&dst_cset->mg_preload_node))
			list_add(&dst_cset->mg_preload_node, &csets);
		else
			put_css_set(dst_cset);
	}

	list_splice_tail(&csets, preloaded_csets);
	return 0;
err:
	cgroup_migrate_finish(&csets);
	return -ENOMEM;
}

*/
 cgroup_migrate - migrate a process or task to a cgroup
 @leader: the leader of the process or the task to migrate
 @threadgroup: whether @leader points to the whole process or a single task
 @root: cgroup root migration is taking place on

 Migrate a process or task denoted by @leader.  If migrating a process,
 the caller must be holding cgroup_threadgroup_rwsem.  The caller is also
 responsible for invoking cgroup_migrate_add_src() and
 cgroup_migrate_prepare_dst() on the targets before invoking this
 function and following up with cgroup_migrate_finish().

 As long as a controller's ->can_attach() doesn't fail, this function is
 guaranteed to succeed.  This means that, excluding ->can_attach()
 failure, when migrating multiple targets, the success or failure can be
 decided for all targets by invoking group_migrate_prepare_dst() before
 actually starting migrating.
 /*
static int cgroup_migrate(struct task_structleader, bool threadgroup,
			  struct cgroup_rootroot)
{
	struct cgroup_taskset tset = CGROUP_TASKSET_INIT(tset);
	struct task_structtask;

	*/
	 Prevent freeing of tasks while we take a snapshot. Tasks that are
	 already PF_EXITING could be freed from underneath us unless we
	 take an rcu_read_lock.
	 /*
	spin_lock_bh(&css_set_lock);
	rcu_read_lock();
	task = leader;
	do {
		cgroup_taskset_add(task, &tset);
		if (!threadgroup)
			break;
	} while_each_thread(leader, task);
	rcu_read_unlock();
	spin_unlock_bh(&css_set_lock);

	return cgroup_taskset_migrate(&tset, root);
}

*/
 cgroup_attach_task - attach a task or a whole threadgroup to a cgroup
 @dst_cgrp: the cgroup to attach to
 @leader: the task or the leader of the threadgroup to be attached
 @threadgroup: attach the whole threadgroup?

 Call holding cgroup_mutex and cgroup_threadgroup_rwsem.
 /*
static int cgroup_attach_task(struct cgroupdst_cgrp,
			      struct task_structleader, bool threadgroup)
{
	LIST_HEAD(preloaded_csets);
	struct task_structtask;
	int ret;

	if (!cgroup_may_migrate_to(dst_cgrp))
		return -EBUSY;

	*/ look up all src csets /*
	spin_lock_bh(&css_set_lock);
	rcu_read_lock();
	task = leader;
	do {
		cgroup_migrate_add_src(task_css_set(task), dst_cgrp,
				       &preloaded_csets);
		if (!threadgroup)
			break;
	} while_each_thread(leader, task);
	rcu_read_unlock();
	spin_unlock_bh(&css_set_lock);

	*/ prepare dst csets and commit /*
	ret = cgroup_migrate_prepare_dst(&preloaded_csets);
	if (!ret)
		ret = cgroup_migrate(leader, threadgroup, dst_cgrp->root);

	cgroup_migrate_finish(&preloaded_csets);
	return ret;
}

static int cgroup_procs_write_permission(struct task_structtask,
					 struct cgroupdst_cgrp,
					 struct kernfs_open_fileof)
{
	const struct credcred = current_cred();
	const struct credtcred = get_task_cred(task);
	int ret = 0;

	*/
	 even if we're attaching all tasks in the thread group, we only
	 need to check permissions on one of them.
	 /*
	if (!uid_eq(cred->euid, GLOBAL_ROOT_UID) &&
	    !uid_eq(cred->euid, tcred->uid) &&
	    !uid_eq(cred->euid, tcred->suid))
		ret = -EACCES;

	if (!ret && cgroup_on_dfl(dst_cgrp)) {
		struct super_blocksb = of->file->f_path.dentry->d_sb;
		struct cgroupcgrp;
		struct inodeinode;

		spin_lock_bh(&css_set_lock);
		cgrp = task_cgroup_from_root(task, &cgrp_dfl_root);
		spin_unlock_bh(&css_set_lock);

		while (!cgroup_is_descendant(dst_cgrp, cgrp))
			cgrp = cgroup_parent(cgrp);

		ret = -ENOMEM;
		inode = kernfs_get_inode(sb, cgrp->procs_file.kn);
		if (inode) {
			ret = inode_permission(inode, MAY_WRITE);
			iput(inode);
		}
	}

	put_cred(tcred);
	return ret;
}

*/
 Find the task_struct of the task to attach by vpid and pass it along to the
 function to attach either it or all tasks in its threadgroup. Will lock
 cgroup_mutex and threadgroup.
 /*
static ssize_t __cgroup_procs_write(struct kernfs_open_fileof, charbuf,
				    size_t nbytes, loff_t off, bool threadgroup)
{
	struct task_structtsk;
	struct cgroup_subsysss;
	struct cgroupcgrp;
	pid_t pid;
	int ssid, ret;

	if (kstrtoint(strstrip(buf), 0, &pid) || pid < 0)
		return -EINVAL;

	cgrp = cgroup_kn_lock_live(of->kn, false);
	if (!cgrp)
		return -ENODEV;

	percpu_down_write(&cgroup_threadgroup_rwsem);
	rcu_read_lock();
	if (pid) {
		tsk = find_task_by_vpid(pid);
		if (!tsk) {
			ret = -ESRCH;
			goto out_unlock_rcu;
		}
	} else {
		tsk = current;
	}

	if (threadgroup)
		tsk = tsk->group_leader;

	*/
	 Workqueue threads may acquire PF_NO_SETAFFINITY and become
	 trapped in a cpuset, or RT worker may be born in a cgroup
	 with no rt_runtime allocated.  Just say no.
	 /*
	if (tsk == kthreadd_task || (tsk->flags & PF_NO_SETAFFINITY)) {
		ret = -EINVAL;
		goto out_unlock_rcu;
	}

	get_task_struct(tsk);
	rcu_read_unlock();

	ret = cgroup_procs_write_permission(tsk, cgrp, of);
	if (!ret)
		ret = cgroup_attach_task(cgrp, tsk, threadgroup);

	put_task_struct(tsk);
	goto out_unlock_threadgroup;

out_unlock_rcu:
	rcu_read_unlock();
out_unlock_threadgroup:
	percpu_up_write(&cgroup_threadgroup_rwsem);
	for_each_subsys(ss, ssid)
		if (ss->post_attach)
			ss->post_attach();
	cgroup_kn_unlock(of->kn);
	return ret ?: nbytes;
}

*/
 cgroup_attach_task_all - attach task 'tsk' to all cgroups of task 'from'
 @from: attach to all cgroups of a given task
 @tsk: the task to be attached
 /*
int cgroup_attach_task_all(struct task_structfrom, struct task_structtsk)
{
	struct cgroup_rootroot;
	int retval = 0;

	mutex_lock(&cgroup_mutex);
	for_each_root(root) {
		struct cgroupfrom_cgrp;

		if (root == &cgrp_dfl_root)
			continue;

		spin_lock_bh(&css_set_lock);
		from_cgrp = task_cgroup_from_root(from, root);
		spin_unlock_bh(&css_set_lock);

		retval = cgroup_attach_task(from_cgrp, tsk, false);
		if (retval)
			break;
	}
	mutex_unlock(&cgroup_mutex);

	return retval;
}
EXPORT_SYMBOL_GPL(cgroup_attach_task_all);

static ssize_t cgroup_tasks_write(struct kernfs_open_fileof,
				  charbuf, size_t nbytes, loff_t off)
{
	return __cgroup_procs_write(of, buf, nbytes, off, false);
}

static ssize_t cgroup_procs_write(struct kernfs_open_fileof,
				  charbuf, size_t nbytes, loff_t off)
{
	return __cgroup_procs_write(of, buf, nbytes, off, true);
}

static ssize_t cgroup_release_agent_write(struct kernfs_open_fileof,
					  charbuf, size_t nbytes, loff_t off)
{
	struct cgroupcgrp;

	BUILD_BUG_ON(sizeof(cgrp->root->release_agent_path) < PATH_MAX);

	cgrp = cgroup_kn_lock_live(of->kn, false);
	if (!cgrp)
		return -ENODEV;
	spin_lock(&release_agent_path_lock);
	strlcpy(cgrp->root->release_agent_path, strstrip(buf),
		sizeof(cgrp->root->release_agent_path));
	spin_unlock(&release_agent_path_lock);
	cgroup_kn_unlock(of->kn);
	return nbytes;
}

static int cgroup_release_agent_show(struct seq_fileseq, voidv)
{
	struct cgroupcgrp = seq_css(seq)->cgroup;

	spin_lock(&release_agent_path_lock);
	seq_puts(seq, cgrp->root->release_agent_path);
	spin_unlock(&release_agent_path_lock);
	seq_putc(seq, '\n');
	return 0;
}

static int cgroup_sane_behavior_show(struct seq_fileseq, voidv)
{
	seq_puts(seq, "0\n");
	return 0;
}

static void cgroup_print_ss_mask(struct seq_fileseq, u16 ss_mask)
{
	struct cgroup_subsysss;
	bool printed = false;
	int ssid;

	do_each_subsys_mask(ss, ssid, ss_mask) {
		if (printed)
			seq_putc(seq, ' ');
		seq_printf(seq, "%s", ss->name);
		printed = true;
	} while_each_subsys_mask();
	if (printed)
		seq_putc(seq, '\n');
}

*/ show controllers which are enabled from the parent /*
static int cgroup_controllers_show(struct seq_fileseq, voidv)
{
	struct cgroupcgrp = seq_css(seq)->cgroup;

	cgroup_print_ss_mask(seq, cgroup_control(cgrp));
	return 0;
}

*/ show controllers which are enabled for a given cgroup's children /*
static int cgroup_subtree_control_show(struct seq_fileseq, voidv)
{
	struct cgroupcgrp = seq_css(seq)->cgroup;

	cgroup_print_ss_mask(seq, cgrp->subtree_control);
	return 0;
}

*/
 cgroup_update_dfl_csses - update css assoc of a subtree in default hierarchy
 @cgrp: root of the subtree to update csses for

 @cgrp's control masks have changed and its subtree's css associations
 need to be updated accordingly.  This function looks up all css_sets
 which are attached to the subtree, creates the matching updated css_sets
 and migrates the tasks to the new ones.
 /*
static int cgroup_update_dfl_csses(struct cgroupcgrp)
{
	LIST_HEAD(preloaded_csets);
	struct cgroup_taskset tset = CGROUP_TASKSET_INIT(tset);
	struct cgroup_subsys_stated_css;
	struct cgroupdsct;
	struct css_setsrc_cset;
	int ret;

	lockdep_assert_held(&cgroup_mutex);

	percpu_down_write(&cgroup_threadgroup_rwsem);

	*/ look up all csses currently attached to @cgrp's subtree /*
	spin_lock_bh(&css_set_lock);
	cgroup_for_each_live_descendant_pre(dsct, d_css, cgrp) {
		struct cgrp_cset_linklink;

		list_for_each_entry(link, &dsct->cset_links, cset_link)
			cgroup_migrate_add_src(link->cset, dsct,
					       &preloaded_csets);
	}
	spin_unlock_bh(&css_set_lock);

	*/ NULL dst indicates self on default hierarchy /*
	ret = cgroup_migrate_prepare_dst(&preloaded_csets);
	if (ret)
		goto out_finish;

	spin_lock_bh(&css_set_lock);
	list_for_each_entry(src_cset, &preloaded_csets, mg_preload_node) {
		struct task_structtask,ntask;

		*/ src_csets precede dst_csets, break on the first dst_cset /*
		if (!src_cset->mg_src_cgrp)
			break;

		*/ all tasks in src_csets need to be migrated /*
		list_for_each_entry_safe(task, ntask, &src_cset->tasks, cg_list)
			cgroup_taskset_add(task, &tset);
	}
	spin_unlock_bh(&css_set_lock);

	ret = cgroup_taskset_migrate(&tset, cgrp->root);
out_finish:
	cgroup_migrate_finish(&preloaded_csets);
	percpu_up_write(&cgroup_threadgroup_rwsem);
	return ret;
}

*/
 cgroup_lock_and_drain_offline - lock cgroup_mutex and drain offlined csses
 @cgrp: root of the target subtree

 Because css offlining is asynchronous, userland may try to re-enable a
 controller while the previous css is still around.  This function grabs
 cgroup_mutex and drains the previous css instances of @cgrp's subtree.
 /*
static void cgroup_lock_and_drain_offline(struct cgroupcgrp)
	__acquires(&cgroup_mutex)
{
	struct cgroupdsct;
	struct cgroup_subsys_stated_css;
	struct cgroup_subsysss;
	int ssid;

restart:
	mutex_lock(&cgroup_mutex);

	cgroup_for_each_live_descendant_post(dsct, d_css, cgrp) {
		for_each_subsys(ss, ssid) {
			struct cgroup_subsys_statecss = cgroup_css(dsct, ss);
			DEFINE_WAIT(wait);

			if (!css || !percpu_ref_is_dying(&css->refcnt))
				continue;

			cgroup_get(dsct);
			prepare_to_wait(&dsct->offline_waitq, &wait,
					TASK_UNINTERRUPTIBLE);

			mutex_unlock(&cgroup_mutex);
			schedule();
			finish_wait(&dsct->offline_waitq, &wait);

			cgroup_put(dsct);
			goto restart;
		}
	}
}

*/
 cgroup_save_control - save control masks of a subtree
 @cgrp: root of the target subtree

 Save ->subtree_control and ->subtree_ss_mask to the respective old_
 prefixed fields for @cgrp's subtree including @cgrp itself.
 /*
static void cgroup_save_control(struct cgroupcgrp)
{
	struct cgroupdsct;
	struct cgroup_subsys_stated_css;

	cgroup_for_each_live_descendant_pre(dsct, d_css, cgrp) {
		dsct->old_subtree_control = dsct->subtree_control;
		dsct->old_subtree_ss_mask = dsct->subtree_ss_mask;
	}
}

*/
 cgroup_propagate_control - refresh control masks of a subtree
 @cgrp: root of the target subtree

 For @cgrp and its subtree, ensure ->subtree_ss_mask matches
 ->subtree_control and propagate controller availability through the
 subtree so that descendants don't have unavailable controllers enabled.
 /*
static void cgroup_propagate_control(struct cgroupcgrp)
{
	struct cgroupdsct;
	struct cgroup_subsys_stated_css;

	cgroup_for_each_live_descendant_pre(dsct, d_css, cgrp) {
		dsct->subtree_control &= cgroup_control(dsct);
		dsct->subtree_ss_mask =
			cgroup_calc_subtree_ss_mask(dsct->subtree_control,
						    cgroup_ss_mask(dsct));
	}
}

*/
 cgroup_restore_control - restore control masks of a subtree
 @cgrp: root of the target subtree

 Restore ->subtree_control and ->subtree_ss_mask from the respective old_
 prefixed fields for @cgrp's subtree including @cgrp itself.
 /*
static void cgroup_restore_control(struct cgroupcgrp)
{
	struct cgroupdsct;
	struct cgroup_subsys_stated_css;

	cgroup_for_each_live_descendant_post(dsct, d_css, cgrp) {
		dsct->subtree_control = dsct->old_subtree_control;
		dsct->subtree_ss_mask = dsct->old_subtree_ss_mask;
	}
}

static bool css_visible(struct cgroup_subsys_statecss)
{
	struct cgroup_subsysss = css->ss;
	struct cgroupcgrp = css->cgroup;

	if (cgroup_control(cgrp) & (1 << ss->id))
		return true;
	if (!(cgroup_ss_mask(cgrp) & (1 << ss->id)))
		return false;
	return cgroup_on_dfl(cgrp) && ss->implicit_on_dfl;
}

*/
 cgroup_apply_control_enable - enable or show csses according to control
 @cgrp: root of the target subtree

 Walk @cgrp's subtree and create new csses or make the existing ones
 visible.  A css is created invisible if it's being implicitly enabled
 through dependency.  An invisible css is made visible when the userland
 explicitly enables it.

 Returns 0 on success, -errno on failure.  On failure, csses which have
 been processed already aren't cleaned up.  The caller is responsible for
 cleaning up with cgroup_apply_control_disble().
 /*
static int cgroup_apply_control_enable(struct cgroupcgrp)
{
	struct cgroupdsct;
	struct cgroup_subsys_stated_css;
	struct cgroup_subsysss;
	int ssid, ret;

	cgroup_for_each_live_descendant_pre(dsct, d_css, cgrp) {
		for_each_subsys(ss, ssid) {
			struct cgroup_subsys_statecss = cgroup_css(dsct, ss);

			WARN_ON_ONCE(css && percpu_ref_is_dying(&css->refcnt));

			if (!(cgroup_ss_mask(dsct) & (1 << ss->id)))
				continue;

			if (!css) {
				css = css_create(dsct, ss);
				if (IS_ERR(css))
					return PTR_ERR(css);
			}

			if (css_visible(css)) {
				ret = css_populate_dir(css);
				if (ret)
					return ret;
			}
		}
	}

	return 0;
}

*/
 cgroup_apply_control_disable - kill or hide csses according to control
 @cgrp: root of the target subtree

 Walk @cgrp's subtree and kill and hide csses so that they match
 cgroup_ss_mask() and cgroup_visible_mask().

 A css is hidden when the userland requests it to be disabled while other
 subsystems are still depending on it.  The css must not actively control
 resources and be in the vanilla state if it's made visible again later.
 Controllers which may be depended upon should provide ->css_reset() for
 this purpose.
 /*
static void cgroup_apply_control_disable(struct cgroupcgrp)
{
	struct cgroupdsct;
	struct cgroup_subsys_stated_css;
	struct cgroup_subsysss;
	int ssid;

	cgroup_for_each_live_descendant_post(dsct, d_css, cgrp) {
		for_each_subsys(ss, ssid) {
			struct cgroup_subsys_statecss = cgroup_css(dsct, ss);

			WARN_ON_ONCE(css && percpu_ref_is_dying(&css->refcnt));

			if (!css)
				continue;

			if (css->parent &&
			    !(cgroup_ss_mask(dsct) & (1 << ss->id))) {
				kill_css(css);
			} else if (!css_visible(css)) {
				css_clear_dir(css);
				if (ss->css_reset)
					ss->css_reset(css);
			}
		}
	}
}

*/
 cgroup_apply_control - apply control mask updates to the subtree
 @cgrp: root of the target subtree

 subsystems can be enabled and disabled in a subtree using the following
 steps.

 1. Call cgroup_save_control() to stash the current state.
 2. Update ->subtree_control masks in the subtree as desired.
 3. Call cgroup_apply_control() to apply the changes.
 4. Optionally perform other related operations.
 5. Call cgroup_finalize_control() to finish up.

 This function implements step 3 and propagates the mask changes
 throughout @cgrp's subtree, updates csses accordingly and perform
 process migrations.
 /*
static int cgroup_apply_control(struct cgroupcgrp)
{
	int ret;

	cgroup_propagate_control(cgrp);

	ret = cgroup_apply_control_enable(cgrp);
	if (ret)
		return ret;

	*/
	 At this point, cgroup_e_css() results reflect the new csses
	 making the following cgroup_update_dfl_csses() properly update
	 css associations of all tasks in the subtree.
	 /*
	ret = cgroup_update_dfl_csses(cgrp);
	if (ret)
		return ret;

	return 0;
}

*/
 cgroup_finalize_control - finalize control mask update
 @cgrp: root of the target subtree
 @ret: the result of the update

 Finalize control mask update.  See cgroup_apply_control() for more info.
 /*
static void cgroup_finalize_control(struct cgroupcgrp, int ret)
{
	if (ret) {
		cgroup_restore_control(cgrp);
		cgroup_propagate_control(cgrp);
	}

	cgroup_apply_control_disable(cgrp);
}

*/ change the enabled child controllers for a cgroup in the default hierarchy /*
static ssize_t cgroup_subtree_control_write(struct kernfs_open_fileof,
					    charbuf, size_t nbytes,
					    loff_t off)
{
	u16 enable = 0, disable = 0;
	struct cgroupcgrp,child;
	struct cgroup_subsysss;
	chartok;
	int ssid, ret;

	*/
	 Parse input - space separated list of subsystem names prefixed
	 with either + or -.
	 /*
	buf = strstrip(buf);
	while ((tok = strsep(&buf, " "))) {
		if (tok[0] == '\0')
			continue;
		do_each_subsys_mask(ss, ssid, ~cgrp_dfl_inhibit_ss_mask) {
			if (!cgroup_ssid_enabled(ssid) ||
			    strcmp(tok + 1, ss->name))
				continue;

			if (*tok == '+') {
				enable |= 1 << ssid;
				disable &= ~(1 << ssid);
			} else if (*tok == '-') {
				disable |= 1 << ssid;
				enable &= ~(1 << ssid);
			} else {
				return -EINVAL;
			}
			break;
		} while_each_subsys_mask();
		if (ssid == CGROUP_SUBSYS_COUNT)
			return -EINVAL;
	}

	cgrp = cgroup_kn_lock_live(of->kn, true);
	if (!cgrp)
		return -ENODEV;

	for_each_subsys(ss, ssid) {
		if (enable & (1 << ssid)) {
			if (cgrp->subtree_control & (1 << ssid)) {
				enable &= ~(1 << ssid);
				continue;
			}

			if (!(cgroup_control(cgrp) & (1 << ssid))) {
				ret = -ENOENT;
				goto out_unlock;
			}
		} else if (disable & (1 << ssid)) {
			if (!(cgrp->subtree_control & (1 << ssid))) {
				disable &= ~(1 << ssid);
				continue;
			}

			*/ a child has it enabled? /*
			cgroup_for_each_live_child(child, cgrp) {
				if (child->subtree_control & (1 << ssid)) {
					ret = -EBUSY;
					goto out_unlock;
				}
			}
		}
	}

	if (!enable && !disable) {
		ret = 0;
		goto out_unlock;
	}

	*/
	 Except for the root, subtree_control must be zero for a cgroup
	 with tasks so that child cgroups don't compete against tasks.
	 /*
	if (enable && cgroup_parent(cgrp) && !list_empty(&cgrp->cset_links)) {
		ret = -EBUSY;
		goto out_unlock;
	}

	*/ save and update control masks and prepare csses /*
	cgroup_save_control(cgrp);

	cgrp->subtree_control |= enable;
	cgrp->subtree_control &= ~disable;

	ret = cgroup_apply_control(cgrp);

	cgroup_finalize_control(cgrp, ret);

	kernfs_activate(cgrp->kn);
	ret = 0;
out_unlock:
	cgroup_kn_unlock(of->kn);
	return ret ?: nbytes;
}

static int cgroup_events_show(struct seq_fileseq, voidv)
{
	seq_printf(seq, "populated %d\n",
		   cgroup_is_populated(seq_css(seq)->cgroup));
	return 0;
}

static ssize_t cgroup_file_write(struct kernfs_open_fileof, charbuf,
				 size_t nbytes, loff_t off)
{
	struct cgroupcgrp = of->kn->parent->priv;
	struct cftypecft = of->kn->priv;
	struct cgroup_subsys_statecss;
	int ret;

	if (cft->write)
		return cft->write(of, buf, nbytes, off);

	*/
	 kernfs guarantees that a file isn't deleted with operations in
	 flight, which means that the matching css is and stays alive and
	 doesn't need to be pinned.  The RCU locking is not necessary
	 either.  It's just for the convenience of using cgroup_css().
	 /*
	rcu_read_lock();
	css = cgroup_css(cgrp, cft->ss);
	rcu_read_unlock();

	if (cft->write_u64) {
		unsigned long long v;
		ret = kstrtoull(buf, 0, &v);
		if (!ret)
			ret = cft->write_u64(css, cft, v);
	} else if (cft->write_s64) {
		long long v;
		ret = kstrtoll(buf, 0, &v);
		if (!ret)
			ret = cft->write_s64(css, cft, v);
	} else {
		ret = -EINVAL;
	}

	return ret ?: nbytes;
}

static voidcgroup_seqfile_start(struct seq_fileseq, loff_tppos)
{
	return seq_cft(seq)->seq_start(seq, ppos);
}

static voidcgroup_seqfile_next(struct seq_fileseq, voidv, loff_tppos)
{
	return seq_cft(seq)->seq_next(seq, v, ppos);
}

static void cgroup_seqfile_stop(struct seq_fileseq, voidv)
{
	seq_cft(seq)->seq_stop(seq, v);
}

static int cgroup_seqfile_show(struct seq_filem, voidarg)
{
	struct cftypecft = seq_cft(m);
	struct cgroup_subsys_statecss = seq_css(m);

	if (cft->seq_show)
		return cft->seq_show(m, arg);

	if (cft->read_u64)
		seq_printf(m, "%llu\n", cft->read_u64(css, cft));
	else if (cft->read_s64)
		seq_printf(m, "%lld\n", cft->read_s64(css, cft));
	else
		return -EINVAL;
	return 0;
}

static struct kernfs_ops cgroup_kf_single_ops = {
	.atomic_write_len	= PAGE_SIZE,
	.write			= cgroup_file_write,
	.seq_show		= cgroup_seqfile_show,
};

static struct kernfs_ops cgroup_kf_ops = {
	.atomic_write_len	= PAGE_SIZE,
	.write			= cgroup_file_write,
	.seq_start		= cgroup_seqfile_start,
	.seq_next		= cgroup_seqfile_next,
	.seq_stop		= cgroup_seqfile_stop,
	.seq_show		= cgroup_seqfile_show,
};

*/
 cgroup_rename - Only allow simple rename of directories in place.
 /*
static int cgroup_rename(struct kernfs_nodekn, struct kernfs_nodenew_parent,
			 const charnew_name_str)
{
	struct cgroupcgrp = kn->priv;
	int ret;

	if (kernfs_type(kn) != KERNFS_DIR)
		return -ENOTDIR;
	if (kn->parent != new_parent)
		return -EIO;

	*/
	 This isn't a proper migration and its usefulness is very
	 limited.  Disallow on the default hierarchy.
	 /*
	if (cgroup_on_dfl(cgrp))
		return -EPERM;

	*/
	 We're gonna grab cgroup_mutex which nests outside kernfs
	 active_ref.  kernfs_rename() doesn't require active_ref
	 protection.  Break them before grabbing cgroup_mutex.
	 /*
	kernfs_break_active_protection(new_parent);
	kernfs_break_active_protection(kn);

	mutex_lock(&cgroup_mutex);

	ret = kernfs_rename(kn, new_parent, new_name_str);

	mutex_unlock(&cgroup_mutex);

	kernfs_unbreak_active_protection(kn);
	kernfs_unbreak_active_protection(new_parent);
	return ret;
}

*/ set uid and gid of cgroup dirs and files to that of the creator /*
static int cgroup_kn_set_ugid(struct kernfs_nodekn)
{
	struct iattr iattr = { .ia_valid = ATTR_UID | ATTR_GID,
			       .ia_uid = current_fsuid(),
			       .ia_gid = current_fsgid(), };

	if (uid_eq(iattr.ia_uid, GLOBAL_ROOT_UID) &&
	    gid_eq(iattr.ia_gid, GLOBAL_ROOT_GID))
		return 0;

	return kernfs_setattr(kn, &iattr);
}

static int cgroup_add_file(struct cgroup_subsys_statecss, struct cgroupcgrp,
			   struct cftypecft)
{
	char name[CGROUP_FILE_NAME_MAX];
	struct kernfs_nodekn;
	struct lock_class_keykey = NULL;
	int ret;

#ifdef CONFIG_DEBUG_LOCK_ALLOC
	key = &cft->lockdep_key;
#endif
	kn = __kernfs_create_file(cgrp->kn, cgroup_file_name(cgrp, cft, name),
				  cgroup_file_mode(cft), 0, cft->kf_ops, cft,
				  NULL, key);
	if (IS_ERR(kn))
		return PTR_ERR(kn);

	ret = cgroup_kn_set_ugid(kn);
	if (ret) {
		kernfs_remove(kn);
		return ret;
	}

	if (cft->file_offset) {
		struct cgroup_filecfile = (void)css + cft->file_offset;

		spin_lock_irq(&cgroup_file_kn_lock);
		cfile->kn = kn;
		spin_unlock_irq(&cgroup_file_kn_lock);
	}

	return 0;
}

*/
 cgroup_addrm_files - add or remove files to a cgroup directory
 @css: the target css
 @cgrp: the target cgroup (usually css->cgroup)
 @cfts: array of cftypes to be added
 @is_add: whether to add or remove

 Depending on @is_add, add or remove files defined by @cfts on @cgrp.
 For removals, this function never fails.
 /*
static int cgroup_addrm_files(struct cgroup_subsys_statecss,
			      struct cgroupcgrp, struct cftype cfts[],
			      bool is_add)
{
	struct cftypecft,cft_end = NULL;
	int ret = 0;

	lockdep_assert_held(&cgroup_mutex);

restart:
	for (cft = cfts; cft != cft_end && cft->name[0] != '\0'; cft++) {
		*/ does cft->flags tell us to skip this file on @cgrp? /*
		if ((cft->flags & __CFTYPE_ONLY_ON_DFL) && !cgroup_on_dfl(cgrp))
			continue;
		if ((cft->flags & __CFTYPE_NOT_ON_DFL) && cgroup_on_dfl(cgrp))
			continue;
		if ((cft->flags & CFTYPE_NOT_ON_ROOT) && !cgroup_parent(cgrp))
			continue;
		if ((cft->flags & CFTYPE_ONLY_ON_ROOT) && cgroup_parent(cgrp))
			continue;

		if (is_add) {
			ret = cgroup_add_file(css, cgrp, cft);
			if (ret) {
				pr_warn("%s: failed to add %s, err=%d\n",
					__func__, cft->name, ret);
				cft_end = cft;
				is_add = false;
				goto restart;
			}
		} else {
			cgroup_rm_file(cgrp, cft);
		}
	}
	return ret;
}

static int cgroup_apply_cftypes(struct cftypecfts, bool is_add)
{
	LIST_HEAD(pending);
	struct cgroup_subsysss = cfts[0].ss;
	struct cgrouproot = &ss->root->cgrp;
	struct cgroup_subsys_statecss;
	int ret = 0;

	lockdep_assert_held(&cgroup_mutex);

	*/ add/rm files for all cgroups created before /*
	css_for_each_descendant_pre(css, cgroup_css(root, ss)) {
		struct cgroupcgrp = css->cgroup;

		if (!(css->flags & CSS_VISIBLE))
			continue;

		ret = cgroup_addrm_files(css, cgrp, cfts, is_add);
		if (ret)
			break;
	}

	if (is_add && !ret)
		kernfs_activate(root->kn);
	return ret;
}

static void cgroup_exit_cftypes(struct cftypecfts)
{
	struct cftypecft;

	for (cft = cfts; cft->name[0] != '\0'; cft++) {
		*/ free copy for custom atomic_write_len, see init_cftypes() /*
		if (cft->max_write_len && cft->max_write_len != PAGE_SIZE)
			kfree(cft->kf_ops);
		cft->kf_ops = NULL;
		cft->ss = NULL;

		*/ revert flags set by cgroup core while adding @cfts /*
		cft->flags &= ~(__CFTYPE_ONLY_ON_DFL | __CFTYPE_NOT_ON_DFL);
	}
}

static int cgroup_init_cftypes(struct cgroup_subsysss, struct cftypecfts)
{
	struct cftypecft;

	for (cft = cfts; cft->name[0] != '\0'; cft++) {
		struct kernfs_opskf_ops;

		WARN_ON(cft->ss || cft->kf_ops);

		if (cft->seq_start)
			kf_ops = &cgroup_kf_ops;
		else
			kf_ops = &cgroup_kf_single_ops;

		*/
		 Ugh... if @cft wants a custom max_write_len, we need to
		 make a copy of kf_ops to set its atomic_write_len.
		 /*
		if (cft->max_write_len && cft->max_write_len != PAGE_SIZE) {
			kf_ops = kmemdup(kf_ops, sizeof(*kf_ops), GFP_KERNEL);
			if (!kf_ops) {
				cgroup_exit_cftypes(cfts);
				return -ENOMEM;
			}
			kf_ops->atomic_write_len = cft->max_write_len;
		}

		cft->kf_ops = kf_ops;
		cft->ss = ss;
	}

	return 0;
}

static int cgroup_rm_cftypes_locked(struct cftypecfts)
{
	lockdep_assert_held(&cgroup_mutex);

	if (!cfts || !cfts[0].ss)
		return -ENOENT;

	list_del(&cfts->node);
	cgroup_apply_cftypes(cfts, false);
	cgroup_exit_cftypes(cfts);
	return 0;
}

*/
 cgroup_rm_cftypes - remove an array of cftypes from a subsystem
 @cfts: zero-length name terminated array of cftypes

 Unregister @cfts.  Files described by @cfts are removed from all
 existing cgroups and all future cgroups won't have them either.  This
 function can be called anytime whether @cfts' subsys is attached or not.

 Returns 0 on successful unregistration, -ENOENT if @cfts is not
 registered.
 /*
int cgroup_rm_cftypes(struct cftypecfts)
{
	int ret;

	mutex_lock(&cgroup_mutex);
	ret = cgroup_rm_cftypes_locked(cfts);
	mutex_unlock(&cgroup_mutex);
	return ret;
}

*/
 cgroup_add_cftypes - add an array of cftypes to a subsystem
 @ss: target cgroup subsystem
 @cfts: zero-length name terminated array of cftypes

 Register @cfts to @ss.  Files described by @cfts are created for all
 existing cgroups to which @ss is attached and all future cgroups will
 have them too.  This function can be called anytime whether @ss is
 attached or not.

 Returns 0 on successful registration, -errno on failure.  Note that this
 function currently returns 0 as long as @cfts registration is successful
 even if some file creation attempts on existing cgroups fail.
 /*
static int cgroup_add_cftypes(struct cgroup_subsysss, struct cftypecfts)
{
	int ret;

	if (!cgroup_ssid_enabled(ss->id))
		return 0;

	if (!cfts || cfts[0].name[0] == '\0')
		return 0;

	ret = cgroup_init_cftypes(ss, cfts);
	if (ret)
		return ret;

	mutex_lock(&cgroup_mutex);

	list_add_tail(&cfts->node, &ss->cfts);
	ret = cgroup_apply_cftypes(cfts, true);
	if (ret)
		cgroup_rm_cftypes_locked(cfts);

	mutex_unlock(&cgroup_mutex);
	return ret;
}

*/
 cgroup_add_dfl_cftypes - add an array of cftypes for default hierarchy
 @ss: target cgroup subsystem
 @cfts: zero-length name terminated array of cftypes

 Similar to cgroup_add_cftypes() but the added files are only used for
 the default hierarchy.
 /*
int cgroup_add_dfl_cftypes(struct cgroup_subsysss, struct cftypecfts)
{
	struct cftypecft;

	for (cft = cfts; cft && cft->name[0] != '\0'; cft++)
		cft->flags |= __CFTYPE_ONLY_ON_DFL;
	return cgroup_add_cftypes(ss, cfts);
}

*/
 cgroup_add_legacy_cftypes - add an array of cftypes for legacy hierarchies
 @ss: target cgroup subsystem
 @cfts: zero-length name terminated array of cftypes

 Similar to cgroup_add_cftypes() but the added files are only used for
 the legacy hierarchies.
 /*
int cgroup_add_legacy_cftypes(struct cgroup_subsysss, struct cftypecfts)
{
	struct cftypecft;

	for (cft = cfts; cft && cft->name[0] != '\0'; cft++)
		cft->flags |= __CFTYPE_NOT_ON_DFL;
	return cgroup_add_cftypes(ss, cfts);
}

*/
 cgroup_file_notify - generate a file modified event for a cgroup_file
 @cfile: target cgroup_file

 @cfile must have been obtained by setting cftype->file_offset.
 /*
void cgroup_file_notify(struct cgroup_filecfile)
{
	unsigned long flags;

	spin_lock_irqsave(&cgroup_file_kn_lock, flags);
	if (cfile->kn)
		kernfs_notify(cfile->kn);
	spin_unlock_irqrestore(&cgroup_file_kn_lock, flags);
}

*/
 cgroup_task_count - count the number of tasks in a cgroup.
 @cgrp: the cgroup in question

 Return the number of tasks in the cgroup.
 /*
static int cgroup_task_count(const struct cgroupcgrp)
{
	int count = 0;
	struct cgrp_cset_linklink;

	spin_lock_bh(&css_set_lock);
	list_for_each_entry(link, &cgrp->cset_links, cset_link)
		count += atomic_read(&link->cset->refcount);
	spin_unlock_bh(&css_set_lock);
	return count;
}

*/
 css_next_child - find the next child of a given css
 @pos: the current position (%NULL to initiate traversal)
 @parent: css whose children to walk

 This function returns the next child of @parent and should be called
 under either cgroup_mutex or RCU read lock.  The only requirement is
 that @parent and @pos are accessible.  The next sibling is guaranteed to
 be returned regardless of their states.

 If a subsystem synchronizes ->css_online() and the start of iteration, a
 css which finished ->css_online() is guaranteed to be visible in the
 future iterations and will stay visible until the last reference is put.
 A css which hasn't finished ->css_online() or already finished
 ->css_offline() may show up during traversal.  It's each subsystem's
 responsibility to synchronize against on/offlining.
 /*
struct cgroup_subsys_statecss_next_child(struct cgroup_subsys_statepos,
					   struct cgroup_subsys_stateparent)
{
	struct cgroup_subsys_statenext;

	cgroup_assert_mutex_or_rcu_locked();

	*/
	 @pos could already have been unlinked from the sibling list.
	 Once a cgroup is removed, its ->sibling.next is no longer
	 updated when its next sibling changes.  CSS_RELEASED is set when
	 @pos is taken off list, at which time its next pointer is valid,
	 and, as releases are serialized, the one pointed to by the next
	 pointer is guaranteed to not have started release yet.  This
	 implies that if we observe !CSS_RELEASED on @pos in this RCU
	 critical section, the one pointed to by its next pointer is
	 guaranteed to not have finished its RCU grace period even if we
	 have dropped rcu_read_lock() inbetween iterations.
	
	 If @pos has CSS_RELEASED set, its next pointer can't be
	 dereferenced; however, as each css is given a monotonically
	 increasing unique serial number and always appended to the
	 sibling list, the next one can be found by walking the parent's
	 children until the first css with higher serial number than
	 @pos's.  While this path can be slower, it happens iff iteration
	 races against release and the race window is very small.
	 /*
	if (!pos) {
		next = list_entry_rcu(parent->children.next, struct cgroup_subsys_state, sibling);
	} else if (likely(!(pos->flags & CSS_RELEASED))) {
		next = list_entry_rcu(pos->sibling.next, struct cgroup_subsys_state, sibling);
	} else {
		list_for_each_entry_rcu(next, &parent->children, sibling)
			if (next->serial_nr > pos->serial_nr)
				break;
	}

	*/
	 @next, if not pointing to the head, can be dereferenced and is
	 the next sibling.
	 /*
	if (&next->sibling != &parent->children)
		return next;
	return NULL;
}

*/
 css_next_descendant_pre - find the next descendant for pre-order walk
 @pos: the current position (%NULL to initiate traversal)
 @root: css whose descendants to walk

 To be used by css_for_each_descendant_pre().  Find the next descendant
 to visit for pre-order traversal of @root's descendants.  @root is
 included in the iteration and the first node to be visited.

 While this function requires cgroup_mutex or RCU read locking, it
 doesn't require the whole traversal to be contained in a single critical
 section.  This function will return the correct next descendant as long
 as both @pos and @root are accessible and @pos is a descendant of @root.

 If a subsystem synchronizes ->css_online() and the start of iteration, a
 css which finished ->css_online() is guaranteed to be visible in the
 future iterations and will stay visible until the last reference is put.
 A css which hasn't finished ->css_online() or already finished
 ->css_offline() may show up during traversal.  It's each subsystem's
 responsibility to synchronize against on/offlining.
 /*
struct cgroup_subsys_state
css_next_descendant_pre(struct cgroup_subsys_statepos,
			struct cgroup_subsys_stateroot)
{
	struct cgroup_subsys_statenext;

	cgroup_assert_mutex_or_rcu_locked();

	*/ if first iteration, visit @root /*
	if (!pos)
		return root;

	*/ visit the first child if exists /*
	next = css_next_child(NULL, pos);
	if (next)
		return next;

	*/ no child, visit my or the closest ancestor's next sibling /*
	while (pos != root) {
		next = css_next_child(pos, pos->parent);
		if (next)
			return next;
		pos = pos->parent;
	}

	return NULL;
}

*/
 css_rightmost_descendant - return the rightmost descendant of a css
 @pos: css of interest

 Return the rightmost descendant of @pos.  If there's no descendant, @pos
 is returned.  This can be used during pre-order traversal to skip
 subtree of @pos.

 While this function requires cgroup_mutex or RCU read locking, it
 doesn't require the whole traversal to be contained in a single critical
 section.  This function will return the correct rightmost descendant as
 long as @pos is accessible.
 /*
struct cgroup_subsys_state
css_rightmost_descendant(struct cgroup_subsys_statepos)
{
	struct cgroup_subsys_statelast,tmp;

	cgroup_assert_mutex_or_rcu_locked();

	do {
		last = pos;
		*/ ->prev isn't RCU safe, walk ->next till the end /*
		pos = NULL;
		css_for_each_child(tmp, last)
			pos = tmp;
	} while (pos);

	return last;
}

static struct cgroup_subsys_state
css_leftmost_descendant(struct cgroup_subsys_statepos)
{
	struct cgroup_subsys_statelast;

	do {
		last = pos;
		pos = css_next_child(NULL, pos);
	} while (pos);

	return last;
}

*/
 css_next_descendant_post - find the next descendant for post-order walk
 @pos: the current position (%NULL to initiate traversal)
 @root: css whose descendants to walk

 To be used by css_for_each_descendant_post().  Find the next descendant
 to visit for post-order traversal of @root's descendants.  @root is
 included in the iteration and the last node to be visited.

 While this function requires cgroup_mutex or RCU read locking, it
 doesn't require the whole traversal to be contained in a single critical
 section.  This function will return the correct next descendant as long
 as both @pos and @cgroup are accessible and @pos is a descendant of
 @cgroup.

 If a subsystem synchronizes ->css_online() and the start of iteration, a
 css which finished ->css_online() is guaranteed to be visible in the
 future iterations and will stay visible until the last reference is put.
 A css which hasn't finished ->css_online() or already finished
 ->css_offline() may show up during traversal.  It's each subsystem's
 responsibility to synchronize against on/offlining.
 /*
struct cgroup_subsys_state
css_next_descendant_post(struct cgroup_subsys_statepos,
			 struct cgroup_subsys_stateroot)
{
	struct cgroup_subsys_statenext;

	cgroup_assert_mutex_or_rcu_locked();

	*/ if first iteration, visit leftmost descendant which may be @root /*
	if (!pos)
		return css_leftmost_descendant(root);

	*/ if we visited @root, we're done /*
	if (pos == root)
		return NULL;

	*/ if there's an unvisited sibling, visit its leftmost descendant /*
	next = css_next_child(pos, pos->parent);
	if (next)
		return css_leftmost_descendant(next);

	*/ no sibling left, visit parent /*
	return pos->parent;
}

*/
 css_has_online_children - does a css have online children
 @css: the target css

 Returns %true if @css has any online children; otherwise, %false.  This
 function can be called from any context but the caller is responsible
 for synchronizing against on/offlining as necessary.
 /*
bool css_has_online_children(struct cgroup_subsys_statecss)
{
	struct cgroup_subsys_statechild;
	bool ret = false;

	rcu_read_lock();
	css_for_each_child(child, css) {
		if (child->flags & CSS_ONLINE) {
			ret = true;
			break;
		}
	}
	rcu_read_unlock();
	return ret;
}

*/
 css_task_iter_advance_css_set - advance a task itererator to the next css_set
 @it: the iterator to advance

 Advance @it to the next css_set to walk.
 /*
static void css_task_iter_advance_css_set(struct css_task_iterit)
{
	struct list_headl = it->cset_pos;
	struct cgrp_cset_linklink;
	struct css_setcset;

	lockdep_assert_held(&css_set_lock);

	*/ Advance to the next non-empty css_set /*
	do {
		l = l->next;
		if (l == it->cset_head) {
			it->cset_pos = NULL;
			it->task_pos = NULL;
			return;
		}

		if (it->ss) {
			cset = container_of(l, struct css_set,
					    e_cset_node[it->ss->id]);
		} else {
			link = list_entry(l, struct cgrp_cset_link, cset_link);
			cset = link->cset;
		}
	} while (!css_set_populated(cset));

	it->cset_pos = l;

	if (!list_empty(&cset->tasks))
		it->task_pos = cset->tasks.next;
	else
		it->task_pos = cset->mg_tasks.next;

	it->tasks_head = &cset->tasks;
	it->mg_tasks_head = &cset->mg_tasks;

	*/
	 We don't keep css_sets locked across iteration steps and thus
	 need to take steps to ensure that iteration can be resumed after
	 the lock is re-acquired.  Iteration is performed at two levels -
	 css_sets and tasks in them.
	
	 Once created, a css_set never leaves its cgroup lists, so a
	 pinned css_set is guaranteed to stay put and we can resume
	 iteration afterwards.
	
	 Tasks may leave @cset across iteration steps.  This is resolved
	 by registering each iterator with the css_set currently being
	 walked and making css_set_move_task() advance iterators whose
	 next task is leaving.
	 /*
	if (it->cur_cset) {
		list_del(&it->iters_node);
		put_css_set_locked(it->cur_cset);
	}
	get_css_set(cset);
	it->cur_cset = cset;
	list_add(&it->iters_node, &cset->task_iters);
}

static void css_task_iter_advance(struct css_task_iterit)
{
	struct list_headl = it->task_pos;

	lockdep_assert_held(&css_set_lock);
	WARN_ON_ONCE(!l);

	*/
	 Advance iterator to find next entry.  cset->tasks is consumed
	 first and then ->mg_tasks.  After ->mg_tasks, we move onto the
	 next cset.
	 /*
	l = l->next;

	if (l == it->tasks_head)
		l = it->mg_tasks_head->next;

	if (l == it->mg_tasks_head)
		css_task_iter_advance_css_set(it);
	else
		it->task_pos = l;
}

*/
 css_task_iter_start - initiate task iteration
 @css: the css to walk tasks of
 @it: the task iterator to use

 Initiate iteration through the tasks of @css.  The caller can call
 css_task_iter_next() to walk through the tasks until the function
 returns NULL.  On completion of iteration, css_task_iter_end() must be
 called.
 /*
void css_task_iter_start(struct cgroup_subsys_statecss,
			 struct css_task_iterit)
{
	*/ no one should try to iterate before mounting cgroups /*
	WARN_ON_ONCE(!use_task_css_set_links);

	memset(it, 0, sizeof(*it));

	spin_lock_bh(&css_set_lock);

	it->ss = css->ss;

	if (it->ss)
		it->cset_pos = &css->cgroup->e_csets[css->ss->id];
	else
		it->cset_pos = &css->cgroup->cset_links;

	it->cset_head = it->cset_pos;

	css_task_iter_advance_css_set(it);

	spin_unlock_bh(&css_set_lock);
}

*/
 css_task_iter_next - return the next task for the iterator
 @it: the task iterator being iterated

 The "next" function for task iteration.  @it should have been
 initialized via css_task_iter_start().  Returns NULL when the iteration
 reaches the end.
 /*
struct task_structcss_task_iter_next(struct css_task_iterit)
{
	if (it->cur_task) {
		put_task_struct(it->cur_task);
		it->cur_task = NULL;
	}

	spin_lock_bh(&css_set_lock);

	if (it->task_pos) {
		it->cur_task = list_entry(it->task_pos, struct task_struct,
					  cg_list);
		get_task_struct(it->cur_task);
		css_task_iter_advance(it);
	}

	spin_unlock_bh(&css_set_lock);

	return it->cur_task;
}

*/
 css_task_iter_end - finish task iteration
 @it: the task iterator to finish

 Finish task iteration started by css_task_iter_start().
 /*
void css_task_iter_end(struct css_task_iterit)
{
	if (it->cur_cset) {
		spin_lock_bh(&css_set_lock);
		list_del(&it->iters_node);
		put_css_set_locked(it->cur_cset);
		spin_unlock_bh(&css_set_lock);
	}

	if (it->cur_task)
		put_task_struct(it->cur_task);
}

*/
 cgroup_trasnsfer_tasks - move tasks from one cgroup to another
 @to: cgroup to which the tasks will be moved
 @from: cgroup in which the tasks currently reside

 Locking rules between cgroup_post_fork() and the migration path
 guarantee that, if a task is forking while being migrated, the new child
 is guaranteed to be either visible in the source cgroup after the
 parent's migration is complete or put into the target cgroup.  No task
 can slip out of migration through forking.
 /*
int cgroup_transfer_tasks(struct cgroupto, struct cgroupfrom)
{
	LIST_HEAD(preloaded_csets);
	struct cgrp_cset_linklink;
	struct css_task_iter it;
	struct task_structtask;
	int ret;

	if (!cgroup_may_migrate_to(to))
		return -EBUSY;

	mutex_lock(&cgroup_mutex);

	*/ all tasks in @from are being moved, all csets are source /*
	spin_lock_bh(&css_set_lock);
	list_for_each_entry(link, &from->cset_links, cset_link)
		cgroup_migrate_add_src(link->cset, to, &preloaded_csets);
	spin_unlock_bh(&css_set_lock);

	ret = cgroup_migrate_prepare_dst(&preloaded_csets);
	if (ret)
		goto out_err;

	*/
	 Migrate tasks one-by-one until @from is empty.  This fails iff
	 ->can_attach() fails.
	 /*
	do {
		css_task_iter_start(&from->self, &it);
		task = css_task_iter_next(&it);
		if (task)
			get_task_struct(task);
		css_task_iter_end(&it);

		if (task) {
			ret = cgroup_migrate(task, false, to->root);
			put_task_struct(task);
		}
	} while (task && !ret);
out_err:
	cgroup_migrate_finish(&preloaded_csets);
	mutex_unlock(&cgroup_mutex);
	return ret;
}

*/
 Stuff for reading the 'tasks'/'procs' files.

 Reading this file can return large amounts of data if a cgroup has
lots* of attached tasks. So it may need several calls to read(),
 but we cannot guarantee that the information we produce is correct
 unless we produce it entirely atomically.

 /*

*/ which pidlist file are we talking about? /*
enum cgroup_filetype {
	CGROUP_FILE_PROCS,
	CGROUP_FILE_TASKS,
};

*/
 A pidlist is a list of pids that virtually represents the contents of one
 of the cgroup files ("procs" or "tasks"). We keep a list of such pidlists,
 a pair (one each for procs, tasks) for each pid namespace that's relevant
 to the cgroup.
 /*
struct cgroup_pidlist {
	*/
	 used to find which pidlist is wanted. doesn't change as long as
	 this particular list stays in the list.
	/*
	struct { enum cgroup_filetype type; struct pid_namespacens; } key;
	*/ array of xids /*
	pid_tlist;
	*/ how many elements the above list has /*
	int length;
	*/ each of these stored in a list by its cgroup /*
	struct list_head links;
	*/ pointer to the cgroup we belong to, for list removal purposes /*
	struct cgroupowner;
	*/ for delayed destruction /*
	struct delayed_work destroy_dwork;
};

*/
 The following two functions "fix" the issue where there are more pids
 than kmalloc will give memory for; in such cases, we use vmalloc/vfree.
 TODO: replace with a kernel-wide solution to this problem
 /*
#define PIDLIST_TOO_LARGE(c) ((c) sizeof(pid_t) > (PAGE_SIZE 2))
static voidpidlist_allocate(int count)
{
	if (PIDLIST_TOO_LARGE(count))
		return vmalloc(count sizeof(pid_t));
	else
		return kmalloc(count sizeof(pid_t), GFP_KERNEL);
}

static void pidlist_free(voidp)
{
	kvfree(p);
}

*/
 Used to destroy all pidlists lingering waiting for destroy timer.  None
 should be left afterwards.
 /*
static void cgroup_pidlist_destroy_all(struct cgroupcgrp)
{
	struct cgroup_pidlistl,tmp_l;

	mutex_lock(&cgrp->pidlist_mutex);
	list_for_each_entry_safe(l, tmp_l, &cgrp->pidlists, links)
		mod_delayed_work(cgroup_pidlist_destroy_wq, &l->destroy_dwork, 0);
	mutex_unlock(&cgrp->pidlist_mutex);

	flush_workqueue(cgroup_pidlist_destroy_wq);
	BUG_ON(!list_empty(&cgrp->pidlists));
}

static void cgroup_pidlist_destroy_work_fn(struct work_structwork)
{
	struct delayed_workdwork = to_delayed_work(work);
	struct cgroup_pidlistl = container_of(dwork, struct cgroup_pidlist,
						destroy_dwork);
	struct cgroup_pidlisttofree = NULL;

	mutex_lock(&l->owner->pidlist_mutex);

	*/
	 Destroy iff we didn't get queued again.  The state won't change
	 as destroy_dwork can only be queued while locked.
	 /*
	if (!delayed_work_pending(dwork)) {
		list_del(&l->links);
		pidlist_free(l->list);
		put_pid_ns(l->key.ns);
		tofree = l;
	}

	mutex_unlock(&l->owner->pidlist_mutex);
	kfree(tofree);
}

*/
 pidlist_uniq - given a kmalloc()ed list, strip out all duplicate entries
 Returns the number of unique elements.
 /*
static int pidlist_uniq(pid_tlist, int length)
{
	int src, dest = 1;

	*/
	 we presume the 0th element is unique, so i starts at 1. trivial
	 edge cases first; no work needs to be done for either
	 /*
	if (length == 0 || length == 1)
		return length;
	*/ src and dest walk down the list; dest counts unique elements /*
	for (src = 1; src < length; src++) {
		*/ find next unique element /*
		while (list[src] == list[src-1]) {
			src++;
			if (src == length)
				goto after;
		}
		*/ dest always points to where the next unique element goes /*
		list[dest] = list[src];
		dest++;
	}
after:
	return dest;
}

*/
 The two pid files - task and cgroup.procs - guaranteed that the result
 is sorted, which forced this whole pidlist fiasco.  As pid order is
 different per namespace, each namespace needs differently sorted list,
 making it impossible to use, for example, single rbtree of member tasks
 sorted by task pointer.  As pidlists can be fairly large, allocating one
 per open file is dangerous, so cgroup had to implement shared pool of
 pidlists keyed by cgroup and namespace.

 All this extra complexity was caused by the original implementation
 committing to an entirely unnecessary property.  In the long term, we
 want to do away with it.  Explicitly scramble sort order if on the
 default hierarchy so that no such expectation exists in the new
 interface.

 Scrambling is done by swapping every two consecutive bits, which is
 non-identity one-to-one mapping which disturbs sort order sufficiently.
 /*
static pid_t pid_fry(pid_t pid)
{
	unsigned a = pid & 0x55555555;
	unsigned b = pid & 0xAAAAAAAA;

	return (a << 1) | (b >> 1);
}

static pid_t cgroup_pid_fry(struct cgroupcgrp, pid_t pid)
{
	if (cgroup_on_dfl(cgrp))
		return pid_fry(pid);
	else
		return pid;
}

static int cmppid(const voida, const voidb)
{
	return(pid_t)a -(pid_t)b;
}

static int fried_cmppid(const voida, const voidb)
{
	return pid_fry(*(pid_t)a) - pid_fry(*(pid_t)b);
}

static struct cgroup_pidlistcgroup_pidlist_find(struct cgroupcgrp,
						  enum cgroup_filetype type)
{
	struct cgroup_pidlistl;
	*/ don't need task_nsproxy() if we're looking at ourself /*
	struct pid_namespacens = task_active_pid_ns(current);

	lockdep_assert_held(&cgrp->pidlist_mutex);

	list_for_each_entry(l, &cgrp->pidlists, links)
		if (l->key.type == type && l->key.ns == ns)
			return l;
	return NULL;
}

*/
 find the appropriate pidlist for our purpose (given procs vs tasks)
 returns with the lock on that pidlist already held, and takes care
 of the use count, or returns NULL with no locks held if we're out of
 memory.
 /*
static struct cgroup_pidlistcgroup_pidlist_find_create(struct cgroupcgrp,
						enum cgroup_filetype type)
{
	struct cgroup_pidlistl;

	lockdep_assert_held(&cgrp->pidlist_mutex);

	l = cgroup_pidlist_find(cgrp, type);
	if (l)
		return l;

	*/ entry not found; create a new one /*
	l = kzalloc(sizeof(struct cgroup_pidlist), GFP_KERNEL);
	if (!l)
		return l;

	INIT_DELAYED_WORK(&l->destroy_dwork, cgroup_pidlist_destroy_work_fn);
	l->key.type = type;
	*/ don't need task_nsproxy() if we're looking at ourself /*
	l->key.ns = get_pid_ns(task_active_pid_ns(current));
	l->owner = cgrp;
	list_add(&l->links, &cgrp->pidlists);
	return l;
}

*/
 Load a cgroup's pidarray with either procs' tgids or tasks' pids
 /*
static int pidlist_array_load(struct cgroupcgrp, enum cgroup_filetype type,
			      struct cgroup_pidlist*lp)
{
	pid_tarray;
	int length;
	int pid, n = 0;/ used for populating the array /*
	struct css_task_iter it;
	struct task_structtsk;
	struct cgroup_pidlistl;

	lockdep_assert_held(&cgrp->pidlist_mutex);

	*/
	 If cgroup gets more users after we read count, we won't have
	 enough space - tough.  This race is indistinguishable to the
	 caller from the case that the additional cgroup users didn't
	 show up until sometime later on.
	 /*
	length = cgroup_task_count(cgrp);
	array = pidlist_allocate(length);
	if (!array)
		return -ENOMEM;
	*/ now, populate the array /*
	css_task_iter_start(&cgrp->self, &it);
	while ((tsk = css_task_iter_next(&it))) {
		if (unlikely(n == length))
			break;
		*/ get tgid or pid for procs or tasks file respectively /*
		if (type == CGROUP_FILE_PROCS)
			pid = task_tgid_vnr(tsk);
		else
			pid = task_pid_vnr(tsk);
		if (pid > 0)/ make sure to only use valid results /*
			array[n++] = pid;
	}
	css_task_iter_end(&it);
	length = n;
	*/ now sort & (if procs) strip out duplicates /*
	if (cgroup_on_dfl(cgrp))
		sort(array, length, sizeof(pid_t), fried_cmppid, NULL);
	else
		sort(array, length, sizeof(pid_t), cmppid, NULL);
	if (type == CGROUP_FILE_PROCS)
		length = pidlist_uniq(array, length);

	l = cgroup_pidlist_find_create(cgrp, type);
	if (!l) {
		pidlist_free(array);
		return -ENOMEM;
	}

	*/ store array, freeing old if necessary /*
	pidlist_free(l->list);
	l->list = array;
	l->length = length;
	*lp = l;
	return 0;
}

*/
 cgroupstats_build - build and fill cgroupstats
 @stats: cgroupstats to fill information into
 @dentry: A dentry entry belonging to the cgroup for which stats have
 been requested.

 Build and fill cgroupstats so that taskstats can export it to user
 space.
 /*
int cgroupstats_build(struct cgroupstatsstats, struct dentrydentry)
{
	struct kernfs_nodekn = kernfs_node_from_dentry(dentry);
	struct cgroupcgrp;
	struct css_task_iter it;
	struct task_structtsk;

	*/ it should be kernfs_node belonging to cgroupfs and is a directory /*
	if (dentry->d_sb->s_type != &cgroup_fs_type || !kn ||
	    kernfs_type(kn) != KERNFS_DIR)
		return -EINVAL;

	mutex_lock(&cgroup_mutex);

	*/
	 We aren't being called from kernfs and there's no guarantee on
	 @kn->priv's validity.  For this and css_tryget_online_from_dir(),
	 @kn->priv is RCU safe.  Let's do the RCU dancing.
	 /*
	rcu_read_lock();
	cgrp = rcu_dereference(kn->priv);
	if (!cgrp || cgroup_is_dead(cgrp)) {
		rcu_read_unlock();
		mutex_unlock(&cgroup_mutex);
		return -ENOENT;
	}
	rcu_read_unlock();

	css_task_iter_start(&cgrp->self, &it);
	while ((tsk = css_task_iter_next(&it))) {
		switch (tsk->state) {
		case TASK_RUNNING:
			stats->nr_running++;
			break;
		case TASK_INTERRUPTIBLE:
			stats->nr_sleeping++;
			break;
		case TASK_UNINTERRUPTIBLE:
			stats->nr_uninterruptible++;
			break;
		case TASK_STOPPED:
			stats->nr_stopped++;
			break;
		default:
			if (delayacct_is_task_waiting_on_io(tsk))
				stats->nr_io_wait++;
			break;
		}
	}
	css_task_iter_end(&it);

	mutex_unlock(&cgroup_mutex);
	return 0;
}


*/
 seq_file methods for the tasks/procs files. The seq_file position is the
 next pid to display; the seq_file iterator is a pointer to the pid
 in the cgroup->l->list array.
 /*

static voidcgroup_pidlist_start(struct seq_files, loff_tpos)
{
	*/
	 Initially we receive a position value that corresponds to
	 one more than the last pid shown (or 0 on the first call or
	 after a seek to the start). Use a binary-search to find the
	 next pid to display, if any
	 /*
	struct kernfs_open_fileof = s->private;
	struct cgroupcgrp = seq_css(s)->cgroup;
	struct cgroup_pidlistl;
	enum cgroup_filetype type = seq_cft(s)->private;
	int index = 0, pid =pos;
	intiter, ret;

	mutex_lock(&cgrp->pidlist_mutex);

	*/
	 !NULL @of->priv indicates that this isn't the first start()
	 after open.  If the matching pidlist is around, we can use that.
	 Look for it.  Note that @of->priv can't be used directly.  It
	 could already have been destroyed.
	 /*
	if (of->priv)
		of->priv = cgroup_pidlist_find(cgrp, type);

	*/
	 Either this is the first start() after open or the matching
	 pidlist has been destroyed inbetween.  Create a new one.
	 /*
	if (!of->priv) {
		ret = pidlist_array_load(cgrp, type,
					 (struct cgroup_pidlist*)&of->priv);
		if (ret)
			return ERR_PTR(ret);
	}
	l = of->priv;

	if (pid) {
		int end = l->length;

		while (index < end) {
			int mid = (index + end) / 2;
			if (cgroup_pid_fry(cgrp, l->list[mid]) == pid) {
				index = mid;
				break;
			} else if (cgroup_pid_fry(cgrp, l->list[mid]) <= pid)
				index = mid + 1;
			else
				end = mid;
		}
	}
	*/ If we're off the end of the array, we're done /*
	if (index >= l->length)
		return NULL;
	*/ Update the abstract position to be the actual pid that we found /*
	iter = l->list + index;
	*pos = cgroup_pid_fry(cgrp,iter);
	return iter;
}

static void cgroup_pidlist_stop(struct seq_files, voidv)
{
	struct kernfs_open_fileof = s->private;
	struct cgroup_pidlistl = of->priv;

	if (l)
		mod_delayed_work(cgroup_pidlist_destroy_wq, &l->destroy_dwork,
				 CGROUP_PIDLIST_DESTROY_DELAY);
	mutex_unlock(&seq_css(s)->cgroup->pidlist_mutex);
}

static voidcgroup_pidlist_next(struct seq_files, voidv, loff_tpos)
{
	struct kernfs_open_fileof = s->private;
	struct cgroup_pidlistl = of->priv;
	pid_tp = v;
	pid_tend = l->list + l->length;
	*/
	 Advance to the next pid in the array. If this goes off the
	 end, we're done
	 /*
	p++;
	if (p >= end) {
		return NULL;
	} else {
		*pos = cgroup_pid_fry(seq_css(s)->cgroup,p);
		return p;
	}
}

static int cgroup_pidlist_show(struct seq_files, voidv)
{
	seq_printf(s, "%d\n",(int)v);

	return 0;
}

static u64 cgroup_read_notify_on_release(struct cgroup_subsys_statecss,
					 struct cftypecft)
{
	return notify_on_release(css->cgroup);
}

static int cgroup_write_notify_on_release(struct cgroup_subsys_statecss,
					  struct cftypecft, u64 val)
{
	if (val)
		set_bit(CGRP_NOTIFY_ON_RELEASE, &css->cgroup->flags);
	else
		clear_bit(CGRP_NOTIFY_ON_RELEASE, &css->cgroup->flags);
	return 0;
}

static u64 cgroup_clone_children_read(struct cgroup_subsys_statecss,
				      struct cftypecft)
{
	return test_bit(CGRP_CPUSET_CLONE_CHILDREN, &css->cgroup->flags);
}

static int cgroup_clone_children_write(struct cgroup_subsys_statecss,
				       struct cftypecft, u64 val)
{
	if (val)
		set_bit(CGRP_CPUSET_CLONE_CHILDREN, &css->cgroup->flags);
	else
		clear_bit(CGRP_CPUSET_CLONE_CHILDREN, &css->cgroup->flags);
	return 0;
}

*/ cgroup core interface files for the default hierarchy /*
static struct cftype cgroup_dfl_base_files[] = {
	{
		.name = "cgroup.procs",
		.file_offset = offsetof(struct cgroup, procs_file),
		.seq_start = cgroup_pidlist_start,
		.seq_next = cgroup_pidlist_next,
		.seq_stop = cgroup_pidlist_stop,
		.seq_show = cgroup_pidlist_show,
		.private = CGROUP_FILE_PROCS,
		.write = cgroup_procs_write,
	},
	{
		.name = "cgroup.controllers",
		.seq_show = cgroup_controllers_show,
	},
	{
		.name = "cgroup.subtree_control",
		.seq_show = cgroup_subtree_control_show,
		.write = cgroup_subtree_control_write,
	},
	{
		.name = "cgroup.events",
		.flags = CFTYPE_NOT_ON_ROOT,
		.file_offset = offsetof(struct cgroup, events_file),
		.seq_show = cgroup_events_show,
	},
	{ }	*/ terminate /*
};

*/ cgroup core interface files for the legacy hierarchies /*
static struct cftype cgroup_legacy_base_files[] = {
	{
		.name = "cgroup.procs",
		.seq_start = cgroup_pidlist_start,
		.seq_next = cgroup_pidlist_next,
		.seq_stop = cgroup_pidlist_stop,
		.seq_show = cgroup_pidlist_show,
		.private = CGROUP_FILE_PROCS,
		.write = cgroup_procs_write,
	},
	{
		.name = "cgroup.clone_children",
		.read_u64 = cgroup_clone_children_read,
		.write_u64 = cgroup_clone_children_write,
	},
	{
		.name = "cgroup.sane_behavior",
		.flags = CFTYPE_ONLY_ON_ROOT,
		.seq_show = cgroup_sane_behavior_show,
	},
	{
		.name = "tasks",
		.seq_start = cgroup_pidlist_start,
		.seq_next = cgroup_pidlist_next,
		.seq_stop = cgroup_pidlist_stop,
		.seq_show = cgroup_pidlist_show,
		.private = CGROUP_FILE_TASKS,
		.write = cgroup_tasks_write,
	},
	{
		.name = "notify_on_release",
		.read_u64 = cgroup_read_notify_on_release,
		.write_u64 = cgroup_write_notify_on_release,
	},
	{
		.name = "release_agent",
		.flags = CFTYPE_ONLY_ON_ROOT,
		.seq_show = cgroup_release_agent_show,
		.write = cgroup_release_agent_write,
		.max_write_len = PATH_MAX - 1,
	},
	{ }	*/ terminate /*
};

*/
 css destruction is four-stage process.

 1. Destruction starts.  Killing of the percpu_ref is initiated.
    Implemented in kill_css().

 2. When the percpu_ref is confirmed to be visible as killed on all CPUs
    and thus css_tryget_online() is guaranteed to fail, the css can be
    offlined by invoking offline_css().  After offlining, the base ref is
    put.  Implemented in css_killed_work_fn().

 3. When the percpu_ref reaches zero, the only possible remaining
    accessors are inside RCU read sections.  css_release() schedules the
    RCU callback.

 4. After the grace period, the css can be freed.  Implemented in
    css_free_work_fn().

 It is actually hairier because both step 2 and 4 require process context
 and thus involve punting to css->destroy_work adding two additional
 steps to the already complex sequence.
 /*
static void css_free_work_fn(struct work_structwork)
{
	struct cgroup_subsys_statecss =
		container_of(work, struct cgroup_subsys_state, destroy_work);
	struct cgroup_subsysss = css->ss;
	struct cgroupcgrp = css->cgroup;

	percpu_ref_exit(&css->refcnt);

	if (ss) {
		*/ css free path /*
		struct cgroup_subsys_stateparent = css->parent;
		int id = css->id;

		ss->css_free(css);
		cgroup_idr_remove(&ss->css_idr, id);
		cgroup_put(cgrp);

		if (parent)
			css_put(parent);
	} else {
		*/ cgroup free path /*
		atomic_dec(&cgrp->root->nr_cgrps);
		cgroup_pidlist_destroy_all(cgrp);
		cancel_work_sync(&cgrp->release_agent_work);

		if (cgroup_parent(cgrp)) {
			*/
			 We get a ref to the parent, and put the ref when
			 this cgroup is being freed, so it's guaranteed
			 that the parent won't be destroyed before its
			 children.
			 /*
			cgroup_put(cgroup_parent(cgrp));
			kernfs_put(cgrp->kn);
			kfree(cgrp);
		} else {
			*/
			 This is root cgroup's refcnt reaching zero,
			 which indicates that the root should be
			 released.
			 /*
			cgroup_destroy_root(cgrp->root);
		}
	}
}

static void css_free_rcu_fn(struct rcu_headrcu_head)
{
	struct cgroup_subsys_statecss =
		container_of(rcu_head, struct cgroup_subsys_state, rcu_head);

	INIT_WORK(&css->destroy_work, css_free_work_fn);
	queue_work(cgroup_destroy_wq, &css->destroy_work);
}

static void css_release_work_fn(struct work_structwork)
{
	struct cgroup_subsys_statecss =
		container_of(work, struct cgroup_subsys_state, destroy_work);
	struct cgroup_subsysss = css->ss;
	struct cgroupcgrp = css->cgroup;

	mutex_lock(&cgroup_mutex);

	css->flags |= CSS_RELEASED;
	list_del_rcu(&css->sibling);

	if (ss) {
		*/ css release path /*
		cgroup_idr_replace(&ss->css_idr, NULL, css->id);
		if (ss->css_released)
			ss->css_released(css);
	} else {
		*/ cgroup release path /*
		cgroup_idr_remove(&cgrp->root->cgroup_idr, cgrp->id);
		cgrp->id = -1;

		*/
		 There are two control paths which try to determine
		 cgroup from dentry without going through kernfs -
		 cgroupstats_build() and css_tryget_online_from_dir().
		 Those are supported by RCU protecting clearing of
		 cgrp->kn->priv backpointer.
		 /*
		if (cgrp->kn)
			RCU_INIT_POINTER(*(void __rcu __force*)&cgrp->kn->priv,
					 NULL);
	}

	mutex_unlock(&cgroup_mutex);

	call_rcu(&css->rcu_head, css_free_rcu_fn);
}

static void css_release(struct percpu_refref)
{
	struct cgroup_subsys_statecss =
		container_of(ref, struct cgroup_subsys_state, refcnt);

	INIT_WORK(&css->destroy_work, css_release_work_fn);
	queue_work(cgroup_destroy_wq, &css->destroy_work);
}

static void init_and_link_css(struct cgroup_subsys_statecss,
			      struct cgroup_subsysss, struct cgroupcgrp)
{
	lockdep_assert_held(&cgroup_mutex);

	cgroup_get(cgrp);

	memset(css, 0, sizeof(*css));
	css->cgroup = cgrp;
	css->ss = ss;
	INIT_LIST_HEAD(&css->sibling);
	INIT_LIST_HEAD(&css->children);
	css->serial_nr = css_serial_nr_next++;
	atomic_set(&css->online_cnt, 0);

	if (cgroup_parent(cgrp)) {
		css->parent = cgroup_css(cgroup_parent(cgrp), ss);
		css_get(css->parent);
	}

	BUG_ON(cgroup_css(cgrp, ss));
}

*/ invoke ->css_online() on a new CSS and mark it online if successful /*
static int online_css(struct cgroup_subsys_statecss)
{
	struct cgroup_subsysss = css->ss;
	int ret = 0;

	lockdep_assert_held(&cgroup_mutex);

	if (ss->css_online)
		ret = ss->css_online(css);
	if (!ret) {
		css->flags |= CSS_ONLINE;
		rcu_assign_pointer(css->cgroup->subsys[ss->id], css);

		atomic_inc(&css->online_cnt);
		if (css->parent)
			atomic_inc(&css->parent->online_cnt);
	}
	return ret;
}

*/ if the CSS is online, invoke ->css_offline() on it and mark it offline /*
static void offline_css(struct cgroup_subsys_statecss)
{
	struct cgroup_subsysss = css->ss;

	lockdep_assert_held(&cgroup_mutex);

	if (!(css->flags & CSS_ONLINE))
		return;

	if (ss->css_reset)
		ss->css_reset(css);

	if (ss->css_offline)
		ss->css_offline(css);

	css->flags &= ~CSS_ONLINE;
	RCU_INIT_POINTER(css->cgroup->subsys[ss->id], NULL);

	wake_up_all(&css->cgroup->offline_waitq);
}

*/
 css_create - create a cgroup_subsys_state
 @cgrp: the cgroup new css will be associated with
 @ss: the subsys of new css

 Create a new css associated with @cgrp - @ss pair.  On success, the new
 css is online and installed in @cgrp.  This function doesn't create the
 interface files.  Returns 0 on success, -errno on failure.
 /*
static struct cgroup_subsys_statecss_create(struct cgroupcgrp,
					      struct cgroup_subsysss)
{
	struct cgroupparent = cgroup_parent(cgrp);
	struct cgroup_subsys_stateparent_css = cgroup_css(parent, ss);
	struct cgroup_subsys_statecss;
	int err;

	lockdep_assert_held(&cgroup_mutex);

	css = ss->css_alloc(parent_css);
	if (IS_ERR(css))
		return css;

	init_and_link_css(css, ss, cgrp);

	err = percpu_ref_init(&css->refcnt, css_release, 0, GFP_KERNEL);
	if (err)
		goto err_free_css;

	err = cgroup_idr_alloc(&ss->css_idr, NULL, 2, 0, GFP_KERNEL);
	if (err < 0)
		goto err_free_percpu_ref;
	css->id = err;

	*/ @css is ready to be brought online now, make it visible /*
	list_add_tail_rcu(&css->sibling, &parent_css->children);
	cgroup_idr_replace(&ss->css_idr, css, css->id);

	err = online_css(css);
	if (err)
		goto err_list_del;

	if (ss->broken_hierarchy && !ss->warned_broken_hierarchy &&
	    cgroup_parent(parent)) {
		pr_warn("%s (%d) created nested cgroup for controller \"%s\" which has incomplete hierarchy support. Nested cgroups may change behavior in the future.\n",
			current->comm, current->pid, ss->name);
		if (!strcmp(ss->name, "memory"))
			pr_warn("\"memory\" requires setting use_hierarchy to 1 on the root\n");
		ss->warned_broken_hierarchy = true;
	}

	return css;

err_list_del:
	list_del_rcu(&css->sibling);
	cgroup_idr_remove(&ss->css_idr, css->id);
err_free_percpu_ref:
	percpu_ref_exit(&css->refcnt);
err_free_css:
	call_rcu(&css->rcu_head, css_free_rcu_fn);
	return ERR_PTR(err);
}

static struct cgroupcgroup_create(struct cgroupparent)
{
	struct cgroup_rootroot = parent->root;
	struct cgroupcgrp,tcgrp;
	int level = parent->level + 1;
	int ret;

	*/ allocate the cgroup and its ID, 0 is reserved for the root /*
	cgrp = kzalloc(sizeof(*cgrp) +
		       sizeof(cgrp->ancestor_ids[0]) (level + 1), GFP_KERNEL);
	if (!cgrp)
		return ERR_PTR(-ENOMEM);

	ret = percpu_ref_init(&cgrp->self.refcnt, css_release, 0, GFP_KERNEL);
	if (ret)
		goto out_free_cgrp;

	*/
	 Temporarily set the pointer to NULL, so idr_find() won't return
	 a half-baked cgroup.
	 /*
	cgrp->id = cgroup_idr_alloc(&root->cgroup_idr, NULL, 2, 0, GFP_KERNEL);
	if (cgrp->id < 0) {
		ret = -ENOMEM;
		goto out_cancel_ref;
	}

	init_cgroup_housekeeping(cgrp);

	cgrp->self.parent = &parent->self;
	cgrp->root = root;
	cgrp->level = level;

	for (tcgrp = cgrp; tcgrp; tcgrp = cgroup_parent(tcgrp))
		cgrp->ancestor_ids[tcgrp->level] = tcgrp->id;

	if (notify_on_release(parent))
		set_bit(CGRP_NOTIFY_ON_RELEASE, &cgrp->flags);

	if (test_bit(CGRP_CPUSET_CLONE_CHILDREN, &parent->flags))
		set_bit(CGRP_CPUSET_CLONE_CHILDREN, &cgrp->flags);

	cgrp->self.serial_nr = css_serial_nr_next++;

	*/ allocation complete, commit to creation /*
	list_add_tail_rcu(&cgrp->self.sibling, &cgroup_parent(cgrp)->self.children);
	atomic_inc(&root->nr_cgrps);
	cgroup_get(parent);

	*/
	 @cgrp is now fully operational.  If something fails after this
	 point, it'll be released via the normal destruction path.
	 /*
	cgroup_idr_replace(&root->cgroup_idr, cgrp, cgrp->id);

	*/
	 On the default hierarchy, a child doesn't automatically inherit
	 subtree_control from the parent.  Each is configured manually.
	 /*
	if (!cgroup_on_dfl(cgrp))
		cgrp->subtree_control = cgroup_control(cgrp);

	cgroup_propagate_control(cgrp);

	*/ @cgrp doesn't have dir yet so the following will only create csses /*
	ret = cgroup_apply_control_enable(cgrp);
	if (ret)
		goto out_destroy;

	return cgrp;

out_cancel_ref:
	percpu_ref_exit(&cgrp->self.refcnt);
out_free_cgrp:
	kfree(cgrp);
	return ERR_PTR(ret);
out_destroy:
	cgroup_destroy_locked(cgrp);
	return ERR_PTR(ret);
}

static int cgroup_mkdir(struct kernfs_nodeparent_kn, const charname,
			umode_t mode)
{
	struct cgroupparent,cgrp;
	struct kernfs_nodekn;
	int ret;

	*/ do not accept '\n' to prevent making /proc/<pid>/cgroup unparsable /*
	if (strchr(name, '\n'))
		return -EINVAL;

	parent = cgroup_kn_lock_live(parent_kn, false);
	if (!parent)
		return -ENODEV;

	cgrp = cgroup_create(parent);
	if (IS_ERR(cgrp)) {
		ret = PTR_ERR(cgrp);
		goto out_unlock;
	}

	*/ create the directory /*
	kn = kernfs_create_dir(parent->kn, name, mode, cgrp);
	if (IS_ERR(kn)) {
		ret = PTR_ERR(kn);
		goto out_destroy;
	}
	cgrp->kn = kn;

	*/
	 This extra ref will be put in cgroup_free_fn() and guarantees
	 that @cgrp->kn is always accessible.
	 /*
	kernfs_get(kn);

	ret = cgroup_kn_set_ugid(kn);
	if (ret)
		goto out_destroy;

	ret = css_populate_dir(&cgrp->self);
	if (ret)
		goto out_destroy;

	ret = cgroup_apply_control_enable(cgrp);
	if (ret)
		goto out_destroy;

	*/ let's create and online css's /*
	kernfs_activate(kn);

	ret = 0;
	goto out_unlock;

out_destroy:
	cgroup_destroy_locked(cgrp);
out_unlock:
	cgroup_kn_unlock(parent_kn);
	return ret;
}

*/
 This is called when the refcnt of a css is confirmed to be killed.
 css_tryget_online() is now guaranteed to fail.  Tell the subsystem to
 initate destruction and put the css ref from kill_css().
 /*
static void css_killed_work_fn(struct work_structwork)
{
	struct cgroup_subsys_statecss =
		container_of(work, struct cgroup_subsys_state, destroy_work);

	mutex_lock(&cgroup_mutex);

	do {
		offline_css(css);
		css_put(css);
		*/ @css can't go away while we're holding cgroup_mutex /*
		css = css->parent;
	} while (css && atomic_dec_and_test(&css->online_cnt));

	mutex_unlock(&cgroup_mutex);
}

*/ css kill confirmation processing requires process context, bounce /*
static void css_killed_ref_fn(struct percpu_refref)
{
	struct cgroup_subsys_statecss =
		container_of(ref, struct cgroup_subsys_state, refcnt);

	if (atomic_dec_and_test(&css->online_cnt)) {
		INIT_WORK(&css->destroy_work, css_killed_work_fn);
		queue_work(cgroup_destroy_wq, &css->destroy_work);
	}
}

*/
 kill_css - destroy a css
 @css: css to destroy

 This function initiates destruction of @css by removing cgroup interface
 files and putting its base reference.  ->css_offline() will be invoked
 asynchronously once css_tryget_online() is guaranteed to fail and when
 the reference count reaches zero, @css will be released.
 /*
static void kill_css(struct cgroup_subsys_statecss)
{
	lockdep_assert_held(&cgroup_mutex);

	*/
	 This must happen before css is disassociated with its cgroup.
	 See seq_css() for details.
	 /*
	css_clear_dir(css);

	*/
	 Killing would put the base ref, but we need to keep it alive
	 until after ->css_offline().
	 /*
	css_get(css);

	*/
	 cgroup core guarantees that, by the time ->css_offline() is
	 invoked, no new css reference will be given out via
	 css_tryget_online().  We can't simply call percpu_ref_kill() and
	 proceed to offlining css's because percpu_ref_kill() doesn't
	 guarantee that the ref is seen as killed on all CPUs on return.
	
	 Use percpu_ref_kill_and_confirm() to get notifications as each
	 css is confirmed to be seen as killed on all CPUs.
	 /*
	percpu_ref_kill_and_confirm(&css->refcnt, css_killed_ref_fn);
}

*/
 cgroup_destroy_locked - the first stage of cgroup destruction
 @cgrp: cgroup to be destroyed

 css's make use of percpu refcnts whose killing latency shouldn't be
 exposed to userland and are RCU protected.  Also, cgroup core needs to
 guarantee that css_tryget_online() won't succeed by the time
 ->css_offline() is invoked.  To satisfy all the requirements,
 destruction is implemented in the following two steps.

 s1. Verify @cgrp can be destroyed and mark it dying.  Remove all
     userland visible parts and start killing the percpu refcnts of
     css's.  Set up so that the next stage will be kicked off once all
     the percpu refcnts are confirmed to be killed.

 s2. Invoke ->css_offline(), mark the cgroup dead and proceed with the
     rest of destruction.  Once all cgroup references are gone, the
     cgroup is RCU-freed.

 This function implements s1.  After this step, @cgrp is gone as far as
 the userland is concerned and a new cgroup with the same name may be
 created.  As cgroup doesn't care about the names internally, this
 doesn't cause any problem.
 /*
static int cgroup_destroy_locked(struct cgroupcgrp)
	__releases(&cgroup_mutex) __acquires(&cgroup_mutex)
{
	struct cgroup_subsys_statecss;
	struct cgrp_cset_linklink;
	int ssid;

	lockdep_assert_held(&cgroup_mutex);

	*/
	 Only migration can raise populated from zero and we're already
	 holding cgroup_mutex.
	 /*
	if (cgroup_is_populated(cgrp))
		return -EBUSY;

	*/
	 Make sure there's no live children.  We can't test emptiness of
	 ->self.children as dead children linger on it while being
	 drained; otherwise, "rmdir parent/child parent" may fail.
	 /*
	if (css_has_online_children(&cgrp->self))
		return -EBUSY;

	*/
	 Mark @cgrp and the associated csets dead.  The former prevents
	 further task migration and child creation by disabling
	 cgroup_lock_live_group().  The latter makes the csets ignored by
	 the migration path.
	 /*
	cgrp->self.flags &= ~CSS_ONLINE;

	spin_lock_bh(&css_set_lock);
	list_for_each_entry(link, &cgrp->cset_links, cset_link)
		link->cset->dead = true;
	spin_unlock_bh(&css_set_lock);

	*/ initiate massacre of all css's /*
	for_each_css(css, ssid, cgrp)
		kill_css(css);

	*/
	 Remove @cgrp directory along with the base files.  @cgrp has an
	 extra ref on its kn.
	 /*
	kernfs_remove(cgrp->kn);

	check_for_release(cgroup_parent(cgrp));

	*/ put the base reference /*
	percpu_ref_kill(&cgrp->self.refcnt);

	return 0;
};

static int cgroup_rmdir(struct kernfs_nodekn)
{
	struct cgroupcgrp;
	int ret = 0;

	cgrp = cgroup_kn_lock_live(kn, false);
	if (!cgrp)
		return 0;

	ret = cgroup_destroy_locked(cgrp);

	cgroup_kn_unlock(kn);
	return ret;
}

static struct kernfs_syscall_ops cgroup_kf_syscall_ops = {
	.remount_fs		= cgroup_remount,
	.show_options		= cgroup_show_options,
	.mkdir			= cgroup_mkdir,
	.rmdir			= cgroup_rmdir,
	.rename			= cgroup_rename,
};

static void __init cgroup_init_subsys(struct cgroup_subsysss, bool early)
{
	struct cgroup_subsys_statecss;

	pr_debug("Initializing cgroup subsys %s\n", ss->name);

	mutex_lock(&cgroup_mutex);

	idr_init(&ss->css_idr);
	INIT_LIST_HEAD(&ss->cfts);

	*/ Create the root cgroup state for this subsystem /*
	ss->root = &cgrp_dfl_root;
	css = ss->css_alloc(cgroup_css(&cgrp_dfl_root.cgrp, ss));
	*/ We don't handle early failures gracefully /*
	BUG_ON(IS_ERR(css));
	init_and_link_css(css, ss, &cgrp_dfl_root.cgrp);

	*/
	 Root csses are never destroyed and we can't initialize
	 percpu_ref during early init.  Disable refcnting.
	 /*
	css->flags |= CSS_NO_REF;

	if (early) {
		*/ allocation can't be done safely during early init /*
		css->id = 1;
	} else {
		css->id = cgroup_idr_alloc(&ss->css_idr, css, 1, 2, GFP_KERNEL);
		BUG_ON(css->id < 0);
	}

	*/ Update the init_css_set to contain a subsys
	 pointer to this state - since the subsystem is
	 newly registered, all tasks and hence the
	 init_css_set is in the subsystem's root cgroup. /*
	init_css_set.subsys[ss->id] = css;

	have_fork_callback |= (bool)ss->fork << ss->id;
	have_exit_callback |= (bool)ss->exit << ss->id;
	have_free_callback |= (bool)ss->free << ss->id;
	have_canfork_callback |= (bool)ss->can_fork << ss->id;

	*/ At system boot, before all subsystems have been
	 registered, no tasks have been forked, so we don't
	 need to invoke fork callbacks here. /*
	BUG_ON(!list_empty(&init_task.tasks));

	BUG_ON(online_css(css));

	mutex_unlock(&cgroup_mutex);
}

*/
 cgroup_init_early - cgroup initialization at system boot

 Initialize cgroups at system boot, and initialize any
 subsystems that request early init.
 /*
int __init cgroup_init_early(void)
{
	static struct cgroup_sb_opts __initdata opts;
	struct cgroup_subsysss;
	int i;

	init_cgroup_root(&cgrp_dfl_root, &opts);
	cgrp_dfl_root.cgrp.self.flags |= CSS_NO_REF;

	RCU_INIT_POINTER(init_task.cgroups, &init_css_set);

	for_each_subsys(ss, i) {
		WARN(!ss->css_alloc || !ss->css_free || ss->name || ss->id,
		     "invalid cgroup_subsys %d:%s css_alloc=%p css_free=%p id:name=%d:%s\n",
		     i, cgroup_subsys_name[i], ss->css_alloc, ss->css_free,
		     ss->id, ss->name);
		WARN(strlen(cgroup_subsys_name[i]) > MAX_CGROUP_TYPE_NAMELEN,
		     "cgroup_subsys_name %s too long\n", cgroup_subsys_name[i]);

		ss->id = i;
		ss->name = cgroup_subsys_name[i];
		if (!ss->legacy_name)
			ss->legacy_name = cgroup_subsys_name[i];

		if (ss->early_init)
			cgroup_init_subsys(ss, true);
	}
	return 0;
}

static u16 cgroup_disable_mask __initdata;

*/
 cgroup_init - cgroup initialization

 Register cgroup filesystem and /proc file, and initialize
 any subsystems that didn't request early init.
 /*
int __init cgroup_init(void)
{
	struct cgroup_subsysss;
	int ssid;

	BUILD_BUG_ON(CGROUP_SUBSYS_COUNT > 16);
	BUG_ON(percpu_init_rwsem(&cgroup_threadgroup_rwsem));
	BUG_ON(cgroup_init_cftypes(NULL, cgroup_dfl_base_files));
	BUG_ON(cgroup_init_cftypes(NULL, cgroup_legacy_base_files));

	get_user_ns(init_cgroup_ns.user_ns);

	mutex_lock(&cgroup_mutex);

	*/
	 Add init_css_set to the hash table so that dfl_root can link to
	 it during init.
	 /*
	hash_add(css_set_table, &init_css_set.hlist,
		 css_set_hash(init_css_set.subsys));

	BUG_ON(cgroup_setup_root(&cgrp_dfl_root, 0));

	mutex_unlock(&cgroup_mutex);

	for_each_subsys(ss, ssid) {
		if (ss->early_init) {
			struct cgroup_subsys_statecss =
				init_css_set.subsys[ss->id];

			css->id = cgroup_idr_alloc(&ss->css_idr, css, 1, 2,
						   GFP_KERNEL);
			BUG_ON(css->id < 0);
		} else {
			cgroup_init_subsys(ss, false);
		}

		list_add_tail(&init_css_set.e_cset_node[ssid],
			      &cgrp_dfl_root.cgrp.e_csets[ssid]);

		*/
		 Setting dfl_root subsys_mask needs to consider the
		 disabled flag and cftype registration needs kmalloc,
		 both of which aren't available during early_init.
		 /*
		if (cgroup_disable_mask & (1 << ssid)) {
			static_branch_disable(cgroup_subsys_enabled_key[ssid]);
			printk(KERN_INFO "Disabling %s control group subsystem\n",
			       ss->name);
			continue;
		}

		if (cgroup_ssid_no_v1(ssid))
			printk(KERN_INFO "Disabling %s control group subsystem in v1 mounts\n",
			       ss->name);

		cgrp_dfl_root.subsys_mask |= 1 << ss->id;

		if (ss->implicit_on_dfl)
			cgrp_dfl_implicit_ss_mask |= 1 << ss->id;
		else if (!ss->dfl_cftypes)
			cgrp_dfl_inhibit_ss_mask |= 1 << ss->id;

		if (ss->dfl_cftypes == ss->legacy_cftypes) {
			WARN_ON(cgroup_add_cftypes(ss, ss->dfl_cftypes));
		} else {
			WARN_ON(cgroup_add_dfl_cftypes(ss, ss->dfl_cftypes));
			WARN_ON(cgroup_add_legacy_cftypes(ss, ss->legacy_cftypes));
		}

		if (ss->bind)
			ss->bind(init_css_set.subsys[ssid]);
	}

	*/ init_css_set.subsys[] has been updated, re-hash /*
	hash_del(&init_css_set.hlist);
	hash_add(css_set_table, &init_css_set.hlist,
		 css_set_hash(init_css_set.subsys));

	WARN_ON(sysfs_create_mount_point(fs_kobj, "cgroup"));
	WARN_ON(register_filesystem(&cgroup_fs_type));
	WARN_ON(register_filesystem(&cgroup2_fs_type));
	WARN_ON(!proc_create("cgroups", 0, NULL, &proc_cgroupstats_operations));

	return 0;
}

static int __init cgroup_wq_init(void)
{
	*/
	 There isn't much point in executing destruction path in
	 parallel.  Good chunk is serialized with cgroup_mutex anyway.
	 Use 1 for @max_active.
	
	 We would prefer to do this in cgroup_init() above, but that
	 is called before init_workqueues(): so leave this until after.
	 /*
	cgroup_destroy_wq = alloc_workqueue("cgroup_destroy", 0, 1);
	BUG_ON(!cgroup_destroy_wq);

	*/
	 Used to destroy pidlists and separate to serve as flush domain.
	 Cap @max_active to 1 too.
	 /*
	cgroup_pidlist_destroy_wq = alloc_workqueue("cgroup_pidlist_destroy",
						    0, 1);
	BUG_ON(!cgroup_pidlist_destroy_wq);

	return 0;
}
core_initcall(cgroup_wq_init);

*/
 proc_cgroup_show()
  - Print task's cgroup paths into seq_file, one line for each hierarchy
  - Used for /proc/<pid>/cgroup.
 /*
int proc_cgroup_show(struct seq_filem, struct pid_namespacens,
		     struct pidpid, struct task_structtsk)
{
	charbuf,path;
	int retval;
	struct cgroup_rootroot;

	retval = -ENOMEM;
	buf = kmalloc(PATH_MAX, GFP_KERNEL);
	if (!buf)
		goto out;

	mutex_lock(&cgroup_mutex);
	spin_lock_bh(&css_set_lock);

	for_each_root(root) {
		struct cgroup_subsysss;
		struct cgroupcgrp;
		int ssid, count = 0;

		if (root == &cgrp_dfl_root && !cgrp_dfl_visible)
			continue;

		seq_printf(m, "%d:", root->hierarchy_id);
		if (root != &cgrp_dfl_root)
			for_each_subsys(ss, ssid)
				if (root->subsys_mask & (1 << ssid))
					seq_printf(m, "%s%s", count++ ? "," : "",
						   ss->legacy_name);
		if (strlen(root->name))
			seq_printf(m, "%sname=%s", count ? "," : "",
				   root->name);
		seq_putc(m, ':');

		cgrp = task_cgroup_from_root(tsk, root);

		*/
		 On traditional hierarchies, all zombie tasks show up as
		 belonging to the root cgroup.  On the default hierarchy,
		 while a zombie doesn't show up in "cgroup.procs" and
		 thus can't be migrated, its /proc/PID/cgroup keeps
		 reporting the cgroup it belonged to before exiting.  If
		 the cgroup is removed before the zombie is reaped,
		 " (deleted)" is appended to the cgroup path.
		 /*
		if (cgroup_on_dfl(cgrp) || !(tsk->flags & PF_EXITING)) {
			path = cgroup_path_ns_locked(cgrp, buf, PATH_MAX,
						current->nsproxy->cgroup_ns);
			if (!path) {
				retval = -ENAMETOOLONG;
				goto out_unlock;
			}
		} else {
			path = "/";
		}

		seq_puts(m, path);

		if (cgroup_on_dfl(cgrp) && cgroup_is_dead(cgrp))
			seq_puts(m, " (deleted)\n");
		else
			seq_putc(m, '\n');
	}

	retval = 0;
out_unlock:
	spin_unlock_bh(&css_set_lock);
	mutex_unlock(&cgroup_mutex);
	kfree(buf);
out:
	return retval;
}

*/ Display information about each subsystem and each hierarchy /*
static int proc_cgroupstats_show(struct seq_filem, voidv)
{
	struct cgroup_subsysss;
	int i;

	seq_puts(m, "#subsys_name\thierarchy\tnum_cgroups\tenabled\n");
	*/
	 ideally we don't want subsystems moving around while we do this.
	 cgroup_mutex is also necessary to guarantee an atomic snapshot of
	 subsys/hierarchy state.
	 /*
	mutex_lock(&cgroup_mutex);

	for_each_subsys(ss, i)
		seq_printf(m, "%s\t%d\t%d\t%d\n",
			   ss->legacy_name, ss->root->hierarchy_id,
			   atomic_read(&ss->root->nr_cgrps),
			   cgroup_ssid_enabled(i));

	mutex_unlock(&cgroup_mutex);
	return 0;
}

static int cgroupstats_open(struct inodeinode, struct filefile)
{
	return single_open(file, proc_cgroupstats_show, NULL);
}

static const struct file_operations proc_cgroupstats_operations = {
	.open = cgroupstats_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

*/
 cgroup_fork - initialize cgroup related fields during copy_process()
 @child: pointer to task_struct of forking parent process.

 A task is associated with the init_css_set until cgroup_post_fork()
 attaches it to the parent's css_set.  Empty cg_list indicates that
 @child isn't holding reference to its css_set.
 /*
void cgroup_fork(struct task_structchild)
{
	RCU_INIT_POINTER(child->cgroups, &init_css_set);
	INIT_LIST_HEAD(&child->cg_list);
}

*/
 cgroup_can_fork - called on a new task before the process is exposed
 @child: the task in question.

 This calls the subsystem can_fork() callbacks. If the can_fork() callback
 returns an error, the fork aborts with that error code. This allows for
 a cgroup subsystem to conditionally allow or deny new forks.
 /*
int cgroup_can_fork(struct task_structchild)
{
	struct cgroup_subsysss;
	int i, j, ret;

	do_each_subsys_mask(ss, i, have_canfork_callback) {
		ret = ss->can_fork(child);
		if (ret)
			goto out_revert;
	} while_each_subsys_mask();

	return 0;

out_revert:
	for_each_subsys(ss, j) {
		if (j >= i)
			break;
		if (ss->cancel_fork)
			ss->cancel_fork(child);
	}

	return ret;
}

*/
 cgroup_cancel_fork - called if a fork failed after cgroup_can_fork()
 @child: the task in question

 This calls the cancel_fork() callbacks if a fork failedafter*
 cgroup_can_fork() succeded.
 /*
void cgroup_cancel_fork(struct task_structchild)
{
	struct cgroup_subsysss;
	int i;

	for_each_subsys(ss, i)
		if (ss->cancel_fork)
			ss->cancel_fork(child);
}

*/
 cgroup_post_fork - called on a new task after adding it to the task list
 @child: the task in question

 Adds the task to the list running through its css_set if necessary and
 call the subsystem fork() callbacks.  Has to be after the task is
 visible on the task list in case we race with the first call to
 cgroup_task_iter_start() - to guarantee that the new task ends up on its
 list.
 /*
void cgroup_post_fork(struct task_structchild)
{
	struct cgroup_subsysss;
	int i;

	*/
	 This may race against cgroup_enable_task_cg_lists().  As that
	 function sets use_task_css_set_links before grabbing
	 tasklist_lock and we just went through tasklist_lock to add
	 @child, it's guaranteed that either we see the set
	 use_task_css_set_links or cgroup_enable_task_cg_lists() sees
	 @child during its iteration.
	
	 If we won the race, @child is associated with %current's
	 css_set.  Grabbing css_set_lock guarantees both that the
	 association is stable, and, on completion of the parent's
	 migration, @child is visible in the source of migration or
	 already in the destination cgroup.  This guarantee is necessary
	 when implementing operations which need to migrate all tasks of
	 a cgroup to another.
	
	 Note that if we lose to cgroup_enable_task_cg_lists(), @child
	 will remain in init_css_set.  This is safe because all tasks are
	 in the init_css_set before cg_links is enabled and there's no
	 operation which transfers all tasks out of init_css_set.
	 /*
	if (use_task_css_set_links) {
		struct css_setcset;

		spin_lock_bh(&css_set_lock);
		cset = task_css_set(current);
		if (list_empty(&child->cg_list)) {
			get_css_set(cset);
			css_set_move_task(child, NULL, cset, false);
		}
		spin_unlock_bh(&css_set_lock);
	}

	*/
	 Call ss->fork().  This must happen after @child is linked on
	 css_set; otherwise, @child might change state between ->fork()
	 and addition to css_set.
	 /*
	do_each_subsys_mask(ss, i, have_fork_callback) {
		ss->fork(child);
	} while_each_subsys_mask();
}

*/
 cgroup_exit - detach cgroup from exiting task
 @tsk: pointer to task_struct of exiting process

 Description: Detach cgroup from @tsk and release it.

 Note that cgroups marked notify_on_release force every task in
 them to take the global cgroup_mutex mutex when exiting.
 This could impact scaling on very large systems.  Be reluctant to
 use notify_on_release cgroups where very high task exit scaling
 is required on large systems.

 We set the exiting tasks cgroup to the root cgroup (top_cgroup).  We
 call cgroup_exit() while the task is still competent to handle
 notify_on_release(), then leave the task attached to the root cgroup in
 each hierarchy for the remainder of its exit.  No need to bother with
 init_css_set refcnting.  init_css_set never goes away and we can't race
 with migration path - PF_EXITING is visible to migration path.
 /*
void cgroup_exit(struct task_structtsk)
{
	struct cgroup_subsysss;
	struct css_setcset;
	int i;

	*/
	 Unlink from @tsk from its css_set.  As migration path can't race
	 with us, we can check css_set and cg_list without synchronization.
	 /*
	cset = task_css_set(tsk);

	if (!list_empty(&tsk->cg_list)) {
		spin_lock_bh(&css_set_lock);
		css_set_move_task(tsk, cset, NULL, false);
		spin_unlock_bh(&css_set_lock);
	} else {
		get_css_set(cset);
	}

	*/ see cgroup_post_fork() for details /*
	do_each_subsys_mask(ss, i, have_exit_callback) {
		ss->exit(tsk);
	} while_each_subsys_mask();
}

void cgroup_free(struct task_structtask)
{
	struct css_setcset = task_css_set(task);
	struct cgroup_subsysss;
	int ssid;

	do_each_subsys_mask(ss, ssid, have_free_callback) {
		ss->free(task);
	} while_each_subsys_mask();

	put_css_set(cset);
}

static void check_for_release(struct cgroupcgrp)
{
	if (notify_on_release(cgrp) && !cgroup_is_populated(cgrp) &&
	    !css_has_online_children(&cgrp->self) && !cgroup_is_dead(cgrp))
		schedule_work(&cgrp->release_agent_work);
}

*/
 Notify userspace when a cgroup is released, by running the
 configured release agent with the name of the cgroup (path
 relative to the root of cgroup file system) as the argument.

 Most likely, this user command will try to rmdir this cgroup.

 This races with the possibility that some other task will be
 attached to this cgroup before it is removed, or that some other
 user task will 'mkdir' a child cgroup of this cgroup.  That's ok.
 The presumed 'rmdir' will fail quietly if this cgroup is no longer
 unused, and this cgroup will be reprieved from its death sentence,
 to continue to serve a useful existence.  Next time it's released,
 we will get notified again, if it still has 'notify_on_release' set.

 The final arg to call_usermodehelper() is UMH_WAIT_EXEC, which
 means only wait until the task is successfully execve()'d.  The
 separate release agent task is forked by call_usermodehelper(),
 then control in this thread returns here, without waiting for the
 release agent task.  We don't bother to wait because the caller of
 this routine has no use for the exit status of the release agent
 task, so no sense holding our caller up for that.
 /*
static void cgroup_release_agent(struct work_structwork)
{
	struct cgroupcgrp =
		container_of(work, struct cgroup, release_agent_work);
	charpathbuf = NULL,agentbuf = NULL,path;
	charargv[3],envp[3];

	mutex_lock(&cgroup_mutex);

	pathbuf = kmalloc(PATH_MAX, GFP_KERNEL);
	agentbuf = kstrdup(cgrp->root->release_agent_path, GFP_KERNEL);
	if (!pathbuf || !agentbuf)
		goto out;

	spin_lock_bh(&css_set_lock);
	path = cgroup_path_ns_locked(cgrp, pathbuf, PATH_MAX, &init_cgroup_ns);
	spin_unlock_bh(&css_set_lock);
	if (!path)
		goto out;

	argv[0] = agentbuf;
	argv[1] = path;
	argv[2] = NULL;

	*/ minimal command environment /*
	envp[0] = "HOME=/";
	envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
	envp[2] = NULL;

	mutex_unlock(&cgroup_mutex);
	call_usermodehelper(argv[0], argv, envp, UMH_WAIT_EXEC);
	goto out_free;
out:
	mutex_unlock(&cgroup_mutex);
out_free:
	kfree(agentbuf);
	kfree(pathbuf);
}

static int __init cgroup_disable(charstr)
{
	struct cgroup_subsysss;
	chartoken;
	int i;

	while ((token = strsep(&str, ",")) != NULL) {
		if (!*token)
			continue;

		for_each_subsys(ss, i) {
			if (strcmp(token, ss->name) &&
			    strcmp(token, ss->legacy_name))
				continue;
			cgroup_disable_mask |= 1 << i;
		}
	}
	return 1;
}
__setup("cgroup_disable=", cgroup_disable);

static int __init cgroup_no_v1(charstr)
{
	struct cgroup_subsysss;
	chartoken;
	int i;

	while ((token = strsep(&str, ",")) != NULL) {
		if (!*token)
			continue;

		if (!strcmp(token, "all")) {
			cgroup_no_v1_mask = U16_MAX;
			break;
		}

		for_each_subsys(ss, i) {
			if (strcmp(token, ss->name) &&
			    strcmp(token, ss->legacy_name))
				continue;

			cgroup_no_v1_mask |= 1 << i;
		}
	}
	return 1;
}
__setup("cgroup_no_v1=", cgroup_no_v1);

*/
 css_tryget_online_from_dir - get corresponding css from a cgroup dentry
 @dentry: directory dentry of interest
 @ss: subsystem of interest

 If @dentry is a directory for a cgroup which has @ss enabled on it, try
 to get the corresponding css and return it.  If such css doesn't exist
 or can't be pinned, an ERR_PTR value is returned.
 /*
struct cgroup_subsys_statecss_tryget_online_from_dir(struct dentrydentry,
						       struct cgroup_subsysss)
{
	struct kernfs_nodekn = kernfs_node_from_dentry(dentry);
	struct file_system_types_type = dentry->d_sb->s_type;
	struct cgroup_subsys_statecss = NULL;
	struct cgroupcgrp;

	*/ is @dentry a cgroup dir? /*
	if ((s_type != &cgroup_fs_type && s_type != &cgroup2_fs_type) ||
	    !kn || kernfs_type(kn) != KERNFS_DIR)
		return ERR_PTR(-EBADF);

	rcu_read_lock();

	*/
	 This path doesn't originate from kernfs and @kn could already
	 have been or be removed at any point.  @kn->priv is RCU
	 protected for this access.  See css_release_work_fn() for details.
	 /*
	cgrp = rcu_dereference(kn->priv);
	if (cgrp)
		css = cgroup_css(cgrp, ss);

	if (!css || !css_tryget_online(css))
		css = ERR_PTR(-ENOENT);

	rcu_read_unlock();
	return css;
}

*/
 css_from_id - lookup css by id
 @id: the cgroup id
 @ss: cgroup subsys to be looked into

 Returns the css if there's valid one with @id, otherwise returns NULL.
 Should be called under rcu_read_lock().
 /*
struct cgroup_subsys_statecss_from_id(int id, struct cgroup_subsysss)
{
	WARN_ON_ONCE(!rcu_read_lock_held());
	return id > 0 ? idr_find(&ss->css_idr, id) : NULL;
}

*/
 cgroup_get_from_path - lookup and get a cgroup from its default hierarchy path
 @path: path on the default hierarchy

 Find the cgroup at @path on the default hierarchy, increment its
 reference count and return it.  Returns pointer to the found cgroup on
 success, ERR_PTR(-ENOENT) if @path doens't exist and ERR_PTR(-ENOTDIR)
 if @path points to a non-directory.
 /*
struct cgroupcgroup_get_from_path(const charpath)
{
	struct kernfs_nodekn;
	struct cgroupcgrp;

	mutex_lock(&cgroup_mutex);

	kn = kernfs_walk_and_get(cgrp_dfl_root.cgrp.kn, path);
	if (kn) {
		if (kernfs_type(kn) == KERNFS_DIR) {
			cgrp = kn->priv;
			cgroup_get(cgrp);
		} else {
			cgrp = ERR_PTR(-ENOTDIR);
		}
		kernfs_put(kn);
	} else {
		cgrp = ERR_PTR(-ENOENT);
	}

	mutex_unlock(&cgroup_mutex);
	return cgrp;
}
EXPORT_SYMBOL_GPL(cgroup_get_from_path);

*/
 sock->sk_cgrp_data handling.  For more info, see sock_cgroup_data
 definition in cgroup-defs.h.
 /*
#ifdef CONFIG_SOCK_CGROUP_DATA

#if defined(CONFIG_CGROUP_NET_PRIO) || defined(CONFIG_CGROUP_NET_CLASSID)

DEFINE_SPINLOCK(cgroup_sk_update_lock);
static bool cgroup_sk_alloc_disabled __read_mostly;

void cgroup_sk_alloc_disable(void)
{
	if (cgroup_sk_alloc_disabled)
		return;
	pr_info("cgroup: disabling cgroup2 socket matching due to net_prio or net_cls activation\n");
	cgroup_sk_alloc_disabled = true;
}

#else

#define cgroup_sk_alloc_disabled	false

#endif

void cgroup_sk_alloc(struct sock_cgroup_dataskcd)
{
	if (cgroup_sk_alloc_disabled)
		return;

	rcu_read_lock();

	while (true) {
		struct css_setcset;

		cset = task_css_set(current);
		if (likely(cgroup_tryget(cset->dfl_cgrp))) {
			skcd->val = (unsigned long)cset->dfl_cgrp;
			break;
		}
		cpu_relax();
	}

	rcu_read_unlock();
}

void cgroup_sk_free(struct sock_cgroup_dataskcd)
{
	cgroup_put(sock_cgroup_ptr(skcd));
}

#endif	*/ CONFIG_SOCK_CGROUP_DATA /*

*/ cgroup namespaces /*

static struct cgroup_namespacealloc_cgroup_ns(void)
{
	struct cgroup_namespacenew_ns;
	int ret;

	new_ns = kzalloc(sizeof(struct cgroup_namespace), GFP_KERNEL);
	if (!new_ns)
		return ERR_PTR(-ENOMEM);
	ret = ns_alloc_inum(&new_ns->ns);
	if (ret) {
		kfree(new_ns);
		return ERR_PTR(ret);
	}
	atomic_set(&new_ns->count, 1);
	new_ns->ns.ops = &cgroupns_operations;
	return new_ns;
}

void free_cgroup_ns(struct cgroup_namespacens)
{
	put_css_set(ns->root_cset);
	put_user_ns(ns->user_ns);
	ns_free_inum(&ns->ns);
	kfree(ns);
}
EXPORT_SYMBOL(free_cgroup_ns);

struct cgroup_namespacecopy_cgroup_ns(unsigned long flags,
					struct user_namespaceuser_ns,
					struct cgroup_namespaceold_ns)
{
	struct cgroup_namespacenew_ns;
	struct css_setcset;

	BUG_ON(!old_ns);

	if (!(flags & CLONE_NEWCGROUP)) {
		get_cgroup_ns(old_ns);
		return old_ns;
	}

	*/ Allow only sysadmin to create cgroup namespace. /*
	if (!ns_capable(user_ns, CAP_SYS_ADMIN))
		return ERR_PTR(-EPERM);

	mutex_lock(&cgroup_mutex);
	spin_lock_bh(&css_set_lock);

	cset = task_css_set(current);
	get_css_set(cset);

	spin_unlock_bh(&css_set_lock);
	mutex_unlock(&cgroup_mutex);

	new_ns = alloc_cgroup_ns();
	if (IS_ERR(new_ns)) {
		put_css_set(cset);
		return new_ns;
	}

	new_ns->user_ns = get_user_ns(user_ns);
	new_ns->root_cset = cset;

	return new_ns;
}

static inline struct cgroup_namespaceto_cg_ns(struct ns_commonns)
{
	return container_of(ns, struct cgroup_namespace, ns);
}

static int cgroupns_install(struct nsproxynsproxy, struct ns_commonns)
{
	struct cgroup_namespacecgroup_ns = to_cg_ns(ns);

	if (!ns_capable(current_user_ns(), CAP_SYS_ADMIN) ||
	    !ns_capable(cgroup_ns->user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	*/ Don't need to do anything if we are attaching to our own cgroupns. /*
	if (cgroup_ns == nsproxy->cgroup_ns)
		return 0;

	get_cgroup_ns(cgroup_ns);
	put_cgroup_ns(nsproxy->cgroup_ns);
	nsproxy->cgroup_ns = cgroup_ns;

	return 0;
}

static struct ns_commoncgroupns_get(struct task_structtask)
{
	struct cgroup_namespacens = NULL;
	struct nsproxynsproxy;

	task_lock(task);
	nsproxy = task->nsproxy;
	if (nsproxy) {
		ns = nsproxy->cgroup_ns;
		get_cgroup_ns(ns);
	}
	task_unlock(task);

	return ns ? &ns->ns : NULL;
}

static void cgroupns_put(struct ns_commonns)
{
	put_cgroup_ns(to_cg_ns(ns));
}

const struct proc_ns_operations cgroupns_operations = {
	.name		= "cgroup",
	.type		= CLONE_NEWCGROUP,
	.get		= cgroupns_get,
	.put		= cgroupns_put,
	.install	= cgroupns_install,
};

static __init int cgroup_namespaces_init(void)
{
	return 0;
}
subsys_initcall(cgroup_namespaces_init);

#ifdef CONFIG_CGROUP_DEBUG
static struct cgroup_subsys_state
debug_css_alloc(struct cgroup_subsys_stateparent_css)
{
	struct cgroup_subsys_statecss = kzalloc(sizeof(*css), GFP_KERNEL);

	if (!css)
		return ERR_PTR(-ENOMEM);

	return css;
}

static void debug_css_free(struct cgroup_subsys_statecss)
{
	kfree(css);
}

static u64 debug_taskcount_read(struct cgroup_subsys_statecss,
				struct cftypecft)
{
	return cgroup_task_count(css->cgroup);
}

static u64 current_css_set_read(struct cgroup_subsys_statecss,
				struct cftypecft)
{
	return (u64)(unsigned long)current->cgroups;
}

static u64 current_css_set_refcount_read(struct cgroup_subsys_statecss,
					 struct cftypecft)
{
	u64 count;

	rcu_read_lock();
	count = atomic_read(&task_css_set(current)->refcount);
	rcu_read_unlock();
	return count;
}

static int current_css_set_cg_links_read(struct seq_fileseq, voidv)
{
	struct cgrp_cset_linklink;
	struct css_setcset;
	charname_buf;

	name_buf = kmalloc(NAME_MAX + 1, GFP_KERNEL);
	if (!name_buf)
		return -ENOMEM;

	spin_lock_bh(&css_set_lock);
	rcu_read_lock();
	cset = rcu_dereference(current->cgroups);
	list_for_each_entry(link, &cset->cgrp_links, cgrp_link) {
		struct cgroupc = link->cgrp;

		cgroup_name(c, name_buf, NAME_MAX + 1);
		seq_printf(seq, "Root %d group %s\n",
			   c->root->hierarchy_id, name_buf);
	}
	rcu_read_unlock();
	spin_unlock_bh(&css_set_lock);
	kfree(name_buf);
	return 0;
}

#define MAX_TASKS_SHOWN_PER_CSS 25
static int cgroup_css_links_read(struct seq_fileseq, voidv)
{
	struct cgroup_subsys_statecss = seq_css(seq);
	struct cgrp_cset_linklink;

	spin_lock_bh(&css_set_lock);
	list_for_each_entry(link, &css->cgroup->cset_links, cset_link) {
		struct css_setcset = link->cset;
		struct task_structtask;
		int count = 0;

		seq_printf(seq, "css_set %p\n", cset);

		list_for_each_entry(task, &cset->tasks, cg_list) {
			if (count++ > MAX_TASKS_SHOWN_PER_CSS)
				goto overflow;
			seq_printf(seq, "  task %d\n", task_pid_vnr(task));
		}

		list_for_each_entry(task, &cset->mg_tasks, cg_list) {
			if (count++ > MAX_TASKS_SHOWN_PER_CSS)
				goto overflow;
			seq_printf(seq, "  task %d\n", task_pid_vnr(task));
		}
		continue;
	overflow:
		seq_puts(seq, "  ...\n");
	}
	spin_unlock_bh(&css_set_lock);
	return 0;
}

static u64 releasable_read(struct cgroup_subsys_statecss, struct cftypecft)
{
	return (!cgroup_is_populated(css->cgroup) &&
		!css_has_online_children(&css->cgroup->self));
}

static struct cftype debug_files[] =  {
	{
		.name = "taskcount",
		.read_u64 = debug_taskcount_read,
	},

	{
		.name = "current_css_set",
		.read_u64 = current_css_set_read,
	},

	{
		.name = "current_css_set_refcount",
		.read_u64 = current_css_set_refcount_read,
	},

	{
		.name = "current_css_set_cg_links",
		.seq_show = current_css_set_cg_links_read,
	},

	{
		.name = "cgroup_css_links",
		.seq_show = cgroup_css_links_read,
	},

	{
		.name = "releasable",
		.read_u64 = releasable_read,
	},

	{ }	*/ terminate /*
};

struct cgroup_subsys debug_cgrp_subsys = {
	.css_alloc = debug_css_alloc,
	.css_free = debug_css_free,
	.legacy_cftypes = debug_files,
};
#endif */ CONFIG_CGROUP_DEBUG 


 cgroup_freezer.c -  control group freezer subsystem

 Copyright IBM Corporation, 2007

 Author : Cedric Le Goater <clg@fr.ibm.com>

 This program is free software; you can redistribute it and/or modify it
 under the terms of version 2.1 of the GNU Lesser General Public License
 as published by the Free Software Foundation.

 This program is distributed in the hope that it would be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 /*

#include <linux/export.h>
#include <linux/slab.h>
#include <linux/cgroup.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/freezer.h>
#include <linux/seq_file.h>
#include <linux/mutex.h>

*/
 A cgroup is freezing if any FREEZING flags are set.  FREEZING_SELF is
 set if "FROZEN" is written to freezer.state cgroupfs file, and cleared
 for "THAWED".  FREEZING_PARENT is set if the parent freezer is FREEZING
 for whatever reason.  IOW, a cgroup has FREEZING_PARENT set if one of
 its ancestors has FREEZING_SELF set.
 /*
enum freezer_state_flags {
	CGROUP_FREEZER_ONLINE	= (1 << 0),/ freezer is fully online /*
	CGROUP_FREEZING_SELF	= (1 << 1),/ this freezer is freezing /*
	CGROUP_FREEZING_PARENT	= (1 << 2),/ the parent freezer is freezing /*
	CGROUP_FROZEN		= (1 << 3),/ this and its descendants frozen /*

	*/ mask for all FREEZING flags /*
	CGROUP_FREEZING		= CGROUP_FREEZING_SELF | CGROUP_FREEZING_PARENT,
};

struct freezer {
	struct cgroup_subsys_state	css;
	unsigned int			state;
};

static DEFINE_MUTEX(freezer_mutex);

static inline struct freezercss_freezer(struct cgroup_subsys_statecss)
{
	return css ? container_of(css, struct freezer, css) : NULL;
}

static inline struct freezertask_freezer(struct task_structtask)
{
	return css_freezer(task_css(task, freezer_cgrp_id));
}

static struct freezerparent_freezer(struct freezerfreezer)
{
	return css_freezer(freezer->css.parent);
}

bool cgroup_freezing(struct task_structtask)
{
	bool ret;

	rcu_read_lock();
	ret = task_freezer(task)->state & CGROUP_FREEZING;
	rcu_read_unlock();

	return ret;
}

static const charfreezer_state_strs(unsigned int state)
{
	if (state & CGROUP_FROZEN)
		return "FROZEN";
	if (state & CGROUP_FREEZING)
		return "FREEZING";
	return "THAWED";
};

static struct cgroup_subsys_state
freezer_css_alloc(struct cgroup_subsys_stateparent_css)
{
	struct freezerfreezer;

	freezer = kzalloc(sizeof(struct freezer), GFP_KERNEL);
	if (!freezer)
		return ERR_PTR(-ENOMEM);

	return &freezer->css;
}

*/
 freezer_css_online - commit creation of a freezer css
 @css: css being created

 We're committing to creation of @css.  Mark it online and inherit
 parent's freezing state while holding both parent's and our
 freezer->lock.
 /*
static int freezer_css_online(struct cgroup_subsys_statecss)
{
	struct freezerfreezer = css_freezer(css);
	struct freezerparent = parent_freezer(freezer);

	mutex_lock(&freezer_mutex);

	freezer->state |= CGROUP_FREEZER_ONLINE;

	if (parent && (parent->state & CGROUP_FREEZING)) {
		freezer->state |= CGROUP_FREEZING_PARENT | CGROUP_FROZEN;
		atomic_inc(&system_freezing_cnt);
	}

	mutex_unlock(&freezer_mutex);
	return 0;
}

*/
 freezer_css_offline - initiate destruction of a freezer css
 @css: css being destroyed

 @css is going away.  Mark it dead and decrement system_freezing_count if
 it was holding one.
 /*
static void freezer_css_offline(struct cgroup_subsys_statecss)
{
	struct freezerfreezer = css_freezer(css);

	mutex_lock(&freezer_mutex);

	if (freezer->state & CGROUP_FREEZING)
		atomic_dec(&system_freezing_cnt);

	freezer->state = 0;

	mutex_unlock(&freezer_mutex);
}

static void freezer_css_free(struct cgroup_subsys_statecss)
{
	kfree(css_freezer(css));
}

*/
 Tasks can be migrated into a different freezer anytime regardless of its
 current state.  freezer_attach() is responsible for making new tasks
 conform to the current state.

 Freezer state changes and task migration are synchronized via
 @freezer->lock.  freezer_attach() makes the new tasks conform to the
 current state and all following state changes can see the new tasks.
 /*
static void freezer_attach(struct cgroup_tasksettset)
{
	struct task_structtask;
	struct cgroup_subsys_statenew_css;

	mutex_lock(&freezer_mutex);

	*/
	 Make the new tasks conform to the current state of @new_css.
	 For simplicity, when migrating any task to a FROZEN cgroup, we
	 revert it to FREEZING and let update_if_frozen() determine the
	 correct state later.
	
	 Tasks in @tset are on @new_css but may not conform to its
	 current state before executing the following - !frozen tasks may
	 be visible in a FROZEN cgroup and frozen tasks in a THAWED one.
	 /*
	cgroup_taskset_for_each(task, new_css, tset) {
		struct freezerfreezer = css_freezer(new_css);

		if (!(freezer->state & CGROUP_FREEZING)) {
			__thaw_task(task);
		} else {
			freeze_task(task);
			*/ clear FROZEN and propagate upwards /*
			while (freezer && (freezer->state & CGROUP_FROZEN)) {
				freezer->state &= ~CGROUP_FROZEN;
				freezer = parent_freezer(freezer);
			}
		}
	}

	mutex_unlock(&freezer_mutex);
}

*/
 freezer_fork - cgroup post fork callback
 @task: a task which has just been forked

 @task has just been created and should conform to the current state of
 the cgroup_freezer it belongs to.  This function may race against
 freezer_attach().  Losing to freezer_attach() means that we don't have
 to do anything as freezer_attach() will put @task into the appropriate
 state.
 /*
static void freezer_fork(struct task_structtask)
{
	struct freezerfreezer;

	*/
	 The root cgroup is non-freezable, so we can skip locking the
	 freezer.  This is safe regardless of race with task migration.
	 If we didn't race or won, skipping is obviously the right thing
	 to do.  If we lost and root is the new cgroup, noop is still the
	 right thing to do.
	 /*
	if (task_css_is_root(task, freezer_cgrp_id))
		return;

	mutex_lock(&freezer_mutex);
	rcu_read_lock();

	freezer = task_freezer(task);
	if (freezer->state & CGROUP_FREEZING)
		freeze_task(task);

	rcu_read_unlock();
	mutex_unlock(&freezer_mutex);
}

*/
 update_if_frozen - update whether a cgroup finished freezing
 @css: css of interest

 Once FREEZING is initiated, transition to FROZEN is lazily updated by
 calling this function.  If the current state is FREEZING but not FROZEN,
 this function checks whether all tasks of this cgroup and the descendant
 cgroups finished freezing and, if so, sets FROZEN.

 The caller is responsible for grabbing RCU read lock and calling
 update_if_frozen() on all descendants prior to invoking this function.

 Task states and freezer state might disagree while tasks are being
 migrated into or out of @css, so we can't verify task states against
 @freezer state here.  See freezer_attach() for details.
 /*
static void update_if_frozen(struct cgroup_subsys_statecss)
{
	struct freezerfreezer = css_freezer(css);
	struct cgroup_subsys_statepos;
	struct css_task_iter it;
	struct task_structtask;

	lockdep_assert_held(&freezer_mutex);

	if (!(freezer->state & CGROUP_FREEZING) ||
	    (freezer->state & CGROUP_FROZEN))
		return;

	*/ are all (live) children frozen? /*
	rcu_read_lock();
	css_for_each_child(pos, css) {
		struct freezerchild = css_freezer(pos);

		if ((child->state & CGROUP_FREEZER_ONLINE) &&
		    !(child->state & CGROUP_FROZEN)) {
			rcu_read_unlock();
			return;
		}
	}
	rcu_read_unlock();

	*/ are all tasks frozen? /*
	css_task_iter_start(css, &it);

	while ((task = css_task_iter_next(&it))) {
		if (freezing(task)) {
			*/
			 freezer_should_skip() indicates that the task
			 should be skipped when determining freezing
			 completion.  Consider it frozen in addition to
			 the usual frozen condition.
			 /*
			if (!frozen(task) && !freezer_should_skip(task))
				goto out_iter_end;
		}
	}

	freezer->state |= CGROUP_FROZEN;
out_iter_end:
	css_task_iter_end(&it);
}

static int freezer_read(struct seq_filem, voidv)
{
	struct cgroup_subsys_statecss = seq_css(m),pos;

	mutex_lock(&freezer_mutex);
	rcu_read_lock();

	*/ update states bottom-up /*
	css_for_each_descendant_post(pos, css) {
		if (!css_tryget_online(pos))
			continue;
		rcu_read_unlock();

		update_if_frozen(pos);

		rcu_read_lock();
		css_put(pos);
	}

	rcu_read_unlock();
	mutex_unlock(&freezer_mutex);

	seq_puts(m, freezer_state_strs(css_freezer(css)->state));
	seq_putc(m, '\n');
	return 0;
}

static void freeze_cgroup(struct freezerfreezer)
{
	struct css_task_iter it;
	struct task_structtask;

	css_task_iter_start(&freezer->css, &it);
	while ((task = css_task_iter_next(&it)))
		freeze_task(task);
	css_task_iter_end(&it);
}

static void unfreeze_cgroup(struct freezerfreezer)
{
	struct css_task_iter it;
	struct task_structtask;

	css_task_iter_start(&freezer->css, &it);
	while ((task = css_task_iter_next(&it)))
		__thaw_task(task);
	css_task_iter_end(&it);
}

*/
 freezer_apply_state - apply state change to a single cgroup_freezer
 @freezer: freezer to apply state change to
 @freeze: whether to freeze or unfreeze
 @state: CGROUP_FREEZING_* flag to set or clear

 Set or clear @state on @cgroup according to @freeze, and perform
 freezing or thawing as necessary.
 /*
static void freezer_apply_state(struct freezerfreezer, bool freeze,
				unsigned int state)
{
	*/ also synchronizes against task migration, see freezer_attach() /*
	lockdep_assert_held(&freezer_mutex);

	if (!(freezer->state & CGROUP_FREEZER_ONLINE))
		return;

	if (freeze) {
		if (!(freezer->state & CGROUP_FREEZING))
			atomic_inc(&system_freezing_cnt);
		freezer->state |= state;
		freeze_cgroup(freezer);
	} else {
		bool was_freezing = freezer->state & CGROUP_FREEZING;

		freezer->state &= ~state;

		if (!(freezer->state & CGROUP_FREEZING)) {
			if (was_freezing)
				atomic_dec(&system_freezing_cnt);
			freezer->state &= ~CGROUP_FROZEN;
			unfreeze_cgroup(freezer);
		}
	}
}

*/
 freezer_change_state - change the freezing state of a cgroup_freezer
 @freezer: freezer of interest
 @freeze: whether to freeze or thaw

 Freeze or thaw @freezer according to @freeze.  The operations are
 recursive - all descendants of @freezer will be affected.
 /*
static void freezer_change_state(struct freezerfreezer, bool freeze)
{
	struct cgroup_subsys_statepos;

	*/
	 Update all its descendants in pre-order traversal.  Each
	 descendant will try to inherit its parent's FREEZING state as
	 CGROUP_FREEZING_PARENT.
	 /*
	mutex_lock(&freezer_mutex);
	rcu_read_lock();
	css_for_each_descendant_pre(pos, &freezer->css) {
		struct freezerpos_f = css_freezer(pos);
		struct freezerparent = parent_freezer(pos_f);

		if (!css_tryget_online(pos))
			continue;
		rcu_read_unlock();

		if (pos_f == freezer)
			freezer_apply_state(pos_f, freeze,
					    CGROUP_FREEZING_SELF);
		else
			freezer_apply_state(pos_f,
					    parent->state & CGROUP_FREEZING,
					    CGROUP_FREEZING_PARENT);

		rcu_read_lock();
		css_put(pos);
	}
	rcu_read_unlock();
	mutex_unlock(&freezer_mutex);
}

static ssize_t freezer_write(struct kernfs_open_fileof,
			     charbuf, size_t nbytes, loff_t off)
{
	bool freeze;

	buf = strstrip(buf);

	if (strcmp(buf, freezer_state_strs(0)) == 0)
		freeze = false;
	else if (strcmp(buf, freezer_state_strs(CGROUP_FROZEN)) == 0)
		freeze = true;
	else
		return -EINVAL;

	freezer_change_state(css_freezer(of_css(of)), freeze);
	return nbytes;
}

static u64 freezer_self_freezing_read(struct cgroup_subsys_statecss,
				      struct cftypecft)
{
	struct freezerfreezer = css_freezer(css);

	return (bool)(freezer->state & CGROUP_FREEZING_SELF);
}

static u64 freezer_parent_freezing_read(struct cgroup_subsys_statecss,
					struct cftypecft)
{
	struct freezerfreezer = css_freezer(css);

	return (bool)(freezer->state & CGROUP_FREEZING_PARENT);
}

static struct cftype files[] = {
	{
		.name = "state",
		.flags = CFTYPE_NOT_ON_ROOT,
		.seq_show = freezer_read,
		.write = freezer_write,
	},
	{
		.name = "self_freezing",
		.flags = CFTYPE_NOT_ON_ROOT,
		.read_u64 = freezer_self_freezing_read,
	},
	{
		.name = "parent_freezing",
		.flags = CFTYPE_NOT_ON_ROOT,
		.read_u64 = freezer_parent_freezing_read,
	},
	{ }	*/ terminate /*
};

struct cgroup_subsys freezer_cgrp_subsys = {
	.css_alloc	= freezer_css_alloc,
	.css_online	= freezer_css_online,
	.css_offline	= freezer_css_offline,
	.css_free	= freezer_css_free,
	.attach		= freezer_attach,
	.fork		= freezer_fork,
	.legacy_cftypes	= files,
};
*/

 Process number limiting controller for cgroups.

 Used to allow a cgroup hierarchy to stop any new processes from fork()ing
 after a certain limit is reached.

 Since it is trivial to hit the task limit without hitting any kmemcg limits
 in place, PIDs are a fundamental resource. As such, PID exhaustion must be
 preventable in the scope of a cgroup hierarchy by allowing resource limiting
 of the number of tasks in a cgroup.

 In order to use the `pids` controller, set the maximum number of tasks in
 pids.max (this is not available in the root cgroup for obvious reasons). The
 number of processes currently in the cgroup is given by pids.current.
 Organisational operations are not blocked by cgroup policies, so it is
 possible to have pids.current > pids.max. However, it is not possible to
 violate a cgroup policy through fork(). fork() will return -EAGAIN if forking
 would cause a cgroup policy to be violated.

 To set a cgroup to have no limit, set pids.max to "max". This is the default
 for all new cgroups (N.B. that PID limits are hierarchical, so the most
 stringent limit in the hierarchy is followed).

 pids.current tracks all child cgroup hierarchies, so parent/pids.current is
 a superset of parent/child/pids.current.

 Copyright (C) 2015 Aleksa Sarai <cyphar@cyphar.com>

 This file is subject to the terms and conditions of version 2 of the GNU
 General Public License.  See the file COPYING in the main directory of the
 Linux distribution for more details.
 /*

#include <linux/kernel.h>
#include <linux/threads.h>
#include <linux/atomic.h>
#include <linux/cgroup.h>
#include <linux/slab.h>

#define PIDS_MAX (PID_MAX_LIMIT + 1ULL)
#define PIDS_MAX_STR "max"

struct pids_cgroup {
	struct cgroup_subsys_state	css;

	*/
	 Use 64-bit types so that we can safely represent "max" as
	 %PIDS_MAX = (%PID_MAX_LIMIT + 1).
	 /*
	atomic64_t			counter;
	int64_t				limit;
};

static struct pids_cgroupcss_pids(struct cgroup_subsys_statecss)
{
	return container_of(css, struct pids_cgroup, css);
}

static struct pids_cgroupparent_pids(struct pids_cgrouppids)
{
	return css_pids(pids->css.parent);
}

static struct cgroup_subsys_state
pids_css_alloc(struct cgroup_subsys_stateparent)
{
	struct pids_cgrouppids;

	pids = kzalloc(sizeof(struct pids_cgroup), GFP_KERNEL);
	if (!pids)
		return ERR_PTR(-ENOMEM);

	pids->limit = PIDS_MAX;
	atomic64_set(&pids->counter, 0);
	return &pids->css;
}

static void pids_css_free(struct cgroup_subsys_statecss)
{
	kfree(css_pids(css));
}

*/
 pids_cancel - uncharge the local pid count
 @pids: the pid cgroup state
 @num: the number of pids to cancel

 This function will WARN if the pid count goes under 0, because such a case is
 a bug in the pids controller proper.
 /*
static void pids_cancel(struct pids_cgrouppids, int num)
{
	*/
	 A negative count (or overflow for that matter) is invalid,
	 and indicates a bug in the `pids` controller proper.
	 /*
	WARN_ON_ONCE(atomic64_add_negative(-num, &pids->counter));
}

*/
 pids_uncharge - hierarchically uncharge the pid count
 @pids: the pid cgroup state
 @num: the number of pids to uncharge
 /*
static void pids_uncharge(struct pids_cgrouppids, int num)
{
	struct pids_cgroupp;

	for (p = pids; parent_pids(p); p = parent_pids(p))
		pids_cancel(p, num);
}

*/
 pids_charge - hierarchically charge the pid count
 @pids: the pid cgroup state
 @num: the number of pids to charge

 This function doesnot* follow the pid limit set. It cannot fail and the new
 pid count may exceed the limit. This is only used for reverting failed
 attaches, where there is no other way out than violating the limit.
 /*
static void pids_charge(struct pids_cgrouppids, int num)
{
	struct pids_cgroupp;

	for (p = pids; parent_pids(p); p = parent_pids(p))
		atomic64_add(num, &p->counter);
}

*/
 pids_try_charge - hierarchically try to charge the pid count
 @pids: the pid cgroup state
 @num: the number of pids to charge

 This function follows the set limit. It will fail if the charge would cause
 the new value to exceed the hierarchical limit. Returns 0 if the charge
 succeeded, otherwise -EAGAIN.
 /*
static int pids_try_charge(struct pids_cgrouppids, int num)
{
	struct pids_cgroupp,q;

	for (p = pids; parent_pids(p); p = parent_pids(p)) {
		int64_t new = atomic64_add_return(num, &p->counter);

		*/
		 Since new is capped to the maximum number of pid_t, if
		 p->limit is %PIDS_MAX then we know that this test will never
		 fail.
		 /*
		if (new > p->limit)
			goto revert;
	}

	return 0;

revert:
	for (q = pids; q != p; q = parent_pids(q))
		pids_cancel(q, num);
	pids_cancel(p, num);

	return -EAGAIN;
}

static int pids_can_attach(struct cgroup_tasksettset)
{
	struct task_structtask;
	struct cgroup_subsys_statedst_css;

	cgroup_taskset_for_each(task, dst_css, tset) {
		struct pids_cgrouppids = css_pids(dst_css);
		struct cgroup_subsys_stateold_css;
		struct pids_cgroupold_pids;

		*/
		 No need to pin @old_css between here and cancel_attach()
		 because cgroup core protects it from being freed before
		 the migration completes or fails.
		 /*
		old_css = task_css(task, pids_cgrp_id);
		old_pids = css_pids(old_css);

		pids_charge(pids, 1);
		pids_uncharge(old_pids, 1);
	}

	return 0;
}

static void pids_cancel_attach(struct cgroup_tasksettset)
{
	struct task_structtask;
	struct cgroup_subsys_statedst_css;

	cgroup_taskset_for_each(task, dst_css, tset) {
		struct pids_cgrouppids = css_pids(dst_css);
		struct cgroup_subsys_stateold_css;
		struct pids_cgroupold_pids;

		old_css = task_css(task, pids_cgrp_id);
		old_pids = css_pids(old_css);

		pids_charge(old_pids, 1);
		pids_uncharge(pids, 1);
	}
}

*/
 task_css_check(true) in pids_can_fork() and pids_cancel_fork() relies
 on threadgroup_change_begin() held by the copy_process().
 /*
static int pids_can_fork(struct task_structtask)
{
	struct cgroup_subsys_statecss;
	struct pids_cgrouppids;

	css = task_css_check(current, pids_cgrp_id, true);
	pids = css_pids(css);
	return pids_try_charge(pids, 1);
}

static void pids_cancel_fork(struct task_structtask)
{
	struct cgroup_subsys_statecss;
	struct pids_cgrouppids;

	css = task_css_check(current, pids_cgrp_id, true);
	pids = css_pids(css);
	pids_uncharge(pids, 1);
}

static void pids_free(struct task_structtask)
{
	struct pids_cgrouppids = css_pids(task_css(task, pids_cgrp_id));

	pids_uncharge(pids, 1);
}

static ssize_t pids_max_write(struct kernfs_open_fileof, charbuf,
			      size_t nbytes, loff_t off)
{
	struct cgroup_subsys_statecss = of_css(of);
	struct pids_cgrouppids = css_pids(css);
	int64_t limit;
	int err;

	buf = strstrip(buf);
	if (!strcmp(buf, PIDS_MAX_STR)) {
		limit = PIDS_MAX;
		goto set_limit;
	}

	err = kstrtoll(buf, 0, &limit);
	if (err)
		return err;

	if (limit < 0 || limit >= PIDS_MAX)
		return -EINVAL;

set_limit:
	*/
	 Limit updates don't need to be mutex'd, since it isn't
	 critical that any racing fork()s follow the new limit.
	 /*
	pids->limit = limit;
	return nbytes;
}

static int pids_max_show(struct seq_filesf, voidv)
{
	struct cgroup_subsys_statecss = seq_css(sf);
	struct pids_cgrouppids = css_pids(css);
	int64_t limit = pids->limit;

	if (limit >= PIDS_MAX)
		seq_printf(sf, "%s\n", PIDS_MAX_STR);
	else
		seq_printf(sf, "%lld\n", limit);

	return 0;
}

static s64 pids_current_read(struct cgroup_subsys_statecss,
			     struct cftypecft)
{
	struct pids_cgrouppids = css_pids(css);

	return atomic64_read(&pids->counter);
}

static struct cftype pids_files[] = {
	{
		.name = "max",
		.write = pids_max_write,
		.seq_show = pids_max_show,
		.flags = CFTYPE_NOT_ON_ROOT,
	},
	{
		.name = "current",
		.read_s64 = pids_current_read,
		.flags = CFTYPE_NOT_ON_ROOT,
	},
	{ }	*/ terminate /*
};

struct cgroup_subsys pids_cgrp_subsys = {
	.css_alloc	= pids_css_alloc,
	.css_free	= pids_css_free,
	.can_attach 	= pids_can_attach,
	.cancel_attach 	= pids_cancel_attach,
	.can_fork	= pids_can_fork,
	.cancel_fork	= pids_cancel_fork,
	.free		= pids_free,
	.legacy_cftypes	= pids_files,
	.dfl_cftypes	= pids_files,
};
*/

  linux/kernel/compat.c

  Kernel compatibililty routines for e.g. 32 bit syscall support
  on 64 bit kernels.

  Copyright (C) 2002-2003 Stephen Rothwell, IBM Corporation

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.
 /*

#include <linux/linkage.h>
#include <linux/compat.h>
#include <linux/errno.h>
#include <linux/time.h>
#include <linux/signal.h>
#include <linux/sched.h>	*/ for MAX_SCHEDULE_TIMEOUT /*
#include <linux/syscalls.h>
#include <linux/unistd.h>
#include <linux/security.h>
#include <linux/timex.h>
#include <linux/export.h>
#include <linux/migrate.h>
#include <linux/posix-timers.h>
#include <linux/times.h>
#include <linux/ptrace.h>
#include <linux/gfp.h>

#include <asm/uaccess.h>

static int compat_get_timex(struct timextxc, struct compat_timex __userutp)
{
	memset(txc, 0, sizeof(struct timex));

	if (!access_ok(VERIFY_READ, utp, sizeof(struct compat_timex)) ||
			__get_user(txc->modes, &utp->modes) ||
			__get_user(txc->offset, &utp->offset) ||
			__get_user(txc->freq, &utp->freq) ||
			__get_user(txc->maxerror, &utp->maxerror) ||
			__get_user(txc->esterror, &utp->esterror) ||
			__get_user(txc->status, &utp->status) ||
			__get_user(txc->constant, &utp->constant) ||
			__get_user(txc->precision, &utp->precision) ||
			__get_user(txc->tolerance, &utp->tolerance) ||
			__get_user(txc->time.tv_sec, &utp->time.tv_sec) ||
			__get_user(txc->time.tv_usec, &utp->time.tv_usec) ||
			__get_user(txc->tick, &utp->tick) ||
			__get_user(txc->ppsfreq, &utp->ppsfreq) ||
			__get_user(txc->jitter, &utp->jitter) ||
			__get_user(txc->shift, &utp->shift) ||
			__get_user(txc->stabil, &utp->stabil) ||
			__get_user(txc->jitcnt, &utp->jitcnt) ||
			__get_user(txc->calcnt, &utp->calcnt) ||
			__get_user(txc->errcnt, &utp->errcnt) ||
			__get_user(txc->stbcnt, &utp->stbcnt))
		return -EFAULT;

	return 0;
}

static int compat_put_timex(struct compat_timex __userutp, struct timextxc)
{
	if (!access_ok(VERIFY_WRITE, utp, sizeof(struct compat_timex)) ||
			__put_user(txc->modes, &utp->modes) ||
			__put_user(txc->offset, &utp->offset) ||
			__put_user(txc->freq, &utp->freq) ||
			__put_user(txc->maxerror, &utp->maxerror) ||
			__put_user(txc->esterror, &utp->esterror) ||
			__put_user(txc->status, &utp->status) ||
			__put_user(txc->constant, &utp->constant) ||
			__put_user(txc->precision, &utp->precision) ||
			__put_user(txc->tolerance, &utp->tolerance) ||
			__put_user(txc->time.tv_sec, &utp->time.tv_sec) ||
			__put_user(txc->time.tv_usec, &utp->time.tv_usec) ||
			__put_user(txc->tick, &utp->tick) ||
			__put_user(txc->ppsfreq, &utp->ppsfreq) ||
			__put_user(txc->jitter, &utp->jitter) ||
			__put_user(txc->shift, &utp->shift) ||
			__put_user(txc->stabil, &utp->stabil) ||
			__put_user(txc->jitcnt, &utp->jitcnt) ||
			__put_user(txc->calcnt, &utp->calcnt) ||
			__put_user(txc->errcnt, &utp->errcnt) ||
			__put_user(txc->stbcnt, &utp->stbcnt) ||
			__put_user(txc->tai, &utp->tai))
		return -EFAULT;
	return 0;
}

COMPAT_SYSCALL_DEFINE2(gettimeofday, struct compat_timeval __user, tv,
		       struct timezone __user, tz)
{
	if (tv) {
		struct timeval ktv;
		do_gettimeofday(&ktv);
		if (compat_put_timeval(&ktv, tv))
			return -EFAULT;
	}
	if (tz) {
		if (copy_to_user(tz, &sys_tz, sizeof(sys_tz)))
			return -EFAULT;
	}

	return 0;
}

COMPAT_SYSCALL_DEFINE2(settimeofday, struct compat_timeval __user, tv,
		       struct timezone __user, tz)
{
	struct timeval user_tv;
	struct timespec	new_ts;
	struct timezone new_tz;

	if (tv) {
		if (compat_get_timeval(&user_tv, tv))
			return -EFAULT;
		new_ts.tv_sec = user_tv.tv_sec;
		new_ts.tv_nsec = user_tv.tv_usec NSEC_PER_USEC;
	}
	if (tz) {
		if (copy_from_user(&new_tz, tz, sizeof(*tz)))
			return -EFAULT;
	}

	return do_sys_settimeofday(tv ? &new_ts : NULL, tz ? &new_tz : NULL);
}

static int __compat_get_timeval(struct timevaltv, const struct compat_timeval __userctv)
{
	return (!access_ok(VERIFY_READ, ctv, sizeof(*ctv)) ||
			__get_user(tv->tv_sec, &ctv->tv_sec) ||
			__get_user(tv->tv_usec, &ctv->tv_usec)) ? -EFAULT : 0;
}

static int __compat_put_timeval(const struct timevaltv, struct compat_timeval __userctv)
{
	return (!access_ok(VERIFY_WRITE, ctv, sizeof(*ctv)) ||
			__put_user(tv->tv_sec, &ctv->tv_sec) ||
			__put_user(tv->tv_usec, &ctv->tv_usec)) ? -EFAULT : 0;
}

static int __compat_get_timespec(struct timespects, const struct compat_timespec __usercts)
{
	return (!access_ok(VERIFY_READ, cts, sizeof(*cts)) ||
			__get_user(ts->tv_sec, &cts->tv_sec) ||
			__get_user(ts->tv_nsec, &cts->tv_nsec)) ? -EFAULT : 0;
}

static int __compat_put_timespec(const struct timespects, struct compat_timespec __usercts)
{
	return (!access_ok(VERIFY_WRITE, cts, sizeof(*cts)) ||
			__put_user(ts->tv_sec, &cts->tv_sec) ||
			__put_user(ts->tv_nsec, &cts->tv_nsec)) ? -EFAULT : 0;
}

int compat_get_timeval(struct timevaltv, const void __userutv)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_from_user(tv, utv, sizeof(*tv)) ? -EFAULT : 0;
	else
		return __compat_get_timeval(tv, utv);
}
EXPORT_SYMBOL_GPL(compat_get_timeval);

int compat_put_timeval(const struct timevaltv, void __userutv)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_to_user(utv, tv, sizeof(*tv)) ? -EFAULT : 0;
	else
		return __compat_put_timeval(tv, utv);
}
EXPORT_SYMBOL_GPL(compat_put_timeval);

int compat_get_timespec(struct timespects, const void __useruts)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_from_user(ts, uts, sizeof(*ts)) ? -EFAULT : 0;
	else
		return __compat_get_timespec(ts, uts);
}
EXPORT_SYMBOL_GPL(compat_get_timespec);

int compat_put_timespec(const struct timespects, void __useruts)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_to_user(uts, ts, sizeof(*ts)) ? -EFAULT : 0;
	else
		return __compat_put_timespec(ts, uts);
}
EXPORT_SYMBOL_GPL(compat_put_timespec);

int compat_convert_timespec(struct timespec __user*kts,
			    const void __usercts)
{
	struct timespec ts;
	struct timespec __useruts;

	if (!cts || COMPAT_USE_64BIT_TIME) {
		*kts = (struct timespec __user)cts;
		return 0;
	}

	uts = compat_alloc_user_space(sizeof(ts));
	if (!uts)
		return -EFAULT;
	if (compat_get_timespec(&ts, cts))
		return -EFAULT;
	if (copy_to_user(uts, &ts, sizeof(ts)))
		return -EFAULT;

	*kts = uts;
	return 0;
}

static long compat_nanosleep_restart(struct restart_blockrestart)
{
	struct compat_timespec __userrmtp;
	struct timespec rmt;
	mm_segment_t oldfs;
	long ret;

	restart->nanosleep.rmtp = (struct timespec __user) &rmt;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	ret = hrtimer_nanosleep_restart(restart);
	set_fs(oldfs);

	if (ret == -ERESTART_RESTARTBLOCK) {
		rmtp = restart->nanosleep.compat_rmtp;

		if (rmtp && compat_put_timespec(&rmt, rmtp))
			return -EFAULT;
	}

	return ret;
}

COMPAT_SYSCALL_DEFINE2(nanosleep, struct compat_timespec __user, rqtp,
		       struct compat_timespec __user, rmtp)
{
	struct timespec tu, rmt;
	mm_segment_t oldfs;
	long ret;

	if (compat_get_timespec(&tu, rqtp))
		return -EFAULT;

	if (!timespec_valid(&tu))
		return -EINVAL;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	ret = hrtimer_nanosleep(&tu,
				rmtp ? (struct timespec __user)&rmt : NULL,
				HRTIMER_MODE_REL, CLOCK_MONOTONIC);
	set_fs(oldfs);

	*/
	 hrtimer_nanosleep() can only return 0 or
	 -ERESTART_RESTARTBLOCK here because:
	
	 - we call it with HRTIMER_MODE_REL and therefor exclude the
	   -ERESTARTNOHAND return path.
	
	 - we supply the rmtp argument from the task stack (due to
	   the necessary compat conversion. So the update cannot
	   fail, which excludes the -EFAULT return path as well. If
	   it fails nevertheless we have a bigger problem and wont
	   reach this place anymore.
	
	 - if the return value is 0, we do not have to update rmtp
	    because there is no remaining time.
	
	 We check for -ERESTART_RESTARTBLOCK nevertheless if the
	 core implementation decides to return random nonsense.
	 /*
	if (ret == -ERESTART_RESTARTBLOCK) {
		struct restart_blockrestart = &current->restart_block;

		restart->fn = compat_nanosleep_restart;
		restart->nanosleep.compat_rmtp = rmtp;

		if (rmtp && compat_put_timespec(&rmt, rmtp))
			return -EFAULT;
	}
	return ret;
}

static inline long get_compat_itimerval(struct itimervalo,
		struct compat_itimerval __useri)
{
	return (!access_ok(VERIFY_READ, i, sizeof(*i)) ||
		(__get_user(o->it_interval.tv_sec, &i->it_interval.tv_sec) |
		 __get_user(o->it_interval.tv_usec, &i->it_interval.tv_usec) |
		 __get_user(o->it_value.tv_sec, &i->it_value.tv_sec) |
		 __get_user(o->it_value.tv_usec, &i->it_value.tv_usec)));
}

static inline long put_compat_itimerval(struct compat_itimerval __usero,
		struct itimervali)
{
	return (!access_ok(VERIFY_WRITE, o, sizeof(*o)) ||
		(__put_user(i->it_interval.tv_sec, &o->it_interval.tv_sec) |
		 __put_user(i->it_interval.tv_usec, &o->it_interval.tv_usec) |
		 __put_user(i->it_value.tv_sec, &o->it_value.tv_sec) |
		 __put_user(i->it_value.tv_usec, &o->it_value.tv_usec)));
}

COMPAT_SYSCALL_DEFINE2(getitimer, int, which,
		struct compat_itimerval __user, it)
{
	struct itimerval kit;
	int error;

	error = do_getitimer(which, &kit);
	if (!error && put_compat_itimerval(it, &kit))
		error = -EFAULT;
	return error;
}

COMPAT_SYSCALL_DEFINE3(setitimer, int, which,
		struct compat_itimerval __user, in,
		struct compat_itimerval __user, out)
{
	struct itimerval kin, kout;
	int error;

	if (in) {
		if (get_compat_itimerval(&kin, in))
			return -EFAULT;
	} else
		memset(&kin, 0, sizeof(kin));

	error = do_setitimer(which, &kin, out ? &kout : NULL);
	if (error || !out)
		return error;
	if (put_compat_itimerval(out, &kout))
		return -EFAULT;
	return 0;
}

static compat_clock_t clock_t_to_compat_clock_t(clock_t x)
{
	return compat_jiffies_to_clock_t(clock_t_to_jiffies(x));
}

COMPAT_SYSCALL_DEFINE1(times, struct compat_tms __user, tbuf)
{
	if (tbuf) {
		struct tms tms;
		struct compat_tms tmp;

		do_sys_times(&tms);
		*/ Convert our struct tms to the compat version. /*
		tmp.tms_utime = clock_t_to_compat_clock_t(tms.tms_utime);
		tmp.tms_stime = clock_t_to_compat_clock_t(tms.tms_stime);
		tmp.tms_cutime = clock_t_to_compat_clock_t(tms.tms_cutime);
		tmp.tms_cstime = clock_t_to_compat_clock_t(tms.tms_cstime);
		if (copy_to_user(tbuf, &tmp, sizeof(tmp)))
			return -EFAULT;
	}
	force_successful_syscall_return();
	return compat_jiffies_to_clock_t(jiffies);
}

#ifdef __ARCH_WANT_SYS_SIGPENDING

*/
 Assumption: old_sigset_t and compat_old_sigset_t are both
 types that can be passed to put_user()/get_user().
 /*

COMPAT_SYSCALL_DEFINE1(sigpending, compat_old_sigset_t __user, set)
{
	old_sigset_t s;
	long ret;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	ret = sys_sigpending((old_sigset_t __user) &s);
	set_fs(old_fs);
	if (ret == 0)
		ret = put_user(s, set);
	return ret;
}

#endif

#ifdef __ARCH_WANT_SYS_SIGPROCMASK

*/
 sys_sigprocmask SIG_SETMASK sets the first (compat) word of the
 blocked set of signals to the supplied signal set
 /*
static inline void compat_sig_setmask(sigset_tblocked, compat_sigset_word set)
{
	memcpy(blocked->sig, &set, sizeof(set));
}

COMPAT_SYSCALL_DEFINE3(sigprocmask, int, how,
		       compat_old_sigset_t __user, nset,
		       compat_old_sigset_t __user, oset)
{
	old_sigset_t old_set, new_set;
	sigset_t new_blocked;

	old_set = current->blocked.sig[0];

	if (nset) {
		if (get_user(new_set, nset))
			return -EFAULT;
		new_set &= ~(sigmask(SIGKILL) | sigmask(SIGSTOP));

		new_blocked = current->blocked;

		switch (how) {
		case SIG_BLOCK:
			sigaddsetmask(&new_blocked, new_set);
			break;
		case SIG_UNBLOCK:
			sigdelsetmask(&new_blocked, new_set);
			break;
		case SIG_SETMASK:
			compat_sig_setmask(&new_blocked, new_set);
			break;
		default:
			return -EINVAL;
		}

		set_current_blocked(&new_blocked);
	}

	if (oset) {
		if (put_user(old_set, oset))
			return -EFAULT;
	}

	return 0;
}

#endif

COMPAT_SYSCALL_DEFINE2(setrlimit, unsigned int, resource,
		       struct compat_rlimit __user, rlim)
{
	struct rlimit r;

	if (!access_ok(VERIFY_READ, rlim, sizeof(*rlim)) ||
	    __get_user(r.rlim_cur, &rlim->rlim_cur) ||
	    __get_user(r.rlim_max, &rlim->rlim_max))
		return -EFAULT;

	if (r.rlim_cur == COMPAT_RLIM_INFINITY)
		r.rlim_cur = RLIM_INFINITY;
	if (r.rlim_max == COMPAT_RLIM_INFINITY)
		r.rlim_max = RLIM_INFINITY;
	return do_prlimit(current, resource, &r, NULL);
}

#ifdef COMPAT_RLIM_OLD_INFINITY

COMPAT_SYSCALL_DEFINE2(old_getrlimit, unsigned int, resource,
		       struct compat_rlimit __user, rlim)
{
	struct rlimit r;
	int ret;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	ret = sys_old_getrlimit(resource, (struct rlimit __user)&r);
	set_fs(old_fs);

	if (!ret) {
		if (r.rlim_cur > COMPAT_RLIM_OLD_INFINITY)
			r.rlim_cur = COMPAT_RLIM_INFINITY;
		if (r.rlim_max > COMPAT_RLIM_OLD_INFINITY)
			r.rlim_max = COMPAT_RLIM_INFINITY;

		if (!access_ok(VERIFY_WRITE, rlim, sizeof(*rlim)) ||
		    __put_user(r.rlim_cur, &rlim->rlim_cur) ||
		    __put_user(r.rlim_max, &rlim->rlim_max))
			return -EFAULT;
	}
	return ret;
}

#endif

COMPAT_SYSCALL_DEFINE2(getrlimit, unsigned int, resource,
		       struct compat_rlimit __user, rlim)
{
	struct rlimit r;
	int ret;

	ret = do_prlimit(current, resource, NULL, &r);
	if (!ret) {
		if (r.rlim_cur > COMPAT_RLIM_INFINITY)
			r.rlim_cur = COMPAT_RLIM_INFINITY;
		if (r.rlim_max > COMPAT_RLIM_INFINITY)
			r.rlim_max = COMPAT_RLIM_INFINITY;

		if (!access_ok(VERIFY_WRITE, rlim, sizeof(*rlim)) ||
		    __put_user(r.rlim_cur, &rlim->rlim_cur) ||
		    __put_user(r.rlim_max, &rlim->rlim_max))
			return -EFAULT;
	}
	return ret;
}

int put_compat_rusage(const struct rusager, struct compat_rusage __userru)
{
	if (!access_ok(VERIFY_WRITE, ru, sizeof(*ru)) ||
	    __put_user(r->ru_utime.tv_sec, &ru->ru_utime.tv_sec) ||
	    __put_user(r->ru_utime.tv_usec, &ru->ru_utime.tv_usec) ||
	    __put_user(r->ru_stime.tv_sec, &ru->ru_stime.tv_sec) ||
	    __put_user(r->ru_stime.tv_usec, &ru->ru_stime.tv_usec) ||
	    __put_user(r->ru_maxrss, &ru->ru_maxrss) ||
	    __put_user(r->ru_ixrss, &ru->ru_ixrss) ||
	    __put_user(r->ru_idrss, &ru->ru_idrss) ||
	    __put_user(r->ru_isrss, &ru->ru_isrss) ||
	    __put_user(r->ru_minflt, &ru->ru_minflt) ||
	    __put_user(r->ru_majflt, &ru->ru_majflt) ||
	    __put_user(r->ru_nswap, &ru->ru_nswap) ||
	    __put_user(r->ru_inblock, &ru->ru_inblock) ||
	    __put_user(r->ru_oublock, &ru->ru_oublock) ||
	    __put_user(r->ru_msgsnd, &ru->ru_msgsnd) ||
	    __put_user(r->ru_msgrcv, &ru->ru_msgrcv) ||
	    __put_user(r->ru_nsignals, &ru->ru_nsignals) ||
	    __put_user(r->ru_nvcsw, &ru->ru_nvcsw) ||
	    __put_user(r->ru_nivcsw, &ru->ru_nivcsw))
		return -EFAULT;
	return 0;
}

COMPAT_SYSCALL_DEFINE4(wait4,
	compat_pid_t, pid,
	compat_uint_t __user, stat_addr,
	int, options,
	struct compat_rusage __user, ru)
{
	if (!ru) {
		return sys_wait4(pid, stat_addr, options, NULL);
	} else {
		struct rusage r;
		int ret;
		unsigned int status;
		mm_segment_t old_fs = get_fs();

		set_fs (KERNEL_DS);
		ret = sys_wait4(pid,
				(stat_addr ?
				 (unsigned int __user) &status : NULL),
				options, (struct rusage __user) &r);
		set_fs (old_fs);

		if (ret > 0) {
			if (put_compat_rusage(&r, ru))
				return -EFAULT;
			if (stat_addr && put_user(status, stat_addr))
				return -EFAULT;
		}
		return ret;
	}
}

COMPAT_SYSCALL_DEFINE5(waitid,
		int, which, compat_pid_t, pid,
		struct compat_siginfo __user, uinfo, int, options,
		struct compat_rusage __user, uru)
{
	siginfo_t info;
	struct rusage ru;
	long ret;
	mm_segment_t old_fs = get_fs();

	memset(&info, 0, sizeof(info));

	set_fs(KERNEL_DS);
	ret = sys_waitid(which, pid, (siginfo_t __user)&info, options,
			 uru ? (struct rusage __user)&ru : NULL);
	set_fs(old_fs);

	if ((ret < 0) || (info.si_signo == 0))
		return ret;

	if (uru) {
		*/ sys_waitid() overwrites everything in ru /*
		if (COMPAT_USE_64BIT_TIME)
			ret = copy_to_user(uru, &ru, sizeof(ru));
		else
			ret = put_compat_rusage(&ru, uru);
		if (ret)
			return -EFAULT;
	}

	BUG_ON(info.si_code & __SI_MASK);
	info.si_code |= __SI_CHLD;
	return copy_siginfo_to_user32(uinfo, &info);
}

static int compat_get_user_cpu_mask(compat_ulong_t __useruser_mask_ptr,
				    unsigned len, struct cpumasknew_mask)
{
	unsigned longk;

	if (len < cpumask_size())
		memset(new_mask, 0, cpumask_size());
	else if (len > cpumask_size())
		len = cpumask_size();

	k = cpumask_bits(new_mask);
	return compat_get_bitmap(k, user_mask_ptr, len 8);
}

COMPAT_SYSCALL_DEFINE3(sched_setaffinity, compat_pid_t, pid,
		       unsigned int, len,
		       compat_ulong_t __user, user_mask_ptr)
{
	cpumask_var_t new_mask;
	int retval;

	if (!alloc_cpumask_var(&new_mask, GFP_KERNEL))
		return -ENOMEM;

	retval = compat_get_user_cpu_mask(user_mask_ptr, len, new_mask);
	if (retval)
		goto out;

	retval = sched_setaffinity(pid, new_mask);
out:
	free_cpumask_var(new_mask);
	return retval;
}

COMPAT_SYSCALL_DEFINE3(sched_getaffinity, compat_pid_t,  pid, unsigned int, len,
		       compat_ulong_t __user, user_mask_ptr)
{
	int ret;
	cpumask_var_t mask;

	if ((len BITS_PER_BYTE) < nr_cpu_ids)
		return -EINVAL;
	if (len & (sizeof(compat_ulong_t)-1))
		return -EINVAL;

	if (!alloc_cpumask_var(&mask, GFP_KERNEL))
		return -ENOMEM;

	ret = sched_getaffinity(pid, mask);
	if (ret == 0) {
		size_t retlen = min_t(size_t, len, cpumask_size());

		if (compat_put_bitmap(user_mask_ptr, cpumask_bits(mask), retlen 8))
			ret = -EFAULT;
		else
			ret = retlen;
	}
	free_cpumask_var(mask);

	return ret;
}

int get_compat_itimerspec(struct itimerspecdst,
			  const struct compat_itimerspec __usersrc)
{
	if (__compat_get_timespec(&dst->it_interval, &src->it_interval) ||
	    __compat_get_timespec(&dst->it_value, &src->it_value))
		return -EFAULT;
	return 0;
}

int put_compat_itimerspec(struct compat_itimerspec __userdst,
			  const struct itimerspecsrc)
{
	if (__compat_put_timespec(&src->it_interval, &dst->it_interval) ||
	    __compat_put_timespec(&src->it_value, &dst->it_value))
		return -EFAULT;
	return 0;
}

COMPAT_SYSCALL_DEFINE3(timer_create, clockid_t, which_clock,
		       struct compat_sigevent __user, timer_event_spec,
		       timer_t __user, created_timer_id)
{
	struct sigevent __userevent = NULL;

	if (timer_event_spec) {
		struct sigevent kevent;

		event = compat_alloc_user_space(sizeof(*event));
		if (get_compat_sigevent(&kevent, timer_event_spec) ||
		    copy_to_user(event, &kevent, sizeof(*event)))
			return -EFAULT;
	}

	return sys_timer_create(which_clock, event, created_timer_id);
}

COMPAT_SYSCALL_DEFINE4(timer_settime, timer_t, timer_id, int, flags,
		       struct compat_itimerspec __user, new,
		       struct compat_itimerspec __user, old)
{
	long err;
	mm_segment_t oldfs;
	struct itimerspec newts, oldts;

	if (!new)
		return -EINVAL;
	if (get_compat_itimerspec(&newts, new))
		return -EFAULT;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_timer_settime(timer_id, flags,
				(struct itimerspec __user) &newts,
				(struct itimerspec __user) &oldts);
	set_fs(oldfs);
	if (!err && old && put_compat_itimerspec(old, &oldts))
		return -EFAULT;
	return err;
}

COMPAT_SYSCALL_DEFINE2(timer_gettime, timer_t, timer_id,
		       struct compat_itimerspec __user, setting)
{
	long err;
	mm_segment_t oldfs;
	struct itimerspec ts;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_timer_gettime(timer_id,
				(struct itimerspec __user) &ts);
	set_fs(oldfs);
	if (!err && put_compat_itimerspec(setting, &ts))
		return -EFAULT;
	return err;
}

COMPAT_SYSCALL_DEFINE2(clock_settime, clockid_t, which_clock,
		       struct compat_timespec __user, tp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec ts;

	if (compat_get_timespec(&ts, tp))
		return -EFAULT;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_settime(which_clock,
				(struct timespec __user) &ts);
	set_fs(oldfs);
	return err;
}

COMPAT_SYSCALL_DEFINE2(clock_gettime, clockid_t, which_clock,
		       struct compat_timespec __user, tp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec ts;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_gettime(which_clock,
				(struct timespec __user) &ts);
	set_fs(oldfs);
	if (!err && compat_put_timespec(&ts, tp))
		return -EFAULT;
	return err;
}

COMPAT_SYSCALL_DEFINE2(clock_adjtime, clockid_t, which_clock,
		       struct compat_timex __user, utp)
{
	struct timex txc;
	mm_segment_t oldfs;
	int err, ret;

	err = compat_get_timex(&txc, utp);
	if (err)
		return err;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	ret = sys_clock_adjtime(which_clock, (struct timex __user) &txc);
	set_fs(oldfs);

	err = compat_put_timex(utp, &txc);
	if (err)
		return err;

	return ret;
}

COMPAT_SYSCALL_DEFINE2(clock_getres, clockid_t, which_clock,
		       struct compat_timespec __user, tp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec ts;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_getres(which_clock,
			       (struct timespec __user) &ts);
	set_fs(oldfs);
	if (!err && tp && compat_put_timespec(&ts, tp))
		return -EFAULT;
	return err;
}

static long compat_clock_nanosleep_restart(struct restart_blockrestart)
{
	long err;
	mm_segment_t oldfs;
	struct timespec tu;
	struct compat_timespec __userrmtp = restart->nanosleep.compat_rmtp;

	restart->nanosleep.rmtp = (struct timespec __user) &tu;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = clock_nanosleep_restart(restart);
	set_fs(oldfs);

	if ((err == -ERESTART_RESTARTBLOCK) && rmtp &&
	    compat_put_timespec(&tu, rmtp))
		return -EFAULT;

	if (err == -ERESTART_RESTARTBLOCK) {
		restart->fn = compat_clock_nanosleep_restart;
		restart->nanosleep.compat_rmtp = rmtp;
	}
	return err;
}

COMPAT_SYSCALL_DEFINE4(clock_nanosleep, clockid_t, which_clock, int, flags,
		       struct compat_timespec __user, rqtp,
		       struct compat_timespec __user, rmtp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec in, out;
	struct restart_blockrestart;

	if (compat_get_timespec(&in, rqtp))
		return -EFAULT;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_nanosleep(which_clock, flags,
				  (struct timespec __user) &in,
				  (struct timespec __user) &out);
	set_fs(oldfs);

	if ((err == -ERESTART_RESTARTBLOCK) && rmtp &&
	    compat_put_timespec(&out, rmtp))
		return -EFAULT;

	if (err == -ERESTART_RESTARTBLOCK) {
		restart = &current->restart_block;
		restart->fn = compat_clock_nanosleep_restart;
		restart->nanosleep.compat_rmtp = rmtp;
	}
	return err;
}

*/
 We currently only need the following fields from the sigevent
 structure: sigev_value, sigev_signo, sig_notify and (sometimes
 sigev_notify_thread_id).  The others are handled in user mode.
 We also assume that copying sigev_value.sival_int is sufficient
 to keep all the bits of sigev_value.sival_ptr intact.
 /*
int get_compat_sigevent(struct sigeventevent,
		const struct compat_sigevent __useru_event)
{
	memset(event, 0, sizeof(*event));
	return (!access_ok(VERIFY_READ, u_event, sizeof(*u_event)) ||
		__get_user(event->sigev_value.sival_int,
			&u_event->sigev_value.sival_int) ||
		__get_user(event->sigev_signo, &u_event->sigev_signo) ||
		__get_user(event->sigev_notify, &u_event->sigev_notify) ||
		__get_user(event->sigev_notify_thread_id,
			&u_event->sigev_notify_thread_id))
		? -EFAULT : 0;
}

long compat_get_bitmap(unsigned longmask, const compat_ulong_t __userumask,
		       unsigned long bitmap_size)
{
	int i, j;
	unsigned long m;
	compat_ulong_t um;
	unsigned long nr_compat_longs;

	*/ align bitmap up to nearest compat_long_t boundary /*
	bitmap_size = ALIGN(bitmap_size, BITS_PER_COMPAT_LONG);

	if (!access_ok(VERIFY_READ, umask, bitmap_size / 8))
		return -EFAULT;

	nr_compat_longs = BITS_TO_COMPAT_LONGS(bitmap_size);

	for (i = 0; i < BITS_TO_LONGS(bitmap_size); i++) {
		m = 0;

		for (j = 0; j < sizeof(m)/sizeof(um); j++) {
			*/
			 We dont want to read past the end of the userspace
			 bitmap. We must however ensure the end of the
			 kernel bitmap is zeroed.
			 /*
			if (nr_compat_longs) {
				nr_compat_longs--;
				if (__get_user(um, umask))
					return -EFAULT;
			} else {
				um = 0;
			}

			umask++;
			m |= (long)um << (j BITS_PER_COMPAT_LONG);
		}
		*mask++ = m;
	}

	return 0;
}

long compat_put_bitmap(compat_ulong_t __userumask, unsigned longmask,
		       unsigned long bitmap_size)
{
	int i, j;
	unsigned long m;
	compat_ulong_t um;
	unsigned long nr_compat_longs;

	*/ align bitmap up to nearest compat_long_t boundary /*
	bitmap_size = ALIGN(bitmap_size, BITS_PER_COMPAT_LONG);

	if (!access_ok(VERIFY_WRITE, umask, bitmap_size / 8))
		return -EFAULT;

	nr_compat_longs = BITS_TO_COMPAT_LONGS(bitmap_size);

	for (i = 0; i < BITS_TO_LONGS(bitmap_size); i++) {
		m =mask++;

		for (j = 0; j < sizeof(m)/sizeof(um); j++) {
			um = m;

			*/
			 We dont want to write past the end of the userspace
			 bitmap.
			 /*
			if (nr_compat_longs) {
				nr_compat_longs--;
				if (__put_user(um, umask))
					return -EFAULT;
			}

			umask++;
			m >>= 4*sizeof(um);
			m >>= 4*sizeof(um);
		}
	}

	return 0;
}

void
sigset_from_compat(sigset_tset, const compat_sigset_tcompat)
{
	switch (_NSIG_WORDS) {
	case 4: set->sig[3] = compat->sig[6] | (((long)compat->sig[7]) << 32 );
	case 3: set->sig[2] = compat->sig[4] | (((long)compat->sig[5]) << 32 );
	case 2: set->sig[1] = compat->sig[2] | (((long)compat->sig[3]) << 32 );
	case 1: set->sig[0] = compat->sig[0] | (((long)compat->sig[1]) << 32 );
	}
}
EXPORT_SYMBOL_GPL(sigset_from_compat);

void
sigset_to_compat(compat_sigset_tcompat, const sigset_tset)
{
	switch (_NSIG_WORDS) {
	case 4: compat->sig[7] = (set->sig[3] >> 32); compat->sig[6] = set->sig[3];
	case 3: compat->sig[5] = (set->sig[2] >> 32); compat->sig[4] = set->sig[2];
	case 2: compat->sig[3] = (set->sig[1] >> 32); compat->sig[2] = set->sig[1];
	case 1: compat->sig[1] = (set->sig[0] >> 32); compat->sig[0] = set->sig[0];
	}
}

COMPAT_SYSCALL_DEFINE4(rt_sigtimedwait, compat_sigset_t __user, uthese,
		struct compat_siginfo __user, uinfo,
		struct compat_timespec __user, uts, compat_size_t, sigsetsize)
{
	compat_sigset_t s32;
	sigset_t s;
	struct timespec t;
	siginfo_t info;
	long ret;

	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (copy_from_user(&s32, uthese, sizeof(compat_sigset_t)))
		return -EFAULT;
	sigset_from_compat(&s, &s32);

	if (uts) {
		if (compat_get_timespec(&t, uts))
			return -EFAULT;
	}

	ret = do_sigtimedwait(&s, &info, uts ? &t : NULL);

	if (ret > 0 && uinfo) {
		if (copy_siginfo_to_user32(uinfo, &info))
			ret = -EFAULT;
	}

	return ret;
}

#ifdef __ARCH_WANT_COMPAT_SYS_TIME

*/ compat_time_t is a 32 bit "long" and needs to get converted. /*

COMPAT_SYSCALL_DEFINE1(time, compat_time_t __user, tloc)
{
	compat_time_t i;
	struct timeval tv;

	do_gettimeofday(&tv);
	i = tv.tv_sec;

	if (tloc) {
		if (put_user(i,tloc))
			return -EFAULT;
	}
	force_successful_syscall_return();
	return i;
}

COMPAT_SYSCALL_DEFINE1(stime, compat_time_t __user, tptr)
{
	struct timespec tv;
	int err;

	if (get_user(tv.tv_sec, tptr))
		return -EFAULT;

	tv.tv_nsec = 0;

	err = security_settime(&tv, NULL);
	if (err)
		return err;

	do_settimeofday(&tv);
	return 0;
}

#endif */ __ARCH_WANT_COMPAT_SYS_TIME /*

COMPAT_SYSCALL_DEFINE1(adjtimex, struct compat_timex __user, utp)
{
	struct timex txc;
	int err, ret;

	err = compat_get_timex(&txc, utp);
	if (err)
		return err;

	ret = do_adjtimex(&txc);

	err = compat_put_timex(utp, &txc);
	if (err)
		return err;

	return ret;
}

#ifdef CONFIG_NUMA
COMPAT_SYSCALL_DEFINE6(move_pages, pid_t, pid, compat_ulong_t, nr_pages,
		       compat_uptr_t __user, pages32,
		       const int __user, nodes,
		       int __user, status,
		       int, flags)
{
	const void __user __userpages;
	int i;

	pages = compat_alloc_user_space(nr_pages sizeof(void));
	for (i = 0; i < nr_pages; i++) {
		compat_uptr_t p;

		if (get_user(p, pages32 + i) ||
			put_user(compat_ptr(p), pages + i))
			return -EFAULT;
	}
	return sys_move_pages(pid, nr_pages, pages, nodes, status, flags);
}

COMPAT_SYSCALL_DEFINE4(migrate_pages, compat_pid_t, pid,
		       compat_ulong_t, maxnode,
		       const compat_ulong_t __user, old_nodes,
		       const compat_ulong_t __user, new_nodes)
{
	unsigned long __userold = NULL;
	unsigned long __usernew = NULL;
	nodemask_t tmp_mask;
	unsigned long nr_bits;
	unsigned long size;

	nr_bits = min_t(unsigned long, maxnode - 1, MAX_NUMNODES);
	size = ALIGN(nr_bits, BITS_PER_LONG) / 8;
	if (old_nodes) {
		if (compat_get_bitmap(nodes_addr(tmp_mask), old_nodes, nr_bits))
			return -EFAULT;
		old = compat_alloc_user_space(new_nodes ? size 2 : size);
		if (new_nodes)
			new = old + size / sizeof(unsigned long);
		if (copy_to_user(old, nodes_addr(tmp_mask), size))
			return -EFAULT;
	}
	if (new_nodes) {
		if (compat_get_bitmap(nodes_addr(tmp_mask), new_nodes, nr_bits))
			return -EFAULT;
		if (new == NULL)
			new = compat_alloc_user_space(size);
		if (copy_to_user(new, nodes_addr(tmp_mask), size))
			return -EFAULT;
	}
	return sys_migrate_pages(pid, nr_bits + 1, old, new);
}
#endif

COMPAT_SYSCALL_DEFINE2(sched_rr_get_interval,
		       compat_pid_t, pid,
		       struct compat_timespec __user, interval)
{
	struct timespec t;
	int ret;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	ret = sys_sched_rr_get_interval(pid, (struct timespec __user)&t);
	set_fs(old_fs);
	if (compat_put_timespec(&t, interval))
		return -EFAULT;
	return ret;
}

*/
 Allocate user-space memory for the duration of a single system call,
 in order to marshall parameters inside a compat thunk.
 /*
void __usercompat_alloc_user_space(unsigned long len)
{
	void __userptr;

	*/ If len would occupy more than half of the entire compat space... /*
	if (unlikely(len > (((compat_uptr_t)~0) >> 1)))
		return NULL;

	ptr = arch_compat_alloc_user_space(len);

	if (unlikely(!access_ok(VERIFY_WRITE, ptr, len)))
		return NULL;

	return ptr;
}
EXPORT_SYMBOL_GPL(compat_alloc_user_space);
*/

 kernel/configs.c
 Echo the kernel .config file used to build the kernel

 Copyright (C) 2002 Khalid Aziz <khalid_aziz@hp.com>
 Copyright (C) 2002 Randy Dunlap <rdunlap@xenotime.net>
 Copyright (C) 2002 Al Stone <ahs3@fc.hp.com>
 Copyright (C) 2002 Hewlett-Packard Company

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 NON INFRINGEMENT.  See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 /*

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/init.h>
#include <asm/uaccess.h>

*/***********************************************/
*/ the actual current config file                 /*

*/
 Define kernel_config_data and kernel_config_data_size, which contains the
 wrapped and compressed configuration file.  The file is first compressed
 with gzip and then bounded by two eight byte magic numbers to allow
 extraction from a binary kernel image:

   IKCFG_ST
   <image>
   IKCFG_ED
 /*
#define MAGIC_START	"IKCFG_ST"
#define MAGIC_END	"IKCFG_ED"
#include "config_data.h"


#define MAGIC_SIZE (sizeof(MAGIC_START) - 1)
#define kernel_config_data_size \
	(sizeof(kernel_config_data) - 1 - MAGIC_SIZE 2)

#ifdef CONFIG_IKCONFIG_PROC

static ssize_t
ikconfig_read_current(struct filefile, char __userbuf,
		      size_t len, loff_t offset)
{
	return simple_read_from_buffer(buf, len, offset,
				       kernel_config_data + MAGIC_SIZE,
				       kernel_config_data_size);
}

static const struct file_operations ikconfig_file_ops = {
	.owner = THIS_MODULE,
	.read = ikconfig_read_current,
	.llseek = default_llseek,
};

static int __init ikconfig_init(void)
{
	struct proc_dir_entryentry;

	*/ create the current config file /*
	entry = proc_create("config.gz", S_IFREG | S_IRUGO, NULL,
			    &ikconfig_file_ops);
	if (!entry)
		return -ENOMEM;

	proc_set_size(entry, kernel_config_data_size);

	return 0;
}

static void __exit ikconfig_cleanup(void)
{
	remove_proc_entry("config.gz", NULL);
}

module_init(ikconfig_init);
module_exit(ikconfig_cleanup);

#endif */ CONFIG_IKCONFIG_PROC /*

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Randy Dunlap");
MODULE_DESCRIPTION("Echo the kernel .config file used to build the kernel");
*/

 Context tracking: Probe on high level context boundaries such as kernel
 and userspace. This includes syscalls and exceptions entry/exit.

 This is used by RCU to remove its dependency on the timer tick while a CPU
 runs in userspace.

  Started by Frederic Weisbecker:

 Copyright (C) 2012 Red Hat, Inc., Frederic Weisbecker <fweisbec@redhat.com>

 Many thanks to Gilad Ben-Yossef, Paul McKenney, Ingo Molnar, Andrew Morton,
 Steven Rostedt, Peter Zijlstra for suggestions and improvements.

 /*

#include <linux/context_tracking.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/hardirq.h>
#include <linux/export.h>
#include <linux/kprobes.h>

#define CREATE_TRACE_POINTS
#include <trace/events/context_tracking.h>

DEFINE_STATIC_KEY_FALSE(context_tracking_enabled);
EXPORT_SYMBOL_GPL(context_tracking_enabled);

DEFINE_PER_CPU(struct context_tracking, context_tracking);
EXPORT_SYMBOL_GPL(context_tracking);

static bool context_tracking_recursion_enter(void)
{
	int recursion;

	recursion = __this_cpu_inc_return(context_tracking.recursion);
	if (recursion == 1)
		return true;

	WARN_ONCE((recursion < 1), "Invalid context tracking recursion value %d\n", recursion);
	__this_cpu_dec(context_tracking.recursion);

	return false;
}

static void context_tracking_recursion_exit(void)
{
	__this_cpu_dec(context_tracking.recursion);
}

*/
 context_tracking_enter - Inform the context tracking that the CPU is going
                          enter user or guest space mode.

 This function must be called right before we switch from the kernel
 to user or guest space, when it's guaranteed the remaining kernel
 instructions to execute won't use any RCU read side critical section
 because this function sets RCU in extended quiescent state.
 /*
void __context_tracking_enter(enum ctx_state state)
{
	*/ Kernel threads aren't supposed to go to userspace /*
	WARN_ON_ONCE(!current->mm);

	if (!context_tracking_recursion_enter())
		return;

	if ( __this_cpu_read(context_tracking.state) != state) {
		if (__this_cpu_read(context_tracking.active)) {
			*/
			 At this stage, only low level arch entry code remains and
			 then we'll run in userspace. We can assume there won't be
			 any RCU read-side critical section until the next call to
			 user_exit() or rcu_irq_enter(). Let's remove RCU's dependency
			 on the tick.
			 /*
			if (state == CONTEXT_USER) {
				trace_user_enter(0);
				vtime_user_enter(current);
			}
			rcu_user_enter();
		}
		*/
		 Even if context tracking is disabled on this CPU, because it's outside
		 the full dynticks mask for example, we still have to keep track of the
		 context transitions and states to prevent inconsistency on those of
		 other CPUs.
		 If a task triggers an exception in userspace, sleep on the exception
		 handler and then migrate to another CPU, that new CPU must know where
		 the exception returns by the time we call exception_exit().
		 This information can only be provided by the previous CPU when it called
		 exception_enter().
		 OTOH we can spare the calls to vtime and RCU when context_tracking.active
		 is false because we know that CPU is not tickless.
		 /*
		__this_cpu_write(context_tracking.state, state);
	}
	context_tracking_recursion_exit();
}
NOKPROBE_SYMBOL(__context_tracking_enter);
EXPORT_SYMBOL_GPL(__context_tracking_enter);

void context_tracking_enter(enum ctx_state state)
{
	unsigned long flags;

	*/
	 Some contexts may involve an exception occuring in an irq,
	 leading to that nesting:
	 rcu_irq_enter() rcu_user_exit() rcu_user_exit() rcu_irq_exit()
	 This would mess up the dyntick_nesting count though. And rcu_irq_*()
	 helpers are enough to protect RCU uses inside the exception. So
	 just return immediately if we detect we are in an IRQ.
	 /*
	if (in_interrupt())
		return;

	local_irq_save(flags);
	__context_tracking_enter(state);
	local_irq_restore(flags);
}
NOKPROBE_SYMBOL(context_tracking_enter);
EXPORT_SYMBOL_GPL(context_tracking_enter);

void context_tracking_user_enter(void)
{
	user_enter();
}
NOKPROBE_SYMBOL(context_tracking_user_enter);

*/
 context_tracking_exit - Inform the context tracking that the CPU is
                         exiting user or guest mode and entering the kernel.

 This function must be called after we entered the kernel from user or
 guest space before any use of RCU read side critical section. This
 potentially include any high level kernel code like syscalls, exceptions,
 signal handling, etc...

 This call supports re-entrancy. This way it can be called from any exception
 handler without needing to know if we came from userspace or not.
 /*
void __context_tracking_exit(enum ctx_state state)
{
	if (!context_tracking_recursion_enter())
		return;

	if (__this_cpu_read(context_tracking.state) == state) {
		if (__this_cpu_read(context_tracking.active)) {
			*/
			 We are going to run code that may use RCU. Inform
			 RCU core about that (ie: we may need the tick again).
			 /*
			rcu_user_exit();
			if (state == CONTEXT_USER) {
				vtime_user_exit(current);
				trace_user_exit(0);
			}
		}
		__this_cpu_write(context_tracking.state, CONTEXT_KERNEL);
	}
	context_tracking_recursion_exit();
}
NOKPROBE_SYMBOL(__context_tracking_exit);
EXPORT_SYMBOL_GPL(__context_tracking_exit);

void context_tracking_exit(enum ctx_state state)
{
	unsigned long flags;

	if (in_interrupt())
		return;

	local_irq_save(flags);
	__context_tracking_exit(state);
	local_irq_restore(flags);
}
NOKPROBE_SYMBOL(context_tracking_exit);
EXPORT_SYMBOL_GPL(context_tracking_exit);

void context_tracking_user_exit(void)
{
	user_exit();
}
NOKPROBE_SYMBOL(context_tracking_user_exit);

void __init context_tracking_cpu_set(int cpu)
{
	static __initdata bool initialized = false;

	if (!per_cpu(context_tracking.active, cpu)) {
		per_cpu(context_tracking.active, cpu) = true;
		static_branch_inc(&context_tracking_enabled);
	}

	if (initialized)
		return;

	*/
	 Set TIF_NOHZ to init/0 and let it propagate to all tasks through fork
	 This assumes that init is the only task at this early boot stage.
	 /*
	set_tsk_thread_flag(&init_task, TIF_NOHZ);
	WARN_ON_ONCE(!tasklist_empty());

	initialized = true;
}

#ifdef CONFIG_CONTEXT_TRACKING_FORCE
void __init context_tracking_init(void)
{
	int cpu;

	for_each_possible_cpu(cpu)
		context_tracking_cpu_set(cpu);
}
#endif
*/
 CPU control.
 (C) 2001, 2002, 2003, 2004 Rusty Russell

 This code is licenced under the GPL.
 /*
#include <linux/proc_fs.h>
#include <linux/smp.h>
#include <linux/init.h>
#include <linux/notifier.h>
#include <linux/sched.h>
#include <linux/unistd.h>
#include <linux/cpu.h>
#include <linux/oom.h>
#include <linux/rcupdate.h>
#include <linux/export.h>
#include <linux/bug.h>
#include <linux/kthread.h>
#include <linux/stop_machine.h>
#include <linux/mutex.h>
#include <linux/gfp.h>
#include <linux/suspend.h>
#include <linux/lockdep.h>
#include <linux/tick.h>
#include <linux/irq.h>
#include <linux/smpboot.h>

#include <trace/events/power.h>
#define CREATE_TRACE_POINTS
#include <trace/events/cpuhp.h>

#include "smpboot.h"

*/
 cpuhp_cpu_state - Per cpu hotplug state storage
 @state:	The current cpu state
 @target:	The target state
 @thread:	Pointer to the hotplug thread
 @should_run:	Thread should execute
 @rollback:	Perform a rollback
 @cb_stat:	The state for a single callback (install/uninstall)
 @cb:		Single callback function (install/uninstall)
 @result:	Result of the operation
 @done:	Signal completion to the issuer of the task
 /*
struct cpuhp_cpu_state {
	enum cpuhp_state	state;
	enum cpuhp_state	target;
#ifdef CONFIG_SMP
	struct task_struct	*thread;
	bool			should_run;
	bool			rollback;
	enum cpuhp_state	cb_state;
	int			(*cb)(unsigned int cpu);
	int			result;
	struct completion	done;
#endif
};

static DEFINE_PER_CPU(struct cpuhp_cpu_state, cpuhp_state);

*/
 cpuhp_step - Hotplug state machine step
 @name:	Name of the step
 @startup:	Startup function of the step
 @teardown:	Teardown function of the step
 @skip_onerr:	Do not invoke the functions on error rollback
		Will go away once the notifiers	are gone
 @cant_stop:	Bringup/teardown can't be stopped at this step
 /*
struct cpuhp_step {
	const char	*name;
	int		(*startup)(unsigned int cpu);
	int		(*teardown)(unsigned int cpu);
	bool		skip_onerr;
	bool		cant_stop;
};

static DEFINE_MUTEX(cpuhp_state_mutex);
static struct cpuhp_step cpuhp_bp_states[];
static struct cpuhp_step cpuhp_ap_states[];

*/
 cpuhp_invoke_callback _ Invoke the callbacks for a given state
 @cpu:	The cpu for which the callback should be invoked
 @step:	The step in the state machine
 @cb:		The callback function to invoke

 Called from cpu hotplug and from the state register machinery
 /*
static int cpuhp_invoke_callback(unsigned int cpu, enum cpuhp_state step,
				 int (*cb)(unsigned int))
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
	int ret = 0;

	if (cb) {
		trace_cpuhp_enter(cpu, st->target, step, cb);
		ret = cb(cpu);
		trace_cpuhp_exit(cpu, st->state, step, ret);
	}
	return ret;
}

#ifdef CONFIG_SMP
*/ Serializes the updates to cpu_online_mask, cpu_present_mask /*
static DEFINE_MUTEX(cpu_add_remove_lock);
bool cpuhp_tasks_frozen;
EXPORT_SYMBOL_GPL(cpuhp_tasks_frozen);

*/
 The following two APIs (cpu_maps_update_begin/done) must be used when
 attempting to serialize the updates to cpu_online_mask & cpu_present_mask.
 The APIs cpu_notifier_register_begin/done() must be used to protect CPU
 hotplug callback (un)registration performed using __register_cpu_notifier()
 or __unregister_cpu_notifier().
 /*
void cpu_maps_update_begin(void)
{
	mutex_lock(&cpu_add_remove_lock);
}
EXPORT_SYMBOL(cpu_notifier_register_begin);

void cpu_maps_update_done(void)
{
	mutex_unlock(&cpu_add_remove_lock);
}
EXPORT_SYMBOL(cpu_notifier_register_done);

static RAW_NOTIFIER_HEAD(cpu_chain);

*/ If set, cpu_up and cpu_down will return -EBUSY and do nothing.
 Should always be manipulated under cpu_add_remove_lock
 /*
static int cpu_hotplug_disabled;

#ifdef CONFIG_HOTPLUG_CPU

static struct {
	struct task_structactive_writer;
	*/ wait queue to wake up the active_writer /*
	wait_queue_head_t wq;
	*/ verifies that no writer will get active while readers are active /*
	struct mutex lock;
	*/
	 Also blocks the new readers during
	 an ongoing cpu hotplug operation.
	 /*
	atomic_t refcount;

#ifdef CONFIG_DEBUG_LOCK_ALLOC
	struct lockdep_map dep_map;
#endif
} cpu_hotplug = {
	.active_writer = NULL,
	.wq = __WAIT_QUEUE_HEAD_INITIALIZER(cpu_hotplug.wq),
	.lock = __MUTEX_INITIALIZER(cpu_hotplug.lock),
#ifdef CONFIG_DEBUG_LOCK_ALLOC
	.dep_map = {.name = "cpu_hotplug.lock" },
#endif
};

*/ Lockdep annotations for get/put_online_cpus() and cpu_hotplug_begin/end() /*
#define cpuhp_lock_acquire_read() lock_map_acquire_read(&cpu_hotplug.dep_map)
#define cpuhp_lock_acquire_tryread() \
				  lock_map_acquire_tryread(&cpu_hotplug.dep_map)
#define cpuhp_lock_acquire()      lock_map_acquire(&cpu_hotplug.dep_map)
#define cpuhp_lock_release()      lock_map_release(&cpu_hotplug.dep_map)


void get_online_cpus(void)
{
	might_sleep();
	if (cpu_hotplug.active_writer == current)
		return;
	cpuhp_lock_acquire_read();
	mutex_lock(&cpu_hotplug.lock);
	atomic_inc(&cpu_hotplug.refcount);
	mutex_unlock(&cpu_hotplug.lock);
}
EXPORT_SYMBOL_GPL(get_online_cpus);

void put_online_cpus(void)
{
	int refcount;

	if (cpu_hotplug.active_writer == current)
		return;

	refcount = atomic_dec_return(&cpu_hotplug.refcount);
	if (WARN_ON(refcount < 0))/ try to fix things up /*
		atomic_inc(&cpu_hotplug.refcount);

	if (refcount <= 0 && waitqueue_active(&cpu_hotplug.wq))
		wake_up(&cpu_hotplug.wq);

	cpuhp_lock_release();

}
EXPORT_SYMBOL_GPL(put_online_cpus);

*/
 This ensures that the hotplug operation can begin only when the
 refcount goes to zero.

 Note that during a cpu-hotplug operation, the new readers, if any,
 will be blocked by the cpu_hotplug.lock

 Since cpu_hotplug_begin() is always called after invoking
 cpu_maps_update_begin(), we can be sure that only one writer is active.

 Note that theoretically, there is a possibility of a livelock:
 - Refcount goes to zero, last reader wakes up the sleeping
   writer.
 - Last reader unlocks the cpu_hotplug.lock.
 - A new reader arrives at this moment, bumps up the refcount.
 - The writer acquires the cpu_hotplug.lock finds the refcount
   non zero and goes to sleep again.

 However, this is very difficult to achieve in practice since
 get_online_cpus() not an api which is called all that often.

 /*
void cpu_hotplug_begin(void)
{
	DEFINE_WAIT(wait);

	cpu_hotplug.active_writer = current;
	cpuhp_lock_acquire();

	for (;;) {
		mutex_lock(&cpu_hotplug.lock);
		prepare_to_wait(&cpu_hotplug.wq, &wait, TASK_UNINTERRUPTIBLE);
		if (likely(!atomic_read(&cpu_hotplug.refcount)))
				break;
		mutex_unlock(&cpu_hotplug.lock);
		schedule();
	}
	finish_wait(&cpu_hotplug.wq, &wait);
}

void cpu_hotplug_done(void)
{
	cpu_hotplug.active_writer = NULL;
	mutex_unlock(&cpu_hotplug.lock);
	cpuhp_lock_release();
}

*/
 Wait for currently running CPU hotplug operations to complete (if any) and
 disable future CPU hotplug (from sysfs). The 'cpu_add_remove_lock' protects
 the 'cpu_hotplug_disabled' flag. The same lock is also acquired by the
 hotplug path before performing hotplug operations. So acquiring that lock
 guarantees mutual exclusion from any currently running hotplug operations.
 /*
void cpu_hotplug_disable(void)
{
	cpu_maps_update_begin();
	cpu_hotplug_disabled++;
	cpu_maps_update_done();
}
EXPORT_SYMBOL_GPL(cpu_hotplug_disable);

void cpu_hotplug_enable(void)
{
	cpu_maps_update_begin();
	WARN_ON(--cpu_hotplug_disabled < 0);
	cpu_maps_update_done();
}
EXPORT_SYMBOL_GPL(cpu_hotplug_enable);
#endif	*/ CONFIG_HOTPLUG_CPU /*

*/ Need to know about CPUs going up/down? /*
int register_cpu_notifier(struct notifier_blocknb)
{
	int ret;
	cpu_maps_update_begin();
	ret = raw_notifier_chain_register(&cpu_chain, nb);
	cpu_maps_update_done();
	return ret;
}

int __register_cpu_notifier(struct notifier_blocknb)
{
	return raw_notifier_chain_register(&cpu_chain, nb);
}

static int __cpu_notify(unsigned long val, unsigned int cpu, int nr_to_call,
			intnr_calls)
{
	unsigned long mod = cpuhp_tasks_frozen ? CPU_TASKS_FROZEN : 0;
	voidhcpu = (void)(long)cpu;

	int ret;

	ret = __raw_notifier_call_chain(&cpu_chain, val | mod, hcpu, nr_to_call,
					nr_calls);

	return notifier_to_errno(ret);
}

static int cpu_notify(unsigned long val, unsigned int cpu)
{
	return __cpu_notify(val, cpu, -1, NULL);
}

static void cpu_notify_nofail(unsigned long val, unsigned int cpu)
{
	BUG_ON(cpu_notify(val, cpu));
}

*/ Notifier wrappers for transitioning to state machine /*
static int notify_prepare(unsigned int cpu)
{
	int nr_calls = 0;
	int ret;

	ret = __cpu_notify(CPU_UP_PREPARE, cpu, -1, &nr_calls);
	if (ret) {
		nr_calls--;
		printk(KERN_WARNING "%s: attempt to bring up CPU %u failed\n",
				__func__, cpu);
		__cpu_notify(CPU_UP_CANCELED, cpu, nr_calls, NULL);
	}
	return ret;
}

static int notify_online(unsigned int cpu)
{
	cpu_notify(CPU_ONLINE, cpu);
	return 0;
}

static int notify_starting(unsigned int cpu)
{
	cpu_notify(CPU_STARTING, cpu);
	return 0;
}

static int bringup_wait_for_ap(unsigned int cpu)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);

	wait_for_completion(&st->done);
	return st->result;
}

static int bringup_cpu(unsigned int cpu)
{
	struct task_structidle = idle_thread_get(cpu);
	int ret;

	*/ Arch-specific enabling code. /*
	ret = __cpu_up(cpu, idle);
	if (ret) {
		cpu_notify(CPU_UP_CANCELED, cpu);
		return ret;
	}
	ret = bringup_wait_for_ap(cpu);
	BUG_ON(!cpu_online(cpu));
	return ret;
}

*/
 Hotplug state machine related functions
 /*
static void undo_cpu_down(unsigned int cpu, struct cpuhp_cpu_statest,
			  struct cpuhp_stepsteps)
{
	for (st->state++; st->state < st->target; st->state++) {
		struct cpuhp_stepstep = steps + st->state;

		if (!step->skip_onerr)
			cpuhp_invoke_callback(cpu, st->state, step->startup);
	}
}

static int cpuhp_down_callbacks(unsigned int cpu, struct cpuhp_cpu_statest,
				struct cpuhp_stepsteps, enum cpuhp_state target)
{
	enum cpuhp_state prev_state = st->state;
	int ret = 0;

	for (; st->state > target; st->state--) {
		struct cpuhp_stepstep = steps + st->state;

		ret = cpuhp_invoke_callback(cpu, st->state, step->teardown);
		if (ret) {
			st->target = prev_state;
			undo_cpu_down(cpu, st, steps);
			break;
		}
	}
	return ret;
}

static void undo_cpu_up(unsigned int cpu, struct cpuhp_cpu_statest,
			struct cpuhp_stepsteps)
{
	for (st->state--; st->state > st->target; st->state--) {
		struct cpuhp_stepstep = steps + st->state;

		if (!step->skip_onerr)
			cpuhp_invoke_callback(cpu, st->state, step->teardown);
	}
}

static int cpuhp_up_callbacks(unsigned int cpu, struct cpuhp_cpu_statest,
			      struct cpuhp_stepsteps, enum cpuhp_state target)
{
	enum cpuhp_state prev_state = st->state;
	int ret = 0;

	while (st->state < target) {
		struct cpuhp_stepstep;

		st->state++;
		step = steps + st->state;
		ret = cpuhp_invoke_callback(cpu, st->state, step->startup);
		if (ret) {
			st->target = prev_state;
			undo_cpu_up(cpu, st, steps);
			break;
		}
	}
	return ret;
}

*/
 The cpu hotplug threads manage the bringup and teardown of the cpus
 /*
static void cpuhp_create(unsigned int cpu)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);

	init_completion(&st->done);
}

static int cpuhp_should_run(unsigned int cpu)
{
	struct cpuhp_cpu_statest = this_cpu_ptr(&cpuhp_state);

	return st->should_run;
}

*/ Execute the teardown callbacks. Used to be CPU_DOWN_PREPARE /*
static int cpuhp_ap_offline(unsigned int cpu, struct cpuhp_cpu_statest)
{
	enum cpuhp_state target = max((int)st->target, CPUHP_TEARDOWN_CPU);

	return cpuhp_down_callbacks(cpu, st, cpuhp_ap_states, target);
}

*/ Execute the online startup callbacks. Used to be CPU_ONLINE /*
static int cpuhp_ap_online(unsigned int cpu, struct cpuhp_cpu_statest)
{
	return cpuhp_up_callbacks(cpu, st, cpuhp_ap_states, st->target);
}

*/
 Execute teardown/startup callbacks on the plugged cpu. Also used to invoke
 callbacks when a state gets [un]installed at runtime.
 /*
static void cpuhp_thread_fun(unsigned int cpu)
{
	struct cpuhp_cpu_statest = this_cpu_ptr(&cpuhp_state);
	int ret = 0;

	*/
	 Paired with the mb() in cpuhp_kick_ap_work and
	 cpuhp_invoke_ap_callback, so the work set is consistent visible.
	 /*
	smp_mb();
	if (!st->should_run)
		return;

	st->should_run = false;

	*/ Single callback invocation for [un]install ? /*
	if (st->cb) {
		if (st->cb_state < CPUHP_AP_ONLINE) {
			local_irq_disable();
			ret = cpuhp_invoke_callback(cpu, st->cb_state, st->cb);
			local_irq_enable();
		} else {
			ret = cpuhp_invoke_callback(cpu, st->cb_state, st->cb);
		}
	} else if (st->rollback) {
		BUG_ON(st->state < CPUHP_AP_ONLINE_IDLE);

		undo_cpu_down(cpu, st, cpuhp_ap_states);
		*/
		 This is a momentary workaround to keep the notifier users
		 happy. Will go away once we got rid of the notifiers.
		 /*
		cpu_notify_nofail(CPU_DOWN_FAILED, cpu);
		st->rollback = false;
	} else {
		*/ Cannot happen .... /*
		BUG_ON(st->state < CPUHP_AP_ONLINE_IDLE);

		*/ Regular hotplug work /*
		if (st->state < st->target)
			ret = cpuhp_ap_online(cpu, st);
		else if (st->state > st->target)
			ret = cpuhp_ap_offline(cpu, st);
	}
	st->result = ret;
	complete(&st->done);
}

*/ Invoke a single callback on a remote cpu /*
static int cpuhp_invoke_ap_callback(int cpu, enum cpuhp_state state,
				    int (*cb)(unsigned int))
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);

	if (!cpu_online(cpu))
		return 0;

	st->cb_state = state;
	st->cb = cb;
	*/
	 Make sure the above stores are visible before should_run becomes
	 true. Paired with the mb() above in cpuhp_thread_fun()
	 /*
	smp_mb();
	st->should_run = true;
	wake_up_process(st->thread);
	wait_for_completion(&st->done);
	return st->result;
}

*/ Regular hotplug invocation of the AP hotplug thread /*
static void __cpuhp_kick_ap_work(struct cpuhp_cpu_statest)
{
	st->result = 0;
	st->cb = NULL;
	*/
	 Make sure the above stores are visible before should_run becomes
	 true. Paired with the mb() above in cpuhp_thread_fun()
	 /*
	smp_mb();
	st->should_run = true;
	wake_up_process(st->thread);
}

static int cpuhp_kick_ap_work(unsigned int cpu)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
	enum cpuhp_state state = st->state;

	trace_cpuhp_enter(cpu, st->target, state, cpuhp_kick_ap_work);
	__cpuhp_kick_ap_work(st);
	wait_for_completion(&st->done);
	trace_cpuhp_exit(cpu, st->state, state, st->result);
	return st->result;
}

static struct smp_hotplug_thread cpuhp_threads = {
	.store			= &cpuhp_state.thread,
	.create			= &cpuhp_create,
	.thread_should_run	= cpuhp_should_run,
	.thread_fn		= cpuhp_thread_fun,
	.thread_comm		= "cpuhp/%u",
	.selfparking		= true,
};

void __init cpuhp_threads_init(void)
{
	BUG_ON(smpboot_register_percpu_thread(&cpuhp_threads));
	kthread_unpark(this_cpu_read(cpuhp_state.thread));
}

#ifdef CONFIG_HOTPLUG_CPU
EXPORT_SYMBOL(register_cpu_notifier);
EXPORT_SYMBOL(__register_cpu_notifier);
void unregister_cpu_notifier(struct notifier_blocknb)
{
	cpu_maps_update_begin();
	raw_notifier_chain_unregister(&cpu_chain, nb);
	cpu_maps_update_done();
}
EXPORT_SYMBOL(unregister_cpu_notifier);

void __unregister_cpu_notifier(struct notifier_blocknb)
{
	raw_notifier_chain_unregister(&cpu_chain, nb);
}
EXPORT_SYMBOL(__unregister_cpu_notifier);

*/
 clear_tasks_mm_cpumask - Safely clear tasks' mm_cpumask for a CPU
 @cpu: a CPU id

 This function walks all processes, finds a valid mm struct for each one and
 then clears a corresponding bit in mm's cpumask.  While this all sounds
 trivial, there are various non-obvious corner cases, which this function
 tries to solve in a safe manner.

 Also note that the function uses a somewhat relaxed locking scheme, so it may
 be called only for an already offlined CPU.
 /*
void clear_tasks_mm_cpumask(int cpu)
{
	struct task_structp;

	*/
	 This function is called after the cpu is taken down and marked
	 offline, so its not like new tasks will ever get this cpu set in
	 their mm mask. -- Peter Zijlstra
	 Thus, we may use rcu_read_lock() here, instead of grabbing
	 full-fledged tasklist_lock.
	 /*
	WARN_ON(cpu_online(cpu));
	rcu_read_lock();
	for_each_process(p) {
		struct task_structt;

		*/
		 Main thread might exit, but other threads may still have
		 a valid mm. Find one.
		 /*
		t = find_lock_task_mm(p);
		if (!t)
			continue;
		cpumask_clear_cpu(cpu, mm_cpumask(t->mm));
		task_unlock(t);
	}
	rcu_read_unlock();
}

static inline void check_for_tasks(int dead_cpu)
{
	struct task_structg,p;

	read_lock(&tasklist_lock);
	for_each_process_thread(g, p) {
		if (!p->on_rq)
			continue;
		*/
		 We do the check with unlocked task_rq(p)->lock.
		 Order the reading to do not warn about a task,
		 which was running on this cpu in the past, and
		 it's just been woken on another cpu.
		 /*
		rmb();
		if (task_cpu(p) != dead_cpu)
			continue;

		pr_warn("Task %s (pid=%d) is on cpu %d (state=%ld, flags=%x)\n",
			p->comm, task_pid_nr(p), dead_cpu, p->state, p->flags);
	}
	read_unlock(&tasklist_lock);
}

static int notify_down_prepare(unsigned int cpu)
{
	int err, nr_calls = 0;

	err = __cpu_notify(CPU_DOWN_PREPARE, cpu, -1, &nr_calls);
	if (err) {
		nr_calls--;
		__cpu_notify(CPU_DOWN_FAILED, cpu, nr_calls, NULL);
		pr_warn("%s: attempt to take down CPU %u failed\n",
				__func__, cpu);
	}
	return err;
}

static int notify_dying(unsigned int cpu)
{
	cpu_notify(CPU_DYING, cpu);
	return 0;
}

*/ Take this CPU down. /*
static int take_cpu_down(void_param)
{
	struct cpuhp_cpu_statest = this_cpu_ptr(&cpuhp_state);
	enum cpuhp_state target = max((int)st->target, CPUHP_AP_OFFLINE);
	int err, cpu = smp_processor_id();

	*/ Ensure this CPU doesn't handle any more interrupts. /*
	err = __cpu_disable();
	if (err < 0)
		return err;

	*/ Invoke the former CPU_DYING callbacks /*
	for (; st->state > target; st->state--) {
		struct cpuhp_stepstep = cpuhp_ap_states + st->state;

		cpuhp_invoke_callback(cpu, st->state, step->teardown);
	}
	*/ Give up timekeeping duties /*
	tick_handover_do_timer();
	*/ Park the stopper thread /*
	stop_machine_park(cpu);
	return 0;
}

static int takedown_cpu(unsigned int cpu)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
	int err;

	*/
	 By now we've cleared cpu_active_mask, wait for all preempt-disabled
	 and RCU users of this state to go away such that all new such users
	 will observe it.
	
	 For CONFIG_PREEMPT we have preemptible RCU and its sync_rcu() might
	 not imply sync_sched(), so wait for both.
	
	 Do sync before park smpboot threads to take care the rcu boost case.
	 /*
	if (IS_ENABLED(CONFIG_PREEMPT))
		synchronize_rcu_mult(call_rcu, call_rcu_sched);
	else
		synchronize_rcu();

	*/ Park the smpboot threads /*
	kthread_park(per_cpu_ptr(&cpuhp_state, cpu)->thread);
	smpboot_park_threads(cpu);

	*/
	 Prevent irq alloc/free while the dying cpu reorganizes the
	 interrupt affinities.
	 /*
	irq_lock_sparse();

	*/
	 So now all preempt/rcu users must observe !cpu_active().
	 /*
	err = stop_machine(take_cpu_down, NULL, cpumask_of(cpu));
	if (err) {
		*/ CPU refused to die /*
		irq_unlock_sparse();
		*/ Unpark the hotplug thread so we can rollback there /*
		kthread_unpark(per_cpu_ptr(&cpuhp_state, cpu)->thread);
		return err;
	}
	BUG_ON(cpu_online(cpu));

	*/
	 The migration_call() CPU_DYING callback will have removed all
	 runnable tasks from the cpu, there's only the idle task left now
	 that the migration thread is done doing the stop_machine thing.
	
	 Wait for the stop thread to go away.
	 /*
	wait_for_completion(&st->done);
	BUG_ON(st->state != CPUHP_AP_IDLE_DEAD);

	*/ Interrupts are moved away from the dying cpu, reenable alloc/free /*
	irq_unlock_sparse();

	hotplug_cpu__broadcast_tick_pull(cpu);
	*/ This actually kills the CPU. /*
	__cpu_die(cpu);

	tick_cleanup_dead_cpu(cpu);
	return 0;
}

static int notify_dead(unsigned int cpu)
{
	cpu_notify_nofail(CPU_DEAD, cpu);
	check_for_tasks(cpu);
	return 0;
}

static void cpuhp_complete_idle_dead(voidarg)
{
	struct cpuhp_cpu_statest = arg;

	complete(&st->done);
}

void cpuhp_report_idle_dead(void)
{
	struct cpuhp_cpu_statest = this_cpu_ptr(&cpuhp_state);

	BUG_ON(st->state != CPUHP_AP_OFFLINE);
	rcu_report_dead(smp_processor_id());
	st->state = CPUHP_AP_IDLE_DEAD;
	*/
	 We cannot call complete after rcu_report_dead() so we delegate it
	 to an online cpu.
	 /*
	smp_call_function_single(cpumask_first(cpu_online_mask),
				 cpuhp_complete_idle_dead, st, 0);
}

#else
#define notify_down_prepare	NULL
#define takedown_cpu		NULL
#define notify_dead		NULL
#define notify_dying		NULL
#endif

#ifdef CONFIG_HOTPLUG_CPU

*/ Requires cpu_add_remove_lock to be held /*
static int __ref _cpu_down(unsigned int cpu, int tasks_frozen,
			   enum cpuhp_state target)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
	int prev_state, ret = 0;
	bool hasdied = false;

	if (num_online_cpus() == 1)
		return -EBUSY;

	if (!cpu_present(cpu))
		return -EINVAL;

	cpu_hotplug_begin();

	cpuhp_tasks_frozen = tasks_frozen;

	prev_state = st->state;
	st->target = target;
	*/
	 If the current CPU state is in the range of the AP hotplug thread,
	 then we need to kick the thread.
	 /*
	if (st->state > CPUHP_TEARDOWN_CPU) {
		ret = cpuhp_kick_ap_work(cpu);
		*/
		 The AP side has done the error rollback already. Just
		 return the error code..
		 /*
		if (ret)
			goto out;

		*/
		 We might have stopped still in the range of the AP hotplug
		 thread. Nothing to do anymore.
		 /*
		if (st->state > CPUHP_TEARDOWN_CPU)
			goto out;
	}
	*/
	 The AP brought itself down to CPUHP_TEARDOWN_CPU. So we need
	 to do the further cleanups.
	 /*
	ret = cpuhp_down_callbacks(cpu, st, cpuhp_bp_states, target);
	if (ret && st->state > CPUHP_TEARDOWN_CPU && st->state < prev_state) {
		st->target = prev_state;
		st->rollback = true;
		cpuhp_kick_ap_work(cpu);
	}

	hasdied = prev_state != st->state && st->state == CPUHP_OFFLINE;
out:
	cpu_hotplug_done();
	*/ This post dead nonsense must die /*
	if (!ret && hasdied)
		cpu_notify_nofail(CPU_POST_DEAD, cpu);
	return ret;
}

static int do_cpu_down(unsigned int cpu, enum cpuhp_state target)
{
	int err;

	cpu_maps_update_begin();

	if (cpu_hotplug_disabled) {
		err = -EBUSY;
		goto out;
	}

	err = _cpu_down(cpu, 0, target);

out:
	cpu_maps_update_done();
	return err;
}
int cpu_down(unsigned int cpu)
{
	return do_cpu_down(cpu, CPUHP_OFFLINE);
}
EXPORT_SYMBOL(cpu_down);
#endif */CONFIG_HOTPLUG_CPU/*

*/
 notify_cpu_starting(cpu) - call the CPU_STARTING notifiers
 @cpu: cpu that just started

 This function calls the cpu_chain notifiers with CPU_STARTING.
 It must be called by the arch code on the new cpu, before the new cpu
 enables interrupts and before the "boot" cpu returns from __cpu_up().
 /*
void notify_cpu_starting(unsigned int cpu)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
	enum cpuhp_state target = min((int)st->target, CPUHP_AP_ONLINE);

	while (st->state < target) {
		struct cpuhp_stepstep;

		st->state++;
		step = cpuhp_ap_states + st->state;
		cpuhp_invoke_callback(cpu, st->state, step->startup);
	}
}

*/
 Called from the idle task. We need to set active here, so we can kick off
 the stopper thread and unpark the smpboot threads. If the target state is
 beyond CPUHP_AP_ONLINE_IDLE we kick cpuhp thread and let it bring up the
 cpu further.
 /*
void cpuhp_online_idle(enum cpuhp_state state)
{
	struct cpuhp_cpu_statest = this_cpu_ptr(&cpuhp_state);
	unsigned int cpu = smp_processor_id();

	*/ Happens for the boot cpu /*
	if (state != CPUHP_AP_ONLINE_IDLE)
		return;

	st->state = CPUHP_AP_ONLINE_IDLE;

	*/ The cpu is marked online, set it active now /*
	set_cpu_active(cpu, true);
	*/ Unpark the stopper thread and the hotplug thread of this cpu /*
	stop_machine_unpark(cpu);
	kthread_unpark(st->thread);

	*/ Should we go further up ? /*
	if (st->target > CPUHP_AP_ONLINE_IDLE)
		__cpuhp_kick_ap_work(st);
	else
		complete(&st->done);
}

*/ Requires cpu_add_remove_lock to be held /*
static int _cpu_up(unsigned int cpu, int tasks_frozen, enum cpuhp_state target)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
	struct task_structidle;
	int ret = 0;

	cpu_hotplug_begin();

	if (!cpu_present(cpu)) {
		ret = -EINVAL;
		goto out;
	}

	*/
	 The caller of do_cpu_up might have raced with another
	 caller. Ignore it for now.
	 /*
	if (st->state >= target)
		goto out;

	if (st->state == CPUHP_OFFLINE) {
		*/ Let it fail before we try to bring the cpu up /*
		idle = idle_thread_get(cpu);
		if (IS_ERR(idle)) {
			ret = PTR_ERR(idle);
			goto out;
		}
	}

	cpuhp_tasks_frozen = tasks_frozen;

	st->target = target;
	*/
	 If the current CPU state is in the range of the AP hotplug thread,
	 then we need to kick the thread once more.
	 /*
	if (st->state > CPUHP_BRINGUP_CPU) {
		ret = cpuhp_kick_ap_work(cpu);
		*/
		 The AP side has done the error rollback already. Just
		 return the error code..
		 /*
		if (ret)
			goto out;
	}

	*/
	 Try to reach the target state. We max out on the BP at
	 CPUHP_BRINGUP_CPU. After that the AP hotplug thread is
	 responsible for bringing it up to the target state.
	 /*
	target = min((int)target, CPUHP_BRINGUP_CPU);
	ret = cpuhp_up_callbacks(cpu, st, cpuhp_bp_states, target);
out:
	cpu_hotplug_done();
	return ret;
}

static int do_cpu_up(unsigned int cpu, enum cpuhp_state target)
{
	int err = 0;

	if (!cpu_possible(cpu)) {
		pr_err("can't online cpu %d because it is not configured as may-hotadd at boot time\n",
		       cpu);
#if defined(CONFIG_IA64)
		pr_err("please check additional_cpus= boot parameter\n");
#endif
		return -EINVAL;
	}

	err = try_online_node(cpu_to_node(cpu));
	if (err)
		return err;

	cpu_maps_update_begin();

	if (cpu_hotplug_disabled) {
		err = -EBUSY;
		goto out;
	}

	err = _cpu_up(cpu, 0, target);
out:
	cpu_maps_update_done();
	return err;
}

int cpu_up(unsigned int cpu)
{
	return do_cpu_up(cpu, CPUHP_ONLINE);
}
EXPORT_SYMBOL_GPL(cpu_up);

#ifdef CONFIG_PM_SLEEP_SMP
static cpumask_var_t frozen_cpus;

int disable_nonboot_cpus(void)
{
	int cpu, first_cpu, error = 0;

	cpu_maps_update_begin();
	first_cpu = cpumask_first(cpu_online_mask);
	*/
	 We take down all of the non-boot CPUs in one shot to avoid races
	 with the userspace trying to use the CPU hotplug at the same time
	 /*
	cpumask_clear(frozen_cpus);

	pr_info("Disabling non-boot CPUs ...\n");
	for_each_online_cpu(cpu) {
		if (cpu == first_cpu)
			continue;
		trace_suspend_resume(TPS("CPU_OFF"), cpu, true);
		error = _cpu_down(cpu, 1, CPUHP_OFFLINE);
		trace_suspend_resume(TPS("CPU_OFF"), cpu, false);
		if (!error)
			cpumask_set_cpu(cpu, frozen_cpus);
		else {
			pr_err("Error taking CPU%d down: %d\n", cpu, error);
			break;
		}
	}

	if (!error)
		BUG_ON(num_online_cpus() > 1);
	else
		pr_err("Non-boot CPUs are not disabled\n");

	*/
	 Make sure the CPUs won't be enabled by someone else. We need to do
	 this even in case of failure as all disable_nonboot_cpus() users are
	 supposed to do enable_nonboot_cpus() on the failure path.
	 /*
	cpu_hotplug_disabled++;

	cpu_maps_update_done();
	return error;
}

void __weak arch_enable_nonboot_cpus_begin(void)
{
}

void __weak arch_enable_nonboot_cpus_end(void)
{
}

void enable_nonboot_cpus(void)
{
	int cpu, error;

	*/ Allow everyone to use the CPU hotplug again /*
	cpu_maps_update_begin();
	WARN_ON(--cpu_hotplug_disabled < 0);
	if (cpumask_empty(frozen_cpus))
		goto out;

	pr_info("Enabling non-boot CPUs ...\n");

	arch_enable_nonboot_cpus_begin();

	for_each_cpu(cpu, frozen_cpus) {
		trace_suspend_resume(TPS("CPU_ON"), cpu, true);
		error = _cpu_up(cpu, 1, CPUHP_ONLINE);
		trace_suspend_resume(TPS("CPU_ON"), cpu, false);
		if (!error) {
			pr_info("CPU%d is up\n", cpu);
			continue;
		}
		pr_warn("Error taking CPU%d up: %d\n", cpu, error);
	}

	arch_enable_nonboot_cpus_end();

	cpumask_clear(frozen_cpus);
out:
	cpu_maps_update_done();
}

static int __init alloc_frozen_cpus(void)
{
	if (!alloc_cpumask_var(&frozen_cpus, GFP_KERNEL|__GFP_ZERO))
		return -ENOMEM;
	return 0;
}
core_initcall(alloc_frozen_cpus);

*/
 When callbacks for CPU hotplug notifications are being executed, we must
 ensure that the state of the system with respect to the tasks being frozen
 or not, as reported by the notification, remains unchangedthroughout the
 duration* of the execution of the callbacks.
 Hence we need to prevent the freezer from racing with regular CPU hotplug.

 This synchronization is implemented by mutually excluding regular CPU
 hotplug and Suspend/Hibernate call paths by hooking onto the Suspend/
 Hibernate notifications.
 /*
static int
cpu_hotplug_pm_callback(struct notifier_blocknb,
			unsigned long action, voidptr)
{
	switch (action) {

	case PM_SUSPEND_PREPARE:
	case PM_HIBERNATION_PREPARE:
		cpu_hotplug_disable();
		break;

	case PM_POST_SUSPEND:
	case PM_POST_HIBERNATION:
		cpu_hotplug_enable();
		break;

	default:
		return NOTIFY_DONE;
	}

	return NOTIFY_OK;
}


static int __init cpu_hotplug_pm_sync_init(void)
{
	*/
	 cpu_hotplug_pm_callback has higher priority than x86
	 bsp_pm_callback which depends on cpu_hotplug_pm_callback
	 to disable cpu hotplug to avoid cpu hotplug race.
	 /*
	pm_notifier(cpu_hotplug_pm_callback, 0);
	return 0;
}
core_initcall(cpu_hotplug_pm_sync_init);

#endif */ CONFIG_PM_SLEEP_SMP /*

#endif */ CONFIG_SMP /*

*/ Boot processor state steps /*
static struct cpuhp_step cpuhp_bp_states[] = {
	[CPUHP_OFFLINE] = {
		.name			= "offline",
		.startup		= NULL,
		.teardown		= NULL,
	},
#ifdef CONFIG_SMP
	[CPUHP_CREATE_THREADS]= {
		.name			= "threads:create",
		.startup		= smpboot_create_threads,
		.teardown		= NULL,
		.cant_stop		= true,
	},
	*/
	 Preparatory and dead notifiers. Will be replaced once the notifiers
	 are converted to states.
	 /*
	[CPUHP_NOTIFY_PREPARE] = {
		.name			= "notify:prepare",
		.startup		= notify_prepare,
		.teardown		= notify_dead,
		.skip_onerr		= true,
		.cant_stop		= true,
	},
	*/ Kicks the plugged cpu into life /*
	[CPUHP_BRINGUP_CPU] = {
		.name			= "cpu:bringup",
		.startup		= bringup_cpu,
		.teardown		= NULL,
		.cant_stop		= true,
	},
	*/
	 Handled on controll processor until the plugged processor manages
	 this itself.
	 /*
	[CPUHP_TEARDOWN_CPU] = {
		.name			= "cpu:teardown",
		.startup		= NULL,
		.teardown		= takedown_cpu,
		.cant_stop		= true,
	},
#endif
};

*/ Application processor state steps /*
static struct cpuhp_step cpuhp_ap_states[] = {
#ifdef CONFIG_SMP
	*/ Final state before CPU kills itself /*
	[CPUHP_AP_IDLE_DEAD] = {
		.name			= "idle:dead",
	},
	*/
	 Last state before CPU enters the idle loop to die. Transient state
	 for synchronization.
	 /*
	[CPUHP_AP_OFFLINE] = {
		.name			= "ap:offline",
		.cant_stop		= true,
	},
	*/
	 Low level startup/teardown notifiers. Run with interrupts
	 disabled. Will be removed once the notifiers are converted to
	 states.
	 /*
	[CPUHP_AP_NOTIFY_STARTING] = {
		.name			= "notify:starting",
		.startup		= notify_starting,
		.teardown		= notify_dying,
		.skip_onerr		= true,
		.cant_stop		= true,
	},
	*/ Entry state on starting. Interrupts enabled from here on. Transient
	 state for synchronsization /*
	[CPUHP_AP_ONLINE] = {
		.name			= "ap:online",
	},
	*/ Handle smpboot threads park/unpark /*
	[CPUHP_AP_SMPBOOT_THREADS] = {
		.name			= "smpboot:threads",
		.startup		= smpboot_unpark_threads,
		.teardown		= NULL,
	},
	*/
	 Online/down_prepare notifiers. Will be removed once the notifiers
	 are converted to states.
	 /*
	[CPUHP_AP_NOTIFY_ONLINE] = {
		.name			= "notify:online",
		.startup		= notify_online,
		.teardown		= notify_down_prepare,
		.skip_onerr		= true,
	},
#endif
	*/
	 The dynamically registered state space is here
	 /*

	*/ CPU is fully up and running. /*
	[CPUHP_ONLINE] = {
		.name			= "online",
		.startup		= NULL,
		.teardown		= NULL,
	},
};

*/ Sanity check for callbacks /*
static int cpuhp_cb_check(enum cpuhp_state state)
{
	if (state <= CPUHP_OFFLINE || state >= CPUHP_ONLINE)
		return -EINVAL;
	return 0;
}

static bool cpuhp_is_ap_state(enum cpuhp_state state)
{
	*/
	 The extra check for CPUHP_TEARDOWN_CPU is only for documentation
	 purposes as that state is handled explicitely in cpu_down.
	 /*
	return state > CPUHP_BRINGUP_CPU && state != CPUHP_TEARDOWN_CPU;
}

static struct cpuhp_stepcpuhp_get_step(enum cpuhp_state state)
{
	struct cpuhp_stepsp;

	sp = cpuhp_is_ap_state(state) ? cpuhp_ap_states : cpuhp_bp_states;
	return sp + state;
}

static void cpuhp_store_callbacks(enum cpuhp_state state,
				  const charname,
				  int (*startup)(unsigned int cpu),
				  int (*teardown)(unsigned int cpu))
{
	*/ (Un)Install the callbacks for further cpu hotplug operations /*
	struct cpuhp_stepsp;

	mutex_lock(&cpuhp_state_mutex);
	sp = cpuhp_get_step(state);
	sp->startup = startup;
	sp->teardown = teardown;
	sp->name = name;
	mutex_unlock(&cpuhp_state_mutex);
}

static voidcpuhp_get_teardown_cb(enum cpuhp_state state)
{
	return cpuhp_get_step(state)->teardown;
}

*/
 Call the startup/teardown function for a step either on the AP or
 on the current CPU.
 /*
static int cpuhp_issue_call(int cpu, enum cpuhp_state state,
			    int (*cb)(unsigned int), bool bringup)
{
	int ret;

	if (!cb)
		return 0;
	*/
	 The non AP bound callbacks can fail on bringup. On teardown
	 e.g. module removal we crash for now.
	 /*
#ifdef CONFIG_SMP
	if (cpuhp_is_ap_state(state))
		ret = cpuhp_invoke_ap_callback(cpu, state, cb);
	else
		ret = cpuhp_invoke_callback(cpu, state, cb);
#else
	ret = cpuhp_invoke_callback(cpu, state, cb);
#endif
	BUG_ON(ret && !bringup);
	return ret;
}

*/
 Called from __cpuhp_setup_state on a recoverable failure.

 Note: The teardown callbacks for rollback are not allowed to fail!
 /*
static void cpuhp_rollback_install(int failedcpu, enum cpuhp_state state,
				   int (*teardown)(unsigned int cpu))
{
	int cpu;

	if (!teardown)
		return;

	*/ Roll back the already executed steps on the other cpus /*
	for_each_present_cpu(cpu) {
		struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
		int cpustate = st->state;

		if (cpu >= failedcpu)
			break;

		*/ Did we invoke the startup call on that cpu ? /*
		if (cpustate >= state)
			cpuhp_issue_call(cpu, state, teardown, false);
	}
}

*/
 Returns a free for dynamic slot assignment of the Online state. The states
 are protected by the cpuhp_slot_states mutex and an empty slot is identified
 by having no name assigned.
 /*
static int cpuhp_reserve_state(enum cpuhp_state state)
{
	enum cpuhp_state i;

	mutex_lock(&cpuhp_state_mutex);
	for (i = CPUHP_AP_ONLINE_DYN; i <= CPUHP_AP_ONLINE_DYN_END; i++) {
		if (cpuhp_ap_states[i].name)
			continue;

		cpuhp_ap_states[i].name = "Reserved";
		mutex_unlock(&cpuhp_state_mutex);
		return i;
	}
	mutex_unlock(&cpuhp_state_mutex);
	WARN(1, "No more dynamic states available for CPU hotplug\n");
	return -ENOSPC;
}

*/
 __cpuhp_setup_state - Setup the callbacks for an hotplug machine state
 @state:	The state to setup
 @invoke:	If true, the startup function is invoked for cpus where
		cpu state >= @state
 @startup:	startup callback function
 @teardown:	teardown callback function

 Returns 0 if successful, otherwise a proper error code
 /*
int __cpuhp_setup_state(enum cpuhp_state state,
			const charname, bool invoke,
			int (*startup)(unsigned int cpu),
			int (*teardown)(unsigned int cpu))
{
	int cpu, ret = 0;
	int dyn_state = 0;

	if (cpuhp_cb_check(state) || !name)
		return -EINVAL;

	get_online_cpus();

	*/ currently assignments for the ONLINE state are possible /*
	if (state == CPUHP_AP_ONLINE_DYN) {
		dyn_state = 1;
		ret = cpuhp_reserve_state(state);
		if (ret < 0)
			goto out;
		state = ret;
	}

	cpuhp_store_callbacks(state, name, startup, teardown);

	if (!invoke || !startup)
		goto out;

	*/
	 Try to call the startup callback for each present cpu
	 depending on the hotplug state of the cpu.
	 /*
	for_each_present_cpu(cpu) {
		struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
		int cpustate = st->state;

		if (cpustate < state)
			continue;

		ret = cpuhp_issue_call(cpu, state, startup, true);
		if (ret) {
			cpuhp_rollback_install(cpu, state, teardown);
			cpuhp_store_callbacks(state, NULL, NULL, NULL);
			goto out;
		}
	}
out:
	put_online_cpus();
	if (!ret && dyn_state)
		return state;
	return ret;
}
EXPORT_SYMBOL(__cpuhp_setup_state);

*/
 __cpuhp_remove_state - Remove the callbacks for an hotplug machine state
 @state:	The state to remove
 @invoke:	If true, the teardown function is invoked for cpus where
		cpu state >= @state

 The teardown callback is currently not allowed to fail. Think
 about module removal!
 /*
void __cpuhp_remove_state(enum cpuhp_state state, bool invoke)
{
	int (*teardown)(unsigned int cpu) = cpuhp_get_teardown_cb(state);
	int cpu;

	BUG_ON(cpuhp_cb_check(state));

	get_online_cpus();

	if (!invoke || !teardown)
		goto remove;

	*/
	 Call the teardown callback for each present cpu depending
	 on the hotplug state of the cpu. This function is not
	 allowed to fail currently!
	 /*
	for_each_present_cpu(cpu) {
		struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, cpu);
		int cpustate = st->state;

		if (cpustate >= state)
			cpuhp_issue_call(cpu, state, teardown, false);
	}
remove:
	cpuhp_store_callbacks(state, NULL, NULL, NULL);
	put_online_cpus();
}
EXPORT_SYMBOL(__cpuhp_remove_state);

#if defined(CONFIG_SYSFS) && defined(CONFIG_HOTPLUG_CPU)
static ssize_t show_cpuhp_state(struct devicedev,
				struct device_attributeattr, charbuf)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, dev->id);

	return sprintf(buf, "%d\n", st->state);
}
static DEVICE_ATTR(state, 0444, show_cpuhp_state, NULL);

static ssize_t write_cpuhp_target(struct devicedev,
				  struct device_attributeattr,
				  const charbuf, size_t count)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, dev->id);
	struct cpuhp_stepsp;
	int target, ret;

	ret = kstrtoint(buf, 10, &target);
	if (ret)
		return ret;

#ifdef CONFIG_CPU_HOTPLUG_STATE_CONTROL
	if (target < CPUHP_OFFLINE || target > CPUHP_ONLINE)
		return -EINVAL;
#else
	if (target != CPUHP_OFFLINE && target != CPUHP_ONLINE)
		return -EINVAL;
#endif

	ret = lock_device_hotplug_sysfs();
	if (ret)
		return ret;

	mutex_lock(&cpuhp_state_mutex);
	sp = cpuhp_get_step(target);
	ret = !sp->name || sp->cant_stop ? -EINVAL : 0;
	mutex_unlock(&cpuhp_state_mutex);
	if (ret)
		return ret;

	if (st->state < target)
		ret = do_cpu_up(dev->id, target);
	else
		ret = do_cpu_down(dev->id, target);

	unlock_device_hotplug();
	return ret ? ret : count;
}

static ssize_t show_cpuhp_target(struct devicedev,
				 struct device_attributeattr, charbuf)
{
	struct cpuhp_cpu_statest = per_cpu_ptr(&cpuhp_state, dev->id);

	return sprintf(buf, "%d\n", st->target);
}
static DEVICE_ATTR(target, 0644, show_cpuhp_target, write_cpuhp_target);

static struct attributecpuhp_cpu_attrs[] = {
	&dev_attr_state.attr,
	&dev_attr_target.attr,
	NULL
};

static struct attribute_group cpuhp_cpu_attr_group = {
	.attrs = cpuhp_cpu_attrs,
	.name = "hotplug",
	NULL
};

static ssize_t show_cpuhp_states(struct devicedev,
				 struct device_attributeattr, charbuf)
{
	ssize_t cur, res = 0;
	int i;

	mutex_lock(&cpuhp_state_mutex);
	for (i = CPUHP_OFFLINE; i <= CPUHP_ONLINE; i++) {
		struct cpuhp_stepsp = cpuhp_get_step(i);

		if (sp->name) {
			cur = sprintf(buf, "%3d: %s\n", i, sp->name);
			buf += cur;
			res += cur;
		}
	}
	mutex_unlock(&cpuhp_state_mutex);
	return res;
}
static DEVICE_ATTR(states, 0444, show_cpuhp_states, NULL);

static struct attributecpuhp_cpu_root_attrs[] = {
	&dev_attr_states.attr,
	NULL
};

static struct attribute_group cpuhp_cpu_root_attr_group = {
	.attrs = cpuhp_cpu_root_attrs,
	.name = "hotplug",
	NULL
};

static int __init cpuhp_sysfs_init(void)
{
	int cpu, ret;

	ret = sysfs_create_group(&cpu_subsys.dev_root->kobj,
				 &cpuhp_cpu_root_attr_group);
	if (ret)
		return ret;

	for_each_possible_cpu(cpu) {
		struct devicedev = get_cpu_device(cpu);

		if (!dev)
			continue;
		ret = sysfs_create_group(&dev->kobj, &cpuhp_cpu_attr_group);
		if (ret)
			return ret;
	}
	return 0;
}
device_initcall(cpuhp_sysfs_init);
#endif

*/
 cpu_bit_bitmap[] is a special, "compressed" data structure that
 represents all NR_CPUS bits binary values of 1<<nr.

 It is used by cpumask_of() to get a constant address to a CPU
 mask value that has a single bit set only.
 /*

*/ cpu_bit_bitmap[0] is empty - so we can back into it /*
#define MASK_DECLARE_1(x)	[x+1][0] = (1UL << (x))
#define MASK_DECLARE_2(x)	MASK_DECLARE_1(x), MASK_DECLARE_1(x+1)
#define MASK_DECLARE_4(x)	MASK_DECLARE_2(x), MASK_DECLARE_2(x+2)
#define MASK_DECLARE_8(x)	MASK_DECLARE_4(x), MASK_DECLARE_4(x+4)

const unsigned long cpu_bit_bitmap[BITS_PER_LONG+1][BITS_TO_LONGS(NR_CPUS)] = {

	MASK_DECLARE_8(0),	MASK_DECLARE_8(8),
	MASK_DECLARE_8(16),	MASK_DECLARE_8(24),
#if BITS_PER_LONG > 32
	MASK_DECLARE_8(32),	MASK_DECLARE_8(40),
	MASK_DECLARE_8(48),	MASK_DECLARE_8(56),
#endif
};
EXPORT_SYMBOL_GPL(cpu_bit_bitmap);

const DECLARE_BITMAP(cpu_all_bits, NR_CPUS) = CPU_BITS_ALL;
EXPORT_SYMBOL(cpu_all_bits);

#ifdef CONFIG_INIT_ALL_POSSIBLE
struct cpumask __cpu_possible_mask __read_mostly
	= {CPU_BITS_ALL};
#else
struct cpumask __cpu_possible_mask __read_mostly;
#endif
EXPORT_SYMBOL(__cpu_possible_mask);

struct cpumask __cpu_online_mask __read_mostly;
EXPORT_SYMBOL(__cpu_online_mask);

struct cpumask __cpu_present_mask __read_mostly;
EXPORT_SYMBOL(__cpu_present_mask);

struct cpumask __cpu_active_mask __read_mostly;
EXPORT_SYMBOL(__cpu_active_mask);

void init_cpu_present(const struct cpumasksrc)
{
	cpumask_copy(&__cpu_present_mask, src);
}

void init_cpu_possible(const struct cpumasksrc)
{
	cpumask_copy(&__cpu_possible_mask, src);
}

void init_cpu_online(const struct cpumasksrc)
{
	cpumask_copy(&__cpu_online_mask, src);
}

*/
 Activate the first processor.
 /*
void __init boot_cpu_init(void)
{
	int cpu = smp_processor_id();

	*/ Mark the boot cpu "present", "online" etc for SMP and UP case /*
	set_cpu_online(cpu, true);
	set_cpu_active(cpu, true);
	set_cpu_present(cpu, true);
	set_cpu_possible(cpu, true);
}

*/
 Must be called _AFTER_ setting up the per_cpu areas
 /*
void __init boot_cpu_state_init(void)
{
	per_cpu_ptr(&cpuhp_state, smp_processor_id())->state = CPUHP_ONLINE;
}
*/

 Copyright (C) 2011 Google, Inc.

 Author:
	Colin Cross <ccross@android.com>

 This software is licensed under the terms of the GNU General Public
 License version 2, as published by the Free Software Foundation, and
 may be copied, distributed, and modified under those terms.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 /*

#include <linux/kernel.h>
#include <linux/cpu_pm.h>
#include <linux/module.h>
#include <linux/notifier.h>
#include <linux/spinlock.h>
#include <linux/syscore_ops.h>

static DEFINE_RWLOCK(cpu_pm_notifier_lock);
static RAW_NOTIFIER_HEAD(cpu_pm_notifier_chain);

static int cpu_pm_notify(enum cpu_pm_event event, int nr_to_call, intnr_calls)
{
	int ret;

	ret = __raw_notifier_call_chain(&cpu_pm_notifier_chain, event, NULL,
		nr_to_call, nr_calls);

	return notifier_to_errno(ret);
}

*/
 cpu_pm_register_notifier - register a driver with cpu_pm
 @nb: notifier block to register

 Add a driver to a list of drivers that are notified about
 CPU and CPU cluster low power entry and exit.

 This function may sleep, and has the same return conditions as
 raw_notifier_chain_register.
 /*
int cpu_pm_register_notifier(struct notifier_blocknb)
{
	unsigned long flags;
	int ret;

	write_lock_irqsave(&cpu_pm_notifier_lock, flags);
	ret = raw_notifier_chain_register(&cpu_pm_notifier_chain, nb);
	write_unlock_irqrestore(&cpu_pm_notifier_lock, flags);

	return ret;
}
EXPORT_SYMBOL_GPL(cpu_pm_register_notifier);

*/
 cpu_pm_unregister_notifier - unregister a driver with cpu_pm
 @nb: notifier block to be unregistered

 Remove a driver from the CPU PM notifier list.

 This function may sleep, and has the same return conditions as
 raw_notifier_chain_unregister.
 /*
int cpu_pm_unregister_notifier(struct notifier_blocknb)
{
	unsigned long flags;
	int ret;

	write_lock_irqsave(&cpu_pm_notifier_lock, flags);
	ret = raw_notifier_chain_unregister(&cpu_pm_notifier_chain, nb);
	write_unlock_irqrestore(&cpu_pm_notifier_lock, flags);

	return ret;
}
EXPORT_SYMBOL_GPL(cpu_pm_unregister_notifier);

*/
 cpu_pm_enter - CPU low power entry notifier

 Notifies listeners that a single CPU is entering a low power state that may
 cause some blocks in the same power domain as the cpu to reset.

 Must be called on the affected CPU with interrupts disabled.  Platform is
 responsible for ensuring that cpu_pm_enter is not called twice on the same
 CPU before cpu_pm_exit is called. Notified drivers can include VFP
 co-processor, interrupt controller and its PM extensions, local CPU
 timers context save/restore which shouldn't be interrupted. Hence it
 must be called with interrupts disabled.

 Return conditions are same as __raw_notifier_call_chain.
 /*
int cpu_pm_enter(void)
{
	int nr_calls;
	int ret = 0;

	read_lock(&cpu_pm_notifier_lock);
	ret = cpu_pm_notify(CPU_PM_ENTER, -1, &nr_calls);
	if (ret)
		*/
		 Inform listeners (nr_calls - 1) about failure of CPU PM
		 PM entry who are notified earlier to prepare for it.
		 /*
		cpu_pm_notify(CPU_PM_ENTER_FAILED, nr_calls - 1, NULL);
	read_unlock(&cpu_pm_notifier_lock);

	return ret;
}
EXPORT_SYMBOL_GPL(cpu_pm_enter);

*/
 cpu_pm_exit - CPU low power exit notifier

 Notifies listeners that a single CPU is exiting a low power state that may
 have caused some blocks in the same power domain as the cpu to reset.

 Notified drivers can include VFP co-processor, interrupt controller
 and its PM extensions, local CPU timers context save/restore which
 shouldn't be interrupted. Hence it must be called with interrupts disabled.

 Return conditions are same as __raw_notifier_call_chain.
 /*
int cpu_pm_exit(void)
{
	int ret;

	read_lock(&cpu_pm_notifier_lock);
	ret = cpu_pm_notify(CPU_PM_EXIT, -1, NULL);
	read_unlock(&cpu_pm_notifier_lock);

	return ret;
}
EXPORT_SYMBOL_GPL(cpu_pm_exit);

*/
 cpu_cluster_pm_enter - CPU cluster low power entry notifier

 Notifies listeners that all cpus in a power domain are entering a low power
 state that may cause some blocks in the same power domain to reset.

 Must be called after cpu_pm_enter has been called on all cpus in the power
 domain, and before cpu_pm_exit has been called on any cpu in the power
 domain. Notified drivers can include VFP co-processor, interrupt controller
 and its PM extensions, local CPU timers context save/restore which
 shouldn't be interrupted. Hence it must be called with interrupts disabled.

 Must be called with interrupts disabled.

 Return conditions are same as __raw_notifier_call_chain.
 /*
int cpu_cluster_pm_enter(void)
{
	int nr_calls;
	int ret = 0;

	read_lock(&cpu_pm_notifier_lock);
	ret = cpu_pm_notify(CPU_CLUSTER_PM_ENTER, -1, &nr_calls);
	if (ret)
		*/
		 Inform listeners (nr_calls - 1) about failure of CPU cluster
		 PM entry who are notified earlier to prepare for it.
		 /*
		cpu_pm_notify(CPU_CLUSTER_PM_ENTER_FAILED, nr_calls - 1, NULL);
	read_unlock(&cpu_pm_notifier_lock);

	return ret;
}
EXPORT_SYMBOL_GPL(cpu_cluster_pm_enter);

*/
 cpu_cluster_pm_exit - CPU cluster low power exit notifier

 Notifies listeners that all cpus in a power domain are exiting form a
 low power state that may have caused some blocks in the same power domain
 to reset.

 Must be called after cpu_cluster_pm_enter has been called for the power
 domain, and before cpu_pm_exit has been called on any cpu in the power
 domain. Notified drivers can include VFP co-processor, interrupt controller
 and its PM extensions, local CPU timers context save/restore which
 shouldn't be interrupted. Hence it must be called with interrupts disabled.

 Return conditions are same as __raw_notifier_call_chain.
 /*
int cpu_cluster_pm_exit(void)
{
	int ret;

	read_lock(&cpu_pm_notifier_lock);
	ret = cpu_pm_notify(CPU_CLUSTER_PM_EXIT, -1, NULL);
	read_unlock(&cpu_pm_notifier_lock);

	return ret;
}
EXPORT_SYMBOL_GPL(cpu_cluster_pm_exit);

#ifdef CONFIG_PM
static int cpu_pm_suspend(void)
{
	int ret;

	ret = cpu_pm_enter();
	if (ret)
		return ret;

	ret = cpu_cluster_pm_enter();
	return ret;
}

static void cpu_pm_resume(void)
{
	cpu_cluster_pm_exit();
	cpu_pm_exit();
}

static struct syscore_ops cpu_pm_syscore_ops = {
	.suspend = cpu_pm_suspend,
	.resume = cpu_pm_resume,
};

static int cpu_pm_init(void)
{
	register_syscore_ops(&cpu_pm_syscore_ops);
	return 0;
}
core_initcall(cpu_pm_init);
#endif
*/
