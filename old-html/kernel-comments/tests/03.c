
  kernel/cpuset.c

  Processor and Memory placement constraints for sets of tasks.

  Copyright (C) 2003 BULL SA.
  Copyright (C) 2004-2007 Silicon Graphics, Inc.
  Copyright (C) 2006 Google, Inc

  Portions derived from Patrick Mochel's sysfs code.
  sysfs is Copyright (c) 2001-3 Patrick Mochel

  2003-10-10 Written by Simon Derr.
  2003-10-22 Updates by Stephen Hemminger.
  2004 May-July Rework by Paul Jackson.
  2006 Rework by Paul Menage to use generic cgroups
  2008 Rework of the scheduler domains and CPU hotplug handling
       by Max Krasnyansky

  This file is subject to the terms and conditions of the GNU General Public
  License.  See the file COPYING in the main directory of the Linux
  distribution for more details.
 /*

#include <linux/cpu.h>
#include <linux/cpumask.h>
#include <linux/cpuset.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/kmod.h>
#include <linux/list.h>
#include <linux/mempolicy.h>
#include <linux/mm.h>
#include <linux/memory.h>
#include <linux/export.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/pagemap.h>
#include <linux/proc_fs.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/security.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/stat.h>
#include <linux/string.h>
#include <linux/time.h>
#include <linux/time64.h>
#include <linux/backing-dev.h>
#include <linux/sort.h>

#include <asm/uaccess.h>
#include <linux/atomic.h>
#include <linux/mutex.h>
#include <linux/cgroup.h>
#include <linux/wait.h>

struct static_key cpusets_enabled_key __read_mostly = STATIC_KEY_INIT_FALSE;

*/ See "Frequency meter" comments, below. /*

struct fmeter {
	int cnt;		*/ unprocessed events count /*
	int val;		*/ most recent output value /*
	time64_t time;		*/ clock (secs) when val computed /*
	spinlock_t lock;	*/ guards read or write of above /*
};

struct cpuset {
	struct cgroup_subsys_state css;

	unsigned long flags;		*/ "unsigned long" so bitops work /*

	*/
	 On default hierarchy:
	
	 The user-configured masks can only be changed by writing to
	 cpuset.cpus and cpuset.mems, and won't be limited by the
	 parent masks.
	
	 The effective masks is the real masks that apply to the tasks
	 in the cpuset. They may be changed if the configured masks are
	 changed or hotplug happens.
	
	 effective_mask == configured_mask & parent's effective_mask,
	 and if it ends up empty, it will inherit the parent's mask.
	
	
	 On legacy hierachy:
	
	 The user-configured masks are always the same with effective masks.
	 /*

	*/ user-configured CPUs and Memory Nodes allow to tasks /*
	cpumask_var_t cpus_allowed;
	nodemask_t mems_allowed;

	*/ effective CPUs and Memory Nodes allow to tasks /*
	cpumask_var_t effective_cpus;
	nodemask_t effective_mems;

	*/
	 This is old Memory Nodes tasks took on.
	
	 - top_cpuset.old_mems_allowed is initialized to mems_allowed.
	 - A new cpuset's old_mems_allowed is initialized when some
	   task is moved into it.
	 - old_mems_allowed is used in cpuset_migrate_mm() when we change
	   cpuset.mems_allowed and have tasks' nodemask updated, and
	   then old_mems_allowed is updated to mems_allowed.
	 /*
	nodemask_t old_mems_allowed;

	struct fmeter fmeter;		*/ memory_pressure filter /*

	*/
	 Tasks are being attached to this cpuset.  Used to prevent
	 zeroing cpus/mems_allowed between ->can_attach() and ->attach().
	 /*
	int attach_in_progress;

	*/ partition number for rebuild_sched_domains() /*
	int pn;

	*/ for custom sched domain /*
	int relax_domain_level;
};

static inline struct cpusetcss_cs(struct cgroup_subsys_statecss)
{
	return css ? container_of(css, struct cpuset, css) : NULL;
}

*/ Retrieve the cpuset for a task /*
static inline struct cpusettask_cs(struct task_structtask)
{
	return css_cs(task_css(task, cpuset_cgrp_id));
}

static inline struct cpusetparent_cs(struct cpusetcs)
{
	return css_cs(cs->css.parent);
}

#ifdef CONFIG_NUMA
static inline bool task_has_mempolicy(struct task_structtask)
{
	return task->mempolicy;
}
#else
static inline bool task_has_mempolicy(struct task_structtask)
{
	return false;
}
#endif


*/ bits in struct cpuset flags field /*
typedef enum {
	CS_ONLINE,
	CS_CPU_EXCLUSIVE,
	CS_MEM_EXCLUSIVE,
	CS_MEM_HARDWALL,
	CS_MEMORY_MIGRATE,
	CS_SCHED_LOAD_BALANCE,
	CS_SPREAD_PAGE,
	CS_SPREAD_SLAB,
} cpuset_flagbits_t;

*/ convenient tests for these bits /*
static inline bool is_cpuset_online(const struct cpusetcs)
{
	return test_bit(CS_ONLINE, &cs->flags);
}

static inline int is_cpu_exclusive(const struct cpusetcs)
{
	return test_bit(CS_CPU_EXCLUSIVE, &cs->flags);
}

static inline int is_mem_exclusive(const struct cpusetcs)
{
	return test_bit(CS_MEM_EXCLUSIVE, &cs->flags);
}

static inline int is_mem_hardwall(const struct cpusetcs)
{
	return test_bit(CS_MEM_HARDWALL, &cs->flags);
}

static inline int is_sched_load_balance(const struct cpusetcs)
{
	return test_bit(CS_SCHED_LOAD_BALANCE, &cs->flags);
}

static inline int is_memory_migrate(const struct cpusetcs)
{
	return test_bit(CS_MEMORY_MIGRATE, &cs->flags);
}

static inline int is_spread_page(const struct cpusetcs)
{
	return test_bit(CS_SPREAD_PAGE, &cs->flags);
}

static inline int is_spread_slab(const struct cpusetcs)
{
	return test_bit(CS_SPREAD_SLAB, &cs->flags);
}

static struct cpuset top_cpuset = {
	.flags = ((1 << CS_ONLINE) | (1 << CS_CPU_EXCLUSIVE) |
		  (1 << CS_MEM_EXCLUSIVE)),
};

*/
 cpuset_for_each_child - traverse online children of a cpuset
 @child_cs: loop cursor pointing to the current child
 @pos_css: used for iteration
 @parent_cs: target cpuset to walk children of

 Walk @child_cs through the online children of @parent_cs.  Must be used
 with RCU read locked.
 /*
#define cpuset_for_each_child(child_cs, pos_css, parent_cs)		\
	css_for_each_child((pos_css), &(parent_cs)->css)		\
		if (is_cpuset_online(((child_cs) = css_cs((pos_css)))))

*/
 cpuset_for_each_descendant_pre - pre-order walk of a cpuset's descendants
 @des_cs: loop cursor pointing to the current descendant
 @pos_css: used for iteration
 @root_cs: target cpuset to walk ancestor of

 Walk @des_cs through the online descendants of @root_cs.  Must be used
 with RCU read locked.  The caller may modify @pos_css by calling
 css_rightmost_descendant() to skip subtree.  @root_cs is included in the
 iteration and the first node to be visited.
 /*
#define cpuset_for_each_descendant_pre(des_cs, pos_css, root_cs)	\
	css_for_each_descendant_pre((pos_css), &(root_cs)->css)		\
		if (is_cpuset_online(((des_cs) = css_cs((pos_css)))))

*/
 There are two global locks guarding cpuset structures - cpuset_mutex and
 callback_lock. We also require taking task_lock() when dereferencing a
 task's cpuset pointer. See "The task_lock() exception", at the end of this
 comment.

 A task must hold both locks to modify cpusets.  If a task holds
 cpuset_mutex, then it blocks others wanting that mutex, ensuring that it
 is the only task able to also acquire callback_lock and be able to
 modify cpusets.  It can perform various checks on the cpuset structure
 first, knowing nothing will change.  It can also allocate memory while
 just holding cpuset_mutex.  While it is performing these checks, various
 callback routines can briefly acquire callback_lock to query cpusets.
 Once it is ready to make the changes, it takes callback_lock, blocking
 everyone else.

 Calls to the kernel memory allocator can not be made while holding
 callback_lock, as that would risk double tripping on callback_lock
 from one of the callbacks into the cpuset code from within
 __alloc_pages().

 If a task is only holding callback_lock, then it has read-only
 access to cpusets.

 Now, the task_struct fields mems_allowed and mempolicy may be changed
 by other task, we use alloc_lock in the task_struct fields to protect
 them.

 The cpuset_common_file_read() handlers only hold callback_lock across
 small pieces of code, such as when reading out possibly multi-word
 cpumasks and nodemasks.

 Accessing a task's cpuset should be done in accordance with the
 guidelines for accessing subsystem state in kernel/cgroup.c
 /*

static DEFINE_MUTEX(cpuset_mutex);
static DEFINE_SPINLOCK(callback_lock);

static struct workqueue_structcpuset_migrate_mm_wq;

*/
 CPU / memory hotplug is handled asynchronously.
 /*
static void cpuset_hotplug_workfn(struct work_structwork);
static DECLARE_WORK(cpuset_hotplug_work, cpuset_hotplug_workfn);

static DECLARE_WAIT_QUEUE_HEAD(cpuset_attach_wq);

*/
 This is ugly, but preserves the userspace API for existing cpuset
 users. If someone tries to mount the "cpuset" filesystem, we
 silently switch it to mount "cgroup" instead
 /*
static struct dentrycpuset_mount(struct file_system_typefs_type,
			 int flags, const charunused_dev_name, voiddata)
{
	struct file_system_typecgroup_fs = get_fs_type("cgroup");
	struct dentryret = ERR_PTR(-ENODEV);
	if (cgroup_fs) {
		char mountopts[] =
			"cpuset,noprefix,"
			"release_agent=/sbin/cpuset_release_agent";
		ret = cgroup_fs->mount(cgroup_fs, flags,
					   unused_dev_name, mountopts);
		put_filesystem(cgroup_fs);
	}
	return ret;
}

static struct file_system_type cpuset_fs_type = {
	.name = "cpuset",
	.mount = cpuset_mount,
};

*/
 Return in pmask the portion of a cpusets's cpus_allowed that
 are online.  If none are online, walk up the cpuset hierarchy
 until we find one that does have some online cpus.  The top
 cpuset always has some cpus online.

 One way or another, we guarantee to return some non-empty subset
 of cpu_online_mask.

 Call with callback_lock or cpuset_mutex held.
 /*
static void guarantee_online_cpus(struct cpusetcs, struct cpumaskpmask)
{
	while (!cpumask_intersects(cs->effective_cpus, cpu_online_mask))
		cs = parent_cs(cs);
	cpumask_and(pmask, cs->effective_cpus, cpu_online_mask);
}

*/
 Return inpmask the portion of a cpusets's mems_allowed that
 are online, with memory.  If none are online with memory, walk
 up the cpuset hierarchy until we find one that does have some
 online mems.  The top cpuset always has some mems online.

 One way or another, we guarantee to return some non-empty subset
 of node_states[N_MEMORY].

 Call with callback_lock or cpuset_mutex held.
 /*
static void guarantee_online_mems(struct cpusetcs, nodemask_tpmask)
{
	while (!nodes_intersects(cs->effective_mems, node_states[N_MEMORY]))
		cs = parent_cs(cs);
	nodes_and(*pmask, cs->effective_mems, node_states[N_MEMORY]);
}

*/
 update task's spread flag if cpuset's page/slab spread flag is set

 Call with callback_lock or cpuset_mutex held.
 /*
static void cpuset_update_task_spread_flag(struct cpusetcs,
					struct task_structtsk)
{
	if (is_spread_page(cs))
		task_set_spread_page(tsk);
	else
		task_clear_spread_page(tsk);

	if (is_spread_slab(cs))
		task_set_spread_slab(tsk);
	else
		task_clear_spread_slab(tsk);
}

*/
 is_cpuset_subset(p, q) - Is cpuset p a subset of cpuset q?

 One cpuset is a subset of another if all its allowed CPUs and
 Memory Nodes are a subset of the other, and its exclusive flags
 are only set if the other's are set.  Call holding cpuset_mutex.
 /*

static int is_cpuset_subset(const struct cpusetp, const struct cpusetq)
{
	return	cpumask_subset(p->cpus_allowed, q->cpus_allowed) &&
		nodes_subset(p->mems_allowed, q->mems_allowed) &&
		is_cpu_exclusive(p) <= is_cpu_exclusive(q) &&
		is_mem_exclusive(p) <= is_mem_exclusive(q);
}

*/
 alloc_trial_cpuset - allocate a trial cpuset
 @cs: the cpuset that the trial cpuset duplicates
 /*
static struct cpusetalloc_trial_cpuset(struct cpusetcs)
{
	struct cpusettrial;

	trial = kmemdup(cs, sizeof(*cs), GFP_KERNEL);
	if (!trial)
		return NULL;

	if (!alloc_cpumask_var(&trial->cpus_allowed, GFP_KERNEL))
		goto free_cs;
	if (!alloc_cpumask_var(&trial->effective_cpus, GFP_KERNEL))
		goto free_cpus;

	cpumask_copy(trial->cpus_allowed, cs->cpus_allowed);
	cpumask_copy(trial->effective_cpus, cs->effective_cpus);
	return trial;

free_cpus:
	free_cpumask_var(trial->cpus_allowed);
free_cs:
	kfree(trial);
	return NULL;
}

*/
 free_trial_cpuset - free the trial cpuset
 @trial: the trial cpuset to be freed
 /*
static void free_trial_cpuset(struct cpusettrial)
{
	free_cpumask_var(trial->effective_cpus);
	free_cpumask_var(trial->cpus_allowed);
	kfree(trial);
}

*/
 validate_change() - Used to validate that any proposed cpuset change
		       follows the structural rules for cpusets.

 If we replaced the flag and mask values of the current cpuset
 (cur) with those values in the trial cpuset (trial), would
 our various subset and exclusive rules still be valid?  Presumes
 cpuset_mutex held.

 'cur' is the address of an actual, in-use cpuset.  Operations
 such as list traversal that depend on the actual address of the
 cpuset in the list must use cur below, not trial.

 'trial' is the address of bulk structure copy of cur, with
 perhaps one or more of the fields cpus_allowed, mems_allowed,
 or flags changed to new, trial values.

 Return 0 if valid, -errno if not.
 /*

static int validate_change(struct cpusetcur, struct cpusettrial)
{
	struct cgroup_subsys_statecss;
	struct cpusetc,par;
	int ret;

	rcu_read_lock();

	*/ Each of our child cpusets must be a subset of us /*
	ret = -EBUSY;
	cpuset_for_each_child(c, css, cur)
		if (!is_cpuset_subset(c, trial))
			goto out;

	*/ Remaining checks don't apply to root cpuset /*
	ret = 0;
	if (cur == &top_cpuset)
		goto out;

	par = parent_cs(cur);

	*/ On legacy hiearchy, we must be a subset of our parent cpuset. /*
	ret = -EACCES;
	if (!cgroup_subsys_on_dfl(cpuset_cgrp_subsys) &&
	    !is_cpuset_subset(trial, par))
		goto out;

	*/
	 If either I or some sibling (!= me) is exclusive, we can't
	 overlap
	 /*
	ret = -EINVAL;
	cpuset_for_each_child(c, css, par) {
		if ((is_cpu_exclusive(trial) || is_cpu_exclusive(c)) &&
		    c != cur &&
		    cpumask_intersects(trial->cpus_allowed, c->cpus_allowed))
			goto out;
		if ((is_mem_exclusive(trial) || is_mem_exclusive(c)) &&
		    c != cur &&
		    nodes_intersects(trial->mems_allowed, c->mems_allowed))
			goto out;
	}

	*/
	 Cpusets with tasks - existing or newly being attached - can't
	 be changed to have empty cpus_allowed or mems_allowed.
	 /*
	ret = -ENOSPC;
	if ((cgroup_is_populated(cur->css.cgroup) || cur->attach_in_progress)) {
		if (!cpumask_empty(cur->cpus_allowed) &&
		    cpumask_empty(trial->cpus_allowed))
			goto out;
		if (!nodes_empty(cur->mems_allowed) &&
		    nodes_empty(trial->mems_allowed))
			goto out;
	}

	*/
	 We can't shrink if we won't have enough room for SCHED_DEADLINE
	 tasks.
	 /*
	ret = -EBUSY;
	if (is_cpu_exclusive(cur) &&
	    !cpuset_cpumask_can_shrink(cur->cpus_allowed,
				       trial->cpus_allowed))
		goto out;

	ret = 0;
out:
	rcu_read_unlock();
	return ret;
}

#ifdef CONFIG_SMP
*/
 Helper routine for generate_sched_domains().
 Do cpusets a, b have overlapping effective cpus_allowed masks?
 /*
static int cpusets_overlap(struct cpuseta, struct cpusetb)
{
	return cpumask_intersects(a->effective_cpus, b->effective_cpus);
}

static void
update_domain_attr(struct sched_domain_attrdattr, struct cpusetc)
{
	if (dattr->relax_domain_level < c->relax_domain_level)
		dattr->relax_domain_level = c->relax_domain_level;
	return;
}

static void update_domain_attr_tree(struct sched_domain_attrdattr,
				    struct cpusetroot_cs)
{
	struct cpusetcp;
	struct cgroup_subsys_statepos_css;

	rcu_read_lock();
	cpuset_for_each_descendant_pre(cp, pos_css, root_cs) {
		*/ skip the whole subtree if @cp doesn't have any CPU /*
		if (cpumask_empty(cp->cpus_allowed)) {
			pos_css = css_rightmost_descendant(pos_css);
			continue;
		}

		if (is_sched_load_balance(cp))
			update_domain_attr(dattr, cp);
	}
	rcu_read_unlock();
}

*/
 generate_sched_domains()

 This function builds a partial partition of the systems CPUs
 A 'partial partition' is a set of non-overlapping subsets whose
 union is a subset of that set.
 The output of this function needs to be passed to kernel/sched/core.c
 partition_sched_domains() routine, which will rebuild the scheduler's
 load balancing domains (sched domains) as specified by that partial
 partition.

 See "What is sched_load_balance" in Documentation/cgroups/cpusets.txt
 for a background explanation of this.

 Does not return errors, on the theory that the callers of this
 routine would rather not worry about failures to rebuild sched
 domains when operating in the severe memory shortage situations
 that could cause allocation failures below.

 Must be called with cpuset_mutex held.

 The three key local variables below are:
    q  - a linked-list queue of cpuset pointers, used to implement a
	   top-down scan of all cpusets.  This scan loads a pointer
	   to each cpuset marked is_sched_load_balance into the
	   array 'csa'.  For our purposes, rebuilding the schedulers
	   sched domains, we can ignore !is_sched_load_balance cpusets.
  csa  - (for CpuSet Array) Array of pointers to all the cpusets
	   that need to be load balanced, for convenient iterative
	   access by the subsequent code that finds the best partition,
	   i.e the set of domains (subsets) of CPUs such that the
	   cpus_allowed of every cpuset marked is_sched_load_balance
	   is a subset of one of these domains, while there are as
	   many such domains as possible, each as small as possible.
 doms  - Conversion of 'csa' to an array of cpumasks, for passing to
	   the kernel/sched/core.c routine partition_sched_domains() in a
	   convenient format, that can be easily compared to the prior
	   value to determine what partition elements (sched domains)
	   were changed (added or removed.)

 Finding the best partition (set of domains):
	The triple nested loops below over i, j, k scan over the
	load balanced cpusets (using the array of cpuset pointers in
	csa[]) looking for pairs of cpusets that have overlapping
	cpus_allowed, but which don't have the same 'pn' partition
	number and gives them in the same partition number.  It keeps
	looping on the 'restart' label until it can no longer find
	any such pairs.

	The union of the cpus_allowed masks from the set of
	all cpusets having the same 'pn' value then form the one
	element of the partition (one sched domain) to be passed to
	partition_sched_domains().
 /*
static int generate_sched_domains(cpumask_var_t*domains,
			struct sched_domain_attr*attributes)
{
	struct cpusetcp;	*/ scans q /*
	struct cpuset*csa;	*/ array of all cpuset ptrs /*
	int csn;		*/ how many cpuset ptrs in csa so far /*
	int i, j, k;		*/ indices for partition finding loops /*
	cpumask_var_tdoms;	*/ resulting partition; i.e. sched domains /*
	cpumask_var_t non_isolated_cpus; / load balanced CPUs /*
	struct sched_domain_attrdattr; / attributes for custom domains /*
	int ndoms = 0;		*/ number of sched domains in result /*
	int nslot;		*/ next empty doms[] struct cpumask slot /*
	struct cgroup_subsys_statepos_css;

	doms = NULL;
	dattr = NULL;
	csa = NULL;

	if (!alloc_cpumask_var(&non_isolated_cpus, GFP_KERNEL))
		goto done;
	cpumask_andnot(non_isolated_cpus, cpu_possible_mask, cpu_isolated_map);

	*/ Special case for the 99% of systems with one, full, sched domain /*
	if (is_sched_load_balance(&top_cpuset)) {
		ndoms = 1;
		doms = alloc_sched_domains(ndoms);
		if (!doms)
			goto done;

		dattr = kmalloc(sizeof(struct sched_domain_attr), GFP_KERNEL);
		if (dattr) {
			*dattr = SD_ATTR_INIT;
			update_domain_attr_tree(dattr, &top_cpuset);
		}
		cpumask_and(doms[0], top_cpuset.effective_cpus,
				     non_isolated_cpus);

		goto done;
	}

	csa = kmalloc(nr_cpusets() sizeof(cp), GFP_KERNEL);
	if (!csa)
		goto done;
	csn = 0;

	rcu_read_lock();
	cpuset_for_each_descendant_pre(cp, pos_css, &top_cpuset) {
		if (cp == &top_cpuset)
			continue;
		*/
		 Continue traversing beyond @cp iff @cp has some CPUs and
		 isn't load balancing.  The former is obvious.  The
		 latter: All child cpusets contain a subset of the
		 parent's cpus, so just skip them, and then we call
		 update_domain_attr_tree() to calc relax_domain_level of
		 the corresponding sched domain.
		 /*
		if (!cpumask_empty(cp->cpus_allowed) &&
		    !(is_sched_load_balance(cp) &&
		      cpumask_intersects(cp->cpus_allowed, non_isolated_cpus)))
			continue;

		if (is_sched_load_balance(cp))
			csa[csn++] = cp;

		*/ skip @cp's subtree /*
		pos_css = css_rightmost_descendant(pos_css);
	}
	rcu_read_unlock();

	for (i = 0; i < csn; i++)
		csa[i]->pn = i;
	ndoms = csn;

restart:
	*/ Find the best partition (set of sched domains) /*
	for (i = 0; i < csn; i++) {
		struct cpuseta = csa[i];
		int apn = a->pn;

		for (j = 0; j < csn; j++) {
			struct cpusetb = csa[j];
			int bpn = b->pn;

			if (apn != bpn && cpusets_overlap(a, b)) {
				for (k = 0; k < csn; k++) {
					struct cpusetc = csa[k];

					if (c->pn == bpn)
						c->pn = apn;
				}
				ndoms--;	*/ one less element /*
				goto restart;
			}
		}
	}

	*/
	 Now we know how many domains to create.
	 Convert <csn, csa> to <ndoms, doms> and populate cpu masks.
	 /*
	doms = alloc_sched_domains(ndoms);
	if (!doms)
		goto done;

	*/
	 The rest of the code, including the scheduler, can deal with
	 dattr==NULL case. No need to abort if alloc fails.
	 /*
	dattr = kmalloc(ndoms sizeof(struct sched_domain_attr), GFP_KERNEL);

	for (nslot = 0, i = 0; i < csn; i++) {
		struct cpuseta = csa[i];
		struct cpumaskdp;
		int apn = a->pn;

		if (apn < 0) {
			*/ Skip completed partitions /*
			continue;
		}

		dp = doms[nslot];

		if (nslot == ndoms) {
			static int warnings = 10;
			if (warnings) {
				pr_warn("rebuild_sched_domains confused: nslot %d, ndoms %d, csn %d, i %d, apn %d\n",
					nslot, ndoms, csn, i, apn);
				warnings--;
			}
			continue;
		}

		cpumask_clear(dp);
		if (dattr)
			*(dattr + nslot) = SD_ATTR_INIT;
		for (j = i; j < csn; j++) {
			struct cpusetb = csa[j];

			if (apn == b->pn) {
				cpumask_or(dp, dp, b->effective_cpus);
				cpumask_and(dp, dp, non_isolated_cpus);
				if (dattr)
					update_domain_attr_tree(dattr + nslot, b);

				*/ Done with this partition /*
				b->pn = -1;
			}
		}
		nslot++;
	}
	BUG_ON(nslot != ndoms);

done:
	free_cpumask_var(non_isolated_cpus);
	kfree(csa);

	*/
	 Fallback to the default domain if kmalloc() failed.
	 See comments in partition_sched_domains().
	 /*
	if (doms == NULL)
		ndoms = 1;

	*domains    = doms;
	*attributes = dattr;
	return ndoms;
}

*/
 Rebuild scheduler domains.

 If the flag 'sched_load_balance' of any cpuset with non-empty
 'cpus' changes, or if the 'cpus' allowed changes in any cpuset
 which has that flag enabled, or if any cpuset with a non-empty
 'cpus' is removed, then call this routine to rebuild the
 scheduler's dynamic sched domains.

 Call with cpuset_mutex held.  Takes get_online_cpus().
 /*
static void rebuild_sched_domains_locked(void)
{
	struct sched_domain_attrattr;
	cpumask_var_tdoms;
	int ndoms;

	lockdep_assert_held(&cpuset_mutex);
	get_online_cpus();

	*/
	 We have raced with CPU hotplug. Don't do anything to avoid
	 passing doms with offlined cpu to partition_sched_domains().
	 Anyways, hotplug work item will rebuild sched domains.
	 /*
	if (!cpumask_equal(top_cpuset.effective_cpus, cpu_active_mask))
		goto out;

	*/ Generate domain masks and attrs /*
	ndoms = generate_sched_domains(&doms, &attr);

	*/ Have scheduler rebuild the domains /*
	partition_sched_domains(ndoms, doms, attr);
out:
	put_online_cpus();
}
#else */ !CONFIG_SMP /*
static void rebuild_sched_domains_locked(void)
{
}
#endif */ CONFIG_SMP /*

void rebuild_sched_domains(void)
{
	mutex_lock(&cpuset_mutex);
	rebuild_sched_domains_locked();
	mutex_unlock(&cpuset_mutex);
}

*/
 update_tasks_cpumask - Update the cpumasks of tasks in the cpuset.
 @cs: the cpuset in which each task's cpus_allowed mask needs to be changed

 Iterate through each task of @cs updating its cpus_allowed to the
 effective cpuset's.  As this function is called with cpuset_mutex held,
 cpuset membership stays stable.
 /*
static void update_tasks_cpumask(struct cpusetcs)
{
	struct css_task_iter it;
	struct task_structtask;

	css_task_iter_start(&cs->css, &it);
	while ((task = css_task_iter_next(&it)))
		set_cpus_allowed_ptr(task, cs->effective_cpus);
	css_task_iter_end(&it);
}

*/
 update_cpumasks_hier - Update effective cpumasks and tasks in the subtree
 @cs: the cpuset to consider
 @new_cpus: temp variable for calculating new effective_cpus

 When congifured cpumask is changed, the effective cpumasks of this cpuset
 and all its descendants need to be updated.

 On legacy hierachy, effective_cpus will be the same with cpu_allowed.

 Called with cpuset_mutex held
 /*
static void update_cpumasks_hier(struct cpusetcs, struct cpumasknew_cpus)
{
	struct cpusetcp;
	struct cgroup_subsys_statepos_css;
	bool need_rebuild_sched_domains = false;

	rcu_read_lock();
	cpuset_for_each_descendant_pre(cp, pos_css, cs) {
		struct cpusetparent = parent_cs(cp);

		cpumask_and(new_cpus, cp->cpus_allowed, parent->effective_cpus);

		*/
		 If it becomes empty, inherit the effective mask of the
		 parent, which is guaranteed to have some CPUs.
		 /*
		if (cgroup_subsys_on_dfl(cpuset_cgrp_subsys) &&
		    cpumask_empty(new_cpus))
			cpumask_copy(new_cpus, parent->effective_cpus);

		*/ Skip the whole subtree if the cpumask remains the same. /*
		if (cpumask_equal(new_cpus, cp->effective_cpus)) {
			pos_css = css_rightmost_descendant(pos_css);
			continue;
		}

		if (!css_tryget_online(&cp->css))
			continue;
		rcu_read_unlock();

		spin_lock_irq(&callback_lock);
		cpumask_copy(cp->effective_cpus, new_cpus);
		spin_unlock_irq(&callback_lock);

		WARN_ON(!cgroup_subsys_on_dfl(cpuset_cgrp_subsys) &&
			!cpumask_equal(cp->cpus_allowed, cp->effective_cpus));

		update_tasks_cpumask(cp);

		*/
		 If the effective cpumask of any non-empty cpuset is changed,
		 we need to rebuild sched domains.
		 /*
		if (!cpumask_empty(cp->cpus_allowed) &&
		    is_sched_load_balance(cp))
			need_rebuild_sched_domains = true;

		rcu_read_lock();
		css_put(&cp->css);
	}
	rcu_read_unlock();

	if (need_rebuild_sched_domains)
		rebuild_sched_domains_locked();
}

*/
 update_cpumask - update the cpus_allowed mask of a cpuset and all tasks in it
 @cs: the cpuset to consider
 @trialcs: trial cpuset
 @buf: buffer of cpu numbers written to this cpuset
 /*
static int update_cpumask(struct cpusetcs, struct cpusettrialcs,
			  const charbuf)
{
	int retval;

	*/ top_cpuset.cpus_allowed tracks cpu_online_mask; it's read-only /*
	if (cs == &top_cpuset)
		return -EACCES;

	*/
	 An empty cpus_allowed is ok only if the cpuset has no tasks.
	 Since cpulist_parse() fails on an empty mask, we special case
	 that parsing.  The validate_change() call ensures that cpusets
	 with tasks have cpus.
	 /*
	if (!*buf) {
		cpumask_clear(trialcs->cpus_allowed);
	} else {
		retval = cpulist_parse(buf, trialcs->cpus_allowed);
		if (retval < 0)
			return retval;

		if (!cpumask_subset(trialcs->cpus_allowed,
				    top_cpuset.cpus_allowed))
			return -EINVAL;
	}

	*/ Nothing to do if the cpus didn't change /*
	if (cpumask_equal(cs->cpus_allowed, trialcs->cpus_allowed))
		return 0;

	retval = validate_change(cs, trialcs);
	if (retval < 0)
		return retval;

	spin_lock_irq(&callback_lock);
	cpumask_copy(cs->cpus_allowed, trialcs->cpus_allowed);
	spin_unlock_irq(&callback_lock);

	*/ use trialcs->cpus_allowed as a temp variable /*
	update_cpumasks_hier(cs, trialcs->cpus_allowed);
	return 0;
}

*/
 Migrate memory region from one set of nodes to another.  This is
 performed asynchronously as it can be called from process migration path
 holding locks involved in process management.  All mm migrations are
 performed in the queued order and can be waited for by flushing
 cpuset_migrate_mm_wq.
 /*

struct cpuset_migrate_mm_work {
	struct work_struct	work;
	struct mm_struct	*mm;
	nodemask_t		from;
	nodemask_t		to;
};

static void cpuset_migrate_mm_workfn(struct work_structwork)
{
	struct cpuset_migrate_mm_workmwork =
		container_of(work, struct cpuset_migrate_mm_work, work);

	*/ on a wq worker, no need to worry about %current's mems_allowed /*
	do_migrate_pages(mwork->mm, &mwork->from, &mwork->to, MPOL_MF_MOVE_ALL);
	mmput(mwork->mm);
	kfree(mwork);
}

static void cpuset_migrate_mm(struct mm_structmm, const nodemask_tfrom,
							const nodemask_tto)
{
	struct cpuset_migrate_mm_workmwork;

	mwork = kzalloc(sizeof(*mwork), GFP_KERNEL);
	if (mwork) {
		mwork->mm = mm;
		mwork->from =from;
		mwork->to =to;
		INIT_WORK(&mwork->work, cpuset_migrate_mm_workfn);
		queue_work(cpuset_migrate_mm_wq, &mwork->work);
	} else {
		mmput(mm);
	}
}

static void cpuset_post_attach(void)
{
	flush_workqueue(cpuset_migrate_mm_wq);
}

*/
 cpuset_change_task_nodemask - change task's mems_allowed and mempolicy
 @tsk: the task to change
 @newmems: new nodes that the task will be set

 In order to avoid seeing no nodes if the old and new nodes are disjoint,
 we structure updates as setting all new allowed nodes, then clearing newly
 disallowed ones.
 /*
static void cpuset_change_task_nodemask(struct task_structtsk,
					nodemask_tnewmems)
{
	bool need_loop;

	*/
	 Allow tasks that have access to memory reserves because they have
	 been OOM killed to get memory anywhere.
	 /*
	if (unlikely(test_thread_flag(TIF_MEMDIE)))
		return;
	if (current->flags & PF_EXITING)/ Let dying task have memory /*
		return;

	task_lock(tsk);
	*/
	 Determine if a loop is necessary if another thread is doing
	 read_mems_allowed_begin().  If at least one node remains unchanged and
	 tsk does not have a mempolicy, then an empty nodemask will not be
	 possible when mems_allowed is larger than a word.
	 /*
	need_loop = task_has_mempolicy(tsk) ||
			!nodes_intersects(*newmems, tsk->mems_allowed);

	if (need_loop) {
		local_irq_disable();
		write_seqcount_begin(&tsk->mems_allowed_seq);
	}

	nodes_or(tsk->mems_allowed, tsk->mems_allowed,newmems);
	mpol_rebind_task(tsk, newmems, MPOL_REBIND_STEP1);

	mpol_rebind_task(tsk, newmems, MPOL_REBIND_STEP2);
	tsk->mems_allowed =newmems;

	if (need_loop) {
		write_seqcount_end(&tsk->mems_allowed_seq);
		local_irq_enable();
	}

	task_unlock(tsk);
}

static voidcpuset_being_rebound;

*/
 update_tasks_nodemask - Update the nodemasks of tasks in the cpuset.
 @cs: the cpuset in which each task's mems_allowed mask needs to be changed

 Iterate through each task of @cs updating its mems_allowed to the
 effective cpuset's.  As this function is called with cpuset_mutex held,
 cpuset membership stays stable.
 /*
static void update_tasks_nodemask(struct cpusetcs)
{
	static nodemask_t newmems;	*/ protected by cpuset_mutex /*
	struct css_task_iter it;
	struct task_structtask;

	cpuset_being_rebound = cs;		*/ causes mpol_dup() rebind /*

	guarantee_online_mems(cs, &newmems);

	*/
	 The mpol_rebind_mm() call takes mmap_sem, which we couldn't
	 take while holding tasklist_lock.  Forks can happen - the
	 mpol_dup() cpuset_being_rebound check will catch such forks,
	 and rebind their vma mempolicies too.  Because we still hold
	 the global cpuset_mutex, we know that no other rebind effort
	 will be contending for the global variable cpuset_being_rebound.
	 It's ok if we rebind the same mm twice; mpol_rebind_mm()
	 is idempotent.  Also migrate pages in each mm to new nodes.
	 /*
	css_task_iter_start(&cs->css, &it);
	while ((task = css_task_iter_next(&it))) {
		struct mm_structmm;
		bool migrate;

		cpuset_change_task_nodemask(task, &newmems);

		mm = get_task_mm(task);
		if (!mm)
			continue;

		migrate = is_memory_migrate(cs);

		mpol_rebind_mm(mm, &cs->mems_allowed);
		if (migrate)
			cpuset_migrate_mm(mm, &cs->old_mems_allowed, &newmems);
		else
			mmput(mm);
	}
	css_task_iter_end(&it);

	*/
	 All the tasks' nodemasks have been updated, update
	 cs->old_mems_allowed.
	 /*
	cs->old_mems_allowed = newmems;

	*/ We're done rebinding vmas to this cpuset's new mems_allowed. /*
	cpuset_being_rebound = NULL;
}

*/
 update_nodemasks_hier - Update effective nodemasks and tasks in the subtree
 @cs: the cpuset to consider
 @new_mems: a temp variable for calculating new effective_mems

 When configured nodemask is changed, the effective nodemasks of this cpuset
 and all its descendants need to be updated.

 On legacy hiearchy, effective_mems will be the same with mems_allowed.

 Called with cpuset_mutex held
 /*
static void update_nodemasks_hier(struct cpusetcs, nodemask_tnew_mems)
{
	struct cpusetcp;
	struct cgroup_subsys_statepos_css;

	rcu_read_lock();
	cpuset_for_each_descendant_pre(cp, pos_css, cs) {
		struct cpusetparent = parent_cs(cp);

		nodes_and(*new_mems, cp->mems_allowed, parent->effective_mems);

		*/
		 If it becomes empty, inherit the effective mask of the
		 parent, which is guaranteed to have some MEMs.
		 /*
		if (cgroup_subsys_on_dfl(cpuset_cgrp_subsys) &&
		    nodes_empty(*new_mems))
			*new_mems = parent->effective_mems;

		*/ Skip the whole subtree if the nodemask remains the same. /*
		if (nodes_equal(*new_mems, cp->effective_mems)) {
			pos_css = css_rightmost_descendant(pos_css);
			continue;
		}

		if (!css_tryget_online(&cp->css))
			continue;
		rcu_read_unlock();

		spin_lock_irq(&callback_lock);
		cp->effective_mems =new_mems;
		spin_unlock_irq(&callback_lock);

		WARN_ON(!cgroup_subsys_on_dfl(cpuset_cgrp_subsys) &&
			!nodes_equal(cp->mems_allowed, cp->effective_mems));

		update_tasks_nodemask(cp);

		rcu_read_lock();
		css_put(&cp->css);
	}
	rcu_read_unlock();
}

*/
 Handle user request to change the 'mems' memory placement
 of a cpuset.  Needs to validate the request, update the
 cpusets mems_allowed, and for each task in the cpuset,
 update mems_allowed and rebind task's mempolicy and any vma
 mempolicies and if the cpuset is marked 'memory_migrate',
 migrate the tasks pages to the new memory.

 Call with cpuset_mutex held. May take callback_lock during call.
 Will take tasklist_lock, scan tasklist for tasks in cpuset cs,
 lock each such tasks mm->mmap_sem, scan its vma's and rebind
 their mempolicies to the cpusets new mems_allowed.
 /*
static int update_nodemask(struct cpusetcs, struct cpusettrialcs,
			   const charbuf)
{
	int retval;

	*/
	 top_cpuset.mems_allowed tracks node_stats[N_MEMORY];
	 it's read-only
	 /*
	if (cs == &top_cpuset) {
		retval = -EACCES;
		goto done;
	}

	*/
	 An empty mems_allowed is ok iff there are no tasks in the cpuset.
	 Since nodelist_parse() fails on an empty mask, we special case
	 that parsing.  The validate_change() call ensures that cpusets
	 with tasks have memory.
	 /*
	if (!*buf) {
		nodes_clear(trialcs->mems_allowed);
	} else {
		retval = nodelist_parse(buf, trialcs->mems_allowed);
		if (retval < 0)
			goto done;

		if (!nodes_subset(trialcs->mems_allowed,
				  top_cpuset.mems_allowed)) {
			retval = -EINVAL;
			goto done;
		}
	}

	if (nodes_equal(cs->mems_allowed, trialcs->mems_allowed)) {
		retval = 0;		*/ Too easy - nothing to do /*
		goto done;
	}
	retval = validate_change(cs, trialcs);
	if (retval < 0)
		goto done;

	spin_lock_irq(&callback_lock);
	cs->mems_allowed = trialcs->mems_allowed;
	spin_unlock_irq(&callback_lock);

	*/ use trialcs->mems_allowed as a temp variable /*
	update_nodemasks_hier(cs, &trialcs->mems_allowed);
done:
	return retval;
}

int current_cpuset_is_being_rebound(void)
{
	int ret;

	rcu_read_lock();
	ret = task_cs(current) == cpuset_being_rebound;
	rcu_read_unlock();

	return ret;
}

static int update_relax_domain_level(struct cpusetcs, s64 val)
{
#ifdef CONFIG_SMP
	if (val < -1 || val >= sched_domain_level_max)
		return -EINVAL;
#endif

	if (val != cs->relax_domain_level) {
		cs->relax_domain_level = val;
		if (!cpumask_empty(cs->cpus_allowed) &&
		    is_sched_load_balance(cs))
			rebuild_sched_domains_locked();
	}

	return 0;
}

*/
 update_tasks_flags - update the spread flags of tasks in the cpuset.
 @cs: the cpuset in which each task's spread flags needs to be changed

 Iterate through each task of @cs updating its spread flags.  As this
 function is called with cpuset_mutex held, cpuset membership stays
 stable.
 /*
static void update_tasks_flags(struct cpusetcs)
{
	struct css_task_iter it;
	struct task_structtask;

	css_task_iter_start(&cs->css, &it);
	while ((task = css_task_iter_next(&it)))
		cpuset_update_task_spread_flag(cs, task);
	css_task_iter_end(&it);
}

*/
 update_flag - read a 0 or a 1 in a file and update associated flag
 bit:		the bit to update (see cpuset_flagbits_t)
 cs:		the cpuset to update
 turning_on: 	whether the flag is being set or cleared

 Call with cpuset_mutex held.
 /*

static int update_flag(cpuset_flagbits_t bit, struct cpusetcs,
		       int turning_on)
{
	struct cpusettrialcs;
	int balance_flag_changed;
	int spread_flag_changed;
	int err;

	trialcs = alloc_trial_cpuset(cs);
	if (!trialcs)
		return -ENOMEM;

	if (turning_on)
		set_bit(bit, &trialcs->flags);
	else
		clear_bit(bit, &trialcs->flags);

	err = validate_change(cs, trialcs);
	if (err < 0)
		goto out;

	balance_flag_changed = (is_sched_load_balance(cs) !=
				is_sched_load_balance(trialcs));

	spread_flag_changed = ((is_spread_slab(cs) != is_spread_slab(trialcs))
			|| (is_spread_page(cs) != is_spread_page(trialcs)));

	spin_lock_irq(&callback_lock);
	cs->flags = trialcs->flags;
	spin_unlock_irq(&callback_lock);

	if (!cpumask_empty(trialcs->cpus_allowed) && balance_flag_changed)
		rebuild_sched_domains_locked();

	if (spread_flag_changed)
		update_tasks_flags(cs);
out:
	free_trial_cpuset(trialcs);
	return err;
}

*/
 Frequency meter - How fast is some event occurring?

 These routines manage a digitally filtered, constant time based,
 event frequency meter.  There are four routines:
   fmeter_init() - initialize a frequency meter.
   fmeter_markevent() - called each time the event happens.
   fmeter_getrate() - returns the recent rate of such events.
   fmeter_update() - internal routine used to update fmeter.

 A common data structure is passed to each of these routines,
 which is used to keep track of the state required to manage the
 frequency meter and its digital filter.

 The filter works on the number of events marked per unit time.
 The filter is single-pole low-pass recursive (IIR).  The time unit
 is 1 second.  Arithmetic is done using 32-bit integers scaled to
 simulate 3 decimal digits of precision (multiplied by 1000).

 With an FM_COEF of 933, and a time base of 1 second, the filter
 has a half-life of 10 seconds, meaning that if the events quit
 happening, then the rate returned from the fmeter_getrate()
 will be cut in half each 10 seconds, until it converges to zero.

 It is not worth doing a real infinitely recursive filter.  If more
 than FM_MAXTICKS ticks have elapsed since the last filter event,
 just compute FM_MAXTICKS ticks worth, by which point the level
 will be stable.

 Limit the count of unprocessed events to FM_MAXCNT, so as to avoid
 arithmetic overflow in the fmeter_update() routine.

 Given the simple 32 bit integer arithmetic used, this meter works
 best for reporting rates between one per millisecond (msec) and
 one per 32 (approx) seconds.  At constant rates faster than one
 per msec it maxes out at values just under 1,000,000.  At constant
 rates between one per msec, and one per second it will stabilize
 to a value N*1000, where N is the rate of events per second.
 At constant rates between one per second and one per 32 seconds,
 it will be choppy, moving up on the seconds that have an event,
 and then decaying until the next event.  At rates slower than
 about one in 32 seconds, it decays all the way back to zero between
 each event.
 /*

#define FM_COEF 933		*/ coefficient for half-life of 10 secs /*
#define FM_MAXTICKS ((u32)99)  / useless computing more ticks than this /*
#define FM_MAXCNT 1000000	*/ limit cnt to avoid overflow /*
#define FM_SCALE 1000		*/ faux fixed point scale /*

*/ Initialize a frequency meter /*
static void fmeter_init(struct fmeterfmp)
{
	fmp->cnt = 0;
	fmp->val = 0;
	fmp->time = 0;
	spin_lock_init(&fmp->lock);
}

*/ Internal meter update - process cnt events and update value /*
static void fmeter_update(struct fmeterfmp)
{
	time64_t now;
	u32 ticks;

	now = ktime_get_seconds();
	ticks = now - fmp->time;

	if (ticks == 0)
		return;

	ticks = min(FM_MAXTICKS, ticks);
	while (ticks-- > 0)
		fmp->val = (FM_COEF fmp->val) / FM_SCALE;
	fmp->time = now;

	fmp->val += ((FM_SCALE - FM_COEF) fmp->cnt) / FM_SCALE;
	fmp->cnt = 0;
}

*/ Process any previous ticks, then bump cnt by one (times scale). /*
static void fmeter_markevent(struct fmeterfmp)
{
	spin_lock(&fmp->lock);
	fmeter_update(fmp);
	fmp->cnt = min(FM_MAXCNT, fmp->cnt + FM_SCALE);
	spin_unlock(&fmp->lock);
}

*/ Process any previous ticks, then return current value. /*
static int fmeter_getrate(struct fmeterfmp)
{
	int val;

	spin_lock(&fmp->lock);
	fmeter_update(fmp);
	val = fmp->val;
	spin_unlock(&fmp->lock);
	return val;
}

static struct cpusetcpuset_attach_old_cs;

*/ Called by cgroups to determine if a cpuset is usable; cpuset_mutex held /*
static int cpuset_can_attach(struct cgroup_tasksettset)
{
	struct cgroup_subsys_statecss;
	struct cpusetcs;
	struct task_structtask;
	int ret;

	*/ used later by cpuset_attach() /*
	cpuset_attach_old_cs = task_cs(cgroup_taskset_first(tset, &css));
	cs = css_cs(css);

	mutex_lock(&cpuset_mutex);

	*/ allow moving tasks into an empty cpuset if on default hierarchy /*
	ret = -ENOSPC;
	if (!cgroup_subsys_on_dfl(cpuset_cgrp_subsys) &&
	    (cpumask_empty(cs->cpus_allowed) || nodes_empty(cs->mems_allowed)))
		goto out_unlock;

	cgroup_taskset_for_each(task, css, tset) {
		ret = task_can_attach(task, cs->cpus_allowed);
		if (ret)
			goto out_unlock;
		ret = security_task_setscheduler(task);
		if (ret)
			goto out_unlock;
	}

	*/
	 Mark attach is in progress.  This makes validate_change() fail
	 changes which zero cpus/mems_allowed.
	 /*
	cs->attach_in_progress++;
	ret = 0;
out_unlock:
	mutex_unlock(&cpuset_mutex);
	return ret;
}

static void cpuset_cancel_attach(struct cgroup_tasksettset)
{
	struct cgroup_subsys_statecss;
	struct cpusetcs;

	cgroup_taskset_first(tset, &css);
	cs = css_cs(css);

	mutex_lock(&cpuset_mutex);
	css_cs(css)->attach_in_progress--;
	mutex_unlock(&cpuset_mutex);
}

*/
 Protected by cpuset_mutex.  cpus_attach is used only by cpuset_attach()
 but we can't allocate it dynamically there.  Define it global and
 allocate from cpuset_init().
 /*
static cpumask_var_t cpus_attach;

static void cpuset_attach(struct cgroup_tasksettset)
{
	*/ static buf protected by cpuset_mutex /*
	static nodemask_t cpuset_attach_nodemask_to;
	struct task_structtask;
	struct task_structleader;
	struct cgroup_subsys_statecss;
	struct cpusetcs;
	struct cpusetoldcs = cpuset_attach_old_cs;

	cgroup_taskset_first(tset, &css);
	cs = css_cs(css);

	mutex_lock(&cpuset_mutex);

	*/ prepare for attach /*
	if (cs == &top_cpuset)
		cpumask_copy(cpus_attach, cpu_possible_mask);
	else
		guarantee_online_cpus(cs, cpus_attach);

	guarantee_online_mems(cs, &cpuset_attach_nodemask_to);

	cgroup_taskset_for_each(task, css, tset) {
		*/
		 can_attach beforehand should guarantee that this doesn't
		 fail.  TODO: have a better way to handle failure here
		 /*
		WARN_ON_ONCE(set_cpus_allowed_ptr(task, cpus_attach));

		cpuset_change_task_nodemask(task, &cpuset_attach_nodemask_to);
		cpuset_update_task_spread_flag(cs, task);
	}

	*/
	 Change mm for all threadgroup leaders. This is expensive and may
	 sleep and should be moved outside migration path proper.
	 /*
	cpuset_attach_nodemask_to = cs->effective_mems;
	cgroup_taskset_for_each_leader(leader, css, tset) {
		struct mm_structmm = get_task_mm(leader);

		if (mm) {
			mpol_rebind_mm(mm, &cpuset_attach_nodemask_to);

			*/
			 old_mems_allowed is the same with mems_allowed
			 here, except if this task is being moved
			 automatically due to hotplug.  In that case
			 @mems_allowed has been updated and is empty, so
			 @old_mems_allowed is the right nodesets that we
			 migrate mm from.
			 /*
			if (is_memory_migrate(cs))
				cpuset_migrate_mm(mm, &oldcs->old_mems_allowed,
						  &cpuset_attach_nodemask_to);
			else
				mmput(mm);
		}
	}

	cs->old_mems_allowed = cpuset_attach_nodemask_to;

	cs->attach_in_progress--;
	if (!cs->attach_in_progress)
		wake_up(&cpuset_attach_wq);

	mutex_unlock(&cpuset_mutex);
}

*/ The various types of files and directories in a cpuset file system /*

typedef enum {
	FILE_MEMORY_MIGRATE,
	FILE_CPULIST,
	FILE_MEMLIST,
	FILE_EFFECTIVE_CPULIST,
	FILE_EFFECTIVE_MEMLIST,
	FILE_CPU_EXCLUSIVE,
	FILE_MEM_EXCLUSIVE,
	FILE_MEM_HARDWALL,
	FILE_SCHED_LOAD_BALANCE,
	FILE_SCHED_RELAX_DOMAIN_LEVEL,
	FILE_MEMORY_PRESSURE_ENABLED,
	FILE_MEMORY_PRESSURE,
	FILE_SPREAD_PAGE,
	FILE_SPREAD_SLAB,
} cpuset_filetype_t;

static int cpuset_write_u64(struct cgroup_subsys_statecss, struct cftypecft,
			    u64 val)
{
	struct cpusetcs = css_cs(css);
	cpuset_filetype_t type = cft->private;
	int retval = 0;

	mutex_lock(&cpuset_mutex);
	if (!is_cpuset_online(cs)) {
		retval = -ENODEV;
		goto out_unlock;
	}

	switch (type) {
	case FILE_CPU_EXCLUSIVE:
		retval = update_flag(CS_CPU_EXCLUSIVE, cs, val);
		break;
	case FILE_MEM_EXCLUSIVE:
		retval = update_flag(CS_MEM_EXCLUSIVE, cs, val);
		break;
	case FILE_MEM_HARDWALL:
		retval = update_flag(CS_MEM_HARDWALL, cs, val);
		break;
	case FILE_SCHED_LOAD_BALANCE:
		retval = update_flag(CS_SCHED_LOAD_BALANCE, cs, val);
		break;
	case FILE_MEMORY_MIGRATE:
		retval = update_flag(CS_MEMORY_MIGRATE, cs, val);
		break;
	case FILE_MEMORY_PRESSURE_ENABLED:
		cpuset_memory_pressure_enabled = !!val;
		break;
	case FILE_SPREAD_PAGE:
		retval = update_flag(CS_SPREAD_PAGE, cs, val);
		break;
	case FILE_SPREAD_SLAB:
		retval = update_flag(CS_SPREAD_SLAB, cs, val);
		break;
	default:
		retval = -EINVAL;
		break;
	}
out_unlock:
	mutex_unlock(&cpuset_mutex);
	return retval;
}

static int cpuset_write_s64(struct cgroup_subsys_statecss, struct cftypecft,
			    s64 val)
{
	struct cpusetcs = css_cs(css);
	cpuset_filetype_t type = cft->private;
	int retval = -ENODEV;

	mutex_lock(&cpuset_mutex);
	if (!is_cpuset_online(cs))
		goto out_unlock;

	switch (type) {
	case FILE_SCHED_RELAX_DOMAIN_LEVEL:
		retval = update_relax_domain_level(cs, val);
		break;
	default:
		retval = -EINVAL;
		break;
	}
out_unlock:
	mutex_unlock(&cpuset_mutex);
	return retval;
}

*/
 Common handling for a write to a "cpus" or "mems" file.
 /*
static ssize_t cpuset_write_resmask(struct kernfs_open_fileof,
				    charbuf, size_t nbytes, loff_t off)
{
	struct cpusetcs = css_cs(of_css(of));
	struct cpusettrialcs;
	int retval = -ENODEV;

	buf = strstrip(buf);

	*/
	 CPU or memory hotunplug may leave @cs w/o any execution
	 resources, in which case the hotplug code asynchronously updates
	 configuration and transfers all tasks to the nearest ancestor
	 which can execute.
	
	 As writes to "cpus" or "mems" may restore @cs's execution
	 resources, wait for the previously scheduled operations before
	 proceeding, so that we don't end up keep removing tasks added
	 after execution capability is restored.
	
	 cpuset_hotplug_work calls back into cgroup core via
	 cgroup_transfer_tasks() and waiting for it from a cgroupfs
	 operation like this one can lead to a deadlock through kernfs
	 active_ref protection.  Let's break the protection.  Losing the
	 protection is okay as we check whether @cs is online after
	 grabbing cpuset_mutex anyway.  This only happens on the legacy
	 hierarchies.
	 /*
	css_get(&cs->css);
	kernfs_break_active_protection(of->kn);
	flush_work(&cpuset_hotplug_work);

	mutex_lock(&cpuset_mutex);
	if (!is_cpuset_online(cs))
		goto out_unlock;

	trialcs = alloc_trial_cpuset(cs);
	if (!trialcs) {
		retval = -ENOMEM;
		goto out_unlock;
	}

	switch (of_cft(of)->private) {
	case FILE_CPULIST:
		retval = update_cpumask(cs, trialcs, buf);
		break;
	case FILE_MEMLIST:
		retval = update_nodemask(cs, trialcs, buf);
		break;
	default:
		retval = -EINVAL;
		break;
	}

	free_trial_cpuset(trialcs);
out_unlock:
	mutex_unlock(&cpuset_mutex);
	kernfs_unbreak_active_protection(of->kn);
	css_put(&cs->css);
	flush_workqueue(cpuset_migrate_mm_wq);
	return retval ?: nbytes;
}

*/
 These ascii lists should be read in a single call, by using a user
 buffer large enough to hold the entire map.  If read in smaller
 chunks, there is no guarantee of atomicity.  Since the display format
 used, list of ranges of sequential numbers, is variable length,
 and since these maps can change value dynamically, one could read
 gibberish by doing partial reads while a list was changing.
 /*
static int cpuset_common_seq_show(struct seq_filesf, voidv)
{
	struct cpusetcs = css_cs(seq_css(sf));
	cpuset_filetype_t type = seq_cft(sf)->private;
	int ret = 0;

	spin_lock_irq(&callback_lock);

	switch (type) {
	case FILE_CPULIST:
		seq_printf(sf, "%*pbl\n", cpumask_pr_args(cs->cpus_allowed));
		break;
	case FILE_MEMLIST:
		seq_printf(sf, "%*pbl\n", nodemask_pr_args(&cs->mems_allowed));
		break;
	case FILE_EFFECTIVE_CPULIST:
		seq_printf(sf, "%*pbl\n", cpumask_pr_args(cs->effective_cpus));
		break;
	case FILE_EFFECTIVE_MEMLIST:
		seq_printf(sf, "%*pbl\n", nodemask_pr_args(&cs->effective_mems));
		break;
	default:
		ret = -EINVAL;
	}

	spin_unlock_irq(&callback_lock);
	return ret;
}

static u64 cpuset_read_u64(struct cgroup_subsys_statecss, struct cftypecft)
{
	struct cpusetcs = css_cs(css);
	cpuset_filetype_t type = cft->private;
	switch (type) {
	case FILE_CPU_EXCLUSIVE:
		return is_cpu_exclusive(cs);
	case FILE_MEM_EXCLUSIVE:
		return is_mem_exclusive(cs);
	case FILE_MEM_HARDWALL:
		return is_mem_hardwall(cs);
	case FILE_SCHED_LOAD_BALANCE:
		return is_sched_load_balance(cs);
	case FILE_MEMORY_MIGRATE:
		return is_memory_migrate(cs);
	case FILE_MEMORY_PRESSURE_ENABLED:
		return cpuset_memory_pressure_enabled;
	case FILE_MEMORY_PRESSURE:
		return fmeter_getrate(&cs->fmeter);
	case FILE_SPREAD_PAGE:
		return is_spread_page(cs);
	case FILE_SPREAD_SLAB:
		return is_spread_slab(cs);
	default:
		BUG();
	}

	*/ Unreachable but makes gcc happy /*
	return 0;
}

static s64 cpuset_read_s64(struct cgroup_subsys_statecss, struct cftypecft)
{
	struct cpusetcs = css_cs(css);
	cpuset_filetype_t type = cft->private;
	switch (type) {
	case FILE_SCHED_RELAX_DOMAIN_LEVEL:
		return cs->relax_domain_level;
	default:
		BUG();
	}

	*/ Unrechable but makes gcc happy /*
	return 0;
}


*/
 for the common functions, 'private' gives the type of file
 /*

static struct cftype files[] = {
	{
		.name = "cpus",
		.seq_show = cpuset_common_seq_show,
		.write = cpuset_write_resmask,
		.max_write_len = (100U + 6 NR_CPUS),
		.private = FILE_CPULIST,
	},

	{
		.name = "mems",
		.seq_show = cpuset_common_seq_show,
		.write = cpuset_write_resmask,
		.max_write_len = (100U + 6 MAX_NUMNODES),
		.private = FILE_MEMLIST,
	},

	{
		.name = "effective_cpus",
		.seq_show = cpuset_common_seq_show,
		.private = FILE_EFFECTIVE_CPULIST,
	},

	{
		.name = "effective_mems",
		.seq_show = cpuset_common_seq_show,
		.private = FILE_EFFECTIVE_MEMLIST,
	},

	{
		.name = "cpu_exclusive",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_CPU_EXCLUSIVE,
	},

	{
		.name = "mem_exclusive",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_MEM_EXCLUSIVE,
	},

	{
		.name = "mem_hardwall",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_MEM_HARDWALL,
	},

	{
		.name = "sched_load_balance",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_SCHED_LOAD_BALANCE,
	},

	{
		.name = "sched_relax_domain_level",
		.read_s64 = cpuset_read_s64,
		.write_s64 = cpuset_write_s64,
		.private = FILE_SCHED_RELAX_DOMAIN_LEVEL,
	},

	{
		.name = "memory_migrate",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_MEMORY_MIGRATE,
	},

	{
		.name = "memory_pressure",
		.read_u64 = cpuset_read_u64,
	},

	{
		.name = "memory_spread_page",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_SPREAD_PAGE,
	},

	{
		.name = "memory_spread_slab",
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_SPREAD_SLAB,
	},

	{
		.name = "memory_pressure_enabled",
		.flags = CFTYPE_ONLY_ON_ROOT,
		.read_u64 = cpuset_read_u64,
		.write_u64 = cpuset_write_u64,
		.private = FILE_MEMORY_PRESSURE_ENABLED,
	},

	{ }	*/ terminate /*
};

*/
	cpuset_css_alloc - allocate a cpuset css
	cgrp:	control group that the new cpuset will be part of
 /*

static struct cgroup_subsys_state
cpuset_css_alloc(struct cgroup_subsys_stateparent_css)
{
	struct cpusetcs;

	if (!parent_css)
		return &top_cpuset.css;

	cs = kzalloc(sizeof(*cs), GFP_KERNEL);
	if (!cs)
		return ERR_PTR(-ENOMEM);
	if (!alloc_cpumask_var(&cs->cpus_allowed, GFP_KERNEL))
		goto free_cs;
	if (!alloc_cpumask_var(&cs->effective_cpus, GFP_KERNEL))
		goto free_cpus;

	set_bit(CS_SCHED_LOAD_BALANCE, &cs->flags);
	cpumask_clear(cs->cpus_allowed);
	nodes_clear(cs->mems_allowed);
	cpumask_clear(cs->effective_cpus);
	nodes_clear(cs->effective_mems);
	fmeter_init(&cs->fmeter);
	cs->relax_domain_level = -1;

	return &cs->css;

free_cpus:
	free_cpumask_var(cs->cpus_allowed);
free_cs:
	kfree(cs);
	return ERR_PTR(-ENOMEM);
}

static int cpuset_css_online(struct cgroup_subsys_statecss)
{
	struct cpusetcs = css_cs(css);
	struct cpusetparent = parent_cs(cs);
	struct cpusettmp_cs;
	struct cgroup_subsys_statepos_css;

	if (!parent)
		return 0;

	mutex_lock(&cpuset_mutex);

	set_bit(CS_ONLINE, &cs->flags);
	if (is_spread_page(parent))
		set_bit(CS_SPREAD_PAGE, &cs->flags);
	if (is_spread_slab(parent))
		set_bit(CS_SPREAD_SLAB, &cs->flags);

	cpuset_inc();

	spin_lock_irq(&callback_lock);
	if (cgroup_subsys_on_dfl(cpuset_cgrp_subsys)) {
		cpumask_copy(cs->effective_cpus, parent->effective_cpus);
		cs->effective_mems = parent->effective_mems;
	}
	spin_unlock_irq(&callback_lock);

	if (!test_bit(CGRP_CPUSET_CLONE_CHILDREN, &css->cgroup->flags))
		goto out_unlock;

	*/
	 Clone @parent's configuration if CGRP_CPUSET_CLONE_CHILDREN is
	 set.  This flag handling is implemented in cgroup core for
	 histrical reasons - the flag may be specified during mount.
	
	 Currently, if any sibling cpusets have exclusive cpus or mem, we
	 refuse to clone the configuration - thereby refusing the task to
	 be entered, and as a result refusing the sys_unshare() or
	 clone() which initiated it.  If this becomes a problem for some
	 users who wish to allow that scenario, then this could be
	 changed to grant parent->cpus_allowed-sibling_cpus_exclusive
	 (and likewise for mems) to the new cgroup.
	 /*
	rcu_read_lock();
	cpuset_for_each_child(tmp_cs, pos_css, parent) {
		if (is_mem_exclusive(tmp_cs) || is_cpu_exclusive(tmp_cs)) {
			rcu_read_unlock();
			goto out_unlock;
		}
	}
	rcu_read_unlock();

	spin_lock_irq(&callback_lock);
	cs->mems_allowed = parent->mems_allowed;
	cs->effective_mems = parent->mems_allowed;
	cpumask_copy(cs->cpus_allowed, parent->cpus_allowed);
	cpumask_copy(cs->effective_cpus, parent->cpus_allowed);
	spin_unlock_irq(&callback_lock);
out_unlock:
	mutex_unlock(&cpuset_mutex);
	return 0;
}

*/
 If the cpuset being removed has its flag 'sched_load_balance'
 enabled, then simulate turning sched_load_balance off, which
 will call rebuild_sched_domains_locked().
 /*

static void cpuset_css_offline(struct cgroup_subsys_statecss)
{
	struct cpusetcs = css_cs(css);

	mutex_lock(&cpuset_mutex);

	if (is_sched_load_balance(cs))
		update_flag(CS_SCHED_LOAD_BALANCE, cs, 0);

	cpuset_dec();
	clear_bit(CS_ONLINE, &cs->flags);

	mutex_unlock(&cpuset_mutex);
}

static void cpuset_css_free(struct cgroup_subsys_statecss)
{
	struct cpusetcs = css_cs(css);

	free_cpumask_var(cs->effective_cpus);
	free_cpumask_var(cs->cpus_allowed);
	kfree(cs);
}

static void cpuset_bind(struct cgroup_subsys_stateroot_css)
{
	mutex_lock(&cpuset_mutex);
	spin_lock_irq(&callback_lock);

	if (cgroup_subsys_on_dfl(cpuset_cgrp_subsys)) {
		cpumask_copy(top_cpuset.cpus_allowed, cpu_possible_mask);
		top_cpuset.mems_allowed = node_possible_map;
	} else {
		cpumask_copy(top_cpuset.cpus_allowed,
			     top_cpuset.effective_cpus);
		top_cpuset.mems_allowed = top_cpuset.effective_mems;
	}

	spin_unlock_irq(&callback_lock);
	mutex_unlock(&cpuset_mutex);
}

struct cgroup_subsys cpuset_cgrp_subsys = {
	.css_alloc	= cpuset_css_alloc,
	.css_online	= cpuset_css_online,
	.css_offline	= cpuset_css_offline,
	.css_free	= cpuset_css_free,
	.can_attach	= cpuset_can_attach,
	.cancel_attach	= cpuset_cancel_attach,
	.attach		= cpuset_attach,
	.post_attach	= cpuset_post_attach,
	.bind		= cpuset_bind,
	.legacy_cftypes	= files,
	.early_init	= true,
};

*/
 cpuset_init - initialize cpusets at system boot

 Description: Initialize top_cpuset and the cpuset internal file system,
/*

int __init cpuset_init(void)
{
	int err = 0;

	if (!alloc_cpumask_var(&top_cpuset.cpus_allowed, GFP_KERNEL))
		BUG();
	if (!alloc_cpumask_var(&top_cpuset.effective_cpus, GFP_KERNEL))
		BUG();

	cpumask_setall(top_cpuset.cpus_allowed);
	nodes_setall(top_cpuset.mems_allowed);
	cpumask_setall(top_cpuset.effective_cpus);
	nodes_setall(top_cpuset.effective_mems);

	fmeter_init(&top_cpuset.fmeter);
	set_bit(CS_SCHED_LOAD_BALANCE, &top_cpuset.flags);
	top_cpuset.relax_domain_level = -1;

	err = register_filesystem(&cpuset_fs_type);
	if (err < 0)
		return err;

	if (!alloc_cpumask_var(&cpus_attach, GFP_KERNEL))
		BUG();

	return 0;
}

*/
 If CPU and/or memory hotplug handlers, below, unplug any CPUs
 or memory nodes, we need to walk over the cpuset hierarchy,
 removing that CPU or node from all cpusets.  If this removes the
 last CPU or node from a cpuset, then move the tasks in the empty
 cpuset to its next-highest non-empty parent.
 /*
static void remove_tasks_in_empty_cpuset(struct cpusetcs)
{
	struct cpusetparent;

	*/
	 Find its next-highest non-empty parent, (top cpuset
	 has online cpus, so can't be empty).
	 /*
	parent = parent_cs(cs);
	while (cpumask_empty(parent->cpus_allowed) ||
			nodes_empty(parent->mems_allowed))
		parent = parent_cs(parent);

	if (cgroup_transfer_tasks(parent->css.cgroup, cs->css.cgroup)) {
		pr_err("cpuset: failed to transfer tasks out of empty cpuset ");
		pr_cont_cgroup_name(cs->css.cgroup);
		pr_cont("\n");
	}
}

static void
hotplug_update_tasks_legacy(struct cpusetcs,
			    struct cpumasknew_cpus, nodemask_tnew_mems,
			    bool cpus_updated, bool mems_updated)
{
	bool is_empty;

	spin_lock_irq(&callback_lock);
	cpumask_copy(cs->cpus_allowed, new_cpus);
	cpumask_copy(cs->effective_cpus, new_cpus);
	cs->mems_allowed =new_mems;
	cs->effective_mems =new_mems;
	spin_unlock_irq(&callback_lock);

	*/
	 Don't call update_tasks_cpumask() if the cpuset becomes empty,
	 as the tasks will be migratecd to an ancestor.
	 /*
	if (cpus_updated && !cpumask_empty(cs->cpus_allowed))
		update_tasks_cpumask(cs);
	if (mems_updated && !nodes_empty(cs->mems_allowed))
		update_tasks_nodemask(cs);

	is_empty = cpumask_empty(cs->cpus_allowed) ||
		   nodes_empty(cs->mems_allowed);

	mutex_unlock(&cpuset_mutex);

	*/
	 Move tasks to the nearest ancestor with execution resources,
	 This is full cgroup operation which will also call back into
	 cpuset. Should be done outside any lock.
	 /*
	if (is_empty)
		remove_tasks_in_empty_cpuset(cs);

	mutex_lock(&cpuset_mutex);
}

static void
hotplug_update_tasks(struct cpusetcs,
		     struct cpumasknew_cpus, nodemask_tnew_mems,
		     bool cpus_updated, bool mems_updated)
{
	if (cpumask_empty(new_cpus))
		cpumask_copy(new_cpus, parent_cs(cs)->effective_cpus);
	if (nodes_empty(*new_mems))
		*new_mems = parent_cs(cs)->effective_mems;

	spin_lock_irq(&callback_lock);
	cpumask_copy(cs->effective_cpus, new_cpus);
	cs->effective_mems =new_mems;
	spin_unlock_irq(&callback_lock);

	if (cpus_updated)
		update_tasks_cpumask(cs);
	if (mems_updated)
		update_tasks_nodemask(cs);
}

*/
 cpuset_hotplug_update_tasks - update tasks in a cpuset for hotunplug
 @cs: cpuset in interest

 Compare @cs's cpu and mem masks against top_cpuset and if some have gone
 offline, update @cs accordingly.  If @cs ends up with no CPU or memory,
 all its tasks are moved to the nearest ancestor with both resources.
 /*
static void cpuset_hotplug_update_tasks(struct cpusetcs)
{
	static cpumask_t new_cpus;
	static nodemask_t new_mems;
	bool cpus_updated;
	bool mems_updated;
retry:
	wait_event(cpuset_attach_wq, cs->attach_in_progress == 0);

	mutex_lock(&cpuset_mutex);

	*/
	 We have raced with task attaching. We wait until attaching
	 is finished, so we won't attach a task to an empty cpuset.
	 /*
	if (cs->attach_in_progress) {
		mutex_unlock(&cpuset_mutex);
		goto retry;
	}

	cpumask_and(&new_cpus, cs->cpus_allowed, parent_cs(cs)->effective_cpus);
	nodes_and(new_mems, cs->mems_allowed, parent_cs(cs)->effective_mems);

	cpus_updated = !cpumask_equal(&new_cpus, cs->effective_cpus);
	mems_updated = !nodes_equal(new_mems, cs->effective_mems);

	if (cgroup_subsys_on_dfl(cpuset_cgrp_subsys))
		hotplug_update_tasks(cs, &new_cpus, &new_mems,
				     cpus_updated, mems_updated);
	else
		hotplug_update_tasks_legacy(cs, &new_cpus, &new_mems,
					    cpus_updated, mems_updated);

	mutex_unlock(&cpuset_mutex);
}

*/
 cpuset_hotplug_workfn - handle CPU/memory hotunplug for a cpuset

 This function is called after either CPU or memory configuration has
 changed and updates cpuset accordingly.  The top_cpuset is always
 synchronized to cpu_active_mask and N_MEMORY, which is necessary in
 order to make cpusets transparent (of no affect) on systems that are
 actively using CPU hotplug but making no active use of cpusets.

 Non-root cpusets are only affected by offlining.  If any CPUs or memory
 nodes have been taken down, cpuset_hotplug_update_tasks() is invoked on
 all descendants.

 Note that CPU offlining during suspend is ignored.  We don't modify
 cpusets across suspend/resume cycles at all.
 /*
static void cpuset_hotplug_workfn(struct work_structwork)
{
	static cpumask_t new_cpus;
	static nodemask_t new_mems;
	bool cpus_updated, mems_updated;
	bool on_dfl = cgroup_subsys_on_dfl(cpuset_cgrp_subsys);

	mutex_lock(&cpuset_mutex);

	*/ fetch the available cpus/mems and find out which changed how /*
	cpumask_copy(&new_cpus, cpu_active_mask);
	new_mems = node_states[N_MEMORY];

	cpus_updated = !cpumask_equal(top_cpuset.effective_cpus, &new_cpus);
	mems_updated = !nodes_equal(top_cpuset.effective_mems, new_mems);

	*/ synchronize cpus_allowed to cpu_active_mask /*
	if (cpus_updated) {
		spin_lock_irq(&callback_lock);
		if (!on_dfl)
			cpumask_copy(top_cpuset.cpus_allowed, &new_cpus);
		cpumask_copy(top_cpuset.effective_cpus, &new_cpus);
		spin_unlock_irq(&callback_lock);
		*/ we don't mess with cpumasks of tasks in top_cpuset /*
	}

	*/ synchronize mems_allowed to N_MEMORY /*
	if (mems_updated) {
		spin_lock_irq(&callback_lock);
		if (!on_dfl)
			top_cpuset.mems_allowed = new_mems;
		top_cpuset.effective_mems = new_mems;
		spin_unlock_irq(&callback_lock);
		update_tasks_nodemask(&top_cpuset);
	}

	mutex_unlock(&cpuset_mutex);

	*/ if cpus or mems changed, we need to propagate to descendants /*
	if (cpus_updated || mems_updated) {
		struct cpusetcs;
		struct cgroup_subsys_statepos_css;

		rcu_read_lock();
		cpuset_for_each_descendant_pre(cs, pos_css, &top_cpuset) {
			if (cs == &top_cpuset || !css_tryget_online(&cs->css))
				continue;
			rcu_read_unlock();

			cpuset_hotplug_update_tasks(cs);

			rcu_read_lock();
			css_put(&cs->css);
		}
		rcu_read_unlock();
	}

	*/ rebuild sched domains if cpus_allowed has changed /*
	if (cpus_updated)
		rebuild_sched_domains();
}

void cpuset_update_active_cpus(bool cpu_online)
{
	*/
	 We're inside cpu hotplug critical region which usually nests
	 inside cgroup synchronization.  Bounce actual hotplug processing
	 to a work item to avoid reverse locking order.
	
	 We still need to do partition_sched_domains() synchronously;
	 otherwise, the scheduler will get confused and put tasks to the
	 dead CPU.  Fall back to the default single domain.
	 cpuset_hotplug_workfn() will rebuild it as necessary.
	 /*
	partition_sched_domains(1, NULL, NULL);
	schedule_work(&cpuset_hotplug_work);
}

*/
 Keep top_cpuset.mems_allowed tracking node_states[N_MEMORY].
 Call this routine anytime after node_states[N_MEMORY] changes.
 See cpuset_update_active_cpus() for CPU hotplug handling.
 /*
static int cpuset_track_online_nodes(struct notifier_blockself,
				unsigned long action, voidarg)
{
	schedule_work(&cpuset_hotplug_work);
	return NOTIFY_OK;
}

static struct notifier_block cpuset_track_online_nodes_nb = {
	.notifier_call = cpuset_track_online_nodes,
	.priority = 10,		*/ ??! /*
};

*/
 cpuset_init_smp - initialize cpus_allowed

 Description: Finish top cpuset after cpu, node maps are initialized
 /*
void __init cpuset_init_smp(void)
{
	cpumask_copy(top_cpuset.cpus_allowed, cpu_active_mask);
	top_cpuset.mems_allowed = node_states[N_MEMORY];
	top_cpuset.old_mems_allowed = top_cpuset.mems_allowed;

	cpumask_copy(top_cpuset.effective_cpus, cpu_active_mask);
	top_cpuset.effective_mems = node_states[N_MEMORY];

	register_hotmemory_notifier(&cpuset_track_online_nodes_nb);

	cpuset_migrate_mm_wq = alloc_ordered_workqueue("cpuset_migrate_mm", 0);
	BUG_ON(!cpuset_migrate_mm_wq);
}

*/
 cpuset_cpus_allowed - return cpus_allowed mask from a tasks cpuset.
 @tsk: pointer to task_struct from which to obtain cpuset->cpus_allowed.
 @pmask: pointer to struct cpumask variable to receive cpus_allowed set.

 Description: Returns the cpumask_var_t cpus_allowed of the cpuset
 attached to the specified @tsk.  Guaranteed to return some non-empty
 subset of cpu_online_mask, even if this means going outside the
 tasks cpuset.
/*

void cpuset_cpus_allowed(struct task_structtsk, struct cpumaskpmask)
{
	unsigned long flags;

	spin_lock_irqsave(&callback_lock, flags);
	rcu_read_lock();
	guarantee_online_cpus(task_cs(tsk), pmask);
	rcu_read_unlock();
	spin_unlock_irqrestore(&callback_lock, flags);
}

void cpuset_cpus_allowed_fallback(struct task_structtsk)
{
	rcu_read_lock();
	do_set_cpus_allowed(tsk, task_cs(tsk)->effective_cpus);
	rcu_read_unlock();

	*/
	 We own tsk->cpus_allowed, nobody can change it under us.
	
	 But we used cs && cs->cpus_allowed lockless and thus can
	 race with cgroup_attach_task() or update_cpumask() and get
	 the wrong tsk->cpus_allowed. However, both cases imply the
	 subsequent cpuset_change_cpumask()->set_cpus_allowed_ptr()
	 which takes task_rq_lock().
	
	 If we are called after it dropped the lock we must see all
	 changes in tsk_cs()->cpus_allowed. Otherwise we can temporary
	 set any mask even if it is not right from task_cs() pov,
	 the pending set_cpus_allowed_ptr() will fix things.
	
	 select_fallback_rq() will fix things ups and set cpu_possible_mask
	 if required.
	 /*
}

void __init cpuset_init_current_mems_allowed(void)
{
	nodes_setall(current->mems_allowed);
}

*/
 cpuset_mems_allowed - return mems_allowed mask from a tasks cpuset.
 @tsk: pointer to task_struct from which to obtain cpuset->mems_allowed.

 Description: Returns the nodemask_t mems_allowed of the cpuset
 attached to the specified @tsk.  Guaranteed to return some non-empty
 subset of node_states[N_MEMORY], even if this means going outside the
 tasks cpuset.
/*

nodemask_t cpuset_mems_allowed(struct task_structtsk)
{
	nodemask_t mask;
	unsigned long flags;

	spin_lock_irqsave(&callback_lock, flags);
	rcu_read_lock();
	guarantee_online_mems(task_cs(tsk), &mask);
	rcu_read_unlock();
	spin_unlock_irqrestore(&callback_lock, flags);

	return mask;
}

*/
 cpuset_nodemask_valid_mems_allowed - check nodemask vs. curremt mems_allowed
 @nodemask: the nodemask to be checked

 Are any of the nodes in the nodemask allowed in current->mems_allowed?
 /*
int cpuset_nodemask_valid_mems_allowed(nodemask_tnodemask)
{
	return nodes_intersects(*nodemask, current->mems_allowed);
}

*/
 nearest_hardwall_ancestor() - Returns the nearest mem_exclusive or
 mem_hardwall ancestor to the specified cpuset.  Call holding
 callback_lock.  If no ancestor is mem_exclusive or mem_hardwall
 (an unusual configuration), then returns the root cpuset.
 /*
static struct cpusetnearest_hardwall_ancestor(struct cpusetcs)
{
	while (!(is_mem_exclusive(cs) || is_mem_hardwall(cs)) && parent_cs(cs))
		cs = parent_cs(cs);
	return cs;
}

*/
 cpuset_node_allowed - Can we allocate on a memory node?
 @node: is this an allowed node?
 @gfp_mask: memory allocation flags

 If we're in interrupt, yes, we can always allocate.  If @node is set in
 current's mems_allowed, yes.  If it's not a __GFP_HARDWALL request and this
 node is set in the nearest hardwalled cpuset ancestor to current's cpuset,
 yes.  If current has access to memory reserves due to TIF_MEMDIE, yes.
 Otherwise, no.

 GFP_USER allocations are marked with the __GFP_HARDWALL bit,
 and do not allow allocations outside the current tasks cpuset
 unless the task has been OOM killed as is marked TIF_MEMDIE.
 GFP_KERNEL allocations are not so marked, so can escape to the
 nearest enclosing hardwalled ancestor cpuset.

 Scanning up parent cpusets requires callback_lock.  The
 __alloc_pages() routine only calls here with __GFP_HARDWALL bit
 _not_ set if it's a GFP_KERNEL allocation, and all nodes in the
 current tasks mems_allowed came up empty on the first pass over
 the zonelist.  So only GFP_KERNEL allocations, if all nodes in the
 cpuset are short of memory, might require taking the callback_lock.

 The first call here from mm/page_alloc:get_page_from_freelist()
 has __GFP_HARDWALL set in gfp_mask, enforcing hardwall cpusets,
 so no allocation on a node outside the cpuset is allowed (unless
 in interrupt, of course).

 The second pass through get_page_from_freelist() doesn't even call
 here for GFP_ATOMIC calls.  For those calls, the __alloc_pages()
 variable 'wait' is not set, and the bit ALLOC_CPUSET is not set
 in alloc_flags.  That logic and the checks below have the combined
 affect that:
	in_interrupt - any node ok (current task context irrelevant)
	GFP_ATOMIC   - any node ok
	TIF_MEMDIE   - any node ok
	GFP_KERNEL   - any node in enclosing hardwalled cpuset ok
	GFP_USER     - only nodes in current tasks mems allowed ok.
 /*
int __cpuset_node_allowed(int node, gfp_t gfp_mask)
{
	struct cpusetcs;		*/ current cpuset ancestors /*
	int allowed;			*/ is allocation in zone z allowed? /*
	unsigned long flags;

	if (in_interrupt())
		return 1;
	if (node_isset(node, current->mems_allowed))
		return 1;
	*/
	 Allow tasks that have access to memory reserves because they have
	 been OOM killed to get memory anywhere.
	 /*
	if (unlikely(test_thread_flag(TIF_MEMDIE)))
		return 1;
	if (gfp_mask & __GFP_HARDWALL)	*/ If hardwall request, stop here /*
		return 0;

	if (current->flags & PF_EXITING)/ Let dying task have memory /*
		return 1;

	*/ Not hardwall and node outside mems_allowed: scan up cpusets /*
	spin_lock_irqsave(&callback_lock, flags);

	rcu_read_lock();
	cs = nearest_hardwall_ancestor(task_cs(current));
	allowed = node_isset(node, cs->mems_allowed);
	rcu_read_unlock();

	spin_unlock_irqrestore(&callback_lock, flags);
	return allowed;
}

*/
 cpuset_mem_spread_node() - On which node to begin search for a file page
 cpuset_slab_spread_node() - On which node to begin search for a slab page

 If a task is marked PF_SPREAD_PAGE or PF_SPREAD_SLAB (as for
 tasks in a cpuset with is_spread_page or is_spread_slab set),
 and if the memory allocation used cpuset_mem_spread_node()
 to determine on which node to start looking, as it will for
 certain page cache or slab cache pages such as used for file
 system buffers and inode caches, then instead of starting on the
 local node to look for a free page, rather spread the starting
 node around the tasks mems_allowed nodes.

 We don't have to worry about the returned node being offline
 because "it can't happen", and even if it did, it would be ok.

 The routines calling guarantee_online_mems() are careful to
 only set nodes in task->mems_allowed that are online.  So it
 should not be possible for the following code to return an
 offline node.  But if it did, that would be ok, as this routine
 is not returning the node where the allocation must be, only
 the node where the search should start.  The zonelist passed to
 __alloc_pages() will include all nodes.  If the slab allocator
 is passed an offline node, it will fall back to the local node.
 See kmem_cache_alloc_node().
 /*

static int cpuset_spread_node(introtor)
{
	int node;

	node = next_node(*rotor, current->mems_allowed);
	if (node == MAX_NUMNODES)
		node = first_node(current->mems_allowed);
	*rotor = node;
	return node;
}

int cpuset_mem_spread_node(void)
{
	if (current->cpuset_mem_spread_rotor == NUMA_NO_NODE)
		current->cpuset_mem_spread_rotor =
			node_random(&current->mems_allowed);

	return cpuset_spread_node(&current->cpuset_mem_spread_rotor);
}

int cpuset_slab_spread_node(void)
{
	if (current->cpuset_slab_spread_rotor == NUMA_NO_NODE)
		current->cpuset_slab_spread_rotor =
			node_random(&current->mems_allowed);

	return cpuset_spread_node(&current->cpuset_slab_spread_rotor);
}

EXPORT_SYMBOL_GPL(cpuset_mem_spread_node);

*/
 cpuset_mems_allowed_intersects - Does @tsk1's mems_allowed intersect @tsk2's?
 @tsk1: pointer to task_struct of some task.
 @tsk2: pointer to task_struct of some other task.

 Description: Return true if @tsk1's mems_allowed intersects the
 mems_allowed of @tsk2.  Used by the OOM killer to determine if
 one of the task's memory usage might impact the memory available
 to the other.
/*

int cpuset_mems_allowed_intersects(const struct task_structtsk1,
				   const struct task_structtsk2)
{
	return nodes_intersects(tsk1->mems_allowed, tsk2->mems_allowed);
}

*/
 cpuset_print_current_mems_allowed - prints current's cpuset and mems_allowed

 Description: Prints current's name, cpuset name, and cached copy of its
 mems_allowed to the kernel log.
 /*
void cpuset_print_current_mems_allowed(void)
{
	struct cgroupcgrp;

	rcu_read_lock();

	cgrp = task_cs(current)->css.cgroup;
	pr_info("%s cpuset=", current->comm);
	pr_cont_cgroup_name(cgrp);
	pr_cont(" mems_allowed=%*pbl\n",
		nodemask_pr_args(&current->mems_allowed));

	rcu_read_unlock();
}

*/
 Collection of memory_pressure is suppressed unless
 this flag is enabled by writing "1" to the special
 cpuset file 'memory_pressure_enabled' in the root cpuset.
 /*

int cpuset_memory_pressure_enabled __read_mostly;

*/
 cpuset_memory_pressure_bump - keep stats of per-cpuset reclaims.

 Keep a running average of the rate of synchronous (direct)
 page reclaim efforts initiated by tasks in each cpuset.

 This represents the rate at which some task in the cpuset
 ran low on memory on all nodes it was allowed to use, and
 had to enter the kernels page reclaim code in an effort to
 create more free memory by tossing clean pages or swapping
 or writing dirty pages.

 Display to user space in the per-cpuset read-only file
 "memory_pressure".  Value displayed is an integer
 representing the recent rate of entry into the synchronous
 (direct) page reclaim by any task attached to the cpuset.
/*

void __cpuset_memory_pressure_bump(void)
{
	rcu_read_lock();
	fmeter_markevent(&task_cs(current)->fmeter);
	rcu_read_unlock();
}

#ifdef CONFIG_PROC_PID_CPUSET
*/
 proc_cpuset_show()
  - Print tasks cpuset path into seq_file.
  - Used for /proc/<pid>/cpuset.
  - No need to task_lock(tsk) on this tsk->cpuset reference, as it
    doesn't really matter if tsk->cpuset changes after we read it,
    and we take cpuset_mutex, keeping cpuset_attach() from changing it
    anyway.
 /*
int proc_cpuset_show(struct seq_filem, struct pid_namespacens,
		     struct pidpid, struct task_structtsk)
{
	charbuf,p;
	struct cgroup_subsys_statecss;
	int retval;

	retval = -ENOMEM;
	buf = kmalloc(PATH_MAX, GFP_KERNEL);
	if (!buf)
		goto out;

	retval = -ENAMETOOLONG;
	css = task_get_css(tsk, cpuset_cgrp_id);
	p = cgroup_path_ns(css->cgroup, buf, PATH_MAX,
			   current->nsproxy->cgroup_ns);
	css_put(css);
	if (!p)
		goto out_free;
	seq_puts(m, p);
	seq_putc(m, '\n');
	retval = 0;
out_free:
	kfree(buf);
out:
	return retval;
}
#endif */ CONFIG_PROC_PID_CPUSET /*

*/ Display task mems_allowed in /proc/<pid>/status file. /*
void cpuset_task_status_allowed(struct seq_filem, struct task_structtask)
{
	seq_printf(m, "Mems_allowed:\t%*pb\n",
		   nodemask_pr_args(&task->mems_allowed));
	seq_printf(m, "Mems_allowed_list:\t%*pbl\n",
		   nodemask_pr_args(&task->mems_allowed));
}
*/
/*
#include <linux/kernel.h>
#include <linux/crash_dump.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/export.h>

*/
 If we have booted due to a crash, max_pfn will be a very low value. We need
 to know the amount of memory that the previous kernel used.
 /*
unsigned long saved_max_pfn;

*/
 stores the physical address of elf header of crash image

 Note: elfcorehdr_addr is not just limited to vmcore. It is also used by
 is_kdump_kernel() to determine if we are booting after a panic. Hence put
 it under CONFIG_CRASH_DUMP and not CONFIG_PROC_VMCORE.
 /*
unsigned long long elfcorehdr_addr = ELFCORE_ADDR_MAX;
EXPORT_SYMBOL_GPL(elfcorehdr_addr);

*/
 stores the size of elf header of crash image
 /*
unsigned long long elfcorehdr_size;

*/
 elfcorehdr= specifies the location of elf core header stored by the crashed
 kernel. This option will be passed by kexec loader to the capture kernel.

 Syntax: elfcorehdr=[size[KMG]@]offset[KMG]
 /*
static int __init setup_elfcorehdr(chararg)
{
	charend;
	if (!arg)
		return -EINVAL;
	elfcorehdr_addr = memparse(arg, &end);
	if (*end == '@') {
		elfcorehdr_size = elfcorehdr_addr;
		elfcorehdr_addr = memparse(end + 1, &end);
	}
	return end > arg ? 0 : -EINVAL;
}
early_param("elfcorehdr", setup_elfcorehdr);
*/
 Task credentials management - see Documentation/security/credentials.txt

 Copyright (C) 2008 Red Hat, Inc. All Rights Reserved.
 Written by David Howells (dhowells@redhat.com)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence
 as published by the Free Software Foundation; either version
 2 of the Licence, or (at your option) any later version.
 /*
#include <linux/export.h>
#include <linux/cred.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/key.h>
#include <linux/keyctl.h>
#include <linux/init_task.h>
#include <linux/security.h>
#include <linux/binfmts.h>
#include <linux/cn_proc.h>

#if 0
#define kdebug(FMT, ...)						\
	printk("[%-5.5s%5u] " FMT "\n",					\
	       current->comm, current->pid, ##__VA_ARGS__)
#else
#define kdebug(FMT, ...)						\
do {									\
	if (0)								\
		no_printk("[%-5.5s%5u] " FMT "\n",			\
			  current->comm, current->pid, ##__VA_ARGS__);	\
} while (0)
#endif

static struct kmem_cachecred_jar;

*/ init to 2 - one for init_task, one to ensure it is never freed /*
struct group_info init_groups = { .usage = ATOMIC_INIT(2) };

*/
 The initial credentials for the initial task
 /*
struct cred init_cred = {
	.usage			= ATOMIC_INIT(4),
#ifdef CONFIG_DEBUG_CREDENTIALS
	.subscribers		= ATOMIC_INIT(2),
	.magic			= CRED_MAGIC,
#endif
	.uid			= GLOBAL_ROOT_UID,
	.gid			= GLOBAL_ROOT_GID,
	.suid			= GLOBAL_ROOT_UID,
	.sgid			= GLOBAL_ROOT_GID,
	.euid			= GLOBAL_ROOT_UID,
	.egid			= GLOBAL_ROOT_GID,
	.fsuid			= GLOBAL_ROOT_UID,
	.fsgid			= GLOBAL_ROOT_GID,
	.securebits		= SECUREBITS_DEFAULT,
	.cap_inheritable	= CAP_EMPTY_SET,
	.cap_permitted		= CAP_FULL_SET,
	.cap_effective		= CAP_FULL_SET,
	.cap_bset		= CAP_FULL_SET,
	.user			= INIT_USER,
	.user_ns		= &init_user_ns,
	.group_info		= &init_groups,
};

static inline void set_cred_subscribers(struct credcred, int n)
{
#ifdef CONFIG_DEBUG_CREDENTIALS
	atomic_set(&cred->subscribers, n);
#endif
}

static inline int read_cred_subscribers(const struct credcred)
{
#ifdef CONFIG_DEBUG_CREDENTIALS
	return atomic_read(&cred->subscribers);
#else
	return 0;
#endif
}

static inline void alter_cred_subscribers(const struct cred_cred, int n)
{
#ifdef CONFIG_DEBUG_CREDENTIALS
	struct credcred = (struct cred) _cred;

	atomic_add(n, &cred->subscribers);
#endif
}

*/
 The RCU callback to actually dispose of a set of credentials
 /*
static void put_cred_rcu(struct rcu_headrcu)
{
	struct credcred = container_of(rcu, struct cred, rcu);

	kdebug("put_cred_rcu(%p)", cred);

#ifdef CONFIG_DEBUG_CREDENTIALS
	if (cred->magic != CRED_MAGIC_DEAD ||
	    atomic_read(&cred->usage) != 0 ||
	    read_cred_subscribers(cred) != 0)
		panic("CRED: put_cred_rcu() sees %p with"
		      " mag %x, put %p, usage %d, subscr %d\n",
		      cred, cred->magic, cred->put_addr,
		      atomic_read(&cred->usage),
		      read_cred_subscribers(cred));
#else
	if (atomic_read(&cred->usage) != 0)
		panic("CRED: put_cred_rcu() sees %p with usage %d\n",
		      cred, atomic_read(&cred->usage));
#endif

	security_cred_free(cred);
	key_put(cred->session_keyring);
	key_put(cred->process_keyring);
	key_put(cred->thread_keyring);
	key_put(cred->request_key_auth);
	if (cred->group_info)
		put_group_info(cred->group_info);
	free_uid(cred->user);
	put_user_ns(cred->user_ns);
	kmem_cache_free(cred_jar, cred);
}

*/
 __put_cred - Destroy a set of credentials
 @cred: The record to release

 Destroy a set of credentials on which no references remain.
 /*
void __put_cred(struct credcred)
{
	kdebug("__put_cred(%p{%d,%d})", cred,
	       atomic_read(&cred->usage),
	       read_cred_subscribers(cred));

	BUG_ON(atomic_read(&cred->usage) != 0);
#ifdef CONFIG_DEBUG_CREDENTIALS
	BUG_ON(read_cred_subscribers(cred) != 0);
	cred->magic = CRED_MAGIC_DEAD;
	cred->put_addr = __builtin_return_address(0);
#endif
	BUG_ON(cred == current->cred);
	BUG_ON(cred == current->real_cred);

	call_rcu(&cred->rcu, put_cred_rcu);
}
EXPORT_SYMBOL(__put_cred);

*/
 Clean up a task's credentials when it exits
 /*
void exit_creds(struct task_structtsk)
{
	struct credcred;

	kdebug("exit_creds(%u,%p,%p,{%d,%d})", tsk->pid, tsk->real_cred, tsk->cred,
	       atomic_read(&tsk->cred->usage),
	       read_cred_subscribers(tsk->cred));

	cred = (struct cred) tsk->real_cred;
	tsk->real_cred = NULL;
	validate_creds(cred);
	alter_cred_subscribers(cred, -1);
	put_cred(cred);

	cred = (struct cred) tsk->cred;
	tsk->cred = NULL;
	validate_creds(cred);
	alter_cred_subscribers(cred, -1);
	put_cred(cred);
}

*/
 get_task_cred - Get another task's objective credentials
 @task: The task to query

 Get the objective credentials of a task, pinning them so that they can't go
 away.  Accessing a task's credentials directly is not permitted.

 The caller must also make sure task doesn't get deleted, either by holding a
 ref on task or by holding tasklist_lock to prevent it from being unlinked.
 /*
const struct credget_task_cred(struct task_structtask)
{
	const struct credcred;

	rcu_read_lock();

	do {
		cred = __task_cred((task));
		BUG_ON(!cred);
	} while (!atomic_inc_not_zero(&((struct cred)cred)->usage));

	rcu_read_unlock();
	return cred;
}

*/
 Allocate blank credentials, such that the credentials can be filled in at a
 later date without risk of ENOMEM.
 /*
struct credcred_alloc_blank(void)
{
	struct crednew;

	new = kmem_cache_zalloc(cred_jar, GFP_KERNEL);
	if (!new)
		return NULL;

	atomic_set(&new->usage, 1);
#ifdef CONFIG_DEBUG_CREDENTIALS
	new->magic = CRED_MAGIC;
#endif

	if (security_cred_alloc_blank(new, GFP_KERNEL) < 0)
		goto error;

	return new;

error:
	abort_creds(new);
	return NULL;
}

*/
 prepare_creds - Prepare a new set of credentials for modification

 Prepare a new set of task credentials for modification.  A task's creds
 shouldn't generally be modified directly, therefore this function is used to
 prepare a new copy, which the caller then modifies and then commits by
 calling commit_creds().

 Preparation involves making a copy of the objective creds for modification.

 Returns a pointer to the new creds-to-be if successful, NULL otherwise.

 Call commit_creds() or abort_creds() to clean up.
 /*
struct credprepare_creds(void)
{
	struct task_structtask = current;
	const struct credold;
	struct crednew;

	validate_process_creds();

	new = kmem_cache_alloc(cred_jar, GFP_KERNEL);
	if (!new)
		return NULL;

	kdebug("prepare_creds() alloc %p", new);

	old = task->cred;
	memcpy(new, old, sizeof(struct cred));

	atomic_set(&new->usage, 1);
	set_cred_subscribers(new, 0);
	get_group_info(new->group_info);
	get_uid(new->user);
	get_user_ns(new->user_ns);

#ifdef CONFIG_KEYS
	key_get(new->session_keyring);
	key_get(new->process_keyring);
	key_get(new->thread_keyring);
	key_get(new->request_key_auth);
#endif

#ifdef CONFIG_SECURITY
	new->security = NULL;
#endif

	if (security_prepare_creds(new, old, GFP_KERNEL) < 0)
		goto error;
	validate_creds(new);
	return new;

error:
	abort_creds(new);
	return NULL;
}
EXPORT_SYMBOL(prepare_creds);

*/
 Prepare credentials for current to perform an execve()
 - The caller must hold ->cred_guard_mutex
 /*
struct credprepare_exec_creds(void)
{
	struct crednew;

	new = prepare_creds();
	if (!new)
		return new;

#ifdef CONFIG_KEYS
	*/ newly exec'd tasks don't get a thread keyring /*
	key_put(new->thread_keyring);
	new->thread_keyring = NULL;

	*/ inherit the session keyring; new process keyring /*
	key_put(new->process_keyring);
	new->process_keyring = NULL;
#endif

	return new;
}

*/
 Copy credentials for the new process created by fork()

 We share if we can, but under some circumstances we have to generate a new
 set.

 The new process gets the current process's subjective credentials as its
 objective and subjective credentials
 /*
int copy_creds(struct task_structp, unsigned long clone_flags)
{
	struct crednew;
	int ret;

	if (
#ifdef CONFIG_KEYS
		!p->cred->thread_keyring &&
#endif
		clone_flags & CLONE_THREAD
	    ) {
		p->real_cred = get_cred(p->cred);
		get_cred(p->cred);
		alter_cred_subscribers(p->cred, 2);
		kdebug("share_creds(%p{%d,%d})",
		       p->cred, atomic_read(&p->cred->usage),
		       read_cred_subscribers(p->cred));
		atomic_inc(&p->cred->user->processes);
		return 0;
	}

	new = prepare_creds();
	if (!new)
		return -ENOMEM;

	if (clone_flags & CLONE_NEWUSER) {
		ret = create_user_ns(new);
		if (ret < 0)
			goto error_put;
	}

#ifdef CONFIG_KEYS
	*/ new threads get their own thread keyrings if their parent already
	 had one /*
	if (new->thread_keyring) {
		key_put(new->thread_keyring);
		new->thread_keyring = NULL;
		if (clone_flags & CLONE_THREAD)
			install_thread_keyring_to_cred(new);
	}

	*/ The process keyring is only shared between the threads in a process;
	 anything outside of those threads doesn't inherit.
	 /*
	if (!(clone_flags & CLONE_THREAD)) {
		key_put(new->process_keyring);
		new->process_keyring = NULL;
	}
#endif

	atomic_inc(&new->user->processes);
	p->cred = p->real_cred = get_cred(new);
	alter_cred_subscribers(new, 2);
	validate_creds(new);
	return 0;

error_put:
	put_cred(new);
	return ret;
}

static bool cred_cap_issubset(const struct credset, const struct credsubset)
{
	const struct user_namespaceset_ns = set->user_ns;
	const struct user_namespacesubset_ns = subset->user_ns;

	*/ If the two credentials are in the same user namespace see if
	 the capabilities of subset are a subset of set.
	 /*
	if (set_ns == subset_ns)
		return cap_issubset(subset->cap_permitted, set->cap_permitted);

	*/ The credentials are in a different user namespaces
	 therefore one is a subset of the other only if a set is an
	 ancestor of subset and set->euid is owner of subset or one
	 of subsets ancestors.
	 /*
	for (;subset_ns != &init_user_ns; subset_ns = subset_ns->parent) {
		if ((set_ns == subset_ns->parent)  &&
		    uid_eq(subset_ns->owner, set->euid))
			return true;
	}

	return false;
}

*/
 commit_creds - Install new credentials upon the current task
 @new: The credentials to be assigned

 Install a new set of credentials to the current task, using RCU to replace
 the old set.  Both the objective and the subjective credentials pointers are
 updated.  This function may not be called if the subjective credentials are
 in an overridden state.

 This function eats the caller's reference to the new credentials.

 Always returns 0 thus allowing this function to be tail-called at the end
 of, say, sys_setgid().
 /*
int commit_creds(struct crednew)
{
	struct task_structtask = current;
	const struct credold = task->real_cred;

	kdebug("commit_creds(%p{%d,%d})", new,
	       atomic_read(&new->usage),
	       read_cred_subscribers(new));

	BUG_ON(task->cred != old);
#ifdef CONFIG_DEBUG_CREDENTIALS
	BUG_ON(read_cred_subscribers(old) < 2);
	validate_creds(old);
	validate_creds(new);
#endif
	BUG_ON(atomic_read(&new->usage) < 1);

	get_cred(new);/ we will require a ref for the subj creds too /*

	*/ dumpability changes /*
	if (!uid_eq(old->euid, new->euid) ||
	    !gid_eq(old->egid, new->egid) ||
	    !uid_eq(old->fsuid, new->fsuid) ||
	    !gid_eq(old->fsgid, new->fsgid) ||
	    !cred_cap_issubset(old, new)) {
		if (task->mm)
			set_dumpable(task->mm, suid_dumpable);
		task->pdeath_signal = 0;
		smp_wmb();
	}

	*/ alter the thread keyring /*
	if (!uid_eq(new->fsuid, old->fsuid))
		key_fsuid_changed(task);
	if (!gid_eq(new->fsgid, old->fsgid))
		key_fsgid_changed(task);

	*/ do it
	 RLIMIT_NPROC limits on user->processes have already been checked
	 in set_user().
	 /*
	alter_cred_subscribers(new, 2);
	if (new->user != old->user)
		atomic_inc(&new->user->processes);
	rcu_assign_pointer(task->real_cred, new);
	rcu_assign_pointer(task->cred, new);
	if (new->user != old->user)
		atomic_dec(&old->user->processes);
	alter_cred_subscribers(old, -2);

	*/ send notifications /*
	if (!uid_eq(new->uid,   old->uid)  ||
	    !uid_eq(new->euid,  old->euid) ||
	    !uid_eq(new->suid,  old->suid) ||
	    !uid_eq(new->fsuid, old->fsuid))
		proc_id_connector(task, PROC_EVENT_UID);

	if (!gid_eq(new->gid,   old->gid)  ||
	    !gid_eq(new->egid,  old->egid) ||
	    !gid_eq(new->sgid,  old->sgid) ||
	    !gid_eq(new->fsgid, old->fsgid))
		proc_id_connector(task, PROC_EVENT_GID);

	*/ release the old obj and subj refs both /*
	put_cred(old);
	put_cred(old);
	return 0;
}
EXPORT_SYMBOL(commit_creds);

*/
 abort_creds - Discard a set of credentials and unlock the current task
 @new: The credentials that were going to be applied

 Discard a set of credentials that were under construction and unlock the
 current task.
 /*
void abort_creds(struct crednew)
{
	kdebug("abort_creds(%p{%d,%d})", new,
	       atomic_read(&new->usage),
	       read_cred_subscribers(new));

#ifdef CONFIG_DEBUG_CREDENTIALS
	BUG_ON(read_cred_subscribers(new) != 0);
#endif
	BUG_ON(atomic_read(&new->usage) < 1);
	put_cred(new);
}
EXPORT_SYMBOL(abort_creds);

*/
 override_creds - Override the current process's subjective credentials
 @new: The credentials to be assigned

 Install a set of temporary override subjective credentials on the current
 process, returning the old set for later reversion.
 /*
const struct credoverride_creds(const struct crednew)
{
	const struct credold = current->cred;

	kdebug("override_creds(%p{%d,%d})", new,
	       atomic_read(&new->usage),
	       read_cred_subscribers(new));

	validate_creds(old);
	validate_creds(new);
	get_cred(new);
	alter_cred_subscribers(new, 1);
	rcu_assign_pointer(current->cred, new);
	alter_cred_subscribers(old, -1);

	kdebug("override_creds() = %p{%d,%d}", old,
	       atomic_read(&old->usage),
	       read_cred_subscribers(old));
	return old;
}
EXPORT_SYMBOL(override_creds);

*/
 revert_creds - Revert a temporary subjective credentials override
 @old: The credentials to be restored

 Revert a temporary set of override subjective credentials to an old set,
 discarding the override set.
 /*
void revert_creds(const struct credold)
{
	const struct credoverride = current->cred;

	kdebug("revert_creds(%p{%d,%d})", old,
	       atomic_read(&old->usage),
	       read_cred_subscribers(old));

	validate_creds(old);
	validate_creds(override);
	alter_cred_subscribers(old, 1);
	rcu_assign_pointer(current->cred, old);
	alter_cred_subscribers(override, -1);
	put_cred(override);
}
EXPORT_SYMBOL(revert_creds);

*/
 initialise the credentials stuff
 /*
void __init cred_init(void)
{
	*/ allocate a slab in which we can store credentials /*
	cred_jar = kmem_cache_create("cred_jar", sizeof(struct cred), 0,
			SLAB_HWCACHE_ALIGN|SLAB_PANIC|SLAB_ACCOUNT, NULL);
}

*/
 prepare_kernel_cred - Prepare a set of credentials for a kernel service
 @daemon: A userspace daemon to be used as a reference

 Prepare a set of credentials for a kernel service.  This can then be used to
 override a task's own credentials so that work can be done on behalf of that
 task that requires a different subjective context.

 @daemon is used to provide a base for the security record, but can be NULL.
 If @daemon is supplied, then the security data will be derived from that;
 otherwise they'll be set to 0 and no groups, full capabilities and no keys.

 The caller may change these controls afterwards if desired.

 Returns the new credentials or NULL if out of memory.

 Does not take, and does not return holding current->cred_replace_mutex.
 /*
struct credprepare_kernel_cred(struct task_structdaemon)
{
	const struct credold;
	struct crednew;

	new = kmem_cache_alloc(cred_jar, GFP_KERNEL);
	if (!new)
		return NULL;

	kdebug("prepare_kernel_cred() alloc %p", new);

	if (daemon)
		old = get_task_cred(daemon);
	else
		old = get_cred(&init_cred);

	validate_creds(old);

	*new =old;
	atomic_set(&new->usage, 1);
	set_cred_subscribers(new, 0);
	get_uid(new->user);
	get_user_ns(new->user_ns);
	get_group_info(new->group_info);

#ifdef CONFIG_KEYS
	new->session_keyring = NULL;
	new->process_keyring = NULL;
	new->thread_keyring = NULL;
	new->request_key_auth = NULL;
	new->jit_keyring = KEY_REQKEY_DEFL_THREAD_KEYRING;
#endif

#ifdef CONFIG_SECURITY
	new->security = NULL;
#endif
	if (security_prepare_creds(new, old, GFP_KERNEL) < 0)
		goto error;

	put_cred(old);
	validate_creds(new);
	return new;

error:
	put_cred(new);
	put_cred(old);
	return NULL;
}
EXPORT_SYMBOL(prepare_kernel_cred);

*/
 set_security_override - Set the security ID in a set of credentials
 @new: The credentials to alter
 @secid: The LSM security ID to set

 Set the LSM security ID in a set of credentials so that the subjective
 security is overridden when an alternative set of credentials is used.
 /*
int set_security_override(struct crednew, u32 secid)
{
	return security_kernel_act_as(new, secid);
}
EXPORT_SYMBOL(set_security_override);

*/
 set_security_override_from_ctx - Set the security ID in a set of credentials
 @new: The credentials to alter
 @secctx: The LSM security context to generate the security ID from.

 Set the LSM security ID in a set of credentials so that the subjective
 security is overridden when an alternative set of credentials is used.  The
 security ID is specified in string form as a security context to be
 interpreted by the LSM.
 /*
int set_security_override_from_ctx(struct crednew, const charsecctx)
{
	u32 secid;
	int ret;

	ret = security_secctx_to_secid(secctx, strlen(secctx), &secid);
	if (ret < 0)
		return ret;

	return set_security_override(new, secid);
}
EXPORT_SYMBOL(set_security_override_from_ctx);

*/
 set_create_files_as - Set the LSM file create context in a set of credentials
 @new: The credentials to alter
 @inode: The inode to take the context from

 Change the LSM file creation context in a set of credentials to be the same
 as the object context of the specified inode, so that the new inodes have
 the same MAC context as that inode.
 /*
int set_create_files_as(struct crednew, struct inodeinode)
{
	new->fsuid = inode->i_uid;
	new->fsgid = inode->i_gid;
	return security_kernel_create_files_as(new, inode);
}
EXPORT_SYMBOL(set_create_files_as);

#ifdef CONFIG_DEBUG_CREDENTIALS

bool creds_are_invalid(const struct credcred)
{
	if (cred->magic != CRED_MAGIC)
		return true;
#ifdef CONFIG_SECURITY_SELINUX
	*/
	 cred->security == NULL if security_cred_alloc_blank() or
	 security_prepare_creds() returned an error.
	 /*
	if (selinux_is_enabled() && cred->security) {
		if ((unsigned long) cred->security < PAGE_SIZE)
			return true;
		if ((*(u32)cred->security & 0xffffff00) ==
		    (POISON_FREE << 24 | POISON_FREE << 16 | POISON_FREE << 8))
			return true;
	}
#endif
	return false;
}
EXPORT_SYMBOL(creds_are_invalid);

*/
 dump invalid credentials
 /*
static void dump_invalid_creds(const struct credcred, const charlabel,
			       const struct task_structtsk)
{
	printk(KERN_ERR "CRED: %s credentials: %p %s%s%s\n",
	       label, cred,
	       cred == &init_cred ? "[init]" : "",
	       cred == tsk->real_cred ? "[real]" : "",
	       cred == tsk->cred ? "[eff]" : "");
	printk(KERN_ERR "CRED: ->magic=%x, put_addr=%p\n",
	       cred->magic, cred->put_addr);
	printk(KERN_ERR "CRED: ->usage=%d, subscr=%d\n",
	       atomic_read(&cred->usage),
	       read_cred_subscribers(cred));
	printk(KERN_ERR "CRED: ->*uid = { %d,%d,%d,%d }\n",
		from_kuid_munged(&init_user_ns, cred->uid),
		from_kuid_munged(&init_user_ns, cred->euid),
		from_kuid_munged(&init_user_ns, cred->suid),
		from_kuid_munged(&init_user_ns, cred->fsuid));
	printk(KERN_ERR "CRED: ->*gid = { %d,%d,%d,%d }\n",
		from_kgid_munged(&init_user_ns, cred->gid),
		from_kgid_munged(&init_user_ns, cred->egid),
		from_kgid_munged(&init_user_ns, cred->sgid),
		from_kgid_munged(&init_user_ns, cred->fsgid));
#ifdef CONFIG_SECURITY
	printk(KERN_ERR "CRED: ->security is %p\n", cred->security);
	if ((unsigned long) cred->security >= PAGE_SIZE &&
	    (((unsigned long) cred->security & 0xffffff00) !=
	     (POISON_FREE << 24 | POISON_FREE << 16 | POISON_FREE << 8)))
		printk(KERN_ERR "CRED: ->security {%x, %x}\n",
		       ((u32*)cred->security)[0],
		       ((u32*)cred->security)[1]);
#endif
}

*/
 report use of invalid credentials
 /*
void __invalid_creds(const struct credcred, const charfile, unsigned line)
{
	printk(KERN_ERR "CRED: Invalid credentials\n");
	printk(KERN_ERR "CRED: At %s:%u\n", file, line);
	dump_invalid_creds(cred, "Specified", current);
	BUG();
}
EXPORT_SYMBOL(__invalid_creds);

*/
 check the credentials on a process
 /*
void __validate_process_creds(struct task_structtsk,
			      const charfile, unsigned line)
{
	if (tsk->cred == tsk->real_cred) {
		if (unlikely(read_cred_subscribers(tsk->cred) < 2 ||
			     creds_are_invalid(tsk->cred)))
			goto invalid_creds;
	} else {
		if (unlikely(read_cred_subscribers(tsk->real_cred) < 1 ||
			     read_cred_subscribers(tsk->cred) < 1 ||
			     creds_are_invalid(tsk->real_cred) ||
			     creds_are_invalid(tsk->cred)))
			goto invalid_creds;
	}
	return;

invalid_creds:
	printk(KERN_ERR "CRED: Invalid process credentials\n");
	printk(KERN_ERR "CRED: At %s:%u\n", file, line);

	dump_invalid_creds(tsk->real_cred, "Real", tsk);
	if (tsk->cred != tsk->real_cred)
		dump_invalid_creds(tsk->cred, "Effective", tsk);
	else
		printk(KERN_ERR "CRED: Effective creds == Real creds\n");
	BUG();
}
EXPORT_SYMBOL(__validate_process_creds);

*/
 check creds for do_exit()
 /*
void validate_creds_for_do_exit(struct task_structtsk)
{
	kdebug("validate_creds_for_do_exit(%p,%p{%d,%d})",
	       tsk->real_cred, tsk->cred,
	       atomic_read(&tsk->cred->usage),
	       read_cred_subscribers(tsk->cred));

	__validate_process_creds(tsk, __FILE__, __LINE__);
}

#endif */ CONFIG_DEBUG_CREDENTIALS 
*/
 delayacct.c - per-task delay accounting

 Copyright (C) Shailabh Nagar, IBM Corp. 2006

 This program is free software;  you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it would be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 the GNU General Public License for more details.
 /*

#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/taskstats.h>
#include <linux/time.h>
#include <linux/sysctl.h>
#include <linux/delayacct.h>
#include <linux/module.h>

int delayacct_on __read_mostly = 1;	*/ Delay accounting turned on/off /*
EXPORT_SYMBOL_GPL(delayacct_on);
struct kmem_cachedelayacct_cache;

static int __init delayacct_setup_disable(charstr)
{
	delayacct_on = 0;
	return 1;
}
__setup("nodelayacct", delayacct_setup_disable);

void delayacct_init(void)
{
	delayacct_cache = KMEM_CACHE(task_delay_info, SLAB_PANIC|SLAB_ACCOUNT);
	delayacct_tsk_init(&init_task);
}

void __delayacct_tsk_init(struct task_structtsk)
{
	tsk->delays = kmem_cache_zalloc(delayacct_cache, GFP_KERNEL);
	if (tsk->delays)
		spin_lock_init(&tsk->delays->lock);
}

*/
 Finish delay accounting for a statistic using its timestamps (@start),
 accumalator (@total) and @count
 /*
static void delayacct_end(u64start, u64total, u32count)
{
	s64 ns = ktime_get_ns() -start;
	unsigned long flags;

	if (ns > 0) {
		spin_lock_irqsave(&current->delays->lock, flags);
		*total += ns;
		(*count)++;
		spin_unlock_irqrestore(&current->delays->lock, flags);
	}
}

void __delayacct_blkio_start(void)
{
	current->delays->blkio_start = ktime_get_ns();
}

void __delayacct_blkio_end(void)
{
	if (current->delays->flags & DELAYACCT_PF_SWAPIN)
		*/ Swapin block I/O /*
		delayacct_end(&current->delays->blkio_start,
			&current->delays->swapin_delay,
			&current->delays->swapin_count);
	else	*/ Other block I/O /*
		delayacct_end(&current->delays->blkio_start,
			&current->delays->blkio_delay,
			&current->delays->blkio_count);
}

int __delayacct_add_tsk(struct taskstatsd, struct task_structtsk)
{
	cputime_t utime, stime, stimescaled, utimescaled;
	unsigned long long t2, t3;
	unsigned long flags, t1;
	s64 tmp;

	task_cputime(tsk, &utime, &stime);
	tmp = (s64)d->cpu_run_real_total;
	tmp += cputime_to_nsecs(utime + stime);
	d->cpu_run_real_total = (tmp < (s64)d->cpu_run_real_total) ? 0 : tmp;

	task_cputime_scaled(tsk, &utimescaled, &stimescaled);
	tmp = (s64)d->cpu_scaled_run_real_total;
	tmp += cputime_to_nsecs(utimescaled + stimescaled);
	d->cpu_scaled_run_real_total =
		(tmp < (s64)d->cpu_scaled_run_real_total) ? 0 : tmp;

	*/
	 No locking available for sched_info (and too expensive to add one)
	 Mitigate by taking snapshot of values
	 /*
	t1 = tsk->sched_info.pcount;
	t2 = tsk->sched_info.run_delay;
	t3 = tsk->se.sum_exec_runtime;

	d->cpu_count += t1;

	tmp = (s64)d->cpu_delay_total + t2;
	d->cpu_delay_total = (tmp < (s64)d->cpu_delay_total) ? 0 : tmp;

	tmp = (s64)d->cpu_run_virtual_total + t3;
	d->cpu_run_virtual_total =
		(tmp < (s64)d->cpu_run_virtual_total) ?	0 : tmp;

	*/ zero XXX_total, non-zero XXX_count implies XXX stat overflowed /*

	spin_lock_irqsave(&tsk->delays->lock, flags);
	tmp = d->blkio_delay_total + tsk->delays->blkio_delay;
	d->blkio_delay_total = (tmp < d->blkio_delay_total) ? 0 : tmp;
	tmp = d->swapin_delay_total + tsk->delays->swapin_delay;
	d->swapin_delay_total = (tmp < d->swapin_delay_total) ? 0 : tmp;
	tmp = d->freepages_delay_total + tsk->delays->freepages_delay;
	d->freepages_delay_total = (tmp < d->freepages_delay_total) ? 0 : tmp;
	d->blkio_count += tsk->delays->blkio_count;
	d->swapin_count += tsk->delays->swapin_count;
	d->freepages_count += tsk->delays->freepages_count;
	spin_unlock_irqrestore(&tsk->delays->lock, flags);

	return 0;
}

__u64 __delayacct_blkio_ticks(struct task_structtsk)
{
	__u64 ret;
	unsigned long flags;

	spin_lock_irqsave(&tsk->delays->lock, flags);
	ret = nsec_to_clock_t(tsk->delays->blkio_delay +
				tsk->delays->swapin_delay);
	spin_unlock_irqrestore(&tsk->delays->lock, flags);
	return ret;
}

void __delayacct_freepages_start(void)
{
	current->delays->freepages_start = ktime_get_ns();
}

void __delayacct_freepages_end(void)
{
	delayacct_end(&current->delays->freepages_start,
			&current->delays->freepages_delay,
			&current->delays->freepages_count);
}
*/

 linux/kernel/dma.c: A DMA channel allocator. Inspired by linux/kernel/irq.c.

 Written by Hennus Bergman, 1992.

 1994/12/26: Changes by Alex Nash to fix a minor bug in /proc/dma.
   In the previous version the reported device could end up being wrong,
   if a device requested a DMA channel that was already in use.
   [It also happened to remove the sizeof(char) == sizeof(int)
   assumption introduced because of those /proc/dma patches. -- Hennus]
 /*
#include <linux/export.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/spinlock.h>
#include <linux/string.h>
#include <linux/seq_file.h>
#include <linux/proc_fs.h>
#include <linux/init.h>
#include <asm/dma.h>



*/ A note on resource allocation:

 All drivers needing DMA channels, should allocate and release them
 through the public routines `request_dma()' and `free_dma()'.

 In order to avoid problems, all processes should allocate resources in
 the same sequence and release them in the reverse order.

 So, when allocating DMAs and IRQs, first allocate the IRQ, then the DMA.
 When releasing them, first release the DMA, then release the IRQ.
 If you don't, you may cause allocation requests to fail unnecessarily.
 This doesn't really matter now, but it will once we get real semaphores
 in the kernel.
 /*


DEFINE_SPINLOCK(dma_spin_lock);

*/
	If our port doesn't define this it has no PC like DMA
 /*

#ifdef MAX_DMA_CHANNELS


*/ Channel n is busy iff dma_chan_busy[n].lock != 0.
 DMA0 used to be reserved for DRAM refresh, but apparently not any more...
 DMA4 is reserved for cascading.
 /*

struct dma_chan {
	int  lock;
	const chardevice_id;
};

static struct dma_chan dma_chan_busy[MAX_DMA_CHANNELS] = {
	[4] = { 1, "cascade" },
};


*/
 request_dma - request and reserve a system DMA channel
 @dmanr: DMA channel number
 @device_id: reserving device ID string, used in /proc/dma
 /*
int request_dma(unsigned int dmanr, const char device_id)
{
	if (dmanr >= MAX_DMA_CHANNELS)
		return -EINVAL;

	if (xchg(&dma_chan_busy[dmanr].lock, 1) != 0)
		return -EBUSY;

	dma_chan_busy[dmanr].device_id = device_id;

	*/ old flag was 0, now contains 1 to indicate busy /*
	return 0;
}/ request_dma /*

*/
 free_dma - free a reserved system DMA channel
 @dmanr: DMA channel number
 /*
void free_dma(unsigned int dmanr)
{
	if (dmanr >= MAX_DMA_CHANNELS) {
		printk(KERN_WARNING "Trying to free DMA%d\n", dmanr);
		return;
	}

	if (xchg(&dma_chan_busy[dmanr].lock, 0) == 0) {
		printk(KERN_WARNING "Trying to free free DMA%d\n", dmanr);
		return;
	}

}/ free_dma /*

#else

int request_dma(unsigned int dmanr, const chardevice_id)
{
	return -EINVAL;
}

void free_dma(unsigned int dmanr)
{
}

#endif

#ifdef CONFIG_PROC_FS

#ifdef MAX_DMA_CHANNELS
static int proc_dma_show(struct seq_filem, voidv)
{
	int i;

	for (i = 0 ; i < MAX_DMA_CHANNELS ; i++) {
		if (dma_chan_busy[i].lock) {
			seq_printf(m, "%2d: %s\n", i,
				   dma_chan_busy[i].device_id);
		}
	}
	return 0;
}
#else
static int proc_dma_show(struct seq_filem, voidv)
{
	seq_puts(m, "No DMA\n");
	return 0;
}
#endif */ MAX_DMA_CHANNELS /*

static int proc_dma_open(struct inodeinode, struct filefile)
{
	return single_open(file, proc_dma_show, NULL);
}

static const struct file_operations proc_dma_operations = {
	.open		= proc_dma_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_dma_init(void)
{
	proc_create("dma", 0, NULL, &proc_dma_operations);
	return 0;
}

__initcall(proc_dma_init);
#endif

EXPORT_SYMBOL(request_dma);
EXPORT_SYMBOL(free_dma);
EXPORT_SYMBOL(dma_spin_lock);
*/
/*
#include <linux/elf.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/binfmts.h>

Elf_Half __weak elf_core_extra_phdrs(void)
{
	return 0;
}

int __weak elf_core_write_extra_phdrs(struct coredump_paramscprm, loff_t offset)
{
	return 1;
}

int __weak elf_core_write_extra_data(struct coredump_paramscprm)
{
	return 1;
}

size_t __weak elf_core_extra_data_size(void)
{
	return 0;
}
*/

 Handling of different ABIs (personalities).

 We group personalities into execution domains which have their
 own handlers for kernel entry points, signal mapping, etc...

 2001-05-06	Complete rewrite,  Christoph Hellwig (hch@infradead.org)
 /*

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kmod.h>
#include <linux/module.h>
#include <linux/personality.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/syscalls.h>
#include <linux/sysctl.h>
#include <linux/types.h>
#include <linux/fs_struct.h>

#ifdef CONFIG_PROC_FS
static int execdomains_proc_show(struct seq_filem, voidv)
{
	seq_puts(m, "0-0\tLinux           \t[kernel]\n");
	return 0;
}

static int execdomains_proc_open(struct inodeinode, struct filefile)
{
	return single_open(file, execdomains_proc_show, NULL);
}

static const struct file_operations execdomains_proc_fops = {
	.open		= execdomains_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_execdomains_init(void)
{
	proc_create("execdomains", 0, NULL, &execdomains_proc_fops);
	return 0;
}
module_init(proc_execdomains_init);
#endif

SYSCALL_DEFINE1(personality, unsigned int, personality)
{
	unsigned int old = current->personality;

	if (personality != 0xffffffff)
		set_personality(personality);

	return old;
}
*/

  linux/kernel/exit.c

  Copyright (C) 1991, 1992  Linus Torvalds
 /*

#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/capability.h>
#include <linux/completion.h>
#include <linux/personality.h>
#include <linux/tty.h>
#include <linux/iocontext.h>
#include <linux/key.h>
#include <linux/security.h>
#include <linux/cpu.h>
#include <linux/acct.h>
#include <linux/tsacct_kern.h>
#include <linux/file.h>
#include <linux/fdtable.h>
#include <linux/freezer.h>
#include <linux/binfmts.h>
#include <linux/nsproxy.h>
#include <linux/pid_namespace.h>
#include <linux/ptrace.h>
#include <linux/profile.h>
#include <linux/mount.h>
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/mempolicy.h>
#include <linux/taskstats_kern.h>
#include <linux/delayacct.h>
#include <linux/cgroup.h>
#include <linux/syscalls.h>
#include <linux/signal.h>
#include <linux/posix-timers.h>
#include <linux/cn_proc.h>
#include <linux/mutex.h>
#include <linux/futex.h>
#include <linux/pipe_fs_i.h>
#include <linux/audit.h>/ for audit_free() /*
#include <linux/resource.h>
#include <linux/blkdev.h>
#include <linux/task_io_accounting_ops.h>
#include <linux/tracehook.h>
#include <linux/fs_struct.h>
#include <linux/init_task.h>
#include <linux/perf_event.h>
#include <trace/events/sched.h>
#include <linux/hw_breakpoint.h>
#include <linux/oom.h>
#include <linux/writeback.h>
#include <linux/shm.h>
#include <linux/kcov.h>

#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <asm/pgtable.h>
#include <asm/mmu_context.h>

static void __unhash_process(struct task_structp, bool group_dead)
{
	nr_threads--;
	detach_pid(p, PIDTYPE_PID);
	if (group_dead) {
		detach_pid(p, PIDTYPE_PGID);
		detach_pid(p, PIDTYPE_SID);

		list_del_rcu(&p->tasks);
		list_del_init(&p->sibling);
		__this_cpu_dec(process_counts);
	}
	list_del_rcu(&p->thread_group);
	list_del_rcu(&p->thread_node);
}

*/
 This function expects the tasklist_lock write-locked.
 /*
static void __exit_signal(struct task_structtsk)
{
	struct signal_structsig = tsk->signal;
	bool group_dead = thread_group_leader(tsk);
	struct sighand_structsighand;
	struct tty_structuninitialized_var(tty);
	cputime_t utime, stime;

	sighand = rcu_dereference_check(tsk->sighand,
					lockdep_tasklist_lock_is_held());
	spin_lock(&sighand->siglock);

	posix_cpu_timers_exit(tsk);
	if (group_dead) {
		posix_cpu_timers_exit_group(tsk);
		tty = sig->tty;
		sig->tty = NULL;
	} else {
		*/
		 This can only happen if the caller is de_thread().
		 FIXME: this is the temporary hack, we should teach
		 posix-cpu-timers to handle this case correctly.
		 /*
		if (unlikely(has_group_leader_pid(tsk)))
			posix_cpu_timers_exit_group(tsk);

		*/
		 If there is any task waiting for the group exit
		 then notify it:
		 /*
		if (sig->notify_count > 0 && !--sig->notify_count)
			wake_up_process(sig->group_exit_task);

		if (tsk == sig->curr_target)
			sig->curr_target = next_thread(tsk);
	}

	*/
	 Accumulate here the counters for all threads as they die. We could
	 skip the group leader because it is the last user of signal_struct,
	 but we want to avoid the race with thread_group_cputime() which can
	 see the empty ->thread_head list.
	 /*
	task_cputime(tsk, &utime, &stime);
	write_seqlock(&sig->stats_lock);
	sig->utime += utime;
	sig->stime += stime;
	sig->gtime += task_gtime(tsk);
	sig->min_flt += tsk->min_flt;
	sig->maj_flt += tsk->maj_flt;
	sig->nvcsw += tsk->nvcsw;
	sig->nivcsw += tsk->nivcsw;
	sig->inblock += task_io_get_inblock(tsk);
	sig->oublock += task_io_get_oublock(tsk);
	task_io_accounting_add(&sig->ioac, &tsk->ioac);
	sig->sum_sched_runtime += tsk->se.sum_exec_runtime;
	sig->nr_threads--;
	__unhash_process(tsk, group_dead);
	write_sequnlock(&sig->stats_lock);

	*/
	 Do this under ->siglock, we can race with another thread
	 doing sigqueue_free() if we have SIGQUEUE_PREALLOC signals.
	 /*
	flush_sigqueue(&tsk->pending);
	tsk->sighand = NULL;
	spin_unlock(&sighand->siglock);

	__cleanup_sighand(sighand);
	clear_tsk_thread_flag(tsk, TIF_SIGPENDING);
	if (group_dead) {
		flush_sigqueue(&sig->shared_pending);
		tty_kref_put(tty);
	}
}

static void delayed_put_task_struct(struct rcu_headrhp)
{
	struct task_structtsk = container_of(rhp, struct task_struct, rcu);

	perf_event_delayed_put(tsk);
	trace_sched_process_free(tsk);
	put_task_struct(tsk);
}


void release_task(struct task_structp)
{
	struct task_structleader;
	int zap_leader;
repeat:
	*/ don't need to get the RCU readlock here - the process is dead and
	 can't be modifying its own credentials. But shut RCU-lockdep up /*
	rcu_read_lock();
	atomic_dec(&__task_cred(p)->user->processes);
	rcu_read_unlock();

	proc_flush_task(p);

	write_lock_irq(&tasklist_lock);
	ptrace_release_task(p);
	__exit_signal(p);

	*/
	 If we are the last non-leader member of the thread
	 group, and the leader is zombie, then notify the
	 group leader's parent process. (if it wants notification.)
	 /*
	zap_leader = 0;
	leader = p->group_leader;
	if (leader != p && thread_group_empty(leader)
			&& leader->exit_state == EXIT_ZOMBIE) {
		*/
		 If we were the last child thread and the leader has
		 exited already, and the leader's parent ignores SIGCHLD,
		 then we are the one who should release the leader.
		 /*
		zap_leader = do_notify_parent(leader, leader->exit_signal);
		if (zap_leader)
			leader->exit_state = EXIT_DEAD;
	}

	write_unlock_irq(&tasklist_lock);
	release_thread(p);
	call_rcu(&p->rcu, delayed_put_task_struct);

	p = leader;
	if (unlikely(zap_leader))
		goto repeat;
}

*/
 Determine if a process group is "orphaned", according to the POSIX
 definition in 2.2.2.52.  Orphaned process groups are not to be affected
 by terminal-generated stop signals.  Newly orphaned process groups are
 to receive a SIGHUP and a SIGCONT.

 "I ask you, have you ever known what it is to be an orphan?"
 /*
static int will_become_orphaned_pgrp(struct pidpgrp,
					struct task_structignored_task)
{
	struct task_structp;

	do_each_pid_task(pgrp, PIDTYPE_PGID, p) {
		if ((p == ignored_task) ||
		    (p->exit_state && thread_group_empty(p)) ||
		    is_global_init(p->real_parent))
			continue;

		if (task_pgrp(p->real_parent) != pgrp &&
		    task_session(p->real_parent) == task_session(p))
			return 0;
	} while_each_pid_task(pgrp, PIDTYPE_PGID, p);

	return 1;
}

int is_current_pgrp_orphaned(void)
{
	int retval;

	read_lock(&tasklist_lock);
	retval = will_become_orphaned_pgrp(task_pgrp(current), NULL);
	read_unlock(&tasklist_lock);

	return retval;
}

static bool has_stopped_jobs(struct pidpgrp)
{
	struct task_structp;

	do_each_pid_task(pgrp, PIDTYPE_PGID, p) {
		if (p->signal->flags & SIGNAL_STOP_STOPPED)
			return true;
	} while_each_pid_task(pgrp, PIDTYPE_PGID, p);

	return false;
}

*/
 Check to see if any process groups have become orphaned as
 a result of our exiting, and if they have any stopped jobs,
 send them a SIGHUP and then a SIGCONT. (POSIX 3.2.2.2)
 /*
static void
kill_orphaned_pgrp(struct task_structtsk, struct task_structparent)
{
	struct pidpgrp = task_pgrp(tsk);
	struct task_structignored_task = tsk;

	if (!parent)
		*/ exit: our father is in a different pgrp than
		 we are and we were the only connection outside.
		 /*
		parent = tsk->real_parent;
	else
		*/ reparent: our child is in a different pgrp than
		 we are, and it was the only connection outside.
		 /*
		ignored_task = NULL;

	if (task_pgrp(parent) != pgrp &&
	    task_session(parent) == task_session(tsk) &&
	    will_become_orphaned_pgrp(pgrp, ignored_task) &&
	    has_stopped_jobs(pgrp)) {
		__kill_pgrp_info(SIGHUP, SEND_SIG_PRIV, pgrp);
		__kill_pgrp_info(SIGCONT, SEND_SIG_PRIV, pgrp);
	}
}

#ifdef CONFIG_MEMCG
*/
 A task is exiting.   If it owned this mm, find a new owner for the mm.
 /*
void mm_update_next_owner(struct mm_structmm)
{
	struct task_structc,g,p = current;

retry:
	*/
	 If the exiting or execing task is not the owner, it's
	 someone else's problem.
	 /*
	if (mm->owner != p)
		return;
	*/
	 The current owner is exiting/execing and there are no other
	 candidates.  Do not leave the mm pointing to a possibly
	 freed task structure.
	 /*
	if (atomic_read(&mm->mm_users) <= 1) {
		mm->owner = NULL;
		return;
	}

	read_lock(&tasklist_lock);
	*/
	 Search in the children
	 /*
	list_for_each_entry(c, &p->children, sibling) {
		if (c->mm == mm)
			goto assign_new_owner;
	}

	*/
	 Search in the siblings
	 /*
	list_for_each_entry(c, &p->real_parent->children, sibling) {
		if (c->mm == mm)
			goto assign_new_owner;
	}

	*/
	 Search through everything else, we should not get here often.
	 /*
	for_each_process(g) {
		if (g->flags & PF_KTHREAD)
			continue;
		for_each_thread(g, c) {
			if (c->mm == mm)
				goto assign_new_owner;
			if (c->mm)
				break;
		}
	}
	read_unlock(&tasklist_lock);
	*/
	 We found no owner yet mm_users > 1: this implies that we are
	 most likely racing with swapoff (try_to_unuse()) or /proc or
	 ptrace or page migration (get_task_mm()).  Mark owner as NULL.
	 /*
	mm->owner = NULL;
	return;

assign_new_owner:
	BUG_ON(c == p);
	get_task_struct(c);
	*/
	 The task_lock protects c->mm from changing.
	 We always want mm->owner->mm == mm
	 /*
	task_lock(c);
	*/
	 Delay read_unlock() till we have the task_lock()
	 to ensure that c does not slip away underneath us
	 /*
	read_unlock(&tasklist_lock);
	if (c->mm != mm) {
		task_unlock(c);
		put_task_struct(c);
		goto retry;
	}
	mm->owner = c;
	task_unlock(c);
	put_task_struct(c);
}
#endif */ CONFIG_MEMCG /*

*/
 Turn us into a lazy TLB process if we
 aren't already..
 /*
static void exit_mm(struct task_structtsk)
{
	struct mm_structmm = tsk->mm;
	struct core_statecore_state;

	mm_release(tsk, mm);
	if (!mm)
		return;
	sync_mm_rss(mm);
	*/
	 Serialize with any possible pending coredump.
	 We must hold mmap_sem around checking core_state
	 and clearing tsk->mm.  The core-inducing thread
	 will increment ->nr_threads for each thread in the
	 group with ->mm != NULL.
	 /*
	down_read(&mm->mmap_sem);
	core_state = mm->core_state;
	if (core_state) {
		struct core_thread self;

		up_read(&mm->mmap_sem);

		self.task = tsk;
		self.next = xchg(&core_state->dumper.next, &self);
		*/
		 Implies mb(), the result of xchg() must be visible
		 to core_state->dumper.
		 /*
		if (atomic_dec_and_test(&core_state->nr_threads))
			complete(&core_state->startup);

		for (;;) {
			set_task_state(tsk, TASK_UNINTERRUPTIBLE);
			if (!self.task)/ see coredump_finish() /*
				break;
			freezable_schedule();
		}
		__set_task_state(tsk, TASK_RUNNING);
		down_read(&mm->mmap_sem);
	}
	atomic_inc(&mm->mm_count);
	BUG_ON(mm != tsk->active_mm);
	*/ more a memory barrier than a real lock /*
	task_lock(tsk);
	tsk->mm = NULL;
	up_read(&mm->mmap_sem);
	enter_lazy_tlb(mm, current);
	task_unlock(tsk);
	mm_update_next_owner(mm);
	mmput(mm);
	if (test_thread_flag(TIF_MEMDIE))
		exit_oom_victim(tsk);
}

static struct task_structfind_alive_thread(struct task_structp)
{
	struct task_structt;

	for_each_thread(p, t) {
		if (!(t->flags & PF_EXITING))
			return t;
	}
	return NULL;
}

static struct task_structfind_child_reaper(struct task_structfather)
	__releases(&tasklist_lock)
	__acquires(&tasklist_lock)
{
	struct pid_namespacepid_ns = task_active_pid_ns(father);
	struct task_structreaper = pid_ns->child_reaper;

	if (likely(reaper != father))
		return reaper;

	reaper = find_alive_thread(father);
	if (reaper) {
		pid_ns->child_reaper = reaper;
		return reaper;
	}

	write_unlock_irq(&tasklist_lock);
	if (unlikely(pid_ns == &init_pid_ns)) {
		panic("Attempted to kill init! exitcode=0x%08x\n",
			father->signal->group_exit_code ?: father->exit_code);
	}
	zap_pid_ns_processes(pid_ns);
	write_lock_irq(&tasklist_lock);

	return father;
}

*/
 When we die, we re-parent all our children, and try to:
 1. give them to another thread in our thread group, if such a member exists
 2. give it to the first ancestor process which prctl'd itself as a
    child_subreaper for its children (like a service manager)
 3. give it to the init process (PID 1) in our pid namespace
 /*
static struct task_structfind_new_reaper(struct task_structfather,
					   struct task_structchild_reaper)
{
	struct task_structthread,reaper;

	thread = find_alive_thread(father);
	if (thread)
		return thread;

	if (father->signal->has_child_subreaper) {
		*/
		 Find the first ->is_child_subreaper ancestor in our pid_ns.
		 We start from father to ensure we can not look into another
		 namespace, this is safe because all its threads are dead.
		 /*
		for (reaper = father;
		     !same_thread_group(reaper, child_reaper);
		     reaper = reaper->real_parent) {
			*/ call_usermodehelper() descendants need this check /*
			if (reaper == &init_task)
				break;
			if (!reaper->signal->is_child_subreaper)
				continue;
			thread = find_alive_thread(reaper);
			if (thread)
				return thread;
		}
	}

	return child_reaper;
}

*/
* Any that need to be release_task'd are put on the @dead list.
 /*
static void reparent_leader(struct task_structfather, struct task_structp,
				struct list_headdead)
{
	if (unlikely(p->exit_state == EXIT_DEAD))
		return;

	*/ We don't want people slaying init. /*
	p->exit_signal = SIGCHLD;

	*/ If it has exited notify the new parent about this child's death. /*
	if (!p->ptrace &&
	    p->exit_state == EXIT_ZOMBIE && thread_group_empty(p)) {
		if (do_notify_parent(p, p->exit_signal)) {
			p->exit_state = EXIT_DEAD;
			list_add(&p->ptrace_entry, dead);
		}
	}

	kill_orphaned_pgrp(p, father);
}

*/
 This does two things:

 A.  Make init inherit all the child processes
 B.  Check to see if any process groups have become orphaned
	as a result of our exiting, and if they have any stopped
	jobs, send them a SIGHUP and then a SIGCONT.  (POSIX 3.2.2.2)
 /*
static void forget_original_parent(struct task_structfather,
					struct list_headdead)
{
	struct task_structp,t,reaper;

	if (unlikely(!list_empty(&father->ptraced)))
		exit_ptrace(father, dead);

	*/ Can drop and reacquire tasklist_lock /*
	reaper = find_child_reaper(father);
	if (list_empty(&father->children))
		return;

	reaper = find_new_reaper(father, reaper);
	list_for_each_entry(p, &father->children, sibling) {
		for_each_thread(p, t) {
			t->real_parent = reaper;
			BUG_ON((!t->ptrace) != (t->parent == father));
			if (likely(!t->ptrace))
				t->parent = t->real_parent;
			if (t->pdeath_signal)
				group_send_sig_info(t->pdeath_signal,
						    SEND_SIG_NOINFO, t);
		}
		*/
		 If this is a threaded reparent there is no need to
		 notify anyone anything has happened.
		 /*
		if (!same_thread_group(reaper, father))
			reparent_leader(father, p, dead);
	}
	list_splice_tail_init(&father->children, &reaper->children);
}

*/
 Send signals to all our closest relatives so that they know
 to properly mourn us..
 /*
static void exit_notify(struct task_structtsk, int group_dead)
{
	bool autoreap;
	struct task_structp,n;
	LIST_HEAD(dead);

	write_lock_irq(&tasklist_lock);
	forget_original_parent(tsk, &dead);

	if (group_dead)
		kill_orphaned_pgrp(tsk->group_leader, NULL);

	if (unlikely(tsk->ptrace)) {
		int sig = thread_group_leader(tsk) &&
				thread_group_empty(tsk) &&
				!ptrace_reparented(tsk) ?
			tsk->exit_signal : SIGCHLD;
		autoreap = do_notify_parent(tsk, sig);
	} else if (thread_group_leader(tsk)) {
		autoreap = thread_group_empty(tsk) &&
			do_notify_parent(tsk, tsk->exit_signal);
	} else {
		autoreap = true;
	}

	tsk->exit_state = autoreap ? EXIT_DEAD : EXIT_ZOMBIE;
	if (tsk->exit_state == EXIT_DEAD)
		list_add(&tsk->ptrace_entry, &dead);

	*/ mt-exec, de_thread() is waiting for group leader /*
	if (unlikely(tsk->signal->notify_count < 0))
		wake_up_process(tsk->signal->group_exit_task);
	write_unlock_irq(&tasklist_lock);

	list_for_each_entry_safe(p, n, &dead, ptrace_entry) {
		list_del_init(&p->ptrace_entry);
		release_task(p);
	}
}

#ifdef CONFIG_DEBUG_STACK_USAGE
static void check_stack_usage(void)
{
	static DEFINE_SPINLOCK(low_water_lock);
	static int lowest_to_date = THREAD_SIZE;
	unsigned long free;

	free = stack_not_used(current);

	if (free >= lowest_to_date)
		return;

	spin_lock(&low_water_lock);
	if (free < lowest_to_date) {
		pr_warn("%s (%d) used greatest stack depth: %lu bytes left\n",
			current->comm, task_pid_nr(current), free);
		lowest_to_date = free;
	}
	spin_unlock(&low_water_lock);
}
#else
static inline void check_stack_usage(void) {}
#endif

void do_exit(long code)
{
	struct task_structtsk = current;
	int group_dead;
	TASKS_RCU(int tasks_rcu_i);

	profile_task_exit(tsk);
	kcov_task_exit(tsk);

	WARN_ON(blk_needs_flush_plug(tsk));

	if (unlikely(in_interrupt()))
		panic("Aiee, killing interrupt handler!");
	if (unlikely(!tsk->pid))
		panic("Attempted to kill the idle task!");

	*/
	 If do_exit is called because this processes oopsed, it's possible
	 that get_fs() was left as KERNEL_DS, so reset it to USER_DS before
	 continuing. Amongst other possible reasons, this is to prevent
	 mm_release()->clear_child_tid() from writing to a user-controlled
	 kernel address.
	 /*
	set_fs(USER_DS);

	ptrace_event(PTRACE_EVENT_EXIT, code);

	validate_creds_for_do_exit(tsk);

	*/
	 We're taking recursive faults here in do_exit. Safest is to just
	 leave this task alone and wait for reboot.
	 /*
	if (unlikely(tsk->flags & PF_EXITING)) {
		pr_alert("Fixing recursive fault but reboot is needed!\n");
		*/
		 We can do this unlocked here. The futex code uses
		 this flag just to verify whether the pi state
		 cleanup has been done or not. In the worst case it
		 loops once more. We pretend that the cleanup was
		 done as there is no way to return. Either the
		 OWNER_DIED bit is set by now or we push the blocked
		 task into the wait for ever nirwana as well.
		 /*
		tsk->flags |= PF_EXITPIDONE;
		set_current_state(TASK_UNINTERRUPTIBLE);
		schedule();
	}

	exit_signals(tsk); / sets PF_EXITING /*
	*/
	 tsk->flags are checked in the futex code to protect against
	 an exiting task cleaning up the robust pi futexes.
	 /*
	smp_mb();
	raw_spin_unlock_wait(&tsk->pi_lock);

	if (unlikely(in_atomic())) {
		pr_info("note: %s[%d] exited with preempt_count %d\n",
			current->comm, task_pid_nr(current),
			preempt_count());
		preempt_count_set(PREEMPT_ENABLED);
	}

	*/ sync mm's RSS info before statistics gathering /*
	if (tsk->mm)
		sync_mm_rss(tsk->mm);
	acct_update_integrals(tsk);
	group_dead = atomic_dec_and_test(&tsk->signal->live);
	if (group_dead) {
		hrtimer_cancel(&tsk->signal->real_timer);
		exit_itimers(tsk->signal);
		if (tsk->mm)
			setmax_mm_hiwater_rss(&tsk->signal->maxrss, tsk->mm);
	}
	acct_collect(code, group_dead);
	if (group_dead)
		tty_audit_exit();
	audit_free(tsk);

	tsk->exit_code = code;
	taskstats_exit(tsk, group_dead);

	exit_mm(tsk);

	if (group_dead)
		acct_process();
	trace_sched_process_exit(tsk);

	exit_sem(tsk);
	exit_shm(tsk);
	exit_files(tsk);
	exit_fs(tsk);
	if (group_dead)
		disassociate_ctty(1);
	exit_task_namespaces(tsk);
	exit_task_work(tsk);
	exit_thread();

	*/
	 Flush inherited counters to the parent - before the parent
	 gets woken up by child-exit notifications.
	
	 because of cgroup mode, must be called before cgroup_exit()
	 /*
	perf_event_exit_task(tsk);

	cgroup_exit(tsk);

	*/
	 FIXME: do that only when needed, using sched_exit tracepoint
	 /*
	flush_ptrace_hw_breakpoint(tsk);

	TASKS_RCU(preempt_disable());
	TASKS_RCU(tasks_rcu_i = __srcu_read_lock(&tasks_rcu_exit_srcu));
	TASKS_RCU(preempt_enable());
	exit_notify(tsk, group_dead);
	proc_exit_connector(tsk);
#ifdef CONFIG_NUMA
	task_lock(tsk);
	mpol_put(tsk->mempolicy);
	tsk->mempolicy = NULL;
	task_unlock(tsk);
#endif
#ifdef CONFIG_FUTEX
	if (unlikely(current->pi_state_cache))
		kfree(current->pi_state_cache);
#endif
	*/
	 Make sure we are holding no locks:
	 /*
	debug_check_no_locks_held();
	*/
	 We can do this unlocked here. The futex code uses this flag
	 just to verify whether the pi state cleanup has been done
	 or not. In the worst case it loops once more.
	 /*
	tsk->flags |= PF_EXITPIDONE;

	if (tsk->io_context)
		exit_io_context(tsk);

	if (tsk->splice_pipe)
		free_pipe_info(tsk->splice_pipe);

	if (tsk->task_frag.page)
		put_page(tsk->task_frag.page);

	validate_creds_for_do_exit(tsk);

	check_stack_usage();
	preempt_disable();
	if (tsk->nr_dirtied)
		__this_cpu_add(dirty_throttle_leaks, tsk->nr_dirtied);
	exit_rcu();
	TASKS_RCU(__srcu_read_unlock(&tasks_rcu_exit_srcu, tasks_rcu_i));

	*/
	 The setting of TASK_RUNNING by try_to_wake_up() may be delayed
	 when the following two conditions become true.
	   - There is race condition of mmap_sem (It is acquired by
	     exit_mm()), and
	   - SMI occurs before setting TASK_RUNINNG.
	     (or hypervisor of virtual machine switches to other guest)
	  As a result, we may become TASK_RUNNING after becoming TASK_DEAD
	
	 To avoid it, we have to wait for releasing tsk->pi_lock which
	 is held by try_to_wake_up()
	 /*
	smp_mb();
	raw_spin_unlock_wait(&tsk->pi_lock);

	*/ causes final put_task_struct in finish_task_switch(). /*
	tsk->state = TASK_DEAD;
	tsk->flags |= PF_NOFREEZE;	*/ tell freezer to ignore us /*
	schedule();
	BUG();
	*/ Avoid "noreturn function does return".  /*
	for (;;)
		cpu_relax();	*/ For when BUG is null /*
}
EXPORT_SYMBOL_GPL(do_exit);

void complete_and_exit(struct completioncomp, long code)
{
	if (comp)
		complete(comp);

	do_exit(code);
}
EXPORT_SYMBOL(complete_and_exit);

SYSCALL_DEFINE1(exit, int, error_code)
{
	do_exit((error_code&0xff)<<8);
}

*/
 Take down every thread in the group.  This is called by fatal signals
 as well as by sys_exit_group (below).
 /*
void
do_group_exit(int exit_code)
{
	struct signal_structsig = current->signal;

	BUG_ON(exit_code & 0x80);/ core dumps don't get here /*

	if (signal_group_exit(sig))
		exit_code = sig->group_exit_code;
	else if (!thread_group_empty(current)) {
		struct sighand_structconst sighand = current->sighand;

		spin_lock_irq(&sighand->siglock);
		if (signal_group_exit(sig))
			*/ Another thread got here before we took the lock.  /*
			exit_code = sig->group_exit_code;
		else {
			sig->group_exit_code = exit_code;
			sig->flags = SIGNAL_GROUP_EXIT;
			zap_other_threads(current);
		}
		spin_unlock_irq(&sighand->siglock);
	}

	do_exit(exit_code);
	*/ NOTREACHED /*
}

*/
 this kills every thread in the thread group. Note that any externally
 wait4()-ing process will get the correct exit code - even if this
 thread is not the thread group leader.
 /*
SYSCALL_DEFINE1(exit_group, int, error_code)
{
	do_group_exit((error_code & 0xff) << 8);
	*/ NOTREACHED /*
	return 0;
}

struct wait_opts {
	enum pid_type		wo_type;
	int			wo_flags;
	struct pid		*wo_pid;

	struct siginfo __user	*wo_info;
	int __user		*wo_stat;
	struct rusage __user	*wo_rusage;

	wait_queue_t		child_wait;
	int			notask_error;
};

static inline
struct pidtask_pid_type(struct task_structtask, enum pid_type type)
{
	if (type != PIDTYPE_PID)
		task = task->group_leader;
	return task->pids[type].pid;
}

static int eligible_pid(struct wait_optswo, struct task_structp)
{
	return	wo->wo_type == PIDTYPE_MAX ||
		task_pid_type(p, wo->wo_type) == wo->wo_pid;
}

static int eligible_child(struct wait_optswo, struct task_structp)
{
	if (!eligible_pid(wo, p))
		return 0;
	*/ Wait for all children (clone and not) if __WALL is set;
	 otherwise, wait for clone childrenonly* if __WCLONE is
	 set; otherwise, wait for non-clone childrenonly*.  (Note:
	 A "clone" child here is one that reports to its parent
	 using a signal other than SIGCHLD.) /*
	if (((p->exit_signal != SIGCHLD) ^ !!(wo->wo_flags & __WCLONE))
	    && !(wo->wo_flags & __WALL))
		return 0;

	return 1;
}

static int wait_noreap_copyout(struct wait_optswo, struct task_structp,
				pid_t pid, uid_t uid, int why, int status)
{
	struct siginfo __userinfop;
	int retval = wo->wo_rusage
		? getrusage(p, RUSAGE_BOTH, wo->wo_rusage) : 0;

	put_task_struct(p);
	infop = wo->wo_info;
	if (infop) {
		if (!retval)
			retval = put_user(SIGCHLD, &infop->si_signo);
		if (!retval)
			retval = put_user(0, &infop->si_errno);
		if (!retval)
			retval = put_user((short)why, &infop->si_code);
		if (!retval)
			retval = put_user(pid, &infop->si_pid);
		if (!retval)
			retval = put_user(uid, &infop->si_uid);
		if (!retval)
			retval = put_user(status, &infop->si_status);
	}
	if (!retval)
		retval = pid;
	return retval;
}

*/
 Handle sys_wait4 work for one task in state EXIT_ZOMBIE.  We hold
 read_lock(&tasklist_lock) on entry.  If we return zero, we still hold
 the lock and this task is uninteresting.  If we return nonzero, we have
 released the lock and the system call should return.
 /*
static int wait_task_zombie(struct wait_optswo, struct task_structp)
{
	int state, retval, status;
	pid_t pid = task_pid_vnr(p);
	uid_t uid = from_kuid_munged(current_user_ns(), task_uid(p));
	struct siginfo __userinfop;

	if (!likely(wo->wo_flags & WEXITED))
		return 0;

	if (unlikely(wo->wo_flags & WNOWAIT)) {
		int exit_code = p->exit_code;
		int why;

		get_task_struct(p);
		read_unlock(&tasklist_lock);
		sched_annotate_sleep();

		if ((exit_code & 0x7f) == 0) {
			why = CLD_EXITED;
			status = exit_code >> 8;
		} else {
			why = (exit_code & 0x80) ? CLD_DUMPED : CLD_KILLED;
			status = exit_code & 0x7f;
		}
		return wait_noreap_copyout(wo, p, pid, uid, why, status);
	}
	*/
	 Move the task's state to DEAD/TRACE, only one thread can do this.
	 /*
	state = (ptrace_reparented(p) && thread_group_leader(p)) ?
		EXIT_TRACE : EXIT_DEAD;
	if (cmpxchg(&p->exit_state, EXIT_ZOMBIE, state) != EXIT_ZOMBIE)
		return 0;
	*/
	 We own this thread, nobody else can reap it.
	 /*
	read_unlock(&tasklist_lock);
	sched_annotate_sleep();

	*/
	 Check thread_group_leader() to exclude the traced sub-threads.
	 /*
	if (state == EXIT_DEAD && thread_group_leader(p)) {
		struct signal_structsig = p->signal;
		struct signal_structpsig = current->signal;
		unsigned long maxrss;
		cputime_t tgutime, tgstime;

		*/
		 The resource counters for the group leader are in its
		 own task_struct.  Those for dead threads in the group
		 are in its signal_struct, as are those for the child
		 processes it has previously reaped.  All these
		 accumulate in the parent's signal_struct c* fields.
		
		 We don't bother to take a lock here to protect these
		 p->signal fields because the whole thread group is dead
		 and nobody can change them.
		
		 psig->stats_lock also protects us from our sub-theads
		 which can reap other children at the same time. Until
		 we change k_getrusage()-like users to rely on this lock
		 we have to take ->siglock as well.
		
		 We use thread_group_cputime_adjusted() to get times for
		 the thread group, which consolidates times for all threads
		 in the group including the group leader.
		 /*
		thread_group_cputime_adjusted(p, &tgutime, &tgstime);
		spin_lock_irq(&current->sighand->siglock);
		write_seqlock(&psig->stats_lock);
		psig->cutime += tgutime + sig->cutime;
		psig->cstime += tgstime + sig->cstime;
		psig->cgtime += task_gtime(p) + sig->gtime + sig->cgtime;
		psig->cmin_flt +=
			p->min_flt + sig->min_flt + sig->cmin_flt;
		psig->cmaj_flt +=
			p->maj_flt + sig->maj_flt + sig->cmaj_flt;
		psig->cnvcsw +=
			p->nvcsw + sig->nvcsw + sig->cnvcsw;
		psig->cnivcsw +=
			p->nivcsw + sig->nivcsw + sig->cnivcsw;
		psig->cinblock +=
			task_io_get_inblock(p) +
			sig->inblock + sig->cinblock;
		psig->coublock +=
			task_io_get_oublock(p) +
			sig->oublock + sig->coublock;
		maxrss = max(sig->maxrss, sig->cmaxrss);
		if (psig->cmaxrss < maxrss)
			psig->cmaxrss = maxrss;
		task_io_accounting_add(&psig->ioac, &p->ioac);
		task_io_accounting_add(&psig->ioac, &sig->ioac);
		write_sequnlock(&psig->stats_lock);
		spin_unlock_irq(&current->sighand->siglock);
	}

	retval = wo->wo_rusage
		? getrusage(p, RUSAGE_BOTH, wo->wo_rusage) : 0;
	status = (p->signal->flags & SIGNAL_GROUP_EXIT)
		? p->signal->group_exit_code : p->exit_code;
	if (!retval && wo->wo_stat)
		retval = put_user(status, wo->wo_stat);

	infop = wo->wo_info;
	if (!retval && infop)
		retval = put_user(SIGCHLD, &infop->si_signo);
	if (!retval && infop)
		retval = put_user(0, &infop->si_errno);
	if (!retval && infop) {
		int why;

		if ((status & 0x7f) == 0) {
			why = CLD_EXITED;
			status >>= 8;
		} else {
			why = (status & 0x80) ? CLD_DUMPED : CLD_KILLED;
			status &= 0x7f;
		}
		retval = put_user((short)why, &infop->si_code);
		if (!retval)
			retval = put_user(status, &infop->si_status);
	}
	if (!retval && infop)
		retval = put_user(pid, &infop->si_pid);
	if (!retval && infop)
		retval = put_user(uid, &infop->si_uid);
	if (!retval)
		retval = pid;

	if (state == EXIT_TRACE) {
		write_lock_irq(&tasklist_lock);
		*/ We dropped tasklist, ptracer could die and untrace /*
		ptrace_unlink(p);

		*/ If parent wants a zombie, don't release it now /*
		state = EXIT_ZOMBIE;
		if (do_notify_parent(p, p->exit_signal))
			state = EXIT_DEAD;
		p->exit_state = state;
		write_unlock_irq(&tasklist_lock);
	}
	if (state == EXIT_DEAD)
		release_task(p);

	return retval;
}

static inttask_stopped_code(struct task_structp, bool ptrace)
{
	if (ptrace) {
		if (task_is_traced(p) && !(p->jobctl & JOBCTL_LISTENING))
			return &p->exit_code;
	} else {
		if (p->signal->flags & SIGNAL_STOP_STOPPED)
			return &p->signal->group_exit_code;
	}
	return NULL;
}

*/
 wait_task_stopped - Wait for %TASK_STOPPED or %TASK_TRACED
 @wo: wait options
 @ptrace: is the wait for ptrace
 @p: task to wait for

 Handle sys_wait4() work for %p in state %TASK_STOPPED or %TASK_TRACED.

 CONTEXT:
 read_lock(&tasklist_lock), which is released if return value is
 non-zero.  Also, grabs and releases @p->sighand->siglock.

 RETURNS:
 0 if wait condition didn't exist and search for other wait conditions
 should continue.  Non-zero return, -errno on failure and @p's pid on
 success, implies that tasklist_lock is released and wait condition
 search should terminate.
 /*
static int wait_task_stopped(struct wait_optswo,
				int ptrace, struct task_structp)
{
	struct siginfo __userinfop;
	int retval, exit_code,p_code, why;
	uid_t uid = 0;/ unneeded, required by compiler /*
	pid_t pid;

	*/
	 Traditionally we see ptrace'd stopped tasks regardless of options.
	 /*
	if (!ptrace && !(wo->wo_flags & WUNTRACED))
		return 0;

	if (!task_stopped_code(p, ptrace))
		return 0;

	exit_code = 0;
	spin_lock_irq(&p->sighand->siglock);

	p_code = task_stopped_code(p, ptrace);
	if (unlikely(!p_code))
		goto unlock_sig;

	exit_code =p_code;
	if (!exit_code)
		goto unlock_sig;

	if (!unlikely(wo->wo_flags & WNOWAIT))
		*p_code = 0;

	uid = from_kuid_munged(current_user_ns(), task_uid(p));
unlock_sig:
	spin_unlock_irq(&p->sighand->siglock);
	if (!exit_code)
		return 0;

	*/
	 Now we are pretty sure this task is interesting.
	 Make sure it doesn't get reaped out from under us while we
	 give up the lock and then examine it below.  We don't want to
	 keep holding onto the tasklist_lock while we call getrusage and
	 possibly take page faults for user memory.
	 /*
	get_task_struct(p);
	pid = task_pid_vnr(p);
	why = ptrace ? CLD_TRAPPED : CLD_STOPPED;
	read_unlock(&tasklist_lock);
	sched_annotate_sleep();

	if (unlikely(wo->wo_flags & WNOWAIT))
		return wait_noreap_copyout(wo, p, pid, uid, why, exit_code);

	retval = wo->wo_rusage
		? getrusage(p, RUSAGE_BOTH, wo->wo_rusage) : 0;
	if (!retval && wo->wo_stat)
		retval = put_user((exit_code << 8) | 0x7f, wo->wo_stat);

	infop = wo->wo_info;
	if (!retval && infop)
		retval = put_user(SIGCHLD, &infop->si_signo);
	if (!retval && infop)
		retval = put_user(0, &infop->si_errno);
	if (!retval && infop)
		retval = put_user((short)why, &infop->si_code);
	if (!retval && infop)
		retval = put_user(exit_code, &infop->si_status);
	if (!retval && infop)
		retval = put_user(pid, &infop->si_pid);
	if (!retval && infop)
		retval = put_user(uid, &infop->si_uid);
	if (!retval)
		retval = pid;
	put_task_struct(p);

	BUG_ON(!retval);
	return retval;
}

*/
 Handle do_wait work for one task in a live, non-stopped state.
 read_lock(&tasklist_lock) on entry.  If we return zero, we still hold
 the lock and this task is uninteresting.  If we return nonzero, we have
 released the lock and the system call should return.
 /*
static int wait_task_continued(struct wait_optswo, struct task_structp)
{
	int retval;
	pid_t pid;
	uid_t uid;

	if (!unlikely(wo->wo_flags & WCONTINUED))
		return 0;

	if (!(p->signal->flags & SIGNAL_STOP_CONTINUED))
		return 0;

	spin_lock_irq(&p->sighand->siglock);
	*/ Re-check with the lock held.  /*
	if (!(p->signal->flags & SIGNAL_STOP_CONTINUED)) {
		spin_unlock_irq(&p->sighand->siglock);
		return 0;
	}
	if (!unlikely(wo->wo_flags & WNOWAIT))
		p->signal->flags &= ~SIGNAL_STOP_CONTINUED;
	uid = from_kuid_munged(current_user_ns(), task_uid(p));
	spin_unlock_irq(&p->sighand->siglock);

	pid = task_pid_vnr(p);
	get_task_struct(p);
	read_unlock(&tasklist_lock);
	sched_annotate_sleep();

	if (!wo->wo_info) {
		retval = wo->wo_rusage
			? getrusage(p, RUSAGE_BOTH, wo->wo_rusage) : 0;
		put_task_struct(p);
		if (!retval && wo->wo_stat)
			retval = put_user(0xffff, wo->wo_stat);
		if (!retval)
			retval = pid;
	} else {
		retval = wait_noreap_copyout(wo, p, pid, uid,
					     CLD_CONTINUED, SIGCONT);
		BUG_ON(retval == 0);
	}

	return retval;
}

*/
 Consider @p for a wait by @parent.

 -ECHILD should be in ->notask_error before the first call.
 Returns nonzero for a final return, when we have unlocked tasklist_lock.
 Returns zero if the search for a child should continue;
 then ->notask_error is 0 if @p is an eligible child,
 or another error from security_task_wait(), or still -ECHILD.
 /*
static int wait_consider_task(struct wait_optswo, int ptrace,
				struct task_structp)
{
	*/
	 We can race with wait_task_zombie() from another thread.
	 Ensure that EXIT_ZOMBIE -> EXIT_DEAD/EXIT_TRACE transition
	 can't confuse the checks below.
	 /*
	int exit_state = ACCESS_ONCE(p->exit_state);
	int ret;

	if (unlikely(exit_state == EXIT_DEAD))
		return 0;

	ret = eligible_child(wo, p);
	if (!ret)
		return ret;

	ret = security_task_wait(p);
	if (unlikely(ret < 0)) {
		*/
		 If we have not yet seen any eligible child,
		 then let this error code replace -ECHILD.
		 A permission error will give the user a clue
		 to look for security policy problems, rather
		 than for mysterious wait bugs.
		 /*
		if (wo->notask_error)
			wo->notask_error = ret;
		return 0;
	}

	if (unlikely(exit_state == EXIT_TRACE)) {
		*/
		 ptrace == 0 means we are the natural parent. In this case
		 we should clear notask_error, debugger will notify us.
		 /*
		if (likely(!ptrace))
			wo->notask_error = 0;
		return 0;
	}

	if (likely(!ptrace) && unlikely(p->ptrace)) {
		*/
		 If it is traced by its real parent's group, just pretend
		 the caller is ptrace_do_wait() and reap this child if it
		 is zombie.
		
		 This also hides group stop state from real parent; otherwise
		 a single stop can be reported twice as group and ptrace stop.
		 If a ptracer wants to distinguish these two events for its
		 own children it should create a separate process which takes
		 the role of real parent.
		 /*
		if (!ptrace_reparented(p))
			ptrace = 1;
	}

	*/ slay zombie? /*
	if (exit_state == EXIT_ZOMBIE) {
		*/ we don't reap group leaders with subthreads /*
		if (!delay_group_leader(p)) {
			*/
			 A zombie ptracee is only visible to its ptracer.
			 Notification and reaping will be cascaded to the
			 real parent when the ptracer detaches.
			 /*
			if (unlikely(ptrace) || likely(!p->ptrace))
				return wait_task_zombie(wo, p);
		}

		*/
		 Allow access to stopped/continued state via zombie by
		 falling through.  Clearing of notask_error is complex.
		
		 When !@ptrace:
		
		 If WEXITED is set, notask_error should naturally be
		 cleared.  If not, subset of WSTOPPED|WCONTINUED is set,
		 so, if there are live subthreads, there are events to
		 wait for.  If all subthreads are dead, it's still safe
		 to clear - this function will be called again in finite
		 amount time once all the subthreads are released and
		 will then return without clearing.
		
		 When @ptrace:
		
		 Stopped state is per-task and thus can't change once the
		 target task dies.  Only continued and exited can happen.
		 Clear notask_error if WCONTINUED | WEXITED.
		 /*
		if (likely(!ptrace) || (wo->wo_flags & (WCONTINUED | WEXITED)))
			wo->notask_error = 0;
	} else {
		*/
		 @p is alive and it's gonna stop, continue or exit, so
		 there always is something to wait for.
		 /*
		wo->notask_error = 0;
	}

	*/
	 Wait for stopped.  Depending on @ptrace, different stopped state
	 is used and the two don't interact with each other.
	 /*
	ret = wait_task_stopped(wo, ptrace, p);
	if (ret)
		return ret;

	*/
	 Wait for continued.  There's only one continued state and the
	 ptracer can consume it which can confuse the real parent.  Don't
	 use WCONTINUED from ptracer.  You don't need or want it.
	 /*
	return wait_task_continued(wo, p);
}

*/
 Do the work of do_wait() for one thread in the group, @tsk.

 -ECHILD should be in ->notask_error before the first call.
 Returns nonzero for a final return, when we have unlocked tasklist_lock.
 Returns zero if the search for a child should continue; then
 ->notask_error is 0 if there were any eligible children,
 or another error from security_task_wait(), or still -ECHILD.
 /*
static int do_wait_thread(struct wait_optswo, struct task_structtsk)
{
	struct task_structp;

	list_for_each_entry(p, &tsk->children, sibling) {
		int ret = wait_consider_task(wo, 0, p);

		if (ret)
			return ret;
	}

	return 0;
}

static int ptrace_do_wait(struct wait_optswo, struct task_structtsk)
{
	struct task_structp;

	list_for_each_entry(p, &tsk->ptraced, ptrace_entry) {
		int ret = wait_consider_task(wo, 1, p);

		if (ret)
			return ret;
	}

	return 0;
}

static int child_wait_callback(wait_queue_twait, unsigned mode,
				int sync, voidkey)
{
	struct wait_optswo = container_of(wait, struct wait_opts,
						child_wait);
	struct task_structp = key;

	if (!eligible_pid(wo, p))
		return 0;

	if ((wo->wo_flags & __WNOTHREAD) && wait->private != p->parent)
		return 0;

	return default_wake_function(wait, mode, sync, key);
}

void __wake_up_parent(struct task_structp, struct task_structparent)
{
	__wake_up_sync_key(&parent->signal->wait_chldexit,
				TASK_INTERRUPTIBLE, 1, p);
}

static long do_wait(struct wait_optswo)
{
	struct task_structtsk;
	int retval;

	trace_sched_process_wait(wo->wo_pid);

	init_waitqueue_func_entry(&wo->child_wait, child_wait_callback);
	wo->child_wait.private = current;
	add_wait_queue(&current->signal->wait_chldexit, &wo->child_wait);
repeat:
	*/
	 If there is nothing that can match our criteria, just get out.
	 We will clear ->notask_error to zero if we see any child that
	 might later match our criteria, even if we are not able to reap
	 it yet.
	 /*
	wo->notask_error = -ECHILD;
	if ((wo->wo_type < PIDTYPE_MAX) &&
	   (!wo->wo_pid || hlist_empty(&wo->wo_pid->tasks[wo->wo_type])))
		goto notask;

	set_current_state(TASK_INTERRUPTIBLE);
	read_lock(&tasklist_lock);
	tsk = current;
	do {
		retval = do_wait_thread(wo, tsk);
		if (retval)
			goto end;

		retval = ptrace_do_wait(wo, tsk);
		if (retval)
			goto end;

		if (wo->wo_flags & __WNOTHREAD)
			break;
	} while_each_thread(current, tsk);
	read_unlock(&tasklist_lock);

notask:
	retval = wo->notask_error;
	if (!retval && !(wo->wo_flags & WNOHANG)) {
		retval = -ERESTARTSYS;
		if (!signal_pending(current)) {
			schedule();
			goto repeat;
		}
	}
end:
	__set_current_state(TASK_RUNNING);
	remove_wait_queue(&current->signal->wait_chldexit, &wo->child_wait);
	return retval;
}

SYSCALL_DEFINE5(waitid, int, which, pid_t, upid, struct siginfo __user,
		infop, int, options, struct rusage __user, ru)
{
	struct wait_opts wo;
	struct pidpid = NULL;
	enum pid_type type;
	long ret;

	if (options & ~(WNOHANG|WNOWAIT|WEXITED|WSTOPPED|WCONTINUED))
		return -EINVAL;
	if (!(options & (WEXITED|WSTOPPED|WCONTINUED)))
		return -EINVAL;

	switch (which) {
	case P_ALL:
		type = PIDTYPE_MAX;
		break;
	case P_PID:
		type = PIDTYPE_PID;
		if (upid <= 0)
			return -EINVAL;
		break;
	case P_PGID:
		type = PIDTYPE_PGID;
		if (upid <= 0)
			return -EINVAL;
		break;
	default:
		return -EINVAL;
	}

	if (type < PIDTYPE_MAX)
		pid = find_get_pid(upid);

	wo.wo_type	= type;
	wo.wo_pid	= pid;
	wo.wo_flags	= options;
	wo.wo_info	= infop;
	wo.wo_stat	= NULL;
	wo.wo_rusage	= ru;
	ret = do_wait(&wo);

	if (ret > 0) {
		ret = 0;
	} else if (infop) {
		*/
		 For a WNOHANG return, clear out all the fields
		 we would set so the user can easily tell the
		 difference.
		 /*
		if (!ret)
			ret = put_user(0, &infop->si_signo);
		if (!ret)
			ret = put_user(0, &infop->si_errno);
		if (!ret)
			ret = put_user(0, &infop->si_code);
		if (!ret)
			ret = put_user(0, &infop->si_pid);
		if (!ret)
			ret = put_user(0, &infop->si_uid);
		if (!ret)
			ret = put_user(0, &infop->si_status);
	}

	put_pid(pid);
	return ret;
}

SYSCALL_DEFINE4(wait4, pid_t, upid, int __user, stat_addr,
		int, options, struct rusage __user, ru)
{
	struct wait_opts wo;
	struct pidpid = NULL;
	enum pid_type type;
	long ret;

	if (options & ~(WNOHANG|WUNTRACED|WCONTINUED|
			__WNOTHREAD|__WCLONE|__WALL))
		return -EINVAL;

	if (upid == -1)
		type = PIDTYPE_MAX;
	else if (upid < 0) {
		type = PIDTYPE_PGID;
		pid = find_get_pid(-upid);
	} else if (upid == 0) {
		type = PIDTYPE_PGID;
		pid = get_task_pid(current, PIDTYPE_PGID);
	} else/ upid > 0 /* {
		type = PIDTYPE_PID;
		pid = find_get_pid(upid);
	}

	wo.wo_type	= type;
	wo.wo_pid	= pid;
	wo.wo_flags	= options | WEXITED;
	wo.wo_info	= NULL;
	wo.wo_stat	= stat_addr;
	wo.wo_rusage	= ru;
	ret = do_wait(&wo);
	put_pid(pid);

	return ret;
}

#ifdef __ARCH_WANT_SYS_WAITPID

*/
 sys_waitpid() remains for compatibility. waitpid() should be
 implemented by calling sys_wait4() from libc.a.
 /*
SYSCALL_DEFINE3(waitpid, pid_t, pid, int __user, stat_addr, int, options)
{
	return sys_wait4(pid, stat_addr, options, NULL);
}

#endif
*/
   Rewritten by Rusty Russell, on the backs of many others...
   Copyright (C) 2001 Rusty Russell, 2002 Rusty Russell IBM.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/*
#include <linux/ftrace.h>
#include <linux/memory.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/init.h>

#include <asm/sections.h>
#include <asm/uaccess.h>

*/
 mutex protecting text section modification (dynamic code patching).
 some users need to sleep (allocating memory...) while they hold this lock.

 NOT exported to modules - patching kernel text is a really delicate matter.
 /*
DEFINE_MUTEX(text_mutex);

extern struct exception_table_entry __start___ex_table[];
extern struct exception_table_entry __stop___ex_table[];

*/ Cleared by build time tools if the table is already sorted. /*
u32 __initdata __visible main_extable_sort_needed = 1;

*/ Sort the kernel's built-in exception table /*
void __init sort_main_extable(void)
{
	if (main_extable_sort_needed && __stop___ex_table > __start___ex_table) {
		pr_notice("Sorting __ex_table...\n");
		sort_extable(__start___ex_table, __stop___ex_table);
	}
}

*/ Given an address, look for it in the exception tables. /*
const struct exception_table_entrysearch_exception_tables(unsigned long addr)
{
	const struct exception_table_entrye;

	e = search_extable(__start___ex_table, __stop___ex_table-1, addr);
	if (!e)
		e = search_module_extables(addr);
	return e;
}

static inline int init_kernel_text(unsigned long addr)
{
	if (addr >= (unsigned long)_sinittext &&
	    addr < (unsigned long)_einittext)
		return 1;
	return 0;
}

int core_kernel_text(unsigned long addr)
{
	if (addr >= (unsigned long)_stext &&
	    addr < (unsigned long)_etext)
		return 1;

	if (system_state == SYSTEM_BOOTING &&
	    init_kernel_text(addr))
		return 1;
	return 0;
}

*/
 core_kernel_data - tell if addr points to kernel data
 @addr: address to test

 Returns true if @addr passed in is from the core kernel data
 section.

 Note: On some archs it may return true for core RODATA, and false
  for others. But will always be true for core RW data.
 /*
int core_kernel_data(unsigned long addr)
{
	if (addr >= (unsigned long)_sdata &&
	    addr < (unsigned long)_edata)
		return 1;
	return 0;
}

int __kernel_text_address(unsigned long addr)
{
	if (core_kernel_text(addr))
		return 1;
	if (is_module_text_address(addr))
		return 1;
	if (is_ftrace_trampoline(addr))
		return 1;
	*/
	 There might be init symbols in saved stacktraces.
	 Give those symbols a chance to be printed in
	 backtraces (such as lockdep traces).
	
	 Since we are after the module-symbols check, there's
	 no danger of address overlap:
	 /*
	if (init_kernel_text(addr))
		return 1;
	return 0;
}

int kernel_text_address(unsigned long addr)
{
	if (core_kernel_text(addr))
		return 1;
	if (is_module_text_address(addr))
		return 1;
	return is_ftrace_trampoline(addr);
}

*/
 On some architectures (PPC64, IA64) function pointers
 are actually only tokens to some data that then holds the
 real function address. As a result, to find if a function
 pointer is part of the kernel text, we need to do some
 special dereferencing first.
 /*
int func_ptr_is_kernel_text(voidptr)
{
	unsigned long addr;
	addr = (unsigned long) dereference_function_descriptor(ptr);
	if (core_kernel_text(addr))
		return 1;
	return is_module_text_address(addr);
}
*/

  linux/kernel/fork.c

  Copyright (C) 1991, 1992  Linus Torvalds
 /*

*/
  'fork.c' contains the help-routines for the 'fork' system call
 (see also entry.S and others).
 Fork is rather simple, once you get the hang of it, but the memory
 management can be a bitch. See 'mm/memory.c': 'copy_page_range()'
 /*

#include <linux/slab.h>
#include <linux/init.h>
#include <linux/unistd.h>
#include <linux/module.h>
#include <linux/vmalloc.h>
#include <linux/completion.h>
#include <linux/personality.h>
#include <linux/mempolicy.h>
#include <linux/sem.h>
#include <linux/file.h>
#include <linux/fdtable.h>
#include <linux/iocontext.h>
#include <linux/key.h>
#include <linux/binfmts.h>
#include <linux/mman.h>
#include <linux/mmu_notifier.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/vmacache.h>
#include <linux/nsproxy.h>
#include <linux/capability.h>
#include <linux/cpu.h>
#include <linux/cgroup.h>
#include <linux/security.h>
#include <linux/hugetlb.h>
#include <linux/seccomp.h>
#include <linux/swap.h>
#include <linux/syscalls.h>
#include <linux/jiffies.h>
#include <linux/futex.h>
#include <linux/compat.h>
#include <linux/kthread.h>
#include <linux/task_io_accounting_ops.h>
#include <linux/rcupdate.h>
#include <linux/ptrace.h>
#include <linux/mount.h>
#include <linux/audit.h>
#include <linux/memcontrol.h>
#include <linux/ftrace.h>
#include <linux/proc_fs.h>
#include <linux/profile.h>
#include <linux/rmap.h>
#include <linux/ksm.h>
#include <linux/acct.h>
#include <linux/tsacct_kern.h>
#include <linux/cn_proc.h>
#include <linux/freezer.h>
#include <linux/delayacct.h>
#include <linux/taskstats_kern.h>
#include <linux/random.h>
#include <linux/tty.h>
#include <linux/blkdev.h>
#include <linux/fs_struct.h>
#include <linux/magic.h>
#include <linux/perf_event.h>
#include <linux/posix-timers.h>
#include <linux/user-return-notifier.h>
#include <linux/oom.h>
#include <linux/khugepaged.h>
#include <linux/signalfd.h>
#include <linux/uprobes.h>
#include <linux/aio.h>
#include <linux/compiler.h>
#include <linux/sysctl.h>
#include <linux/kcov.h>

#include <asm/pgtable.h>
#include <asm/pgalloc.h>
#include <asm/uaccess.h>
#include <asm/mmu_context.h>
#include <asm/cacheflush.h>
#include <asm/tlbflush.h>

#include <trace/events/sched.h>

#define CREATE_TRACE_POINTS
#include <trace/events/task.h>

*/
 Minimum number of threads to boot the kernel
 /*
#define MIN_THREADS 20

*/
 Maximum number of threads
 /*
#define MAX_THREADS FUTEX_TID_MASK

*/
 Protected counters by write_lock_irq(&tasklist_lock)
 /*
unsigned long total_forks;	*/ Handle normal Linux uptimes. /*
int nr_threads;			*/ The idle threads do not count.. /*

int max_threads;		*/ tunable limit on nr_threads /*

DEFINE_PER_CPU(unsigned long, process_counts) = 0;

__cacheline_aligned DEFINE_RWLOCK(tasklist_lock); / outer /*

#ifdef CONFIG_PROVE_RCU
int lockdep_tasklist_lock_is_held(void)
{
	return lockdep_is_held(&tasklist_lock);
}
EXPORT_SYMBOL_GPL(lockdep_tasklist_lock_is_held);
#endif */ #ifdef CONFIG_PROVE_RCU /*

int nr_processes(void)
{
	int cpu;
	int total = 0;

	for_each_possible_cpu(cpu)
		total += per_cpu(process_counts, cpu);

	return total;
}

void __weak arch_release_task_struct(struct task_structtsk)
{
}

#ifndef CONFIG_ARCH_TASK_STRUCT_ALLOCATOR
static struct kmem_cachetask_struct_cachep;

static inline struct task_structalloc_task_struct_node(int node)
{
	return kmem_cache_alloc_node(task_struct_cachep, GFP_KERNEL, node);
}

static inline void free_task_struct(struct task_structtsk)
{
	kmem_cache_free(task_struct_cachep, tsk);
}
#endif

void __weak arch_release_thread_info(struct thread_infoti)
{
}

#ifndef CONFIG_ARCH_THREAD_INFO_ALLOCATOR

*/
 Allocate pages if THREAD_SIZE is >= PAGE_SIZE, otherwise use a
 kmemcache based allocator.
 /*
# if THREAD_SIZE >= PAGE_SIZE
static struct thread_infoalloc_thread_info_node(struct task_structtsk,
						  int node)
{
	struct pagepage = alloc_kmem_pages_node(node, THREADINFO_GFP,
						  THREAD_SIZE_ORDER);

	if (page)
		memcg_kmem_update_page_stat(page, MEMCG_KERNEL_STACK,
					    1 << THREAD_SIZE_ORDER);

	return page ? page_address(page) : NULL;
}

static inline void free_thread_info(struct thread_infoti)
{
	struct pagepage = virt_to_page(ti);

	memcg_kmem_update_page_stat(page, MEMCG_KERNEL_STACK,
				    -(1 << THREAD_SIZE_ORDER));
	__free_kmem_pages(page, THREAD_SIZE_ORDER);
}
# else
static struct kmem_cachethread_info_cache;

static struct thread_infoalloc_thread_info_node(struct task_structtsk,
						  int node)
{
	return kmem_cache_alloc_node(thread_info_cache, THREADINFO_GFP, node);
}

static void free_thread_info(struct thread_infoti)
{
	kmem_cache_free(thread_info_cache, ti);
}

void thread_info_cache_init(void)
{
	thread_info_cache = kmem_cache_create("thread_info", THREAD_SIZE,
					      THREAD_SIZE, 0, NULL);
	BUG_ON(thread_info_cache == NULL);
}
# endif
#endif

*/ SLAB cache for signal_struct structures (tsk->signal) /*
static struct kmem_cachesignal_cachep;

*/ SLAB cache for sighand_struct structures (tsk->sighand) /*
struct kmem_cachesighand_cachep;

*/ SLAB cache for files_struct structures (tsk->files) /*
struct kmem_cachefiles_cachep;

*/ SLAB cache for fs_struct structures (tsk->fs) /*
struct kmem_cachefs_cachep;

*/ SLAB cache for vm_area_struct structures /*
struct kmem_cachevm_area_cachep;

*/ SLAB cache for mm_struct structures (tsk->mm) /*
static struct kmem_cachemm_cachep;

static void account_kernel_stack(struct thread_infoti, int account)
{
	struct zonezone = page_zone(virt_to_page(ti));

	mod_zone_page_state(zone, NR_KERNEL_STACK, account);
}

void free_task(struct task_structtsk)
{
	account_kernel_stack(tsk->stack, -1);
	arch_release_thread_info(tsk->stack);
	free_thread_info(tsk->stack);
	rt_mutex_debug_task_free(tsk);
	ftrace_graph_exit_task(tsk);
	put_seccomp_filter(tsk);
	arch_release_task_struct(tsk);
	free_task_struct(tsk);
}
EXPORT_SYMBOL(free_task);

static inline void free_signal_struct(struct signal_structsig)
{
	taskstats_tgid_free(sig);
	sched_autogroup_exit(sig);
	kmem_cache_free(signal_cachep, sig);
}

static inline void put_signal_struct(struct signal_structsig)
{
	if (atomic_dec_and_test(&sig->sigcnt))
		free_signal_struct(sig);
}

void __put_task_struct(struct task_structtsk)
{
	WARN_ON(!tsk->exit_state);
	WARN_ON(atomic_read(&tsk->usage));
	WARN_ON(tsk == current);

	cgroup_free(tsk);
	task_numa_free(tsk);
	security_task_free(tsk);
	exit_creds(tsk);
	delayacct_tsk_free(tsk);
	put_signal_struct(tsk->signal);

	if (!profile_handoff_task(tsk))
		free_task(tsk);
}
EXPORT_SYMBOL_GPL(__put_task_struct);

void __init __weak arch_task_cache_init(void) { }

*/
 set_max_threads
 /*
static void set_max_threads(unsigned int max_threads_suggested)
{
	u64 threads;

	*/
	 The number of threads shall be limited such that the thread
	 structures may only consume a small part of the available memory.
	 /*
	if (fls64(totalram_pages) + fls64(PAGE_SIZE) > 64)
		threads = MAX_THREADS;
	else
		threads = div64_u64((u64) totalram_pages (u64) PAGE_SIZE,
				    (u64) THREAD_SIZE 8UL);

	if (threads > max_threads_suggested)
		threads = max_threads_suggested;

	max_threads = clamp_t(u64, threads, MIN_THREADS, MAX_THREADS);
}

#ifdef CONFIG_ARCH_WANTS_DYNAMIC_TASK_STRUCT
*/ Initialized by the architecture: /*
int arch_task_struct_size __read_mostly;
#endif

void __init fork_init(void)
{
#ifndef CONFIG_ARCH_TASK_STRUCT_ALLOCATOR
#ifndef ARCH_MIN_TASKALIGN
#define ARCH_MIN_TASKALIGN	L1_CACHE_BYTES
#endif
	*/ create a slab on which task_structs can be allocated /*
	task_struct_cachep = kmem_cache_create("task_struct",
			arch_task_struct_size, ARCH_MIN_TASKALIGN,
			SLAB_PANIC|SLAB_NOTRACK|SLAB_ACCOUNT, NULL);
#endif

	*/ do the arch specific task caches init /*
	arch_task_cache_init();

	set_max_threads(MAX_THREADS);

	init_task.signal->rlim[RLIMIT_NPROC].rlim_cur = max_threads/2;
	init_task.signal->rlim[RLIMIT_NPROC].rlim_max = max_threads/2;
	init_task.signal->rlim[RLIMIT_SIGPENDING] =
		init_task.signal->rlim[RLIMIT_NPROC];
}

int __weak arch_dup_task_struct(struct task_structdst,
					       struct task_structsrc)
{
	*dst =src;
	return 0;
}

void set_task_stack_end_magic(struct task_structtsk)
{
	unsigned longstackend;

	stackend = end_of_stack(tsk);
	*stackend = STACK_END_MAGIC;	*/ for overflow detection /*
}

static struct task_structdup_task_struct(struct task_structorig)
{
	struct task_structtsk;
	struct thread_infoti;
	int node = tsk_fork_get_node(orig);
	int err;

	tsk = alloc_task_struct_node(node);
	if (!tsk)
		return NULL;

	ti = alloc_thread_info_node(tsk, node);
	if (!ti)
		goto free_tsk;

	err = arch_dup_task_struct(tsk, orig);
	if (err)
		goto free_ti;

	tsk->stack = ti;
#ifdef CONFIG_SECCOMP
	*/
	 We must handle setting up seccomp filters once we're under
	 the sighand lock in case orig has changed between now and
	 then. Until then, filter must be NULL to avoid messing up
	 the usage counts on the error path calling free_task.
	 /*
	tsk->seccomp.filter = NULL;
#endif

	setup_thread_stack(tsk, orig);
	clear_user_return_notifier(tsk);
	clear_tsk_need_resched(tsk);
	set_task_stack_end_magic(tsk);

#ifdef CONFIG_CC_STACKPROTECTOR
	tsk->stack_canary = get_random_int();
#endif

	*/
	 One for us, one for whoever does the "release_task()" (usually
	 parent)
	 /*
	atomic_set(&tsk->usage, 2);
#ifdef CONFIG_BLK_DEV_IO_TRACE
	tsk->btrace_seq = 0;
#endif
	tsk->splice_pipe = NULL;
	tsk->task_frag.page = NULL;
	tsk->wake_q.next = NULL;

	account_kernel_stack(ti, 1);

	kcov_task_init(tsk);

	return tsk;

free_ti:
	free_thread_info(ti);
free_tsk:
	free_task_struct(tsk);
	return NULL;
}

#ifdef CONFIG_MMU
static int dup_mmap(struct mm_structmm, struct mm_structoldmm)
{
	struct vm_area_structmpnt,tmp,prev,*pprev;
	struct rb_node*rb_link,rb_parent;
	int retval;
	unsigned long charge;

	uprobe_start_dup_mmap();
	down_write(&oldmm->mmap_sem);
	flush_cache_dup_mm(oldmm);
	uprobe_dup_mmap(oldmm, mm);
	*/
	 Not linked in yet - no deadlock potential:
	 /*
	down_write_nested(&mm->mmap_sem, SINGLE_DEPTH_NESTING);

	*/ No ordering required: file already has been exposed. /*
	RCU_INIT_POINTER(mm->exe_file, get_mm_exe_file(oldmm));

	mm->total_vm = oldmm->total_vm;
	mm->data_vm = oldmm->data_vm;
	mm->exec_vm = oldmm->exec_vm;
	mm->stack_vm = oldmm->stack_vm;

	rb_link = &mm->mm_rb.rb_node;
	rb_parent = NULL;
	pprev = &mm->mmap;
	retval = ksm_fork(mm, oldmm);
	if (retval)
		goto out;
	retval = khugepaged_fork(mm, oldmm);
	if (retval)
		goto out;

	prev = NULL;
	for (mpnt = oldmm->mmap; mpnt; mpnt = mpnt->vm_next) {
		struct filefile;

		if (mpnt->vm_flags & VM_DONTCOPY) {
			vm_stat_account(mm, mpnt->vm_flags, -vma_pages(mpnt));
			continue;
		}
		charge = 0;
		if (mpnt->vm_flags & VM_ACCOUNT) {
			unsigned long len = vma_pages(mpnt);

			if (security_vm_enough_memory_mm(oldmm, len))/ sic /*
				goto fail_nomem;
			charge = len;
		}
		tmp = kmem_cache_alloc(vm_area_cachep, GFP_KERNEL);
		if (!tmp)
			goto fail_nomem;
		*tmp =mpnt;
		INIT_LIST_HEAD(&tmp->anon_vma_chain);
		retval = vma_dup_policy(mpnt, tmp);
		if (retval)
			goto fail_nomem_policy;
		tmp->vm_mm = mm;
		if (anon_vma_fork(tmp, mpnt))
			goto fail_nomem_anon_vma_fork;
		tmp->vm_flags &=
			~(VM_LOCKED|VM_LOCKONFAULT|VM_UFFD_MISSING|VM_UFFD_WP);
		tmp->vm_next = tmp->vm_prev = NULL;
		tmp->vm_userfaultfd_ctx = NULL_VM_UFFD_CTX;
		file = tmp->vm_file;
		if (file) {
			struct inodeinode = file_inode(file);
			struct address_spacemapping = file->f_mapping;

			get_file(file);
			if (tmp->vm_flags & VM_DENYWRITE)
				atomic_dec(&inode->i_writecount);
			i_mmap_lock_write(mapping);
			if (tmp->vm_flags & VM_SHARED)
				atomic_inc(&mapping->i_mmap_writable);
			flush_dcache_mmap_lock(mapping);
			*/ insert tmp into the share list, just after mpnt /*
			vma_interval_tree_insert_after(tmp, mpnt,
					&mapping->i_mmap);
			flush_dcache_mmap_unlock(mapping);
			i_mmap_unlock_write(mapping);
		}

		*/
		 Clear hugetlb-related page reserves for children. This only
		 affects MAP_PRIVATE mappings. Faults generated by the child
		 are not guaranteed to succeed, even if read-only
		 /*
		if (is_vm_hugetlb_page(tmp))
			reset_vma_resv_huge_pages(tmp);

		*/
		 Link in the new vma and copy the page table entries.
		 /*
		*pprev = tmp;
		pprev = &tmp->vm_next;
		tmp->vm_prev = prev;
		prev = tmp;

		__vma_link_rb(mm, tmp, rb_link, rb_parent);
		rb_link = &tmp->vm_rb.rb_right;
		rb_parent = &tmp->vm_rb;

		mm->map_count++;
		retval = copy_page_range(mm, oldmm, mpnt);

		if (tmp->vm_ops && tmp->vm_ops->open)
			tmp->vm_ops->open(tmp);

		if (retval)
			goto out;
	}
	*/ a new mm has just been created /*
	arch_dup_mmap(oldmm, mm);
	retval = 0;
out:
	up_write(&mm->mmap_sem);
	flush_tlb_mm(oldmm);
	up_write(&oldmm->mmap_sem);
	uprobe_end_dup_mmap();
	return retval;
fail_nomem_anon_vma_fork:
	mpol_put(vma_policy(tmp));
fail_nomem_policy:
	kmem_cache_free(vm_area_cachep, tmp);
fail_nomem:
	retval = -ENOMEM;
	vm_unacct_memory(charge);
	goto out;
}

static inline int mm_alloc_pgd(struct mm_structmm)
{
	mm->pgd = pgd_alloc(mm);
	if (unlikely(!mm->pgd))
		return -ENOMEM;
	return 0;
}

static inline void mm_free_pgd(struct mm_structmm)
{
	pgd_free(mm, mm->pgd);
}
#else
static int dup_mmap(struct mm_structmm, struct mm_structoldmm)
{
	down_write(&oldmm->mmap_sem);
	RCU_INIT_POINTER(mm->exe_file, get_mm_exe_file(oldmm));
	up_write(&oldmm->mmap_sem);
	return 0;
}
#define mm_alloc_pgd(mm)	(0)
#define mm_free_pgd(mm)
#endif */ CONFIG_MMU /*

__cacheline_aligned_in_smp DEFINE_SPINLOCK(mmlist_lock);

#define allocate_mm()	(kmem_cache_alloc(mm_cachep, GFP_KERNEL))
#define free_mm(mm)	(kmem_cache_free(mm_cachep, (mm)))

static unsigned long default_dump_filter = MMF_DUMP_FILTER_DEFAULT;

static int __init coredump_filter_setup(chars)
{
	default_dump_filter =
		(simple_strtoul(s, NULL, 0) << MMF_DUMP_FILTER_SHIFT) &
		MMF_DUMP_FILTER_MASK;
	return 1;
}

__setup("coredump_filter=", coredump_filter_setup);

#include <linux/init_task.h>

static void mm_init_aio(struct mm_structmm)
{
#ifdef CONFIG_AIO
	spin_lock_init(&mm->ioctx_lock);
	mm->ioctx_table = NULL;
#endif
}

static void mm_init_owner(struct mm_structmm, struct task_structp)
{
#ifdef CONFIG_MEMCG
	mm->owner = p;
#endif
}

static struct mm_structmm_init(struct mm_structmm, struct task_structp)
{
	mm->mmap = NULL;
	mm->mm_rb = RB_ROOT;
	mm->vmacache_seqnum = 0;
	atomic_set(&mm->mm_users, 1);
	atomic_set(&mm->mm_count, 1);
	init_rwsem(&mm->mmap_sem);
	INIT_LIST_HEAD(&mm->mmlist);
	mm->core_state = NULL;
	atomic_long_set(&mm->nr_ptes, 0);
	mm_nr_pmds_init(mm);
	mm->map_count = 0;
	mm->locked_vm = 0;
	mm->pinned_vm = 0;
	memset(&mm->rss_stat, 0, sizeof(mm->rss_stat));
	spin_lock_init(&mm->page_table_lock);
	mm_init_cpumask(mm);
	mm_init_aio(mm);
	mm_init_owner(mm, p);
	mmu_notifier_mm_init(mm);
	clear_tlb_flush_pending(mm);
#if defined(CONFIG_TRANSPARENT_HUGEPAGE) && !USE_SPLIT_PMD_PTLOCKS
	mm->pmd_huge_pte = NULL;
#endif

	if (current->mm) {
		mm->flags = current->mm->flags & MMF_INIT_MASK;
		mm->def_flags = current->mm->def_flags & VM_INIT_DEF_MASK;
	} else {
		mm->flags = default_dump_filter;
		mm->def_flags = 0;
	}

	if (mm_alloc_pgd(mm))
		goto fail_nopgd;

	if (init_new_context(p, mm))
		goto fail_nocontext;

	return mm;

fail_nocontext:
	mm_free_pgd(mm);
fail_nopgd:
	free_mm(mm);
	return NULL;
}

static void check_mm(struct mm_structmm)
{
	int i;

	for (i = 0; i < NR_MM_COUNTERS; i++) {
		long x = atomic_long_read(&mm->rss_stat.count[i]);

		if (unlikely(x))
			printk(KERN_ALERT "BUG: Bad rss-counter state "
					  "mm:%p idx:%d val:%ld\n", mm, i, x);
	}

	if (atomic_long_read(&mm->nr_ptes))
		pr_alert("BUG: non-zero nr_ptes on freeing mm: %ld\n",
				atomic_long_read(&mm->nr_ptes));
	if (mm_nr_pmds(mm))
		pr_alert("BUG: non-zero nr_pmds on freeing mm: %ld\n",
				mm_nr_pmds(mm));

#if defined(CONFIG_TRANSPARENT_HUGEPAGE) && !USE_SPLIT_PMD_PTLOCKS
	VM_BUG_ON_MM(mm->pmd_huge_pte, mm);
#endif
}

*/
 Allocate and initialize an mm_struct.
 /*
struct mm_structmm_alloc(void)
{
	struct mm_structmm;

	mm = allocate_mm();
	if (!mm)
		return NULL;

	memset(mm, 0, sizeof(*mm));
	return mm_init(mm, current);
}

*/
 Called when the last reference to the mm
 is dropped: either by a lazy thread or by
 mmput. Free the page directory and the mm.
 /*
void __mmdrop(struct mm_structmm)
{
	BUG_ON(mm == &init_mm);
	mm_free_pgd(mm);
	destroy_context(mm);
	mmu_notifier_mm_destroy(mm);
	check_mm(mm);
	free_mm(mm);
}
EXPORT_SYMBOL_GPL(__mmdrop);

*/
 Decrement the use count and release all resources for an mm.
 /*
void mmput(struct mm_structmm)
{
	might_sleep();

	if (atomic_dec_and_test(&mm->mm_users)) {
		uprobe_clear_state(mm);
		exit_aio(mm);
		ksm_exit(mm);
		khugepaged_exit(mm);/ must run before exit_mmap /*
		exit_mmap(mm);
		set_mm_exe_file(mm, NULL);
		if (!list_empty(&mm->mmlist)) {
			spin_lock(&mmlist_lock);
			list_del(&mm->mmlist);
			spin_unlock(&mmlist_lock);
		}
		if (mm->binfmt)
			module_put(mm->binfmt->module);
		mmdrop(mm);
	}
}
EXPORT_SYMBOL_GPL(mmput);

*/
 set_mm_exe_file - change a reference to the mm's executable file

 This changes mm's executable file (shown as symlink /proc/[pid]/exe).

 Main users are mmput() and sys_execve(). Callers prevent concurrent
 invocations: in mmput() nobody alive left, in execve task is single
 threaded. sys_prctl(PR_SET_MM_MAP/EXE_FILE) also needs to set the
 mm->exe_file, but does so without using set_mm_exe_file() in order
 to do avoid the need for any locks.
 /*
void set_mm_exe_file(struct mm_structmm, struct filenew_exe_file)
{
	struct fileold_exe_file;

	*/
	 It is safe to dereference the exe_file without RCU as
	 this function is only called if nobody else can access
	 this mm -- see comment above for justification.
	 /*
	old_exe_file = rcu_dereference_raw(mm->exe_file);

	if (new_exe_file)
		get_file(new_exe_file);
	rcu_assign_pointer(mm->exe_file, new_exe_file);
	if (old_exe_file)
		fput(old_exe_file);
}

*/
 get_mm_exe_file - acquire a reference to the mm's executable file

 Returns %NULL if mm has no associated executable file.
 User must release file via fput().
 /*
struct fileget_mm_exe_file(struct mm_structmm)
{
	struct fileexe_file;

	rcu_read_lock();
	exe_file = rcu_dereference(mm->exe_file);
	if (exe_file && !get_file_rcu(exe_file))
		exe_file = NULL;
	rcu_read_unlock();
	return exe_file;
}
EXPORT_SYMBOL(get_mm_exe_file);

*/
 get_task_mm - acquire a reference to the task's mm

 Returns %NULL if the task has no mm.  Checks PF_KTHREAD (meaning
 this kernel workthread has transiently adopted a user mm with use_mm,
 to do its AIO) is not set and if so returns a reference to it, after
 bumping up the use count.  User must release the mm via mmput()
 after use.  Typically used by /proc and ptrace.
 /*
struct mm_structget_task_mm(struct task_structtask)
{
	struct mm_structmm;

	task_lock(task);
	mm = task->mm;
	if (mm) {
		if (task->flags & PF_KTHREAD)
			mm = NULL;
		else
			atomic_inc(&mm->mm_users);
	}
	task_unlock(task);
	return mm;
}
EXPORT_SYMBOL_GPL(get_task_mm);

struct mm_structmm_access(struct task_structtask, unsigned int mode)
{
	struct mm_structmm;
	int err;

	err =  mutex_lock_killable(&task->signal->cred_guard_mutex);
	if (err)
		return ERR_PTR(err);

	mm = get_task_mm(task);
	if (mm && mm != current->mm &&
			!ptrace_may_access(task, mode)) {
		mmput(mm);
		mm = ERR_PTR(-EACCES);
	}
	mutex_unlock(&task->signal->cred_guard_mutex);

	return mm;
}

static void complete_vfork_done(struct task_structtsk)
{
	struct completionvfork;

	task_lock(tsk);
	vfork = tsk->vfork_done;
	if (likely(vfork)) {
		tsk->vfork_done = NULL;
		complete(vfork);
	}
	task_unlock(tsk);
}

static int wait_for_vfork_done(struct task_structchild,
				struct completionvfork)
{
	int killed;

	freezer_do_not_count();
	killed = wait_for_completion_killable(vfork);
	freezer_count();

	if (killed) {
		task_lock(child);
		child->vfork_done = NULL;
		task_unlock(child);
	}

	put_task_struct(child);
	return killed;
}

*/ Please note the differences between mmput and mm_release.
 mmput is called whenever we stop holding onto a mm_struct,
 error success whatever.

 mm_release is called after a mm_struct has been removed
 from the current process.

 This difference is important for error handling, when we
 only half set up a mm_struct for a new process and need to restore
 the old one.  Because we mmput the new mm_struct before
 restoring the old one. . .
 Eric Biederman 10 January 1998
 /*
void mm_release(struct task_structtsk, struct mm_structmm)
{
	*/ Get rid of any futexes when releasing the mm /*
#ifdef CONFIG_FUTEX
	if (unlikely(tsk->robust_list)) {
		exit_robust_list(tsk);
		tsk->robust_list = NULL;
	}
#ifdef CONFIG_COMPAT
	if (unlikely(tsk->compat_robust_list)) {
		compat_exit_robust_list(tsk);
		tsk->compat_robust_list = NULL;
	}
#endif
	if (unlikely(!list_empty(&tsk->pi_state_list)))
		exit_pi_state_list(tsk);
#endif

	uprobe_free_utask(tsk);

	*/ Get rid of any cached register state /*
	deactivate_mm(tsk, mm);

	*/
	 If we're exiting normally, clear a user-space tid field if
	 requested.  We leave this alone when dying by signal, to leave
	 the value intact in a core dump, and to save the unnecessary
	 trouble, say, a killed vfork parent shouldn't touch this mm.
	 Userland only wants this done for a sys_exit.
	 /*
	if (tsk->clear_child_tid) {
		if (!(tsk->flags & PF_SIGNALED) &&
		    atomic_read(&mm->mm_users) > 1) {
			*/
			 We don't check the error code - if userspace has
			 not set up a proper pointer then tough luck.
			 /*
			put_user(0, tsk->clear_child_tid);
			sys_futex(tsk->clear_child_tid, FUTEX_WAKE,
					1, NULL, NULL, 0);
		}
		tsk->clear_child_tid = NULL;
	}

	*/
	 All done, finally we can wake up parent and return this mm to him.
	 Also kthread_stop() uses this completion for synchronization.
	 /*
	if (tsk->vfork_done)
		complete_vfork_done(tsk);
}

*/
 Allocate a new mm structure and copy contents from the
 mm structure of the passed in task structure.
 /*
static struct mm_structdup_mm(struct task_structtsk)
{
	struct mm_structmm,oldmm = current->mm;
	int err;

	mm = allocate_mm();
	if (!mm)
		goto fail_nomem;

	memcpy(mm, oldmm, sizeof(*mm));

	if (!mm_init(mm, tsk))
		goto fail_nomem;

	err = dup_mmap(mm, oldmm);
	if (err)
		goto free_pt;

	mm->hiwater_rss = get_mm_rss(mm);
	mm->hiwater_vm = mm->total_vm;

	if (mm->binfmt && !try_module_get(mm->binfmt->module))
		goto free_pt;

	return mm;

free_pt:
	*/ don't put binfmt in mmput, we haven't got module yet /*
	mm->binfmt = NULL;
	mmput(mm);

fail_nomem:
	return NULL;
}

static int copy_mm(unsigned long clone_flags, struct task_structtsk)
{
	struct mm_structmm,oldmm;
	int retval;

	tsk->min_flt = tsk->maj_flt = 0;
	tsk->nvcsw = tsk->nivcsw = 0;
#ifdef CONFIG_DETECT_HUNG_TASK
	tsk->last_switch_count = tsk->nvcsw + tsk->nivcsw;
#endif

	tsk->mm = NULL;
	tsk->active_mm = NULL;

	*/
	 Are we cloning a kernel thread?
	
	 We need to steal a active VM for that..
	 /*
	oldmm = current->mm;
	if (!oldmm)
		return 0;

	*/ initialize the new vmacache entries /*
	vmacache_flush(tsk);

	if (clone_flags & CLONE_VM) {
		atomic_inc(&oldmm->mm_users);
		mm = oldmm;
		goto good_mm;
	}

	retval = -ENOMEM;
	mm = dup_mm(tsk);
	if (!mm)
		goto fail_nomem;

good_mm:
	tsk->mm = mm;
	tsk->active_mm = mm;
	return 0;

fail_nomem:
	return retval;
}

static int copy_fs(unsigned long clone_flags, struct task_structtsk)
{
	struct fs_structfs = current->fs;
	if (clone_flags & CLONE_FS) {
		*/ tsk->fs is already what we want /*
		spin_lock(&fs->lock);
		if (fs->in_exec) {
			spin_unlock(&fs->lock);
			return -EAGAIN;
		}
		fs->users++;
		spin_unlock(&fs->lock);
		return 0;
	}
	tsk->fs = copy_fs_struct(fs);
	if (!tsk->fs)
		return -ENOMEM;
	return 0;
}

static int copy_files(unsigned long clone_flags, struct task_structtsk)
{
	struct files_structoldf,newf;
	int error = 0;

	*/
	 A background process may not have any files ...
	 /*
	oldf = current->files;
	if (!oldf)
		goto out;

	if (clone_flags & CLONE_FILES) {
		atomic_inc(&oldf->count);
		goto out;
	}

	newf = dup_fd(oldf, &error);
	if (!newf)
		goto out;

	tsk->files = newf;
	error = 0;
out:
	return error;
}

static int copy_io(unsigned long clone_flags, struct task_structtsk)
{
#ifdef CONFIG_BLOCK
	struct io_contextioc = current->io_context;
	struct io_contextnew_ioc;

	if (!ioc)
		return 0;
	*/
	 Share io context with parent, if CLONE_IO is set
	 /*
	if (clone_flags & CLONE_IO) {
		ioc_task_link(ioc);
		tsk->io_context = ioc;
	} else if (ioprio_valid(ioc->ioprio)) {
		new_ioc = get_task_io_context(tsk, GFP_KERNEL, NUMA_NO_NODE);
		if (unlikely(!new_ioc))
			return -ENOMEM;

		new_ioc->ioprio = ioc->ioprio;
		put_io_context(new_ioc);
	}
#endif
	return 0;
}

static int copy_sighand(unsigned long clone_flags, struct task_structtsk)
{
	struct sighand_structsig;

	if (clone_flags & CLONE_SIGHAND) {
		atomic_inc(&current->sighand->count);
		return 0;
	}
	sig = kmem_cache_alloc(sighand_cachep, GFP_KERNEL);
	rcu_assign_pointer(tsk->sighand, sig);
	if (!sig)
		return -ENOMEM;

	atomic_set(&sig->count, 1);
	memcpy(sig->action, current->sighand->action, sizeof(sig->action));
	return 0;
}

void __cleanup_sighand(struct sighand_structsighand)
{
	if (atomic_dec_and_test(&sighand->count)) {
		signalfd_cleanup(sighand);
		*/
		 sighand_cachep is SLAB_DESTROY_BY_RCU so we can free it
		 without an RCU grace period, see __lock_task_sighand().
		 /*
		kmem_cache_free(sighand_cachep, sighand);
	}
}

*/
 Initialize POSIX timer handling for a thread group.
 /*
static void posix_cpu_timers_init_group(struct signal_structsig)
{
	unsigned long cpu_limit;

	cpu_limit = READ_ONCE(sig->rlim[RLIMIT_CPU].rlim_cur);
	if (cpu_limit != RLIM_INFINITY) {
		sig->cputime_expires.prof_exp = secs_to_cputime(cpu_limit);
		sig->cputimer.running = true;
	}

	*/ The timer lists. /*
	INIT_LIST_HEAD(&sig->cpu_timers[0]);
	INIT_LIST_HEAD(&sig->cpu_timers[1]);
	INIT_LIST_HEAD(&sig->cpu_timers[2]);
}

static int copy_signal(unsigned long clone_flags, struct task_structtsk)
{
	struct signal_structsig;

	if (clone_flags & CLONE_THREAD)
		return 0;

	sig = kmem_cache_zalloc(signal_cachep, GFP_KERNEL);
	tsk->signal = sig;
	if (!sig)
		return -ENOMEM;

	sig->nr_threads = 1;
	atomic_set(&sig->live, 1);
	atomic_set(&sig->sigcnt, 1);

	*/ list_add(thread_node, thread_head) without INIT_LIST_HEAD() /*
	sig->thread_head = (struct list_head)LIST_HEAD_INIT(tsk->thread_node);
	tsk->thread_node = (struct list_head)LIST_HEAD_INIT(sig->thread_head);

	init_waitqueue_head(&sig->wait_chldexit);
	sig->curr_target = tsk;
	init_sigpending(&sig->shared_pending);
	INIT_LIST_HEAD(&sig->posix_timers);
	seqlock_init(&sig->stats_lock);
	prev_cputime_init(&sig->prev_cputime);

	hrtimer_init(&sig->real_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	sig->real_timer.function = it_real_fn;

	task_lock(current->group_leader);
	memcpy(sig->rlim, current->signal->rlim, sizeof sig->rlim);
	task_unlock(current->group_leader);

	posix_cpu_timers_init_group(sig);

	tty_audit_fork(sig);
	sched_autogroup_fork(sig);

	sig->oom_score_adj = current->signal->oom_score_adj;
	sig->oom_score_adj_min = current->signal->oom_score_adj_min;

	sig->has_child_subreaper = current->signal->has_child_subreaper ||
				   current->signal->is_child_subreaper;

	mutex_init(&sig->cred_guard_mutex);

	return 0;
}

static void copy_seccomp(struct task_structp)
{
#ifdef CONFIG_SECCOMP
	*/
	 Must be called with sighand->lock held, which is common to
	 all threads in the group. Holding cred_guard_mutex is not
	 needed because this new task is not yet running and cannot
	 be racing exec.
	 /*
	assert_spin_locked(&current->sighand->siglock);

	*/ Ref-count the new filter user, and assign it. /*
	get_seccomp_filter(current);
	p->seccomp = current->seccomp;

	*/
	 Explicitly enable no_new_privs here in case it got set
	 between the task_struct being duplicated and holding the
	 sighand lock. The seccomp state and nnp must be in sync.
	 /*
	if (task_no_new_privs(current))
		task_set_no_new_privs(p);

	*/
	 If the parent gained a seccomp mode after copying thread
	 flags and between before we held the sighand lock, we have
	 to manually enable the seccomp thread flag here.
	 /*
	if (p->seccomp.mode != SECCOMP_MODE_DISABLED)
		set_tsk_thread_flag(p, TIF_SECCOMP);
#endif
}

SYSCALL_DEFINE1(set_tid_address, int __user, tidptr)
{
	current->clear_child_tid = tidptr;

	return task_pid_vnr(current);
}

static void rt_mutex_init_task(struct task_structp)
{
	raw_spin_lock_init(&p->pi_lock);
#ifdef CONFIG_RT_MUTEXES
	p->pi_waiters = RB_ROOT;
	p->pi_waiters_leftmost = NULL;
	p->pi_blocked_on = NULL;
#endif
}

*/
 Initialize POSIX timer handling for a single task.
 /*
static void posix_cpu_timers_init(struct task_structtsk)
{
	tsk->cputime_expires.prof_exp = 0;
	tsk->cputime_expires.virt_exp = 0;
	tsk->cputime_expires.sched_exp = 0;
	INIT_LIST_HEAD(&tsk->cpu_timers[0]);
	INIT_LIST_HEAD(&tsk->cpu_timers[1]);
	INIT_LIST_HEAD(&tsk->cpu_timers[2]);
}

static inline void
init_task_pid(struct task_structtask, enum pid_type type, struct pidpid)
{
	 task->pids[type].pid = pid;
}

*/
 This creates a new process as a copy of the old one,
 but does not actually start it yet.

 It copies the registers, and all the appropriate
 parts of the process environment (as per the clone
 flags). The actual kick-off is left to the caller.
 /*
static struct task_structcopy_process(unsigned long clone_flags,
					unsigned long stack_start,
					unsigned long stack_size,
					int __userchild_tidptr,
					struct pidpid,
					int trace,
					unsigned long tls)
{
	int retval;
	struct task_structp;

	if ((clone_flags & (CLONE_NEWNS|CLONE_FS)) == (CLONE_NEWNS|CLONE_FS))
		return ERR_PTR(-EINVAL);

	if ((clone_flags & (CLONE_NEWUSER|CLONE_FS)) == (CLONE_NEWUSER|CLONE_FS))
		return ERR_PTR(-EINVAL);

	*/
	 Thread groups must share signals as well, and detached threads
	 can only be started up within the thread group.
	 /*
	if ((clone_flags & CLONE_THREAD) && !(clone_flags & CLONE_SIGHAND))
		return ERR_PTR(-EINVAL);

	*/
	 Shared signal handlers imply shared VM. By way of the above,
	 thread groups also imply shared VM. Blocking this case allows
	 for various simplifications in other code.
	 /*
	if ((clone_flags & CLONE_SIGHAND) && !(clone_flags & CLONE_VM))
		return ERR_PTR(-EINVAL);

	*/
	 Siblings of global init remain as zombies on exit since they are
	 not reaped by their parent (swapper). To solve this and to avoid
	 multi-rooted process trees, prevent global and container-inits
	 from creating siblings.
	 /*
	if ((clone_flags & CLONE_PARENT) &&
				current->signal->flags & SIGNAL_UNKILLABLE)
		return ERR_PTR(-EINVAL);

	*/
	 If the new process will be in a different pid or user namespace
	 do not allow it to share a thread group with the forking task.
	 /*
	if (clone_flags & CLONE_THREAD) {
		if ((clone_flags & (CLONE_NEWUSER | CLONE_NEWPID)) ||
		    (task_active_pid_ns(current) !=
				current->nsproxy->pid_ns_for_children))
			return ERR_PTR(-EINVAL);
	}

	retval = security_task_create(clone_flags);
	if (retval)
		goto fork_out;

	retval = -ENOMEM;
	p = dup_task_struct(current);
	if (!p)
		goto fork_out;

	ftrace_graph_init_task(p);

	rt_mutex_init_task(p);

#ifdef CONFIG_PROVE_LOCKING
	DEBUG_LOCKS_WARN_ON(!p->hardirqs_enabled);
	DEBUG_LOCKS_WARN_ON(!p->softirqs_enabled);
#endif
	retval = -EAGAIN;
	if (atomic_read(&p->real_cred->user->processes) >=
			task_rlimit(p, RLIMIT_NPROC)) {
		if (p->real_cred->user != INIT_USER &&
		    !capable(CAP_SYS_RESOURCE) && !capable(CAP_SYS_ADMIN))
			goto bad_fork_free;
	}
	current->flags &= ~PF_NPROC_EXCEEDED;

	retval = copy_creds(p, clone_flags);
	if (retval < 0)
		goto bad_fork_free;

	*/
	 If multiple threads are within copy_process(), then this check
	 triggers too late. This doesn't hurt, the check is only there
	 to stop root fork bombs.
	 /*
	retval = -EAGAIN;
	if (nr_threads >= max_threads)
		goto bad_fork_cleanup_count;

	delayacct_tsk_init(p);	*/ Must remain after dup_task_struct() /*
	p->flags &= ~(PF_SUPERPRIV | PF_WQ_WORKER);
	p->flags |= PF_FORKNOEXEC;
	INIT_LIST_HEAD(&p->children);
	INIT_LIST_HEAD(&p->sibling);
	rcu_copy_process(p);
	p->vfork_done = NULL;
	spin_lock_init(&p->alloc_lock);

	init_sigpending(&p->pending);

	p->utime = p->stime = p->gtime = 0;
	p->utimescaled = p->stimescaled = 0;
	prev_cputime_init(&p->prev_cputime);

#ifdef CONFIG_VIRT_CPU_ACCOUNTING_GEN
	seqcount_init(&p->vtime_seqcount);
	p->vtime_snap = 0;
	p->vtime_snap_whence = VTIME_INACTIVE;
#endif

#if defined(SPLIT_RSS_COUNTING)
	memset(&p->rss_stat, 0, sizeof(p->rss_stat));
#endif

	p->default_timer_slack_ns = current->timer_slack_ns;

	task_io_accounting_init(&p->ioac);
	acct_clear_integrals(p);

	posix_cpu_timers_init(p);

	p->start_time = ktime_get_ns();
	p->real_start_time = ktime_get_boot_ns();
	p->io_context = NULL;
	p->audit_context = NULL;
	threadgroup_change_begin(current);
	cgroup_fork(p);
#ifdef CONFIG_NUMA
	p->mempolicy = mpol_dup(p->mempolicy);
	if (IS_ERR(p->mempolicy)) {
		retval = PTR_ERR(p->mempolicy);
		p->mempolicy = NULL;
		goto bad_fork_cleanup_threadgroup_lock;
	}
#endif
#ifdef CONFIG_CPUSETS
	p->cpuset_mem_spread_rotor = NUMA_NO_NODE;
	p->cpuset_slab_spread_rotor = NUMA_NO_NODE;
	seqcount_init(&p->mems_allowed_seq);
#endif
#ifdef CONFIG_TRACE_IRQFLAGS
	p->irq_events = 0;
	p->hardirqs_enabled = 0;
	p->hardirq_enable_ip = 0;
	p->hardirq_enable_event = 0;
	p->hardirq_disable_ip = _THIS_IP_;
	p->hardirq_disable_event = 0;
	p->softirqs_enabled = 1;
	p->softirq_enable_ip = _THIS_IP_;
	p->softirq_enable_event = 0;
	p->softirq_disable_ip = 0;
	p->softirq_disable_event = 0;
	p->hardirq_context = 0;
	p->softirq_context = 0;
#endif

	p->pagefault_disabled = 0;

#ifdef CONFIG_LOCKDEP
	p->lockdep_depth = 0;/ no locks held yet /*
	p->curr_chain_key = 0;
	p->lockdep_recursion = 0;
#endif

#ifdef CONFIG_DEBUG_MUTEXES
	p->blocked_on = NULL;/ not blocked yet /*
#endif
#ifdef CONFIG_BCACHE
	p->sequential_io	= 0;
	p->sequential_io_avg	= 0;
#endif

	*/ Perform scheduler related setup. Assign this task to a CPU. /*
	retval = sched_fork(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_policy;

	retval = perf_event_init_task(p);
	if (retval)
		goto bad_fork_cleanup_policy;
	retval = audit_alloc(p);
	if (retval)
		goto bad_fork_cleanup_perf;
	*/ copy all the process information /*
	shm_init_task(p);
	retval = copy_semundo(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_audit;
	retval = copy_files(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_semundo;
	retval = copy_fs(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_files;
	retval = copy_sighand(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_fs;
	retval = copy_signal(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_sighand;
	retval = copy_mm(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_signal;
	retval = copy_namespaces(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_mm;
	retval = copy_io(clone_flags, p);
	if (retval)
		goto bad_fork_cleanup_namespaces;
	retval = copy_thread_tls(clone_flags, stack_start, stack_size, p, tls);
	if (retval)
		goto bad_fork_cleanup_io;

	if (pid != &init_struct_pid) {
		pid = alloc_pid(p->nsproxy->pid_ns_for_children);
		if (IS_ERR(pid)) {
			retval = PTR_ERR(pid);
			goto bad_fork_cleanup_io;
		}
	}

	p->set_child_tid = (clone_flags & CLONE_CHILD_SETTID) ? child_tidptr : NULL;
	*/
	 Clear TID on mm_release()?
	 /*
	p->clear_child_tid = (clone_flags & CLONE_CHILD_CLEARTID) ? child_tidptr : NULL;
#ifdef CONFIG_BLOCK
	p->plug = NULL;
#endif
#ifdef CONFIG_FUTEX
	p->robust_list = NULL;
#ifdef CONFIG_COMPAT
	p->compat_robust_list = NULL;
#endif
	INIT_LIST_HEAD(&p->pi_state_list);
	p->pi_state_cache = NULL;
#endif
	*/
	 sigaltstack should be cleared when sharing the same VM
	 /*
	if ((clone_flags & (CLONE_VM|CLONE_VFORK)) == CLONE_VM)
		p->sas_ss_sp = p->sas_ss_size = 0;

	*/
	 Syscall tracing and stepping should be turned off in the
	 child regardless of CLONE_PTRACE.
	 /*
	user_disable_single_step(p);
	clear_tsk_thread_flag(p, TIF_SYSCALL_TRACE);
#ifdef TIF_SYSCALL_EMU
	clear_tsk_thread_flag(p, TIF_SYSCALL_EMU);
#endif
	clear_all_latency_tracing(p);

	*/ ok, now we should be set up.. /*
	p->pid = pid_nr(pid);
	if (clone_flags & CLONE_THREAD) {
		p->exit_signal = -1;
		p->group_leader = current->group_leader;
		p->tgid = current->tgid;
	} else {
		if (clone_flags & CLONE_PARENT)
			p->exit_signal = current->group_leader->exit_signal;
		else
			p->exit_signal = (clone_flags & CSIGNAL);
		p->group_leader = p;
		p->tgid = p->pid;
	}

	p->nr_dirtied = 0;
	p->nr_dirtied_pause = 128 >> (PAGE_SHIFT - 10);
	p->dirty_paused_when = 0;

	p->pdeath_signal = 0;
	INIT_LIST_HEAD(&p->thread_group);
	p->task_works = NULL;

	*/
	 Ensure that the cgroup subsystem policies allow the new process to be
	 forked. It should be noted the the new process's css_set can be changed
	 between here and cgroup_post_fork() if an organisation operation is in
	 progress.
	 /*
	retval = cgroup_can_fork(p);
	if (retval)
		goto bad_fork_free_pid;

	*/
	 Make it visible to the rest of the system, but dont wake it up yet.
	 Need tasklist lock for parent etc handling!
	 /*
	write_lock_irq(&tasklist_lock);

	*/ CLONE_PARENT re-uses the old parent /*
	if (clone_flags & (CLONE_PARENT|CLONE_THREAD)) {
		p->real_parent = current->real_parent;
		p->parent_exec_id = current->parent_exec_id;
	} else {
		p->real_parent = current;
		p->parent_exec_id = current->self_exec_id;
	}

	spin_lock(&current->sighand->siglock);

	*/
	 Copy seccomp details explicitly here, in case they were changed
	 before holding sighand lock.
	 /*
	copy_seccomp(p);

	*/
	 Process group and session signals need to be delivered to just the
	 parent before the fork or both the parent and the child after the
	 fork. Restart if a signal comes in before we add the new process to
	 it's process group.
	 A fatal signal pending means that current will exit, so the new
	 thread can't slip out of an OOM kill (or normal SIGKILL).
	/*
	recalc_sigpending();
	if (signal_pending(current)) {
		spin_unlock(&current->sighand->siglock);
		write_unlock_irq(&tasklist_lock);
		retval = -ERESTARTNOINTR;
		goto bad_fork_cancel_cgroup;
	}

	if (likely(p->pid)) {
		ptrace_init_task(p, (clone_flags & CLONE_PTRACE) || trace);

		init_task_pid(p, PIDTYPE_PID, pid);
		if (thread_group_leader(p)) {
			init_task_pid(p, PIDTYPE_PGID, task_pgrp(current));
			init_task_pid(p, PIDTYPE_SID, task_session(current));

			if (is_child_reaper(pid)) {
				ns_of_pid(pid)->child_reaper = p;
				p->signal->flags |= SIGNAL_UNKILLABLE;
			}

			p->signal->leader_pid = pid;
			p->signal->tty = tty_kref_get(current->signal->tty);
			list_add_tail(&p->sibling, &p->real_parent->children);
			list_add_tail_rcu(&p->tasks, &init_task.tasks);
			attach_pid(p, PIDTYPE_PGID);
			attach_pid(p, PIDTYPE_SID);
			__this_cpu_inc(process_counts);
		} else {
			current->signal->nr_threads++;
			atomic_inc(&current->signal->live);
			atomic_inc(&current->signal->sigcnt);
			list_add_tail_rcu(&p->thread_group,
					  &p->group_leader->thread_group);
			list_add_tail_rcu(&p->thread_node,
					  &p->signal->thread_head);
		}
		attach_pid(p, PIDTYPE_PID);
		nr_threads++;
	}

	total_forks++;
	spin_unlock(&current->sighand->siglock);
	syscall_tracepoint_update(p);
	write_unlock_irq(&tasklist_lock);

	proc_fork_connector(p);
	cgroup_post_fork(p);
	threadgroup_change_end(current);
	perf_event_fork(p);

	trace_task_newtask(p, clone_flags);
	uprobe_copy_process(p, clone_flags);

	return p;

bad_fork_cancel_cgroup:
	cgroup_cancel_fork(p);
bad_fork_free_pid:
	if (pid != &init_struct_pid)
		free_pid(pid);
bad_fork_cleanup_io:
	if (p->io_context)
		exit_io_context(p);
bad_fork_cleanup_namespaces:
	exit_task_namespaces(p);
bad_fork_cleanup_mm:
	if (p->mm)
		mmput(p->mm);
bad_fork_cleanup_signal:
	if (!(clone_flags & CLONE_THREAD))
		free_signal_struct(p->signal);
bad_fork_cleanup_sighand:
	__cleanup_sighand(p->sighand);
bad_fork_cleanup_fs:
	exit_fs(p);/ blocking /*
bad_fork_cleanup_files:
	exit_files(p);/ blocking /*
bad_fork_cleanup_semundo:
	exit_sem(p);
bad_fork_cleanup_audit:
	audit_free(p);
bad_fork_cleanup_perf:
	perf_event_free_task(p);
bad_fork_cleanup_policy:
#ifdef CONFIG_NUMA
	mpol_put(p->mempolicy);
bad_fork_cleanup_threadgroup_lock:
#endif
	threadgroup_change_end(current);
	delayacct_tsk_free(p);
bad_fork_cleanup_count:
	atomic_dec(&p->cred->user->processes);
	exit_creds(p);
bad_fork_free:
	free_task(p);
fork_out:
	return ERR_PTR(retval);
}

static inline void init_idle_pids(struct pid_linklinks)
{
	enum pid_type type;

	for (type = PIDTYPE_PID; type < PIDTYPE_MAX; ++type) {
		INIT_HLIST_NODE(&links[type].node);/ not really needed /*
		links[type].pid = &init_struct_pid;
	}
}

struct task_structfork_idle(int cpu)
{
	struct task_structtask;
	task = copy_process(CLONE_VM, 0, 0, NULL, &init_struct_pid, 0, 0);
	if (!IS_ERR(task)) {
		init_idle_pids(task->pids);
		init_idle(task, cpu);
	}

	return task;
}

*/
  Ok, this is the main fork-routine.

 It copies the process, and if successful kick-starts
 it and waits for it to finish using the VM if required.
 /*
long _do_fork(unsigned long clone_flags,
	      unsigned long stack_start,
	      unsigned long stack_size,
	      int __userparent_tidptr,
	      int __userchild_tidptr,
	      unsigned long tls)
{
	struct task_structp;
	int trace = 0;
	long nr;

	*/
	 Determine whether and which event to report to ptracer.  When
	 called from kernel_thread or CLONE_UNTRACED is explicitly
	 requested, no event is reported; otherwise, report if the event
	 for the type of forking is enabled.
	 /*
	if (!(clone_flags & CLONE_UNTRACED)) {
		if (clone_flags & CLONE_VFORK)
			trace = PTRACE_EVENT_VFORK;
		else if ((clone_flags & CSIGNAL) != SIGCHLD)
			trace = PTRACE_EVENT_CLONE;
		else
			trace = PTRACE_EVENT_FORK;

		if (likely(!ptrace_event_enabled(current, trace)))
			trace = 0;
	}

	p = copy_process(clone_flags, stack_start, stack_size,
			 child_tidptr, NULL, trace, tls);
	*/
	 Do this prior waking up the new thread - the thread pointer
	 might get invalid after that point, if the thread exits quickly.
	 /*
	if (!IS_ERR(p)) {
		struct completion vfork;
		struct pidpid;

		trace_sched_process_fork(current, p);

		pid = get_task_pid(p, PIDTYPE_PID);
		nr = pid_vnr(pid);

		if (clone_flags & CLONE_PARENT_SETTID)
			put_user(nr, parent_tidptr);

		if (clone_flags & CLONE_VFORK) {
			p->vfork_done = &vfork;
			init_completion(&vfork);
			get_task_struct(p);
		}

		wake_up_new_task(p);

		*/ forking complete and child started to run, tell ptracer /*
		if (unlikely(trace))
			ptrace_event_pid(trace, pid);

		if (clone_flags & CLONE_VFORK) {
			if (!wait_for_vfork_done(p, &vfork))
				ptrace_event_pid(PTRACE_EVENT_VFORK_DONE, pid);
		}

		put_pid(pid);
	} else {
		nr = PTR_ERR(p);
	}
	return nr;
}

#ifndef CONFIG_HAVE_COPY_THREAD_TLS
*/ For compatibility with architectures that call do_fork directly rather than
 using the syscall entry points below. /*
long do_fork(unsigned long clone_flags,
	      unsigned long stack_start,
	      unsigned long stack_size,
	      int __userparent_tidptr,
	      int __userchild_tidptr)
{
	return _do_fork(clone_flags, stack_start, stack_size,
			parent_tidptr, child_tidptr, 0);
}
#endif

*/
 Create a kernel thread.
 /*
pid_t kernel_thread(int (*fn)(void), voidarg, unsigned long flags)
{
	return _do_fork(flags|CLONE_VM|CLONE_UNTRACED, (unsigned long)fn,
		(unsigned long)arg, NULL, NULL, 0);
}

#ifdef __ARCH_WANT_SYS_FORK
SYSCALL_DEFINE0(fork)
{
#ifdef CONFIG_MMU
	return _do_fork(SIGCHLD, 0, 0, NULL, NULL, 0);
#else
	*/ can not support in nommu mode /*
	return -EINVAL;
#endif
}
#endif

#ifdef __ARCH_WANT_SYS_VFORK
SYSCALL_DEFINE0(vfork)
{
	return _do_fork(CLONE_VFORK | CLONE_VM | SIGCHLD, 0,
			0, NULL, NULL, 0);
}
#endif

#ifdef __ARCH_WANT_SYS_CLONE
#ifdef CONFIG_CLONE_BACKWARDS
SYSCALL_DEFINE5(clone, unsigned long, clone_flags, unsigned long, newsp,
		 int __user, parent_tidptr,
		 unsigned long, tls,
		 int __user, child_tidptr)
#elif defined(CONFIG_CLONE_BACKWARDS2)
SYSCALL_DEFINE5(clone, unsigned long, newsp, unsigned long, clone_flags,
		 int __user, parent_tidptr,
		 int __user, child_tidptr,
		 unsigned long, tls)
#elif defined(CONFIG_CLONE_BACKWARDS3)
SYSCALL_DEFINE6(clone, unsigned long, clone_flags, unsigned long, newsp,
		int, stack_size,
		int __user, parent_tidptr,
		int __user, child_tidptr,
		unsigned long, tls)
#else
SYSCALL_DEFINE5(clone, unsigned long, clone_flags, unsigned long, newsp,
		 int __user, parent_tidptr,
		 int __user, child_tidptr,
		 unsigned long, tls)
#endif
{
	return _do_fork(clone_flags, newsp, 0, parent_tidptr, child_tidptr, tls);
}
#endif

#ifndef ARCH_MIN_MMSTRUCT_ALIGN
#define ARCH_MIN_MMSTRUCT_ALIGN 0
#endif

static void sighand_ctor(voiddata)
{
	struct sighand_structsighand = data;

	spin_lock_init(&sighand->siglock);
	init_waitqueue_head(&sighand->signalfd_wqh);
}

void __init proc_caches_init(void)
{
	sighand_cachep = kmem_cache_create("sighand_cache",
			sizeof(struct sighand_struct), 0,
			SLAB_HWCACHE_ALIGN|SLAB_PANIC|SLAB_DESTROY_BY_RCU|
			SLAB_NOTRACK|SLAB_ACCOUNT, sighand_ctor);
	signal_cachep = kmem_cache_create("signal_cache",
			sizeof(struct signal_struct), 0,
			SLAB_HWCACHE_ALIGN|SLAB_PANIC|SLAB_NOTRACK|SLAB_ACCOUNT,
			NULL);
	files_cachep = kmem_cache_create("files_cache",
			sizeof(struct files_struct), 0,
			SLAB_HWCACHE_ALIGN|SLAB_PANIC|SLAB_NOTRACK|SLAB_ACCOUNT,
			NULL);
	fs_cachep = kmem_cache_create("fs_cache",
			sizeof(struct fs_struct), 0,
			SLAB_HWCACHE_ALIGN|SLAB_PANIC|SLAB_NOTRACK|SLAB_ACCOUNT,
			NULL);
	*/
	 FIXME! The "sizeof(struct mm_struct)" currently includes the
	 whole struct cpumask for the OFFSTACK case. We could change
	 this toonly* allocate as much of it as required by the
	 maximum number of CPU's we can ever have.  The cpumask_allocation
	 is at the end of the structure, exactly for that reason.
	 /*
	mm_cachep = kmem_cache_create("mm_struct",
			sizeof(struct mm_struct), ARCH_MIN_MMSTRUCT_ALIGN,
			SLAB_HWCACHE_ALIGN|SLAB_PANIC|SLAB_NOTRACK|SLAB_ACCOUNT,
			NULL);
	vm_area_cachep = KMEM_CACHE(vm_area_struct, SLAB_PANIC|SLAB_ACCOUNT);
	mmap_init();
	nsproxy_cache_init();
}

*/
 Check constraints on flags passed to the unshare system call.
 /*
static int check_unshare_flags(unsigned long unshare_flags)
{
	if (unshare_flags & ~(CLONE_THREAD|CLONE_FS|CLONE_NEWNS|CLONE_SIGHAND|
				CLONE_VM|CLONE_FILES|CLONE_SYSVSEM|
				CLONE_NEWUTS|CLONE_NEWIPC|CLONE_NEWNET|
				CLONE_NEWUSER|CLONE_NEWPID|CLONE_NEWCGROUP))
		return -EINVAL;
	*/
	 Not implemented, but pretend it works if there is nothing
	 to unshare.  Note that unsharing the address space or the
	 signal handlers also need to unshare the signal queues (aka
	 CLONE_THREAD).
	 /*
	if (unshare_flags & (CLONE_THREAD | CLONE_SIGHAND | CLONE_VM)) {
		if (!thread_group_empty(current))
			return -EINVAL;
	}
	if (unshare_flags & (CLONE_SIGHAND | CLONE_VM)) {
		if (atomic_read(&current->sighand->count) > 1)
			return -EINVAL;
	}
	if (unshare_flags & CLONE_VM) {
		if (!current_is_single_threaded())
			return -EINVAL;
	}

	return 0;
}

*/
 Unshare the filesystem structure if it is being shared
 /*
static int unshare_fs(unsigned long unshare_flags, struct fs_struct*new_fsp)
{
	struct fs_structfs = current->fs;

	if (!(unshare_flags & CLONE_FS) || !fs)
		return 0;

	*/ don't need lock here; in the worst case we'll do useless copy /*
	if (fs->users == 1)
		return 0;

	*new_fsp = copy_fs_struct(fs);
	if (!*new_fsp)
		return -ENOMEM;

	return 0;
}

*/
 Unshare file descriptor table if it is being shared
 /*
static int unshare_fd(unsigned long unshare_flags, struct files_struct*new_fdp)
{
	struct files_structfd = current->files;
	int error = 0;

	if ((unshare_flags & CLONE_FILES) &&
	    (fd && atomic_read(&fd->count) > 1)) {
		*new_fdp = dup_fd(fd, &error);
		if (!*new_fdp)
			return error;
	}

	return 0;
}

*/
 unshare allows a process to 'unshare' part of the process
 context which was originally shared using clone.  copy_*
 functions used by do_fork() cannot be used here directly
 because they modify an inactive task_struct that is being
 constructed. Here we are modifying the current, active,
 task_struct.
 /*
SYSCALL_DEFINE1(unshare, unsigned long, unshare_flags)
{
	struct fs_structfs,new_fs = NULL;
	struct files_structfd,new_fd = NULL;
	struct crednew_cred = NULL;
	struct nsproxynew_nsproxy = NULL;
	int do_sysvsem = 0;
	int err;

	*/
	 If unsharing a user namespace must also unshare the thread group
	 and unshare the filesystem root and working directories.
	 /*
	if (unshare_flags & CLONE_NEWUSER)
		unshare_flags |= CLONE_THREAD | CLONE_FS;
	*/
	 If unsharing vm, must also unshare signal handlers.
	 /*
	if (unshare_flags & CLONE_VM)
		unshare_flags |= CLONE_SIGHAND;
	*/
	 If unsharing a signal handlers, must also unshare the signal queues.
	 /*
	if (unshare_flags & CLONE_SIGHAND)
		unshare_flags |= CLONE_THREAD;
	*/
	 If unsharing namespace, must also unshare filesystem information.
	 /*
	if (unshare_flags & CLONE_NEWNS)
		unshare_flags |= CLONE_FS;

	err = check_unshare_flags(unshare_flags);
	if (err)
		goto bad_unshare_out;
	*/
	 CLONE_NEWIPC must also detach from the undolist: after switching
	 to a new ipc namespace, the semaphore arrays from the old
	 namespace are unreachable.
	 /*
	if (unshare_flags & (CLONE_NEWIPC|CLONE_SYSVSEM))
		do_sysvsem = 1;
	err = unshare_fs(unshare_flags, &new_fs);
	if (err)
		goto bad_unshare_out;
	err = unshare_fd(unshare_flags, &new_fd);
	if (err)
		goto bad_unshare_cleanup_fs;
	err = unshare_userns(unshare_flags, &new_cred);
	if (err)
		goto bad_unshare_cleanup_fd;
	err = unshare_nsproxy_namespaces(unshare_flags, &new_nsproxy,
					 new_cred, new_fs);
	if (err)
		goto bad_unshare_cleanup_cred;

	if (new_fs || new_fd || do_sysvsem || new_cred || new_nsproxy) {
		if (do_sysvsem) {
			*/
			 CLONE_SYSVSEM is equivalent to sys_exit().
			 /*
			exit_sem(current);
		}
		if (unshare_flags & CLONE_NEWIPC) {
			*/ Orphan segments in old ns (see sem above). /*
			exit_shm(current);
			shm_init_task(current);
		}

		if (new_nsproxy)
			switch_task_namespaces(current, new_nsproxy);

		task_lock(current);

		if (new_fs) {
			fs = current->fs;
			spin_lock(&fs->lock);
			current->fs = new_fs;
			if (--fs->users)
				new_fs = NULL;
			else
				new_fs = fs;
			spin_unlock(&fs->lock);
		}

		if (new_fd) {
			fd = current->files;
			current->files = new_fd;
			new_fd = fd;
		}

		task_unlock(current);

		if (new_cred) {
			*/ Install the new user namespace /*
			commit_creds(new_cred);
			new_cred = NULL;
		}
	}

bad_unshare_cleanup_cred:
	if (new_cred)
		put_cred(new_cred);
bad_unshare_cleanup_fd:
	if (new_fd)
		put_files_struct(new_fd);

bad_unshare_cleanup_fs:
	if (new_fs)
		free_fs_struct(new_fs);

bad_unshare_out:
	return err;
}

*/
	Helper to unshare the files of the current task.
	We don't want to expose copy_files internals to
	the exec layer of the kernel.
 /*

int unshare_files(struct files_struct*displaced)
{
	struct task_structtask = current;
	struct files_structcopy = NULL;
	int error;

	error = unshare_fd(CLONE_FILES, &copy);
	if (error || !copy) {
		*displaced = NULL;
		return error;
	}
	*displaced = task->files;
	task_lock(task);
	task->files = copy;
	task_unlock(task);
	return 0;
}

int sysctl_max_threads(struct ctl_tabletable, int write,
		       void __userbuffer, size_tlenp, loff_tppos)
{
	struct ctl_table t;
	int ret;
	int threads = max_threads;
	int min = MIN_THREADS;
	int max = MAX_THREADS;

	t =table;
	t.data = &threads;
	t.extra1 = &min;
	t.extra2 = &max;

	ret = proc_dointvec_minmax(&t, write, buffer, lenp, ppos);
	if (ret || !write)
		return ret;

	set_max_threads(threads);

	return 0;
}
*/
