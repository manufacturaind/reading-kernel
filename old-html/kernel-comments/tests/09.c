
 taskstats.c - Export per-task statistics to userland

 Copyright (C) Shailabh Nagar, IBM Corp. 2006
           (C) Balbir Singh,   IBM Corp. 2006

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 /*

#include <linux/kernel.h>
#include <linux/taskstats_kern.h>
#include <linux/tsacct_kern.h>
#include <linux/delayacct.h>
#include <linux/cpumask.h>
#include <linux/percpu.h>
#include <linux/slab.h>
#include <linux/cgroupstats.h>
#include <linux/cgroup.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/pid_namespace.h>
#include <net/genetlink.h>
#include <linux/atomic.h>

*/
 Maximum length of a cpumask that can be specified in
 the TASKSTATS_CMD_ATTR_REGISTER/DEREGISTER_CPUMASK attribute
 /*
#define TASKSTATS_CPUMASK_MAXLEN	(100+6*NR_CPUS)

static DEFINE_PER_CPU(__u32, taskstats_seqnum);
static int family_registered;
struct kmem_cachetaskstats_cache;

static struct genl_family family = {
	.id		= GENL_ID_GENERATE,
	.name		= TASKSTATS_GENL_NAME,
	.version	= TASKSTATS_GENL_VERSION,
	.maxattr	= TASKSTATS_CMD_ATTR_MAX,
};

static const struct nla_policy taskstats_cmd_get_policy[TASKSTATS_CMD_ATTR_MAX+1] = {
	[TASKSTATS_CMD_ATTR_PID]  = { .type = NLA_U32 },
	[TASKSTATS_CMD_ATTR_TGID] = { .type = NLA_U32 },
	[TASKSTATS_CMD_ATTR_REGISTER_CPUMASK] = { .type = NLA_STRING },
	[TASKSTATS_CMD_ATTR_DEREGISTER_CPUMASK] = { .type = NLA_STRING },};

static const struct nla_policy cgroupstats_cmd_get_policy[CGROUPSTATS_CMD_ATTR_MAX+1] = {
	[CGROUPSTATS_CMD_ATTR_FD] = { .type = NLA_U32 },
};

struct listener {
	struct list_head list;
	pid_t pid;
	char valid;
};

struct listener_list {
	struct rw_semaphore sem;
	struct list_head list;
};
static DEFINE_PER_CPU(struct listener_list, listener_array);

enum actions {
	REGISTER,
	DEREGISTER,
	CPU_DONT_CARE
};

static int prepare_reply(struct genl_infoinfo, u8 cmd, struct sk_buff*skbp,
				size_t size)
{
	struct sk_buffskb;
	voidreply;

	*/
	 If new attributes are added, please revisit this allocation
	 /*
	skb = genlmsg_new(size, GFP_KERNEL);
	if (!skb)
		return -ENOMEM;

	if (!info) {
		int seq = this_cpu_inc_return(taskstats_seqnum) - 1;

		reply = genlmsg_put(skb, 0, seq, &family, 0, cmd);
	} else
		reply = genlmsg_put_reply(skb, info, &family, 0, cmd);
	if (reply == NULL) {
		nlmsg_free(skb);
		return -EINVAL;
	}

	*skbp = skb;
	return 0;
}

*/
 Send taskstats data in @skb to listener with nl_pid @pid
 /*
static int send_reply(struct sk_buffskb, struct genl_infoinfo)
{
	struct genlmsghdrgenlhdr = nlmsg_data(nlmsg_hdr(skb));
	voidreply = genlmsg_data(genlhdr);

	genlmsg_end(skb, reply);

	return genlmsg_reply(skb, info);
}

*/
 Send taskstats data in @skb to listeners registered for @cpu's exit data
 /*
static void send_cpu_listeners(struct sk_buffskb,
					struct listener_listlisteners)
{
	struct genlmsghdrgenlhdr = nlmsg_data(nlmsg_hdr(skb));
	struct listeners,tmp;
	struct sk_buffskb_next,skb_cur = skb;
	voidreply = genlmsg_data(genlhdr);
	int rc, delcount = 0;

	genlmsg_end(skb, reply);

	rc = 0;
	down_read(&listeners->sem);
	list_for_each_entry(s, &listeners->list, list) {
		skb_next = NULL;
		if (!list_is_last(&s->list, &listeners->list)) {
			skb_next = skb_clone(skb_cur, GFP_KERNEL);
			if (!skb_next)
				break;
		}
		rc = genlmsg_unicast(&init_net, skb_cur, s->pid);
		if (rc == -ECONNREFUSED) {
			s->valid = 0;
			delcount++;
		}
		skb_cur = skb_next;
	}
	up_read(&listeners->sem);

	if (skb_cur)
		nlmsg_free(skb_cur);

	if (!delcount)
		return;

	*/ Delete invalidated entries /*
	down_write(&listeners->sem);
	list_for_each_entry_safe(s, tmp, &listeners->list, list) {
		if (!s->valid) {
			list_del(&s->list);
			kfree(s);
		}
	}
	up_write(&listeners->sem);
}

static void fill_stats(struct user_namespaceuser_ns,
		       struct pid_namespacepid_ns,
		       struct task_structtsk, struct taskstatsstats)
{
	memset(stats, 0, sizeof(*stats));
	*/
	 Each accounting subsystem adds calls to its functions to
	 fill in relevant parts of struct taskstsats as follows
	
		per-task-foo(stats, tsk);
	 /*

	delayacct_add_tsk(stats, tsk);

	*/ fill in basic acct fields /*
	stats->version = TASKSTATS_VERSION;
	stats->nvcsw = tsk->nvcsw;
	stats->nivcsw = tsk->nivcsw;
	bacct_add_tsk(user_ns, pid_ns, stats, tsk);

	*/ fill in extended acct fields /*
	xacct_add_tsk(stats, tsk);
}

static int fill_stats_for_pid(pid_t pid, struct taskstatsstats)
{
	struct task_structtsk;

	rcu_read_lock();
	tsk = find_task_by_vpid(pid);
	if (tsk)
		get_task_struct(tsk);
	rcu_read_unlock();
	if (!tsk)
		return -ESRCH;
	fill_stats(current_user_ns(), task_active_pid_ns(current), tsk, stats);
	put_task_struct(tsk);
	return 0;
}

static int fill_stats_for_tgid(pid_t tgid, struct taskstatsstats)
{
	struct task_structtsk,first;
	unsigned long flags;
	int rc = -ESRCH;

	*/
	 Add additional stats from live tasks except zombie thread group
	 leaders who are already counted with the dead tasks
	 /*
	rcu_read_lock();
	first = find_task_by_vpid(tgid);

	if (!first || !lock_task_sighand(first, &flags))
		goto out;

	if (first->signal->stats)
		memcpy(stats, first->signal->stats, sizeof(*stats));
	else
		memset(stats, 0, sizeof(*stats));

	tsk = first;
	do {
		if (tsk->exit_state)
			continue;
		*/
		 Accounting subsystem can call its functions here to
		 fill in relevant parts of struct taskstsats as follows
		
			per-task-foo(stats, tsk);
		 /*
		delayacct_add_tsk(stats, tsk);

		stats->nvcsw += tsk->nvcsw;
		stats->nivcsw += tsk->nivcsw;
	} while_each_thread(first, tsk);

	unlock_task_sighand(first, &flags);
	rc = 0;
out:
	rcu_read_unlock();

	stats->version = TASKSTATS_VERSION;
	*/
	 Accounting subsystems can also add calls here to modify
	 fields of taskstats.
	 /*
	return rc;
}

static void fill_tgid_exit(struct task_structtsk)
{
	unsigned long flags;

	spin_lock_irqsave(&tsk->sighand->siglock, flags);
	if (!tsk->signal->stats)
		goto ret;

	*/
	 Each accounting subsystem calls its functions here to
	 accumalate its per-task stats for tsk, into the per-tgid structure
	
		per-task-foo(tsk->signal->stats, tsk);
	 /*
	delayacct_add_tsk(tsk->signal->stats, tsk);
ret:
	spin_unlock_irqrestore(&tsk->sighand->siglock, flags);
	return;
}

static int add_del_listener(pid_t pid, const struct cpumaskmask, int isadd)
{
	struct listener_listlisteners;
	struct listeners,tmp,s2;
	unsigned int cpu;
	int ret = 0;

	if (!cpumask_subset(mask, cpu_possible_mask))
		return -EINVAL;

	if (current_user_ns() != &init_user_ns)
		return -EINVAL;

	if (task_active_pid_ns(current) != &init_pid_ns)
		return -EINVAL;

	if (isadd == REGISTER) {
		for_each_cpu(cpu, mask) {
			s = kmalloc_node(sizeof(struct listener),
					GFP_KERNEL, cpu_to_node(cpu));
			if (!s) {
				ret = -ENOMEM;
				goto cleanup;
			}
			s->pid = pid;
			s->valid = 1;

			listeners = &per_cpu(listener_array, cpu);
			down_write(&listeners->sem);
			list_for_each_entry(s2, &listeners->list, list) {
				if (s2->pid == pid && s2->valid)
					goto exists;
			}
			list_add(&s->list, &listeners->list);
			s = NULL;
exists:
			up_write(&listeners->sem);
			kfree(s);/ nop if NULL /*
		}
		return 0;
	}

	*/ Deregister or cleanup /*
cleanup:
	for_each_cpu(cpu, mask) {
		listeners = &per_cpu(listener_array, cpu);
		down_write(&listeners->sem);
		list_for_each_entry_safe(s, tmp, &listeners->list, list) {
			if (s->pid == pid) {
				list_del(&s->list);
				kfree(s);
				break;
			}
		}
		up_write(&listeners->sem);
	}
	return ret;
}

static int parse(struct nlattrna, struct cpumaskmask)
{
	chardata;
	int len;
	int ret;

	if (na == NULL)
		return 1;
	len = nla_len(na);
	if (len > TASKSTATS_CPUMASK_MAXLEN)
		return -E2BIG;
	if (len < 1)
		return -EINVAL;
	data = kmalloc(len, GFP_KERNEL);
	if (!data)
		return -ENOMEM;
	nla_strlcpy(data, na, len);
	ret = cpulist_parse(data, mask);
	kfree(data);
	return ret;
}

#if defined(CONFIG_64BIT) && !defined(CONFIG_HAVE_EFFICIENT_UNALIGNED_ACCESS)
#define TASKSTATS_NEEDS_PADDING 1
#endif

static struct taskstatsmk_reply(struct sk_buffskb, int type, u32 pid)
{
	struct nlattrna,ret;
	int aggr;

	aggr = (type == TASKSTATS_TYPE_PID)
			? TASKSTATS_TYPE_AGGR_PID
			: TASKSTATS_TYPE_AGGR_TGID;

	*/
	 The taskstats structure is internally aligned on 8 byte
	 boundaries but the layout of the aggregrate reply, with
	 two NLA headers and the pid (each 4 bytes), actually
	 force the entire structure to be unaligned. This causes
	 the kernel to issue unaligned access warnings on some
	 architectures like ia64. Unfortunately, some software out there
	 doesn't properly unroll the NLA packet and assumes that the start
	 of the taskstats structure will always be 20 bytes from the start
	 of the netlink payload. Aligning the start of the taskstats
	 structure breaks this software, which we don't want. So, for now
	 the alignment only happens on architectures that require it
	 and those users will have to update to fixed versions of those
	 packages. Space is reserved in the packet only when needed.
	 This ifdef should be removed in several years e.g. 2012 once
	 we can be confident that fixed versions are installed on most
	 systems. We add the padding before the aggregate since the
	 aggregate is already a defined type.
	 /*
#ifdef TASKSTATS_NEEDS_PADDING
	if (nla_put(skb, TASKSTATS_TYPE_NULL, 0, NULL) < 0)
		goto err;
#endif
	na = nla_nest_start(skb, aggr);
	if (!na)
		goto err;

	if (nla_put(skb, type, sizeof(pid), &pid) < 0) {
		nla_nest_cancel(skb, na);
		goto err;
	}
	ret = nla_reserve(skb, TASKSTATS_TYPE_STATS, sizeof(struct taskstats));
	if (!ret) {
		nla_nest_cancel(skb, na);
		goto err;
	}
	nla_nest_end(skb, na);

	return nla_data(ret);
err:
	return NULL;
}

static int cgroupstats_user_cmd(struct sk_buffskb, struct genl_infoinfo)
{
	int rc = 0;
	struct sk_buffrep_skb;
	struct cgroupstatsstats;
	struct nlattrna;
	size_t size;
	u32 fd;
	struct fd f;

	na = info->attrs[CGROUPSTATS_CMD_ATTR_FD];
	if (!na)
		return -EINVAL;

	fd = nla_get_u32(info->attrs[CGROUPSTATS_CMD_ATTR_FD]);
	f = fdget(fd);
	if (!f.file)
		return 0;

	size = nla_total_size(sizeof(struct cgroupstats));

	rc = prepare_reply(info, CGROUPSTATS_CMD_NEW, &rep_skb,
				size);
	if (rc < 0)
		goto err;

	na = nla_reserve(rep_skb, CGROUPSTATS_TYPE_CGROUP_STATS,
				sizeof(struct cgroupstats));
	if (na == NULL) {
		nlmsg_free(rep_skb);
		rc = -EMSGSIZE;
		goto err;
	}

	stats = nla_data(na);
	memset(stats, 0, sizeof(*stats));

	rc = cgroupstats_build(stats, f.file->f_path.dentry);
	if (rc < 0) {
		nlmsg_free(rep_skb);
		goto err;
	}

	rc = send_reply(rep_skb, info);

err:
	fdput(f);
	return rc;
}

static int cmd_attr_register_cpumask(struct genl_infoinfo)
{
	cpumask_var_t mask;
	int rc;

	if (!alloc_cpumask_var(&mask, GFP_KERNEL))
		return -ENOMEM;
	rc = parse(info->attrs[TASKSTATS_CMD_ATTR_REGISTER_CPUMASK], mask);
	if (rc < 0)
		goto out;
	rc = add_del_listener(info->snd_portid, mask, REGISTER);
out:
	free_cpumask_var(mask);
	return rc;
}

static int cmd_attr_deregister_cpumask(struct genl_infoinfo)
{
	cpumask_var_t mask;
	int rc;

	if (!alloc_cpumask_var(&mask, GFP_KERNEL))
		return -ENOMEM;
	rc = parse(info->attrs[TASKSTATS_CMD_ATTR_DEREGISTER_CPUMASK], mask);
	if (rc < 0)
		goto out;
	rc = add_del_listener(info->snd_portid, mask, DEREGISTER);
out:
	free_cpumask_var(mask);
	return rc;
}

static size_t taskstats_packet_size(void)
{
	size_t size;

	size = nla_total_size(sizeof(u32)) +
		nla_total_size(sizeof(struct taskstats)) + nla_total_size(0);
#ifdef TASKSTATS_NEEDS_PADDING
	size += nla_total_size(0);/ Padding for alignment /*
#endif
	return size;
}

static int cmd_attr_pid(struct genl_infoinfo)
{
	struct taskstatsstats;
	struct sk_buffrep_skb;
	size_t size;
	u32 pid;
	int rc;

	size = taskstats_packet_size();

	rc = prepare_reply(info, TASKSTATS_CMD_NEW, &rep_skb, size);
	if (rc < 0)
		return rc;

	rc = -EINVAL;
	pid = nla_get_u32(info->attrs[TASKSTATS_CMD_ATTR_PID]);
	stats = mk_reply(rep_skb, TASKSTATS_TYPE_PID, pid);
	if (!stats)
		goto err;

	rc = fill_stats_for_pid(pid, stats);
	if (rc < 0)
		goto err;
	return send_reply(rep_skb, info);
err:
	nlmsg_free(rep_skb);
	return rc;
}

static int cmd_attr_tgid(struct genl_infoinfo)
{
	struct taskstatsstats;
	struct sk_buffrep_skb;
	size_t size;
	u32 tgid;
	int rc;

	size = taskstats_packet_size();

	rc = prepare_reply(info, TASKSTATS_CMD_NEW, &rep_skb, size);
	if (rc < 0)
		return rc;

	rc = -EINVAL;
	tgid = nla_get_u32(info->attrs[TASKSTATS_CMD_ATTR_TGID]);
	stats = mk_reply(rep_skb, TASKSTATS_TYPE_TGID, tgid);
	if (!stats)
		goto err;

	rc = fill_stats_for_tgid(tgid, stats);
	if (rc < 0)
		goto err;
	return send_reply(rep_skb, info);
err:
	nlmsg_free(rep_skb);
	return rc;
}

static int taskstats_user_cmd(struct sk_buffskb, struct genl_infoinfo)
{
	if (info->attrs[TASKSTATS_CMD_ATTR_REGISTER_CPUMASK])
		return cmd_attr_register_cpumask(info);
	else if (info->attrs[TASKSTATS_CMD_ATTR_DEREGISTER_CPUMASK])
		return cmd_attr_deregister_cpumask(info);
	else if (info->attrs[TASKSTATS_CMD_ATTR_PID])
		return cmd_attr_pid(info);
	else if (info->attrs[TASKSTATS_CMD_ATTR_TGID])
		return cmd_attr_tgid(info);
	else
		return -EINVAL;
}

static struct taskstatstaskstats_tgid_alloc(struct task_structtsk)
{
	struct signal_structsig = tsk->signal;
	struct taskstatsstats;

	if (sig->stats || thread_group_empty(tsk))
		goto ret;

	*/ No problem if kmem_cache_zalloc() fails /*
	stats = kmem_cache_zalloc(taskstats_cache, GFP_KERNEL);

	spin_lock_irq(&tsk->sighand->siglock);
	if (!sig->stats) {
		sig->stats = stats;
		stats = NULL;
	}
	spin_unlock_irq(&tsk->sighand->siglock);

	if (stats)
		kmem_cache_free(taskstats_cache, stats);
ret:
	return sig->stats;
}

*/ Send pid data out on exit /*
void taskstats_exit(struct task_structtsk, int group_dead)
{
	int rc;
	struct listener_listlisteners;
	struct taskstatsstats;
	struct sk_buffrep_skb;
	size_t size;
	int is_thread_group;

	if (!family_registered)
		return;

	*/
	 Size includes space for nested attributes
	 /*
	size = taskstats_packet_size();

	is_thread_group = !!taskstats_tgid_alloc(tsk);
	if (is_thread_group) {
		*/ PID + STATS + TGID + STATS /*
		size = 2 size;
		*/ fill the tsk->signal->stats structure /*
		fill_tgid_exit(tsk);
	}

	listeners = raw_cpu_ptr(&listener_array);
	if (list_empty(&listeners->list))
		return;

	rc = prepare_reply(NULL, TASKSTATS_CMD_NEW, &rep_skb, size);
	if (rc < 0)
		return;

	stats = mk_reply(rep_skb, TASKSTATS_TYPE_PID,
			 task_pid_nr_ns(tsk, &init_pid_ns));
	if (!stats)
		goto err;

	fill_stats(&init_user_ns, &init_pid_ns, tsk, stats);

	*/
	 Doesn't matter if tsk is the leader or the last group member leaving
	 /*
	if (!is_thread_group || !group_dead)
		goto send;

	stats = mk_reply(rep_skb, TASKSTATS_TYPE_TGID,
			 task_tgid_nr_ns(tsk, &init_pid_ns));
	if (!stats)
		goto err;

	memcpy(stats, tsk->signal->stats, sizeof(*stats));

send:
	send_cpu_listeners(rep_skb, listeners);
	return;
err:
	nlmsg_free(rep_skb);
}

static const struct genl_ops taskstats_ops[] = {
	{
		.cmd		= TASKSTATS_CMD_GET,
		.doit		= taskstats_user_cmd,
		.policy		= taskstats_cmd_get_policy,
		.flags		= GENL_ADMIN_PERM,
	},
	{
		.cmd		= CGROUPSTATS_CMD_GET,
		.doit		= cgroupstats_user_cmd,
		.policy		= cgroupstats_cmd_get_policy,
	},
};

*/ Needed early in initialization /*
void __init taskstats_init_early(void)
{
	unsigned int i;

	taskstats_cache = KMEM_CACHE(taskstats, SLAB_PANIC);
	for_each_possible_cpu(i) {
		INIT_LIST_HEAD(&(per_cpu(listener_array, i).list));
		init_rwsem(&(per_cpu(listener_array, i).sem));
	}
}

static int __init taskstats_init(void)
{
	int rc;

	rc = genl_register_family_with_ops(&family, taskstats_ops);
	if (rc)
		return rc;

	family_registered = 1;
	pr_info("registered taskstats version %d\n", TASKSTATS_GENL_VERSION);
	return 0;
}

*/
 late initcall ensures initialization of statistics collection
 mechanisms precedes initialization of the taskstats interface
 /*
late_initcall(taskstats_init);
*/
/*
#include <linux/spinlock.h>
#include <linux/task_work.h>
#include <linux/tracehook.h>

static struct callback_head work_exited;/ all we need is ->next == NULL /*

*/
 task_work_add - ask the @task to execute @work->func()
 @task: the task which should run the callback
 @work: the callback to run
 @notify: send the notification if true

 Queue @work for task_work_run() below and notify the @task if @notify.
 Fails if the @task is exiting/exited and thus it can't process this @work.
 Otherwise @work->func() will be called when the @task returns from kernel
 mode or exits.

 This is like the signal handler which runs in kernel mode, but it doesn't
 try to wake up the @task.

 Note: there is no ordering guarantee on works queued here.

 RETURNS:
 0 if succeeds or -ESRCH.
 /*
int
task_work_add(struct task_structtask, struct callback_headwork, bool notify)
{
	struct callback_headhead;

	do {
		head = ACCESS_ONCE(task->task_works);
		if (unlikely(head == &work_exited))
			return -ESRCH;
		work->next = head;
	} while (cmpxchg(&task->task_works, head, work) != head);

	if (notify)
		set_notify_resume(task);
	return 0;
}

*/
 task_work_cancel - cancel a pending work added by task_work_add()
 @task: the task which should execute the work
 @func: identifies the work to remove

 Find the last queued pending work with ->func == @func and remove
 it from queue.

 RETURNS:
 The found work or NULL if not found.
 /*
struct callback_head
task_work_cancel(struct task_structtask, task_work_func_t func)
{
	struct callback_head*pprev = &task->task_works;
	struct callback_headwork;
	unsigned long flags;
	*/
	 If cmpxchg() fails we continue without updating pprev.
	 Either we raced with task_work_add() which added the
	 new entry before this work, we will find it again. Or
	 we raced with task_work_run(),pprev == NULL/exited.
	 /*
	raw_spin_lock_irqsave(&task->pi_lock, flags);
	while ((work = ACCESS_ONCE(*pprev))) {
		smp_read_barrier_depends();
		if (work->func != func)
			pprev = &work->next;
		else if (cmpxchg(pprev, work, work->next) == work)
			break;
	}
	raw_spin_unlock_irqrestore(&task->pi_lock, flags);

	return work;
}

*/
 task_work_run - execute the works added by task_work_add()

 Flush the pending works. Should be used by the core kernel code.
 Called before the task returns to the user-mode or stops, or when
 it exits. In the latter case task_work_add() can no longer add the
 new work after task_work_run() returns.
 /*
void task_work_run(void)
{
	struct task_structtask = current;
	struct callback_headwork,head,next;

	for (;;) {
		*/
		 work->func() can do task_work_add(), do not set
		 work_exited unless the list is empty.
		 /*
		do {
			work = ACCESS_ONCE(task->task_works);
			head = !work && (task->flags & PF_EXITING) ?
				&work_exited : NULL;
		} while (cmpxchg(&task->task_works, work, head) != work);

		if (!work)
			break;
		*/
		 Synchronize with task_work_cancel(). It can't remove
		 the first entry == work, cmpxchg(task_works) should
		 fail, but it can play withwork and other entries.
		 /*
		raw_spin_unlock_wait(&task->pi_lock);
		smp_mb();

		do {
			next = work->next;
			work->func(work);
			work = next;
			cond_resched();
		} while (work);
	}
}
*/

 test_kprobes.c - simple sanity test forprobes

 Copyright IBM Corp. 2008

 This program is free software;  you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it would be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 the GNU General Public License for more details.
 /*

#define pr_fmt(fmt) "Kprobe smoke test: " fmt

#include <linux/kernel.h>
#include <linux/kprobes.h>
#include <linux/random.h>

#define div_factor 3

static u32 rand1, preh_val, posth_val, jph_val;
static int errors, handler_errors, num_tests;
static u32 (*target)(u32 value);
static u32 (*target2)(u32 value);

static noinline u32 kprobe_target(u32 value)
{
	return (value / div_factor);
}

static int kp_pre_handler(struct kprobep, struct pt_regsregs)
{
	preh_val = (rand1 / div_factor);
	return 0;
}

static void kp_post_handler(struct kprobep, struct pt_regsregs,
		unsigned long flags)
{
	if (preh_val != (rand1 / div_factor)) {
		handler_errors++;
		pr_err("incorrect value in post_handler\n");
	}
	posth_val = preh_val + div_factor;
}

static struct kprobe kp = {
	.symbol_name = "kprobe_target",
	.pre_handler = kp_pre_handler,
	.post_handler = kp_post_handler
};

static int test_kprobe(void)
{
	int ret;

	ret = register_kprobe(&kp);
	if (ret < 0) {
		pr_err("register_kprobe returned %d\n", ret);
		return ret;
	}

	ret = target(rand1);
	unregister_kprobe(&kp);

	if (preh_val == 0) {
		pr_err("kprobe pre_handler not called\n");
		handler_errors++;
	}

	if (posth_val == 0) {
		pr_err("kprobe post_handler not called\n");
		handler_errors++;
	}

	return 0;
}

static noinline u32 kprobe_target2(u32 value)
{
	return (value / div_factor) + 1;
}

static int kp_pre_handler2(struct kprobep, struct pt_regsregs)
{
	preh_val = (rand1 / div_factor) + 1;
	return 0;
}

static void kp_post_handler2(struct kprobep, struct pt_regsregs,
		unsigned long flags)
{
	if (preh_val != (rand1 / div_factor) + 1) {
		handler_errors++;
		pr_err("incorrect value in post_handler2\n");
	}
	posth_val = preh_val + div_factor;
}

static struct kprobe kp2 = {
	.symbol_name = "kprobe_target2",
	.pre_handler = kp_pre_handler2,
	.post_handler = kp_post_handler2
};

static int test_kprobes(void)
{
	int ret;
	struct kprobekps[2] = {&kp, &kp2};

	*/ addr and flags should be cleard for reusing kprobe. /*
	kp.addr = NULL;
	kp.flags = 0;
	ret = register_kprobes(kps, 2);
	if (ret < 0) {
		pr_err("register_kprobes returned %d\n", ret);
		return ret;
	}

	preh_val = 0;
	posth_val = 0;
	ret = target(rand1);

	if (preh_val == 0) {
		pr_err("kprobe pre_handler not called\n");
		handler_errors++;
	}

	if (posth_val == 0) {
		pr_err("kprobe post_handler not called\n");
		handler_errors++;
	}

	preh_val = 0;
	posth_val = 0;
	ret = target2(rand1);

	if (preh_val == 0) {
		pr_err("kprobe pre_handler2 not called\n");
		handler_errors++;
	}

	if (posth_val == 0) {
		pr_err("kprobe post_handler2 not called\n");
		handler_errors++;
	}

	unregister_kprobes(kps, 2);
	return 0;

}

static u32 j_kprobe_target(u32 value)
{
	if (value != rand1) {
		handler_errors++;
		pr_err("incorrect value in jprobe handler\n");
	}

	jph_val = rand1;
	jprobe_return();
	return 0;
}

static struct jprobe jp = {
	.entry		= j_kprobe_target,
	.kp.symbol_name = "kprobe_target"
};

static int test_jprobe(void)
{
	int ret;

	ret = register_jprobe(&jp);
	if (ret < 0) {
		pr_err("register_jprobe returned %d\n", ret);
		return ret;
	}

	ret = target(rand1);
	unregister_jprobe(&jp);
	if (jph_val == 0) {
		pr_err("jprobe handler not called\n");
		handler_errors++;
	}

	return 0;
}

static struct jprobe jp2 = {
	.entry          = j_kprobe_target,
	.kp.symbol_name = "kprobe_target2"
};

static int test_jprobes(void)
{
	int ret;
	struct jprobejps[2] = {&jp, &jp2};

	*/ addr and flags should be cleard for reusing kprobe. /*
	jp.kp.addr = NULL;
	jp.kp.flags = 0;
	ret = register_jprobes(jps, 2);
	if (ret < 0) {
		pr_err("register_jprobes returned %d\n", ret);
		return ret;
	}

	jph_val = 0;
	ret = target(rand1);
	if (jph_val == 0) {
		pr_err("jprobe handler not called\n");
		handler_errors++;
	}

	jph_val = 0;
	ret = target2(rand1);
	if (jph_val == 0) {
		pr_err("jprobe handler2 not called\n");
		handler_errors++;
	}
	unregister_jprobes(jps, 2);

	return 0;
}
#ifdef CONFIG_KRETPROBES
static u32 krph_val;

static int entry_handler(struct kretprobe_instanceri, struct pt_regsregs)
{
	krph_val = (rand1 / div_factor);
	return 0;
}

static int return_handler(struct kretprobe_instanceri, struct pt_regsregs)
{
	unsigned long ret = regs_return_value(regs);

	if (ret != (rand1 / div_factor)) {
		handler_errors++;
		pr_err("incorrect value in kretprobe handler\n");
	}
	if (krph_val == 0) {
		handler_errors++;
		pr_err("call to kretprobe entry handler failed\n");
	}

	krph_val = rand1;
	return 0;
}

static struct kretprobe rp = {
	.handler	= return_handler,
	.entry_handler  = entry_handler,
	.kp.symbol_name = "kprobe_target"
};

static int test_kretprobe(void)
{
	int ret;

	ret = register_kretprobe(&rp);
	if (ret < 0) {
		pr_err("register_kretprobe returned %d\n", ret);
		return ret;
	}

	ret = target(rand1);
	unregister_kretprobe(&rp);
	if (krph_val != rand1) {
		pr_err("kretprobe handler not called\n");
		handler_errors++;
	}

	return 0;
}

static int return_handler2(struct kretprobe_instanceri, struct pt_regsregs)
{
	unsigned long ret = regs_return_value(regs);

	if (ret != (rand1 / div_factor) + 1) {
		handler_errors++;
		pr_err("incorrect value in kretprobe handler2\n");
	}
	if (krph_val == 0) {
		handler_errors++;
		pr_err("call to kretprobe entry handler failed\n");
	}

	krph_val = rand1;
	return 0;
}

static struct kretprobe rp2 = {
	.handler	= return_handler2,
	.entry_handler  = entry_handler,
	.kp.symbol_name = "kprobe_target2"
};

static int test_kretprobes(void)
{
	int ret;
	struct kretproberps[2] = {&rp, &rp2};

	*/ addr and flags should be cleard for reusing kprobe. /*
	rp.kp.addr = NULL;
	rp.kp.flags = 0;
	ret = register_kretprobes(rps, 2);
	if (ret < 0) {
		pr_err("register_kretprobe returned %d\n", ret);
		return ret;
	}

	krph_val = 0;
	ret = target(rand1);
	if (krph_val != rand1) {
		pr_err("kretprobe handler not called\n");
		handler_errors++;
	}

	krph_val = 0;
	ret = target2(rand1);
	if (krph_val != rand1) {
		pr_err("kretprobe handler2 not called\n");
		handler_errors++;
	}
	unregister_kretprobes(rps, 2);
	return 0;
}
#endif */ CONFIG_KRETPROBES /*

int init_test_probes(void)
{
	int ret;

	target = kprobe_target;
	target2 = kprobe_target2;

	do {
		rand1 = prandom_u32();
	} while (rand1 <= div_factor);

	pr_info("started\n");
	num_tests++;
	ret = test_kprobe();
	if (ret < 0)
		errors++;

	num_tests++;
	ret = test_kprobes();
	if (ret < 0)
		errors++;

	num_tests++;
	ret = test_jprobe();
	if (ret < 0)
		errors++;

	num_tests++;
	ret = test_jprobes();
	if (ret < 0)
		errors++;

#ifdef CONFIG_KRETPROBES
	num_tests++;
	ret = test_kretprobe();
	if (ret < 0)
		errors++;

	num_tests++;
	ret = test_kretprobes();
	if (ret < 0)
		errors++;
#endif */ CONFIG_KRETPROBES /*

	if (errors)
		pr_err("BUG: %d out of %d tests failed\n", errors, num_tests);
	else if (handler_errors)
		pr_err("BUG: %d error(s) running handlers\n", handler_errors);
	else
		pr_info("passed successfully\n");

	return 0;
}
*/

 Common functions for in-kernel torture tests.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, you can access it online at
 http://www.gnu.org/licenses/gpl-2.0.html.

 Copyright (C) IBM Corporation, 2014

 Author: Paul E. McKenney <paulmck@us.ibm.com>
	Based on kernel/rcu/torture.c.
 /*
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/spinlock.h>
#include <linux/smp.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/atomic.h>
#include <linux/bitops.h>
#include <linux/completion.h>
#include <linux/moduleparam.h>
#include <linux/percpu.h>
#include <linux/notifier.h>
#include <linux/reboot.h>
#include <linux/freezer.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <linux/stat.h>
#include <linux/slab.h>
#include <linux/trace_clock.h>
#include <asm/byteorder.h>
#include <linux/torture.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Paul E. McKenney <paulmck@us.ibm.com>");

static chartorture_type;
static bool verbose;

*/ Mediate rmmod and system shutdown.  Concurrent rmmod & shutdown illegal! /*
#define FULLSTOP_DONTSTOP 0	*/ Normal operation. /*
#define FULLSTOP_SHUTDOWN 1	*/ System shutdown with torture running. /*
#define FULLSTOP_RMMOD    2	*/ Normal rmmod of torture. /*
static int fullstop = FULLSTOP_RMMOD;
static DEFINE_MUTEX(fullstop_mutex);
static inttorture_runnable;

#ifdef CONFIG_HOTPLUG_CPU

*/
 Variables for online-offline handling.  Only present if CPU hotplug
 is enabled, otherwise does nothing.
 /*

static struct task_structonoff_task;
static long onoff_holdoff;
static long onoff_interval;
static long n_offline_attempts;
static long n_offline_successes;
static unsigned long sum_offline;
static int min_offline = -1;
static int max_offline;
static long n_online_attempts;
static long n_online_successes;
static unsigned long sum_online;
static int min_online = -1;
static int max_online;

*/
 Execute random CPU-hotplug operations at the interval specified
 by the onoff_interval.
 /*
static int
torture_onoff(voidarg)
{
	int cpu;
	unsigned long delta;
	int maxcpu = -1;
	DEFINE_TORTURE_RANDOM(rand);
	int ret;
	unsigned long starttime;

	VERBOSE_TOROUT_STRING("torture_onoff task started");
	for_each_online_cpu(cpu)
		maxcpu = cpu;
	WARN_ON(maxcpu < 0);
	if (onoff_holdoff > 0) {
		VERBOSE_TOROUT_STRING("torture_onoff begin holdoff");
		schedule_timeout_interruptible(onoff_holdoff);
		VERBOSE_TOROUT_STRING("torture_onoff end holdoff");
	}
	while (!torture_must_stop()) {
		cpu = (torture_random(&rand) >> 4) % (maxcpu + 1);
		if (cpu_online(cpu) && cpu_is_hotpluggable(cpu)) {
			if (verbose)
				pr_alert("%s" TORTURE_FLAG
					 "torture_onoff task: offlining %d\n",
					 torture_type, cpu);
			starttime = jiffies;
			n_offline_attempts++;
			ret = cpu_down(cpu);
			if (ret) {
				if (verbose)
					pr_alert("%s" TORTURE_FLAG
						 "torture_onoff task: offline %d failed: errno %d\n",
						 torture_type, cpu, ret);
			} else {
				if (verbose)
					pr_alert("%s" TORTURE_FLAG
						 "torture_onoff task: offlined %d\n",
						 torture_type, cpu);
				n_offline_successes++;
				delta = jiffies - starttime;
				sum_offline += delta;
				if (min_offline < 0) {
					min_offline = delta;
					max_offline = delta;
				}
				if (min_offline > delta)
					min_offline = delta;
				if (max_offline < delta)
					max_offline = delta;
			}
		} else if (cpu_is_hotpluggable(cpu)) {
			if (verbose)
				pr_alert("%s" TORTURE_FLAG
					 "torture_onoff task: onlining %d\n",
					 torture_type, cpu);
			starttime = jiffies;
			n_online_attempts++;
			ret = cpu_up(cpu);
			if (ret) {
				if (verbose)
					pr_alert("%s" TORTURE_FLAG
						 "torture_onoff task: online %d failed: errno %d\n",
						 torture_type, cpu, ret);
			} else {
				if (verbose)
					pr_alert("%s" TORTURE_FLAG
						 "torture_onoff task: onlined %d\n",
						 torture_type, cpu);
				n_online_successes++;
				delta = jiffies - starttime;
				sum_online += delta;
				if (min_online < 0) {
					min_online = delta;
					max_online = delta;
				}
				if (min_online > delta)
					min_online = delta;
				if (max_online < delta)
					max_online = delta;
			}
		}
		schedule_timeout_interruptible(onoff_interval);
	}
	torture_kthread_stopping("torture_onoff");
	return 0;
}

#endif */ #ifdef CONFIG_HOTPLUG_CPU /*

*/
 Initiate online-offline handling.
 /*
int torture_onoff_init(long ooholdoff, long oointerval)
{
	int ret = 0;

#ifdef CONFIG_HOTPLUG_CPU
	onoff_holdoff = ooholdoff;
	onoff_interval = oointerval;
	if (onoff_interval <= 0)
		return 0;
	ret = torture_create_kthread(torture_onoff, NULL, onoff_task);
#endif */ #ifdef CONFIG_HOTPLUG_CPU /*
	return ret;
}
EXPORT_SYMBOL_GPL(torture_onoff_init);

*/
 Clean up after online/offline testing.
 /*
static void torture_onoff_cleanup(void)
{
#ifdef CONFIG_HOTPLUG_CPU
	if (onoff_task == NULL)
		return;
	VERBOSE_TOROUT_STRING("Stopping torture_onoff task");
	kthread_stop(onoff_task);
	onoff_task = NULL;
#endif */ #ifdef CONFIG_HOTPLUG_CPU /*
}
EXPORT_SYMBOL_GPL(torture_onoff_cleanup);

*/
 Print online/offline testing statistics.
 /*
void torture_onoff_stats(void)
{
#ifdef CONFIG_HOTPLUG_CPU
	pr_cont("onoff: %ld/%ld:%ld/%ld %d,%d:%d,%d %lu:%lu (HZ=%d) ",
		n_online_successes, n_online_attempts,
		n_offline_successes, n_offline_attempts,
		min_online, max_online,
		min_offline, max_offline,
		sum_online, sum_offline, HZ);
#endif */ #ifdef CONFIG_HOTPLUG_CPU /*
}
EXPORT_SYMBOL_GPL(torture_onoff_stats);

*/
 Were all the online/offline operations successful?
 /*
bool torture_onoff_failures(void)
{
#ifdef CONFIG_HOTPLUG_CPU
	return n_online_successes != n_online_attempts ||
	       n_offline_successes != n_offline_attempts;
#else */ #ifdef CONFIG_HOTPLUG_CPU /*
	return false;
#endif */ #else #ifdef CONFIG_HOTPLUG_CPU /*
}
EXPORT_SYMBOL_GPL(torture_onoff_failures);

#define TORTURE_RANDOM_MULT	39916801 / prime /*
#define TORTURE_RANDOM_ADD	479001701/ prime /*
#define TORTURE_RANDOM_REFRESH	10000

*/
 Crude but fast random-number generator.  Uses a linear congruential
 generator, with occasional help from cpu_clock().
 /*
unsigned long
torture_random(struct torture_random_statetrsp)
{
	if (--trsp->trs_count < 0) {
		trsp->trs_state += (unsigned long)local_clock();
		trsp->trs_count = TORTURE_RANDOM_REFRESH;
	}
	trsp->trs_state = trsp->trs_state TORTURE_RANDOM_MULT +
		TORTURE_RANDOM_ADD;
	return swahw32(trsp->trs_state);
}
EXPORT_SYMBOL_GPL(torture_random);

*/
 Variables for shuffling.  The idea is to ensure that each CPU stays
 idle for an extended period to test interactions with dyntick idle,
 as well as interactions with any per-CPU varibles.
 /*
struct shuffle_task {
	struct list_head st_l;
	struct task_structst_t;
};

static long shuffle_interval;	*/ In jiffies. /*
static struct task_structshuffler_task;
static cpumask_var_t shuffle_tmp_mask;
static int shuffle_idle_cpu;	*/ Force all torture tasks off this CPU /*
static struct list_head shuffle_task_list = LIST_HEAD_INIT(shuffle_task_list);
static DEFINE_MUTEX(shuffle_task_mutex);

*/
 Register a task to be shuffled.  If there is no memory, just splat
 and don't bother registering.
 /*
void torture_shuffle_task_register(struct task_structtp)
{
	struct shuffle_taskstp;

	if (WARN_ON_ONCE(tp == NULL))
		return;
	stp = kmalloc(sizeof(*stp), GFP_KERNEL);
	if (WARN_ON_ONCE(stp == NULL))
		return;
	stp->st_t = tp;
	mutex_lock(&shuffle_task_mutex);
	list_add(&stp->st_l, &shuffle_task_list);
	mutex_unlock(&shuffle_task_mutex);
}
EXPORT_SYMBOL_GPL(torture_shuffle_task_register);

*/
 Unregister all tasks, for example, at the end of the torture run.
 /*
static void torture_shuffle_task_unregister_all(void)
{
	struct shuffle_taskstp;
	struct shuffle_taskp;

	mutex_lock(&shuffle_task_mutex);
	list_for_each_entry_safe(stp, p, &shuffle_task_list, st_l) {
		list_del(&stp->st_l);
		kfree(stp);
	}
	mutex_unlock(&shuffle_task_mutex);
}

*/ Shuffle tasks such that we allow shuffle_idle_cpu to become idle.
 A special case is when shuffle_idle_cpu = -1, in which case we allow
 the tasks to run on all CPUs.
 /*
static void torture_shuffle_tasks(void)
{
	struct shuffle_taskstp;

	cpumask_setall(shuffle_tmp_mask);
	get_online_cpus();

	*/ No point in shuffling if there is only one online CPU (ex: UP) /*
	if (num_online_cpus() == 1) {
		put_online_cpus();
		return;
	}

	*/ Advance to the next CPU.  Upon overflow, don't idle any CPUs. /*
	shuffle_idle_cpu = cpumask_next(shuffle_idle_cpu, shuffle_tmp_mask);
	if (shuffle_idle_cpu >= nr_cpu_ids)
		shuffle_idle_cpu = -1;
	else
		cpumask_clear_cpu(shuffle_idle_cpu, shuffle_tmp_mask);

	mutex_lock(&shuffle_task_mutex);
	list_for_each_entry(stp, &shuffle_task_list, st_l)
		set_cpus_allowed_ptr(stp->st_t, shuffle_tmp_mask);
	mutex_unlock(&shuffle_task_mutex);

	put_online_cpus();
}

*/ Shuffle tasks across CPUs, with the intent of allowing each CPU in the
 system to become idle at a time and cut off its timer ticks. This is meant
 to test the support for such tickless idle CPU in RCU.
 /*
static int torture_shuffle(voidarg)
{
	VERBOSE_TOROUT_STRING("torture_shuffle task started");
	do {
		schedule_timeout_interruptible(shuffle_interval);
		torture_shuffle_tasks();
		torture_shutdown_absorb("torture_shuffle");
	} while (!torture_must_stop());
	torture_kthread_stopping("torture_shuffle");
	return 0;
}

*/
 Start the shuffler, with shuffint in jiffies.
 /*
int torture_shuffle_init(long shuffint)
{
	shuffle_interval = shuffint;

	shuffle_idle_cpu = -1;

	if (!alloc_cpumask_var(&shuffle_tmp_mask, GFP_KERNEL)) {
		VERBOSE_TOROUT_ERRSTRING("Failed to alloc mask");
		return -ENOMEM;
	}

	*/ Create the shuffler thread /*
	return torture_create_kthread(torture_shuffle, NULL, shuffler_task);
}
EXPORT_SYMBOL_GPL(torture_shuffle_init);

*/
 Stop the shuffling.
 /*
static void torture_shuffle_cleanup(void)
{
	torture_shuffle_task_unregister_all();
	if (shuffler_task) {
		VERBOSE_TOROUT_STRING("Stopping torture_shuffle task");
		kthread_stop(shuffler_task);
		free_cpumask_var(shuffle_tmp_mask);
	}
	shuffler_task = NULL;
}
EXPORT_SYMBOL_GPL(torture_shuffle_cleanup);

*/
 Variables for auto-shutdown.  This allows "lights out" torture runs
 to be fully scripted.
 /*
static int shutdown_secs;		*/ desired test duration in seconds. /*
static struct task_structshutdown_task;
static unsigned long shutdown_time;	*/ jiffies to system shutdown. /*
static void (*torture_shutdown_hook)(void);

*/
 Absorb kthreads into a kernel function that won't return, so that
 they won't ever access module text or data again.
 /*
void torture_shutdown_absorb(const chartitle)
{
	while (READ_ONCE(fullstop) == FULLSTOP_SHUTDOWN) {
		pr_notice("torture thread %s parking due to system shutdown\n",
			  title);
		schedule_timeout_uninterruptible(MAX_SCHEDULE_TIMEOUT);
	}
}
EXPORT_SYMBOL_GPL(torture_shutdown_absorb);

*/
 Cause the torture test to shutdown the system after the test has
 run for the time specified by the shutdown_secs parameter.
 /*
static int torture_shutdown(voidarg)
{
	long delta;
	unsigned long jiffies_snap;

	VERBOSE_TOROUT_STRING("torture_shutdown task started");
	jiffies_snap = jiffies;
	while (ULONG_CMP_LT(jiffies_snap, shutdown_time) &&
	       !torture_must_stop()) {
		delta = shutdown_time - jiffies_snap;
		if (verbose)
			pr_alert("%s" TORTURE_FLAG
				 "torture_shutdown task: %lu jiffies remaining\n",
				 torture_type, delta);
		schedule_timeout_interruptible(delta);
		jiffies_snap = jiffies;
	}
	if (torture_must_stop()) {
		torture_kthread_stopping("torture_shutdown");
		return 0;
	}

	*/ OK, shut down the system. /*

	VERBOSE_TOROUT_STRING("torture_shutdown task shutting down system");
	shutdown_task = NULL;	*/ Avoid self-kill deadlock. /*
	if (torture_shutdown_hook)
		torture_shutdown_hook();
	else
		VERBOSE_TOROUT_STRING("No torture_shutdown_hook(), skipping.");
	kernel_power_off();	*/ Shut down the system. /*
	return 0;
}

*/
 Start up the shutdown task.
 /*
int torture_shutdown_init(int ssecs, void (*cleanup)(void))
{
	int ret = 0;

	shutdown_secs = ssecs;
	torture_shutdown_hook = cleanup;
	if (shutdown_secs > 0) {
		shutdown_time = jiffies + shutdown_secs HZ;
		ret = torture_create_kthread(torture_shutdown, NULL,
					     shutdown_task);
	}
	return ret;
}
EXPORT_SYMBOL_GPL(torture_shutdown_init);

*/
 Detect and respond to a system shutdown.
 /*
static int torture_shutdown_notify(struct notifier_blockunused1,
				   unsigned long unused2, voidunused3)
{
	mutex_lock(&fullstop_mutex);
	if (READ_ONCE(fullstop) == FULLSTOP_DONTSTOP) {
		VERBOSE_TOROUT_STRING("Unscheduled system shutdown detected");
		WRITE_ONCE(fullstop, FULLSTOP_SHUTDOWN);
	} else {
		pr_warn("Concurrent rmmod and shutdown illegal!\n");
	}
	mutex_unlock(&fullstop_mutex);
	return NOTIFY_DONE;
}

static struct notifier_block torture_shutdown_nb = {
	.notifier_call = torture_shutdown_notify,
};

*/
 Shut down the shutdown task.  Say what???  Heh!  This can happen if
 the torture module gets an rmmod before the shutdown time arrives.  ;-)
 /*
static void torture_shutdown_cleanup(void)
{
	unregister_reboot_notifier(&torture_shutdown_nb);
	if (shutdown_task != NULL) {
		VERBOSE_TOROUT_STRING("Stopping torture_shutdown task");
		kthread_stop(shutdown_task);
	}
	shutdown_task = NULL;
}

*/
 Variables for stuttering, which means to periodically pause and
 restart testing in order to catch bugs that appear when load is
 suddenly applied to or removed from the system.
 /*
static struct task_structstutter_task;
static int stutter_pause_test;
static int stutter;

*/
 Block until the stutter interval ends.  This must be called periodically
 by all running kthreads that need to be subject to stuttering.
 /*
void stutter_wait(const chartitle)
{
	cond_resched_rcu_qs();
	while (READ_ONCE(stutter_pause_test) ||
	       (torture_runnable && !READ_ONCE(*torture_runnable))) {
		if (stutter_pause_test)
			if (READ_ONCE(stutter_pause_test) == 1)
				schedule_timeout_interruptible(1);
			else
				while (READ_ONCE(stutter_pause_test))
					cond_resched();
		else
			schedule_timeout_interruptible(round_jiffies_relative(HZ));
		torture_shutdown_absorb(title);
	}
}
EXPORT_SYMBOL_GPL(stutter_wait);

*/
 Cause the torture test to "stutter", starting and stopping all
 threads periodically.
 /*
static int torture_stutter(voidarg)
{
	VERBOSE_TOROUT_STRING("torture_stutter task started");
	do {
		if (!torture_must_stop()) {
			if (stutter > 1) {
				schedule_timeout_interruptible(stutter - 1);
				WRITE_ONCE(stutter_pause_test, 2);
			}
			schedule_timeout_interruptible(1);
			WRITE_ONCE(stutter_pause_test, 1);
		}
		if (!torture_must_stop())
			schedule_timeout_interruptible(stutter);
		WRITE_ONCE(stutter_pause_test, 0);
		torture_shutdown_absorb("torture_stutter");
	} while (!torture_must_stop());
	torture_kthread_stopping("torture_stutter");
	return 0;
}

*/
 Initialize and kick off the torture_stutter kthread.
 /*
int torture_stutter_init(int s)
{
	int ret;

	stutter = s;
	ret = torture_create_kthread(torture_stutter, NULL, stutter_task);
	return ret;
}
EXPORT_SYMBOL_GPL(torture_stutter_init);

*/
 Cleanup after the torture_stutter kthread.
 /*
static void torture_stutter_cleanup(void)
{
	if (!stutter_task)
		return;
	VERBOSE_TOROUT_STRING("Stopping torture_stutter task");
	kthread_stop(stutter_task);
	stutter_task = NULL;
}

*/
 Initialize torture module.  Please note that this is -not- invoked via
 the usual module_init() mechanism, but rather by an explicit call from
 the client torture module.  This call must be paired with a later
 torture_init_end().

 The runnable parameter points to a flag that controls whether or not
 the test is currently runnable.  If there is no such flag, pass in NULL.
 /*
bool torture_init_begin(charttype, bool v, intrunnable)
{
	mutex_lock(&fullstop_mutex);
	if (torture_type != NULL) {
		pr_alert("torture_init_begin: refusing %s init: %s running",
			 ttype, torture_type);
		mutex_unlock(&fullstop_mutex);
		return false;
	}
	torture_type = ttype;
	verbose = v;
	torture_runnable = runnable;
	fullstop = FULLSTOP_DONTSTOP;
	return true;
}
EXPORT_SYMBOL_GPL(torture_init_begin);

*/
 Tell the torture module that initialization is complete.
 /*
void torture_init_end(void)
{
	mutex_unlock(&fullstop_mutex);
	register_reboot_notifier(&torture_shutdown_nb);
}
EXPORT_SYMBOL_GPL(torture_init_end);

*/
 Clean up torture module.  Please note that this is -not- invoked via
 the usual module_exit() mechanism, but rather by an explicit call from
 the client torture module.  Returns true if a race with system shutdown
 is detected, otherwise, all kthreads started by functions in this file
 will be shut down.

 This must be called before the caller starts shutting down its own
 kthreads.

 Both torture_cleanup_begin() and torture_cleanup_end() must be paired,
 in order to correctly perform the cleanup. They are separated because
 threads can still need to reference the torture_type type, thus nullify
 only after completing all other relevant calls.
 /*
bool torture_cleanup_begin(void)
{
	mutex_lock(&fullstop_mutex);
	if (READ_ONCE(fullstop) == FULLSTOP_SHUTDOWN) {
		pr_warn("Concurrent rmmod and shutdown illegal!\n");
		mutex_unlock(&fullstop_mutex);
		schedule_timeout_uninterruptible(10);
		return true;
	}
	WRITE_ONCE(fullstop, FULLSTOP_RMMOD);
	mutex_unlock(&fullstop_mutex);
	torture_shutdown_cleanup();
	torture_shuffle_cleanup();
	torture_stutter_cleanup();
	torture_onoff_cleanup();
	return false;
}
EXPORT_SYMBOL_GPL(torture_cleanup_begin);

void torture_cleanup_end(void)
{
	mutex_lock(&fullstop_mutex);
	torture_type = NULL;
	mutex_unlock(&fullstop_mutex);
}
EXPORT_SYMBOL_GPL(torture_cleanup_end);

*/
 Is it time for the current torture test to stop?
 /*
bool torture_must_stop(void)
{
	return torture_must_stop_irq() || kthread_should_stop();
}
EXPORT_SYMBOL_GPL(torture_must_stop);

*/
 Is it time for the current torture test to stop?  This is the irq-safe
 version, hence no check for kthread_should_stop().
 /*
bool torture_must_stop_irq(void)
{
	return READ_ONCE(fullstop) != FULLSTOP_DONTSTOP;
}
EXPORT_SYMBOL_GPL(torture_must_stop_irq);

*/
 Each kthread must wait for kthread_should_stop() before returning from
 its top-level function, otherwise segfaults ensue.  This function
 prints a "stopping" message and waits for kthread_should_stop(), and
 should be called from all torture kthreads immediately prior to
 returning.
 /*
void torture_kthread_stopping(chartitle)
{
	char buf[128];

	snprintf(buf, sizeof(buf), "Stopping %s", title);
	VERBOSE_TOROUT_STRING(buf);
	while (!kthread_should_stop()) {
		torture_shutdown_absorb(title);
		schedule_timeout_uninterruptible(1);
	}
}
EXPORT_SYMBOL_GPL(torture_kthread_stopping);

*/
 Create a generic torture kthread that is immediately runnable.  If you
 need the kthread to be stopped so that you can do something to it before
 it starts, you will need to open-code your own.
 /*
int _torture_create_kthread(int (*fn)(voidarg), voidarg, chars, charm,
			    charf, struct task_struct*tp)
{
	int ret = 0;

	VERBOSE_TOROUT_STRING(m);
	*tp = kthread_run(fn, arg, "%s", s);
	if (IS_ERR(*tp)) {
		ret = PTR_ERR(*tp);
		VERBOSE_TOROUT_ERRSTRING(f);
		*tp = NULL;
	}
	torture_shuffle_task_register(*tp);
	return ret;
}
EXPORT_SYMBOL_GPL(_torture_create_kthread);

*/
 Stop a generic kthread, emitting a message.
 /*
void _torture_stop_kthread(charm, struct task_struct*tp)
{
	if (*tp == NULL)
		return;
	VERBOSE_TOROUT_STRING(m);
	kthread_stop(*tp);
	*tp = NULL;
}
EXPORT_SYMBOL_GPL(_torture_stop_kthread);
*/

 Copyright (C) 2008-2014 Mathieu Desnoyers

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 /*
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/types.h>
#include <linux/jhash.h>
#include <linux/list.h>
#include <linux/rcupdate.h>
#include <linux/tracepoint.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/static_key.h>

extern struct tracepoint const __start___tracepoints_ptrs[];
extern struct tracepoint const __stop___tracepoints_ptrs[];

*/ Set to 1 to enable tracepoint debug output /*
static const int tracepoint_debug;

#ifdef CONFIG_MODULES
*/
 Tracepoint module list mutex protects the local module list.
 /*
static DEFINE_MUTEX(tracepoint_module_list_mutex);

*/ Local list of struct tp_module /*
static LIST_HEAD(tracepoint_module_list);
#endif */ CONFIG_MODULES /*

*/
 tracepoints_mutex protects the builtin and module tracepoints.
 tracepoints_mutex nests inside tracepoint_module_list_mutex.
 /*
static DEFINE_MUTEX(tracepoints_mutex);

*/
 Note about RCU :
 It is used to delay the free of multiple probes array until a quiescent
 state is reached.
 /*
struct tp_probes {
	struct rcu_head rcu;
	struct tracepoint_func probes[0];
};

static inline voidallocate_probes(int count)
{
	struct tp_probesp  = kmalloc(count sizeof(struct tracepoint_func)
			+ sizeof(struct tp_probes), GFP_KERNEL);
	return p == NULL ? NULL : p->probes;
}

static void rcu_free_old_probes(struct rcu_headhead)
{
	kfree(container_of(head, struct tp_probes, rcu));
}

static inline void release_probes(struct tracepoint_funcold)
{
	if (old) {
		struct tp_probestp_probes = container_of(old,
			struct tp_probes, probes[0]);
		call_rcu_sched(&tp_probes->rcu, rcu_free_old_probes);
	}
}

static void debug_print_probes(struct tracepoint_funcfuncs)
{
	int i;

	if (!tracepoint_debug || !funcs)
		return;

	for (i = 0; funcs[i].func; i++)
		printk(KERN_DEBUG "Probe %d : %p\n", i, funcs[i].func);
}

static struct tracepoint_func
func_add(struct tracepoint_func*funcs, struct tracepoint_functp_func,
	 int prio)
{
	struct tracepoint_funcold,new;
	int nr_probes = 0;
	int pos = -1;

	if (WARN_ON(!tp_func->func))
		return ERR_PTR(-EINVAL);

	debug_print_probes(*funcs);
	old =funcs;
	if (old) {
		*/ (N -> N+1), (N != 0, 1) probes /*
		for (nr_probes = 0; old[nr_probes].func; nr_probes++) {
			*/ Insert before probes of lower priority /*
			if (pos < 0 && old[nr_probes].prio < prio)
				pos = nr_probes;
			if (old[nr_probes].func == tp_func->func &&
			    old[nr_probes].data == tp_func->data)
				return ERR_PTR(-EEXIST);
		}
	}
	*/ + 2 : one for new probe, one for NULL func /*
	new = allocate_probes(nr_probes + 2);
	if (new == NULL)
		return ERR_PTR(-ENOMEM);
	if (old) {
		if (pos < 0) {
			pos = nr_probes;
			memcpy(new, old, nr_probes sizeof(struct tracepoint_func));
		} else {
			*/ Copy higher priority probes ahead of the new probe /*
			memcpy(new, old, pos sizeof(struct tracepoint_func));
			*/ Copy the rest after it. /*
			memcpy(new + pos + 1, old + pos,
			       (nr_probes - pos) sizeof(struct tracepoint_func));
		}
	} else
		pos = 0;
	new[pos] =tp_func;
	new[nr_probes + 1].func = NULL;
	*funcs = new;
	debug_print_probes(*funcs);
	return old;
}

static voidfunc_remove(struct tracepoint_func*funcs,
		struct tracepoint_functp_func)
{
	int nr_probes = 0, nr_del = 0, i;
	struct tracepoint_funcold,new;

	old =funcs;

	if (!old)
		return ERR_PTR(-ENOENT);

	debug_print_probes(*funcs);
	*/ (N -> M), (N > 1, M >= 0) probes /*
	if (tp_func->func) {
		for (nr_probes = 0; old[nr_probes].func; nr_probes++) {
			if (old[nr_probes].func == tp_func->func &&
			     old[nr_probes].data == tp_func->data)
				nr_del++;
		}
	}

	*/
	 If probe is NULL, then nr_probes = nr_del = 0, and then the
	 entire entry will be removed.
	 /*
	if (nr_probes - nr_del == 0) {
		*/ N -> 0, (N > 1) /*
		*funcs = NULL;
		debug_print_probes(*funcs);
		return old;
	} else {
		int j = 0;
		*/ N -> M, (N > 1, M > 0) /*
		*/ + 1 for NULL /*
		new = allocate_probes(nr_probes - nr_del + 1);
		if (new == NULL)
			return ERR_PTR(-ENOMEM);
		for (i = 0; old[i].func; i++)
			if (old[i].func != tp_func->func
					|| old[i].data != tp_func->data)
				new[j++] = old[i];
		new[nr_probes - nr_del].func = NULL;
		*funcs = new;
	}
	debug_print_probes(*funcs);
	return old;
}

*/
 Add the probe function to a tracepoint.
 /*
static int tracepoint_add_func(struct tracepointtp,
			       struct tracepoint_funcfunc, int prio)
{
	struct tracepoint_funcold,tp_funcs;

	if (tp->regfunc && !static_key_enabled(&tp->key))
		tp->regfunc();

	tp_funcs = rcu_dereference_protected(tp->funcs,
			lockdep_is_held(&tracepoints_mutex));
	old = func_add(&tp_funcs, func, prio);
	if (IS_ERR(old)) {
		WARN_ON_ONCE(1);
		return PTR_ERR(old);
	}

	*/
	 rcu_assign_pointer has a smp_wmb() which makes sure that the new
	 probe callbacks array is consistent before setting a pointer to it.
	 This array is referenced by __DO_TRACE from
	 include/linux/tracepoints.h. A matching smp_read_barrier_depends()
	 is used.
	 /*
	rcu_assign_pointer(tp->funcs, tp_funcs);
	if (!static_key_enabled(&tp->key))
		static_key_slow_inc(&tp->key);
	release_probes(old);
	return 0;
}

*/
 Remove a probe function from a tracepoint.
 Note: only waiting an RCU period after setting elem->call to the empty
 function insures that the original callback is not used anymore. This insured
 by preempt_disable around the call site.
 /*
static int tracepoint_remove_func(struct tracepointtp,
		struct tracepoint_funcfunc)
{
	struct tracepoint_funcold,tp_funcs;

	tp_funcs = rcu_dereference_protected(tp->funcs,
			lockdep_is_held(&tracepoints_mutex));
	old = func_remove(&tp_funcs, func);
	if (IS_ERR(old)) {
		WARN_ON_ONCE(1);
		return PTR_ERR(old);
	}

	if (!tp_funcs) {
		*/ Removed last function /*
		if (tp->unregfunc && static_key_enabled(&tp->key))
			tp->unregfunc();

		if (static_key_enabled(&tp->key))
			static_key_slow_dec(&tp->key);
	}
	rcu_assign_pointer(tp->funcs, tp_funcs);
	release_probes(old);
	return 0;
}

*/
 tracepoint_probe_register -  Connect a probe to a tracepoint
 @tp: tracepoint
 @probe: probe handler
 @data: tracepoint data
 @prio: priority of this function over other registered functions

 Returns 0 if ok, error value on error.
 Note: if @tp is within a module, the caller is responsible for
 unregistering the probe before the module is gone. This can be
 performed either with a tracepoint module going notifier, or from
 within module exit functions.
 /*
int tracepoint_probe_register_prio(struct tracepointtp, voidprobe,
				   voiddata, int prio)
{
	struct tracepoint_func tp_func;
	int ret;

	mutex_lock(&tracepoints_mutex);
	tp_func.func = probe;
	tp_func.data = data;
	tp_func.prio = prio;
	ret = tracepoint_add_func(tp, &tp_func, prio);
	mutex_unlock(&tracepoints_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(tracepoint_probe_register_prio);

*/
 tracepoint_probe_register -  Connect a probe to a tracepoint
 @tp: tracepoint
 @probe: probe handler
 @data: tracepoint data
 @prio: priority of this function over other registered functions

 Returns 0 if ok, error value on error.
 Note: if @tp is within a module, the caller is responsible for
 unregistering the probe before the module is gone. This can be
 performed either with a tracepoint module going notifier, or from
 within module exit functions.
 /*
int tracepoint_probe_register(struct tracepointtp, voidprobe, voiddata)
{
	return tracepoint_probe_register_prio(tp, probe, data, TRACEPOINT_DEFAULT_PRIO);
}
EXPORT_SYMBOL_GPL(tracepoint_probe_register);

*/
 tracepoint_probe_unregister -  Disconnect a probe from a tracepoint
 @tp: tracepoint
 @probe: probe function pointer
 @data: tracepoint data

 Returns 0 if ok, error value on error.
 /*
int tracepoint_probe_unregister(struct tracepointtp, voidprobe, voiddata)
{
	struct tracepoint_func tp_func;
	int ret;

	mutex_lock(&tracepoints_mutex);
	tp_func.func = probe;
	tp_func.data = data;
	ret = tracepoint_remove_func(tp, &tp_func);
	mutex_unlock(&tracepoints_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(tracepoint_probe_unregister);

#ifdef CONFIG_MODULES
bool trace_module_has_bad_taint(struct modulemod)
{
	return mod->taints & ~((1 << TAINT_OOT_MODULE) | (1 << TAINT_CRAP) |
			       (1 << TAINT_UNSIGNED_MODULE));
}

static BLOCKING_NOTIFIER_HEAD(tracepoint_notify_list);

*/
 register_tracepoint_notifier - register tracepoint coming/going notifier
 @nb: notifier block

 Notifiers registered with this function are called on module
 coming/going with the tracepoint_module_list_mutex held.
 The notifier block callback should expect a "struct tp_module" data
 pointer.
 /*
int register_tracepoint_module_notifier(struct notifier_blocknb)
{
	struct tp_moduletp_mod;
	int ret;

	mutex_lock(&tracepoint_module_list_mutex);
	ret = blocking_notifier_chain_register(&tracepoint_notify_list, nb);
	if (ret)
		goto end;
	list_for_each_entry(tp_mod, &tracepoint_module_list, list)
		(void) nb->notifier_call(nb, MODULE_STATE_COMING, tp_mod);
end:
	mutex_unlock(&tracepoint_module_list_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(register_tracepoint_module_notifier);

*/
 unregister_tracepoint_notifier - unregister tracepoint coming/going notifier
 @nb: notifier block

 The notifier block callback should expect a "struct tp_module" data
 pointer.
 /*
int unregister_tracepoint_module_notifier(struct notifier_blocknb)
{
	struct tp_moduletp_mod;
	int ret;

	mutex_lock(&tracepoint_module_list_mutex);
	ret = blocking_notifier_chain_unregister(&tracepoint_notify_list, nb);
	if (ret)
		goto end;
	list_for_each_entry(tp_mod, &tracepoint_module_list, list)
		(void) nb->notifier_call(nb, MODULE_STATE_GOING, tp_mod);
end:
	mutex_unlock(&tracepoint_module_list_mutex);
	return ret;

}
EXPORT_SYMBOL_GPL(unregister_tracepoint_module_notifier);

*/
 Ensure the tracer unregistered the module's probes before the module
 teardown is performed. Prevents leaks of probe and data pointers.
 /*
static void tp_module_going_check_quiescent(struct tracepoint constbegin,
		struct tracepoint constend)
{
	struct tracepoint constiter;

	if (!begin)
		return;
	for (iter = begin; iter < end; iter++)
		WARN_ON_ONCE((*iter)->funcs);
}

static int tracepoint_module_coming(struct modulemod)
{
	struct tp_moduletp_mod;
	int ret = 0;

	if (!mod->num_tracepoints)
		return 0;

	*/
	 We skip modules that taint the kernel, especially those with different
	 module headers (for forced load), to make sure we don't cause a crash.
	 Staging, out-of-tree, and unsigned GPL modules are fine.
	 /*
	if (trace_module_has_bad_taint(mod))
		return 0;
	mutex_lock(&tracepoint_module_list_mutex);
	tp_mod = kmalloc(sizeof(struct tp_module), GFP_KERNEL);
	if (!tp_mod) {
		ret = -ENOMEM;
		goto end;
	}
	tp_mod->mod = mod;
	list_add_tail(&tp_mod->list, &tracepoint_module_list);
	blocking_notifier_call_chain(&tracepoint_notify_list,
			MODULE_STATE_COMING, tp_mod);
end:
	mutex_unlock(&tracepoint_module_list_mutex);
	return ret;
}

static void tracepoint_module_going(struct modulemod)
{
	struct tp_moduletp_mod;

	if (!mod->num_tracepoints)
		return;

	mutex_lock(&tracepoint_module_list_mutex);
	list_for_each_entry(tp_mod, &tracepoint_module_list, list) {
		if (tp_mod->mod == mod) {
			blocking_notifier_call_chain(&tracepoint_notify_list,
					MODULE_STATE_GOING, tp_mod);
			list_del(&tp_mod->list);
			kfree(tp_mod);
			*/
			 Called the going notifier before checking for
			 quiescence.
			 /*
			tp_module_going_check_quiescent(mod->tracepoints_ptrs,
				mod->tracepoints_ptrs + mod->num_tracepoints);
			break;
		}
	}
	*/
	 In the case of modules that were tainted at "coming", we'll simply
	 walk through the list without finding it. We cannot use the "tainted"
	 flag on "going", in case a module taints the kernel only after being
	 loaded.
	 /*
	mutex_unlock(&tracepoint_module_list_mutex);
}

static int tracepoint_module_notify(struct notifier_blockself,
		unsigned long val, voiddata)
{
	struct modulemod = data;
	int ret = 0;

	switch (val) {
	case MODULE_STATE_COMING:
		ret = tracepoint_module_coming(mod);
		break;
	case MODULE_STATE_LIVE:
		break;
	case MODULE_STATE_GOING:
		tracepoint_module_going(mod);
		break;
	case MODULE_STATE_UNFORMED:
		break;
	}
	return ret;
}

static struct notifier_block tracepoint_module_nb = {
	.notifier_call = tracepoint_module_notify,
	.priority = 0,
};

static __init int init_tracepoints(void)
{
	int ret;

	ret = register_module_notifier(&tracepoint_module_nb);
	if (ret)
		pr_warn("Failed to register tracepoint module enter notifier\n");

	return ret;
}
__initcall(init_tracepoints);
#endif */ CONFIG_MODULES /*

static void for_each_tracepoint_range(struct tracepoint constbegin,
		struct tracepoint constend,
		void (*fct)(struct tracepointtp, voidpriv),
		voidpriv)
{
	struct tracepoint constiter;

	if (!begin)
		return;
	for (iter = begin; iter < end; iter++)
		fct(*iter, priv);
}

*/
 for_each_kernel_tracepoint - iteration on all kernel tracepoints
 @fct: callback
 @priv: private data
 /*
void for_each_kernel_tracepoint(void (*fct)(struct tracepointtp, voidpriv),
		voidpriv)
{
	for_each_tracepoint_range(__start___tracepoints_ptrs,
		__stop___tracepoints_ptrs, fct, priv);
}
EXPORT_SYMBOL_GPL(for_each_kernel_tracepoint);

#ifdef CONFIG_HAVE_SYSCALL_TRACEPOINTS

*/ NB: reg/unreg are called while guarded with the tracepoints_mutex /*
static int sys_tracepoint_refcount;

void syscall_regfunc(void)
{
	struct task_structp,t;

	if (!sys_tracepoint_refcount) {
		read_lock(&tasklist_lock);
		for_each_process_thread(p, t) {
			set_tsk_thread_flag(t, TIF_SYSCALL_TRACEPOINT);
		}
		read_unlock(&tasklist_lock);
	}
	sys_tracepoint_refcount++;
}

void syscall_unregfunc(void)
{
	struct task_structp,t;

	sys_tracepoint_refcount--;
	if (!sys_tracepoint_refcount) {
		read_lock(&tasklist_lock);
		for_each_process_thread(p, t) {
			clear_tsk_thread_flag(t, TIF_SYSCALL_TRACEPOINT);
		}
		read_unlock(&tasklist_lock);
	}
}
#endif
*/

 tsacct.c - System accounting over taskstats interface

 Copyright (C) Jay Lan,	<jlan@sgi.com>


 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 /*

#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/tsacct_kern.h>
#include <linux/acct.h>
#include <linux/jiffies.h>
#include <linux/mm.h>

*/
 fill in basic accounting fields
 /*
void bacct_add_tsk(struct user_namespaceuser_ns,
		   struct pid_namespacepid_ns,
		   struct taskstatsstats, struct task_structtsk)
{
	const struct credtcred;
	cputime_t utime, stime, utimescaled, stimescaled;
	u64 delta;

	BUILD_BUG_ON(TS_COMM_LEN < TASK_COMM_LEN);

	*/ calculate task elapsed time in nsec /*
	delta = ktime_get_ns() - tsk->start_time;
	*/ Convert to micro seconds /*
	do_div(delta, NSEC_PER_USEC);
	stats->ac_etime = delta;
	*/ Convert to seconds for btime /*
	do_div(delta, USEC_PER_SEC);
	stats->ac_btime = get_seconds() - delta;
	if (thread_group_leader(tsk)) {
		stats->ac_exitcode = tsk->exit_code;
		if (tsk->flags & PF_FORKNOEXEC)
			stats->ac_flag |= AFORK;
	}
	if (tsk->flags & PF_SUPERPRIV)
		stats->ac_flag |= ASU;
	if (tsk->flags & PF_DUMPCORE)
		stats->ac_flag |= ACORE;
	if (tsk->flags & PF_SIGNALED)
		stats->ac_flag |= AXSIG;
	stats->ac_nice	 = task_nice(tsk);
	stats->ac_sched	 = tsk->policy;
	stats->ac_pid	 = task_pid_nr_ns(tsk, pid_ns);
	rcu_read_lock();
	tcred = __task_cred(tsk);
	stats->ac_uid	 = from_kuid_munged(user_ns, tcred->uid);
	stats->ac_gid	 = from_kgid_munged(user_ns, tcred->gid);
	stats->ac_ppid	 = pid_alive(tsk) ?
		task_tgid_nr_ns(rcu_dereference(tsk->real_parent), pid_ns) : 0;
	rcu_read_unlock();

	task_cputime(tsk, &utime, &stime);
	stats->ac_utime = cputime_to_usecs(utime);
	stats->ac_stime = cputime_to_usecs(stime);

	task_cputime_scaled(tsk, &utimescaled, &stimescaled);
	stats->ac_utimescaled = cputime_to_usecs(utimescaled);
	stats->ac_stimescaled = cputime_to_usecs(stimescaled);

	stats->ac_minflt = tsk->min_flt;
	stats->ac_majflt = tsk->maj_flt;

	strncpy(stats->ac_comm, tsk->comm, sizeof(stats->ac_comm));
}


#ifdef CONFIG_TASK_XACCT

#define KB 1024
#define MB (1024*KB)
#define KB_MASK (~(KB-1))
*/
 fill in extended accounting fields
 /*
void xacct_add_tsk(struct taskstatsstats, struct task_structp)
{
	struct mm_structmm;

	*/ convert pages-nsec/1024 to Mbyte-usec, see __acct_update_integrals /*
	stats->coremem = p->acct_rss_mem1 PAGE_SIZE;
	do_div(stats->coremem, 1000 KB);
	stats->virtmem = p->acct_vm_mem1 PAGE_SIZE;
	do_div(stats->virtmem, 1000 KB);
	mm = get_task_mm(p);
	if (mm) {
		*/ adjust to KB unit /*
		stats->hiwater_rss   = get_mm_hiwater_rss(mm) PAGE_SIZE / KB;
		stats->hiwater_vm    = get_mm_hiwater_vm(mm)  PAGE_SIZE / KB;
		mmput(mm);
	}
	stats->read_char	= p->ioac.rchar & KB_MASK;
	stats->write_char	= p->ioac.wchar & KB_MASK;
	stats->read_syscalls	= p->ioac.syscr & KB_MASK;
	stats->write_syscalls	= p->ioac.syscw & KB_MASK;
#ifdef CONFIG_TASK_IO_ACCOUNTING
	stats->read_bytes	= p->ioac.read_bytes & KB_MASK;
	stats->write_bytes	= p->ioac.write_bytes & KB_MASK;
	stats->cancelled_write_bytes = p->ioac.cancelled_write_bytes & KB_MASK;
#else
	stats->read_bytes	= 0;
	stats->write_bytes	= 0;
	stats->cancelled_write_bytes = 0;
#endif
}
#undef KB
#undef MB

static void __acct_update_integrals(struct task_structtsk,
				    cputime_t utime, cputime_t stime)
{
	cputime_t time, dtime;
	u64 delta;

	if (!likely(tsk->mm))
		return;

	time = stime + utime;
	dtime = time - tsk->acct_timexpd;
	*/ Avoid division: cputime_t is often in nanoseconds already. /*
	delta = cputime_to_nsecs(dtime);

	if (delta < TICK_NSEC)
		return;

	tsk->acct_timexpd = time;
	*/
	 Divide by 1024 to avoid overflow, and to avoid division.
	 The final unit reported to userspace is Mbyte-usecs,
	 the rest of the math is done in xacct_add_tsk.
	 /*
	tsk->acct_rss_mem1 += delta get_mm_rss(tsk->mm) >> 10;
	tsk->acct_vm_mem1 += delta tsk->mm->total_vm >> 10;
}

*/
 acct_update_integrals - update mm integral fields in task_struct
 @tsk: task_struct for accounting
 /*
void acct_update_integrals(struct task_structtsk)
{
	cputime_t utime, stime;
	unsigned long flags;

	local_irq_save(flags);
	task_cputime(tsk, &utime, &stime);
	__acct_update_integrals(tsk, utime, stime);
	local_irq_restore(flags);
}

*/
 acct_account_cputime - update mm integral after cputime update
 @tsk: task_struct for accounting
 /*
void acct_account_cputime(struct task_structtsk)
{
	__acct_update_integrals(tsk, tsk->utime, tsk->stime);
}

*/
 acct_clear_integrals - clear the mm integral fields in task_struct
 @tsk: task_struct whose accounting fields are cleared
 /*
void acct_clear_integrals(struct task_structtsk)
{
	tsk->acct_timexpd = 0;
	tsk->acct_rss_mem1 = 0;
	tsk->acct_vm_mem1 = 0;
}
#endif
*/

	Wrapper functions for 16bit uid back compatibility. All nicely tied
	together in the faint hope we can take the out in five years time.
 /*

#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/notifier.h>
#include <linux/reboot.h>
#include <linux/prctl.h>
#include <linux/capability.h>
#include <linux/init.h>
#include <linux/highuid.h>
#include <linux/security.h>
#include <linux/syscalls.h>

#include <asm/uaccess.h>

SYSCALL_DEFINE3(chown16, const char __user, filename, old_uid_t, user, old_gid_t, group)
{
	return sys_chown(filename, low2highuid(user), low2highgid(group));
}

SYSCALL_DEFINE3(lchown16, const char __user, filename, old_uid_t, user, old_gid_t, group)
{
	return sys_lchown(filename, low2highuid(user), low2highgid(group));
}

SYSCALL_DEFINE3(fchown16, unsigned int, fd, old_uid_t, user, old_gid_t, group)
{
	return sys_fchown(fd, low2highuid(user), low2highgid(group));
}

SYSCALL_DEFINE2(setregid16, old_gid_t, rgid, old_gid_t, egid)
{
	return sys_setregid(low2highgid(rgid), low2highgid(egid));
}

SYSCALL_DEFINE1(setgid16, old_gid_t, gid)
{
	return sys_setgid(low2highgid(gid));
}

SYSCALL_DEFINE2(setreuid16, old_uid_t, ruid, old_uid_t, euid)
{
	return sys_setreuid(low2highuid(ruid), low2highuid(euid));
}

SYSCALL_DEFINE1(setuid16, old_uid_t, uid)
{
	return sys_setuid(low2highuid(uid));
}

SYSCALL_DEFINE3(setresuid16, old_uid_t, ruid, old_uid_t, euid, old_uid_t, suid)
{
	return sys_setresuid(low2highuid(ruid), low2highuid(euid),
				 low2highuid(suid));
}

SYSCALL_DEFINE3(getresuid16, old_uid_t __user, ruidp, old_uid_t __user, euidp, old_uid_t __user, suidp)
{
	const struct credcred = current_cred();
	int retval;
	old_uid_t ruid, euid, suid;

	ruid = high2lowuid(from_kuid_munged(cred->user_ns, cred->uid));
	euid = high2lowuid(from_kuid_munged(cred->user_ns, cred->euid));
	suid = high2lowuid(from_kuid_munged(cred->user_ns, cred->suid));

	if (!(retval   = put_user(ruid, ruidp)) &&
	    !(retval   = put_user(euid, euidp)))
		retval = put_user(suid, suidp);

	return retval;
}

SYSCALL_DEFINE3(setresgid16, old_gid_t, rgid, old_gid_t, egid, old_gid_t, sgid)
{
	return sys_setresgid(low2highgid(rgid), low2highgid(egid),
				 low2highgid(sgid));
}


SYSCALL_DEFINE3(getresgid16, old_gid_t __user, rgidp, old_gid_t __user, egidp, old_gid_t __user, sgidp)
{
	const struct credcred = current_cred();
	int retval;
	old_gid_t rgid, egid, sgid;

	rgid = high2lowgid(from_kgid_munged(cred->user_ns, cred->gid));
	egid = high2lowgid(from_kgid_munged(cred->user_ns, cred->egid));
	sgid = high2lowgid(from_kgid_munged(cred->user_ns, cred->sgid));

	if (!(retval   = put_user(rgid, rgidp)) &&
	    !(retval   = put_user(egid, egidp)))
		retval = put_user(sgid, sgidp);

	return retval;
}

SYSCALL_DEFINE1(setfsuid16, old_uid_t, uid)
{
	return sys_setfsuid(low2highuid(uid));
}

SYSCALL_DEFINE1(setfsgid16, old_gid_t, gid)
{
	return sys_setfsgid(low2highgid(gid));
}

static int groups16_to_user(old_gid_t __usergrouplist,
    struct group_infogroup_info)
{
	struct user_namespaceuser_ns = current_user_ns();
	int i;
	old_gid_t group;
	kgid_t kgid;

	for (i = 0; i < group_info->ngroups; i++) {
		kgid = GROUP_AT(group_info, i);
		group = high2lowgid(from_kgid_munged(user_ns, kgid));
		if (put_user(group, grouplist+i))
			return -EFAULT;
	}

	return 0;
}

static int groups16_from_user(struct group_infogroup_info,
    old_gid_t __usergrouplist)
{
	struct user_namespaceuser_ns = current_user_ns();
	int i;
	old_gid_t group;
	kgid_t kgid;

	for (i = 0; i < group_info->ngroups; i++) {
		if (get_user(group, grouplist+i))
			return  -EFAULT;

		kgid = make_kgid(user_ns, low2highgid(group));
		if (!gid_valid(kgid))
			return -EINVAL;

		GROUP_AT(group_info, i) = kgid;
	}

	return 0;
}

SYSCALL_DEFINE2(getgroups16, int, gidsetsize, old_gid_t __user, grouplist)
{
	const struct credcred = current_cred();
	int i;

	if (gidsetsize < 0)
		return -EINVAL;

	i = cred->group_info->ngroups;
	if (gidsetsize) {
		if (i > gidsetsize) {
			i = -EINVAL;
			goto out;
		}
		if (groups16_to_user(grouplist, cred->group_info)) {
			i = -EFAULT;
			goto out;
		}
	}
out:
	return i;
}

SYSCALL_DEFINE2(setgroups16, int, gidsetsize, old_gid_t __user, grouplist)
{
	struct group_infogroup_info;
	int retval;

	if (!may_setgroups())
		return -EPERM;
	if ((unsigned)gidsetsize > NGROUPS_MAX)
		return -EINVAL;

	group_info = groups_alloc(gidsetsize);
	if (!group_info)
		return -ENOMEM;
	retval = groups16_from_user(group_info, grouplist);
	if (retval) {
		put_group_info(group_info);
		return retval;
	}

	retval = set_current_groups(group_info);
	put_group_info(group_info);

	return retval;
}

SYSCALL_DEFINE0(getuid16)
{
	return high2lowuid(from_kuid_munged(current_user_ns(), current_uid()));
}

SYSCALL_DEFINE0(geteuid16)
{
	return high2lowuid(from_kuid_munged(current_user_ns(), current_euid()));
}

SYSCALL_DEFINE0(getgid16)
{
	return high2lowgid(from_kgid_munged(current_user_ns(), current_gid()));
}

SYSCALL_DEFINE0(getegid16)
{
	return high2lowgid(from_kgid_munged(current_user_ns(), current_egid()));
}
*/

 Uniprocessor-only support functions.  The counterpart to kernel/smp.c
 /*

#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/smp.h>

int smp_call_function_single(int cpu, void (*func) (voidinfo), voidinfo,
				int wait)
{
	unsigned long flags;

	WARN_ON(cpu != 0);

	local_irq_save(flags);
	func(info);
	local_irq_restore(flags);

	return 0;
}
EXPORT_SYMBOL(smp_call_function_single);

int smp_call_function_single_async(int cpu, struct call_single_datacsd)
{
	unsigned long flags;

	local_irq_save(flags);
	csd->func(csd->info);
	local_irq_restore(flags);
	return 0;
}
EXPORT_SYMBOL(smp_call_function_single_async);

int on_each_cpu(smp_call_func_t func, voidinfo, int wait)
{
	unsigned long flags;

	local_irq_save(flags);
	func(info);
	local_irq_restore(flags);
	return 0;
}
EXPORT_SYMBOL(on_each_cpu);

*/
 Note we still need to test the mask even for UP
 because we actually can get an empty mask from
 code that on SMP might call us without the local
 CPU in the mask.
 /*
void on_each_cpu_mask(const struct cpumaskmask,
		      smp_call_func_t func, voidinfo, bool wait)
{
	unsigned long flags;

	if (cpumask_test_cpu(0, mask)) {
		local_irq_save(flags);
		func(info);
		local_irq_restore(flags);
	}
}
EXPORT_SYMBOL(on_each_cpu_mask);

*/
 Preemption is disabled here to make sure the cond_func is called under the
 same condtions in UP and SMP.
 /*
void on_each_cpu_cond(bool (*cond_func)(int cpu, voidinfo),
		      smp_call_func_t func, voidinfo, bool wait,
		      gfp_t gfp_flags)
{
	unsigned long flags;

	preempt_disable();
	if (cond_func(0, info)) {
		local_irq_save(flags);
		func(info);
		local_irq_restore(flags);
	}
	preempt_enable();
}
EXPORT_SYMBOL(on_each_cpu_cond);
*/

 The "user cache".

 (C) Copyright 1991-2000 Linus Torvalds

 We have a per-user structure to keep track of how many
 processes, files etc the user has claimed, in order to be
 able to have per-user limits for system resources. 
 /*

#include <linux/init.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/bitops.h>
#include <linux/key.h>
#include <linux/interrupt.h>
#include <linux/export.h>
#include <linux/user_namespace.h>
#include <linux/proc_ns.h>

*/
 userns count is 1 for root user, 1 for init_uts_ns,
 and 1 for... ?
 /*
struct user_namespace init_user_ns = {
	.uid_map = {
		.nr_extents = 1,
		.extent[0] = {
			.first = 0,
			.lower_first = 0,
			.count = 4294967295U,
		},
	},
	.gid_map = {
		.nr_extents = 1,
		.extent[0] = {
			.first = 0,
			.lower_first = 0,
			.count = 4294967295U,
		},
	},
	.projid_map = {
		.nr_extents = 1,
		.extent[0] = {
			.first = 0,
			.lower_first = 0,
			.count = 4294967295U,
		},
	},
	.count = ATOMIC_INIT(3),
	.owner = GLOBAL_ROOT_UID,
	.group = GLOBAL_ROOT_GID,
	.ns.inum = PROC_USER_INIT_INO,
#ifdef CONFIG_USER_NS
	.ns.ops = &userns_operations,
#endif
	.flags = USERNS_INIT_FLAGS,
#ifdef CONFIG_PERSISTENT_KEYRINGS
	.persistent_keyring_register_sem =
	__RWSEM_INITIALIZER(init_user_ns.persistent_keyring_register_sem),
#endif
};
EXPORT_SYMBOL_GPL(init_user_ns);

*/
 UID task count cache, to get fast user lookup in "alloc_uid"
 when changing user ID's (ie setuid() and friends).
 /*

#define UIDHASH_BITS	(CONFIG_BASE_SMALL ? 3 : 7)
#define UIDHASH_SZ	(1 << UIDHASH_BITS)
#define UIDHASH_MASK		(UIDHASH_SZ - 1)
#define __uidhashfn(uid)	(((uid >> UIDHASH_BITS) + uid) & UIDHASH_MASK)
#define uidhashentry(uid)	(uidhash_table + __uidhashfn((__kuid_val(uid))))

static struct kmem_cacheuid_cachep;
struct hlist_head uidhash_table[UIDHASH_SZ];

*/
 The uidhash_lock is mostly taken from process context, but it is
 occasionally also taken from softirq/tasklet context, when
 task-structs get RCU-freed. Hence all locking must be softirq-safe.
 But free_uid() is also called with local interrupts disabled, and running
 local_bh_enable() with local interrupts disabled is an error - we'll run
 softirq callbacks, and they can unconditionally enable interrupts, and
 the caller of free_uid() didn't expect that..
 /*
static DEFINE_SPINLOCK(uidhash_lock);

*/ root_user.__count is 1, for init task cred /*
struct user_struct root_user = {
	.__count	= ATOMIC_INIT(1),
	.processes	= ATOMIC_INIT(1),
	.sigpending	= ATOMIC_INIT(0),
	.locked_shm     = 0,
	.uid		= GLOBAL_ROOT_UID,
};

*/
 These routines must be called with the uidhash spinlock held!
 /*
static void uid_hash_insert(struct user_structup, struct hlist_headhashent)
{
	hlist_add_head(&up->uidhash_node, hashent);
}

static void uid_hash_remove(struct user_structup)
{
	hlist_del_init(&up->uidhash_node);
}

static struct user_structuid_hash_find(kuid_t uid, struct hlist_headhashent)
{
	struct user_structuser;

	hlist_for_each_entry(user, hashent, uidhash_node) {
		if (uid_eq(user->uid, uid)) {
			atomic_inc(&user->__count);
			return user;
		}
	}

	return NULL;
}

*/ IRQs are disabled and uidhash_lock is held upon function entry.
 IRQ state (as stored in flags) is restored and uidhash_lock released
 upon function exit.
 /*
static void free_user(struct user_structup, unsigned long flags)
	__releases(&uidhash_lock)
{
	uid_hash_remove(up);
	spin_unlock_irqrestore(&uidhash_lock, flags);
	key_put(up->uid_keyring);
	key_put(up->session_keyring);
	kmem_cache_free(uid_cachep, up);
}

*/
 Locate the user_struct for the passed UID.  If found, take a ref on it.  The
 caller must undo that ref with free_uid().

 If the user_struct could not be found, return NULL.
 /*
struct user_structfind_user(kuid_t uid)
{
	struct user_structret;
	unsigned long flags;

	spin_lock_irqsave(&uidhash_lock, flags);
	ret = uid_hash_find(uid, uidhashentry(uid));
	spin_unlock_irqrestore(&uidhash_lock, flags);
	return ret;
}

void free_uid(struct user_structup)
{
	unsigned long flags;

	if (!up)
		return;

	local_irq_save(flags);
	if (atomic_dec_and_lock(&up->__count, &uidhash_lock))
		free_user(up, flags);
	else
		local_irq_restore(flags);
}

struct user_structalloc_uid(kuid_t uid)
{
	struct hlist_headhashent = uidhashentry(uid);
	struct user_structup,new;

	spin_lock_irq(&uidhash_lock);
	up = uid_hash_find(uid, hashent);
	spin_unlock_irq(&uidhash_lock);

	if (!up) {
		new = kmem_cache_zalloc(uid_cachep, GFP_KERNEL);
		if (!new)
			goto out_unlock;

		new->uid = uid;
		atomic_set(&new->__count, 1);

		*/
		 Before adding this, check whether we raced
		 on adding the same user already..
		 /*
		spin_lock_irq(&uidhash_lock);
		up = uid_hash_find(uid, hashent);
		if (up) {
			key_put(new->uid_keyring);
			key_put(new->session_keyring);
			kmem_cache_free(uid_cachep, new);
		} else {
			uid_hash_insert(new, hashent);
			up = new;
		}
		spin_unlock_irq(&uidhash_lock);
	}

	return up;

out_unlock:
	return NULL;
}

static int __init uid_cache_init(void)
{
	int n;

	uid_cachep = kmem_cache_create("uid_cache", sizeof(struct user_struct),
			0, SLAB_HWCACHE_ALIGN|SLAB_PANIC, NULL);

	for(n = 0; n < UIDHASH_SZ; ++n)
		INIT_HLIST_HEAD(uidhash_table + n);

	*/ Insert the root user immediately (init already runs as root) /*
	spin_lock_irq(&uidhash_lock);
	uid_hash_insert(&root_user, uidhashentry(GLOBAL_ROOT_UID));
	spin_unlock_irq(&uidhash_lock);

	return 0;
}
subsys_initcall(uid_cache_init);
*/

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, version 2 of the
  License.
 /*

#include <linux/export.h>
#include <linux/nsproxy.h>
#include <linux/slab.h>
#include <linux/user_namespace.h>
#include <linux/proc_ns.h>
#include <linux/highuid.h>
#include <linux/cred.h>
#include <linux/securebits.h>
#include <linux/keyctl.h>
#include <linux/key-type.h>
#include <keys/user-type.h>
#include <linux/seq_file.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/ctype.h>
#include <linux/projid.h>
#include <linux/fs_struct.h>

static struct kmem_cacheuser_ns_cachep __read_mostly;
static DEFINE_MUTEX(userns_state_mutex);

static bool new_idmap_permitted(const struct filefile,
				struct user_namespacens, int cap_setid,
				struct uid_gid_mapmap);

static void set_cred_user_ns(struct credcred, struct user_namespaceuser_ns)
{
	*/ Start with the same capabilities as init but useless for doing
	 anything as the capabilities are bound to the new user namespace.
	 /*
	cred->securebits = SECUREBITS_DEFAULT;
	cred->cap_inheritable = CAP_EMPTY_SET;
	cred->cap_permitted = CAP_FULL_SET;
	cred->cap_effective = CAP_FULL_SET;
	cred->cap_ambient = CAP_EMPTY_SET;
	cred->cap_bset = CAP_FULL_SET;
#ifdef CONFIG_KEYS
	key_put(cred->request_key_auth);
	cred->request_key_auth = NULL;
#endif
	*/ tgcred will be cleared in our caller bc CLONE_THREAD won't be set /*
	cred->user_ns = user_ns;
}

*/
 Create a new user namespace, deriving the creator from the user in the
 passed credentials, and replacing that user with the new root user for the
 new namespace.

 This is called by copy_creds(), which will finish setting the target task's
 credentials.
 /*
int create_user_ns(struct crednew)
{
	struct user_namespacens,parent_ns = new->user_ns;
	kuid_t owner = new->euid;
	kgid_t group = new->egid;
	int ret;

	if (parent_ns->level > 32)
		return -EUSERS;

	*/
	 Verify that we can not violate the policy of which files
	 may be accessed that is specified by the root directory,
	 by verifing that the root directory is at the root of the
	 mount namespace which allows all files to be accessed.
	 /*
	if (current_chrooted())
		return -EPERM;

	*/ The creator needs a mapping in the parent user namespace
	 or else we won't be able to reasonably tell userspace who
	 created a user_namespace.
	 /*
	if (!kuid_has_mapping(parent_ns, owner) ||
	    !kgid_has_mapping(parent_ns, group))
		return -EPERM;

	ns = kmem_cache_zalloc(user_ns_cachep, GFP_KERNEL);
	if (!ns)
		return -ENOMEM;

	ret = ns_alloc_inum(&ns->ns);
	if (ret) {
		kmem_cache_free(user_ns_cachep, ns);
		return ret;
	}
	ns->ns.ops = &userns_operations;

	atomic_set(&ns->count, 1);
	*/ Leave the new->user_ns reference with the new user namespace. /*
	ns->parent = parent_ns;
	ns->level = parent_ns->level + 1;
	ns->owner = owner;
	ns->group = group;

	*/ Inherit USERNS_SETGROUPS_ALLOWED from our parent /*
	mutex_lock(&userns_state_mutex);
	ns->flags = parent_ns->flags;
	mutex_unlock(&userns_state_mutex);

	set_cred_user_ns(new, ns);

#ifdef CONFIG_PERSISTENT_KEYRINGS
	init_rwsem(&ns->persistent_keyring_register_sem);
#endif
	return 0;
}

int unshare_userns(unsigned long unshare_flags, struct cred*new_cred)
{
	struct credcred;
	int err = -ENOMEM;

	if (!(unshare_flags & CLONE_NEWUSER))
		return 0;

	cred = prepare_creds();
	if (cred) {
		err = create_user_ns(cred);
		if (err)
			put_cred(cred);
		else
			*new_cred = cred;
	}

	return err;
}

void free_user_ns(struct user_namespacens)
{
	struct user_namespaceparent;

	do {
		parent = ns->parent;
#ifdef CONFIG_PERSISTENT_KEYRINGS
		key_put(ns->persistent_keyring_register);
#endif
		ns_free_inum(&ns->ns);
		kmem_cache_free(user_ns_cachep, ns);
		ns = parent;
	} while (atomic_dec_and_test(&parent->count));
}
EXPORT_SYMBOL(free_user_ns);

static u32 map_id_range_down(struct uid_gid_mapmap, u32 id, u32 count)
{
	unsigned idx, extents;
	u32 first, last, id2;

	id2 = id + count - 1;

	*/ Find the matching extent /*
	extents = map->nr_extents;
	smp_rmb();
	for (idx = 0; idx < extents; idx++) {
		first = map->extent[idx].first;
		last = first + map->extent[idx].count - 1;
		if (id >= first && id <= last &&
		    (id2 >= first && id2 <= last))
			break;
	}
	*/ Map the id or note failure /*
	if (idx < extents)
		id = (id - first) + map->extent[idx].lower_first;
	else
		id = (u32) -1;

	return id;
}

static u32 map_id_down(struct uid_gid_mapmap, u32 id)
{
	unsigned idx, extents;
	u32 first, last;

	*/ Find the matching extent /*
	extents = map->nr_extents;
	smp_rmb();
	for (idx = 0; idx < extents; idx++) {
		first = map->extent[idx].first;
		last = first + map->extent[idx].count - 1;
		if (id >= first && id <= last)
			break;
	}
	*/ Map the id or note failure /*
	if (idx < extents)
		id = (id - first) + map->extent[idx].lower_first;
	else
		id = (u32) -1;

	return id;
}

static u32 map_id_up(struct uid_gid_mapmap, u32 id)
{
	unsigned idx, extents;
	u32 first, last;

	*/ Find the matching extent /*
	extents = map->nr_extents;
	smp_rmb();
	for (idx = 0; idx < extents; idx++) {
		first = map->extent[idx].lower_first;
		last = first + map->extent[idx].count - 1;
		if (id >= first && id <= last)
			break;
	}
	*/ Map the id or note failure /*
	if (idx < extents)
		id = (id - first) + map->extent[idx].first;
	else
		id = (u32) -1;

	return id;
}

*/
	make_kuid - Map a user-namespace uid pair into a kuid.
	@ns:  User namespace that the uid is in
	@uid: User identifier

	Maps a user-namespace uid pair into a kernel internal kuid,
	and returns that kuid.

	When there is no mapping defined for the user-namespace uid
	pair INVALID_UID is returned.  Callers are expected to test
	for and handle INVALID_UID being returned.  INVALID_UID
	may be tested for using uid_valid().
 /*
kuid_t make_kuid(struct user_namespacens, uid_t uid)
{
	*/ Map the uid to a global kernel uid /*
	return KUIDT_INIT(map_id_down(&ns->uid_map, uid));
}
EXPORT_SYMBOL(make_kuid);

*/
	from_kuid - Create a uid from a kuid user-namespace pair.
	@targ: The user namespace we want a uid in.
	@kuid: The kernel internal uid to start with.

	Map @kuid into the user-namespace specified by @targ and
	return the resulting uid.

	There is always a mapping into the initial user_namespace.

	If @kuid has no mapping in @targ (uid_t)-1 is returned.
 /*
uid_t from_kuid(struct user_namespacetarg, kuid_t kuid)
{
	*/ Map the uid from a global kernel uid /*
	return map_id_up(&targ->uid_map, __kuid_val(kuid));
}
EXPORT_SYMBOL(from_kuid);

*/
	from_kuid_munged - Create a uid from a kuid user-namespace pair.
	@targ: The user namespace we want a uid in.
	@kuid: The kernel internal uid to start with.

	Map @kuid into the user-namespace specified by @targ and
	return the resulting uid.

	There is always a mapping into the initial user_namespace.

	Unlike from_kuid from_kuid_munged never fails and always
	returns a valid uid.  This makes from_kuid_munged appropriate
	for use in syscalls like stat and getuid where failing the
	system call and failing to provide a valid uid are not an
	options.

	If @kuid has no mapping in @targ overflowuid is returned.
 /*
uid_t from_kuid_munged(struct user_namespacetarg, kuid_t kuid)
{
	uid_t uid;
	uid = from_kuid(targ, kuid);

	if (uid == (uid_t) -1)
		uid = overflowuid;
	return uid;
}
EXPORT_SYMBOL(from_kuid_munged);

*/
	make_kgid - Map a user-namespace gid pair into a kgid.
	@ns:  User namespace that the gid is in
	@gid: group identifier

	Maps a user-namespace gid pair into a kernel internal kgid,
	and returns that kgid.

	When there is no mapping defined for the user-namespace gid
	pair INVALID_GID is returned.  Callers are expected to test
	for and handle INVALID_GID being returned.  INVALID_GID may be
	tested for using gid_valid().
 /*
kgid_t make_kgid(struct user_namespacens, gid_t gid)
{
	*/ Map the gid to a global kernel gid /*
	return KGIDT_INIT(map_id_down(&ns->gid_map, gid));
}
EXPORT_SYMBOL(make_kgid);

*/
	from_kgid - Create a gid from a kgid user-namespace pair.
	@targ: The user namespace we want a gid in.
	@kgid: The kernel internal gid to start with.

	Map @kgid into the user-namespace specified by @targ and
	return the resulting gid.

	There is always a mapping into the initial user_namespace.

	If @kgid has no mapping in @targ (gid_t)-1 is returned.
 /*
gid_t from_kgid(struct user_namespacetarg, kgid_t kgid)
{
	*/ Map the gid from a global kernel gid /*
	return map_id_up(&targ->gid_map, __kgid_val(kgid));
}
EXPORT_SYMBOL(from_kgid);

*/
	from_kgid_munged - Create a gid from a kgid user-namespace pair.
	@targ: The user namespace we want a gid in.
	@kgid: The kernel internal gid to start with.

	Map @kgid into the user-namespace specified by @targ and
	return the resulting gid.

	There is always a mapping into the initial user_namespace.

	Unlike from_kgid from_kgid_munged never fails and always
	returns a valid gid.  This makes from_kgid_munged appropriate
	for use in syscalls like stat and getgid where failing the
	system call and failing to provide a valid gid are not options.

	If @kgid has no mapping in @targ overflowgid is returned.
 /*
gid_t from_kgid_munged(struct user_namespacetarg, kgid_t kgid)
{
	gid_t gid;
	gid = from_kgid(targ, kgid);

	if (gid == (gid_t) -1)
		gid = overflowgid;
	return gid;
}
EXPORT_SYMBOL(from_kgid_munged);

*/
	make_kprojid - Map a user-namespace projid pair into a kprojid.
	@ns:  User namespace that the projid is in
	@projid: Project identifier

	Maps a user-namespace uid pair into a kernel internal kuid,
	and returns that kuid.

	When there is no mapping defined for the user-namespace projid
	pair INVALID_PROJID is returned.  Callers are expected to test
	for and handle handle INVALID_PROJID being returned.  INVALID_PROJID
	may be tested for using projid_valid().
 /*
kprojid_t make_kprojid(struct user_namespacens, projid_t projid)
{
	*/ Map the uid to a global kernel uid /*
	return KPROJIDT_INIT(map_id_down(&ns->projid_map, projid));
}
EXPORT_SYMBOL(make_kprojid);

*/
	from_kprojid - Create a projid from a kprojid user-namespace pair.
	@targ: The user namespace we want a projid in.
	@kprojid: The kernel internal project identifier to start with.

	Map @kprojid into the user-namespace specified by @targ and
	return the resulting projid.

	There is always a mapping into the initial user_namespace.

	If @kprojid has no mapping in @targ (projid_t)-1 is returned.
 /*
projid_t from_kprojid(struct user_namespacetarg, kprojid_t kprojid)
{
	*/ Map the uid from a global kernel uid /*
	return map_id_up(&targ->projid_map, __kprojid_val(kprojid));
}
EXPORT_SYMBOL(from_kprojid);

*/
	from_kprojid_munged - Create a projiid from a kprojid user-namespace pair.
	@targ: The user namespace we want a projid in.
	@kprojid: The kernel internal projid to start with.

	Map @kprojid into the user-namespace specified by @targ and
	return the resulting projid.

	There is always a mapping into the initial user_namespace.

	Unlike from_kprojid from_kprojid_munged never fails and always
	returns a valid projid.  This makes from_kprojid_munged
	appropriate for use in syscalls like stat and where
	failing the system call and failing to provide a valid projid are
	not an options.

	If @kprojid has no mapping in @targ OVERFLOW_PROJID is returned.
 /*
projid_t from_kprojid_munged(struct user_namespacetarg, kprojid_t kprojid)
{
	projid_t projid;
	projid = from_kprojid(targ, kprojid);

	if (projid == (projid_t) -1)
		projid = OVERFLOW_PROJID;
	return projid;
}
EXPORT_SYMBOL(from_kprojid_munged);


static int uid_m_show(struct seq_fileseq, voidv)
{
	struct user_namespacens = seq->private;
	struct uid_gid_extentextent = v;
	struct user_namespacelower_ns;
	uid_t lower;

	lower_ns = seq_user_ns(seq);
	if ((lower_ns == ns) && lower_ns->parent)
		lower_ns = lower_ns->parent;

	lower = from_kuid(lower_ns, KUIDT_INIT(extent->lower_first));

	seq_printf(seq, "%10u %10u %10u\n",
		extent->first,
		lower,
		extent->count);

	return 0;
}

static int gid_m_show(struct seq_fileseq, voidv)
{
	struct user_namespacens = seq->private;
	struct uid_gid_extentextent = v;
	struct user_namespacelower_ns;
	gid_t lower;

	lower_ns = seq_user_ns(seq);
	if ((lower_ns == ns) && lower_ns->parent)
		lower_ns = lower_ns->parent;

	lower = from_kgid(lower_ns, KGIDT_INIT(extent->lower_first));

	seq_printf(seq, "%10u %10u %10u\n",
		extent->first,
		lower,
		extent->count);

	return 0;
}

static int projid_m_show(struct seq_fileseq, voidv)
{
	struct user_namespacens = seq->private;
	struct uid_gid_extentextent = v;
	struct user_namespacelower_ns;
	projid_t lower;

	lower_ns = seq_user_ns(seq);
	if ((lower_ns == ns) && lower_ns->parent)
		lower_ns = lower_ns->parent;

	lower = from_kprojid(lower_ns, KPROJIDT_INIT(extent->lower_first));

	seq_printf(seq, "%10u %10u %10u\n",
		extent->first,
		lower,
		extent->count);

	return 0;
}

static voidm_start(struct seq_fileseq, loff_tppos,
		     struct uid_gid_mapmap)
{
	struct uid_gid_extentextent = NULL;
	loff_t pos =ppos;

	if (pos < map->nr_extents)
		extent = &map->extent[pos];

	return extent;
}

static voiduid_m_start(struct seq_fileseq, loff_tppos)
{
	struct user_namespacens = seq->private;

	return m_start(seq, ppos, &ns->uid_map);
}

static voidgid_m_start(struct seq_fileseq, loff_tppos)
{
	struct user_namespacens = seq->private;

	return m_start(seq, ppos, &ns->gid_map);
}

static voidprojid_m_start(struct seq_fileseq, loff_tppos)
{
	struct user_namespacens = seq->private;

	return m_start(seq, ppos, &ns->projid_map);
}

static voidm_next(struct seq_fileseq, voidv, loff_tpos)
{
	(*pos)++;
	return seq->op->start(seq, pos);
}

static void m_stop(struct seq_fileseq, voidv)
{
	return;
}

const struct seq_operations proc_uid_seq_operations = {
	.start = uid_m_start,
	.stop = m_stop,
	.next = m_next,
	.show = uid_m_show,
};

const struct seq_operations proc_gid_seq_operations = {
	.start = gid_m_start,
	.stop = m_stop,
	.next = m_next,
	.show = gid_m_show,
};

const struct seq_operations proc_projid_seq_operations = {
	.start = projid_m_start,
	.stop = m_stop,
	.next = m_next,
	.show = projid_m_show,
};

static bool mappings_overlap(struct uid_gid_mapnew_map,
			     struct uid_gid_extentextent)
{
	u32 upper_first, lower_first, upper_last, lower_last;
	unsigned idx;

	upper_first = extent->first;
	lower_first = extent->lower_first;
	upper_last = upper_first + extent->count - 1;
	lower_last = lower_first + extent->count - 1;

	for (idx = 0; idx < new_map->nr_extents; idx++) {
		u32 prev_upper_first, prev_lower_first;
		u32 prev_upper_last, prev_lower_last;
		struct uid_gid_extentprev;

		prev = &new_map->extent[idx];

		prev_upper_first = prev->first;
		prev_lower_first = prev->lower_first;
		prev_upper_last = prev_upper_first + prev->count - 1;
		prev_lower_last = prev_lower_first + prev->count - 1;

		*/ Does the upper range intersect a previous extent? /*
		if ((prev_upper_first <= upper_last) &&
		    (prev_upper_last >= upper_first))
			return true;

		*/ Does the lower range intersect a previous extent? /*
		if ((prev_lower_first <= lower_last) &&
		    (prev_lower_last >= lower_first))
			return true;
	}
	return false;
}

static ssize_t map_write(struct filefile, const char __userbuf,
			 size_t count, loff_tppos,
			 int cap_setid,
			 struct uid_gid_mapmap,
			 struct uid_gid_mapparent_map)
{
	struct seq_fileseq = file->private_data;
	struct user_namespacens = seq->private;
	struct uid_gid_map new_map;
	unsigned idx;
	struct uid_gid_extentextent = NULL;
	charkbuf = NULL,pos,next_line;
	ssize_t ret = -EINVAL;

	*/
	 The userns_state_mutex serializes all writes to any given map.
	
	 Any map is only ever written once.
	
	 An id map fits within 1 cache line on most architectures.
	
	 On read nothing needs to be done unless you are on an
	 architecture with a crazy cache coherency model like alpha.
	
	 There is a one time data dependency between reading the
	 count of the extents and the values of the extents.  The
	 desired behavior is to see the values of the extents that
	 were written before the count of the extents.
	
	 To achieve this smp_wmb() is used on guarantee the write
	 order and smp_rmb() is guaranteed that we don't have crazy
	 architectures returning stale data.
	 /*
	mutex_lock(&userns_state_mutex);

	ret = -EPERM;
	*/ Only allow one successful write to the map /*
	if (map->nr_extents != 0)
		goto out;

	*/
	 Adjusting namespace settings requires capabilities on the target.
	 /*
	if (cap_valid(cap_setid) && !file_ns_capable(file, ns, CAP_SYS_ADMIN))
		goto out;

	*/ Only allow < page size writes at the beginning of the file /*
	ret = -EINVAL;
	if ((*ppos != 0) || (count >= PAGE_SIZE))
		goto out;

	*/ Slurp in the user data /*
	kbuf = memdup_user_nul(buf, count);
	if (IS_ERR(kbuf)) {
		ret = PTR_ERR(kbuf);
		kbuf = NULL;
		goto out;
	}

	*/ Parse the user data /*
	ret = -EINVAL;
	pos = kbuf;
	new_map.nr_extents = 0;
	for (; pos; pos = next_line) {
		extent = &new_map.extent[new_map.nr_extents];

		*/ Find the end of line and ensure I don't look past it /*
		next_line = strchr(pos, '\n');
		if (next_line) {
			*next_line = '\0';
			next_line++;
			if (*next_line == '\0')
				next_line = NULL;
		}

		pos = skip_spaces(pos);
		extent->first = simple_strtoul(pos, &pos, 10);
		if (!isspace(*pos))
			goto out;

		pos = skip_spaces(pos);
		extent->lower_first = simple_strtoul(pos, &pos, 10);
		if (!isspace(*pos))
			goto out;

		pos = skip_spaces(pos);
		extent->count = simple_strtoul(pos, &pos, 10);
		if (*pos && !isspace(*pos))
			goto out;

		*/ Verify there is not trailing junk on the line /*
		pos = skip_spaces(pos);
		if (*pos != '\0')
			goto out;

		*/ Verify we have been given valid starting values /*
		if ((extent->first == (u32) -1) ||
		    (extent->lower_first == (u32) -1))
			goto out;

		*/ Verify count is not zero and does not cause the
		 extent to wrap
		 /*
		if ((extent->first + extent->count) <= extent->first)
			goto out;
		if ((extent->lower_first + extent->count) <=
		     extent->lower_first)
			goto out;

		*/ Do the ranges in extent overlap any previous extents? /*
		if (mappings_overlap(&new_map, extent))
			goto out;

		new_map.nr_extents++;

		*/ Fail if the file contains too many extents /*
		if ((new_map.nr_extents == UID_GID_MAP_MAX_EXTENTS) &&
		    (next_line != NULL))
			goto out;
	}
	*/ Be very certaint the new map actually exists /*
	if (new_map.nr_extents == 0)
		goto out;

	ret = -EPERM;
	*/ Validate the user is allowed to use user id's mapped to. /*
	if (!new_idmap_permitted(file, ns, cap_setid, &new_map))
		goto out;

	*/ Map the lower ids from the parent user namespace to the
	 kernel global id space.
	 /*
	for (idx = 0; idx < new_map.nr_extents; idx++) {
		u32 lower_first;
		extent = &new_map.extent[idx];

		lower_first = map_id_range_down(parent_map,
						extent->lower_first,
						extent->count);

		*/ Fail if we can not map the specified extent to
		 the kernel global id space.
		 /*
		if (lower_first == (u32) -1)
			goto out;

		extent->lower_first = lower_first;
	}

	*/ Install the map /*
	memcpy(map->extent, new_map.extent,
		new_map.nr_extents*sizeof(new_map.extent[0]));
	smp_wmb();
	map->nr_extents = new_map.nr_extents;

	*ppos = count;
	ret = count;
out:
	mutex_unlock(&userns_state_mutex);
	kfree(kbuf);
	return ret;
}

ssize_t proc_uid_map_write(struct filefile, const char __userbuf,
			   size_t size, loff_tppos)
{
	struct seq_fileseq = file->private_data;
	struct user_namespacens = seq->private;
	struct user_namespaceseq_ns = seq_user_ns(seq);

	if (!ns->parent)
		return -EPERM;

	if ((seq_ns != ns) && (seq_ns != ns->parent))
		return -EPERM;

	return map_write(file, buf, size, ppos, CAP_SETUID,
			 &ns->uid_map, &ns->parent->uid_map);
}

ssize_t proc_gid_map_write(struct filefile, const char __userbuf,
			   size_t size, loff_tppos)
{
	struct seq_fileseq = file->private_data;
	struct user_namespacens = seq->private;
	struct user_namespaceseq_ns = seq_user_ns(seq);

	if (!ns->parent)
		return -EPERM;

	if ((seq_ns != ns) && (seq_ns != ns->parent))
		return -EPERM;

	return map_write(file, buf, size, ppos, CAP_SETGID,
			 &ns->gid_map, &ns->parent->gid_map);
}

ssize_t proc_projid_map_write(struct filefile, const char __userbuf,
			      size_t size, loff_tppos)
{
	struct seq_fileseq = file->private_data;
	struct user_namespacens = seq->private;
	struct user_namespaceseq_ns = seq_user_ns(seq);

	if (!ns->parent)
		return -EPERM;

	if ((seq_ns != ns) && (seq_ns != ns->parent))
		return -EPERM;

	*/ Anyone can set any valid project id no capability needed /*
	return map_write(file, buf, size, ppos, -1,
			 &ns->projid_map, &ns->parent->projid_map);
}

static bool new_idmap_permitted(const struct filefile,
				struct user_namespacens, int cap_setid,
				struct uid_gid_mapnew_map)
{
	const struct credcred = file->f_cred;
	*/ Don't allow mappings that would allow anything that wouldn't
	 be allowed without the establishment of unprivileged mappings.
	 /*
	if ((new_map->nr_extents == 1) && (new_map->extent[0].count == 1) &&
	    uid_eq(ns->owner, cred->euid)) {
		u32 id = new_map->extent[0].lower_first;
		if (cap_setid == CAP_SETUID) {
			kuid_t uid = make_kuid(ns->parent, id);
			if (uid_eq(uid, cred->euid))
				return true;
		} else if (cap_setid == CAP_SETGID) {
			kgid_t gid = make_kgid(ns->parent, id);
			if (!(ns->flags & USERNS_SETGROUPS_ALLOWED) &&
			    gid_eq(gid, cred->egid))
				return true;
		}
	}

	*/ Allow anyone to set a mapping that doesn't require privilege /*
	if (!cap_valid(cap_setid))
		return true;

	*/ Allow the specified ids if we have the appropriate capability
	 (CAP_SETUID or CAP_SETGID) over the parent user namespace.
	 And the opener of the id file also had the approprpiate capability.
	 /*
	if (ns_capable(ns->parent, cap_setid) &&
	    file_ns_capable(file, ns->parent, cap_setid))
		return true;

	return false;
}

int proc_setgroups_show(struct seq_fileseq, voidv)
{
	struct user_namespacens = seq->private;
	unsigned long userns_flags = ACCESS_ONCE(ns->flags);

	seq_printf(seq, "%s\n",
		   (userns_flags & USERNS_SETGROUPS_ALLOWED) ?
		   "allow" : "deny");
	return 0;
}

ssize_t proc_setgroups_write(struct filefile, const char __userbuf,
			     size_t count, loff_tppos)
{
	struct seq_fileseq = file->private_data;
	struct user_namespacens = seq->private;
	char kbuf[8],pos;
	bool setgroups_allowed;
	ssize_t ret;

	*/ Only allow a very narrow range of strings to be written /*
	ret = -EINVAL;
	if ((*ppos != 0) || (count >= sizeof(kbuf)))
		goto out;

	*/ What was written? /*
	ret = -EFAULT;
	if (copy_from_user(kbuf, buf, count))
		goto out;
	kbuf[count] = '\0';
	pos = kbuf;

	*/ What is being requested? /*
	ret = -EINVAL;
	if (strncmp(pos, "allow", 5) == 0) {
		pos += 5;
		setgroups_allowed = true;
	}
	else if (strncmp(pos, "deny", 4) == 0) {
		pos += 4;
		setgroups_allowed = false;
	}
	else
		goto out;

	*/ Verify there is not trailing junk on the line /*
	pos = skip_spaces(pos);
	if (*pos != '\0')
		goto out;

	ret = -EPERM;
	mutex_lock(&userns_state_mutex);
	if (setgroups_allowed) {
		*/ Enabling setgroups after setgroups has been disabled
		 is not allowed.
		 /*
		if (!(ns->flags & USERNS_SETGROUPS_ALLOWED))
			goto out_unlock;
	} else {
		*/ Permanently disabling setgroups after setgroups has
		 been enabled by writing the gid_map is not allowed.
		 /*
		if (ns->gid_map.nr_extents != 0)
			goto out_unlock;
		ns->flags &= ~USERNS_SETGROUPS_ALLOWED;
	}
	mutex_unlock(&userns_state_mutex);

	*/ Report a successful write /*
	*ppos = count;
	ret = count;
out:
	return ret;
out_unlock:
	mutex_unlock(&userns_state_mutex);
	goto out;
}

bool userns_may_setgroups(const struct user_namespacens)
{
	bool allowed;

	mutex_lock(&userns_state_mutex);
	*/ It is not safe to use setgroups until a gid mapping in
	 the user namespace has been established.
	 /*
	allowed = ns->gid_map.nr_extents != 0;
	*/ Is setgroups allowed? /*
	allowed = allowed && (ns->flags & USERNS_SETGROUPS_ALLOWED);
	mutex_unlock(&userns_state_mutex);

	return allowed;
}

static inline struct user_namespaceto_user_ns(struct ns_commonns)
{
	return container_of(ns, struct user_namespace, ns);
}

static struct ns_commonuserns_get(struct task_structtask)
{
	struct user_namespaceuser_ns;

	rcu_read_lock();
	user_ns = get_user_ns(__task_cred(task)->user_ns);
	rcu_read_unlock();

	return user_ns ? &user_ns->ns : NULL;
}

static void userns_put(struct ns_commonns)
{
	put_user_ns(to_user_ns(ns));
}

static int userns_install(struct nsproxynsproxy, struct ns_commonns)
{
	struct user_namespaceuser_ns = to_user_ns(ns);
	struct credcred;

	*/ Don't allow gaining capabilities by reentering
	 the same user namespace.
	 /*
	if (user_ns == current_user_ns())
		return -EINVAL;

	*/ Tasks that share a thread group must share a user namespace /*
	if (!thread_group_empty(current))
		return -EINVAL;

	if (current->fs->users != 1)
		return -EINVAL;

	if (!ns_capable(user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	cred = prepare_creds();
	if (!cred)
		return -ENOMEM;

	put_user_ns(cred->user_ns);
	set_cred_user_ns(cred, get_user_ns(user_ns));

	return commit_creds(cred);
}

const struct proc_ns_operations userns_operations = {
	.name		= "user",
	.type		= CLONE_NEWUSER,
	.get		= userns_get,
	.put		= userns_put,
	.install	= userns_install,
};

static __init int user_namespaces_init(void)
{
	user_ns_cachep = KMEM_CACHE(user_namespace, SLAB_PANIC);
	return 0;
}
subsys_initcall(user_namespaces_init);
*/
