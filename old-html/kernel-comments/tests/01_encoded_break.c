<br>
<br>  linux/kernel/acct.c
<br>
<br>  BSD Process Accounting for Linux
<br>
<br>  Author: Marco van Wieringen &#x3C;mvw@planets.elm.net&#x3E;
<br>
<br>  Some code based on ideas and code from:
<br>  Thomas K. Dyas &#x3C;tdyas@eden.rutgers.edu&#x3E;
<br>
<br>  This file implements BSD-style process accounting. Whenever any
<br>  process exits, an accounting record of type &#x22;struct acct&#x22; is
<br>  written to the file specified with the acct() system call. It is
<br>  up to user-level programs to do useful things with the accounting
<br>  log. The kernel just provides the raw accounting information.
<br>
<br> (C) Copyright 1995 - 1997 Marco van Wieringen - ELM Consultancy B.V.
<br>
<br>  Plugged two leaks. 1) It didn&#x27;t return acct_file into the free_filps if
<br>  the file happened to be read-only. 2) If the accounting was suspended
<br>  due to the lack of space it happily allowed to reopen it and completely
<br>  lost the old acct_file. 3/10/98, Al Viro.
<br>
<br>  Now we silently close acct_file on attempt to reopen. Cleaned sys_acct().
<br>  XTerms and EMACS are manifestations of pure evil. 21/10/98, AV.
<br>
<br>  Fixed a nasty interaction with with sys_umount(). If the accointing
<br>  was suspeneded we failed to stop it on umount(). Messy.
<br>  Another one: remount to readonly didn&#x27;t stop accounting.
<br>&#x9;Question: what should we do if we have CAP_SYS_ADMIN but not
<br>  CAP_SYS_PACCT? Current code does the following: umount returns -EBUSY
<br>  unless we are messing with the root. In that case we are getting a
<br>  real mess with do_remount_sb(). 9/11/98, AV.
<br>
<br>  Fixed a bunch of races (and pair of leaks). Probably not the best way,
<br>  but this one obviously doesn&#x27;t introduce deadlocks. Later. BTW, found
<br>  one race (and leak) in BSD implementation.
<br>  OK, that&#x27;s better. ANOTHER race and leak in BSD variant. There always
<br>  is one more bug... 10/11/98, AV.
<br>
<br>&#x9;Oh, fsck... Oopsable SMP race in do_process_acct() - we must hold
<br> -&#x3E;mmap_sem to walk the vma list of current-&#x3E;mm. Nasty, since it leaks
<br> a struct file opened for write. Fixed. 2/6/2000, AV.
<br> /*
<br>
<br>#include &#x3C;linux/mm.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/acct.h&#x3E;
<br>#include &#x3C;linux/capability.h&#x3E;
<br>#include &#x3C;linux/file.h&#x3E;
<br>#include &#x3C;linux/tty.h&#x3E;
<br>#include &#x3C;linux/security.h&#x3E;
<br>#include &#x3C;linux/vfs.h&#x3E;
<br>#include &#x3C;linux/jiffies.h&#x3E;
<br>#include &#x3C;linux/times.h&#x3E;
<br>#include &#x3C;linux/syscalls.h&#x3E;
<br>#include &#x3C;linux/mount.h&#x3E;
<br>#include &#x3C;linux/uaccess.h&#x3E;
<br>#include &#x3C;asm/div64.h&#x3E;
<br>#include &#x3C;linux/blkdev.h&#x3E;/ sector_div /*
<br>#include &#x3C;linux/pid_namespace.h&#x3E;
<br>#include &#x3C;linux/fs_pin.h&#x3E;
<br>
<br>*/
<br> These constants control the amount of freespace that suspend and
<br> resume the process accounting system, and the time delay between
<br> each check.
<br> Turned into sysctl-controllable parameters. AV, 12/11/98
<br> /*
<br>
<br>int acct_parm[3] = {4, 2, 30};
<br>#define RESUME&#x9;&#x9;(acct_parm[0])&#x9;*/ &#x3E;foo% free space - resume /*
<br>#define SUSPEND&#x9;&#x9;(acct_parm[1])&#x9;*/ &#x3C;foo% free space - suspend /*
<br>#define ACCT_TIMEOUT&#x9;(acct_parm[2])&#x9;*/ foo second timeout between checks /*
<br>
<br>*/
<br> External references and all of the globals.
<br> /*
<br>
<br>struct bsd_acct_struct {
<br>&#x9;struct fs_pin&#x9;&#x9;pin;
<br>&#x9;atomic_long_t&#x9;&#x9;count;
<br>&#x9;struct rcu_head&#x9;&#x9;rcu;
<br>&#x9;struct mutex&#x9;&#x9;lock;
<br>&#x9;int&#x9;&#x9;&#x9;active;
<br>&#x9;unsigned long&#x9;&#x9;needcheck;
<br>&#x9;struct file&#x9;&#x9;*file;
<br>&#x9;struct pid_namespace&#x9;*ns;
<br>&#x9;struct work_struct&#x9;work;
<br>&#x9;struct completion&#x9;done;
<br>};
<br>
<br>static void do_acct_process(struct bsd_acct_structacct);
<br>
<br>*/
<br> Check the amount of free space and suspend/resume accordingly.
<br> /*
<br>static int check_free_space(struct bsd_acct_structacct)
<br>{
<br>&#x9;struct kstatfs sbuf;
<br>
<br>&#x9;if (time_is_before_jiffies(acct-&#x3E;needcheck))
<br>&#x9;&#x9;goto out;
<br>
<br>&#x9;*/ May block /*
<br>&#x9;if (vfs_statfs(&#x26;acct-&#x3E;file-&#x3E;f_path, &#x26;sbuf))
<br>&#x9;&#x9;goto out;
<br>
<br>&#x9;if (acct-&#x3E;active) {
<br>&#x9;&#x9;u64 suspend = sbuf.f_blocks SUSPEND;
<br>&#x9;&#x9;do_div(suspend, 100);
<br>&#x9;&#x9;if (sbuf.f_bavail &#x3C;= suspend) {
<br>&#x9;&#x9;&#x9;acct-&#x3E;active = 0;
<br>&#x9;&#x9;&#x9;pr_info(&#x22;Process accounting paused\n&#x22;);
<br>&#x9;&#x9;}
<br>&#x9;} else {
<br>&#x9;&#x9;u64 resume = sbuf.f_blocks RESUME;
<br>&#x9;&#x9;do_div(resume, 100);
<br>&#x9;&#x9;if (sbuf.f_bavail &#x3E;= resume) {
<br>&#x9;&#x9;&#x9;acct-&#x3E;active = 1;
<br>&#x9;&#x9;&#x9;pr_info(&#x22;Process accounting resumed\n&#x22;);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;acct-&#x3E;needcheck = jiffies + ACCT_TIMEOUT*HZ;
<br>out:
<br>&#x9;return acct-&#x3E;active;
<br>}
<br>
<br>static void acct_put(struct bsd_acct_structp)
<br>{
<br>&#x9;if (atomic_long_dec_and_test(&#x26;p-&#x3E;count))
<br>&#x9;&#x9;kfree_rcu(p, rcu);
<br>}
<br>
<br>static inline struct bsd_acct_structto_acct(struct fs_pinp)
<br>{
<br>&#x9;return p ? container_of(p, struct bsd_acct_struct, pin) : NULL;
<br>}
<br>
<br>static struct bsd_acct_structacct_get(struct pid_namespacens)
<br>{
<br>&#x9;struct bsd_acct_structres;
<br>again:
<br>&#x9;smp_rmb();
<br>&#x9;rcu_read_lock();
<br>&#x9;res = to_acct(ACCESS_ONCE(ns-&#x3E;bacct));
<br>&#x9;if (!res) {
<br>&#x9;&#x9;rcu_read_unlock();
<br>&#x9;&#x9;return NULL;
<br>&#x9;}
<br>&#x9;if (!atomic_long_inc_not_zero(&#x26;res-&#x3E;count)) {
<br>&#x9;&#x9;rcu_read_unlock();
<br>&#x9;&#x9;cpu_relax();
<br>&#x9;&#x9;goto again;
<br>&#x9;}
<br>&#x9;rcu_read_unlock();
<br>&#x9;mutex_lock(&#x26;res-&#x3E;lock);
<br>&#x9;if (res != to_acct(ACCESS_ONCE(ns-&#x3E;bacct))) {
<br>&#x9;&#x9;mutex_unlock(&#x26;res-&#x3E;lock);
<br>&#x9;&#x9;acct_put(res);
<br>&#x9;&#x9;goto again;
<br>&#x9;}
<br>&#x9;return res;
<br>}
<br>
<br>static void acct_pin_kill(struct fs_pinpin)
<br>{
<br>&#x9;struct bsd_acct_structacct = to_acct(pin);
<br>&#x9;mutex_lock(&#x26;acct-&#x3E;lock);
<br>&#x9;do_acct_process(acct);
<br>&#x9;schedule_work(&#x26;acct-&#x3E;work);
<br>&#x9;wait_for_completion(&#x26;acct-&#x3E;done);
<br>&#x9;cmpxchg(&#x26;acct-&#x3E;ns-&#x3E;bacct, pin, NULL);
<br>&#x9;mutex_unlock(&#x26;acct-&#x3E;lock);
<br>&#x9;pin_remove(pin);
<br>&#x9;acct_put(acct);
<br>}
<br>
<br>static void close_work(struct work_structwork)
<br>{
<br>&#x9;struct bsd_acct_structacct = container_of(work, struct bsd_acct_struct, work);
<br>&#x9;struct filefile = acct-&#x3E;file;
<br>&#x9;if (file-&#x3E;f_op-&#x3E;flush)
<br>&#x9;&#x9;file-&#x3E;f_op-&#x3E;flush(file, NULL);
<br>&#x9;__fput_sync(file);
<br>&#x9;complete(&#x26;acct-&#x3E;done);
<br>}
<br>
<br>static int acct_on(struct filenamepathname)
<br>{
<br>&#x9;struct filefile;
<br>&#x9;struct vfsmountmnt,internal;
<br>&#x9;struct pid_namespacens = task_active_pid_ns(current);
<br>&#x9;struct bsd_acct_structacct;
<br>&#x9;struct fs_pinold;
<br>&#x9;int err;
<br>
<br>&#x9;acct = kzalloc(sizeof(struct bsd_acct_struct), GFP_KERNEL);
<br>&#x9;if (!acct)
<br>&#x9;&#x9;return -ENOMEM;
<br>
<br>&#x9;*/ Difference from BSD - they don&#x27;t do O_APPEND /*
<br>&#x9;file = file_open_name(pathname, O_WRONLY|O_APPEND|O_LARGEFILE, 0);
<br>&#x9;if (IS_ERR(file)) {
<br>&#x9;&#x9;kfree(acct);
<br>&#x9;&#x9;return PTR_ERR(file);
<br>&#x9;}
<br>
<br>&#x9;if (!S_ISREG(file_inode(file)-&#x3E;i_mode)) {
<br>&#x9;&#x9;kfree(acct);
<br>&#x9;&#x9;filp_close(file, NULL);
<br>&#x9;&#x9;return -EACCES;
<br>&#x9;}
<br>
<br>&#x9;if (!(file-&#x3E;f_mode &#x26; FMODE_CAN_WRITE)) {
<br>&#x9;&#x9;kfree(acct);
<br>&#x9;&#x9;filp_close(file, NULL);
<br>&#x9;&#x9;return -EIO;
<br>&#x9;}
<br>&#x9;internal = mnt_clone_internal(&#x26;file-&#x3E;f_path);
<br>&#x9;if (IS_ERR(internal)) {
<br>&#x9;&#x9;kfree(acct);
<br>&#x9;&#x9;filp_close(file, NULL);
<br>&#x9;&#x9;return PTR_ERR(internal);
<br>&#x9;}
<br>&#x9;err = mnt_want_write(internal);
<br>&#x9;if (err) {
<br>&#x9;&#x9;mntput(internal);
<br>&#x9;&#x9;kfree(acct);
<br>&#x9;&#x9;filp_close(file, NULL);
<br>&#x9;&#x9;return err;
<br>&#x9;}
<br>&#x9;mnt = file-&#x3E;f_path.mnt;
<br>&#x9;file-&#x3E;f_path.mnt = internal;
<br>
<br>&#x9;atomic_long_set(&#x26;acct-&#x3E;count, 1);
<br>&#x9;init_fs_pin(&#x26;acct-&#x3E;pin, acct_pin_kill);
<br>&#x9;acct-&#x3E;file = file;
<br>&#x9;acct-&#x3E;needcheck = jiffies;
<br>&#x9;acct-&#x3E;ns = ns;
<br>&#x9;mutex_init(&#x26;acct-&#x3E;lock);
<br>&#x9;INIT_WORK(&#x26;acct-&#x3E;work, close_work);
<br>&#x9;init_completion(&#x26;acct-&#x3E;done);
<br>&#x9;mutex_lock_nested(&#x26;acct-&#x3E;lock, 1);&#x9;*/ nobody has seen it yet /*
<br>&#x9;pin_insert(&#x26;acct-&#x3E;pin, mnt);
<br>
<br>&#x9;rcu_read_lock();
<br>&#x9;old = xchg(&#x26;ns-&#x3E;bacct, &#x26;acct-&#x3E;pin);
<br>&#x9;mutex_unlock(&#x26;acct-&#x3E;lock);
<br>&#x9;pin_kill(old);
<br>&#x9;mnt_drop_write(mnt);
<br>&#x9;mntput(mnt);
<br>&#x9;return 0;
<br>}
<br>
<br>static DEFINE_MUTEX(acct_on_mutex);
<br>
<br>*/
<br> sys_acct - enable/disable process accounting
<br> @name: file name for accounting records or NULL to shutdown accounting
<br>
<br> Returns 0 for success or negative errno values for failure.
<br>
<br> sys_acct() is the only system call needed to implement process
<br> accounting. It takes the name of the file where accounting records
<br> should be written. If the filename is NULL, accounting will be
<br> shutdown.
<br> /*
<br>SYSCALL_DEFINE1(acct, const char __user, name)
<br>{
<br>&#x9;int error = 0;
<br>
<br>&#x9;if (!capable(CAP_SYS_PACCT))
<br>&#x9;&#x9;return -EPERM;
<br>
<br>&#x9;if (name) {
<br>&#x9;&#x9;struct filenametmp = getname(name);
<br>
<br>&#x9;&#x9;if (IS_ERR(tmp))
<br>&#x9;&#x9;&#x9;return PTR_ERR(tmp);
<br>&#x9;&#x9;mutex_lock(&#x26;acct_on_mutex);
<br>&#x9;&#x9;error = acct_on(tmp);
<br>&#x9;&#x9;mutex_unlock(&#x26;acct_on_mutex);
<br>&#x9;&#x9;putname(tmp);
<br>&#x9;} else {
<br>&#x9;&#x9;rcu_read_lock();
<br>&#x9;&#x9;pin_kill(task_active_pid_ns(current)-&#x3E;bacct);
<br>&#x9;}
<br>
<br>&#x9;return error;
<br>}
<br>
<br>void acct_exit_ns(struct pid_namespacens)
<br>{
<br>&#x9;rcu_read_lock();
<br>&#x9;pin_kill(ns-&#x3E;bacct);
<br>}
<br>
<br>*/
<br>  encode an unsigned long into a comp_t
<br>
<br>  This routine has been adopted from the encode_comp_t() function in
<br>  the kern_acct.c file of the FreeBSD operating system. The encoding
<br>  is a 13-bit fraction with a 3-bit (base 8) exponent.
<br> /*
<br>
<br>#define&#x9;MANTSIZE&#x9;13&#x9;&#x9;&#x9;*/ 13 bit mantissa. /*
<br>#define&#x9;EXPSIZE&#x9;&#x9;3&#x9;&#x9;&#x9;*/ Base 8 (3 bit) exponent. /*
<br>#define&#x9;MAXFRACT&#x9;((1 &#x3C;&#x3C; MANTSIZE) - 1)&#x9;*/ Maximum fractional value. /*
<br>
<br>static comp_t encode_comp_t(unsigned long value)
<br>{
<br>&#x9;int exp, rnd;
<br>
<br>&#x9;exp = rnd = 0;
<br>&#x9;while (value &#x3E; MAXFRACT) {
<br>&#x9;&#x9;rnd = value &#x26; (1 &#x3C;&#x3C; (EXPSIZE - 1));&#x9;*/ Round up? /*
<br>&#x9;&#x9;value &#x3E;&#x3E;= EXPSIZE;&#x9;*/ Base 8 exponent == 3 bit shift. /*
<br>&#x9;&#x9;exp++;
<br>&#x9;}
<br>
<br>&#x9;*/
<br>&#x9; If we need to round up, do it (and handle overflow correctly).
<br>&#x9; /*
<br>&#x9;if (rnd &#x26;&#x26; (++value &#x3E; MAXFRACT)) {
<br>&#x9;&#x9;value &#x3E;&#x3E;= EXPSIZE;
<br>&#x9;&#x9;exp++;
<br>&#x9;}
<br>
<br>&#x9;*/
<br>&#x9; Clean it up and polish it off.
<br>&#x9; /*
<br>&#x9;exp &#x3C;&#x3C;= MANTSIZE;&#x9;&#x9;*/ Shift the exponent into place /*
<br>&#x9;exp += value;&#x9;&#x9;&#x9;*/ and add on the mantissa. /*
<br>&#x9;return exp;
<br>}
<br>
<br>#if ACCT_VERSION == 1 || ACCT_VERSION == 2
<br>*/
<br> encode an u64 into a comp2_t (24 bits)
<br>
<br> Format: 5 bit base 2 exponent, 20 bits mantissa.
<br> The leading bit of the mantissa is not stored, but implied for
<br> non-zero exponents.
<br> Largest encodable value is 50 bits.
<br> /*
<br>
<br>#define MANTSIZE2       20                     / 20 bit mantissa. /*
<br>#define EXPSIZE2        5                      / 5 bit base 2 exponent. /*
<br>#define MAXFRACT2       ((1ul &#x3C;&#x3C; MANTSIZE2) - 1)/ Maximum fractional value. /*
<br>#define MAXEXP2         ((1 &#x3C;&#x3C; EXPSIZE2) - 1)   / Maximum exponent. /*
<br>
<br>static comp2_t encode_comp2_t(u64 value)
<br>{
<br>&#x9;int exp, rnd;
<br>
<br>&#x9;exp = (value &#x3E; (MAXFRACT2&#x3E;&#x3E;1));
<br>&#x9;rnd = 0;
<br>&#x9;while (value &#x3E; MAXFRACT2) {
<br>&#x9;&#x9;rnd = value &#x26; 1;
<br>&#x9;&#x9;value &#x3E;&#x3E;= 1;
<br>&#x9;&#x9;exp++;
<br>&#x9;}
<br>
<br>&#x9;*/
<br>&#x9; If we need to round up, do it (and handle overflow correctly).
<br>&#x9; /*
<br>&#x9;if (rnd &#x26;&#x26; (++value &#x3E; MAXFRACT2)) {
<br>&#x9;&#x9;value &#x3E;&#x3E;= 1;
<br>&#x9;&#x9;exp++;
<br>&#x9;}
<br>
<br>&#x9;if (exp &#x3E; MAXEXP2) {
<br>&#x9;&#x9;*/ Overflow. Return largest representable number instead. /*
<br>&#x9;&#x9;return (1ul &#x3C;&#x3C; (MANTSIZE2+EXPSIZE2-1)) - 1;
<br>&#x9;} else {
<br>&#x9;&#x9;return (value &#x26; (MAXFRACT2&#x3E;&#x3E;1)) | (exp &#x3C;&#x3C; (MANTSIZE2-1));
<br>&#x9;}
<br>}
<br>#endif
<br>
<br>#if ACCT_VERSION == 3
<br>*/
<br> encode an u64 into a 32 bit IEEE float
<br> /*
<br>static u32 encode_float(u64 value)
<br>{
<br>&#x9;unsigned exp = 190;
<br>&#x9;unsigned u;
<br>
<br>&#x9;if (value == 0)
<br>&#x9;&#x9;return 0;
<br>&#x9;while ((s64)value &#x3E; 0) {
<br>&#x9;&#x9;value &#x3C;&#x3C;= 1;
<br>&#x9;&#x9;exp--;
<br>&#x9;}
<br>&#x9;u = (u32)(value &#x3E;&#x3E; 40) &#x26; 0x7fffffu;
<br>&#x9;return u | (exp &#x3C;&#x3C; 23);
<br>}
<br>#endif
<br>
<br>*/
<br>  Write an accounting entry for an exiting process
<br>
<br>  The acct_process() call is the workhorse of the process
<br>  accounting system. The struct acct is built here and then written
<br>  into the accounting file. This function should only be called from
<br>  do_exit() or when switching to a different output file.
<br> /*
<br>
<br>static void fill_ac(acct_tac)
<br>{
<br>&#x9;struct pacct_structpacct = &#x26;current-&#x3E;signal-&#x3E;pacct;
<br>&#x9;u64 elapsed, run_time;
<br>&#x9;struct tty_structtty;
<br>
<br>&#x9;*/
<br>&#x9; Fill the accounting struct with the needed info as recorded
<br>&#x9; by the different kernel functions.
<br>&#x9; /*
<br>&#x9;memset(ac, 0, sizeof(acct_t));
<br>
<br>&#x9;ac-&#x3E;ac_version = ACCT_VERSION | ACCT_BYTEORDER;
<br>&#x9;strlcpy(ac-&#x3E;ac_comm, current-&#x3E;comm, sizeof(ac-&#x3E;ac_comm));
<br>
<br>&#x9;*/ calculate run_time in nsec/*
<br>&#x9;run_time = ktime_get_ns();
<br>&#x9;run_time -= current-&#x3E;group_leader-&#x3E;start_time;
<br>&#x9;*/ convert nsec -&#x3E; AHZ /*
<br>&#x9;elapsed = nsec_to_AHZ(run_time);
<br>#if ACCT_VERSION == 3
<br>&#x9;ac-&#x3E;ac_etime = encode_float(elapsed);
<br>#else
<br>&#x9;ac-&#x3E;ac_etime = encode_comp_t(elapsed &#x3C; (unsigned long) -1l ?
<br>&#x9;&#x9;&#x9;&#x9;(unsigned long) elapsed : (unsigned long) -1l);
<br>#endif
<br>#if ACCT_VERSION == 1 || ACCT_VERSION == 2
<br>&#x9;{
<br>&#x9;&#x9;*/ new enlarged etime field /*
<br>&#x9;&#x9;comp2_t etime = encode_comp2_t(elapsed);
<br>
<br>&#x9;&#x9;ac-&#x3E;ac_etime_hi = etime &#x3E;&#x3E; 16;
<br>&#x9;&#x9;ac-&#x3E;ac_etime_lo = (u16) etime;
<br>&#x9;}
<br>#endif
<br>&#x9;do_div(elapsed, AHZ);
<br>&#x9;ac-&#x3E;ac_btime = get_seconds() - elapsed;
<br>#if ACCT_VERSION==2
<br>&#x9;ac-&#x3E;ac_ahz = AHZ;
<br>#endif
<br>
<br>&#x9;spin_lock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
<br>&#x9;tty = current-&#x3E;signal-&#x3E;tty;&#x9;*/ Safe as we hold the siglock /*
<br>&#x9;ac-&#x3E;ac_tty = tty ? old_encode_dev(tty_devnum(tty)) : 0;
<br>&#x9;ac-&#x3E;ac_utime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&#x3E;ac_utime)));
<br>&#x9;ac-&#x3E;ac_stime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&#x3E;ac_stime)));
<br>&#x9;ac-&#x3E;ac_flag = pacct-&#x3E;ac_flag;
<br>&#x9;ac-&#x3E;ac_mem = encode_comp_t(pacct-&#x3E;ac_mem);
<br>&#x9;ac-&#x3E;ac_minflt = encode_comp_t(pacct-&#x3E;ac_minflt);
<br>&#x9;ac-&#x3E;ac_majflt = encode_comp_t(pacct-&#x3E;ac_majflt);
<br>&#x9;ac-&#x3E;ac_exitcode = pacct-&#x3E;ac_exitcode;
<br>&#x9;spin_unlock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
<br>}
<br>*/
<br>  do_acct_process does all actual work. Caller holds the reference to file.
<br> /*
<br>static void do_acct_process(struct bsd_acct_structacct)
<br>{
<br>&#x9;acct_t ac;
<br>&#x9;unsigned long flim;
<br>&#x9;const struct credorig_cred;
<br>&#x9;struct filefile = acct-&#x3E;file;
<br>
<br>&#x9;*/
<br>&#x9; Accounting records are not subject to resource limits.
<br>&#x9; /*
<br>&#x9;flim = current-&#x3E;signal-&#x3E;rlim[RLIMIT_FSIZE].rlim_cur;
<br>&#x9;current-&#x3E;signal-&#x3E;rlim[RLIMIT_FSIZE].rlim_cur = RLIM_INFINITY;
<br>&#x9;*/ Perform file operations on behalf of whoever enabled accounting /*
<br>&#x9;orig_cred = override_creds(file-&#x3E;f_cred);
<br>
<br>&#x9;*/
<br>&#x9; First check to see if there is enough free_space to continue
<br>&#x9; the process accounting system.
<br>&#x9; /*
<br>&#x9;if (!check_free_space(acct))
<br>&#x9;&#x9;goto out;
<br>
<br>&#x9;fill_ac(&#x26;ac);
<br>&#x9;*/ we really need to bite the bullet and change layout /*
<br>&#x9;ac.ac_uid = from_kuid_munged(file-&#x3E;f_cred-&#x3E;user_ns, orig_cred-&#x3E;uid);
<br>&#x9;ac.ac_gid = from_kgid_munged(file-&#x3E;f_cred-&#x3E;user_ns, orig_cred-&#x3E;gid);
<br>#if ACCT_VERSION == 1 || ACCT_VERSION == 2
<br>&#x9;*/ backward-compatible 16 bit fields /*
<br>&#x9;ac.ac_uid16 = ac.ac_uid;
<br>&#x9;ac.ac_gid16 = ac.ac_gid;
<br>#endif
<br>#if ACCT_VERSION == 3
<br>&#x9;{
<br>&#x9;&#x9;struct pid_namespacens = acct-&#x3E;ns;
<br>
<br>&#x9;&#x9;ac.ac_pid = task_tgid_nr_ns(current, ns);
<br>&#x9;&#x9;rcu_read_lock();
<br>&#x9;&#x9;ac.ac_ppid = task_tgid_nr_ns(rcu_dereference(current-&#x3E;real_parent),
<br>&#x9;&#x9;&#x9;&#x9;&#x9;     ns);
<br>&#x9;&#x9;rcu_read_unlock();
<br>&#x9;}
<br>#endif
<br>&#x9;*/
<br>&#x9; Get freeze protection. If the fs is frozen, just skip the write
<br>&#x9; as we could deadlock the system otherwise.
<br>&#x9; /*
<br>&#x9;if (file_start_write_trylock(file)) {
<br>&#x9;&#x9;*/ it&#x27;s been opened O_APPEND, so position is irrelevant /*
<br>&#x9;&#x9;loff_t pos = 0;
<br>&#x9;&#x9;__kernel_write(file, (char)&#x26;ac, sizeof(acct_t), &#x26;pos);
<br>&#x9;&#x9;file_end_write(file);
<br>&#x9;}
<br>out:
<br>&#x9;current-&#x3E;signal-&#x3E;rlim[RLIMIT_FSIZE].rlim_cur = flim;
<br>&#x9;revert_creds(orig_cred);
<br>}
<br>
<br>*/
<br> acct_collect - collect accounting information into pacct_struct
<br> @exitcode: task exit code
<br> @group_dead: not 0, if this thread is the last one in the process.
<br> /*
<br>void acct_collect(long exitcode, int group_dead)
<br>{
<br>&#x9;struct pacct_structpacct = &#x26;current-&#x3E;signal-&#x3E;pacct;
<br>&#x9;cputime_t utime, stime;
<br>&#x9;unsigned long vsize = 0;
<br>
<br>&#x9;if (group_dead &#x26;&#x26; current-&#x3E;mm) {
<br>&#x9;&#x9;struct vm_area_structvma;
<br>
<br>&#x9;&#x9;down_read(&#x26;current-&#x3E;mm-&#x3E;mmap_sem);
<br>&#x9;&#x9;vma = current-&#x3E;mm-&#x3E;mmap;
<br>&#x9;&#x9;while (vma) {
<br>&#x9;&#x9;&#x9;vsize += vma-&#x3E;vm_end - vma-&#x3E;vm_start;
<br>&#x9;&#x9;&#x9;vma = vma-&#x3E;vm_next;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;up_read(&#x26;current-&#x3E;mm-&#x3E;mmap_sem);
<br>&#x9;}
<br>
<br>&#x9;spin_lock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
<br>&#x9;if (group_dead)
<br>&#x9;&#x9;pacct-&#x3E;ac_mem = vsize / 1024;
<br>&#x9;if (thread_group_leader(current)) {
<br>&#x9;&#x9;pacct-&#x3E;ac_exitcode = exitcode;
<br>&#x9;&#x9;if (current-&#x3E;flags &#x26; PF_FORKNOEXEC)
<br>&#x9;&#x9;&#x9;pacct-&#x3E;ac_flag |= AFORK;
<br>&#x9;}
<br>&#x9;if (current-&#x3E;flags &#x26; PF_SUPERPRIV)
<br>&#x9;&#x9;pacct-&#x3E;ac_flag |= ASU;
<br>&#x9;if (current-&#x3E;flags &#x26; PF_DUMPCORE)
<br>&#x9;&#x9;pacct-&#x3E;ac_flag |= ACORE;
<br>&#x9;if (current-&#x3E;flags &#x26; PF_SIGNALED)
<br>&#x9;&#x9;pacct-&#x3E;ac_flag |= AXSIG;
<br>&#x9;task_cputime(current, &#x26;utime, &#x26;stime);
<br>&#x9;pacct-&#x3E;ac_utime += utime;
<br>&#x9;pacct-&#x3E;ac_stime += stime;
<br>&#x9;pacct-&#x3E;ac_minflt += current-&#x3E;min_flt;
<br>&#x9;pacct-&#x3E;ac_majflt += current-&#x3E;maj_flt;
<br>&#x9;spin_unlock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
<br>}
<br>
<br>static void slow_acct_process(struct pid_namespacens)
<br>{
<br>&#x9;for ( ; ns; ns = ns-&#x3E;parent) {
<br>&#x9;&#x9;struct bsd_acct_structacct = acct_get(ns);
<br>&#x9;&#x9;if (acct) {
<br>&#x9;&#x9;&#x9;do_acct_process(acct);
<br>&#x9;&#x9;&#x9;mutex_unlock(&#x26;acct-&#x3E;lock);
<br>&#x9;&#x9;&#x9;acct_put(acct);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>}
<br>
<br>*/
<br> acct_process
<br>
<br> handles process accounting for an exiting task
<br> /*
<br>void acct_process(void)
<br>{
<br>&#x9;struct pid_namespacens;
<br>
<br>&#x9;*/
<br>&#x9; This loop is safe lockless, since current is still
<br>&#x9; alive and holds its namespace, which in turn holds
<br>&#x9; its parent.
<br>&#x9; /*
<br>&#x9;for (ns = task_active_pid_ns(current); ns != NULL; ns = ns-&#x3E;parent) {
<br>&#x9;&#x9;if (ns-&#x3E;bacct)
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;if (unlikely(ns))
<br>&#x9;&#x9;slow_acct_process(ns);
<br>}
<br>*/
<br>
<br> async.c: Asynchronous function calls for boot performance
<br>
<br> (C) Copyright 2009 Intel Corporation
<br> Author: Arjan van de Ven &#x3C;arjan@linux.intel.com&#x3E;
<br>
<br> This program is free software; you can redistribute it and/or
<br> modify it under the terms of the GNU General Public License
<br> as published by the Free Software Foundation; version 2
<br> of the License.
<br> /*
<br>
<br>
<br>*/
<br>
<br>Goals and Theory of Operation
<br>
<br>The primary goal of this feature is to reduce the kernel boot time,
<br>by doing various independent hardware delays and discovery operations
<br>decoupled and not strictly serialized.
<br>
<br>More specifically, the asynchronous function call concept allows
<br>certain operations (primarily during system boot) to happen
<br>asynchronously, out of order, while these operations still
<br>have their externally visible parts happen sequentially and in-order.
<br>(not unlike how out-of-order CPUs retire their instructions in order)
<br>
<br>Key to the asynchronous function call implementation is the concept of
<br>a &#x22;sequence cookie&#x22; (which, although it has an abstracted type, can be
<br>thought of as a monotonically incrementing number).
<br>
<br>The async core will assign each scheduled event such a sequence cookie and
<br>pass this to the called functions.
<br>
<br>The asynchronously called function should before doing a globally visible
<br>operation, such as registering device numbers, call the
<br>async_synchronize_cookie() function and pass in its own cookie. The
<br>async_synchronize_cookie() function will make sure that all asynchronous
<br>operations that were scheduled prior to the operation corresponding with the
<br>cookie have completed.
<br>
<br>Subsystem/driver initialization code that scheduled asynchronous probe
<br>functions, but which shares global resources with other drivers/subsystems
<br>that do not use the asynchronous call feature, need to do a full
<br>synchronization with the async_synchronize_full() function, before returning
<br>from their init function. This is to maintain strict ordering between the
<br>asynchronous and synchronous parts of the kernel.
<br>
<br>/*
<br>
<br>#include &#x3C;linux/async.h&#x3E;
<br>#include &#x3C;linux/atomic.h&#x3E;
<br>#include &#x3C;linux/ktime.h&#x3E;
<br>#include &#x3C;linux/export.h&#x3E;
<br>#include &#x3C;linux/wait.h&#x3E;
<br>#include &#x3C;linux/sched.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/workqueue.h&#x3E;
<br>
<br>#include &#x22;workqueue_internal.h&#x22;
<br>
<br>static async_cookie_t next_cookie = 1;
<br>
<br>#define MAX_WORK&#x9;&#x9;32768
<br>#define ASYNC_COOKIE_MAX&#x9;ULLONG_MAX&#x9;*/ infinity cookie /*
<br>
<br>static LIST_HEAD(async_global_pending);&#x9;*/ pending from all registered doms /*
<br>static ASYNC_DOMAIN(async_dfl_domain);
<br>static DEFINE_SPINLOCK(async_lock);
<br>
<br>struct async_entry {
<br>&#x9;struct list_head&#x9;domain_list;
<br>&#x9;struct list_head&#x9;global_list;
<br>&#x9;struct work_struct&#x9;work;
<br>&#x9;async_cookie_t&#x9;&#x9;cookie;
<br>&#x9;async_func_t&#x9;&#x9;func;
<br>&#x9;void&#x9;&#x9;&#x9;*data;
<br>&#x9;struct async_domain&#x9;*domain;
<br>};
<br>
<br>static DECLARE_WAIT_QUEUE_HEAD(async_done);
<br>
<br>static atomic_t entry_count;
<br>
<br>static async_cookie_t lowest_in_progress(struct async_domaindomain)
<br>{
<br>&#x9;struct list_headpending;
<br>&#x9;async_cookie_t ret = ASYNC_COOKIE_MAX;
<br>&#x9;unsigned long flags;
<br>
<br>&#x9;spin_lock_irqsave(&#x26;async_lock, flags);
<br>
<br>&#x9;if (domain)
<br>&#x9;&#x9;pending = &#x26;domain-&#x3E;pending;
<br>&#x9;else
<br>&#x9;&#x9;pending = &#x26;async_global_pending;
<br>
<br>&#x9;if (!list_empty(pending))
<br>&#x9;&#x9;ret = list_first_entry(pending, struct async_entry,
<br>&#x9;&#x9;&#x9;&#x9;       domain_list)-&#x3E;cookie;
<br>
<br>&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);
<br>&#x9;return ret;
<br>}
<br>
<br>*/
<br> pick the first pending entry and run it
<br> /*
<br>static void async_run_entry_fn(struct work_structwork)
<br>{
<br>&#x9;struct async_entryentry =
<br>&#x9;&#x9;container_of(work, struct async_entry, work);
<br>&#x9;unsigned long flags;
<br>&#x9;ktime_t uninitialized_var(calltime), delta, rettime;
<br>
<br>&#x9;*/ 1) run (and print duration) /*
<br>&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
<br>&#x9;&#x9;pr_debug(&#x22;calling  %lli_%pF @ %i\n&#x22;,
<br>&#x9;&#x9;&#x9;(long long)entry-&#x3E;cookie,
<br>&#x9;&#x9;&#x9;entry-&#x3E;func, task_pid_nr(current));
<br>&#x9;&#x9;calltime = ktime_get();
<br>&#x9;}
<br>&#x9;entry-&#x3E;func(entry-&#x3E;data, entry-&#x3E;cookie);
<br>&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
<br>&#x9;&#x9;rettime = ktime_get();
<br>&#x9;&#x9;delta = ktime_sub(rettime, calltime);
<br>&#x9;&#x9;pr_debug(&#x22;initcall %lli_%pF returned 0 after %lld usecs\n&#x22;,
<br>&#x9;&#x9;&#x9;(long long)entry-&#x3E;cookie,
<br>&#x9;&#x9;&#x9;entry-&#x3E;func,
<br>&#x9;&#x9;&#x9;(long long)ktime_to_ns(delta) &#x3E;&#x3E; 10);
<br>&#x9;}
<br>
<br>&#x9;*/ 2) remove self from the pending queues /*
<br>&#x9;spin_lock_irqsave(&#x26;async_lock, flags);
<br>&#x9;list_del_init(&#x26;entry-&#x3E;domain_list);
<br>&#x9;list_del_init(&#x26;entry-&#x3E;global_list);
<br>
<br>&#x9;*/ 3) free the entry /*
<br>&#x9;kfree(entry);
<br>&#x9;atomic_dec(&#x26;entry_count);
<br>
<br>&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);
<br>
<br>&#x9;*/ 4) wake up any waiters /*
<br>&#x9;wake_up(&#x26;async_done);
<br>}
<br>
<br>static async_cookie_t __async_schedule(async_func_t func, voiddata, struct async_domaindomain)
<br>{
<br>&#x9;struct async_entryentry;
<br>&#x9;unsigned long flags;
<br>&#x9;async_cookie_t newcookie;
<br>
<br>&#x9;*/ allow irq-off callers /*
<br>&#x9;entry = kzalloc(sizeof(struct async_entry), GFP_ATOMIC);
<br>
<br>&#x9;*/
<br>&#x9; If we&#x27;re out of memory or if there&#x27;s too much work
<br>&#x9; pending already, we execute synchronously.
<br>&#x9; /*
<br>&#x9;if (!entry || atomic_read(&#x26;entry_count) &#x3E; MAX_WORK) {
<br>&#x9;&#x9;kfree(entry);
<br>&#x9;&#x9;spin_lock_irqsave(&#x26;async_lock, flags);
<br>&#x9;&#x9;newcookie = next_cookie++;
<br>&#x9;&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);
<br>
<br>&#x9;&#x9;*/ low on memory.. run synchronously /*
<br>&#x9;&#x9;func(data, newcookie);
<br>&#x9;&#x9;return newcookie;
<br>&#x9;}
<br>&#x9;INIT_LIST_HEAD(&#x26;entry-&#x3E;domain_list);
<br>&#x9;INIT_LIST_HEAD(&#x26;entry-&#x3E;global_list);
<br>&#x9;INIT_WORK(&#x26;entry-&#x3E;work, async_run_entry_fn);
<br>&#x9;entry-&#x3E;func = func;
<br>&#x9;entry-&#x3E;data = data;
<br>&#x9;entry-&#x3E;domain = domain;
<br>
<br>&#x9;spin_lock_irqsave(&#x26;async_lock, flags);
<br>
<br>&#x9;*/ allocate cookie and queue /*
<br>&#x9;newcookie = entry-&#x3E;cookie = next_cookie++;
<br>
<br>&#x9;list_add_tail(&#x26;entry-&#x3E;domain_list, &#x26;domain-&#x3E;pending);
<br>&#x9;if (domain-&#x3E;registered)
<br>&#x9;&#x9;list_add_tail(&#x26;entry-&#x3E;global_list, &#x26;async_global_pending);
<br>
<br>&#x9;atomic_inc(&#x26;entry_count);
<br>&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);
<br>
<br>&#x9;*/ mark that this task has queued an async job, used by module init /*
<br>&#x9;current-&#x3E;flags |= PF_USED_ASYNC;
<br>
<br>&#x9;*/ schedule for execution /*
<br>&#x9;queue_work(system_unbound_wq, &#x26;entry-&#x3E;work);
<br>
<br>&#x9;return newcookie;
<br>}
<br>
<br>*/
<br> async_schedule - schedule a function for asynchronous execution
<br> @func: function to execute asynchronously
<br> @data: data pointer to pass to the function
<br>
<br> Returns an async_cookie_t that may be used for checkpointing later.
<br> Note: This function may be called from atomic or non-atomic contexts.
<br> /*
<br>async_cookie_t async_schedule(async_func_t func, voiddata)
<br>{
<br>&#x9;return __async_schedule(func, data, &#x26;async_dfl_domain);
<br>}
<br>EXPORT_SYMBOL_GPL(async_schedule);
<br>
<br>*/
<br> async_schedule_domain - schedule a function for asynchronous execution within a certain domain
<br> @func: function to execute asynchronously
<br> @data: data pointer to pass to the function
<br> @domain: the domain
<br>
<br> Returns an async_cookie_t that may be used for checkpointing later.
<br> @domain may be used in the async_synchronize_*_domain() functions to
<br> wait within a certain synchronization domain rather than globally.  A
<br> synchronization domain is specified via @domain.  Note: This function
<br> may be called from atomic or non-atomic contexts.
<br> /*
<br>async_cookie_t async_schedule_domain(async_func_t func, voiddata,
<br>&#x9;&#x9;&#x9;&#x9;     struct async_domaindomain)
<br>{
<br>&#x9;return __async_schedule(func, data, domain);
<br>}
<br>EXPORT_SYMBOL_GPL(async_schedule_domain);
<br>
<br>*/
<br> async_synchronize_full - synchronize all asynchronous function calls
<br>
<br> This function waits until all asynchronous function calls have been done.
<br> /*
<br>void async_synchronize_full(void)
<br>{
<br>&#x9;async_synchronize_full_domain(NULL);
<br>}
<br>EXPORT_SYMBOL_GPL(async_synchronize_full);
<br>
<br>*/
<br> async_unregister_domain - ensure no more anonymous waiters on this domain
<br> @domain: idle domain to flush out of any async_synchronize_full instances
<br>
<br> async_synchronize_{cookie|full}_domain() are not flushed since callers
<br> of these routines should know the lifetime of @domain
<br>
<br> Prefer ASYNC_DOMAIN_EXCLUSIVE() declarations over flushing
<br> /*
<br>void async_unregister_domain(struct async_domaindomain)
<br>{
<br>&#x9;spin_lock_irq(&#x26;async_lock);
<br>&#x9;WARN_ON(!domain-&#x3E;registered || !list_empty(&#x26;domain-&#x3E;pending));
<br>&#x9;domain-&#x3E;registered = 0;
<br>&#x9;spin_unlock_irq(&#x26;async_lock);
<br>}
<br>EXPORT_SYMBOL_GPL(async_unregister_domain);
<br>
<br>*/
<br> async_synchronize_full_domain - synchronize all asynchronous function within a certain domain
<br> @domain: the domain to synchronize
<br>
<br> This function waits until all asynchronous function calls for the
<br> synchronization domain specified by @domain have been done.
<br> /*
<br>void async_synchronize_full_domain(struct async_domaindomain)
<br>{
<br>&#x9;async_synchronize_cookie_domain(ASYNC_COOKIE_MAX, domain);
<br>}
<br>EXPORT_SYMBOL_GPL(async_synchronize_full_domain);
<br>
<br>*/
<br> async_synchronize_cookie_domain - synchronize asynchronous function calls within a certain domain with cookie checkpointing
<br> @cookie: async_cookie_t to use as checkpoint
<br> @domain: the domain to synchronize (%NULL for all registered domains)
<br>
<br> This function waits until all asynchronous function calls for the
<br> synchronization domain specified by @domain submitted prior to @cookie
<br> have been done.
<br> /*
<br>void async_synchronize_cookie_domain(async_cookie_t cookie, struct async_domaindomain)
<br>{
<br>&#x9;ktime_t uninitialized_var(starttime), delta, endtime;
<br>
<br>&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
<br>&#x9;&#x9;pr_debug(&#x22;async_waiting @ %i\n&#x22;, task_pid_nr(current));
<br>&#x9;&#x9;starttime = ktime_get();
<br>&#x9;}
<br>
<br>&#x9;wait_event(async_done, lowest_in_progress(domain) &#x3E;= cookie);
<br>
<br>&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
<br>&#x9;&#x9;endtime = ktime_get();
<br>&#x9;&#x9;delta = ktime_sub(endtime, starttime);
<br>
<br>&#x9;&#x9;pr_debug(&#x22;async_continuing @ %i after %lli usec\n&#x22;,
<br>&#x9;&#x9;&#x9;task_pid_nr(current),
<br>&#x9;&#x9;&#x9;(long long)ktime_to_ns(delta) &#x3E;&#x3E; 10);
<br>&#x9;}
<br>}
<br>EXPORT_SYMBOL_GPL(async_synchronize_cookie_domain);
<br>
<br>*/
<br> async_synchronize_cookie - synchronize asynchronous function calls with cookie checkpointing
<br> @cookie: async_cookie_t to use as checkpoint
<br>
<br> This function waits until all asynchronous function calls prior to @cookie
<br> have been done.
<br> /*
<br>void async_synchronize_cookie(async_cookie_t cookie)
<br>{
<br>&#x9;async_synchronize_cookie_domain(cookie, &#x26;async_dfl_domain);
<br>}
<br>EXPORT_SYMBOL_GPL(async_synchronize_cookie);
<br>
<br>*/
<br> current_is_async - is %current an async worker task?
<br>
<br> Returns %true if %current is an async worker task.
<br> /*
<br>bool current_is_async(void)
<br>{
<br>&#x9;struct workerworker = current_wq_worker();
<br>
<br>&#x9;return worker &#x26;&#x26; worker-&#x3E;current_func == async_run_entry_fn;
<br>}
<br>EXPORT_SYMBOL_GPL(current_is_async);
<br>*/
<br> audit.c -- Auditing support
<br> Gateway between the kernel (e.g., selinux) and the user-space audit daemon.
<br> System-call specific features have moved to auditsc.c
<br>
<br> Copyright 2003-2007 Red Hat Inc., Durham, North Carolina.
<br> All Rights Reserved.
<br>
<br> This program is free software; you can redistribute it and/or modify
<br> it under the terms of the GNU General Public License as published by
<br> the Free Software Foundation; either version 2 of the License, or
<br> (at your option) any later version.
<br>
<br> This program is distributed in the hope that it will be useful,
<br> but WITHOUT ANY WARRANTY; without even the implied warranty of
<br> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
<br> GNU General Public License for more details.
<br>
<br> You should have received a copy of the GNU General Public License
<br> along with this program; if not, write to the Free Software
<br> Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
<br>
<br> Written by Rickard E. (Rik) Faith &#x3C;faith@redhat.com&#x3E;
<br>
<br> Goals: 1) Integrate fully with Security Modules.
<br>&#x9;  2) Minimal run-time overhead:
<br>&#x9;     a) Minimal when syscall auditing is disabled (audit_enable=0).
<br>&#x9;     b) Small when syscall auditing is enabled and no audit record
<br>&#x9;&#x9;is generated (defer as much work as possible to record
<br>&#x9;&#x9;generation time):
<br>&#x9;&#x9;i) context is allocated,
<br>&#x9;&#x9;ii) names from getname are stored without a copy, and
<br>&#x9;&#x9;iii) inode information stored from path_lookup.
<br>&#x9;  3) Ability to disable syscall auditing at boot time (audit=0).
<br>&#x9;  4) Usable by other parts of the kernel (if audit_log* is called,
<br>&#x9;     then a syscall record will be generated automatically for the
<br>&#x9;     current syscall).
<br>&#x9;  5) Netlink interface to user-space.
<br>&#x9;  6) Support low-overhead kernel-based filtering to minimize the
<br>&#x9;     information that must be passed to user-space.
<br>
<br> Example user-space utilities: http://people.redhat.com/sgrubb/audit/
<br> /*
<br>
<br>#define pr_fmt(fmt) KBUILD_MODNAME &#x22;: &#x22; fmt
<br>
<br>#include &#x3C;linux/file.h&#x3E;
<br>#include &#x3C;linux/init.h&#x3E;
<br>#include &#x3C;linux/types.h&#x3E;
<br>#include &#x3C;linux/atomic.h&#x3E;
<br>#include &#x3C;linux/mm.h&#x3E;
<br>#include &#x3C;linux/export.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/err.h&#x3E;
<br>#include &#x3C;linux/kthread.h&#x3E;
<br>#include &#x3C;linux/kernel.h&#x3E;
<br>#include &#x3C;linux/syscalls.h&#x3E;
<br>
<br>#include &#x3C;linux/audit.h&#x3E;
<br>
<br>#include &#x3C;net/sock.h&#x3E;
<br>#include &#x3C;net/netlink.h&#x3E;
<br>#include &#x3C;linux/skbuff.h&#x3E;
<br>#ifdef CONFIG_SECURITY
<br>#include &#x3C;linux/security.h&#x3E;
<br>#endif
<br>#include &#x3C;linux/freezer.h&#x3E;
<br>#include &#x3C;linux/tty.h&#x3E;
<br>#include &#x3C;linux/pid_namespace.h&#x3E;
<br>#include &#x3C;net/netns/generic.h&#x3E;
<br>
<br>#include &#x22;audit.h&#x22;
<br>
<br>*/ No auditing will take place until audit_initialized == AUDIT_INITIALIZED.
<br> (Initialization happens after skb_init is called.) /*
<br>#define AUDIT_DISABLED&#x9;&#x9;-1
<br>#define AUDIT_UNINITIALIZED&#x9;0
<br>#define AUDIT_INITIALIZED&#x9;1
<br>static int&#x9;audit_initialized;
<br>
<br>#define AUDIT_OFF&#x9;0
<br>#define AUDIT_ON&#x9;1
<br>#define AUDIT_LOCKED&#x9;2
<br>u32&#x9;&#x9;audit_enabled;
<br>u32&#x9;&#x9;audit_ever_enabled;
<br>
<br>EXPORT_SYMBOL_GPL(audit_enabled);
<br>
<br>*/ Default state when kernel boots without any parameters. /*
<br>static u32&#x9;audit_default;
<br>
<br>*/ If auditing cannot proceed, audit_failure selects what happens. /*
<br>static u32&#x9;audit_failure = AUDIT_FAIL_PRINTK;
<br>
<br>*/
<br> If audit records are to be written to the netlink socket, audit_pid
<br> contains the pid of the auditd process and audit_nlk_portid contains
<br> the portid to use to send netlink messages to that process.
<br> /*
<br>int&#x9;&#x9;audit_pid;
<br>static __u32&#x9;audit_nlk_portid;
<br>
<br>*/ If audit_rate_limit is non-zero, limit the rate of sending audit records
<br> to that number per second.  This prevents DoS attacks, but results in
<br> audit records being dropped. /*
<br>static u32&#x9;audit_rate_limit;
<br>
<br>*/ Number of outstanding audit_buffers allowed.
<br> When set to zero, this means unlimited. /*
<br>static u32&#x9;audit_backlog_limit = 64;
<br>#define AUDIT_BACKLOG_WAIT_TIME (60 HZ)
<br>static u32&#x9;audit_backlog_wait_time_master = AUDIT_BACKLOG_WAIT_TIME;
<br>static u32&#x9;audit_backlog_wait_time = AUDIT_BACKLOG_WAIT_TIME;
<br>
<br>*/ The identity of the user shutting down the audit system. /*
<br>kuid_t&#x9;&#x9;audit_sig_uid = INVALID_UID;
<br>pid_t&#x9;&#x9;audit_sig_pid = -1;
<br>u32&#x9;&#x9;audit_sig_sid = 0;
<br>
<br>*/ Records can be lost in several ways:
<br>   0) [suppressed in audit_alloc]
<br>   1) out of memory in audit_log_start [kmalloc of struct audit_buffer]
<br>   2) out of memory in audit_log_move [alloc_skb]
<br>   3) suppressed due to audit_rate_limit
<br>   4) suppressed due to audit_backlog_limit
<br>/*
<br>static atomic_t    audit_lost = ATOMIC_INIT(0);
<br>
<br>*/ The netlink socket. /*
<br>static struct sockaudit_sock;
<br>static int audit_net_id;
<br>
<br>*/ Hash for inode-based rules /*
<br>struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];
<br>
<br>*/ The audit_freelist is a list of pre-allocated audit buffers (if more
<br> than AUDIT_MAXFREE are in use, the audit buffer is freed instead of
<br> being placed on the freelist). /*
<br>static DEFINE_SPINLOCK(audit_freelist_lock);
<br>static int&#x9;   audit_freelist_count;
<br>static LIST_HEAD(audit_freelist);
<br>
<br>static struct sk_buff_head audit_skb_queue;
<br>*/ queue of skbs to send to auditd when/if it comes back /*
<br>static struct sk_buff_head audit_skb_hold_queue;
<br>static struct task_structkauditd_task;
<br>static DECLARE_WAIT_QUEUE_HEAD(kauditd_wait);
<br>static DECLARE_WAIT_QUEUE_HEAD(audit_backlog_wait);
<br>
<br>static struct audit_features af = {.vers = AUDIT_FEATURE_VERSION,
<br>&#x9;&#x9;&#x9;&#x9;   .mask = -1,
<br>&#x9;&#x9;&#x9;&#x9;   .features = 0,
<br>&#x9;&#x9;&#x9;&#x9;   .lock = 0,};
<br>
<br>static charaudit_feature_names[2] = {
<br>&#x9;&#x22;only_unset_loginuid&#x22;,
<br>&#x9;&#x22;loginuid_immutable&#x22;,
<br>};
<br>
<br>
<br>*/ Serialize requests from userspace. /*
<br>DEFINE_MUTEX(audit_cmd_mutex);
<br>
<br>*/ AUDIT_BUFSIZ is the size of the temporary buffer used for formatting
<br> audit records.  Since printk uses a 1024 byte buffer, this buffer
<br> should be at least that large. /*
<br>#define AUDIT_BUFSIZ 1024
<br>
<br>*/ AUDIT_MAXFREE is the number of empty audit_buffers we keep on the
<br> audit_freelist.  Doing so eliminates many kmalloc/kfree calls. /*
<br>#define AUDIT_MAXFREE  (2*NR_CPUS)
<br>
<br>*/ The audit_buffer is used when formatting an audit record.  The caller
<br> locks briefly to get the record off the freelist or to allocate the
<br> buffer, and locks briefly to send the buffer to the netlink layer or
<br> to place it on a transmit queue.  Multiple audit_buffers can be in
<br> use simultaneously. /*
<br>struct audit_buffer {
<br>&#x9;struct list_head     list;
<br>&#x9;struct sk_buff      skb;&#x9;*/ formatted skb ready to send /*
<br>&#x9;struct audit_contextctx;&#x9;*/ NULL or associated context /*
<br>&#x9;gfp_t&#x9;&#x9;     gfp_mask;
<br>};
<br>
<br>struct audit_reply {
<br>&#x9;__u32 portid;
<br>&#x9;struct netnet;
<br>&#x9;struct sk_buffskb;
<br>};
<br>
<br>static void audit_set_portid(struct audit_bufferab, __u32 portid)
<br>{
<br>&#x9;if (ab) {
<br>&#x9;&#x9;struct nlmsghdrnlh = nlmsg_hdr(ab-&#x3E;skb);
<br>&#x9;&#x9;nlh-&#x3E;nlmsg_pid = portid;
<br>&#x9;}
<br>}
<br>
<br>void audit_panic(const charmessage)
<br>{
<br>&#x9;switch (audit_failure) {
<br>&#x9;case AUDIT_FAIL_SILENT:
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_FAIL_PRINTK:
<br>&#x9;&#x9;if (printk_ratelimit())
<br>&#x9;&#x9;&#x9;pr_err(&#x22;%s\n&#x22;, message);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_FAIL_PANIC:
<br>&#x9;&#x9;*/ test audit_pid since printk is always losey, why bother? /*
<br>&#x9;&#x9;if (audit_pid)
<br>&#x9;&#x9;&#x9;panic(&#x22;audit: %s\n&#x22;, message);
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>}
<br>
<br>static inline int audit_rate_check(void)
<br>{
<br>&#x9;static unsigned long&#x9;last_check = 0;
<br>&#x9;static int&#x9;&#x9;messages   = 0;
<br>&#x9;static DEFINE_SPINLOCK(lock);
<br>&#x9;unsigned long&#x9;&#x9;flags;
<br>&#x9;unsigned long&#x9;&#x9;now;
<br>&#x9;unsigned long&#x9;&#x9;elapsed;
<br>&#x9;int&#x9;&#x9;&#x9;retval&#x9;   = 0;
<br>
<br>&#x9;if (!audit_rate_limit) return 1;
<br>
<br>&#x9;spin_lock_irqsave(&#x26;lock, flags);
<br>&#x9;if (++messages &#x3C; audit_rate_limit) {
<br>&#x9;&#x9;retval = 1;
<br>&#x9;} else {
<br>&#x9;&#x9;now     = jiffies;
<br>&#x9;&#x9;elapsed = now - last_check;
<br>&#x9;&#x9;if (elapsed &#x3E; HZ) {
<br>&#x9;&#x9;&#x9;last_check = now;
<br>&#x9;&#x9;&#x9;messages   = 0;
<br>&#x9;&#x9;&#x9;retval     = 1;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;spin_unlock_irqrestore(&#x26;lock, flags);
<br>
<br>&#x9;return retval;
<br>}
<br>
<br>*/
<br> audit_log_lost - conditionally log lost audit message event
<br> @message: the message stating reason for lost audit message
<br>
<br> Emit at least 1 message per second, even if audit_rate_check is
<br> throttling.
<br> Always increment the lost messages counter.
<br>/*
<br>void audit_log_lost(const charmessage)
<br>{
<br>&#x9;static unsigned long&#x9;last_msg = 0;
<br>&#x9;static DEFINE_SPINLOCK(lock);
<br>&#x9;unsigned long&#x9;&#x9;flags;
<br>&#x9;unsigned long&#x9;&#x9;now;
<br>&#x9;int&#x9;&#x9;&#x9;print;
<br>
<br>&#x9;atomic_inc(&#x26;audit_lost);
<br>
<br>&#x9;print = (audit_failure == AUDIT_FAIL_PANIC || !audit_rate_limit);
<br>
<br>&#x9;if (!print) {
<br>&#x9;&#x9;spin_lock_irqsave(&#x26;lock, flags);
<br>&#x9;&#x9;now = jiffies;
<br>&#x9;&#x9;if (now - last_msg &#x3E; HZ) {
<br>&#x9;&#x9;&#x9;print = 1;
<br>&#x9;&#x9;&#x9;last_msg = now;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;spin_unlock_irqrestore(&#x26;lock, flags);
<br>&#x9;}
<br>
<br>&#x9;if (print) {
<br>&#x9;&#x9;if (printk_ratelimit())
<br>&#x9;&#x9;&#x9;pr_warn(&#x22;audit_lost=%u audit_rate_limit=%u audit_backlog_limit=%u\n&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;atomic_read(&#x26;audit_lost),
<br>&#x9;&#x9;&#x9;&#x9;audit_rate_limit,
<br>&#x9;&#x9;&#x9;&#x9;audit_backlog_limit);
<br>&#x9;&#x9;audit_panic(message);
<br>&#x9;}
<br>}
<br>
<br>static int audit_log_config_change(charfunction_name, u32 new, u32 old,
<br>&#x9;&#x9;&#x9;&#x9;   int allow_changes)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;int rc = 0;
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
<br>&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;return rc;
<br>&#x9;audit_log_format(ab, &#x22;%s=%u old=%u&#x22;, function_name, new, old);
<br>&#x9;audit_log_session_info(ab);
<br>&#x9;rc = audit_log_task_context(ab);
<br>&#x9;if (rc)
<br>&#x9;&#x9;allow_changes = 0;/ Something weird, deny request /*
<br>&#x9;audit_log_format(ab, &#x22; res=%d&#x22;, allow_changes);
<br>&#x9;audit_log_end(ab);
<br>&#x9;return rc;
<br>}
<br>
<br>static int audit_do_config_change(charfunction_name, u32to_change, u32 new)
<br>{
<br>&#x9;int allow_changes, rc = 0;
<br>&#x9;u32 old =to_change;
<br>
<br>&#x9;*/ check if we are locked /*
<br>&#x9;if (audit_enabled == AUDIT_LOCKED)
<br>&#x9;&#x9;allow_changes = 0;
<br>&#x9;else
<br>&#x9;&#x9;allow_changes = 1;
<br>
<br>&#x9;if (audit_enabled != AUDIT_OFF) {
<br>&#x9;&#x9;rc = audit_log_config_change(function_name, new, old, allow_changes);
<br>&#x9;&#x9;if (rc)
<br>&#x9;&#x9;&#x9;allow_changes = 0;
<br>&#x9;}
<br>
<br>&#x9;*/ If we are allowed, make the change /*
<br>&#x9;if (allow_changes == 1)
<br>&#x9;&#x9;*to_change = new;
<br>&#x9;*/ Not allowed, update reason /*
<br>&#x9;else if (rc == 0)
<br>&#x9;&#x9;rc = -EPERM;
<br>&#x9;return rc;
<br>}
<br>
<br>static int audit_set_rate_limit(u32 limit)
<br>{
<br>&#x9;return audit_do_config_change(&#x22;audit_rate_limit&#x22;, &#x26;audit_rate_limit, limit);
<br>}
<br>
<br>static int audit_set_backlog_limit(u32 limit)
<br>{
<br>&#x9;return audit_do_config_change(&#x22;audit_backlog_limit&#x22;, &#x26;audit_backlog_limit, limit);
<br>}
<br>
<br>static int audit_set_backlog_wait_time(u32 timeout)
<br>{
<br>&#x9;return audit_do_config_change(&#x22;audit_backlog_wait_time&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;      &#x26;audit_backlog_wait_time_master, timeout);
<br>}
<br>
<br>static int audit_set_enabled(u32 state)
<br>{
<br>&#x9;int rc;
<br>&#x9;if (state &#x3E; AUDIT_LOCKED)
<br>&#x9;&#x9;return -EINVAL;
<br>
<br>&#x9;rc =  audit_do_config_change(&#x22;audit_enabled&#x22;, &#x26;audit_enabled, state);
<br>&#x9;if (!rc)
<br>&#x9;&#x9;audit_ever_enabled |= !!state;
<br>
<br>&#x9;return rc;
<br>}
<br>
<br>static int audit_set_failure(u32 state)
<br>{
<br>&#x9;if (state != AUDIT_FAIL_SILENT
<br>&#x9;    &#x26;&#x26; state != AUDIT_FAIL_PRINTK
<br>&#x9;    &#x26;&#x26; state != AUDIT_FAIL_PANIC)
<br>&#x9;&#x9;return -EINVAL;
<br>
<br>&#x9;return audit_do_config_change(&#x22;audit_failure&#x22;, &#x26;audit_failure, state);
<br>}
<br>
<br>*/
<br> Queue skbs to be sent to auditd when/if it comes back.  These skbs should
<br> already have been sent via prink/syslog and so if these messages are dropped
<br> it is not a huge concern since we already passed the audit_log_lost()
<br> notification and stuff.  This is just nice to get audit messages during
<br> boot before auditd is running or messages generated while auditd is stopped.
<br> This only holds messages is audit_default is set, aka booting with audit=1
<br> or building your kernel that way.
<br> /*
<br>static void audit_hold_skb(struct sk_buffskb)
<br>{
<br>&#x9;if (audit_default &#x26;&#x26;
<br>&#x9;    (!audit_backlog_limit ||
<br>&#x9;     skb_queue_len(&#x26;audit_skb_hold_queue) &#x3C; audit_backlog_limit))
<br>&#x9;&#x9;skb_queue_tail(&#x26;audit_skb_hold_queue, skb);
<br>&#x9;else
<br>&#x9;&#x9;kfree_skb(skb);
<br>}
<br>
<br>*/
<br> For one reason or another this nlh isn&#x27;t getting delivered to the userspace
<br> audit daemon, just send it to printk.
<br> /*
<br>static void audit_printk_skb(struct sk_buffskb)
<br>{
<br>&#x9;struct nlmsghdrnlh = nlmsg_hdr(skb);
<br>&#x9;chardata = nlmsg_data(nlh);
<br>
<br>&#x9;if (nlh-&#x3E;nlmsg_type != AUDIT_EOE) {
<br>&#x9;&#x9;if (printk_ratelimit())
<br>&#x9;&#x9;&#x9;pr_notice(&#x22;type=%d %s\n&#x22;, nlh-&#x3E;nlmsg_type, data);
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;audit_log_lost(&#x22;printk limit exceeded&#x22;);
<br>&#x9;}
<br>
<br>&#x9;audit_hold_skb(skb);
<br>}
<br>
<br>static void kauditd_send_skb(struct sk_buffskb)
<br>{
<br>&#x9;int err;
<br>&#x9;int attempts = 0;
<br>#define AUDITD_RETRIES 5
<br>
<br>restart:
<br>&#x9;*/ take a reference in case we can&#x27;t send it and we want to hold it /*
<br>&#x9;skb_get(skb);
<br>&#x9;err = netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
<br>&#x9;if (err &#x3C; 0) {
<br>&#x9;&#x9;pr_err(&#x22;netlink_unicast sending to audit_pid=%d returned error: %d\n&#x22;,
<br>&#x9;&#x9;       audit_pid, err);
<br>&#x9;&#x9;if (audit_pid) {
<br>&#x9;&#x9;&#x9;if (err == -ECONNREFUSED || err == -EPERM
<br>&#x9;&#x9;&#x9;    || ++attempts &#x3E;= AUDITD_RETRIES) {
<br>&#x9;&#x9;&#x9;&#x9;char s[32];
<br>
<br>&#x9;&#x9;&#x9;&#x9;snprintf(s, sizeof(s), &#x22;audit_pid=%d reset&#x22;, audit_pid);
<br>&#x9;&#x9;&#x9;&#x9;audit_log_lost(s);
<br>&#x9;&#x9;&#x9;&#x9;audit_pid = 0;
<br>&#x9;&#x9;&#x9;&#x9;audit_sock = NULL;
<br>&#x9;&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;&#x9;pr_warn(&#x22;re-scheduling(#%d) write to audit_pid=%d\n&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;attempts, audit_pid);
<br>&#x9;&#x9;&#x9;&#x9;set_current_state(TASK_INTERRUPTIBLE);
<br>&#x9;&#x9;&#x9;&#x9;schedule();
<br>&#x9;&#x9;&#x9;&#x9;__set_current_state(TASK_RUNNING);
<br>&#x9;&#x9;&#x9;&#x9;goto restart;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;&#x9;*/ we might get lucky and get this in the next auditd /*
<br>&#x9;&#x9;audit_hold_skb(skb);
<br>&#x9;} else
<br>&#x9;&#x9;*/ drop the extra reference if sent ok /*
<br>&#x9;&#x9;consume_skb(skb);
<br>}
<br>
<br>*/
<br> kauditd_send_multicast_skb - send the skb to multicast userspace listeners
<br>
<br> This function doesn&#x27;t consume an skb as might be expected since it has to
<br> copy it anyways.
<br> /*
<br>static void kauditd_send_multicast_skb(struct sk_buffskb, gfp_t gfp_mask)
<br>{
<br>&#x9;struct sk_buff&#x9;&#x9;*copy;
<br>&#x9;struct audit_net&#x9;*aunet = net_generic(&#x26;init_net, audit_net_id);
<br>&#x9;struct sock&#x9;&#x9;*sock = aunet-&#x3E;nlsk;
<br>
<br>&#x9;if (!netlink_has_listeners(sock, AUDIT_NLGRP_READLOG))
<br>&#x9;&#x9;return;
<br>
<br>&#x9;*/
<br>&#x9; The seemingly wasteful skb_copy() rather than bumping the refcount
<br>&#x9; using skb_get() is necessary because non-standard mods are made to
<br>&#x9; the skb by the original kaudit unicast socket send routine.  The
<br>&#x9; existing auditd daemon assumes this breakage.  Fixing this would
<br>&#x9; require co-ordinating a change in the established protocol between
<br>&#x9; the kaudit kernel subsystem and the auditd userspace code.  There is
<br>&#x9; no reason for new multicast clients to continue with this
<br>&#x9; non-compliance.
<br>&#x9; /*
<br>&#x9;copy = skb_copy(skb, gfp_mask);
<br>&#x9;if (!copy)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;nlmsg_multicast(sock, copy, 0, AUDIT_NLGRP_READLOG, gfp_mask);
<br>}
<br>
<br>*/
<br> flush_hold_queue - empty the hold queue if auditd appears
<br>
<br> If auditd just started, drain the queue of messages already
<br> sent to syslog/printk.  Remember loss here is ok.  We already
<br> called audit_log_lost() if it didn&#x27;t go out normally.  so the
<br> race between the skb_dequeue and the next check for audit_pid
<br> doesn&#x27;t matter.
<br>
<br> If you ever find kauditd to be too slow we can get a perf win
<br> by doing our own locking and keeping better track if there
<br> are messages in this queue.  I don&#x27;t see the need now, but
<br> in 5 years when I want to play with this again I&#x27;ll see this
<br> note and still have no friggin idea what i&#x27;m thinking today.
<br> /*
<br>static void flush_hold_queue(void)
<br>{
<br>&#x9;struct sk_buffskb;
<br>
<br>&#x9;if (!audit_default || !audit_pid)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;skb = skb_dequeue(&#x26;audit_skb_hold_queue);
<br>&#x9;if (likely(!skb))
<br>&#x9;&#x9;return;
<br>
<br>&#x9;while (skb &#x26;&#x26; audit_pid) {
<br>&#x9;&#x9;kauditd_send_skb(skb);
<br>&#x9;&#x9;skb = skb_dequeue(&#x26;audit_skb_hold_queue);
<br>&#x9;}
<br>
<br>&#x9;*/
<br>&#x9; if auditd just disappeared but we
<br>&#x9; dequeued an skb we need to drop ref
<br>&#x9; /*
<br>&#x9;consume_skb(skb);
<br>}
<br>
<br>static int kauditd_thread(voiddummy)
<br>{
<br>&#x9;set_freezable();
<br>&#x9;while (!kthread_should_stop()) {
<br>&#x9;&#x9;struct sk_buffskb;
<br>
<br>&#x9;&#x9;flush_hold_queue();
<br>
<br>&#x9;&#x9;skb = skb_dequeue(&#x26;audit_skb_queue);
<br>
<br>&#x9;&#x9;if (skb) {
<br>&#x9;&#x9;&#x9;if (!audit_backlog_limit ||
<br>&#x9;&#x9;&#x9;    (skb_queue_len(&#x26;audit_skb_queue) &#x3C;= audit_backlog_limit))
<br>&#x9;&#x9;&#x9;&#x9;wake_up(&#x26;audit_backlog_wait);
<br>&#x9;&#x9;&#x9;if (audit_pid)
<br>&#x9;&#x9;&#x9;&#x9;kauditd_send_skb(skb);
<br>&#x9;&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;&#x9;audit_printk_skb(skb);
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;wait_event_freezable(kauditd_wait, skb_queue_len(&#x26;audit_skb_queue));
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>int audit_send_list(void_dest)
<br>{
<br>&#x9;struct audit_netlink_listdest = _dest;
<br>&#x9;struct sk_buffskb;
<br>&#x9;struct netnet = dest-&#x3E;net;
<br>&#x9;struct audit_netaunet = net_generic(net, audit_net_id);
<br>
<br>&#x9;*/ wait for parent to finish and send an ACK /*
<br>&#x9;mutex_lock(&#x26;audit_cmd_mutex);
<br>&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
<br>
<br>&#x9;while ((skb = __skb_dequeue(&#x26;dest-&#x3E;q)) != NULL)
<br>&#x9;&#x9;netlink_unicast(aunet-&#x3E;nlsk, skb, dest-&#x3E;portid, 0);
<br>
<br>&#x9;put_net(net);
<br>&#x9;kfree(dest);
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>struct sk_buffaudit_make_reply(__u32 portid, int seq, int type, int done,
<br>&#x9;&#x9;&#x9;&#x9; int multi, const voidpayload, int size)
<br>{
<br>&#x9;struct sk_buff&#x9;*skb;
<br>&#x9;struct nlmsghdr&#x9;*nlh;
<br>&#x9;void&#x9;&#x9;*data;
<br>&#x9;int&#x9;&#x9;flags = multi ? NLM_F_MULTI : 0;
<br>&#x9;int&#x9;&#x9;t     = done  ? NLMSG_DONE  : type;
<br>
<br>&#x9;skb = nlmsg_new(size, GFP_KERNEL);
<br>&#x9;if (!skb)
<br>&#x9;&#x9;return NULL;
<br>
<br>&#x9;nlh&#x9;= nlmsg_put(skb, portid, seq, t, size, flags);
<br>&#x9;if (!nlh)
<br>&#x9;&#x9;goto out_kfree_skb;
<br>&#x9;data = nlmsg_data(nlh);
<br>&#x9;memcpy(data, payload, size);
<br>&#x9;return skb;
<br>
<br>out_kfree_skb:
<br>&#x9;kfree_skb(skb);
<br>&#x9;return NULL;
<br>}
<br>
<br>static int audit_send_reply_thread(voidarg)
<br>{
<br>&#x9;struct audit_replyreply = (struct audit_reply)arg;
<br>&#x9;struct netnet = reply-&#x3E;net;
<br>&#x9;struct audit_netaunet = net_generic(net, audit_net_id);
<br>
<br>&#x9;mutex_lock(&#x26;audit_cmd_mutex);
<br>&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
<br>
<br>&#x9;*/ Ignore failure. It&#x27;ll only happen if the sender goes away,
<br>&#x9;   because our timeout is set to infinite. /*
<br>&#x9;netlink_unicast(aunet-&#x3E;nlsk , reply-&#x3E;skb, reply-&#x3E;portid, 0);
<br>&#x9;put_net(net);
<br>&#x9;kfree(reply);
<br>&#x9;return 0;
<br>}
<br>*/
<br> audit_send_reply - send an audit reply message via netlink
<br> @request_skb: skb of request we are replying to (used to target the reply)
<br> @seq: sequence number
<br> @type: audit message type
<br> @done: done (last) flag
<br> @multi: multi-part message flag
<br> @payload: payload data
<br> @size: payload size
<br>
<br> Allocates an skb, builds the netlink message, and sends it to the port id.
<br> No failure notifications.
<br> /*
<br>static void audit_send_reply(struct sk_buffrequest_skb, int seq, int type, int done,
<br>&#x9;&#x9;&#x9;     int multi, const voidpayload, int size)
<br>{
<br>&#x9;u32 portid = NETLINK_CB(request_skb).portid;
<br>&#x9;struct netnet = sock_net(NETLINK_CB(request_skb).sk);
<br>&#x9;struct sk_buffskb;
<br>&#x9;struct task_structtsk;
<br>&#x9;struct audit_replyreply = kmalloc(sizeof(struct audit_reply),
<br>&#x9;&#x9;&#x9;&#x9;&#x9;    GFP_KERNEL);
<br>
<br>&#x9;if (!reply)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;skb = audit_make_reply(portid, seq, type, done, multi, payload, size);
<br>&#x9;if (!skb)
<br>&#x9;&#x9;goto out;
<br>
<br>&#x9;reply-&#x3E;net = get_net(net);
<br>&#x9;reply-&#x3E;portid = portid;
<br>&#x9;reply-&#x3E;skb = skb;
<br>
<br>&#x9;tsk = kthread_run(audit_send_reply_thread, reply, &#x22;audit_send_reply&#x22;);
<br>&#x9;if (!IS_ERR(tsk))
<br>&#x9;&#x9;return;
<br>&#x9;kfree_skb(skb);
<br>out:
<br>&#x9;kfree(reply);
<br>}
<br>
<br>*/
<br> Check for appropriate CAP_AUDIT_ capabilities on incoming audit
<br> control messages.
<br> /*
<br>static int audit_netlink_ok(struct sk_buffskb, u16 msg_type)
<br>{
<br>&#x9;int err = 0;
<br>
<br>&#x9;*/ Only support initial user namespace for now. /*
<br>&#x9;*/
<br>&#x9; We return ECONNREFUSED because it tricks userspace into thinking
<br>&#x9; that audit was not configured into the kernel.  Lots of users
<br>&#x9; configure their PAM stack (because that&#x27;s what the distro does)
<br>&#x9; to reject login if unable to send messages to audit.  If we return
<br>&#x9; ECONNREFUSED the PAM stack thinks the kernel does not have audit
<br>&#x9; configured in and will let login proceed.  If we return EPERM
<br>&#x9; userspace will reject all logins.  This should be removed when we
<br>&#x9; support non init namespaces!!
<br>&#x9; /*
<br>&#x9;if (current_user_ns() != &#x26;init_user_ns)
<br>&#x9;&#x9;return -ECONNREFUSED;
<br>
<br>&#x9;switch (msg_type) {
<br>&#x9;case AUDIT_LIST:
<br>&#x9;case AUDIT_ADD:
<br>&#x9;case AUDIT_DEL:
<br>&#x9;&#x9;return -EOPNOTSUPP;
<br>&#x9;case AUDIT_GET:
<br>&#x9;case AUDIT_SET:
<br>&#x9;case AUDIT_GET_FEATURE:
<br>&#x9;case AUDIT_SET_FEATURE:
<br>&#x9;case AUDIT_LIST_RULES:
<br>&#x9;case AUDIT_ADD_RULE:
<br>&#x9;case AUDIT_DEL_RULE:
<br>&#x9;case AUDIT_SIGNAL_INFO:
<br>&#x9;case AUDIT_TTY_GET:
<br>&#x9;case AUDIT_TTY_SET:
<br>&#x9;case AUDIT_TRIM:
<br>&#x9;case AUDIT_MAKE_EQUIV:
<br>&#x9;&#x9;*/ Only support auditd and auditctl in initial pid namespace
<br>&#x9;&#x9; for now. /*
<br>&#x9;&#x9;if (task_active_pid_ns(current) != &#x26;init_pid_ns)
<br>&#x9;&#x9;&#x9;return -EPERM;
<br>
<br>&#x9;&#x9;if (!netlink_capable(skb, CAP_AUDIT_CONTROL))
<br>&#x9;&#x9;&#x9;err = -EPERM;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_USER:
<br>&#x9;case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
<br>&#x9;case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
<br>&#x9;&#x9;if (!netlink_capable(skb, CAP_AUDIT_WRITE))
<br>&#x9;&#x9;&#x9;err = -EPERM;
<br>&#x9;&#x9;break;
<br>&#x9;default: / bad msg /*
<br>&#x9;&#x9;err = -EINVAL;
<br>&#x9;}
<br>
<br>&#x9;return err;
<br>}
<br>
<br>static void audit_log_common_recv_msg(struct audit_buffer*ab, u16 msg_type)
<br>{
<br>&#x9;uid_t uid = from_kuid(&#x26;init_user_ns, current_uid());
<br>&#x9;pid_t pid = task_tgid_nr(current);
<br>
<br>&#x9;if (!audit_enabled &#x26;&#x26; msg_type != AUDIT_USER_AVC) {
<br>&#x9;&#x9;*ab = NULL;
<br>&#x9;&#x9;return;
<br>&#x9;}
<br>
<br>&#x9;*ab = audit_log_start(NULL, GFP_KERNEL, msg_type);
<br>&#x9;if (unlikely(!*ab))
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_format(*ab, &#x22;pid=%d uid=%u&#x22;, pid, uid);
<br>&#x9;audit_log_session_info(*ab);
<br>&#x9;audit_log_task_context(*ab);
<br>}
<br>
<br>int is_audit_feature_set(int i)
<br>{
<br>&#x9;return af.features &#x26; AUDIT_FEATURE_TO_MASK(i);
<br>}
<br>
<br>
<br>static int audit_get_feature(struct sk_buffskb)
<br>{
<br>&#x9;u32 seq;
<br>
<br>&#x9;seq = nlmsg_hdr(skb)-&#x3E;nlmsg_seq;
<br>
<br>&#x9;audit_send_reply(skb, seq, AUDIT_GET_FEATURE, 0, 0, &#x26;af, sizeof(af));
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>static void audit_log_feature_change(int which, u32 old_feature, u32 new_feature,
<br>&#x9;&#x9;&#x9;&#x9;     u32 old_lock, u32 new_lock, int res)
<br>{
<br>&#x9;struct audit_bufferab;
<br>
<br>&#x9;if (audit_enabled == AUDIT_OFF)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_FEATURE_CHANGE);
<br>&#x9;audit_log_task_info(ab, current);
<br>&#x9;audit_log_format(ab, &#x22; feature=%s old=%u new=%u old_lock=%u new_lock=%u res=%d&#x22;,
<br>&#x9;&#x9;&#x9; audit_feature_names[which], !!old_feature, !!new_feature,
<br>&#x9;&#x9;&#x9; !!old_lock, !!new_lock, res);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>static int audit_set_feature(struct sk_buffskb)
<br>{
<br>&#x9;struct audit_featuresuaf;
<br>&#x9;int i;
<br>
<br>&#x9;BUILD_BUG_ON(AUDIT_LAST_FEATURE + 1 &#x3E; ARRAY_SIZE(audit_feature_names));
<br>&#x9;uaf = nlmsg_data(nlmsg_hdr(skb));
<br>
<br>&#x9;*/ if there is ever a version 2 we should handle that here /*
<br>
<br>&#x9;for (i = 0; i &#x3C;= AUDIT_LAST_FEATURE; i++) {
<br>&#x9;&#x9;u32 feature = AUDIT_FEATURE_TO_MASK(i);
<br>&#x9;&#x9;u32 old_feature, new_feature, old_lock, new_lock;
<br>
<br>&#x9;&#x9;*/ if we are not changing this feature, move along /*
<br>&#x9;&#x9;if (!(feature &#x26; uaf-&#x3E;mask))
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;old_feature = af.features &#x26; feature;
<br>&#x9;&#x9;new_feature = uaf-&#x3E;features &#x26; feature;
<br>&#x9;&#x9;new_lock = (uaf-&#x3E;lock | af.lock) &#x26; feature;
<br>&#x9;&#x9;old_lock = af.lock &#x26; feature;
<br>
<br>&#x9;&#x9;*/ are we changing a locked feature? /*
<br>&#x9;&#x9;if (old_lock &#x26;&#x26; (new_feature != old_feature)) {
<br>&#x9;&#x9;&#x9;audit_log_feature_change(i, old_feature, new_feature,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; old_lock, new_lock, 0);
<br>&#x9;&#x9;&#x9;return -EPERM;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;*/ nothing invalid, do the changes /*
<br>&#x9;for (i = 0; i &#x3C;= AUDIT_LAST_FEATURE; i++) {
<br>&#x9;&#x9;u32 feature = AUDIT_FEATURE_TO_MASK(i);
<br>&#x9;&#x9;u32 old_feature, new_feature, old_lock, new_lock;
<br>
<br>&#x9;&#x9;*/ if we are not changing this feature, move along /*
<br>&#x9;&#x9;if (!(feature &#x26; uaf-&#x3E;mask))
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;old_feature = af.features &#x26; feature;
<br>&#x9;&#x9;new_feature = uaf-&#x3E;features &#x26; feature;
<br>&#x9;&#x9;old_lock = af.lock &#x26; feature;
<br>&#x9;&#x9;new_lock = (uaf-&#x3E;lock | af.lock) &#x26; feature;
<br>
<br>&#x9;&#x9;if (new_feature != old_feature)
<br>&#x9;&#x9;&#x9;audit_log_feature_change(i, old_feature, new_feature,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; old_lock, new_lock, 1);
<br>
<br>&#x9;&#x9;if (new_feature)
<br>&#x9;&#x9;&#x9;af.features |= feature;
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;af.features &#x26;= ~feature;
<br>&#x9;&#x9;af.lock |= new_lock;
<br>&#x9;}
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>static int audit_replace(pid_t pid)
<br>{
<br>&#x9;struct sk_buffskb = audit_make_reply(0, 0, AUDIT_REPLACE, 0, 0,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       &#x26;pid, sizeof(pid));
<br>
<br>&#x9;if (!skb)
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;return netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
<br>}
<br>
<br>static int audit_receive_msg(struct sk_buffskb, struct nlmsghdrnlh)
<br>{
<br>&#x9;u32&#x9;&#x9;&#x9;seq;
<br>&#x9;void&#x9;&#x9;&#x9;*data;
<br>&#x9;int&#x9;&#x9;&#x9;err;
<br>&#x9;struct audit_buffer&#x9;*ab;
<br>&#x9;u16&#x9;&#x9;&#x9;msg_type = nlh-&#x3E;nlmsg_type;
<br>&#x9;struct audit_sig_info  sig_data;
<br>&#x9;char&#x9;&#x9;&#x9;*ctx = NULL;
<br>&#x9;u32&#x9;&#x9;&#x9;len;
<br>
<br>&#x9;err = audit_netlink_ok(skb, msg_type);
<br>&#x9;if (err)
<br>&#x9;&#x9;return err;
<br>
<br>&#x9;*/ As soon as there&#x27;s any sign of userspace auditd,
<br>&#x9; start kauditd to talk to it /*
<br>&#x9;if (!kauditd_task) {
<br>&#x9;&#x9;kauditd_task = kthread_run(kauditd_thread, NULL, &#x22;kauditd&#x22;);
<br>&#x9;&#x9;if (IS_ERR(kauditd_task)) {
<br>&#x9;&#x9;&#x9;err = PTR_ERR(kauditd_task);
<br>&#x9;&#x9;&#x9;kauditd_task = NULL;
<br>&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;seq  = nlh-&#x3E;nlmsg_seq;
<br>&#x9;data = nlmsg_data(nlh);
<br>
<br>&#x9;switch (msg_type) {
<br>&#x9;case AUDIT_GET: {
<br>&#x9;&#x9;struct audit_status&#x9;s;
<br>&#x9;&#x9;memset(&#x26;s, 0, sizeof(s));
<br>&#x9;&#x9;s.enabled&#x9;&#x9;= audit_enabled;
<br>&#x9;&#x9;s.failure&#x9;&#x9;= audit_failure;
<br>&#x9;&#x9;s.pid&#x9;&#x9;&#x9;= audit_pid;
<br>&#x9;&#x9;s.rate_limit&#x9;&#x9;= audit_rate_limit;
<br>&#x9;&#x9;s.backlog_limit&#x9;&#x9;= audit_backlog_limit;
<br>&#x9;&#x9;s.lost&#x9;&#x9;&#x9;= atomic_read(&#x26;audit_lost);
<br>&#x9;&#x9;s.backlog&#x9;&#x9;= skb_queue_len(&#x26;audit_skb_queue);
<br>&#x9;&#x9;s.feature_bitmap&#x9;= AUDIT_FEATURE_BITMAP_ALL;
<br>&#x9;&#x9;s.backlog_wait_time&#x9;= audit_backlog_wait_time_master;
<br>&#x9;&#x9;audit_send_reply(skb, seq, AUDIT_GET, 0, 0, &#x26;s, sizeof(s));
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;case AUDIT_SET: {
<br>&#x9;&#x9;struct audit_status&#x9;s;
<br>&#x9;&#x9;memset(&#x26;s, 0, sizeof(s));
<br>&#x9;&#x9;*/ guard against past and future API changes /*
<br>&#x9;&#x9;memcpy(&#x26;s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
<br>&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_ENABLED) {
<br>&#x9;&#x9;&#x9;err = audit_set_enabled(s.enabled);
<br>&#x9;&#x9;&#x9;if (err &#x3C; 0)
<br>&#x9;&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_FAILURE) {
<br>&#x9;&#x9;&#x9;err = audit_set_failure(s.failure);
<br>&#x9;&#x9;&#x9;if (err &#x3C; 0)
<br>&#x9;&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_PID) {
<br>&#x9;&#x9;&#x9;int new_pid = s.pid;
<br>&#x9;&#x9;&#x9;pid_t requesting_pid = task_tgid_vnr(current);
<br>
<br>&#x9;&#x9;&#x9;if ((!new_pid) &#x26;&#x26; (requesting_pid != audit_pid)) {
<br>&#x9;&#x9;&#x9;&#x9;audit_log_config_change(&#x22;audit_pid&#x22;, new_pid, audit_pid, 0);
<br>&#x9;&#x9;&#x9;&#x9;return -EACCES;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;if (audit_pid &#x26;&#x26; new_pid &#x26;&#x26;
<br>&#x9;&#x9;&#x9;    audit_replace(requesting_pid) != -ECONNREFUSED) {
<br>&#x9;&#x9;&#x9;&#x9;audit_log_config_change(&#x22;audit_pid&#x22;, new_pid, audit_pid, 0);
<br>&#x9;&#x9;&#x9;&#x9;return -EEXIST;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;if (audit_enabled != AUDIT_OFF)
<br>&#x9;&#x9;&#x9;&#x9;audit_log_config_change(&#x22;audit_pid&#x22;, new_pid, audit_pid, 1);
<br>&#x9;&#x9;&#x9;audit_pid = new_pid;
<br>&#x9;&#x9;&#x9;audit_nlk_portid = NETLINK_CB(skb).portid;
<br>&#x9;&#x9;&#x9;audit_sock = skb-&#x3E;sk;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_RATE_LIMIT) {
<br>&#x9;&#x9;&#x9;err = audit_set_rate_limit(s.rate_limit);
<br>&#x9;&#x9;&#x9;if (err &#x3C; 0)
<br>&#x9;&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_BACKLOG_LIMIT) {
<br>&#x9;&#x9;&#x9;err = audit_set_backlog_limit(s.backlog_limit);
<br>&#x9;&#x9;&#x9;if (err &#x3C; 0)
<br>&#x9;&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_BACKLOG_WAIT_TIME) {
<br>&#x9;&#x9;&#x9;if (sizeof(s) &#x3E; (size_t)nlh-&#x3E;nlmsg_len)
<br>&#x9;&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;&#x9;if (s.backlog_wait_time &#x3E; 10*AUDIT_BACKLOG_WAIT_TIME)
<br>&#x9;&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;&#x9;err = audit_set_backlog_wait_time(s.backlog_wait_time);
<br>&#x9;&#x9;&#x9;if (err &#x3C; 0)
<br>&#x9;&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;case AUDIT_GET_FEATURE:
<br>&#x9;&#x9;err = audit_get_feature(skb);
<br>&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_SET_FEATURE:
<br>&#x9;&#x9;err = audit_set_feature(skb);
<br>&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_USER:
<br>&#x9;case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
<br>&#x9;case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
<br>&#x9;&#x9;if (!audit_enabled &#x26;&#x26; msg_type != AUDIT_USER_AVC)
<br>&#x9;&#x9;&#x9;return 0;
<br>
<br>&#x9;&#x9;err = audit_filter_user(msg_type);
<br>&#x9;&#x9;if (err == 1) {/ match or error /*
<br>&#x9;&#x9;&#x9;err = 0;
<br>&#x9;&#x9;&#x9;if (msg_type == AUDIT_USER_TTY) {
<br>&#x9;&#x9;&#x9;&#x9;err = tty_audit_push();
<br>&#x9;&#x9;&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
<br>&#x9;&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, msg_type);
<br>&#x9;&#x9;&#x9;if (msg_type != AUDIT_USER_TTY)
<br>&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; msg=&#x27;%.*s&#x27;&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; AUDIT_MESSAGE_TEXT_MAX,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; (char)data);
<br>&#x9;&#x9;&#x9;else {
<br>&#x9;&#x9;&#x9;&#x9;int size;
<br>
<br>&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; data=&#x22;);
<br>&#x9;&#x9;&#x9;&#x9;size = nlmsg_len(nlh);
<br>&#x9;&#x9;&#x9;&#x9;if (size &#x3E; 0 &#x26;&#x26;
<br>&#x9;&#x9;&#x9;&#x9;    ((unsigned char)data)[size - 1] == &#x27;\0&#x27;)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;size--;
<br>&#x9;&#x9;&#x9;&#x9;audit_log_n_untrustedstring(ab, data, size);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;audit_set_portid(ab, NETLINK_CB(skb).portid);
<br>&#x9;&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_cmd_mutex);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_ADD_RULE:
<br>&#x9;case AUDIT_DEL_RULE:
<br>&#x9;&#x9;if (nlmsg_len(nlh) &#x3C; sizeof(struct audit_rule_data))
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;if (audit_enabled == AUDIT_LOCKED) {
<br>&#x9;&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; audit_enabled=%d res=0&#x22;, audit_enabled);
<br>&#x9;&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;&#x9;return -EPERM;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;err = audit_rule_change(msg_type, NETLINK_CB(skb).portid,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;   seq, data, nlmsg_len(nlh));
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_LIST_RULES:
<br>&#x9;&#x9;err = audit_list_rules_send(skb, seq);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_TRIM:
<br>&#x9;&#x9;audit_trim_trees();
<br>&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
<br>&#x9;&#x9;audit_log_format(ab, &#x22; op=trim res=1&#x22;);
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_MAKE_EQUIV: {
<br>&#x9;&#x9;voidbufp = data;
<br>&#x9;&#x9;u32 sizes[2];
<br>&#x9;&#x9;size_t msglen = nlmsg_len(nlh);
<br>&#x9;&#x9;charold,new;
<br>
<br>&#x9;&#x9;err = -EINVAL;
<br>&#x9;&#x9;if (msglen &#x3C; 2 sizeof(u32))
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;memcpy(sizes, bufp, 2 sizeof(u32));
<br>&#x9;&#x9;bufp += 2 sizeof(u32);
<br>&#x9;&#x9;msglen -= 2 sizeof(u32);
<br>&#x9;&#x9;old = audit_unpack_string(&#x26;bufp, &#x26;msglen, sizes[0]);
<br>&#x9;&#x9;if (IS_ERR(old)) {
<br>&#x9;&#x9;&#x9;err = PTR_ERR(old);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;new = audit_unpack_string(&#x26;bufp, &#x26;msglen, sizes[1]);
<br>&#x9;&#x9;if (IS_ERR(new)) {
<br>&#x9;&#x9;&#x9;err = PTR_ERR(new);
<br>&#x9;&#x9;&#x9;kfree(old);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;*/ OK, here comes... /*
<br>&#x9;&#x9;err = audit_tag_tree(old, new);
<br>
<br>&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
<br>
<br>&#x9;&#x9;audit_log_format(ab, &#x22; op=make_equiv old=&#x22;);
<br>&#x9;&#x9;audit_log_untrustedstring(ab, old);
<br>&#x9;&#x9;audit_log_format(ab, &#x22; new=&#x22;);
<br>&#x9;&#x9;audit_log_untrustedstring(ab, new);
<br>&#x9;&#x9;audit_log_format(ab, &#x22; res=%d&#x22;, !err);
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;kfree(old);
<br>&#x9;&#x9;kfree(new);
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;case AUDIT_SIGNAL_INFO:
<br>&#x9;&#x9;len = 0;
<br>&#x9;&#x9;if (audit_sig_sid) {
<br>&#x9;&#x9;&#x9;err = security_secid_to_secctx(audit_sig_sid, &#x26;ctx, &#x26;len);
<br>&#x9;&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;sig_data = kmalloc(sizeof(*sig_data) + len, GFP_KERNEL);
<br>&#x9;&#x9;if (!sig_data) {
<br>&#x9;&#x9;&#x9;if (audit_sig_sid)
<br>&#x9;&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
<br>&#x9;&#x9;&#x9;return -ENOMEM;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;sig_data-&#x3E;uid = from_kuid(&#x26;init_user_ns, audit_sig_uid);
<br>&#x9;&#x9;sig_data-&#x3E;pid = audit_sig_pid;
<br>&#x9;&#x9;if (audit_sig_sid) {
<br>&#x9;&#x9;&#x9;memcpy(sig_data-&#x3E;ctx, ctx, len);
<br>&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;audit_send_reply(skb, seq, AUDIT_SIGNAL_INFO, 0, 0,
<br>&#x9;&#x9;&#x9;&#x9; sig_data, sizeof(*sig_data) + len);
<br>&#x9;&#x9;kfree(sig_data);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_TTY_GET: {
<br>&#x9;&#x9;struct audit_tty_status s;
<br>&#x9;&#x9;unsigned int t;
<br>
<br>&#x9;&#x9;t = READ_ONCE(current-&#x3E;signal-&#x3E;audit_tty);
<br>&#x9;&#x9;s.enabled = t &#x26; AUDIT_TTY_ENABLE;
<br>&#x9;&#x9;s.log_passwd = !!(t &#x26; AUDIT_TTY_LOG_PASSWD);
<br>
<br>&#x9;&#x9;audit_send_reply(skb, seq, AUDIT_TTY_GET, 0, 0, &#x26;s, sizeof(s));
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;case AUDIT_TTY_SET: {
<br>&#x9;&#x9;struct audit_tty_status s, old;
<br>&#x9;&#x9;struct audit_buffer&#x9;*ab;
<br>&#x9;&#x9;unsigned int t;
<br>
<br>&#x9;&#x9;memset(&#x26;s, 0, sizeof(s));
<br>&#x9;&#x9;*/ guard against past and future API changes /*
<br>&#x9;&#x9;memcpy(&#x26;s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
<br>&#x9;&#x9;*/ check if new data is valid /*
<br>&#x9;&#x9;if ((s.enabled != 0 &#x26;&#x26; s.enabled != 1) ||
<br>&#x9;&#x9;    (s.log_passwd != 0 &#x26;&#x26; s.log_passwd != 1))
<br>&#x9;&#x9;&#x9;err = -EINVAL;
<br>
<br>&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;t = READ_ONCE(current-&#x3E;signal-&#x3E;audit_tty);
<br>&#x9;&#x9;else {
<br>&#x9;&#x9;&#x9;t = s.enabled | (-s.log_passwd &#x26; AUDIT_TTY_LOG_PASSWD);
<br>&#x9;&#x9;&#x9;t = xchg(&#x26;current-&#x3E;signal-&#x3E;audit_tty, t);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;old.enabled = t &#x26; AUDIT_TTY_ENABLE;
<br>&#x9;&#x9;old.log_passwd = !!(t &#x26; AUDIT_TTY_LOG_PASSWD);
<br>
<br>&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
<br>&#x9;&#x9;audit_log_format(ab, &#x22; op=tty_set old-enabled=%d new-enabled=%d&#x22;
<br>&#x9;&#x9;&#x9;&#x9; &#x22; old-log_passwd=%d new-log_passwd=%d res=%d&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; old.enabled, s.enabled, old.log_passwd,
<br>&#x9;&#x9;&#x9;&#x9; s.log_passwd, !err);
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;default:
<br>&#x9;&#x9;err = -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>
<br>&#x9;return err &#x3C; 0 ? err : 0;
<br>}
<br>
<br>*/
<br> Get message from skb.  Each message is processed by audit_receive_msg.
<br> Malformed skbs with wrong length are discarded silently.
<br> /*
<br>static void audit_receive_skb(struct sk_buffskb)
<br>{
<br>&#x9;struct nlmsghdrnlh;
<br>&#x9;*/
<br>&#x9; len MUST be signed for nlmsg_next to be able to dec it below 0
<br>&#x9; if the nlmsg_len was not aligned
<br>&#x9; /*
<br>&#x9;int len;
<br>&#x9;int err;
<br>
<br>&#x9;nlh = nlmsg_hdr(skb);
<br>&#x9;len = skb-&#x3E;len;
<br>
<br>&#x9;while (nlmsg_ok(nlh, len)) {
<br>&#x9;&#x9;err = audit_receive_msg(skb, nlh);
<br>&#x9;&#x9;*/ if err or if this message says it wants a response /*
<br>&#x9;&#x9;if (err || (nlh-&#x3E;nlmsg_flags &#x26; NLM_F_ACK))
<br>&#x9;&#x9;&#x9;netlink_ack(skb, nlh, err);
<br>
<br>&#x9;&#x9;nlh = nlmsg_next(nlh, &#x26;len);
<br>&#x9;}
<br>}
<br>
<br>*/ Receive messages from netlink socket. /*
<br>static void audit_receive(struct sk_buff skb)
<br>{
<br>&#x9;mutex_lock(&#x26;audit_cmd_mutex);
<br>&#x9;audit_receive_skb(skb);
<br>&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
<br>}
<br>
<br>*/ Run custom bind function on netlink socket group connect or bind requests. /*
<br>static int audit_bind(struct netnet, int group)
<br>{
<br>&#x9;if (!capable(CAP_AUDIT_READ))
<br>&#x9;&#x9;return -EPERM;
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>static int __net_init audit_net_init(struct netnet)
<br>{
<br>&#x9;struct netlink_kernel_cfg cfg = {
<br>&#x9;&#x9;.input&#x9;= audit_receive,
<br>&#x9;&#x9;.bind&#x9;= audit_bind,
<br>&#x9;&#x9;.flags&#x9;= NL_CFG_F_NONROOT_RECV,
<br>&#x9;&#x9;.groups&#x9;= AUDIT_NLGRP_MAX,
<br>&#x9;};
<br>
<br>&#x9;struct audit_netaunet = net_generic(net, audit_net_id);
<br>
<br>&#x9;aunet-&#x3E;nlsk = netlink_kernel_create(net, NETLINK_AUDIT, &#x26;cfg);
<br>&#x9;if (aunet-&#x3E;nlsk == NULL) {
<br>&#x9;&#x9;audit_panic(&#x22;cannot initialize netlink socket in namespace&#x22;);
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;}
<br>&#x9;aunet-&#x3E;nlsk-&#x3E;sk_sndtimeo = MAX_SCHEDULE_TIMEOUT;
<br>&#x9;return 0;
<br>}
<br>
<br>static void __net_exit audit_net_exit(struct netnet)
<br>{
<br>&#x9;struct audit_netaunet = net_generic(net, audit_net_id);
<br>&#x9;struct socksock = aunet-&#x3E;nlsk;
<br>&#x9;if (sock == audit_sock) {
<br>&#x9;&#x9;audit_pid = 0;
<br>&#x9;&#x9;audit_sock = NULL;
<br>&#x9;}
<br>
<br>&#x9;RCU_INIT_POINTER(aunet-&#x3E;nlsk, NULL);
<br>&#x9;synchronize_net();
<br>&#x9;netlink_kernel_release(sock);
<br>}
<br>
<br>static struct pernet_operations audit_net_ops __net_initdata = {
<br>&#x9;.init = audit_net_init,
<br>&#x9;.exit = audit_net_exit,
<br>&#x9;.id = &#x26;audit_net_id,
<br>&#x9;.size = sizeof(struct audit_net),
<br>};
<br>
<br>*/ Initialize audit support at boot time. /*
<br>static int __init audit_init(void)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;if (audit_initialized == AUDIT_DISABLED)
<br>&#x9;&#x9;return 0;
<br>
<br>&#x9;pr_info(&#x22;initializing netlink subsys (%s)\n&#x22;,
<br>&#x9;&#x9;audit_default ? &#x22;enabled&#x22; : &#x22;disabled&#x22;);
<br>&#x9;register_pernet_subsys(&#x26;audit_net_ops);
<br>
<br>&#x9;skb_queue_head_init(&#x26;audit_skb_queue);
<br>&#x9;skb_queue_head_init(&#x26;audit_skb_hold_queue);
<br>&#x9;audit_initialized = AUDIT_INITIALIZED;
<br>&#x9;audit_enabled = audit_default;
<br>&#x9;audit_ever_enabled |= !!audit_default;
<br>
<br>&#x9;audit_log(NULL, GFP_KERNEL, AUDIT_KERNEL, &#x22;initialized&#x22;);
<br>
<br>&#x9;for (i = 0; i &#x3C; AUDIT_INODE_BUCKETS; i++)
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;audit_inode_hash[i]);
<br>
<br>&#x9;return 0;
<br>}
<br>__initcall(audit_init);
<br>
<br>*/ Process kernel command-line parameter at boot time.  audit=0 or audit=1. /*
<br>static int __init audit_enable(charstr)
<br>{
<br>&#x9;audit_default = !!simple_strtol(str, NULL, 0);
<br>&#x9;if (!audit_default)
<br>&#x9;&#x9;audit_initialized = AUDIT_DISABLED;
<br>
<br>&#x9;pr_info(&#x22;%s\n&#x22;, audit_default ?
<br>&#x9;&#x9;&#x22;enabled (after initialization)&#x22; : &#x22;disabled (until reboot)&#x22;);
<br>
<br>&#x9;return 1;
<br>}
<br>__setup(&#x22;audit=&#x22;, audit_enable);
<br>
<br>*/ Process kernel command-line parameter at boot time.
<br> audit_backlog_limit=&#x3C;n&#x3E; /*
<br>static int __init audit_backlog_limit_set(charstr)
<br>{
<br>&#x9;u32 audit_backlog_limit_arg;
<br>
<br>&#x9;pr_info(&#x22;audit_backlog_limit: &#x22;);
<br>&#x9;if (kstrtouint(str, 0, &#x26;audit_backlog_limit_arg)) {
<br>&#x9;&#x9;pr_cont(&#x22;using default of %u, unable to parse %s\n&#x22;,
<br>&#x9;&#x9;&#x9;audit_backlog_limit, str);
<br>&#x9;&#x9;return 1;
<br>&#x9;}
<br>
<br>&#x9;audit_backlog_limit = audit_backlog_limit_arg;
<br>&#x9;pr_cont(&#x22;%d\n&#x22;, audit_backlog_limit);
<br>
<br>&#x9;return 1;
<br>}
<br>__setup(&#x22;audit_backlog_limit=&#x22;, audit_backlog_limit_set);
<br>
<br>static void audit_buffer_free(struct audit_bufferab)
<br>{
<br>&#x9;unsigned long flags;
<br>
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;kfree_skb(ab-&#x3E;skb);
<br>&#x9;spin_lock_irqsave(&#x26;audit_freelist_lock, flags);
<br>&#x9;if (audit_freelist_count &#x3E; AUDIT_MAXFREE)
<br>&#x9;&#x9;kfree(ab);
<br>&#x9;else {
<br>&#x9;&#x9;audit_freelist_count++;
<br>&#x9;&#x9;list_add(&#x26;ab-&#x3E;list, &#x26;audit_freelist);
<br>&#x9;}
<br>&#x9;spin_unlock_irqrestore(&#x26;audit_freelist_lock, flags);
<br>}
<br>
<br>static struct audit_buffer audit_buffer_alloc(struct audit_contextctx,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;gfp_t gfp_mask, int type)
<br>{
<br>&#x9;unsigned long flags;
<br>&#x9;struct audit_bufferab = NULL;
<br>&#x9;struct nlmsghdrnlh;
<br>
<br>&#x9;spin_lock_irqsave(&#x26;audit_freelist_lock, flags);
<br>&#x9;if (!list_empty(&#x26;audit_freelist)) {
<br>&#x9;&#x9;ab = list_entry(audit_freelist.next,
<br>&#x9;&#x9;&#x9;&#x9;struct audit_buffer, list);
<br>&#x9;&#x9;list_del(&#x26;ab-&#x3E;list);
<br>&#x9;&#x9;--audit_freelist_count;
<br>&#x9;}
<br>&#x9;spin_unlock_irqrestore(&#x26;audit_freelist_lock, flags);
<br>
<br>&#x9;if (!ab) {
<br>&#x9;&#x9;ab = kmalloc(sizeof(*ab), gfp_mask);
<br>&#x9;&#x9;if (!ab)
<br>&#x9;&#x9;&#x9;goto err;
<br>&#x9;}
<br>
<br>&#x9;ab-&#x3E;ctx = ctx;
<br>&#x9;ab-&#x3E;gfp_mask = gfp_mask;
<br>
<br>&#x9;ab-&#x3E;skb = nlmsg_new(AUDIT_BUFSIZ, gfp_mask);
<br>&#x9;if (!ab-&#x3E;skb)
<br>&#x9;&#x9;goto err;
<br>
<br>&#x9;nlh = nlmsg_put(ab-&#x3E;skb, 0, 0, type, 0, 0);
<br>&#x9;if (!nlh)
<br>&#x9;&#x9;goto out_kfree_skb;
<br>
<br>&#x9;return ab;
<br>
<br>out_kfree_skb:
<br>&#x9;kfree_skb(ab-&#x3E;skb);
<br>&#x9;ab-&#x3E;skb = NULL;
<br>err:
<br>&#x9;audit_buffer_free(ab);
<br>&#x9;return NULL;
<br>}
<br>
<br>*/
<br> audit_serial - compute a serial number for the audit record
<br>
<br> Compute a serial number for the audit record.  Audit records are
<br> written to user-space as soon as they are generated, so a complete
<br> audit record may be written in several pieces.  The timestamp of the
<br> record and this serial number are used by the user-space tools to
<br> determine which pieces belong to the same audit record.  The
<br> (timestamp,serial) tuple is unique for each syscall and is live from
<br> syscall entry to syscall exit.
<br>
<br> NOTE: Another possibility is to store the formatted records off the
<br> audit context (for those records that have a context), and emit them
<br> all at syscall exit.  However, this could delay the reporting of
<br> significant errors until syscall exit (or never, if the system
<br> halts).
<br> /*
<br>unsigned int audit_serial(void)
<br>{
<br>&#x9;static atomic_t serial = ATOMIC_INIT(0);
<br>
<br>&#x9;return atomic_add_return(1, &#x26;serial);
<br>}
<br>
<br>static inline void audit_get_stamp(struct audit_contextctx,
<br>&#x9;&#x9;&#x9;&#x9;   struct timespect, unsigned intserial)
<br>{
<br>&#x9;if (!ctx || !auditsc_get_stamp(ctx, t, serial)) {
<br>&#x9;&#x9;*t = CURRENT_TIME;
<br>&#x9;&#x9;*serial = audit_serial();
<br>&#x9;}
<br>}
<br>
<br>*/
<br> Wait for auditd to drain the queue a little
<br> /*
<br>static long wait_for_auditd(long sleep_time)
<br>{
<br>&#x9;DECLARE_WAITQUEUE(wait, current);
<br>&#x9;set_current_state(TASK_UNINTERRUPTIBLE);
<br>&#x9;add_wait_queue_exclusive(&#x26;audit_backlog_wait, &#x26;wait);
<br>
<br>&#x9;if (audit_backlog_limit &#x26;&#x26;
<br>&#x9;    skb_queue_len(&#x26;audit_skb_queue) &#x3E; audit_backlog_limit)
<br>&#x9;&#x9;sleep_time = schedule_timeout(sleep_time);
<br>
<br>&#x9;__set_current_state(TASK_RUNNING);
<br>&#x9;remove_wait_queue(&#x26;audit_backlog_wait, &#x26;wait);
<br>
<br>&#x9;return sleep_time;
<br>}
<br>
<br>*/
<br> audit_log_start - obtain an audit buffer
<br> @ctx: audit_context (may be NULL)
<br> @gfp_mask: type of allocation
<br> @type: audit message type
<br>
<br> Returns audit_buffer pointer on success or NULL on error.
<br>
<br> Obtain an audit buffer.  This routine does locking to obtain the
<br> audit buffer, but then no locking is required for calls to
<br> audit_log_*format.  If the task (ctx) is a task that is currently in a
<br> syscall, then the syscall is marked as auditable and an audit record
<br> will be written at syscall exit.  If there is no associated task, then
<br> task context (ctx) should be NULL.
<br> /*
<br>struct audit_bufferaudit_log_start(struct audit_contextctx, gfp_t gfp_mask,
<br>&#x9;&#x9;&#x9;&#x9;     int type)
<br>{
<br>&#x9;struct audit_buffer&#x9;*ab&#x9;= NULL;
<br>&#x9;struct timespec&#x9;&#x9;t;
<br>&#x9;unsigned int&#x9;&#x9;uninitialized_var(serial);
<br>&#x9;int reserve = 5;/ Allow atomic callers to go up to five
<br>&#x9;&#x9;&#x9;    entries over the normal backlog limit /*
<br>&#x9;unsigned long timeout_start = jiffies;
<br>
<br>&#x9;if (audit_initialized != AUDIT_INITIALIZED)
<br>&#x9;&#x9;return NULL;
<br>
<br>&#x9;if (unlikely(audit_filter_type(type)))
<br>&#x9;&#x9;return NULL;
<br>
<br>&#x9;if (gfp_mask &#x26; __GFP_DIRECT_RECLAIM) {
<br>&#x9;&#x9;if (audit_pid &#x26;&#x26; audit_pid == current-&#x3E;tgid)
<br>&#x9;&#x9;&#x9;gfp_mask &#x26;= ~__GFP_DIRECT_RECLAIM;
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;reserve = 0;
<br>&#x9;}
<br>
<br>&#x9;while (audit_backlog_limit
<br>&#x9;       &#x26;&#x26; skb_queue_len(&#x26;audit_skb_queue) &#x3E; audit_backlog_limit + reserve) {
<br>&#x9;&#x9;if (gfp_mask &#x26; __GFP_DIRECT_RECLAIM &#x26;&#x26; audit_backlog_wait_time) {
<br>&#x9;&#x9;&#x9;long sleep_time;
<br>
<br>&#x9;&#x9;&#x9;sleep_time = timeout_start + audit_backlog_wait_time - jiffies;
<br>&#x9;&#x9;&#x9;if (sleep_time &#x3E; 0) {
<br>&#x9;&#x9;&#x9;&#x9;sleep_time = wait_for_auditd(sleep_time);
<br>&#x9;&#x9;&#x9;&#x9;if (sleep_time &#x3E; 0)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (audit_rate_check() &#x26;&#x26; printk_ratelimit())
<br>&#x9;&#x9;&#x9;pr_warn(&#x22;audit_backlog=%d &#x3E; audit_backlog_limit=%d\n&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;skb_queue_len(&#x26;audit_skb_queue),
<br>&#x9;&#x9;&#x9;&#x9;audit_backlog_limit);
<br>&#x9;&#x9;audit_log_lost(&#x22;backlog limit exceeded&#x22;);
<br>&#x9;&#x9;audit_backlog_wait_time = 0;
<br>&#x9;&#x9;wake_up(&#x26;audit_backlog_wait);
<br>&#x9;&#x9;return NULL;
<br>&#x9;}
<br>
<br>&#x9;if (!reserve &#x26;&#x26; !audit_backlog_wait_time)
<br>&#x9;&#x9;audit_backlog_wait_time = audit_backlog_wait_time_master;
<br>
<br>&#x9;ab = audit_buffer_alloc(ctx, gfp_mask, type);
<br>&#x9;if (!ab) {
<br>&#x9;&#x9;audit_log_lost(&#x22;out of memory in audit_log_start&#x22;);
<br>&#x9;&#x9;return NULL;
<br>&#x9;}
<br>
<br>&#x9;audit_get_stamp(ab-&#x3E;ctx, &#x26;t, &#x26;serial);
<br>
<br>&#x9;audit_log_format(ab, &#x22;audit(%lu.%03lu:%u): &#x22;,
<br>&#x9;&#x9;&#x9; t.tv_sec, t.tv_nsec/1000000, serial);
<br>&#x9;return ab;
<br>}
<br>
<br>*/
<br> audit_expand - expand skb in the audit buffer
<br> @ab: audit_buffer
<br> @extra: space to add at tail of the skb
<br>
<br> Returns 0 (no space) on failed expansion, or available space if
<br> successful.
<br> /*
<br>static inline int audit_expand(struct audit_bufferab, int extra)
<br>{
<br>&#x9;struct sk_buffskb = ab-&#x3E;skb;
<br>&#x9;int oldtail = skb_tailroom(skb);
<br>&#x9;int ret = pskb_expand_head(skb, 0, extra, ab-&#x3E;gfp_mask);
<br>&#x9;int newtail = skb_tailroom(skb);
<br>
<br>&#x9;if (ret &#x3C; 0) {
<br>&#x9;&#x9;audit_log_lost(&#x22;out of memory in audit_expand&#x22;);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>
<br>&#x9;skb-&#x3E;truesize += newtail - oldtail;
<br>&#x9;return newtail;
<br>}
<br>
<br>*/
<br> Format an audit message into the audit buffer.  If there isn&#x27;t enough
<br> room in the audit buffer, more room will be allocated and vsnprint
<br> will be called a second time.  Currently, we assume that a printk
<br> can&#x27;t format message larger than 1024 bytes, so we don&#x27;t either.
<br> /*
<br>static void audit_log_vformat(struct audit_bufferab, const charfmt,
<br>&#x9;&#x9;&#x9;      va_list args)
<br>{
<br>&#x9;int len, avail;
<br>&#x9;struct sk_buffskb;
<br>&#x9;va_list args2;
<br>
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;BUG_ON(!ab-&#x3E;skb);
<br>&#x9;skb = ab-&#x3E;skb;
<br>&#x9;avail = skb_tailroom(skb);
<br>&#x9;if (avail == 0) {
<br>&#x9;&#x9;avail = audit_expand(ab, AUDIT_BUFSIZ);
<br>&#x9;&#x9;if (!avail)
<br>&#x9;&#x9;&#x9;goto out;
<br>&#x9;}
<br>&#x9;va_copy(args2, args);
<br>&#x9;len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args);
<br>&#x9;if (len &#x3E;= avail) {
<br>&#x9;&#x9;*/ The printk buffer is 1024 bytes long, so if we get
<br>&#x9;&#x9; here and AUDIT_BUFSIZ is at least 1024, then we can
<br>&#x9;&#x9; log everything that printk could have logged. /*
<br>&#x9;&#x9;avail = audit_expand(ab,
<br>&#x9;&#x9;&#x9;max_t(unsigned, AUDIT_BUFSIZ, 1+len-avail));
<br>&#x9;&#x9;if (!avail)
<br>&#x9;&#x9;&#x9;goto out_va_end;
<br>&#x9;&#x9;len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args2);
<br>&#x9;}
<br>&#x9;if (len &#x3E; 0)
<br>&#x9;&#x9;skb_put(skb, len);
<br>out_va_end:
<br>&#x9;va_end(args2);
<br>out:
<br>&#x9;return;
<br>}
<br>
<br>*/
<br> audit_log_format - format a message into the audit buffer.
<br> @ab: audit_buffer
<br> @fmt: format string
<br> @...: optional parameters matching @fmt string
<br>
<br> All the work is done in audit_log_vformat.
<br> /*
<br>void audit_log_format(struct audit_bufferab, const charfmt, ...)
<br>{
<br>&#x9;va_list args;
<br>
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>&#x9;va_start(args, fmt);
<br>&#x9;audit_log_vformat(ab, fmt, args);
<br>&#x9;va_end(args);
<br>}
<br>
<br>*/
<br> audit_log_hex - convert a buffer to hex and append it to the audit skb
<br> @ab: the audit_buffer
<br> @buf: buffer to convert to hex
<br> @len: length of @buf to be converted
<br>
<br> No return value; failure to expand is silently ignored.
<br>
<br> This function will take the passed buf and convert it into a string of
<br> ascii hex digits. The new string is placed onto the skb.
<br> /*
<br>void audit_log_n_hex(struct audit_bufferab, const unsigned charbuf,
<br>&#x9;&#x9;size_t len)
<br>{
<br>&#x9;int i, avail, new_len;
<br>&#x9;unsigned charptr;
<br>&#x9;struct sk_buffskb;
<br>
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;BUG_ON(!ab-&#x3E;skb);
<br>&#x9;skb = ab-&#x3E;skb;
<br>&#x9;avail = skb_tailroom(skb);
<br>&#x9;new_len = len&#x3C;&#x3C;1;
<br>&#x9;if (new_len &#x3E;= avail) {
<br>&#x9;&#x9;*/ Round the buffer request up to the next multiple /*
<br>&#x9;&#x9;new_len = AUDIT_BUFSIZ*(((new_len-avail)/AUDIT_BUFSIZ) + 1);
<br>&#x9;&#x9;avail = audit_expand(ab, new_len);
<br>&#x9;&#x9;if (!avail)
<br>&#x9;&#x9;&#x9;return;
<br>&#x9;}
<br>
<br>&#x9;ptr = skb_tail_pointer(skb);
<br>&#x9;for (i = 0; i &#x3C; len; i++)
<br>&#x9;&#x9;ptr = hex_byte_pack_upper(ptr, buf[i]);
<br>&#x9;*ptr = 0;
<br>&#x9;skb_put(skb, len &#x3C;&#x3C; 1);/ new string is twice the old string /*
<br>}
<br>
<br>*/
<br> Format a string of no more than slen characters into the audit buffer,
<br> enclosed in quote marks.
<br> /*
<br>void audit_log_n_string(struct audit_bufferab, const charstring,
<br>&#x9;&#x9;&#x9;size_t slen)
<br>{
<br>&#x9;int avail, new_len;
<br>&#x9;unsigned charptr;
<br>&#x9;struct sk_buffskb;
<br>
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;BUG_ON(!ab-&#x3E;skb);
<br>&#x9;skb = ab-&#x3E;skb;
<br>&#x9;avail = skb_tailroom(skb);
<br>&#x9;new_len = slen + 3;&#x9;*/ enclosing quotes + null terminator /*
<br>&#x9;if (new_len &#x3E; avail) {
<br>&#x9;&#x9;avail = audit_expand(ab, new_len);
<br>&#x9;&#x9;if (!avail)
<br>&#x9;&#x9;&#x9;return;
<br>&#x9;}
<br>&#x9;ptr = skb_tail_pointer(skb);
<br>&#x9;*ptr++ = &#x27;&#x22;&#x27;;
<br>&#x9;memcpy(ptr, string, slen);
<br>&#x9;ptr += slen;
<br>&#x9;*ptr++ = &#x27;&#x22;&#x27;;
<br>&#x9;*ptr = 0;
<br>&#x9;skb_put(skb, slen + 2);&#x9;*/ don&#x27;t include null terminator /*
<br>}
<br>
<br>*/
<br> audit_string_contains_control - does a string need to be logged in hex
<br> @string: string to be checked
<br> @len: max length of the string to check
<br> /*
<br>bool audit_string_contains_control(const charstring, size_t len)
<br>{
<br>&#x9;const unsigned charp;
<br>&#x9;for (p = string; p &#x3C; (const unsigned char)string + len; p++) {
<br>&#x9;&#x9;if (*p == &#x27;&#x22;&#x27; ||p &#x3C; 0x21 ||p &#x3E; 0x7e)
<br>&#x9;&#x9;&#x9;return true;
<br>&#x9;}
<br>&#x9;return false;
<br>}
<br>
<br>*/
<br> audit_log_n_untrustedstring - log a string that may contain random characters
<br> @ab: audit_buffer
<br> @len: length of string (not including trailing null)
<br> @string: string to be logged
<br>
<br> This code will escape a string that is passed to it if the string
<br> contains a control character, unprintable character, double quote mark,
<br> or a space. Unescaped strings will start and end with a double quote mark.
<br> Strings that are escaped are printed in hex (2 digits per char).
<br>
<br> The caller specifies the number of characters in the string to log, which may
<br> or may not be the entire string.
<br> /*
<br>void audit_log_n_untrustedstring(struct audit_bufferab, const charstring,
<br>&#x9;&#x9;&#x9;&#x9; size_t len)
<br>{
<br>&#x9;if (audit_string_contains_control(string, len))
<br>&#x9;&#x9;audit_log_n_hex(ab, string, len);
<br>&#x9;else
<br>&#x9;&#x9;audit_log_n_string(ab, string, len);
<br>}
<br>
<br>*/
<br> audit_log_untrustedstring - log a string that may contain random characters
<br> @ab: audit_buffer
<br> @string: string to be logged
<br>
<br> Same as audit_log_n_untrustedstring(), except that strlen is used to
<br> determine string length.
<br> /*
<br>void audit_log_untrustedstring(struct audit_bufferab, const charstring)
<br>{
<br>&#x9;audit_log_n_untrustedstring(ab, string, strlen(string));
<br>}
<br>
<br>*/ This is a helper-function to print the escaped d_path /*
<br>void audit_log_d_path(struct audit_bufferab, const charprefix,
<br>&#x9;&#x9;      const struct pathpath)
<br>{
<br>&#x9;charp,pathname;
<br>
<br>&#x9;if (prefix)
<br>&#x9;&#x9;audit_log_format(ab, &#x22;%s&#x22;, prefix);
<br>
<br>&#x9;*/ We will allow 11 spaces for &#x27; (deleted)&#x27; to be appended /*
<br>&#x9;pathname = kmalloc(PATH_MAX+11, ab-&#x3E;gfp_mask);
<br>&#x9;if (!pathname) {
<br>&#x9;&#x9;audit_log_string(ab, &#x22;&#x3C;no_memory&#x3E;&#x22;);
<br>&#x9;&#x9;return;
<br>&#x9;}
<br>&#x9;p = d_path(path, pathname, PATH_MAX+11);
<br>&#x9;if (IS_ERR(p)) {/ Should never happen since we send PATH_MAX /*
<br>&#x9;&#x9;*/ FIXME: can we save some information here? /*
<br>&#x9;&#x9;audit_log_string(ab, &#x22;&#x3C;too_long&#x3E;&#x22;);
<br>&#x9;} else
<br>&#x9;&#x9;audit_log_untrustedstring(ab, p);
<br>&#x9;kfree(pathname);
<br>}
<br>
<br>void audit_log_session_info(struct audit_bufferab)
<br>{
<br>&#x9;unsigned int sessionid = audit_get_sessionid(current);
<br>&#x9;uid_t auid = from_kuid(&#x26;init_user_ns, audit_get_loginuid(current));
<br>
<br>&#x9;audit_log_format(ab, &#x22; auid=%u ses=%u&#x22;, auid, sessionid);
<br>}
<br>
<br>void audit_log_key(struct audit_bufferab, charkey)
<br>{
<br>&#x9;audit_log_format(ab, &#x22; key=&#x22;);
<br>&#x9;if (key)
<br>&#x9;&#x9;audit_log_untrustedstring(ab, key);
<br>&#x9;else
<br>&#x9;&#x9;audit_log_format(ab, &#x22;(null)&#x22;);
<br>}
<br>
<br>void audit_log_cap(struct audit_bufferab, charprefix, kernel_cap_tcap)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;audit_log_format(ab, &#x22; %s=&#x22;, prefix);
<br>&#x9;CAP_FOR_EACH_U32(i) {
<br>&#x9;&#x9;audit_log_format(ab, &#x22;%08x&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; cap-&#x3E;cap[CAP_LAST_U32 - i]);
<br>&#x9;}
<br>}
<br>
<br>static void audit_log_fcaps(struct audit_bufferab, struct audit_namesname)
<br>{
<br>&#x9;kernel_cap_tperm = &#x26;name-&#x3E;fcap.permitted;
<br>&#x9;kernel_cap_tinh = &#x26;name-&#x3E;fcap.inheritable;
<br>&#x9;int log = 0;
<br>
<br>&#x9;if (!cap_isclear(*perm)) {
<br>&#x9;&#x9;audit_log_cap(ab, &#x22;cap_fp&#x22;, perm);
<br>&#x9;&#x9;log = 1;
<br>&#x9;}
<br>&#x9;if (!cap_isclear(*inh)) {
<br>&#x9;&#x9;audit_log_cap(ab, &#x22;cap_fi&#x22;, inh);
<br>&#x9;&#x9;log = 1;
<br>&#x9;}
<br>
<br>&#x9;if (log)
<br>&#x9;&#x9;audit_log_format(ab, &#x22; cap_fe=%d cap_fver=%x&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; name-&#x3E;fcap.fE, name-&#x3E;fcap_ver);
<br>}
<br>
<br>static inline int audit_copy_fcaps(struct audit_namesname,
<br>&#x9;&#x9;&#x9;&#x9;   const struct dentrydentry)
<br>{
<br>&#x9;struct cpu_vfs_cap_data caps;
<br>&#x9;int rc;
<br>
<br>&#x9;if (!dentry)
<br>&#x9;&#x9;return 0;
<br>
<br>&#x9;rc = get_vfs_caps_from_disk(dentry, &#x26;caps);
<br>&#x9;if (rc)
<br>&#x9;&#x9;return rc;
<br>
<br>&#x9;name-&#x3E;fcap.permitted = caps.permitted;
<br>&#x9;name-&#x3E;fcap.inheritable = caps.inheritable;
<br>&#x9;name-&#x3E;fcap.fE = !!(caps.magic_etc &#x26; VFS_CAP_FLAGS_EFFECTIVE);
<br>&#x9;name-&#x3E;fcap_ver = (caps.magic_etc &#x26; VFS_CAP_REVISION_MASK) &#x3E;&#x3E;
<br>&#x9;&#x9;&#x9;&#x9;VFS_CAP_REVISION_SHIFT;
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>*/ Copy inode data into an audit_names. /*
<br>void audit_copy_inode(struct audit_namesname, const struct dentrydentry,
<br>&#x9;&#x9;      struct inodeinode)
<br>{
<br>&#x9;name-&#x3E;ino   = inode-&#x3E;i_ino;
<br>&#x9;name-&#x3E;dev   = inode-&#x3E;i_sb-&#x3E;s_dev;
<br>&#x9;name-&#x3E;mode  = inode-&#x3E;i_mode;
<br>&#x9;name-&#x3E;uid   = inode-&#x3E;i_uid;
<br>&#x9;name-&#x3E;gid   = inode-&#x3E;i_gid;
<br>&#x9;name-&#x3E;rdev  = inode-&#x3E;i_rdev;
<br>&#x9;security_inode_getsecid(inode, &#x26;name-&#x3E;osid);
<br>&#x9;audit_copy_fcaps(name, dentry);
<br>}
<br>
<br>*/
<br> audit_log_name - produce AUDIT_PATH record from struct audit_names
<br> @context: audit_context for the task
<br> @n: audit_names structure with reportable details
<br> @path: optional path to report instead of audit_names-&#x3E;name
<br> @record_num: record number to report when handling a list of names
<br> @call_panic: optional pointer to int that will be updated if secid fails
<br> /*
<br>void audit_log_name(struct audit_contextcontext, struct audit_namesn,
<br>&#x9;&#x9;    struct pathpath, int record_num, intcall_panic)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_PATH);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;audit_log_format(ab, &#x22;item=%d&#x22;, record_num);
<br>
<br>&#x9;if (path)
<br>&#x9;&#x9;audit_log_d_path(ab, &#x22; name=&#x22;, path);
<br>&#x9;else if (n-&#x3E;name) {
<br>&#x9;&#x9;switch (n-&#x3E;name_len) {
<br>&#x9;&#x9;case AUDIT_NAME_FULL:
<br>&#x9;&#x9;&#x9;*/ log the full path /*
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; name=&#x22;);
<br>&#x9;&#x9;&#x9;audit_log_untrustedstring(ab, n-&#x3E;name-&#x3E;name);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case 0:
<br>&#x9;&#x9;&#x9;*/ name was specified as a relative path and the
<br>&#x9;&#x9;&#x9; directory component is the cwd /*
<br>&#x9;&#x9;&#x9;audit_log_d_path(ab, &#x22; name=&#x22;, &#x26;context-&#x3E;pwd);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;default:
<br>&#x9;&#x9;&#x9;*/ log the name&#x27;s directory component /*
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; name=&#x22;);
<br>&#x9;&#x9;&#x9;audit_log_n_untrustedstring(ab, n-&#x3E;name-&#x3E;name,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;    n-&#x3E;name_len);
<br>&#x9;&#x9;}
<br>&#x9;} else
<br>&#x9;&#x9;audit_log_format(ab, &#x22; name=(null)&#x22;);
<br>
<br>&#x9;if (n-&#x3E;ino != AUDIT_INO_UNSET)
<br>&#x9;&#x9;audit_log_format(ab, &#x22; inode=%lu&#x22;
<br>&#x9;&#x9;&#x9;&#x9; &#x22; dev=%02x:%02x mode=%#ho&#x22;
<br>&#x9;&#x9;&#x9;&#x9; &#x22; ouid=%u ogid=%u rdev=%02x:%02x&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; n-&#x3E;ino,
<br>&#x9;&#x9;&#x9;&#x9; MAJOR(n-&#x3E;dev),
<br>&#x9;&#x9;&#x9;&#x9; MINOR(n-&#x3E;dev),
<br>&#x9;&#x9;&#x9;&#x9; n-&#x3E;mode,
<br>&#x9;&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, n-&#x3E;uid),
<br>&#x9;&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, n-&#x3E;gid),
<br>&#x9;&#x9;&#x9;&#x9; MAJOR(n-&#x3E;rdev),
<br>&#x9;&#x9;&#x9;&#x9; MINOR(n-&#x3E;rdev));
<br>&#x9;if (n-&#x3E;osid != 0) {
<br>&#x9;&#x9;charctx = NULL;
<br>&#x9;&#x9;u32 len;
<br>&#x9;&#x9;if (security_secid_to_secctx(
<br>&#x9;&#x9;&#x9;n-&#x3E;osid, &#x26;ctx, &#x26;len)) {
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; osid=%u&#x22;, n-&#x3E;osid);
<br>&#x9;&#x9;&#x9;if (call_panic)
<br>&#x9;&#x9;&#x9;&#x9;*call_panic = 2;
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, ctx);
<br>&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;*/ log the audit_names record type /*
<br>&#x9;audit_log_format(ab, &#x22; nametype=&#x22;);
<br>&#x9;switch(n-&#x3E;type) {
<br>&#x9;case AUDIT_TYPE_NORMAL:
<br>&#x9;&#x9;audit_log_format(ab, &#x22;NORMAL&#x22;);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_TYPE_PARENT:
<br>&#x9;&#x9;audit_log_format(ab, &#x22;PARENT&#x22;);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_TYPE_CHILD_DELETE:
<br>&#x9;&#x9;audit_log_format(ab, &#x22;DELETE&#x22;);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_TYPE_CHILD_CREATE:
<br>&#x9;&#x9;audit_log_format(ab, &#x22;CREATE&#x22;);
<br>&#x9;&#x9;break;
<br>&#x9;default:
<br>&#x9;&#x9;audit_log_format(ab, &#x22;UNKNOWN&#x22;);
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>
<br>&#x9;audit_log_fcaps(ab, n);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>int audit_log_task_context(struct audit_bufferab)
<br>{
<br>&#x9;charctx = NULL;
<br>&#x9;unsigned len;
<br>&#x9;int error;
<br>&#x9;u32 sid;
<br>
<br>&#x9;security_task_getsecid(current, &#x26;sid);
<br>&#x9;if (!sid)
<br>&#x9;&#x9;return 0;
<br>
<br>&#x9;error = security_secid_to_secctx(sid, &#x26;ctx, &#x26;len);
<br>&#x9;if (error) {
<br>&#x9;&#x9;if (error != -EINVAL)
<br>&#x9;&#x9;&#x9;goto error_path;
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>
<br>&#x9;audit_log_format(ab, &#x22; subj=%s&#x22;, ctx);
<br>&#x9;security_release_secctx(ctx, len);
<br>&#x9;return 0;
<br>
<br>error_path:
<br>&#x9;audit_panic(&#x22;error in audit_log_task_context&#x22;);
<br>&#x9;return error;
<br>}
<br>EXPORT_SYMBOL(audit_log_task_context);
<br>
<br>void audit_log_d_path_exe(struct audit_bufferab,
<br>&#x9;&#x9;&#x9;  struct mm_structmm)
<br>{
<br>&#x9;struct fileexe_file;
<br>
<br>&#x9;if (!mm)
<br>&#x9;&#x9;goto out_null;
<br>
<br>&#x9;exe_file = get_mm_exe_file(mm);
<br>&#x9;if (!exe_file)
<br>&#x9;&#x9;goto out_null;
<br>
<br>&#x9;audit_log_d_path(ab, &#x22; exe=&#x22;, &#x26;exe_file-&#x3E;f_path);
<br>&#x9;fput(exe_file);
<br>&#x9;return;
<br>out_null:
<br>&#x9;audit_log_format(ab, &#x22; exe=(null)&#x22;);
<br>}
<br>
<br>void audit_log_task_info(struct audit_bufferab, struct task_structtsk)
<br>{
<br>&#x9;const struct credcred;
<br>&#x9;char comm[sizeof(tsk-&#x3E;comm)];
<br>&#x9;chartty;
<br>
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;*/ tsk == current /*
<br>&#x9;cred = current_cred();
<br>
<br>&#x9;spin_lock_irq(&#x26;tsk-&#x3E;sighand-&#x3E;siglock);
<br>&#x9;if (tsk-&#x3E;signal &#x26;&#x26; tsk-&#x3E;signal-&#x3E;tty &#x26;&#x26; tsk-&#x3E;signal-&#x3E;tty-&#x3E;name)
<br>&#x9;&#x9;tty = tsk-&#x3E;signal-&#x3E;tty-&#x3E;name;
<br>&#x9;else
<br>&#x9;&#x9;tty = &#x22;(none)&#x22;;
<br>&#x9;spin_unlock_irq(&#x26;tsk-&#x3E;sighand-&#x3E;siglock);
<br>
<br>&#x9;audit_log_format(ab,
<br>&#x9;&#x9;&#x9; &#x22; ppid=%d pid=%d auid=%u uid=%u gid=%u&#x22;
<br>&#x9;&#x9;&#x9; &#x22; euid=%u suid=%u fsuid=%u&#x22;
<br>&#x9;&#x9;&#x9; &#x22; egid=%u sgid=%u fsgid=%u tty=%s ses=%u&#x22;,
<br>&#x9;&#x9;&#x9; task_ppid_nr(tsk),
<br>&#x9;&#x9;&#x9; task_pid_nr(tsk),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, audit_get_loginuid(tsk)),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;uid),
<br>&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;gid),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;euid),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;suid),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;fsuid),
<br>&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;egid),
<br>&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;sgid),
<br>&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;fsgid),
<br>&#x9;&#x9;&#x9; tty, audit_get_sessionid(tsk));
<br>
<br>&#x9;audit_log_format(ab, &#x22; comm=&#x22;);
<br>&#x9;audit_log_untrustedstring(ab, get_task_comm(comm, tsk));
<br>
<br>&#x9;audit_log_d_path_exe(ab, tsk-&#x3E;mm);
<br>&#x9;audit_log_task_context(ab);
<br>}
<br>EXPORT_SYMBOL(audit_log_task_info);
<br>
<br>*/
<br> audit_log_link_denied - report a link restriction denial
<br> @operation: specific link operation
<br> @link: the path that triggered the restriction
<br> /*
<br>void audit_log_link_denied(const charoperation, struct pathlink)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;struct audit_namesname;
<br>
<br>&#x9;name = kzalloc(sizeof(*name), GFP_NOFS);
<br>&#x9;if (!name)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;*/ Generate AUDIT_ANOM_LINK with subject, operation, outcome. /*
<br>&#x9;ab = audit_log_start(current-&#x3E;audit_context, GFP_KERNEL,
<br>&#x9;&#x9;&#x9;     AUDIT_ANOM_LINK);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;goto out;
<br>&#x9;audit_log_format(ab, &#x22;op=%s&#x22;, operation);
<br>&#x9;audit_log_task_info(ab, current);
<br>&#x9;audit_log_format(ab, &#x22; res=0&#x22;);
<br>&#x9;audit_log_end(ab);
<br>
<br>&#x9;*/ Generate AUDIT_PATH record with object. /*
<br>&#x9;name-&#x3E;type = AUDIT_TYPE_NORMAL;
<br>&#x9;audit_copy_inode(name, link-&#x3E;dentry, d_backing_inode(link-&#x3E;dentry));
<br>&#x9;audit_log_name(current-&#x3E;audit_context, name, link, 0, NULL);
<br>out:
<br>&#x9;kfree(name);
<br>}
<br>
<br>*/
<br> audit_log_end - end one audit record
<br> @ab: the audit_buffer
<br>
<br> netlink_unicast() cannot be called inside an irq context because it blocks
<br> (last arg, flags, is not set to MSG_DONTWAIT), so the audit buffer is placed
<br> on a queue and a tasklet is scheduled to remove them from the queue outside
<br> the irq context.  May be called in any context.
<br> /*
<br>void audit_log_end(struct audit_bufferab)
<br>{
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>&#x9;if (!audit_rate_check()) {
<br>&#x9;&#x9;audit_log_lost(&#x22;rate limit exceeded&#x22;);
<br>&#x9;} else {
<br>&#x9;&#x9;struct nlmsghdrnlh = nlmsg_hdr(ab-&#x3E;skb);
<br>
<br>&#x9;&#x9;nlh-&#x3E;nlmsg_len = ab-&#x3E;skb-&#x3E;len;
<br>&#x9;&#x9;kauditd_send_multicast_skb(ab-&#x3E;skb, ab-&#x3E;gfp_mask);
<br>
<br>&#x9;&#x9;*/
<br>&#x9;&#x9; The original kaudit unicast socket sends up messages with
<br>&#x9;&#x9; nlmsg_len set to the payload length rather than the entire
<br>&#x9;&#x9; message length.  This breaks the standard set by netlink.
<br>&#x9;&#x9; The existing auditd daemon assumes this breakage.  Fixing
<br>&#x9;&#x9; this would require co-ordinating a change in the established
<br>&#x9;&#x9; protocol between the kaudit kernel subsystem and the auditd
<br>&#x9;&#x9; userspace code.
<br>&#x9;&#x9; /*
<br>&#x9;&#x9;nlh-&#x3E;nlmsg_len -= NLMSG_HDRLEN;
<br>
<br>&#x9;&#x9;if (audit_pid) {
<br>&#x9;&#x9;&#x9;skb_queue_tail(&#x26;audit_skb_queue, ab-&#x3E;skb);
<br>&#x9;&#x9;&#x9;wake_up_interruptible(&#x26;kauditd_wait);
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;audit_printk_skb(ab-&#x3E;skb);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;ab-&#x3E;skb = NULL;
<br>&#x9;}
<br>&#x9;audit_buffer_free(ab);
<br>}
<br>
<br>*/
<br> audit_log - Log an audit record
<br> @ctx: audit context
<br> @gfp_mask: type of allocation
<br> @type: audit message type
<br> @fmt: format string to use
<br> @...: variable parameters matching the format string
<br>
<br> This is a convenience function that calls audit_log_start,
<br> audit_log_vformat, and audit_log_end.  It may be called
<br> in any context.
<br> /*
<br>void audit_log(struct audit_contextctx, gfp_t gfp_mask, int type,
<br>&#x9;       const charfmt, ...)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;va_list args;
<br>
<br>&#x9;ab = audit_log_start(ctx, gfp_mask, type);
<br>&#x9;if (ab) {
<br>&#x9;&#x9;va_start(args, fmt);
<br>&#x9;&#x9;audit_log_vformat(ab, fmt, args);
<br>&#x9;&#x9;va_end(args);
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;}
<br>}
<br>
<br>#ifdef CONFIG_SECURITY
<br>*/
<br> audit_log_secctx - Converts and logs SELinux context
<br> @ab: audit_buffer
<br> @secid: security number
<br>
<br> This is a helper function that calls security_secid_to_secctx to convert
<br> secid to secctx and then adds the (converted) SELinux context to the audit
<br> log by calling audit_log_format, thus also preventing leak of internal secid
<br> to userspace. If secid cannot be converted audit_panic is called.
<br> /*
<br>void audit_log_secctx(struct audit_bufferab, u32 secid)
<br>{
<br>&#x9;u32 len;
<br>&#x9;charsecctx;
<br>
<br>&#x9;if (security_secid_to_secctx(secid, &#x26;secctx, &#x26;len)) {
<br>&#x9;&#x9;audit_panic(&#x22;Cannot convert secid to context&#x22;);
<br>&#x9;} else {
<br>&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, secctx);
<br>&#x9;&#x9;security_release_secctx(secctx, len);
<br>&#x9;}
<br>}
<br>EXPORT_SYMBOL(audit_log_secctx);
<br>#endif
<br>
<br>EXPORT_SYMBOL(audit_log_start);
<br>EXPORT_SYMBOL(audit_log_end);
<br>EXPORT_SYMBOL(audit_log_format);
<br>EXPORT_SYMBOL(audit_log);
<br>*/
<br> auditfilter.c -- filtering of audit events
<br>
<br> Copyright 2003-2004 Red Hat, Inc.
<br> Copyright 2005 Hewlett-Packard Development Company, L.P.
<br> Copyright 2005 IBM Corporation
<br>
<br> This program is free software; you can redistribute it and/or modify
<br> it under the terms of the GNU General Public License as published by
<br> the Free Software Foundation; either version 2 of the License, or
<br> (at your option) any later version.
<br>
<br> This program is distributed in the hope that it will be useful,
<br> but WITHOUT ANY WARRANTY; without even the implied warranty of
<br> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
<br> GNU General Public License for more details.
<br>
<br> You should have received a copy of the GNU General Public License
<br> along with this program; if not, write to the Free Software
<br> Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
<br> /*
<br>
<br>#define pr_fmt(fmt) KBUILD_MODNAME &#x22;: &#x22; fmt
<br>
<br>#include &#x3C;linux/kernel.h&#x3E;
<br>#include &#x3C;linux/audit.h&#x3E;
<br>#include &#x3C;linux/kthread.h&#x3E;
<br>#include &#x3C;linux/mutex.h&#x3E;
<br>#include &#x3C;linux/fs.h&#x3E;
<br>#include &#x3C;linux/namei.h&#x3E;
<br>#include &#x3C;linux/netlink.h&#x3E;
<br>#include &#x3C;linux/sched.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/security.h&#x3E;
<br>#include &#x3C;net/net_namespace.h&#x3E;
<br>#include &#x3C;net/sock.h&#x3E;
<br>#include &#x22;audit.h&#x22;
<br>
<br>*/
<br> Locking model:
<br>
<br> audit_filter_mutex:
<br>&#x9;&#x9;Synchronizes writes and blocking reads of audit&#x27;s filterlist
<br>&#x9;&#x9;data.  Rcu is used to traverse the filterlist and access
<br>&#x9;&#x9;contents of structs audit_entry, audit_watch and opaque
<br>&#x9;&#x9;LSM rules during filtering.  If modified, these structures
<br>&#x9;&#x9;must be copied and replace their counterparts in the filterlist.
<br>&#x9;&#x9;An audit_parent struct is not accessed during filtering, so may
<br>&#x9;&#x9;be written directly provided audit_filter_mutex is held.
<br> /*
<br>
<br>*/ Audit filter lists, defined in &#x3C;linux/audit.h&#x3E; /*
<br>struct list_head audit_filter_list[AUDIT_NR_FILTERS] = {
<br>&#x9;LIST_HEAD_INIT(audit_filter_list[0]),
<br>&#x9;LIST_HEAD_INIT(audit_filter_list[1]),
<br>&#x9;LIST_HEAD_INIT(audit_filter_list[2]),
<br>&#x9;LIST_HEAD_INIT(audit_filter_list[3]),
<br>&#x9;LIST_HEAD_INIT(audit_filter_list[4]),
<br>&#x9;LIST_HEAD_INIT(audit_filter_list[5]),
<br>#if AUDIT_NR_FILTERS != 6
<br>#error Fix audit_filter_list initialiser
<br>#endif
<br>};
<br>static struct list_head audit_rules_list[AUDIT_NR_FILTERS] = {
<br>&#x9;LIST_HEAD_INIT(audit_rules_list[0]),
<br>&#x9;LIST_HEAD_INIT(audit_rules_list[1]),
<br>&#x9;LIST_HEAD_INIT(audit_rules_list[2]),
<br>&#x9;LIST_HEAD_INIT(audit_rules_list[3]),
<br>&#x9;LIST_HEAD_INIT(audit_rules_list[4]),
<br>&#x9;LIST_HEAD_INIT(audit_rules_list[5]),
<br>};
<br>
<br>DEFINE_MUTEX(audit_filter_mutex);
<br>
<br>static void audit_free_lsm_field(struct audit_fieldf)
<br>{
<br>&#x9;switch (f-&#x3E;type) {
<br>&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;case AUDIT_OBJ_USER:
<br>&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;&#x9;kfree(f-&#x3E;lsm_str);
<br>&#x9;&#x9;security_audit_rule_free(f-&#x3E;lsm_rule);
<br>&#x9;}
<br>}
<br>
<br>static inline void audit_free_rule(struct audit_entrye)
<br>{
<br>&#x9;int i;
<br>&#x9;struct audit_kruleerule = &#x26;e-&#x3E;rule;
<br>
<br>&#x9;*/ some rules don&#x27;t have associated watches /*
<br>&#x9;if (erule-&#x3E;watch)
<br>&#x9;&#x9;audit_put_watch(erule-&#x3E;watch);
<br>&#x9;if (erule-&#x3E;fields)
<br>&#x9;&#x9;for (i = 0; i &#x3C; erule-&#x3E;field_count; i++)
<br>&#x9;&#x9;&#x9;audit_free_lsm_field(&#x26;erule-&#x3E;fields[i]);
<br>&#x9;kfree(erule-&#x3E;fields);
<br>&#x9;kfree(erule-&#x3E;filterkey);
<br>&#x9;kfree(e);
<br>}
<br>
<br>void audit_free_rule_rcu(struct rcu_headhead)
<br>{
<br>&#x9;struct audit_entrye = container_of(head, struct audit_entry, rcu);
<br>&#x9;audit_free_rule(e);
<br>}
<br>
<br>*/ Initialize an audit filterlist entry. /*
<br>static inline struct audit_entryaudit_init_entry(u32 field_count)
<br>{
<br>&#x9;struct audit_entryentry;
<br>&#x9;struct audit_fieldfields;
<br>
<br>&#x9;entry = kzalloc(sizeof(*entry), GFP_KERNEL);
<br>&#x9;if (unlikely(!entry))
<br>&#x9;&#x9;return NULL;
<br>
<br>&#x9;fields = kcalloc(field_count, sizeof(*fields), GFP_KERNEL);
<br>&#x9;if (unlikely(!fields)) {
<br>&#x9;&#x9;kfree(entry);
<br>&#x9;&#x9;return NULL;
<br>&#x9;}
<br>&#x9;entry-&#x3E;rule.fields = fields;
<br>
<br>&#x9;return entry;
<br>}
<br>
<br>*/ Unpack a filter field&#x27;s string representation from user-space
<br> buffer. /*
<br>charaudit_unpack_string(void*bufp, size_tremain, size_t len)
<br>{
<br>&#x9;charstr;
<br>
<br>&#x9;if (!*bufp || (len == 0) || (len &#x3E;remain))
<br>&#x9;&#x9;return ERR_PTR(-EINVAL);
<br>
<br>&#x9;*/ Of the currently implemented string fields, PATH_MAX
<br>&#x9; defines the longest valid length.
<br>&#x9; /*
<br>&#x9;if (len &#x3E; PATH_MAX)
<br>&#x9;&#x9;return ERR_PTR(-ENAMETOOLONG);
<br>
<br>&#x9;str = kmalloc(len + 1, GFP_KERNEL);
<br>&#x9;if (unlikely(!str))
<br>&#x9;&#x9;return ERR_PTR(-ENOMEM);
<br>
<br>&#x9;memcpy(str,bufp, len);
<br>&#x9;str[len] = 0;
<br>&#x9;*bufp += len;
<br>&#x9;*remain -= len;
<br>
<br>&#x9;return str;
<br>}
<br>
<br>*/ Translate an inode field to kernel representation. /*
<br>static inline int audit_to_inode(struct audit_krulekrule,
<br>&#x9;&#x9;&#x9;&#x9; struct audit_fieldf)
<br>{
<br>&#x9;if (krule-&#x3E;listnr != AUDIT_FILTER_EXIT ||
<br>&#x9;    krule-&#x3E;inode_f || krule-&#x3E;watch || krule-&#x3E;tree ||
<br>&#x9;    (f-&#x3E;op != Audit_equal &#x26;&#x26; f-&#x3E;op != Audit_not_equal))
<br>&#x9;&#x9;return -EINVAL;
<br>
<br>&#x9;krule-&#x3E;inode_f = f;
<br>&#x9;return 0;
<br>}
<br>
<br>static __u32classes[AUDIT_SYSCALL_CLASSES];
<br>
<br>int __init audit_register_class(int class, unsignedlist)
<br>{
<br>&#x9;__u32p = kcalloc(AUDIT_BITMASK_SIZE, sizeof(__u32), GFP_KERNEL);
<br>&#x9;if (!p)
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;while (*list != ~0U) {
<br>&#x9;&#x9;unsigned n =list++;
<br>&#x9;&#x9;if (n &#x3E;= AUDIT_BITMASK_SIZE 32 - AUDIT_SYSCALL_CLASSES) {
<br>&#x9;&#x9;&#x9;kfree(p);
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;p[AUDIT_WORD(n)] |= AUDIT_BIT(n);
<br>&#x9;}
<br>&#x9;if (class &#x3E;= AUDIT_SYSCALL_CLASSES || classes[class]) {
<br>&#x9;&#x9;kfree(p);
<br>&#x9;&#x9;return -EINVAL;
<br>&#x9;}
<br>&#x9;classes[class] = p;
<br>&#x9;return 0;
<br>}
<br>
<br>int audit_match_class(int class, unsigned syscall)
<br>{
<br>&#x9;if (unlikely(syscall &#x3E;= AUDIT_BITMASK_SIZE 32))
<br>&#x9;&#x9;return 0;
<br>&#x9;if (unlikely(class &#x3E;= AUDIT_SYSCALL_CLASSES || !classes[class]))
<br>&#x9;&#x9;return 0;
<br>&#x9;return classes[class][AUDIT_WORD(syscall)] &#x26; AUDIT_BIT(syscall);
<br>}
<br>
<br>#ifdef CONFIG_AUDITSYSCALL
<br>static inline int audit_match_class_bits(int class, u32mask)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;if (classes[class]) {
<br>&#x9;&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
<br>&#x9;&#x9;&#x9;if (mask[i] &#x26; classes[class][i])
<br>&#x9;&#x9;&#x9;&#x9;return 0;
<br>&#x9;}
<br>&#x9;return 1;
<br>}
<br>
<br>static int audit_match_signal(struct audit_entryentry)
<br>{
<br>&#x9;struct audit_fieldarch = entry-&#x3E;rule.arch_f;
<br>
<br>&#x9;if (!arch) {
<br>&#x9;&#x9;*/ When arch is unspecified, we must check both masks on biarch
<br>&#x9;&#x9; as syscall number alone is ambiguous. /*
<br>&#x9;&#x9;return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask) &#x26;&#x26;
<br>&#x9;&#x9;&#x9;audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask));
<br>&#x9;}
<br>
<br>&#x9;switch(audit_classify_arch(arch-&#x3E;val)) {
<br>&#x9;case 0:/ native /*
<br>&#x9;&#x9;return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask));
<br>&#x9;case 1:/ 32bit on biarch /*
<br>&#x9;&#x9;return (audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask));
<br>&#x9;default:
<br>&#x9;&#x9;return 1;
<br>&#x9;}
<br>}
<br>#endif
<br>
<br>*/ Common user-space to kernel rule translation. /*
<br>static inline struct audit_entryaudit_to_entry_common(struct audit_rule_datarule)
<br>{
<br>&#x9;unsigned listnr;
<br>&#x9;struct audit_entryentry;
<br>&#x9;int i, err;
<br>
<br>&#x9;err = -EINVAL;
<br>&#x9;listnr = rule-&#x3E;flags &#x26; ~AUDIT_FILTER_PREPEND;
<br>&#x9;switch(listnr) {
<br>&#x9;default:
<br>&#x9;&#x9;goto exit_err;
<br>#ifdef CONFIG_AUDITSYSCALL
<br>&#x9;case AUDIT_FILTER_ENTRY:
<br>&#x9;&#x9;if (rule-&#x3E;action == AUDIT_ALWAYS)
<br>&#x9;&#x9;&#x9;goto exit_err;
<br>&#x9;case AUDIT_FILTER_EXIT:
<br>&#x9;case AUDIT_FILTER_TASK:
<br>#endif
<br>&#x9;case AUDIT_FILTER_USER:
<br>&#x9;case AUDIT_FILTER_TYPE:
<br>&#x9;&#x9;;
<br>&#x9;}
<br>&#x9;if (unlikely(rule-&#x3E;action == AUDIT_POSSIBLE)) {
<br>&#x9;&#x9;pr_err(&#x22;AUDIT_POSSIBLE is deprecated\n&#x22;);
<br>&#x9;&#x9;goto exit_err;
<br>&#x9;}
<br>&#x9;if (rule-&#x3E;action != AUDIT_NEVER &#x26;&#x26; rule-&#x3E;action != AUDIT_ALWAYS)
<br>&#x9;&#x9;goto exit_err;
<br>&#x9;if (rule-&#x3E;field_count &#x3E; AUDIT_MAX_FIELDS)
<br>&#x9;&#x9;goto exit_err;
<br>
<br>&#x9;err = -ENOMEM;
<br>&#x9;entry = audit_init_entry(rule-&#x3E;field_count);
<br>&#x9;if (!entry)
<br>&#x9;&#x9;goto exit_err;
<br>
<br>&#x9;entry-&#x3E;rule.flags = rule-&#x3E;flags &#x26; AUDIT_FILTER_PREPEND;
<br>&#x9;entry-&#x3E;rule.listnr = listnr;
<br>&#x9;entry-&#x3E;rule.action = rule-&#x3E;action;
<br>&#x9;entry-&#x3E;rule.field_count = rule-&#x3E;field_count;
<br>
<br>&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
<br>&#x9;&#x9;entry-&#x3E;rule.mask[i] = rule-&#x3E;mask[i];
<br>
<br>&#x9;for (i = 0; i &#x3C; AUDIT_SYSCALL_CLASSES; i++) {
<br>&#x9;&#x9;int bit = AUDIT_BITMASK_SIZE 32 - i - 1;
<br>&#x9;&#x9;__u32p = &#x26;entry-&#x3E;rule.mask[AUDIT_WORD(bit)];
<br>&#x9;&#x9;__u32class;
<br>
<br>&#x9;&#x9;if (!(*p &#x26; AUDIT_BIT(bit)))
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;*p &#x26;= ~AUDIT_BIT(bit);
<br>&#x9;&#x9;class = classes[i];
<br>&#x9;&#x9;if (class) {
<br>&#x9;&#x9;&#x9;int j;
<br>&#x9;&#x9;&#x9;for (j = 0; j &#x3C; AUDIT_BITMASK_SIZE; j++)
<br>&#x9;&#x9;&#x9;&#x9;entry-&#x3E;rule.mask[j] |= class[j];
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;return entry;
<br>
<br>exit_err:
<br>&#x9;return ERR_PTR(err);
<br>}
<br>
<br>static u32 audit_ops[] =
<br>{
<br>&#x9;[Audit_equal] = AUDIT_EQUAL,
<br>&#x9;[Audit_not_equal] = AUDIT_NOT_EQUAL,
<br>&#x9;[Audit_bitmask] = AUDIT_BIT_MASK,
<br>&#x9;[Audit_bittest] = AUDIT_BIT_TEST,
<br>&#x9;[Audit_lt] = AUDIT_LESS_THAN,
<br>&#x9;[Audit_gt] = AUDIT_GREATER_THAN,
<br>&#x9;[Audit_le] = AUDIT_LESS_THAN_OR_EQUAL,
<br>&#x9;[Audit_ge] = AUDIT_GREATER_THAN_OR_EQUAL,
<br>};
<br>
<br>static u32 audit_to_op(u32 op)
<br>{
<br>&#x9;u32 n;
<br>&#x9;for (n = Audit_equal; n &#x3C; Audit_bad &#x26;&#x26; audit_ops[n] != op; n++)
<br>&#x9;&#x9;;
<br>&#x9;return n;
<br>}
<br>
<br>*/ check if an audit field is valid /*
<br>static int audit_field_valid(struct audit_entryentry, struct audit_fieldf)
<br>{
<br>&#x9;switch(f-&#x3E;type) {
<br>&#x9;case AUDIT_MSGTYPE:
<br>&#x9;&#x9;if (entry-&#x3E;rule.listnr != AUDIT_FILTER_TYPE &#x26;&#x26;
<br>&#x9;&#x9;    entry-&#x3E;rule.listnr != AUDIT_FILTER_USER)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;};
<br>
<br>&#x9;switch(f-&#x3E;type) {
<br>&#x9;default:
<br>&#x9;&#x9;return -EINVAL;
<br>&#x9;case AUDIT_UID:
<br>&#x9;case AUDIT_EUID:
<br>&#x9;case AUDIT_SUID:
<br>&#x9;case AUDIT_FSUID:
<br>&#x9;case AUDIT_LOGINUID:
<br>&#x9;case AUDIT_OBJ_UID:
<br>&#x9;case AUDIT_GID:
<br>&#x9;case AUDIT_EGID:
<br>&#x9;case AUDIT_SGID:
<br>&#x9;case AUDIT_FSGID:
<br>&#x9;case AUDIT_OBJ_GID:
<br>&#x9;case AUDIT_PID:
<br>&#x9;case AUDIT_PERS:
<br>&#x9;case AUDIT_MSGTYPE:
<br>&#x9;case AUDIT_PPID:
<br>&#x9;case AUDIT_DEVMAJOR:
<br>&#x9;case AUDIT_DEVMINOR:
<br>&#x9;case AUDIT_EXIT:
<br>&#x9;case AUDIT_SUCCESS:
<br>&#x9;case AUDIT_INODE:
<br>&#x9;&#x9;*/ bit ops are only useful on syscall args /*
<br>&#x9;&#x9;if (f-&#x3E;op == Audit_bitmask || f-&#x3E;op == Audit_bittest)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_ARG0:
<br>&#x9;case AUDIT_ARG1:
<br>&#x9;case AUDIT_ARG2:
<br>&#x9;case AUDIT_ARG3:
<br>&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;case AUDIT_OBJ_USER:
<br>&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;case AUDIT_WATCH:
<br>&#x9;case AUDIT_DIR:
<br>&#x9;case AUDIT_FILTERKEY:
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_LOGINUID_SET:
<br>&#x9;&#x9;if ((f-&#x3E;val != 0) &#x26;&#x26; (f-&#x3E;val != 1))
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;*/ FALL THROUGH /*
<br>&#x9;case AUDIT_ARCH:
<br>&#x9;&#x9;if (f-&#x3E;op != Audit_not_equal &#x26;&#x26; f-&#x3E;op != Audit_equal)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_PERM:
<br>&#x9;&#x9;if (f-&#x3E;val &#x26; ~15)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_FILETYPE:
<br>&#x9;&#x9;if (f-&#x3E;val &#x26; ~S_IFMT)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_FIELD_COMPARE:
<br>&#x9;&#x9;if (f-&#x3E;val &#x3E; AUDIT_MAX_FIELD_COMPARE)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_EXE:
<br>&#x9;&#x9;if (f-&#x3E;op != Audit_equal)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;if (entry-&#x3E;rule.listnr != AUDIT_FILTER_EXIT)
<br>&#x9;&#x9;&#x9;return -EINVAL;
<br>&#x9;&#x9;break;
<br>&#x9;};
<br>&#x9;return 0;
<br>}
<br>
<br>*/ Translate struct audit_rule_data to kernel&#x27;s rule representation. /*
<br>static struct audit_entryaudit_data_to_entry(struct audit_rule_datadata,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       size_t datasz)
<br>{
<br>&#x9;int err = 0;
<br>&#x9;struct audit_entryentry;
<br>&#x9;voidbufp;
<br>&#x9;size_t remain = datasz - sizeof(struct audit_rule_data);
<br>&#x9;int i;
<br>&#x9;charstr;
<br>&#x9;struct audit_fsnotify_markaudit_mark;
<br>
<br>&#x9;entry = audit_to_entry_common(data);
<br>&#x9;if (IS_ERR(entry))
<br>&#x9;&#x9;goto exit_nofree;
<br>
<br>&#x9;bufp = data-&#x3E;buf;
<br>&#x9;for (i = 0; i &#x3C; data-&#x3E;field_count; i++) {
<br>&#x9;&#x9;struct audit_fieldf = &#x26;entry-&#x3E;rule.fields[i];
<br>
<br>&#x9;&#x9;err = -EINVAL;
<br>
<br>&#x9;&#x9;f-&#x3E;op = audit_to_op(data-&#x3E;fieldflags[i]);
<br>&#x9;&#x9;if (f-&#x3E;op == Audit_bad)
<br>&#x9;&#x9;&#x9;goto exit_free;
<br>
<br>&#x9;&#x9;f-&#x3E;type = data-&#x3E;fields[i];
<br>&#x9;&#x9;f-&#x3E;val = data-&#x3E;values[i];
<br>
<br>&#x9;&#x9;*/ Support legacy tests for a valid loginuid /*
<br>&#x9;&#x9;if ((f-&#x3E;type == AUDIT_LOGINUID) &#x26;&#x26; (f-&#x3E;val == AUDIT_UID_UNSET)) {
<br>&#x9;&#x9;&#x9;f-&#x3E;type = AUDIT_LOGINUID_SET;
<br>&#x9;&#x9;&#x9;f-&#x3E;val = 0;
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.pflags |= AUDIT_LOGINUID_LEGACY;
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;err = audit_field_valid(entry, f);
<br>&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;goto exit_free;
<br>
<br>&#x9;&#x9;err = -EINVAL;
<br>&#x9;&#x9;switch (f-&#x3E;type) {
<br>&#x9;&#x9;case AUDIT_LOGINUID:
<br>&#x9;&#x9;case AUDIT_UID:
<br>&#x9;&#x9;case AUDIT_EUID:
<br>&#x9;&#x9;case AUDIT_SUID:
<br>&#x9;&#x9;case AUDIT_FSUID:
<br>&#x9;&#x9;case AUDIT_OBJ_UID:
<br>&#x9;&#x9;&#x9;f-&#x3E;uid = make_kuid(current_user_ns(), f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (!uid_valid(f-&#x3E;uid))
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_GID:
<br>&#x9;&#x9;case AUDIT_EGID:
<br>&#x9;&#x9;case AUDIT_SGID:
<br>&#x9;&#x9;case AUDIT_FSGID:
<br>&#x9;&#x9;case AUDIT_OBJ_GID:
<br>&#x9;&#x9;&#x9;f-&#x3E;gid = make_kgid(current_user_ns(), f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (!gid_valid(f-&#x3E;gid))
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_ARCH:
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.arch_f = f;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;&#x9;case AUDIT_OBJ_USER:
<br>&#x9;&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (IS_ERR(str))
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;
<br>
<br>&#x9;&#x9;&#x9;err = security_audit_rule_init(f-&#x3E;type, f-&#x3E;op, str,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;       (void*)&#x26;f-&#x3E;lsm_rule);
<br>&#x9;&#x9;&#x9;*/ Keep currently invalid fields around in case they
<br>&#x9;&#x9;&#x9; become valid after a policy reload. /*
<br>&#x9;&#x9;&#x9;if (err == -EINVAL) {
<br>&#x9;&#x9;&#x9;&#x9;pr_warn(&#x22;audit rule for LSM \&#x27;%s\&#x27; is invalid\n&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;str);
<br>&#x9;&#x9;&#x9;&#x9;err = 0;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;if (err) {
<br>&#x9;&#x9;&#x9;&#x9;kfree(str);
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;} else
<br>&#x9;&#x9;&#x9;&#x9;f-&#x3E;lsm_str = str;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_WATCH:
<br>&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (IS_ERR(str))
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;
<br>
<br>&#x9;&#x9;&#x9;err = audit_to_watch(&#x26;entry-&#x3E;rule, str, f-&#x3E;val, f-&#x3E;op);
<br>&#x9;&#x9;&#x9;if (err) {
<br>&#x9;&#x9;&#x9;&#x9;kfree(str);
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_DIR:
<br>&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (IS_ERR(str))
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;
<br>
<br>&#x9;&#x9;&#x9;err = audit_make_tree(&#x26;entry-&#x3E;rule, str, f-&#x3E;op);
<br>&#x9;&#x9;&#x9;kfree(str);
<br>&#x9;&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_INODE:
<br>&#x9;&#x9;&#x9;err = audit_to_inode(&#x26;entry-&#x3E;rule, f);
<br>&#x9;&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FILTERKEY:
<br>&#x9;&#x9;&#x9;if (entry-&#x3E;rule.filterkey || f-&#x3E;val &#x3E; AUDIT_MAX_KEY_LEN)
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (IS_ERR(str))
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.filterkey = str;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EXE:
<br>&#x9;&#x9;&#x9;if (entry-&#x3E;rule.exe || f-&#x3E;val &#x3E; PATH_MAX)
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (IS_ERR(str)) {
<br>&#x9;&#x9;&#x9;&#x9;err = PTR_ERR(str);
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;
<br>
<br>&#x9;&#x9;&#x9;audit_mark = audit_alloc_mark(&#x26;entry-&#x3E;rule, str, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;if (IS_ERR(audit_mark)) {
<br>&#x9;&#x9;&#x9;&#x9;kfree(str);
<br>&#x9;&#x9;&#x9;&#x9;err = PTR_ERR(audit_mark);
<br>&#x9;&#x9;&#x9;&#x9;goto exit_free;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.exe = audit_mark;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;if (entry-&#x3E;rule.inode_f &#x26;&#x26; entry-&#x3E;rule.inode_f-&#x3E;op == Audit_not_equal)
<br>&#x9;&#x9;entry-&#x3E;rule.inode_f = NULL;
<br>
<br>exit_nofree:
<br>&#x9;return entry;
<br>
<br>exit_free:
<br>&#x9;if (entry-&#x3E;rule.tree)
<br>&#x9;&#x9;audit_put_tree(entry-&#x3E;rule.tree);/ that&#x27;s the temporary one /*
<br>&#x9;if (entry-&#x3E;rule.exe)
<br>&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);/ that&#x27;s the template one /*
<br>&#x9;audit_free_rule(entry);
<br>&#x9;return ERR_PTR(err);
<br>}
<br>
<br>*/ Pack a filter field&#x27;s string representation into data block. /*
<br>static inline size_t audit_pack_string(void*bufp, const charstr)
<br>{
<br>&#x9;size_t len = strlen(str);
<br>
<br>&#x9;memcpy(*bufp, str, len);
<br>&#x9;*bufp += len;
<br>
<br>&#x9;return len;
<br>}
<br>
<br>*/ Translate kernel rule representation to struct audit_rule_data. /*
<br>static struct audit_rule_dataaudit_krule_to_data(struct audit_krulekrule)
<br>{
<br>&#x9;struct audit_rule_datadata;
<br>&#x9;voidbufp;
<br>&#x9;int i;
<br>
<br>&#x9;data = kmalloc(sizeof(*data) + krule-&#x3E;buflen, GFP_KERNEL);
<br>&#x9;if (unlikely(!data))
<br>&#x9;&#x9;return NULL;
<br>&#x9;memset(data, 0, sizeof(*data));
<br>
<br>&#x9;data-&#x3E;flags = krule-&#x3E;flags | krule-&#x3E;listnr;
<br>&#x9;data-&#x3E;action = krule-&#x3E;action;
<br>&#x9;data-&#x3E;field_count = krule-&#x3E;field_count;
<br>&#x9;bufp = data-&#x3E;buf;
<br>&#x9;for (i = 0; i &#x3C; data-&#x3E;field_count; i++) {
<br>&#x9;&#x9;struct audit_fieldf = &#x26;krule-&#x3E;fields[i];
<br>
<br>&#x9;&#x9;data-&#x3E;fields[i] = f-&#x3E;type;
<br>&#x9;&#x9;data-&#x3E;fieldflags[i] = audit_ops[f-&#x3E;op];
<br>&#x9;&#x9;switch(f-&#x3E;type) {
<br>&#x9;&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;&#x9;case AUDIT_OBJ_USER:
<br>&#x9;&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
<br>&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp, f-&#x3E;lsm_str);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_WATCH:
<br>&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
<br>&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  audit_watch_path(krule-&#x3E;watch));
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_DIR:
<br>&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
<br>&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  audit_tree_path(krule-&#x3E;tree));
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FILTERKEY:
<br>&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
<br>&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp, krule-&#x3E;filterkey);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EXE:
<br>&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
<br>&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp, audit_mark_path(krule-&#x3E;exe));
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_LOGINUID_SET:
<br>&#x9;&#x9;&#x9;if (krule-&#x3E;pflags &#x26; AUDIT_LOGINUID_LEGACY &#x26;&#x26; !f-&#x3E;val) {
<br>&#x9;&#x9;&#x9;&#x9;data-&#x3E;fields[i] = AUDIT_LOGINUID;
<br>&#x9;&#x9;&#x9;&#x9;data-&#x3E;values[i] = AUDIT_UID_UNSET;
<br>&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;*/ fallthrough if set /*
<br>&#x9;&#x9;default:
<br>&#x9;&#x9;&#x9;data-&#x3E;values[i] = f-&#x3E;val;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++) data-&#x3E;mask[i] = krule-&#x3E;mask[i];
<br>
<br>&#x9;return data;
<br>}
<br>
<br>*/ Compare two rules in kernel format.  Considered success if rules
<br> don&#x27;t match. /*
<br>static int audit_compare_rule(struct audit_krulea, struct audit_kruleb)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;if (a-&#x3E;flags != b-&#x3E;flags ||
<br>&#x9;    a-&#x3E;pflags != b-&#x3E;pflags ||
<br>&#x9;    a-&#x3E;listnr != b-&#x3E;listnr ||
<br>&#x9;    a-&#x3E;action != b-&#x3E;action ||
<br>&#x9;    a-&#x3E;field_count != b-&#x3E;field_count)
<br>&#x9;&#x9;return 1;
<br>
<br>&#x9;for (i = 0; i &#x3C; a-&#x3E;field_count; i++) {
<br>&#x9;&#x9;if (a-&#x3E;fields[i].type != b-&#x3E;fields[i].type ||
<br>&#x9;&#x9;    a-&#x3E;fields[i].op != b-&#x3E;fields[i].op)
<br>&#x9;&#x9;&#x9;return 1;
<br>
<br>&#x9;&#x9;switch(a-&#x3E;fields[i].type) {
<br>&#x9;&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;&#x9;case AUDIT_OBJ_USER:
<br>&#x9;&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;&#x9;&#x9;if (strcmp(a-&#x3E;fields[i].lsm_str, b-&#x3E;fields[i].lsm_str))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_WATCH:
<br>&#x9;&#x9;&#x9;if (strcmp(audit_watch_path(a-&#x3E;watch),
<br>&#x9;&#x9;&#x9;&#x9;   audit_watch_path(b-&#x3E;watch)))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_DIR:
<br>&#x9;&#x9;&#x9;if (strcmp(audit_tree_path(a-&#x3E;tree),
<br>&#x9;&#x9;&#x9;&#x9;   audit_tree_path(b-&#x3E;tree)))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FILTERKEY:
<br>&#x9;&#x9;&#x9;*/ both filterkeys exist based on above type compare /*
<br>&#x9;&#x9;&#x9;if (strcmp(a-&#x3E;filterkey, b-&#x3E;filterkey))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EXE:
<br>&#x9;&#x9;&#x9;*/ both paths exist based on above type compare /*
<br>&#x9;&#x9;&#x9;if (strcmp(audit_mark_path(a-&#x3E;exe),
<br>&#x9;&#x9;&#x9;&#x9;   audit_mark_path(b-&#x3E;exe)))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_UID:
<br>&#x9;&#x9;case AUDIT_EUID:
<br>&#x9;&#x9;case AUDIT_SUID:
<br>&#x9;&#x9;case AUDIT_FSUID:
<br>&#x9;&#x9;case AUDIT_LOGINUID:
<br>&#x9;&#x9;case AUDIT_OBJ_UID:
<br>&#x9;&#x9;&#x9;if (!uid_eq(a-&#x3E;fields[i].uid, b-&#x3E;fields[i].uid))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_GID:
<br>&#x9;&#x9;case AUDIT_EGID:
<br>&#x9;&#x9;case AUDIT_SGID:
<br>&#x9;&#x9;case AUDIT_FSGID:
<br>&#x9;&#x9;case AUDIT_OBJ_GID:
<br>&#x9;&#x9;&#x9;if (!gid_eq(a-&#x3E;fields[i].gid, b-&#x3E;fields[i].gid))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;default:
<br>&#x9;&#x9;&#x9;if (a-&#x3E;fields[i].val != b-&#x3E;fields[i].val)
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
<br>&#x9;&#x9;if (a-&#x3E;mask[i] != b-&#x3E;mask[i])
<br>&#x9;&#x9;&#x9;return 1;
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>*/ Duplicate LSM field information.  The lsm_rule is opaque, so must be
<br> re-initialized. /*
<br>static inline int audit_dupe_lsm_field(struct audit_fielddf,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;   struct audit_fieldsf)
<br>{
<br>&#x9;int ret = 0;
<br>&#x9;charlsm_str;
<br>
<br>&#x9;*/ our own copy of lsm_str /*
<br>&#x9;lsm_str = kstrdup(sf-&#x3E;lsm_str, GFP_KERNEL);
<br>&#x9;if (unlikely(!lsm_str))
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;df-&#x3E;lsm_str = lsm_str;
<br>
<br>&#x9;*/ our own (refreshed) copy of lsm_rule /*
<br>&#x9;ret = security_audit_rule_init(df-&#x3E;type, df-&#x3E;op, df-&#x3E;lsm_str,
<br>&#x9;&#x9;&#x9;&#x9;       (void*)&#x26;df-&#x3E;lsm_rule);
<br>&#x9;*/ Keep currently invalid fields around in case they
<br>&#x9; become valid after a policy reload. /*
<br>&#x9;if (ret == -EINVAL) {
<br>&#x9;&#x9;pr_warn(&#x22;audit rule for LSM \&#x27;%s\&#x27; is invalid\n&#x22;,
<br>&#x9;&#x9;&#x9;df-&#x3E;lsm_str);
<br>&#x9;&#x9;ret = 0;
<br>&#x9;}
<br>
<br>&#x9;return ret;
<br>}
<br>
<br>*/ Duplicate an audit rule.  This will be a deep copy with the exception
<br> of the watch - that pointer is carried over.  The LSM specific fields
<br> will be updated in the copy.  The point is to be able to replace the old
<br> rule with the new rule in the filterlist, then free the old rule.
<br> The rlist element is undefined; list manipulations are handled apart from
<br> the initial copy. /*
<br>struct audit_entryaudit_dupe_rule(struct audit_kruleold)
<br>{
<br>&#x9;u32 fcount = old-&#x3E;field_count;
<br>&#x9;struct audit_entryentry;
<br>&#x9;struct audit_krulenew;
<br>&#x9;charfk;
<br>&#x9;int i, err = 0;
<br>
<br>&#x9;entry = audit_init_entry(fcount);
<br>&#x9;if (unlikely(!entry))
<br>&#x9;&#x9;return ERR_PTR(-ENOMEM);
<br>
<br>&#x9;new = &#x26;entry-&#x3E;rule;
<br>&#x9;new-&#x3E;flags = old-&#x3E;flags;
<br>&#x9;new-&#x3E;pflags = old-&#x3E;pflags;
<br>&#x9;new-&#x3E;listnr = old-&#x3E;listnr;
<br>&#x9;new-&#x3E;action = old-&#x3E;action;
<br>&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
<br>&#x9;&#x9;new-&#x3E;mask[i] = old-&#x3E;mask[i];
<br>&#x9;new-&#x3E;prio = old-&#x3E;prio;
<br>&#x9;new-&#x3E;buflen = old-&#x3E;buflen;
<br>&#x9;new-&#x3E;inode_f = old-&#x3E;inode_f;
<br>&#x9;new-&#x3E;field_count = old-&#x3E;field_count;
<br>
<br>&#x9;*/
<br>&#x9; note that we are OK with not refcounting here; audit_match_tree()
<br>&#x9; never dereferences tree and we can&#x27;t get false positives there
<br>&#x9; since we&#x27;d have to have rule gone from the listand* removed
<br>&#x9; before the chunks found by lookup had been allocated, i.e. before
<br>&#x9; the beginning of list scan.
<br>&#x9; /*
<br>&#x9;new-&#x3E;tree = old-&#x3E;tree;
<br>&#x9;memcpy(new-&#x3E;fields, old-&#x3E;fields, sizeof(struct audit_field) fcount);
<br>
<br>&#x9;*/ deep copy this information, updating the lsm_rule fields, because
<br>&#x9; the originals will all be freed when the old rule is freed. /*
<br>&#x9;for (i = 0; i &#x3C; fcount; i++) {
<br>&#x9;&#x9;switch (new-&#x3E;fields[i].type) {
<br>&#x9;&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;&#x9;case AUDIT_OBJ_USER:
<br>&#x9;&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;&#x9;&#x9;err = audit_dupe_lsm_field(&#x26;new-&#x3E;fields[i],
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;       &#x26;old-&#x3E;fields[i]);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FILTERKEY:
<br>&#x9;&#x9;&#x9;fk = kstrdup(old-&#x3E;filterkey, GFP_KERNEL);
<br>&#x9;&#x9;&#x9;if (unlikely(!fk))
<br>&#x9;&#x9;&#x9;&#x9;err = -ENOMEM;
<br>&#x9;&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;&#x9;new-&#x3E;filterkey = fk;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EXE:
<br>&#x9;&#x9;&#x9;err = audit_dupe_exe(new, old);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (err) {
<br>&#x9;&#x9;&#x9;if (new-&#x3E;exe)
<br>&#x9;&#x9;&#x9;&#x9;audit_remove_mark(new-&#x3E;exe);
<br>&#x9;&#x9;&#x9;audit_free_rule(entry);
<br>&#x9;&#x9;&#x9;return ERR_PTR(err);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;if (old-&#x3E;watch) {
<br>&#x9;&#x9;audit_get_watch(old-&#x3E;watch);
<br>&#x9;&#x9;new-&#x3E;watch = old-&#x3E;watch;
<br>&#x9;}
<br>
<br>&#x9;return entry;
<br>}
<br>
<br>*/ Find an existing audit rule.
<br> Caller must hold audit_filter_mutex to prevent stale rule data. /*
<br>static struct audit_entryaudit_find_rule(struct audit_entryentry,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;   struct list_head*p)
<br>{
<br>&#x9;struct audit_entrye,found = NULL;
<br>&#x9;struct list_headlist;
<br>&#x9;int h;
<br>
<br>&#x9;if (entry-&#x3E;rule.inode_f) {
<br>&#x9;&#x9;h = audit_hash_ino(entry-&#x3E;rule.inode_f-&#x3E;val);
<br>&#x9;&#x9;*p = list = &#x26;audit_inode_hash[h];
<br>&#x9;} else if (entry-&#x3E;rule.watch) {
<br>&#x9;&#x9;*/ we don&#x27;t know the inode number, so must walk entire hash /*
<br>&#x9;&#x9;for (h = 0; h &#x3C; AUDIT_INODE_BUCKETS; h++) {
<br>&#x9;&#x9;&#x9;list = &#x26;audit_inode_hash[h];
<br>&#x9;&#x9;&#x9;list_for_each_entry(e, list, list)
<br>&#x9;&#x9;&#x9;&#x9;if (!audit_compare_rule(&#x26;entry-&#x3E;rule, &#x26;e-&#x3E;rule)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;found = e;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;&#x9;goto out;
<br>&#x9;} else {
<br>&#x9;&#x9;*p = list = &#x26;audit_filter_list[entry-&#x3E;rule.listnr];
<br>&#x9;}
<br>
<br>&#x9;list_for_each_entry(e, list, list)
<br>&#x9;&#x9;if (!audit_compare_rule(&#x26;entry-&#x3E;rule, &#x26;e-&#x3E;rule)) {
<br>&#x9;&#x9;&#x9;found = e;
<br>&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;}
<br>
<br>out:
<br>&#x9;return found;
<br>}
<br>
<br>static u64 prio_low = ~0ULL/2;
<br>static u64 prio_high = ~0ULL/2 - 1;
<br>
<br>*/ Add rule to given filterlist if not a duplicate. /*
<br>static inline int audit_add_rule(struct audit_entryentry)
<br>{
<br>&#x9;struct audit_entrye;
<br>&#x9;struct audit_watchwatch = entry-&#x3E;rule.watch;
<br>&#x9;struct audit_treetree = entry-&#x3E;rule.tree;
<br>&#x9;struct list_headlist;
<br>&#x9;int err = 0;
<br>#ifdef CONFIG_AUDITSYSCALL
<br>&#x9;int dont_count = 0;
<br>
<br>&#x9;*/ If either of these, don&#x27;t count towards total /*
<br>&#x9;if (entry-&#x3E;rule.listnr == AUDIT_FILTER_USER ||
<br>&#x9;&#x9;entry-&#x3E;rule.listnr == AUDIT_FILTER_TYPE)
<br>&#x9;&#x9;dont_count = 1;
<br>#endif
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;e = audit_find_rule(entry, &#x26;list);
<br>&#x9;if (e) {
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;err = -EEXIST;
<br>&#x9;&#x9;*/ normally audit_add_tree_rule() will free it on failure /*
<br>&#x9;&#x9;if (tree)
<br>&#x9;&#x9;&#x9;audit_put_tree(tree);
<br>&#x9;&#x9;return err;
<br>&#x9;}
<br>
<br>&#x9;if (watch) {
<br>&#x9;&#x9;*/ audit_filter_mutex is dropped and re-taken during this call /*
<br>&#x9;&#x9;err = audit_add_watch(&#x26;entry-&#x3E;rule, &#x26;list);
<br>&#x9;&#x9;if (err) {
<br>&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;&#x9;*/
<br>&#x9;&#x9;&#x9; normally audit_add_tree_rule() will free it
<br>&#x9;&#x9;&#x9; on failure
<br>&#x9;&#x9;&#x9; /*
<br>&#x9;&#x9;&#x9;if (tree)
<br>&#x9;&#x9;&#x9;&#x9;audit_put_tree(tree);
<br>&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;if (tree) {
<br>&#x9;&#x9;err = audit_add_tree_rule(&#x26;entry-&#x3E;rule);
<br>&#x9;&#x9;if (err) {
<br>&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;&#x9;return err;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;entry-&#x3E;rule.prio = ~0ULL;
<br>&#x9;if (entry-&#x3E;rule.listnr == AUDIT_FILTER_EXIT) {
<br>&#x9;&#x9;if (entry-&#x3E;rule.flags &#x26; AUDIT_FILTER_PREPEND)
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.prio = ++prio_high;
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;entry-&#x3E;rule.prio = --prio_low;
<br>&#x9;}
<br>
<br>&#x9;if (entry-&#x3E;rule.flags &#x26; AUDIT_FILTER_PREPEND) {
<br>&#x9;&#x9;list_add(&#x26;entry-&#x3E;rule.list,
<br>&#x9;&#x9;&#x9; &#x26;audit_rules_list[entry-&#x3E;rule.listnr]);
<br>&#x9;&#x9;list_add_rcu(&#x26;entry-&#x3E;list, list);
<br>&#x9;&#x9;entry-&#x3E;rule.flags &#x26;= ~AUDIT_FILTER_PREPEND;
<br>&#x9;} else {
<br>&#x9;&#x9;list_add_tail(&#x26;entry-&#x3E;rule.list,
<br>&#x9;&#x9;&#x9;      &#x26;audit_rules_list[entry-&#x3E;rule.listnr]);
<br>&#x9;&#x9;list_add_tail_rcu(&#x26;entry-&#x3E;list, list);
<br>&#x9;}
<br>#ifdef CONFIG_AUDITSYSCALL
<br>&#x9;if (!dont_count)
<br>&#x9;&#x9;audit_n_rules++;
<br>
<br>&#x9;if (!audit_match_signal(entry))
<br>&#x9;&#x9;audit_signals++;
<br>#endif
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;return err;
<br>}
<br>
<br>*/ Remove an existing rule from filterlist. /*
<br>int audit_del_rule(struct audit_entryentry)
<br>{
<br>&#x9;struct audit_entry e;
<br>&#x9;struct audit_treetree = entry-&#x3E;rule.tree;
<br>&#x9;struct list_headlist;
<br>&#x9;int ret = 0;
<br>#ifdef CONFIG_AUDITSYSCALL
<br>&#x9;int dont_count = 0;
<br>
<br>&#x9;*/ If either of these, don&#x27;t count towards total /*
<br>&#x9;if (entry-&#x3E;rule.listnr == AUDIT_FILTER_USER ||
<br>&#x9;&#x9;entry-&#x3E;rule.listnr == AUDIT_FILTER_TYPE)
<br>&#x9;&#x9;dont_count = 1;
<br>#endif
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;e = audit_find_rule(entry, &#x26;list);
<br>&#x9;if (!e) {
<br>&#x9;&#x9;ret = -ENOENT;
<br>&#x9;&#x9;goto out;
<br>&#x9;}
<br>
<br>&#x9;if (e-&#x3E;rule.watch)
<br>&#x9;&#x9;audit_remove_watch_rule(&#x26;e-&#x3E;rule);
<br>
<br>&#x9;if (e-&#x3E;rule.tree)
<br>&#x9;&#x9;audit_remove_tree_rule(&#x26;e-&#x3E;rule);
<br>
<br>&#x9;if (e-&#x3E;rule.exe)
<br>&#x9;&#x9;audit_remove_mark_rule(&#x26;e-&#x3E;rule);
<br>
<br>#ifdef CONFIG_AUDITSYSCALL
<br>&#x9;if (!dont_count)
<br>&#x9;&#x9;audit_n_rules--;
<br>
<br>&#x9;if (!audit_match_signal(entry))
<br>&#x9;&#x9;audit_signals--;
<br>#endif
<br>
<br>&#x9;list_del_rcu(&#x26;e-&#x3E;list);
<br>&#x9;list_del(&#x26;e-&#x3E;rule.list);
<br>&#x9;call_rcu(&#x26;e-&#x3E;rcu, audit_free_rule_rcu);
<br>
<br>out:
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;if (tree)
<br>&#x9;&#x9;audit_put_tree(tree);&#x9;*/ that&#x27;s the temporary one /*
<br>
<br>&#x9;return ret;
<br>}
<br>
<br>*/ List rules using struct audit_rule_data. /*
<br>static void audit_list_rules(__u32 portid, int seq, struct sk_buff_headq)
<br>{
<br>&#x9;struct sk_buffskb;
<br>&#x9;struct audit_kruler;
<br>&#x9;int i;
<br>
<br>&#x9;*/ This is a blocking read, so use audit_filter_mutex instead of rcu
<br>&#x9; iterator to sync with list writers. /*
<br>&#x9;for (i=0; i&#x3C;AUDIT_NR_FILTERS; i++) {
<br>&#x9;&#x9;list_for_each_entry(r, &#x26;audit_rules_list[i], list) {
<br>&#x9;&#x9;&#x9;struct audit_rule_datadata;
<br>
<br>&#x9;&#x9;&#x9;data = audit_krule_to_data(r);
<br>&#x9;&#x9;&#x9;if (unlikely(!data))
<br>&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       0, 1, data,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       sizeof(*data) + data-&#x3E;buflen);
<br>&#x9;&#x9;&#x9;if (skb)
<br>&#x9;&#x9;&#x9;&#x9;skb_queue_tail(q, skb);
<br>&#x9;&#x9;&#x9;kfree(data);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES, 1, 1, NULL, 0);
<br>&#x9;if (skb)
<br>&#x9;&#x9;skb_queue_tail(q, skb);
<br>}
<br>
<br>*/ Log rule additions and removals /*
<br>static void audit_log_rule_change(charaction, struct audit_krulerule, int res)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;uid_t loginuid = from_kuid(&#x26;init_user_ns, audit_get_loginuid(current));
<br>&#x9;unsigned int sessionid = audit_get_sessionid(current);
<br>
<br>&#x9;if (!audit_enabled)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_format(ab, &#x22;auid=%u ses=%u&#x22; ,loginuid, sessionid);
<br>&#x9;audit_log_task_context(ab);
<br>&#x9;audit_log_format(ab, &#x22; op=&#x22;);
<br>&#x9;audit_log_string(ab, action);
<br>&#x9;audit_log_key(ab, rule-&#x3E;filterkey);
<br>&#x9;audit_log_format(ab, &#x22; list=%d res=%d&#x22;, rule-&#x3E;listnr, res);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>*/
<br> audit_rule_change - apply all rules to the specified message type
<br> @type: audit message type
<br> @portid: target port id for netlink audit messages
<br> @seq: netlink audit message sequence (serial) number
<br> @data: payload data
<br> @datasz: size of payload data
<br> /*
<br>int audit_rule_change(int type, __u32 portid, int seq, voiddata,
<br>&#x9;&#x9;&#x9;size_t datasz)
<br>{
<br>&#x9;int err = 0;
<br>&#x9;struct audit_entryentry;
<br>
<br>&#x9;entry = audit_data_to_entry(data, datasz);
<br>&#x9;if (IS_ERR(entry))
<br>&#x9;&#x9;return PTR_ERR(entry);
<br>
<br>&#x9;switch (type) {
<br>&#x9;case AUDIT_ADD_RULE:
<br>&#x9;&#x9;err = audit_add_rule(entry);
<br>&#x9;&#x9;audit_log_rule_change(&#x22;add_rule&#x22;, &#x26;entry-&#x3E;rule, !err);
<br>&#x9;&#x9;break;
<br>&#x9;case AUDIT_DEL_RULE:
<br>&#x9;&#x9;err = audit_del_rule(entry);
<br>&#x9;&#x9;audit_log_rule_change(&#x22;remove_rule&#x22;, &#x26;entry-&#x3E;rule, !err);
<br>&#x9;&#x9;break;
<br>&#x9;default:
<br>&#x9;&#x9;err = -EINVAL;
<br>&#x9;&#x9;WARN_ON(1);
<br>&#x9;}
<br>
<br>&#x9;if (err || type == AUDIT_DEL_RULE) {
<br>&#x9;&#x9;if (entry-&#x3E;rule.exe)
<br>&#x9;&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);
<br>&#x9;&#x9;audit_free_rule(entry);
<br>&#x9;}
<br>
<br>&#x9;return err;
<br>}
<br>
<br>*/
<br> audit_list_rules_send - list the audit rules
<br> @request_skb: skb of request we are replying to (used to target the reply)
<br> @seq: netlink audit message sequence (serial) number
<br> /*
<br>int audit_list_rules_send(struct sk_buffrequest_skb, int seq)
<br>{
<br>&#x9;u32 portid = NETLINK_CB(request_skb).portid;
<br>&#x9;struct netnet = sock_net(NETLINK_CB(request_skb).sk);
<br>&#x9;struct task_structtsk;
<br>&#x9;struct audit_netlink_listdest;
<br>&#x9;int err = 0;
<br>
<br>&#x9;*/ We can&#x27;t just spew out the rules here because we might fill
<br>&#x9; the available socket buffer space and deadlock waiting for
<br>&#x9; auditctl to read from it... which isn&#x27;t ever going to
<br>&#x9; happen if we&#x27;re actually running in the context of auditctl
<br>&#x9; trying to _send_ the stuff /*
<br>
<br>&#x9;dest = kmalloc(sizeof(struct audit_netlink_list), GFP_KERNEL);
<br>&#x9;if (!dest)
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;dest-&#x3E;net = get_net(net);
<br>&#x9;dest-&#x3E;portid = portid;
<br>&#x9;skb_queue_head_init(&#x26;dest-&#x3E;q);
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;audit_list_rules(portid, seq, &#x26;dest-&#x3E;q);
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;tsk = kthread_run(audit_send_list, dest, &#x22;audit_send_list&#x22;);
<br>&#x9;if (IS_ERR(tsk)) {
<br>&#x9;&#x9;skb_queue_purge(&#x26;dest-&#x3E;q);
<br>&#x9;&#x9;kfree(dest);
<br>&#x9;&#x9;err = PTR_ERR(tsk);
<br>&#x9;}
<br>
<br>&#x9;return err;
<br>}
<br>
<br>int audit_comparator(u32 left, u32 op, u32 right)
<br>{
<br>&#x9;switch (op) {
<br>&#x9;case Audit_equal:
<br>&#x9;&#x9;return (left == right);
<br>&#x9;case Audit_not_equal:
<br>&#x9;&#x9;return (left != right);
<br>&#x9;case Audit_lt:
<br>&#x9;&#x9;return (left &#x3C; right);
<br>&#x9;case Audit_le:
<br>&#x9;&#x9;return (left &#x3C;= right);
<br>&#x9;case Audit_gt:
<br>&#x9;&#x9;return (left &#x3E; right);
<br>&#x9;case Audit_ge:
<br>&#x9;&#x9;return (left &#x3E;= right);
<br>&#x9;case Audit_bitmask:
<br>&#x9;&#x9;return (left &#x26; right);
<br>&#x9;case Audit_bittest:
<br>&#x9;&#x9;return ((left &#x26; right) == right);
<br>&#x9;default:
<br>&#x9;&#x9;BUG();
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>}
<br>
<br>int audit_uid_comparator(kuid_t left, u32 op, kuid_t right)
<br>{
<br>&#x9;switch (op) {
<br>&#x9;case Audit_equal:
<br>&#x9;&#x9;return uid_eq(left, right);
<br>&#x9;case Audit_not_equal:
<br>&#x9;&#x9;return !uid_eq(left, right);
<br>&#x9;case Audit_lt:
<br>&#x9;&#x9;return uid_lt(left, right);
<br>&#x9;case Audit_le:
<br>&#x9;&#x9;return uid_lte(left, right);
<br>&#x9;case Audit_gt:
<br>&#x9;&#x9;return uid_gt(left, right);
<br>&#x9;case Audit_ge:
<br>&#x9;&#x9;return uid_gte(left, right);
<br>&#x9;case Audit_bitmask:
<br>&#x9;case Audit_bittest:
<br>&#x9;default:
<br>&#x9;&#x9;BUG();
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>}
<br>
<br>int audit_gid_comparator(kgid_t left, u32 op, kgid_t right)
<br>{
<br>&#x9;switch (op) {
<br>&#x9;case Audit_equal:
<br>&#x9;&#x9;return gid_eq(left, right);
<br>&#x9;case Audit_not_equal:
<br>&#x9;&#x9;return !gid_eq(left, right);
<br>&#x9;case Audit_lt:
<br>&#x9;&#x9;return gid_lt(left, right);
<br>&#x9;case Audit_le:
<br>&#x9;&#x9;return gid_lte(left, right);
<br>&#x9;case Audit_gt:
<br>&#x9;&#x9;return gid_gt(left, right);
<br>&#x9;case Audit_ge:
<br>&#x9;&#x9;return gid_gte(left, right);
<br>&#x9;case Audit_bitmask:
<br>&#x9;case Audit_bittest:
<br>&#x9;default:
<br>&#x9;&#x9;BUG();
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>}
<br>
<br>*/
<br> parent_len - find the length of the parent portion of a pathname
<br> @path: pathname of which to determine length
<br> /*
<br>int parent_len(const charpath)
<br>{
<br>&#x9;int plen;
<br>&#x9;const charp;
<br>
<br>&#x9;plen = strlen(path);
<br>
<br>&#x9;if (plen == 0)
<br>&#x9;&#x9;return plen;
<br>
<br>&#x9;*/ disregard trailing slashes /*
<br>&#x9;p = path + plen - 1;
<br>&#x9;while ((*p == &#x27;/&#x27;) &#x26;&#x26; (p &#x3E; path))
<br>&#x9;&#x9;p--;
<br>
<br>&#x9;*/ walk backward until we find the next slash or hit beginning /*
<br>&#x9;while ((*p != &#x27;/&#x27;) &#x26;&#x26; (p &#x3E; path))
<br>&#x9;&#x9;p--;
<br>
<br>&#x9;*/ did we find a slash? Then increment to include it in path /*
<br>&#x9;if (*p == &#x27;/&#x27;)
<br>&#x9;&#x9;p++;
<br>
<br>&#x9;return p - path;
<br>}
<br>
<br>*/
<br> audit_compare_dname_path - compare given dentry name with last component in
<br> &#x9;&#x9;&#x9;      given path. Return of 0 indicates a match.
<br> @dname:&#x9;dentry name that we&#x27;re comparing
<br> @path:&#x9;full pathname that we&#x27;re comparing
<br> @parentlen:&#x9;length of the parent if known. Passing in AUDIT_NAME_FULL
<br> &#x9;&#x9;here indicates that we must compute this value.
<br> /*
<br>int audit_compare_dname_path(const chardname, const charpath, int parentlen)
<br>{
<br>&#x9;int dlen, pathlen;
<br>&#x9;const charp;
<br>
<br>&#x9;dlen = strlen(dname);
<br>&#x9;pathlen = strlen(path);
<br>&#x9;if (pathlen &#x3C; dlen)
<br>&#x9;&#x9;return 1;
<br>
<br>&#x9;parentlen = parentlen == AUDIT_NAME_FULL ? parent_len(path) : parentlen;
<br>&#x9;if (pathlen - parentlen != dlen)
<br>&#x9;&#x9;return 1;
<br>
<br>&#x9;p = path + parentlen;
<br>
<br>&#x9;return strncmp(p, dname, dlen);
<br>}
<br>
<br>static int audit_filter_user_rules(struct audit_krulerule, int type,
<br>&#x9;&#x9;&#x9;&#x9;   enum audit_statestate)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;for (i = 0; i &#x3C; rule-&#x3E;field_count; i++) {
<br>&#x9;&#x9;struct audit_fieldf = &#x26;rule-&#x3E;fields[i];
<br>&#x9;&#x9;pid_t pid;
<br>&#x9;&#x9;int result = 0;
<br>&#x9;&#x9;u32 sid;
<br>
<br>&#x9;&#x9;switch (f-&#x3E;type) {
<br>&#x9;&#x9;case AUDIT_PID:
<br>&#x9;&#x9;&#x9;pid = task_pid_nr(current);
<br>&#x9;&#x9;&#x9;result = audit_comparator(pid, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_UID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(current_uid(), f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_GID:
<br>&#x9;&#x9;&#x9;result = audit_gid_comparator(current_gid(), f-&#x3E;op, f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_LOGINUID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(audit_get_loginuid(current),
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_LOGINUID_SET:
<br>&#x9;&#x9;&#x9;result = audit_comparator(audit_loginuid_set(current),
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_MSGTYPE:
<br>&#x9;&#x9;&#x9;result = audit_comparator(type, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;&#x9;&#x9;if (f-&#x3E;lsm_rule) {
<br>&#x9;&#x9;&#x9;&#x9;security_task_getsecid(current, &#x26;sid);
<br>&#x9;&#x9;&#x9;&#x9;result = security_audit_rule_match(sid,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   f-&#x3E;type,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   f-&#x3E;op,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   f-&#x3E;lsm_rule,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   NULL);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;if (!result)
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;}
<br>&#x9;switch (rule-&#x3E;action) {
<br>&#x9;case AUDIT_NEVER:   state = AUDIT_DISABLED;&#x9;    break;
<br>&#x9;case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
<br>&#x9;}
<br>&#x9;return 1;
<br>}
<br>
<br>int audit_filter_user(int type)
<br>{
<br>&#x9;enum audit_state state = AUDIT_DISABLED;
<br>&#x9;struct audit_entrye;
<br>&#x9;int rc, ret;
<br>
<br>&#x9;ret = 1;/ Audit by default /*
<br>
<br>&#x9;rcu_read_lock();
<br>&#x9;list_for_each_entry_rcu(e, &#x26;audit_filter_list[AUDIT_FILTER_USER], list) {
<br>&#x9;&#x9;rc = audit_filter_user_rules(&#x26;e-&#x3E;rule, type, &#x26;state);
<br>&#x9;&#x9;if (rc) {
<br>&#x9;&#x9;&#x9;if (rc &#x3E; 0 &#x26;&#x26; state == AUDIT_DISABLED)
<br>&#x9;&#x9;&#x9;&#x9;ret = 0;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;rcu_read_unlock();
<br>
<br>&#x9;return ret;
<br>}
<br>
<br>int audit_filter_type(int type)
<br>{
<br>&#x9;struct audit_entrye;
<br>&#x9;int result = 0;
<br>
<br>&#x9;rcu_read_lock();
<br>&#x9;if (list_empty(&#x26;audit_filter_list[AUDIT_FILTER_TYPE]))
<br>&#x9;&#x9;goto unlock_and_return;
<br>
<br>&#x9;list_for_each_entry_rcu(e, &#x26;audit_filter_list[AUDIT_FILTER_TYPE],
<br>&#x9;&#x9;&#x9;&#x9;list) {
<br>&#x9;&#x9;int i;
<br>&#x9;&#x9;for (i = 0; i &#x3C; e-&#x3E;rule.field_count; i++) {
<br>&#x9;&#x9;&#x9;struct audit_fieldf = &#x26;e-&#x3E;rule.fields[i];
<br>&#x9;&#x9;&#x9;if (f-&#x3E;type == AUDIT_MSGTYPE) {
<br>&#x9;&#x9;&#x9;&#x9;result = audit_comparator(type, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;&#x9;if (!result)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (result)
<br>&#x9;&#x9;&#x9;goto unlock_and_return;
<br>&#x9;}
<br>unlock_and_return:
<br>&#x9;rcu_read_unlock();
<br>&#x9;return result;
<br>}
<br>
<br>static int update_lsm_rule(struct audit_kruler)
<br>{
<br>&#x9;struct audit_entryentry = container_of(r, struct audit_entry, rule);
<br>&#x9;struct audit_entrynentry;
<br>&#x9;int err = 0;
<br>
<br>&#x9;if (!security_audit_rule_known(r))
<br>&#x9;&#x9;return 0;
<br>
<br>&#x9;nentry = audit_dupe_rule(r);
<br>&#x9;if (entry-&#x3E;rule.exe)
<br>&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);
<br>&#x9;if (IS_ERR(nentry)) {
<br>&#x9;&#x9;*/ save the first error encountered for the
<br>&#x9;&#x9; return value /*
<br>&#x9;&#x9;err = PTR_ERR(nentry);
<br>&#x9;&#x9;audit_panic(&#x22;error updating LSM filters&#x22;);
<br>&#x9;&#x9;if (r-&#x3E;watch)
<br>&#x9;&#x9;&#x9;list_del(&#x26;r-&#x3E;rlist);
<br>&#x9;&#x9;list_del_rcu(&#x26;entry-&#x3E;list);
<br>&#x9;&#x9;list_del(&#x26;r-&#x3E;list);
<br>&#x9;} else {
<br>&#x9;&#x9;if (r-&#x3E;watch || r-&#x3E;tree)
<br>&#x9;&#x9;&#x9;list_replace_init(&#x26;r-&#x3E;rlist, &#x26;nentry-&#x3E;rule.rlist);
<br>&#x9;&#x9;list_replace_rcu(&#x26;entry-&#x3E;list, &#x26;nentry-&#x3E;list);
<br>&#x9;&#x9;list_replace(&#x26;r-&#x3E;list, &#x26;nentry-&#x3E;rule.list);
<br>&#x9;}
<br>&#x9;call_rcu(&#x26;entry-&#x3E;rcu, audit_free_rule_rcu);
<br>
<br>&#x9;return err;
<br>}
<br>
<br>*/ This function will re-initialize the lsm_rule field of all applicable rules.
<br> It will traverse the filter lists serarching for rules that contain LSM
<br> specific filter fields.  When such a rule is found, it is copied, the
<br> LSM field is re-initialized, and the old rule is replaced with the
<br> updated rule. /*
<br>int audit_update_lsm_rules(void)
<br>{
<br>&#x9;struct audit_kruler,n;
<br>&#x9;int i, err = 0;
<br>
<br>&#x9;*/ audit_filter_mutex synchronizes the writers /*
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;for (i = 0; i &#x3C; AUDIT_NR_FILTERS; i++) {
<br>&#x9;&#x9;list_for_each_entry_safe(r, n, &#x26;audit_rules_list[i], list) {
<br>&#x9;&#x9;&#x9;int res = update_lsm_rule(r);
<br>&#x9;&#x9;&#x9;if (!err)
<br>&#x9;&#x9;&#x9;&#x9;err = res;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;return err;
<br>}
<br>*/
<br> audit_fsnotify.c -- tracking inodes
<br>
<br> Copyright 2003-2009,2014-2015 Red Hat, Inc.
<br> Copyright 2005 Hewlett-Packard Development Company, L.P.
<br> Copyright 2005 IBM Corporation
<br>
<br> This program is free software; you can redistribute it and/or modify
<br> it under the terms of the GNU General Public License as published by
<br> the Free Software Foundation; either version 2 of the License, or
<br> (at your option) any later version.
<br>
<br> This program is distributed in the hope that it will be useful,
<br> but WITHOUT ANY WARRANTY; without even the implied warranty of
<br> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
<br> GNU General Public License for more details.
<br> /*
<br>
<br>#include &#x3C;linux/kernel.h&#x3E;
<br>#include &#x3C;linux/audit.h&#x3E;
<br>#include &#x3C;linux/kthread.h&#x3E;
<br>#include &#x3C;linux/mutex.h&#x3E;
<br>#include &#x3C;linux/fs.h&#x3E;
<br>#include &#x3C;linux/fsnotify_backend.h&#x3E;
<br>#include &#x3C;linux/namei.h&#x3E;
<br>#include &#x3C;linux/netlink.h&#x3E;
<br>#include &#x3C;linux/sched.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/security.h&#x3E;
<br>#include &#x22;audit.h&#x22;
<br>
<br>*/
<br> this mark lives on the parent directory of the inode in question.
<br> but dev, ino, and path are about the child
<br> /*
<br>struct audit_fsnotify_mark {
<br>&#x9;dev_t dev;&#x9;&#x9;*/ associated superblock device /*
<br>&#x9;unsigned long ino;&#x9;*/ associated inode number /*
<br>&#x9;charpath;&#x9;&#x9;*/ insertion path /*
<br>&#x9;struct fsnotify_mark mark;/ fsnotify mark on the inode /*
<br>&#x9;struct audit_krulerule;
<br>};
<br>
<br>*/ fsnotify handle. /*
<br>static struct fsnotify_groupaudit_fsnotify_group;
<br>
<br>*/ fsnotify events we care about. /*
<br>#define AUDIT_FS_EVENTS (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
<br>&#x9;&#x9;&#x9; FS_MOVE_SELF | FS_EVENT_ON_CHILD)
<br>
<br>static void audit_fsnotify_mark_free(struct audit_fsnotify_markaudit_mark)
<br>{
<br>&#x9;kfree(audit_mark-&#x3E;path);
<br>&#x9;kfree(audit_mark);
<br>}
<br>
<br>static void audit_fsnotify_free_mark(struct fsnotify_markmark)
<br>{
<br>&#x9;struct audit_fsnotify_markaudit_mark;
<br>
<br>&#x9;audit_mark = container_of(mark, struct audit_fsnotify_mark, mark);
<br>&#x9;audit_fsnotify_mark_free(audit_mark);
<br>}
<br>
<br>charaudit_mark_path(struct audit_fsnotify_markmark)
<br>{
<br>&#x9;return mark-&#x3E;path;
<br>}
<br>
<br>int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev)
<br>{
<br>&#x9;if (mark-&#x3E;ino == AUDIT_INO_UNSET)
<br>&#x9;&#x9;return 0;
<br>&#x9;return (mark-&#x3E;ino == ino) &#x26;&#x26; (mark-&#x3E;dev == dev);
<br>}
<br>
<br>static void audit_update_mark(struct audit_fsnotify_markaudit_mark,
<br>&#x9;&#x9;&#x9;     struct inodeinode)
<br>{
<br>&#x9;audit_mark-&#x3E;dev = inode ? inode-&#x3E;i_sb-&#x3E;s_dev : AUDIT_DEV_UNSET;
<br>&#x9;audit_mark-&#x3E;ino = inode ? inode-&#x3E;i_ino : AUDIT_INO_UNSET;
<br>}
<br>
<br>struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len)
<br>{
<br>&#x9;struct audit_fsnotify_markaudit_mark;
<br>&#x9;struct path path;
<br>&#x9;struct dentrydentry;
<br>&#x9;struct inodeinode;
<br>&#x9;int ret;
<br>
<br>&#x9;if (pathname[0] != &#x27;/&#x27; || pathname[len-1] == &#x27;/&#x27;)
<br>&#x9;&#x9;return ERR_PTR(-EINVAL);
<br>
<br>&#x9;dentry = kern_path_locked(pathname, &#x26;path);
<br>&#x9;if (IS_ERR(dentry))
<br>&#x9;&#x9;return (void)dentry;/ returning an error /*
<br>&#x9;inode = path.dentry-&#x3E;d_inode;
<br>&#x9;inode_unlock(inode);
<br>
<br>&#x9;audit_mark = kzalloc(sizeof(*audit_mark), GFP_KERNEL);
<br>&#x9;if (unlikely(!audit_mark)) {
<br>&#x9;&#x9;audit_mark = ERR_PTR(-ENOMEM);
<br>&#x9;&#x9;goto out;
<br>&#x9;}
<br>
<br>&#x9;fsnotify_init_mark(&#x26;audit_mark-&#x3E;mark, audit_fsnotify_free_mark);
<br>&#x9;audit_mark-&#x3E;mark.mask = AUDIT_FS_EVENTS;
<br>&#x9;audit_mark-&#x3E;path = pathname;
<br>&#x9;audit_update_mark(audit_mark, dentry-&#x3E;d_inode);
<br>&#x9;audit_mark-&#x3E;rule = krule;
<br>
<br>&#x9;ret = fsnotify_add_mark(&#x26;audit_mark-&#x3E;mark, audit_fsnotify_group, inode, NULL, true);
<br>&#x9;if (ret &#x3C; 0) {
<br>&#x9;&#x9;audit_fsnotify_mark_free(audit_mark);
<br>&#x9;&#x9;audit_mark = ERR_PTR(ret);
<br>&#x9;}
<br>out:
<br>&#x9;dput(dentry);
<br>&#x9;path_put(&#x26;path);
<br>&#x9;return audit_mark;
<br>}
<br>
<br>static void audit_mark_log_rule_change(struct audit_fsnotify_markaudit_mark, charop)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;struct audit_krulerule = audit_mark-&#x3E;rule;
<br>
<br>&#x9;if (!audit_enabled)
<br>&#x9;&#x9;return;
<br>&#x9;ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
<br>&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_format(ab, &#x22;auid=%u ses=%u op=&#x22;,
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, audit_get_loginuid(current)),
<br>&#x9;&#x9;&#x9; audit_get_sessionid(current));
<br>&#x9;audit_log_string(ab, op);
<br>&#x9;audit_log_format(ab, &#x22; path=&#x22;);
<br>&#x9;audit_log_untrustedstring(ab, audit_mark-&#x3E;path);
<br>&#x9;audit_log_key(ab, rule-&#x3E;filterkey);
<br>&#x9;audit_log_format(ab, &#x22; list=%d res=1&#x22;, rule-&#x3E;listnr);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>void audit_remove_mark(struct audit_fsnotify_markaudit_mark)
<br>{
<br>&#x9;fsnotify_destroy_mark(&#x26;audit_mark-&#x3E;mark, audit_fsnotify_group);
<br>&#x9;fsnotify_put_mark(&#x26;audit_mark-&#x3E;mark);
<br>}
<br>
<br>void audit_remove_mark_rule(struct audit_krulekrule)
<br>{
<br>&#x9;struct audit_fsnotify_markmark = krule-&#x3E;exe;
<br>
<br>&#x9;audit_remove_mark(mark);
<br>}
<br>
<br>static void audit_autoremove_mark_rule(struct audit_fsnotify_markaudit_mark)
<br>{
<br>&#x9;struct audit_krulerule = audit_mark-&#x3E;rule;
<br>&#x9;struct audit_entryentry = container_of(rule, struct audit_entry, rule);
<br>
<br>&#x9;audit_mark_log_rule_change(audit_mark, &#x22;autoremove_rule&#x22;);
<br>&#x9;audit_del_rule(entry);
<br>}
<br>
<br>*/ Update mark data in audit rules based on fsnotify events. /*
<br>static int audit_mark_handle_event(struct fsnotify_groupgroup,
<br>&#x9;&#x9;&#x9;&#x9;    struct inodeto_tell,
<br>&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markinode_mark,
<br>&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markvfsmount_mark,
<br>&#x9;&#x9;&#x9;&#x9;    u32 mask, voiddata, int data_type,
<br>&#x9;&#x9;&#x9;&#x9;    const unsigned chardname, u32 cookie)
<br>{
<br>&#x9;struct audit_fsnotify_markaudit_mark;
<br>&#x9;struct inodeinode = NULL;
<br>
<br>&#x9;audit_mark = container_of(inode_mark, struct audit_fsnotify_mark, mark);
<br>
<br>&#x9;BUG_ON(group != audit_fsnotify_group);
<br>
<br>&#x9;switch (data_type) {
<br>&#x9;case (FSNOTIFY_EVENT_PATH):
<br>&#x9;&#x9;inode = ((struct path)data)-&#x3E;dentry-&#x3E;d_inode;
<br>&#x9;&#x9;break;
<br>&#x9;case (FSNOTIFY_EVENT_INODE):
<br>&#x9;&#x9;inode = (struct inode)data;
<br>&#x9;&#x9;break;
<br>&#x9;default:
<br>&#x9;&#x9;BUG();
<br>&#x9;&#x9;return 0;
<br>&#x9;};
<br>
<br>&#x9;if (mask &#x26; (FS_CREATE|FS_MOVED_TO|FS_DELETE|FS_MOVED_FROM)) {
<br>&#x9;&#x9;if (audit_compare_dname_path(dname, audit_mark-&#x3E;path, AUDIT_NAME_FULL))
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;&#x9;audit_update_mark(audit_mark, inode);
<br>&#x9;} else if (mask &#x26; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
<br>&#x9;&#x9;audit_autoremove_mark_rule(audit_mark);
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>static const struct fsnotify_ops audit_mark_fsnotify_ops = {
<br>&#x9;.handle_event =&#x9;audit_mark_handle_event,
<br>};
<br>
<br>static int __init audit_fsnotify_init(void)
<br>{
<br>&#x9;audit_fsnotify_group = fsnotify_alloc_group(&#x26;audit_mark_fsnotify_ops);
<br>&#x9;if (IS_ERR(audit_fsnotify_group)) {
<br>&#x9;&#x9;audit_fsnotify_group = NULL;
<br>&#x9;&#x9;audit_panic(&#x22;cannot create audit fsnotify group&#x22;);
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>device_initcall(audit_fsnotify_init);
<br>*/
<br> audit -- definition of audit_context structure and supporting types 
<br>
<br> Copyright 2003-2004 Red Hat, Inc.
<br> Copyright 2005 Hewlett-Packard Development Company, L.P.
<br> Copyright 2005 IBM Corporation
<br>
<br> This program is free software; you can redistribute it and/or modify
<br> it under the terms of the GNU General Public License as published by
<br> the Free Software Foundation; either version 2 of the License, or
<br> (at your option) any later version.
<br>
<br> This program is distributed in the hope that it will be useful,
<br> but WITHOUT ANY WARRANTY; without even the implied warranty of
<br> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
<br> GNU General Public License for more details.
<br>
<br> You should have received a copy of the GNU General Public License
<br> along with this program; if not, write to the Free Software
<br> Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
<br> /*
<br>
<br>#include &#x3C;linux/fs.h&#x3E;
<br>#include &#x3C;linux/audit.h&#x3E;
<br>#include &#x3C;linux/skbuff.h&#x3E;
<br>#include &#x3C;uapi/linux/mqueue.h&#x3E;
<br>
<br>*/ AUDIT_NAMES is the number of slots we reserve in the audit_context
<br> for saving names from getname().  If we get more names we will allocate
<br> a name dynamically and also add those to the list anchored by names_list. /*
<br>#define AUDIT_NAMES&#x9;5
<br>
<br>*/ At task start time, the audit_state is set in the audit_context using
<br>   a per-task filter.  At syscall entry, the audit_state is augmented by
<br>   the syscall filter. /*
<br>enum audit_state {
<br>&#x9;AUDIT_DISABLED,&#x9;&#x9;*/ Do not create per-task audit_context.
<br>&#x9;&#x9;&#x9;&#x9; No syscall-specific audit records can
<br>&#x9;&#x9;&#x9;&#x9; be generated. /*
<br>&#x9;AUDIT_BUILD_CONTEXT,&#x9;*/ Create the per-task audit_context,
<br>&#x9;&#x9;&#x9;&#x9; and fill it in at syscall
<br>&#x9;&#x9;&#x9;&#x9; entry time.  This makes a full
<br>&#x9;&#x9;&#x9;&#x9; syscall record available if some
<br>&#x9;&#x9;&#x9;&#x9; other part of the kernel decides it
<br>&#x9;&#x9;&#x9;&#x9; should be recorded. /*
<br>&#x9;AUDIT_RECORD_CONTEXT&#x9;*/ Create the per-task audit_context,
<br>&#x9;&#x9;&#x9;&#x9; always fill it in at syscall entry
<br>&#x9;&#x9;&#x9;&#x9; time, and always write out the audit
<br>&#x9;&#x9;&#x9;&#x9; record at syscall exit time.  /*
<br>};
<br>
<br>*/ Rule lists /*
<br>struct audit_watch;
<br>struct audit_fsnotify_mark;
<br>struct audit_tree;
<br>struct audit_chunk;
<br>
<br>struct audit_entry {
<br>&#x9;struct list_head&#x9;list;
<br>&#x9;struct rcu_head&#x9;&#x9;rcu;
<br>&#x9;struct audit_krule&#x9;rule;
<br>};
<br>
<br>struct audit_cap_data {
<br>&#x9;kernel_cap_t&#x9;&#x9;permitted;
<br>&#x9;kernel_cap_t&#x9;&#x9;inheritable;
<br>&#x9;union {
<br>&#x9;&#x9;unsigned int&#x9;fE;&#x9;&#x9;*/ effective bit of file cap /*
<br>&#x9;&#x9;kernel_cap_t&#x9;effective;&#x9;*/ effective set of process /*
<br>&#x9;};
<br>};
<br>
<br>*/ When fs/namei.c:getname() is called, we store the pointer in name and bump
<br> the refcnt in the associated filename struct.
<br>
<br> Further, in fs/namei.c:path_lookup() we store the inode and device.
<br> /*
<br>struct audit_names {
<br>&#x9;struct list_head&#x9;list;&#x9;&#x9;*/ audit_context-&#x3E;names_list /*
<br>
<br>&#x9;struct filename&#x9;&#x9;*name;
<br>&#x9;int&#x9;&#x9;&#x9;name_len;&#x9;*/ number of chars to log /*
<br>&#x9;bool&#x9;&#x9;&#x9;hidden;&#x9;&#x9;*/ don&#x27;t log this record /*
<br>
<br>&#x9;unsigned long&#x9;&#x9;ino;
<br>&#x9;dev_t&#x9;&#x9;&#x9;dev;
<br>&#x9;umode_t&#x9;&#x9;&#x9;mode;
<br>&#x9;kuid_t&#x9;&#x9;&#x9;uid;
<br>&#x9;kgid_t&#x9;&#x9;&#x9;gid;
<br>&#x9;dev_t&#x9;&#x9;&#x9;rdev;
<br>&#x9;u32&#x9;&#x9;&#x9;osid;
<br>&#x9;struct audit_cap_data&#x9;fcap;
<br>&#x9;unsigned int&#x9;&#x9;fcap_ver;
<br>&#x9;unsigned char&#x9;&#x9;type;&#x9;&#x9;*/ record type /*
<br>&#x9;*/
<br>&#x9; This was an allocated audit_names and not from the array of
<br>&#x9; names allocated in the task audit context.  Thus this name
<br>&#x9; should be freed on syscall exit.
<br>&#x9; /*
<br>&#x9;bool&#x9;&#x9;&#x9;should_free;
<br>};
<br>
<br>struct audit_proctitle {
<br>&#x9;int&#x9;len;&#x9;*/ length of the cmdline field. /*
<br>&#x9;char&#x9;*value;&#x9;*/ the cmdline field /*
<br>};
<br>
<br>*/ The per-task audit context. /*
<br>struct audit_context {
<br>&#x9;int&#x9;&#x9;    dummy;&#x9;*/ must be the first element /*
<br>&#x9;int&#x9;&#x9;    in_syscall;&#x9;*/ 1 if task is in a syscall /*
<br>&#x9;enum audit_state    state, current_state;
<br>&#x9;unsigned int&#x9;    serial;    / serial number for record /*
<br>&#x9;int&#x9;&#x9;    major;     / syscall number /*
<br>&#x9;struct timespec&#x9;    ctime;     / time of syscall entry /*
<br>&#x9;unsigned long&#x9;    argv[4];   / syscall arguments /*
<br>&#x9;long&#x9;&#x9;    return_code;*/ syscall return code /*
<br>&#x9;u64&#x9;&#x9;    prio;
<br>&#x9;int&#x9;&#x9;    return_valid;/ return code is valid /*
<br>&#x9;*/
<br>&#x9; The names_list is the list of all audit_names collected during this
<br>&#x9; syscall.  The first AUDIT_NAMES entries in the names_list will
<br>&#x9; actually be from the preallocated_names array for performance
<br>&#x9; reasons.  Except during allocation they should never be referenced
<br>&#x9; through the preallocated_names array and should only be found/used
<br>&#x9; by running the names_list.
<br>&#x9; /*
<br>&#x9;struct audit_names  preallocated_names[AUDIT_NAMES];
<br>&#x9;int&#x9;&#x9;    name_count;/ total records in names_list /*
<br>&#x9;struct list_head    names_list;&#x9;*/ struct audit_names-&#x3E;list anchor /*
<br>&#x9;char&#x9;&#x9;   filterkey;&#x9;*/ key for rule that triggered record /*
<br>&#x9;struct path&#x9;    pwd;
<br>&#x9;struct audit_aux_dataaux;
<br>&#x9;struct audit_aux_dataaux_pids;
<br>&#x9;struct sockaddr_storagesockaddr;
<br>&#x9;size_t sockaddr_len;
<br>&#x9;&#x9;&#x9;&#x9;*/ Save things to print about task_struct /*
<br>&#x9;pid_t&#x9;&#x9;    pid, ppid;
<br>&#x9;kuid_t&#x9;&#x9;    uid, euid, suid, fsuid;
<br>&#x9;kgid_t&#x9;&#x9;    gid, egid, sgid, fsgid;
<br>&#x9;unsigned long&#x9;    personality;
<br>&#x9;int&#x9;&#x9;    arch;
<br>
<br>&#x9;pid_t&#x9;&#x9;    target_pid;
<br>&#x9;kuid_t&#x9;&#x9;    target_auid;
<br>&#x9;kuid_t&#x9;&#x9;    target_uid;
<br>&#x9;unsigned int&#x9;    target_sessionid;
<br>&#x9;u32&#x9;&#x9;    target_sid;
<br>&#x9;char&#x9;&#x9;    target_comm[TASK_COMM_LEN];
<br>
<br>&#x9;struct audit_tree_refstrees,first_trees;
<br>&#x9;struct list_head killed_trees;
<br>&#x9;int tree_count;
<br>
<br>&#x9;int type;
<br>&#x9;union {
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;int nargs;
<br>&#x9;&#x9;&#x9;long args[6];
<br>&#x9;&#x9;} socketcall;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;kuid_t&#x9;&#x9;&#x9;uid;
<br>&#x9;&#x9;&#x9;kgid_t&#x9;&#x9;&#x9;gid;
<br>&#x9;&#x9;&#x9;umode_t&#x9;&#x9;&#x9;mode;
<br>&#x9;&#x9;&#x9;u32&#x9;&#x9;&#x9;osid;
<br>&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;has_perm;
<br>&#x9;&#x9;&#x9;uid_t&#x9;&#x9;&#x9;perm_uid;
<br>&#x9;&#x9;&#x9;gid_t&#x9;&#x9;&#x9;perm_gid;
<br>&#x9;&#x9;&#x9;umode_t&#x9;&#x9;&#x9;perm_mode;
<br>&#x9;&#x9;&#x9;unsigned long&#x9;&#x9;qbytes;
<br>&#x9;&#x9;} ipc;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;mqd_t&#x9;&#x9;&#x9;mqdes;
<br>&#x9;&#x9;&#x9;struct mq_attr&#x9;&#x9;mqstat;
<br>&#x9;&#x9;} mq_getsetattr;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;mqd_t&#x9;&#x9;&#x9;mqdes;
<br>&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;sigev_signo;
<br>&#x9;&#x9;} mq_notify;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;mqd_t&#x9;&#x9;&#x9;mqdes;
<br>&#x9;&#x9;&#x9;size_t&#x9;&#x9;&#x9;msg_len;
<br>&#x9;&#x9;&#x9;unsigned int&#x9;&#x9;msg_prio;
<br>&#x9;&#x9;&#x9;struct timespec&#x9;&#x9;abs_timeout;
<br>&#x9;&#x9;} mq_sendrecv;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;oflag;
<br>&#x9;&#x9;&#x9;umode_t&#x9;&#x9;&#x9;mode;
<br>&#x9;&#x9;&#x9;struct mq_attr&#x9;&#x9;attr;
<br>&#x9;&#x9;} mq_open;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;pid_t&#x9;&#x9;&#x9;pid;
<br>&#x9;&#x9;&#x9;struct audit_cap_data&#x9;cap;
<br>&#x9;&#x9;} capset;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;fd;
<br>&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;flags;
<br>&#x9;&#x9;} mmap;
<br>&#x9;&#x9;struct {
<br>&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;argc;
<br>&#x9;&#x9;} execve;
<br>&#x9;};
<br>&#x9;int fds[2];
<br>&#x9;struct audit_proctitle proctitle;
<br>};
<br>
<br>extern u32 audit_ever_enabled;
<br>
<br>extern void audit_copy_inode(struct audit_namesname,
<br>&#x9;&#x9;&#x9;     const struct dentrydentry,
<br>&#x9;&#x9;&#x9;     struct inodeinode);
<br>extern void audit_log_cap(struct audit_bufferab, charprefix,
<br>&#x9;&#x9;&#x9;  kernel_cap_tcap);
<br>extern void audit_log_name(struct audit_contextcontext,
<br>&#x9;&#x9;&#x9;   struct audit_namesn, struct pathpath,
<br>&#x9;&#x9;&#x9;   int record_num, intcall_panic);
<br>
<br>extern int audit_pid;
<br>
<br>#define AUDIT_INODE_BUCKETS&#x9;32
<br>extern struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];
<br>
<br>static inline int audit_hash_ino(u32 ino)
<br>{
<br>&#x9;return (ino &#x26; (AUDIT_INODE_BUCKETS-1));
<br>}
<br>
<br>*/ Indicates that audit should log the full pathname. /*
<br>#define AUDIT_NAME_FULL -1
<br>
<br>extern int audit_match_class(int class, unsigned syscall);
<br>extern int audit_comparator(const u32 left, const u32 op, const u32 right);
<br>extern int audit_uid_comparator(kuid_t left, u32 op, kuid_t right);
<br>extern int audit_gid_comparator(kgid_t left, u32 op, kgid_t right);
<br>extern int parent_len(const charpath);
<br>extern int audit_compare_dname_path(const chardname, const charpath, int plen);
<br>extern struct sk_buffaudit_make_reply(__u32 portid, int seq, int type,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;int done, int multi,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;const voidpayload, int size);
<br>extern void&#x9;&#x9;    audit_panic(const charmessage);
<br>
<br>struct audit_netlink_list {
<br>&#x9;__u32 portid;
<br>&#x9;struct netnet;
<br>&#x9;struct sk_buff_head q;
<br>};
<br>
<br>int audit_send_list(void);
<br>
<br>struct audit_net {
<br>&#x9;struct socknlsk;
<br>};
<br>
<br>extern int selinux_audit_rule_update(void);
<br>
<br>extern struct mutex audit_filter_mutex;
<br>extern int audit_del_rule(struct audit_entry);
<br>extern void audit_free_rule_rcu(struct rcu_head);
<br>extern struct list_head audit_filter_list[];
<br>
<br>extern struct audit_entryaudit_dupe_rule(struct audit_kruleold);
<br>
<br>extern void audit_log_d_path_exe(struct audit_bufferab,
<br>&#x9;&#x9;&#x9;&#x9; struct mm_structmm);
<br>
<br>*/ audit watch functions /*
<br>#ifdef CONFIG_AUDIT_WATCH
<br>extern void audit_put_watch(struct audit_watchwatch);
<br>extern void audit_get_watch(struct audit_watchwatch);
<br>extern int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op);
<br>extern int audit_add_watch(struct audit_krulekrule, struct list_head*list);
<br>extern void audit_remove_watch_rule(struct audit_krulekrule);
<br>extern charaudit_watch_path(struct audit_watchwatch);
<br>extern int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev);
<br>
<br>extern struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len);
<br>extern charaudit_mark_path(struct audit_fsnotify_markmark);
<br>extern void audit_remove_mark(struct audit_fsnotify_markaudit_mark);
<br>extern void audit_remove_mark_rule(struct audit_krulekrule);
<br>extern int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev);
<br>extern int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold);
<br>extern int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark);
<br>
<br>#else
<br>#define audit_put_watch(w) {}
<br>#define audit_get_watch(w) {}
<br>#define audit_to_watch(k, p, l, o) (-EINVAL)
<br>#define audit_add_watch(k, l) (-EINVAL)
<br>#define audit_remove_watch_rule(k) BUG()
<br>#define audit_watch_path(w) &#x22;&#x22;
<br>#define audit_watch_compare(w, i, d) 0
<br>
<br>#define audit_alloc_mark(k, p, l) (ERR_PTR(-EINVAL))
<br>#define audit_mark_path(m) &#x22;&#x22;
<br>#define audit_remove_mark(m)
<br>#define audit_remove_mark_rule(k)
<br>#define audit_mark_compare(m, i, d) 0
<br>#define audit_exe_compare(t, m) (-EINVAL)
<br>#define audit_dupe_exe(n, o) (-EINVAL)
<br>#endif */ CONFIG_AUDIT_WATCH /*
<br>
<br>#ifdef CONFIG_AUDIT_TREE
<br>extern struct audit_chunkaudit_tree_lookup(const struct inode);
<br>extern void audit_put_chunk(struct audit_chunk);
<br>extern bool audit_tree_match(struct audit_chunk, struct audit_tree);
<br>extern int audit_make_tree(struct audit_krule, char, u32);
<br>extern int audit_add_tree_rule(struct audit_krule);
<br>extern int audit_remove_tree_rule(struct audit_krule);
<br>extern void audit_trim_trees(void);
<br>extern int audit_tag_tree(charold, charnew);
<br>extern const charaudit_tree_path(struct audit_tree);
<br>extern void audit_put_tree(struct audit_tree);
<br>extern void audit_kill_trees(struct list_head);
<br>#else
<br>#define audit_remove_tree_rule(rule) BUG()
<br>#define audit_add_tree_rule(rule) -EINVAL
<br>#define audit_make_tree(rule, str, op) -EINVAL
<br>#define audit_trim_trees() (void)0
<br>#define audit_put_tree(tree) (void)0
<br>#define audit_tag_tree(old, new) -EINVAL
<br>#define audit_tree_path(rule) &#x22;&#x22;&#x9;*/ never called /*
<br>#define audit_kill_trees(list) BUG()
<br>#endif
<br>
<br>extern charaudit_unpack_string(void*, size_t, size_t);
<br>
<br>extern pid_t audit_sig_pid;
<br>extern kuid_t audit_sig_uid;
<br>extern u32 audit_sig_sid;
<br>
<br>#ifdef CONFIG_AUDITSYSCALL
<br>extern int __audit_signal_info(int sig, struct task_structt);
<br>static inline int audit_signal_info(int sig, struct task_structt)
<br>{
<br>&#x9;if (unlikely((audit_pid &#x26;&#x26; t-&#x3E;tgid == audit_pid) ||
<br>&#x9;&#x9;     (audit_signals &#x26;&#x26; !audit_dummy_context())))
<br>&#x9;&#x9;return __audit_signal_info(sig, t);
<br>&#x9;return 0;
<br>}
<br>extern void audit_filter_inodes(struct task_struct, struct audit_context);
<br>extern struct list_headaudit_killed_trees(void);
<br>#else
<br>#define audit_signal_info(s,t) AUDIT_DISABLED
<br>#define audit_filter_inodes(t,c) AUDIT_DISABLED
<br>#endif
<br>
<br>extern struct mutex audit_cmd_mutex;
<br>*/
<br> auditsc.c -- System-call auditing support
<br> Handles all system-call specific auditing features.
<br>
<br> Copyright 2003-2004 Red Hat Inc., Durham, North Carolina.
<br> Copyright 2005 Hewlett-Packard Development Company, L.P.
<br> Copyright (C) 2005, 2006 IBM Corporation
<br> All Rights Reserved.
<br>
<br> This program is free software; you can redistribute it and/or modify
<br> it under the terms of the GNU General Public License as published by
<br> the Free Software Foundation; either version 2 of the License, or
<br> (at your option) any later version.
<br>
<br> This program is distributed in the hope that it will be useful,
<br> but WITHOUT ANY WARRANTY; without even the implied warranty of
<br> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
<br> GNU General Public License for more details.
<br>
<br> You should have received a copy of the GNU General Public License
<br> along with this program; if not, write to the Free Software
<br> Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
<br>
<br> Written by Rickard E. (Rik) Faith &#x3C;faith@redhat.com&#x3E;
<br>
<br> Many of the ideas implemented here are from Stephen C. Tweedie,
<br> especially the idea of avoiding a copy by using getname.
<br>
<br> The method for actual interception of syscall entry and exit (not in
<br> this file -- see entry.S) is based on a GPL&#x27;d patch written by
<br> okir@suse.de and Copyright 2003 SuSE Linux AG.
<br>
<br> POSIX message queue support added by George Wilson &#x3C;ltcgcw@us.ibm.com&#x3E;,
<br> 2006.
<br>
<br> The support of additional filter rules compares (&#x3E;, &#x3C;, &#x3E;=, &#x3C;=) was
<br> added by Dustin Kirkland &#x3C;dustin.kirkland@us.ibm.com&#x3E;, 2005.
<br>
<br> Modified by Amy Griffis &#x3C;amy.griffis@hp.com&#x3E; to collect additional
<br> filesystem information.
<br>
<br> Subject and object context labeling support added by &#x3C;danjones@us.ibm.com&#x3E;
<br> and &#x3C;dustin.kirkland@us.ibm.com&#x3E; for LSPP certification compliance.
<br> /*
<br>
<br>#define pr_fmt(fmt) KBUILD_MODNAME &#x22;: &#x22; fmt
<br>
<br>#include &#x3C;linux/init.h&#x3E;
<br>#include &#x3C;asm/types.h&#x3E;
<br>#include &#x3C;linux/atomic.h&#x3E;
<br>#include &#x3C;linux/fs.h&#x3E;
<br>#include &#x3C;linux/namei.h&#x3E;
<br>#include &#x3C;linux/mm.h&#x3E;
<br>#include &#x3C;linux/export.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/mount.h&#x3E;
<br>#include &#x3C;linux/socket.h&#x3E;
<br>#include &#x3C;linux/mqueue.h&#x3E;
<br>#include &#x3C;linux/audit.h&#x3E;
<br>#include &#x3C;linux/personality.h&#x3E;
<br>#include &#x3C;linux/time.h&#x3E;
<br>#include &#x3C;linux/netlink.h&#x3E;
<br>#include &#x3C;linux/compiler.h&#x3E;
<br>#include &#x3C;asm/unistd.h&#x3E;
<br>#include &#x3C;linux/security.h&#x3E;
<br>#include &#x3C;linux/list.h&#x3E;
<br>#include &#x3C;linux/tty.h&#x3E;
<br>#include &#x3C;linux/binfmts.h&#x3E;
<br>#include &#x3C;linux/highmem.h&#x3E;
<br>#include &#x3C;linux/syscalls.h&#x3E;
<br>#include &#x3C;asm/syscall.h&#x3E;
<br>#include &#x3C;linux/capability.h&#x3E;
<br>#include &#x3C;linux/fs_struct.h&#x3E;
<br>#include &#x3C;linux/compat.h&#x3E;
<br>#include &#x3C;linux/ctype.h&#x3E;
<br>#include &#x3C;linux/string.h&#x3E;
<br>#include &#x3C;uapi/linux/limits.h&#x3E;
<br>
<br>#include &#x22;audit.h&#x22;
<br>
<br>*/ flags stating the success for a syscall /*
<br>#define AUDITSC_INVALID 0
<br>#define AUDITSC_SUCCESS 1
<br>#define AUDITSC_FAILURE 2
<br>
<br>*/ no execve audit message should be longer than this (userspace limits) /*
<br>#define MAX_EXECVE_AUDIT_LEN 7500
<br>
<br>*/ max length to print of cmdline/proctitle value during audit /*
<br>#define MAX_PROCTITLE_AUDIT_LEN 128
<br>
<br>*/ number of audit rules /*
<br>int audit_n_rules;
<br>
<br>*/ determines whether we collect data for signals sent /*
<br>int audit_signals;
<br>
<br>struct audit_aux_data {
<br>&#x9;struct audit_aux_data&#x9;*next;
<br>&#x9;int&#x9;&#x9;&#x9;type;
<br>};
<br>
<br>#define AUDIT_AUX_IPCPERM&#x9;0
<br>
<br>*/ Number of target pids per aux struct. /*
<br>#define AUDIT_AUX_PIDS&#x9;16
<br>
<br>struct audit_aux_data_pids {
<br>&#x9;struct audit_aux_data&#x9;d;
<br>&#x9;pid_t&#x9;&#x9;&#x9;target_pid[AUDIT_AUX_PIDS];
<br>&#x9;kuid_t&#x9;&#x9;&#x9;target_auid[AUDIT_AUX_PIDS];
<br>&#x9;kuid_t&#x9;&#x9;&#x9;target_uid[AUDIT_AUX_PIDS];
<br>&#x9;unsigned int&#x9;&#x9;target_sessionid[AUDIT_AUX_PIDS];
<br>&#x9;u32&#x9;&#x9;&#x9;target_sid[AUDIT_AUX_PIDS];
<br>&#x9;char &#x9;&#x9;&#x9;target_comm[AUDIT_AUX_PIDS][TASK_COMM_LEN];
<br>&#x9;int&#x9;&#x9;&#x9;pid_count;
<br>};
<br>
<br>struct audit_aux_data_bprm_fcaps {
<br>&#x9;struct audit_aux_data&#x9;d;
<br>&#x9;struct audit_cap_data&#x9;fcap;
<br>&#x9;unsigned int&#x9;&#x9;fcap_ver;
<br>&#x9;struct audit_cap_data&#x9;old_pcap;
<br>&#x9;struct audit_cap_data&#x9;new_pcap;
<br>};
<br>
<br>struct audit_tree_refs {
<br>&#x9;struct audit_tree_refsnext;
<br>&#x9;struct audit_chunkc[31];
<br>};
<br>
<br>static int audit_match_perm(struct audit_contextctx, int mask)
<br>{
<br>&#x9;unsigned n;
<br>&#x9;if (unlikely(!ctx))
<br>&#x9;&#x9;return 0;
<br>&#x9;n = ctx-&#x3E;major;
<br>
<br>&#x9;switch (audit_classify_syscall(ctx-&#x3E;arch, n)) {
<br>&#x9;case 0:&#x9;*/ native /*
<br>&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_WRITE) &#x26;&#x26;
<br>&#x9;&#x9;     audit_match_class(AUDIT_CLASS_WRITE, n))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_READ) &#x26;&#x26;
<br>&#x9;&#x9;     audit_match_class(AUDIT_CLASS_READ, n))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_ATTR) &#x26;&#x26;
<br>&#x9;&#x9;     audit_match_class(AUDIT_CLASS_CHATTR, n))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;return 0;
<br>&#x9;case 1:/ 32bit on biarch /*
<br>&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_WRITE) &#x26;&#x26;
<br>&#x9;&#x9;     audit_match_class(AUDIT_CLASS_WRITE_32, n))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_READ) &#x26;&#x26;
<br>&#x9;&#x9;     audit_match_class(AUDIT_CLASS_READ_32, n))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_ATTR) &#x26;&#x26;
<br>&#x9;&#x9;     audit_match_class(AUDIT_CLASS_CHATTR_32, n))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;return 0;
<br>&#x9;case 2:/ open /*
<br>&#x9;&#x9;return mask &#x26; ACC_MODE(ctx-&#x3E;argv[1]);
<br>&#x9;case 3:/ openat /*
<br>&#x9;&#x9;return mask &#x26; ACC_MODE(ctx-&#x3E;argv[2]);
<br>&#x9;case 4:/ socketcall /*
<br>&#x9;&#x9;return ((mask &#x26; AUDIT_PERM_WRITE) &#x26;&#x26; ctx-&#x3E;argv[0] == SYS_BIND);
<br>&#x9;case 5:/ execve /*
<br>&#x9;&#x9;return mask &#x26; AUDIT_PERM_EXEC;
<br>&#x9;default:
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>}
<br>
<br>static int audit_match_filetype(struct audit_contextctx, int val)
<br>{
<br>&#x9;struct audit_namesn;
<br>&#x9;umode_t mode = (umode_t)val;
<br>
<br>&#x9;if (unlikely(!ctx))
<br>&#x9;&#x9;return 0;
<br>
<br>&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;if ((n-&#x3E;ino != AUDIT_INO_UNSET) &#x26;&#x26;
<br>&#x9;&#x9;    ((n-&#x3E;mode &#x26; S_IFMT) == mode))
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;}
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>*/
<br> We keep a linked list of fixed-sized (31 pointer) arrays of audit_chunk;
<br> -&#x3E;first_trees points to its beginning, -&#x3E;trees - to the current end of data.
<br> -&#x3E;tree_count is the number of free entries in array pointed to by -&#x3E;trees.
<br> Original condition is (NULL, NULL, 0); as soon as it grows we never revert to NULL,
<br> &#x22;empty&#x22; becomes (p, p, 31) afterwards.  We don&#x27;t shrink the list (and seriously,
<br> it&#x27;s going to remain 1-element for almost any setup) until we free context itself.
<br> References in it _are_ dropped - at the same time we free/drop aux stuff.
<br> /*
<br>
<br>#ifdef CONFIG_AUDIT_TREE
<br>static void audit_set_auditable(struct audit_contextctx)
<br>{
<br>&#x9;if (!ctx-&#x3E;prio) {
<br>&#x9;&#x9;ctx-&#x3E;prio = 1;
<br>&#x9;&#x9;ctx-&#x3E;current_state = AUDIT_RECORD_CONTEXT;
<br>&#x9;}
<br>}
<br>
<br>static int put_tree_ref(struct audit_contextctx, struct audit_chunkchunk)
<br>{
<br>&#x9;struct audit_tree_refsp = ctx-&#x3E;trees;
<br>&#x9;int left = ctx-&#x3E;tree_count;
<br>&#x9;if (likely(left)) {
<br>&#x9;&#x9;p-&#x3E;c[--left] = chunk;
<br>&#x9;&#x9;ctx-&#x3E;tree_count = left;
<br>&#x9;&#x9;return 1;
<br>&#x9;}
<br>&#x9;if (!p)
<br>&#x9;&#x9;return 0;
<br>&#x9;p = p-&#x3E;next;
<br>&#x9;if (p) {
<br>&#x9;&#x9;p-&#x3E;c[30] = chunk;
<br>&#x9;&#x9;ctx-&#x3E;trees = p;
<br>&#x9;&#x9;ctx-&#x3E;tree_count = 30;
<br>&#x9;&#x9;return 1;
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>static int grow_tree_refs(struct audit_contextctx)
<br>{
<br>&#x9;struct audit_tree_refsp = ctx-&#x3E;trees;
<br>&#x9;ctx-&#x3E;trees = kzalloc(sizeof(struct audit_tree_refs), GFP_KERNEL);
<br>&#x9;if (!ctx-&#x3E;trees) {
<br>&#x9;&#x9;ctx-&#x3E;trees = p;
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>&#x9;if (p)
<br>&#x9;&#x9;p-&#x3E;next = ctx-&#x3E;trees;
<br>&#x9;else
<br>&#x9;&#x9;ctx-&#x3E;first_trees = ctx-&#x3E;trees;
<br>&#x9;ctx-&#x3E;tree_count = 31;
<br>&#x9;return 1;
<br>}
<br>#endif
<br>
<br>static void unroll_tree_refs(struct audit_contextctx,
<br>&#x9;&#x9;      struct audit_tree_refsp, int count)
<br>{
<br>#ifdef CONFIG_AUDIT_TREE
<br>&#x9;struct audit_tree_refsq;
<br>&#x9;int n;
<br>&#x9;if (!p) {
<br>&#x9;&#x9;*/ we started with empty chain /*
<br>&#x9;&#x9;p = ctx-&#x3E;first_trees;
<br>&#x9;&#x9;count = 31;
<br>&#x9;&#x9;*/ if the very first allocation has failed, nothing to do /*
<br>&#x9;&#x9;if (!p)
<br>&#x9;&#x9;&#x9;return;
<br>&#x9;}
<br>&#x9;n = count;
<br>&#x9;for (q = p; q != ctx-&#x3E;trees; q = q-&#x3E;next, n = 31) {
<br>&#x9;&#x9;while (n--) {
<br>&#x9;&#x9;&#x9;audit_put_chunk(q-&#x3E;c[n]);
<br>&#x9;&#x9;&#x9;q-&#x3E;c[n] = NULL;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;while (n-- &#x3E; ctx-&#x3E;tree_count) {
<br>&#x9;&#x9;audit_put_chunk(q-&#x3E;c[n]);
<br>&#x9;&#x9;q-&#x3E;c[n] = NULL;
<br>&#x9;}
<br>&#x9;ctx-&#x3E;trees = p;
<br>&#x9;ctx-&#x3E;tree_count = count;
<br>#endif
<br>}
<br>
<br>static void free_tree_refs(struct audit_contextctx)
<br>{
<br>&#x9;struct audit_tree_refsp,q;
<br>&#x9;for (p = ctx-&#x3E;first_trees; p; p = q) {
<br>&#x9;&#x9;q = p-&#x3E;next;
<br>&#x9;&#x9;kfree(p);
<br>&#x9;}
<br>}
<br>
<br>static int match_tree_refs(struct audit_contextctx, struct audit_treetree)
<br>{
<br>#ifdef CONFIG_AUDIT_TREE
<br>&#x9;struct audit_tree_refsp;
<br>&#x9;int n;
<br>&#x9;if (!tree)
<br>&#x9;&#x9;return 0;
<br>&#x9;*/ full ones /*
<br>&#x9;for (p = ctx-&#x3E;first_trees; p != ctx-&#x3E;trees; p = p-&#x3E;next) {
<br>&#x9;&#x9;for (n = 0; n &#x3C; 31; n++)
<br>&#x9;&#x9;&#x9;if (audit_tree_match(p-&#x3E;c[n], tree))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;}
<br>&#x9;*/ partial /*
<br>&#x9;if (p) {
<br>&#x9;&#x9;for (n = ctx-&#x3E;tree_count; n &#x3C; 31; n++)
<br>&#x9;&#x9;&#x9;if (audit_tree_match(p-&#x3E;c[n], tree))
<br>&#x9;&#x9;&#x9;&#x9;return 1;
<br>&#x9;}
<br>#endif
<br>&#x9;return 0;
<br>}
<br>
<br>static int audit_compare_uid(kuid_t uid,
<br>&#x9;&#x9;&#x9;     struct audit_namesname,
<br>&#x9;&#x9;&#x9;     struct audit_fieldf,
<br>&#x9;&#x9;&#x9;     struct audit_contextctx)
<br>{
<br>&#x9;struct audit_namesn;
<br>&#x9;int rc;
<br> 
<br>&#x9;if (name) {
<br>&#x9;&#x9;rc = audit_uid_comparator(uid, f-&#x3E;op, name-&#x3E;uid);
<br>&#x9;&#x9;if (rc)
<br>&#x9;&#x9;&#x9;return rc;
<br>&#x9;}
<br> 
<br>&#x9;if (ctx) {
<br>&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;rc = audit_uid_comparator(uid, f-&#x3E;op, n-&#x3E;uid);
<br>&#x9;&#x9;&#x9;if (rc)
<br>&#x9;&#x9;&#x9;&#x9;return rc;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>static int audit_compare_gid(kgid_t gid,
<br>&#x9;&#x9;&#x9;     struct audit_namesname,
<br>&#x9;&#x9;&#x9;     struct audit_fieldf,
<br>&#x9;&#x9;&#x9;     struct audit_contextctx)
<br>{
<br>&#x9;struct audit_namesn;
<br>&#x9;int rc;
<br> 
<br>&#x9;if (name) {
<br>&#x9;&#x9;rc = audit_gid_comparator(gid, f-&#x3E;op, name-&#x3E;gid);
<br>&#x9;&#x9;if (rc)
<br>&#x9;&#x9;&#x9;return rc;
<br>&#x9;}
<br> 
<br>&#x9;if (ctx) {
<br>&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;rc = audit_gid_comparator(gid, f-&#x3E;op, n-&#x3E;gid);
<br>&#x9;&#x9;&#x9;if (rc)
<br>&#x9;&#x9;&#x9;&#x9;return rc;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>static int audit_field_compare(struct task_structtsk,
<br>&#x9;&#x9;&#x9;       const struct credcred,
<br>&#x9;&#x9;&#x9;       struct audit_fieldf,
<br>&#x9;&#x9;&#x9;       struct audit_contextctx,
<br>&#x9;&#x9;&#x9;       struct audit_namesname)
<br>{
<br>&#x9;switch (f-&#x3E;val) {
<br>&#x9;*/ process to file object comparisons /*
<br>&#x9;case AUDIT_COMPARE_UID_TO_OBJ_UID:
<br>&#x9;&#x9;return audit_compare_uid(cred-&#x3E;uid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_GID_TO_OBJ_GID:
<br>&#x9;&#x9;return audit_compare_gid(cred-&#x3E;gid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_EUID_TO_OBJ_UID:
<br>&#x9;&#x9;return audit_compare_uid(cred-&#x3E;euid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_EGID_TO_OBJ_GID:
<br>&#x9;&#x9;return audit_compare_gid(cred-&#x3E;egid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_AUID_TO_OBJ_UID:
<br>&#x9;&#x9;return audit_compare_uid(tsk-&#x3E;loginuid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_SUID_TO_OBJ_UID:
<br>&#x9;&#x9;return audit_compare_uid(cred-&#x3E;suid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_SGID_TO_OBJ_GID:
<br>&#x9;&#x9;return audit_compare_gid(cred-&#x3E;sgid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_FSUID_TO_OBJ_UID:
<br>&#x9;&#x9;return audit_compare_uid(cred-&#x3E;fsuid, name, f, ctx);
<br>&#x9;case AUDIT_COMPARE_FSGID_TO_OBJ_GID:
<br>&#x9;&#x9;return audit_compare_gid(cred-&#x3E;fsgid, name, f, ctx);
<br>&#x9;*/ uid comparisons /*
<br>&#x9;case AUDIT_COMPARE_UID_TO_AUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, tsk-&#x3E;loginuid);
<br>&#x9;case AUDIT_COMPARE_UID_TO_EUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, cred-&#x3E;euid);
<br>&#x9;case AUDIT_COMPARE_UID_TO_SUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, cred-&#x3E;suid);
<br>&#x9;case AUDIT_COMPARE_UID_TO_FSUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, cred-&#x3E;fsuid);
<br>&#x9;*/ auid comparisons /*
<br>&#x9;case AUDIT_COMPARE_AUID_TO_EUID:
<br>&#x9;&#x9;return audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, cred-&#x3E;euid);
<br>&#x9;case AUDIT_COMPARE_AUID_TO_SUID:
<br>&#x9;&#x9;return audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, cred-&#x3E;suid);
<br>&#x9;case AUDIT_COMPARE_AUID_TO_FSUID:
<br>&#x9;&#x9;return audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, cred-&#x3E;fsuid);
<br>&#x9;*/ euid comparisons /*
<br>&#x9;case AUDIT_COMPARE_EUID_TO_SUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;euid, f-&#x3E;op, cred-&#x3E;suid);
<br>&#x9;case AUDIT_COMPARE_EUID_TO_FSUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;euid, f-&#x3E;op, cred-&#x3E;fsuid);
<br>&#x9;*/ suid comparisons /*
<br>&#x9;case AUDIT_COMPARE_SUID_TO_FSUID:
<br>&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;suid, f-&#x3E;op, cred-&#x3E;fsuid);
<br>&#x9;*/ gid comparisons /*
<br>&#x9;case AUDIT_COMPARE_GID_TO_EGID:
<br>&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, cred-&#x3E;egid);
<br>&#x9;case AUDIT_COMPARE_GID_TO_SGID:
<br>&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, cred-&#x3E;sgid);
<br>&#x9;case AUDIT_COMPARE_GID_TO_FSGID:
<br>&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, cred-&#x3E;fsgid);
<br>&#x9;*/ egid comparisons /*
<br>&#x9;case AUDIT_COMPARE_EGID_TO_SGID:
<br>&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;egid, f-&#x3E;op, cred-&#x3E;sgid);
<br>&#x9;case AUDIT_COMPARE_EGID_TO_FSGID:
<br>&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;egid, f-&#x3E;op, cred-&#x3E;fsgid);
<br>&#x9;*/ sgid comparison /*
<br>&#x9;case AUDIT_COMPARE_SGID_TO_FSGID:
<br>&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;sgid, f-&#x3E;op, cred-&#x3E;fsgid);
<br>&#x9;default:
<br>&#x9;&#x9;WARN(1, &#x22;Missing AUDIT_COMPARE define.  Report as a bug\n&#x22;);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>*/ Determine if any context name data matches a rule&#x27;s watch data /*
<br>*/ Compare a task_struct with an audit_rule.  Return 1 on match, 0
<br> otherwise.
<br>
<br> If task_creation is true, this is an explicit indication that we are
<br> filtering a task rule at task creation time.  This and tsk == current are
<br> the only situations where tsk-&#x3E;cred may be accessed without an rcu read lock.
<br> /*
<br>static int audit_filter_rules(struct task_structtsk,
<br>&#x9;&#x9;&#x9;      struct audit_krulerule,
<br>&#x9;&#x9;&#x9;      struct audit_contextctx,
<br>&#x9;&#x9;&#x9;      struct audit_namesname,
<br>&#x9;&#x9;&#x9;      enum audit_statestate,
<br>&#x9;&#x9;&#x9;      bool task_creation)
<br>{
<br>&#x9;const struct credcred;
<br>&#x9;int i, need_sid = 1;
<br>&#x9;u32 sid;
<br>
<br>&#x9;cred = rcu_dereference_check(tsk-&#x3E;cred, tsk == current || task_creation);
<br>
<br>&#x9;for (i = 0; i &#x3C; rule-&#x3E;field_count; i++) {
<br>&#x9;&#x9;struct audit_fieldf = &#x26;rule-&#x3E;fields[i];
<br>&#x9;&#x9;struct audit_namesn;
<br>&#x9;&#x9;int result = 0;
<br>&#x9;&#x9;pid_t pid;
<br>
<br>&#x9;&#x9;switch (f-&#x3E;type) {
<br>&#x9;&#x9;case AUDIT_PID:
<br>&#x9;&#x9;&#x9;pid = task_pid_nr(tsk);
<br>&#x9;&#x9;&#x9;result = audit_comparator(pid, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_PPID:
<br>&#x9;&#x9;&#x9;if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;if (!ctx-&#x3E;ppid)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;ctx-&#x3E;ppid = task_ppid_nr(tsk);
<br>&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;ppid, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EXE:
<br>&#x9;&#x9;&#x9;result = audit_exe_compare(tsk, rule-&#x3E;exe);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_UID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EUID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;euid, f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_SUID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;suid, f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FSUID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;fsuid, f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_GID:
<br>&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;if (f-&#x3E;op == Audit_equal) {
<br>&#x9;&#x9;&#x9;&#x9;if (!result)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = in_group_p(f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;} else if (f-&#x3E;op == Audit_not_equal) {
<br>&#x9;&#x9;&#x9;&#x9;if (result)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = !in_group_p(f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_EGID:
<br>&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;egid, f-&#x3E;op, f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;if (f-&#x3E;op == Audit_equal) {
<br>&#x9;&#x9;&#x9;&#x9;if (!result)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = in_egroup_p(f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;} else if (f-&#x3E;op == Audit_not_equal) {
<br>&#x9;&#x9;&#x9;&#x9;if (result)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = !in_egroup_p(f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_SGID:
<br>&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;sgid, f-&#x3E;op, f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FSGID:
<br>&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;fsgid, f-&#x3E;op, f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_PERS:
<br>&#x9;&#x9;&#x9;result = audit_comparator(tsk-&#x3E;personality, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_ARCH:
<br>&#x9;&#x9;&#x9;if (ctx)
<br>&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;arch, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>
<br>&#x9;&#x9;case AUDIT_EXIT:
<br>&#x9;&#x9;&#x9;if (ctx &#x26;&#x26; ctx-&#x3E;return_valid)
<br>&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;return_code, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_SUCCESS:
<br>&#x9;&#x9;&#x9;if (ctx &#x26;&#x26; ctx-&#x3E;return_valid) {
<br>&#x9;&#x9;&#x9;&#x9;if (f-&#x3E;val)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;return_valid, f-&#x3E;op, AUDITSC_SUCCESS);
<br>&#x9;&#x9;&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;return_valid, f-&#x3E;op, AUDITSC_FAILURE);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_DEVMAJOR:
<br>&#x9;&#x9;&#x9;if (name) {
<br>&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MAJOR(name-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
<br>&#x9;&#x9;&#x9;&#x9;    audit_comparator(MAJOR(name-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val))
<br>&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;} else if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MAJOR(n-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
<br>&#x9;&#x9;&#x9;&#x9;&#x9;    audit_comparator(MAJOR(n-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_DEVMINOR:
<br>&#x9;&#x9;&#x9;if (name) {
<br>&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MINOR(name-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
<br>&#x9;&#x9;&#x9;&#x9;    audit_comparator(MINOR(name-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val))
<br>&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;} else if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MINOR(n-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
<br>&#x9;&#x9;&#x9;&#x9;&#x9;    audit_comparator(MINOR(n-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_INODE:
<br>&#x9;&#x9;&#x9;if (name)
<br>&#x9;&#x9;&#x9;&#x9;result = audit_comparator(name-&#x3E;ino, f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;else if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_comparator(n-&#x3E;ino, f-&#x3E;op, f-&#x3E;val)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_OBJ_UID:
<br>&#x9;&#x9;&#x9;if (name) {
<br>&#x9;&#x9;&#x9;&#x9;result = audit_uid_comparator(name-&#x3E;uid, f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;} else if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_uid_comparator(n-&#x3E;uid, f-&#x3E;op, f-&#x3E;uid)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_OBJ_GID:
<br>&#x9;&#x9;&#x9;if (name) {
<br>&#x9;&#x9;&#x9;&#x9;result = audit_gid_comparator(name-&#x3E;gid, f-&#x3E;op, f-&#x3E;gid);
<br>&#x9;&#x9;&#x9;} else if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_gid_comparator(n-&#x3E;gid, f-&#x3E;op, f-&#x3E;gid)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_WATCH:
<br>&#x9;&#x9;&#x9;if (name)
<br>&#x9;&#x9;&#x9;&#x9;result = audit_watch_compare(rule-&#x3E;watch, name-&#x3E;ino, name-&#x3E;dev);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_DIR:
<br>&#x9;&#x9;&#x9;if (ctx)
<br>&#x9;&#x9;&#x9;&#x9;result = match_tree_refs(ctx, rule-&#x3E;tree);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_LOGINUID:
<br>&#x9;&#x9;&#x9;result = audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, f-&#x3E;uid);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_LOGINUID_SET:
<br>&#x9;&#x9;&#x9;result = audit_comparator(audit_loginuid_set(tsk), f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_SUBJ_USER:
<br>&#x9;&#x9;case AUDIT_SUBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_SUBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_SUBJ_SEN:
<br>&#x9;&#x9;case AUDIT_SUBJ_CLR:
<br>&#x9;&#x9;&#x9;*/ NOTE: this may return negative values indicating
<br>&#x9;&#x9;&#x9;   a temporary error.  We simply treat this as a
<br>&#x9;&#x9;&#x9;   match for now to avoid losing information that
<br>&#x9;&#x9;&#x9;   may be wanted.   An error message will also be
<br>&#x9;&#x9;&#x9;   logged upon error /*
<br>&#x9;&#x9;&#x9;if (f-&#x3E;lsm_rule) {
<br>&#x9;&#x9;&#x9;&#x9;if (need_sid) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;security_task_getsecid(tsk, &#x26;sid);
<br>&#x9;&#x9;&#x9;&#x9;&#x9;need_sid = 0;
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;result = security_audit_rule_match(sid, f-&#x3E;type,
<br>&#x9;&#x9;&#x9;&#x9;                                  f-&#x3E;op,
<br>&#x9;&#x9;&#x9;&#x9;                                  f-&#x3E;lsm_rule,
<br>&#x9;&#x9;&#x9;&#x9;                                  ctx);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_OBJ_USER:
<br>&#x9;&#x9;case AUDIT_OBJ_ROLE:
<br>&#x9;&#x9;case AUDIT_OBJ_TYPE:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
<br>&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
<br>&#x9;&#x9;&#x9;*/ The above note for AUDIT_SUBJ_USER...AUDIT_SUBJ_CLR
<br>&#x9;&#x9;&#x9;   also applies here /*
<br>&#x9;&#x9;&#x9;if (f-&#x3E;lsm_rule) {
<br>&#x9;&#x9;&#x9;&#x9;*/ Find files that match /*
<br>&#x9;&#x9;&#x9;&#x9;if (name) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;result = security_audit_rule_match(
<br>&#x9;&#x9;&#x9;&#x9;&#x9;           name-&#x3E;osid, f-&#x3E;type, f-&#x3E;op,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;           f-&#x3E;lsm_rule, ctx);
<br>&#x9;&#x9;&#x9;&#x9;} else if (ctx) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;if (security_audit_rule_match(n-&#x3E;osid, f-&#x3E;type,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      f-&#x3E;op, f-&#x3E;lsm_rule,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      ctx)) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;&#x9;*/ Find ipc objects that match /*
<br>&#x9;&#x9;&#x9;&#x9;if (!ctx || ctx-&#x3E;type != AUDIT_IPC)
<br>&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;if (security_audit_rule_match(ctx-&#x3E;ipc.osid,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      f-&#x3E;type, f-&#x3E;op,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      f-&#x3E;lsm_rule, ctx))
<br>&#x9;&#x9;&#x9;&#x9;&#x9;++result;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_ARG0:
<br>&#x9;&#x9;case AUDIT_ARG1:
<br>&#x9;&#x9;case AUDIT_ARG2:
<br>&#x9;&#x9;case AUDIT_ARG3:
<br>&#x9;&#x9;&#x9;if (ctx)
<br>&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;argv[f-&#x3E;type-AUDIT_ARG0], f-&#x3E;op, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FILTERKEY:
<br>&#x9;&#x9;&#x9;*/ ignore this field for filtering /*
<br>&#x9;&#x9;&#x9;result = 1;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_PERM:
<br>&#x9;&#x9;&#x9;result = audit_match_perm(ctx, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FILETYPE:
<br>&#x9;&#x9;&#x9;result = audit_match_filetype(ctx, f-&#x3E;val);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;case AUDIT_FIELD_COMPARE:
<br>&#x9;&#x9;&#x9;result = audit_field_compare(tsk, cred, f, ctx, name);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (!result)
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;}
<br>
<br>&#x9;if (ctx) {
<br>&#x9;&#x9;if (rule-&#x3E;prio &#x3C;= ctx-&#x3E;prio)
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;&#x9;if (rule-&#x3E;filterkey) {
<br>&#x9;&#x9;&#x9;kfree(ctx-&#x3E;filterkey);
<br>&#x9;&#x9;&#x9;ctx-&#x3E;filterkey = kstrdup(rule-&#x3E;filterkey, GFP_ATOMIC);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;ctx-&#x3E;prio = rule-&#x3E;prio;
<br>&#x9;}
<br>&#x9;switch (rule-&#x3E;action) {
<br>&#x9;case AUDIT_NEVER:   state = AUDIT_DISABLED;&#x9;    break;
<br>&#x9;case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
<br>&#x9;}
<br>&#x9;return 1;
<br>}
<br>
<br>*/ At process creation time, we can determine if system-call auditing is
<br> completely disabled for this task.  Since we only have the task
<br> structure at this point, we can only check uid and gid.
<br> /*
<br>static enum audit_state audit_filter_task(struct task_structtsk, char*key)
<br>{
<br>&#x9;struct audit_entrye;
<br>&#x9;enum audit_state   state;
<br>
<br>&#x9;rcu_read_lock();
<br>&#x9;list_for_each_entry_rcu(e, &#x26;audit_filter_list[AUDIT_FILTER_TASK], list) {
<br>&#x9;&#x9;if (audit_filter_rules(tsk, &#x26;e-&#x3E;rule, NULL, NULL,
<br>&#x9;&#x9;&#x9;&#x9;       &#x26;state, true)) {
<br>&#x9;&#x9;&#x9;if (state == AUDIT_RECORD_CONTEXT)
<br>&#x9;&#x9;&#x9;&#x9;*key = kstrdup(e-&#x3E;rule.filterkey, GFP_ATOMIC);
<br>&#x9;&#x9;&#x9;rcu_read_unlock();
<br>&#x9;&#x9;&#x9;return state;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;rcu_read_unlock();
<br>&#x9;return AUDIT_BUILD_CONTEXT;
<br>}
<br>
<br>static int audit_in_mask(const struct audit_krulerule, unsigned long val)
<br>{
<br>&#x9;int word, bit;
<br>
<br>&#x9;if (val &#x3E; 0xffffffff)
<br>&#x9;&#x9;return false;
<br>
<br>&#x9;word = AUDIT_WORD(val);
<br>&#x9;if (word &#x3E;= AUDIT_BITMASK_SIZE)
<br>&#x9;&#x9;return false;
<br>
<br>&#x9;bit = AUDIT_BIT(val);
<br>
<br>&#x9;return rule-&#x3E;mask[word] &#x26; bit;
<br>}
<br>
<br>*/ At syscall entry and exit time, this filter is called if the
<br> audit_state is not low enough that auditing cannot take place, but is
<br> also not high enough that we already know we have to write an audit
<br> record (i.e., the state is AUDIT_SETUP_CONTEXT or AUDIT_BUILD_CONTEXT).
<br> /*
<br>static enum audit_state audit_filter_syscall(struct task_structtsk,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;     struct audit_contextctx,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;     struct list_headlist)
<br>{
<br>&#x9;struct audit_entrye;
<br>&#x9;enum audit_state state;
<br>
<br>&#x9;if (audit_pid &#x26;&#x26; tsk-&#x3E;tgid == audit_pid)
<br>&#x9;&#x9;return AUDIT_DISABLED;
<br>
<br>&#x9;rcu_read_lock();
<br>&#x9;if (!list_empty(list)) {
<br>&#x9;&#x9;list_for_each_entry_rcu(e, list, list) {
<br>&#x9;&#x9;&#x9;if (audit_in_mask(&#x26;e-&#x3E;rule, ctx-&#x3E;major) &#x26;&#x26;
<br>&#x9;&#x9;&#x9;    audit_filter_rules(tsk, &#x26;e-&#x3E;rule, ctx, NULL,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;       &#x26;state, false)) {
<br>&#x9;&#x9;&#x9;&#x9;rcu_read_unlock();
<br>&#x9;&#x9;&#x9;&#x9;ctx-&#x3E;current_state = state;
<br>&#x9;&#x9;&#x9;&#x9;return state;
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;rcu_read_unlock();
<br>&#x9;return AUDIT_BUILD_CONTEXT;
<br>}
<br>
<br>*/
<br> Given an audit_name check the inode hash table to see if they match.
<br> Called holding the rcu read lock to protect the use of audit_inode_hash
<br> /*
<br>static int audit_filter_inode_name(struct task_structtsk,
<br>&#x9;&#x9;&#x9;&#x9;   struct audit_namesn,
<br>&#x9;&#x9;&#x9;&#x9;   struct audit_contextctx) {
<br>&#x9;int h = audit_hash_ino((u32)n-&#x3E;ino);
<br>&#x9;struct list_headlist = &#x26;audit_inode_hash[h];
<br>&#x9;struct audit_entrye;
<br>&#x9;enum audit_state state;
<br>
<br>&#x9;if (list_empty(list))
<br>&#x9;&#x9;return 0;
<br>
<br>&#x9;list_for_each_entry_rcu(e, list, list) {
<br>&#x9;&#x9;if (audit_in_mask(&#x26;e-&#x3E;rule, ctx-&#x3E;major) &#x26;&#x26;
<br>&#x9;&#x9;    audit_filter_rules(tsk, &#x26;e-&#x3E;rule, ctx, n, &#x26;state, false)) {
<br>&#x9;&#x9;&#x9;ctx-&#x3E;current_state = state;
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>*/ At syscall exit time, this filter is called if any audit_names have been
<br> collected during syscall processing.  We only check rules in sublists at hash
<br> buckets applicable to the inode numbers in audit_names.
<br> Regarding audit_state, same rules apply as for audit_filter_syscall().
<br> /*
<br>void audit_filter_inodes(struct task_structtsk, struct audit_contextctx)
<br>{
<br>&#x9;struct audit_namesn;
<br>
<br>&#x9;if (audit_pid &#x26;&#x26; tsk-&#x3E;tgid == audit_pid)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;rcu_read_lock();
<br>
<br>&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
<br>&#x9;&#x9;if (audit_filter_inode_name(tsk, n, ctx))
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;}
<br>&#x9;rcu_read_unlock();
<br>}
<br>
<br>*/ Transfer the audit context pointer to the caller, clearing it in the tsk&#x27;s struct /*
<br>static inline struct audit_contextaudit_take_context(struct task_structtsk,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      int return_valid,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      long return_code)
<br>{
<br>&#x9;struct audit_contextcontext = tsk-&#x3E;audit_context;
<br>
<br>&#x9;if (!context)
<br>&#x9;&#x9;return NULL;
<br>&#x9;context-&#x3E;return_valid = return_valid;
<br>
<br>&#x9;*/
<br>&#x9; we need to fix up the return code in the audit logs if the actual
<br>&#x9; return codes are later going to be fixed up by the arch specific
<br>&#x9; signal handlers
<br>&#x9;
<br>&#x9; This is actually a test for:
<br>&#x9; (rc == ERESTARTSYS ) || (rc == ERESTARTNOINTR) ||
<br>&#x9; (rc == ERESTARTNOHAND) || (rc == ERESTART_RESTARTBLOCK)
<br>&#x9;
<br>&#x9; but is faster than a bunch of ||
<br>&#x9; /*
<br>&#x9;if (unlikely(return_code &#x3C;= -ERESTARTSYS) &#x26;&#x26;
<br>&#x9;    (return_code &#x3E;= -ERESTART_RESTARTBLOCK) &#x26;&#x26;
<br>&#x9;    (return_code != -ENOIOCTLCMD))
<br>&#x9;&#x9;context-&#x3E;return_code = -EINTR;
<br>&#x9;else
<br>&#x9;&#x9;context-&#x3E;return_code  = return_code;
<br>
<br>&#x9;if (context-&#x3E;in_syscall &#x26;&#x26; !context-&#x3E;dummy) {
<br>&#x9;&#x9;audit_filter_syscall(tsk, context, &#x26;audit_filter_list[AUDIT_FILTER_EXIT]);
<br>&#x9;&#x9;audit_filter_inodes(tsk, context);
<br>&#x9;}
<br>
<br>&#x9;tsk-&#x3E;audit_context = NULL;
<br>&#x9;return context;
<br>}
<br>
<br>static inline void audit_proctitle_free(struct audit_contextcontext)
<br>{
<br>&#x9;kfree(context-&#x3E;proctitle.value);
<br>&#x9;context-&#x3E;proctitle.value = NULL;
<br>&#x9;context-&#x3E;proctitle.len = 0;
<br>}
<br>
<br>static inline void audit_free_names(struct audit_contextcontext)
<br>{
<br>&#x9;struct audit_namesn,next;
<br>
<br>&#x9;list_for_each_entry_safe(n, next, &#x26;context-&#x3E;names_list, list) {
<br>&#x9;&#x9;list_del(&#x26;n-&#x3E;list);
<br>&#x9;&#x9;if (n-&#x3E;name)
<br>&#x9;&#x9;&#x9;putname(n-&#x3E;name);
<br>&#x9;&#x9;if (n-&#x3E;should_free)
<br>&#x9;&#x9;&#x9;kfree(n);
<br>&#x9;}
<br>&#x9;context-&#x3E;name_count = 0;
<br>&#x9;path_put(&#x26;context-&#x3E;pwd);
<br>&#x9;context-&#x3E;pwd.dentry = NULL;
<br>&#x9;context-&#x3E;pwd.mnt = NULL;
<br>}
<br>
<br>static inline void audit_free_aux(struct audit_contextcontext)
<br>{
<br>&#x9;struct audit_aux_dataaux;
<br>
<br>&#x9;while ((aux = context-&#x3E;aux)) {
<br>&#x9;&#x9;context-&#x3E;aux = aux-&#x3E;next;
<br>&#x9;&#x9;kfree(aux);
<br>&#x9;}
<br>&#x9;while ((aux = context-&#x3E;aux_pids)) {
<br>&#x9;&#x9;context-&#x3E;aux_pids = aux-&#x3E;next;
<br>&#x9;&#x9;kfree(aux);
<br>&#x9;}
<br>}
<br>
<br>static inline struct audit_contextaudit_alloc_context(enum audit_state state)
<br>{
<br>&#x9;struct audit_contextcontext;
<br>
<br>&#x9;context = kzalloc(sizeof(*context), GFP_KERNEL);
<br>&#x9;if (!context)
<br>&#x9;&#x9;return NULL;
<br>&#x9;context-&#x3E;state = state;
<br>&#x9;context-&#x3E;prio = state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;
<br>&#x9;INIT_LIST_HEAD(&#x26;context-&#x3E;killed_trees);
<br>&#x9;INIT_LIST_HEAD(&#x26;context-&#x3E;names_list);
<br>&#x9;return context;
<br>}
<br>
<br>*/
<br> audit_alloc - allocate an audit context block for a task
<br> @tsk: task
<br>
<br> Filter on the task information and allocate a per-task audit context
<br> if necessary.  Doing so turns on system call auditing for the
<br> specified task.  This is called from copy_process, so no lock is
<br> needed.
<br> /*
<br>int audit_alloc(struct task_structtsk)
<br>{
<br>&#x9;struct audit_contextcontext;
<br>&#x9;enum audit_state     state;
<br>&#x9;charkey = NULL;
<br>
<br>&#x9;if (likely(!audit_ever_enabled))
<br>&#x9;&#x9;return 0;/ Return if not auditing. /*
<br>
<br>&#x9;state = audit_filter_task(tsk, &#x26;key);
<br>&#x9;if (state == AUDIT_DISABLED) {
<br>&#x9;&#x9;clear_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>
<br>&#x9;if (!(context = audit_alloc_context(state))) {
<br>&#x9;&#x9;kfree(key);
<br>&#x9;&#x9;audit_log_lost(&#x22;out of memory in audit_alloc&#x22;);
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;}
<br>&#x9;context-&#x3E;filterkey = key;
<br>
<br>&#x9;tsk-&#x3E;audit_context  = context;
<br>&#x9;set_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
<br>&#x9;return 0;
<br>}
<br>
<br>static inline void audit_free_context(struct audit_contextcontext)
<br>{
<br>&#x9;audit_free_names(context);
<br>&#x9;unroll_tree_refs(context, NULL, 0);
<br>&#x9;free_tree_refs(context);
<br>&#x9;audit_free_aux(context);
<br>&#x9;kfree(context-&#x3E;filterkey);
<br>&#x9;kfree(context-&#x3E;sockaddr);
<br>&#x9;audit_proctitle_free(context);
<br>&#x9;kfree(context);
<br>}
<br>
<br>static int audit_log_pid_context(struct audit_contextcontext, pid_t pid,
<br>&#x9;&#x9;&#x9;&#x9; kuid_t auid, kuid_t uid, unsigned int sessionid,
<br>&#x9;&#x9;&#x9;&#x9; u32 sid, charcomm)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;charctx = NULL;
<br>&#x9;u32 len;
<br>&#x9;int rc = 0;
<br>
<br>&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_OBJ_PID);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return rc;
<br>
<br>&#x9;audit_log_format(ab, &#x22;opid=%d oauid=%d ouid=%d oses=%d&#x22;, pid,
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, auid),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, uid), sessionid);
<br>&#x9;if (sid) {
<br>&#x9;&#x9;if (security_secid_to_secctx(sid, &#x26;ctx, &#x26;len)) {
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=(none)&#x22;);
<br>&#x9;&#x9;&#x9;rc = 1;
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, ctx);
<br>&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;audit_log_format(ab, &#x22; ocomm=&#x22;);
<br>&#x9;audit_log_untrustedstring(ab, comm);
<br>&#x9;audit_log_end(ab);
<br>
<br>&#x9;return rc;
<br>}
<br>
<br>*/
<br> to_send and len_sent accounting are very loose estimates.  We aren&#x27;t
<br> really worried about a hard cap to MAX_EXECVE_AUDIT_LEN so much as being
<br> within about 500 bytes (next page boundary)
<br>
<br> why snprintf?  an int is up to 12 digits long.  if we just assumed when
<br> logging that a[%d]= was going to be 16 characters long we would be wasting
<br> space in every audit message.  In one 7500 byte message we can log up to
<br> about 1000 min size arguments.  That comes down to about 50% waste of space
<br> if we didn&#x27;t do the snprintf to find out how long arg_num_len was.
<br> /*
<br>static int audit_log_single_execve_arg(struct audit_contextcontext,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;struct audit_buffer*ab,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;int arg_num,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;size_tlen_sent,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;const char __userp,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;charbuf)
<br>{
<br>&#x9;char arg_num_len_buf[12];
<br>&#x9;const char __usertmp_p = p;
<br>&#x9;*/ how many digits are in arg_num? 5 is the length of &#x27; a=&#x22;&#x22;&#x27; /*
<br>&#x9;size_t arg_num_len = snprintf(arg_num_len_buf, 12, &#x22;%d&#x22;, arg_num) + 5;
<br>&#x9;size_t len, len_left, to_send;
<br>&#x9;size_t max_execve_audit_len = MAX_EXECVE_AUDIT_LEN;
<br>&#x9;unsigned int i, has_cntl = 0, too_long = 0;
<br>&#x9;int ret;
<br>
<br>&#x9;*/ strnlen_user includes the null we don&#x27;t want to send /*
<br>&#x9;len_left = len = strnlen_user(p, MAX_ARG_STRLEN) - 1;
<br>
<br>&#x9;*/
<br>&#x9; We just created this mm, if we can&#x27;t find the strings
<br>&#x9; we just copied into it something is _very_ wrong. Similar
<br>&#x9; for strings that are too long, we should not have created
<br>&#x9; any.
<br>&#x9; /*
<br>&#x9;if (WARN_ON_ONCE(len &#x3C; 0 || len &#x3E; MAX_ARG_STRLEN - 1)) {
<br>&#x9;&#x9;send_sig(SIGKILL, current, 0);
<br>&#x9;&#x9;return -1;
<br>&#x9;}
<br>
<br>&#x9;*/ walk the whole argument looking for non-ascii chars /*
<br>&#x9;do {
<br>&#x9;&#x9;if (len_left &#x3E; MAX_EXECVE_AUDIT_LEN)
<br>&#x9;&#x9;&#x9;to_send = MAX_EXECVE_AUDIT_LEN;
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;to_send = len_left;
<br>&#x9;&#x9;ret = copy_from_user(buf, tmp_p, to_send);
<br>&#x9;&#x9;*/
<br>&#x9;&#x9; There is no reason for this copy to be short. We just
<br>&#x9;&#x9; copied them here, and the mm hasn&#x27;t been exposed to user-
<br>&#x9;&#x9; space yet.
<br>&#x9;&#x9; /*
<br>&#x9;&#x9;if (ret) {
<br>&#x9;&#x9;&#x9;WARN_ON(1);
<br>&#x9;&#x9;&#x9;send_sig(SIGKILL, current, 0);
<br>&#x9;&#x9;&#x9;return -1;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;buf[to_send] = &#x27;\0&#x27;;
<br>&#x9;&#x9;has_cntl = audit_string_contains_control(buf, to_send);
<br>&#x9;&#x9;if (has_cntl) {
<br>&#x9;&#x9;&#x9;*/
<br>&#x9;&#x9;&#x9; hex messages get logged as 2 bytes, so we can only
<br>&#x9;&#x9;&#x9; send half as much in each message
<br>&#x9;&#x9;&#x9; /*
<br>&#x9;&#x9;&#x9;max_execve_audit_len = MAX_EXECVE_AUDIT_LEN / 2;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;len_left -= to_send;
<br>&#x9;&#x9;tmp_p += to_send;
<br>&#x9;} while (len_left &#x3E; 0);
<br>
<br>&#x9;len_left = len;
<br>
<br>&#x9;if (len &#x3E; max_execve_audit_len)
<br>&#x9;&#x9;too_long = 1;
<br>
<br>&#x9;*/ rewalk the argument actually logging the message /*
<br>&#x9;for (i = 0; len_left &#x3E; 0; i++) {
<br>&#x9;&#x9;int room_left;
<br>
<br>&#x9;&#x9;if (len_left &#x3E; max_execve_audit_len)
<br>&#x9;&#x9;&#x9;to_send = max_execve_audit_len;
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;to_send = len_left;
<br>
<br>&#x9;&#x9;*/ do we have space left to send this argument in this ab? /*
<br>&#x9;&#x9;room_left = MAX_EXECVE_AUDIT_LEN - arg_num_len -len_sent;
<br>&#x9;&#x9;if (has_cntl)
<br>&#x9;&#x9;&#x9;room_left -= (to_send 2);
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;room_left -= to_send;
<br>&#x9;&#x9;if (room_left &#x3C; 0) {
<br>&#x9;&#x9;&#x9;*len_sent = 0;
<br>&#x9;&#x9;&#x9;audit_log_end(*ab);
<br>&#x9;&#x9;&#x9;*ab = audit_log_start(context, GFP_KERNEL, AUDIT_EXECVE);
<br>&#x9;&#x9;&#x9;if (!*ab)
<br>&#x9;&#x9;&#x9;&#x9;return 0;
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;*/
<br>&#x9;&#x9; first record needs to say how long the original string was
<br>&#x9;&#x9; so we can be sure nothing was lost.
<br>&#x9;&#x9; /*
<br>&#x9;&#x9;if ((i == 0) &#x26;&#x26; (too_long))
<br>&#x9;&#x9;&#x9;audit_log_format(*ab, &#x22; a%d_len=%zu&#x22;, arg_num,
<br>&#x9;&#x9;&#x9;&#x9;&#x9; has_cntl ? 2*len : len);
<br>
<br>&#x9;&#x9;*/
<br>&#x9;&#x9; normally arguments are small enough to fit and we already
<br>&#x9;&#x9; filled buf above when we checked for control characters
<br>&#x9;&#x9; so don&#x27;t bother with another copy_from_user
<br>&#x9;&#x9; /*
<br>&#x9;&#x9;if (len &#x3E;= max_execve_audit_len)
<br>&#x9;&#x9;&#x9;ret = copy_from_user(buf, p, to_send);
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;ret = 0;
<br>&#x9;&#x9;if (ret) {
<br>&#x9;&#x9;&#x9;WARN_ON(1);
<br>&#x9;&#x9;&#x9;send_sig(SIGKILL, current, 0);
<br>&#x9;&#x9;&#x9;return -1;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;buf[to_send] = &#x27;\0&#x27;;
<br>
<br>&#x9;&#x9;*/ actually log it /*
<br>&#x9;&#x9;audit_log_format(*ab, &#x22; a%d&#x22;, arg_num);
<br>&#x9;&#x9;if (too_long)
<br>&#x9;&#x9;&#x9;audit_log_format(*ab, &#x22;[%d]&#x22;, i);
<br>&#x9;&#x9;audit_log_format(*ab, &#x22;=&#x22;);
<br>&#x9;&#x9;if (has_cntl)
<br>&#x9;&#x9;&#x9;audit_log_n_hex(*ab, buf, to_send);
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;audit_log_string(*ab, buf);
<br>
<br>&#x9;&#x9;p += to_send;
<br>&#x9;&#x9;len_left -= to_send;
<br>&#x9;&#x9;*len_sent += arg_num_len;
<br>&#x9;&#x9;if (has_cntl)
<br>&#x9;&#x9;&#x9;*len_sent += to_send 2;
<br>&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;*len_sent += to_send;
<br>&#x9;}
<br>&#x9;*/ include the null we didn&#x27;t log /*
<br>&#x9;return len + 1;
<br>}
<br>
<br>static void audit_log_execve_info(struct audit_contextcontext,
<br>&#x9;&#x9;&#x9;&#x9;  struct audit_buffer*ab)
<br>{
<br>&#x9;int i, len;
<br>&#x9;size_t len_sent = 0;
<br>&#x9;const char __userp;
<br>&#x9;charbuf;
<br>
<br>&#x9;p = (const char __user)current-&#x3E;mm-&#x3E;arg_start;
<br>
<br>&#x9;audit_log_format(*ab, &#x22;argc=%d&#x22;, context-&#x3E;execve.argc);
<br>
<br>&#x9;*/
<br>&#x9; we need some kernel buffer to hold the userspace args.  Just
<br>&#x9; allocate one big one rather than allocating one of the right size
<br>&#x9; for every single argument inside audit_log_single_execve_arg()
<br>&#x9; should be &#x3C;8k allocation so should be pretty safe.
<br>&#x9; /*
<br>&#x9;buf = kmalloc(MAX_EXECVE_AUDIT_LEN + 1, GFP_KERNEL);
<br>&#x9;if (!buf) {
<br>&#x9;&#x9;audit_panic(&#x22;out of memory for argv string&#x22;);
<br>&#x9;&#x9;return;
<br>&#x9;}
<br>
<br>&#x9;for (i = 0; i &#x3C; context-&#x3E;execve.argc; i++) {
<br>&#x9;&#x9;len = audit_log_single_execve_arg(context, ab, i,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  &#x26;len_sent, p, buf);
<br>&#x9;&#x9;if (len &#x3C;= 0)
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;p += len;
<br>&#x9;}
<br>&#x9;kfree(buf);
<br>}
<br>
<br>static void show_special(struct audit_contextcontext, intcall_panic)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;int i;
<br>
<br>&#x9;ab = audit_log_start(context, GFP_KERNEL, context-&#x3E;type);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;switch (context-&#x3E;type) {
<br>&#x9;case AUDIT_SOCKETCALL: {
<br>&#x9;&#x9;int nargs = context-&#x3E;socketcall.nargs;
<br>&#x9;&#x9;audit_log_format(ab, &#x22;nargs=%d&#x22;, nargs);
<br>&#x9;&#x9;for (i = 0; i &#x3C; nargs; i++)
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; a%d=%lx&#x22;, i,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;socketcall.args[i]);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_IPC: {
<br>&#x9;&#x9;u32 osid = context-&#x3E;ipc.osid;
<br>
<br>&#x9;&#x9;audit_log_format(ab, &#x22;ouid=%u ogid=%u mode=%#ho&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, context-&#x3E;ipc.uid),
<br>&#x9;&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, context-&#x3E;ipc.gid),
<br>&#x9;&#x9;&#x9;&#x9; context-&#x3E;ipc.mode);
<br>&#x9;&#x9;if (osid) {
<br>&#x9;&#x9;&#x9;charctx = NULL;
<br>&#x9;&#x9;&#x9;u32 len;
<br>&#x9;&#x9;&#x9;if (security_secid_to_secctx(osid, &#x26;ctx, &#x26;len)) {
<br>&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; osid=%u&#x22;, osid);
<br>&#x9;&#x9;&#x9;&#x9;*call_panic = 1;
<br>&#x9;&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, ctx);
<br>&#x9;&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (context-&#x3E;ipc.has_perm) {
<br>&#x9;&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;     AUDIT_IPC_SET_PERM);
<br>&#x9;&#x9;&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;&#x9;&#x9;return;
<br>&#x9;&#x9;&#x9;audit_log_format(ab,
<br>&#x9;&#x9;&#x9;&#x9;&#x22;qbytes=%lx ouid=%u ogid=%u mode=%#ho&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.qbytes,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.perm_uid,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.perm_gid,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.perm_mode);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_MQ_OPEN: {
<br>&#x9;&#x9;audit_log_format(ab,
<br>&#x9;&#x9;&#x9;&#x22;oflag=0x%x mode=%#ho mq_flags=0x%lx mq_maxmsg=%ld &#x22;
<br>&#x9;&#x9;&#x9;&#x22;mq_msgsize=%ld mq_curmsgs=%ld&#x22;,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_open.oflag, context-&#x3E;mq_open.mode,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_flags,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_maxmsg,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_msgsize,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_curmsgs);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_MQ_SENDRECV: {
<br>&#x9;&#x9;audit_log_format(ab,
<br>&#x9;&#x9;&#x9;&#x22;mqdes=%d msg_len=%zd msg_prio=%u &#x22;
<br>&#x9;&#x9;&#x9;&#x22;abs_timeout_sec=%ld abs_timeout_nsec=%ld&#x22;,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.mqdes,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.msg_len,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.msg_prio,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.abs_timeout.tv_sec,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.abs_timeout.tv_nsec);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_MQ_NOTIFY: {
<br>&#x9;&#x9;audit_log_format(ab, &#x22;mqdes=%d sigev_signo=%d&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;mq_notify.mqdes,
<br>&#x9;&#x9;&#x9;&#x9;context-&#x3E;mq_notify.sigev_signo);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_MQ_GETSETATTR: {
<br>&#x9;&#x9;struct mq_attrattr = &#x26;context-&#x3E;mq_getsetattr.mqstat;
<br>&#x9;&#x9;audit_log_format(ab,
<br>&#x9;&#x9;&#x9;&#x22;mqdes=%d mq_flags=0x%lx mq_maxmsg=%ld mq_msgsize=%ld &#x22;
<br>&#x9;&#x9;&#x9;&#x22;mq_curmsgs=%ld &#x22;,
<br>&#x9;&#x9;&#x9;context-&#x3E;mq_getsetattr.mqdes,
<br>&#x9;&#x9;&#x9;attr-&#x3E;mq_flags, attr-&#x3E;mq_maxmsg,
<br>&#x9;&#x9;&#x9;attr-&#x3E;mq_msgsize, attr-&#x3E;mq_curmsgs);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_CAPSET: {
<br>&#x9;&#x9;audit_log_format(ab, &#x22;pid=%d&#x22;, context-&#x3E;capset.pid);
<br>&#x9;&#x9;audit_log_cap(ab, &#x22;cap_pi&#x22;, &#x26;context-&#x3E;capset.cap.inheritable);
<br>&#x9;&#x9;audit_log_cap(ab, &#x22;cap_pp&#x22;, &#x26;context-&#x3E;capset.cap.permitted);
<br>&#x9;&#x9;audit_log_cap(ab, &#x22;cap_pe&#x22;, &#x26;context-&#x3E;capset.cap.effective);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_MMAP: {
<br>&#x9;&#x9;audit_log_format(ab, &#x22;fd=%d flags=0x%x&#x22;, context-&#x3E;mmap.fd,
<br>&#x9;&#x9;&#x9;&#x9; context-&#x3E;mmap.flags);
<br>&#x9;&#x9;break; }
<br>&#x9;case AUDIT_EXECVE: {
<br>&#x9;&#x9;audit_log_execve_info(context, &#x26;ab);
<br>&#x9;&#x9;break; }
<br>&#x9;}
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>static inline int audit_proctitle_rtrim(charproctitle, int len)
<br>{
<br>&#x9;charend = proctitle + len - 1;
<br>&#x9;while (end &#x3E; proctitle &#x26;&#x26; !isprint(*end))
<br>&#x9;&#x9;end--;
<br>
<br>&#x9;*/ catch the case where proctitle is only 1 non-print character /*
<br>&#x9;len = end - proctitle + 1;
<br>&#x9;len -= isprint(proctitle[len-1]) == 0;
<br>&#x9;return len;
<br>}
<br>
<br>static void audit_log_proctitle(struct task_structtsk,
<br>&#x9;&#x9;&#x9; struct audit_contextcontext)
<br>{
<br>&#x9;int res;
<br>&#x9;charbuf;
<br>&#x9;charmsg = &#x22;(null)&#x22;;
<br>&#x9;int len = strlen(msg);
<br>&#x9;struct audit_bufferab;
<br>
<br>&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_PROCTITLE);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;&#x9;*/ audit_panic or being filtered /*
<br>
<br>&#x9;audit_log_format(ab, &#x22;proctitle=&#x22;);
<br>
<br>&#x9;*/ Not  cached /*
<br>&#x9;if (!context-&#x3E;proctitle.value) {
<br>&#x9;&#x9;buf = kmalloc(MAX_PROCTITLE_AUDIT_LEN, GFP_KERNEL);
<br>&#x9;&#x9;if (!buf)
<br>&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;*/ Historically called this from procfs naming /*
<br>&#x9;&#x9;res = get_cmdline(tsk, buf, MAX_PROCTITLE_AUDIT_LEN);
<br>&#x9;&#x9;if (res == 0) {
<br>&#x9;&#x9;&#x9;kfree(buf);
<br>&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;res = audit_proctitle_rtrim(buf, res);
<br>&#x9;&#x9;if (res == 0) {
<br>&#x9;&#x9;&#x9;kfree(buf);
<br>&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;context-&#x3E;proctitle.value = buf;
<br>&#x9;&#x9;context-&#x3E;proctitle.len = res;
<br>&#x9;}
<br>&#x9;msg = context-&#x3E;proctitle.value;
<br>&#x9;len = context-&#x3E;proctitle.len;
<br>out:
<br>&#x9;audit_log_n_untrustedstring(ab, msg, len);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>static void audit_log_exit(struct audit_contextcontext, struct task_structtsk)
<br>{
<br>&#x9;int i, call_panic = 0;
<br>&#x9;struct audit_bufferab;
<br>&#x9;struct audit_aux_dataaux;
<br>&#x9;struct audit_namesn;
<br>
<br>&#x9;*/ tsk == current /*
<br>&#x9;context-&#x3E;personality = tsk-&#x3E;personality;
<br>
<br>&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_SYSCALL);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;&#x9;&#x9;*/ audit_panic has been called /*
<br>&#x9;audit_log_format(ab, &#x22;arch=%x syscall=%d&#x22;,
<br>&#x9;&#x9;&#x9; context-&#x3E;arch, context-&#x3E;major);
<br>&#x9;if (context-&#x3E;personality != PER_LINUX)
<br>&#x9;&#x9;audit_log_format(ab, &#x22; per=%lx&#x22;, context-&#x3E;personality);
<br>&#x9;if (context-&#x3E;return_valid)
<br>&#x9;&#x9;audit_log_format(ab, &#x22; success=%s exit=%ld&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; (context-&#x3E;return_valid==AUDITSC_SUCCESS)?&#x22;yes&#x22;:&#x22;no&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; context-&#x3E;return_code);
<br>
<br>&#x9;audit_log_format(ab,
<br>&#x9;&#x9;&#x9; &#x22; a0=%lx a1=%lx a2=%lx a3=%lx items=%d&#x22;,
<br>&#x9;&#x9;&#x9; context-&#x3E;argv[0],
<br>&#x9;&#x9;&#x9; context-&#x3E;argv[1],
<br>&#x9;&#x9;&#x9; context-&#x3E;argv[2],
<br>&#x9;&#x9;&#x9; context-&#x3E;argv[3],
<br>&#x9;&#x9;&#x9; context-&#x3E;name_count);
<br>
<br>&#x9;audit_log_task_info(ab, tsk);
<br>&#x9;audit_log_key(ab, context-&#x3E;filterkey);
<br>&#x9;audit_log_end(ab);
<br>
<br>&#x9;for (aux = context-&#x3E;aux; aux; aux = aux-&#x3E;next) {
<br>
<br>&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, aux-&#x3E;type);
<br>&#x9;&#x9;if (!ab)
<br>&#x9;&#x9;&#x9;continue;/ audit_panic has been called /*
<br>
<br>&#x9;&#x9;switch (aux-&#x3E;type) {
<br>
<br>&#x9;&#x9;case AUDIT_BPRM_FCAPS: {
<br>&#x9;&#x9;&#x9;struct audit_aux_data_bprm_fcapsaxs = (void)aux;
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22;fver=%x&#x22;, axs-&#x3E;fcap_ver);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;fp&#x22;, &#x26;axs-&#x3E;fcap.permitted);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;fi&#x22;, &#x26;axs-&#x3E;fcap.inheritable);
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; fe=%d&#x22;, axs-&#x3E;fcap.fE);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;old_pp&#x22;, &#x26;axs-&#x3E;old_pcap.permitted);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;old_pi&#x22;, &#x26;axs-&#x3E;old_pcap.inheritable);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;old_pe&#x22;, &#x26;axs-&#x3E;old_pcap.effective);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;new_pp&#x22;, &#x26;axs-&#x3E;new_pcap.permitted);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;new_pi&#x22;, &#x26;axs-&#x3E;new_pcap.inheritable);
<br>&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;new_pe&#x22;, &#x26;axs-&#x3E;new_pcap.effective);
<br>&#x9;&#x9;&#x9;break; }
<br>
<br>&#x9;&#x9;}
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;}
<br>
<br>&#x9;if (context-&#x3E;type)
<br>&#x9;&#x9;show_special(context, &#x26;call_panic);
<br>
<br>&#x9;if (context-&#x3E;fds[0] &#x3E;= 0) {
<br>&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_FD_PAIR);
<br>&#x9;&#x9;if (ab) {
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22;fd0=%d fd1=%d&#x22;,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;context-&#x3E;fds[0], context-&#x3E;fds[1]);
<br>&#x9;&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;if (context-&#x3E;sockaddr_len) {
<br>&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_SOCKADDR);
<br>&#x9;&#x9;if (ab) {
<br>&#x9;&#x9;&#x9;audit_log_format(ab, &#x22;saddr=&#x22;);
<br>&#x9;&#x9;&#x9;audit_log_n_hex(ab, (void)context-&#x3E;sockaddr,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;context-&#x3E;sockaddr_len);
<br>&#x9;&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;for (aux = context-&#x3E;aux_pids; aux; aux = aux-&#x3E;next) {
<br>&#x9;&#x9;struct audit_aux_data_pidsaxs = (void)aux;
<br>
<br>&#x9;&#x9;for (i = 0; i &#x3C; axs-&#x3E;pid_count; i++)
<br>&#x9;&#x9;&#x9;if (audit_log_pid_context(context, axs-&#x3E;target_pid[i],
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_auid[i],
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_uid[i],
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_sessionid[i],
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_sid[i],
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_comm[i]))
<br>&#x9;&#x9;&#x9;&#x9;call_panic = 1;
<br>&#x9;}
<br>
<br>&#x9;if (context-&#x3E;target_pid &#x26;&#x26;
<br>&#x9;    audit_log_pid_context(context, context-&#x3E;target_pid,
<br>&#x9;&#x9;&#x9;&#x9;  context-&#x3E;target_auid, context-&#x3E;target_uid,
<br>&#x9;&#x9;&#x9;&#x9;  context-&#x3E;target_sessionid,
<br>&#x9;&#x9;&#x9;&#x9;  context-&#x3E;target_sid, context-&#x3E;target_comm))
<br>&#x9;&#x9;&#x9;call_panic = 1;
<br>
<br>&#x9;if (context-&#x3E;pwd.dentry &#x26;&#x26; context-&#x3E;pwd.mnt) {
<br>&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_CWD);
<br>&#x9;&#x9;if (ab) {
<br>&#x9;&#x9;&#x9;audit_log_d_path(ab, &#x22; cwd=&#x22;, &#x26;context-&#x3E;pwd);
<br>&#x9;&#x9;&#x9;audit_log_end(ab);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;i = 0;
<br>&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
<br>&#x9;&#x9;if (n-&#x3E;hidden)
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;audit_log_name(context, n, NULL, i++, &#x26;call_panic);
<br>&#x9;}
<br>
<br>&#x9;audit_log_proctitle(tsk, context);
<br>
<br>&#x9;*/ Send end of event record to help user space know we are finished /*
<br>&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_EOE);
<br>&#x9;if (ab)
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;if (call_panic)
<br>&#x9;&#x9;audit_panic(&#x22;error converting sid to string&#x22;);
<br>}
<br>
<br>*/
<br> audit_free - free a per-task audit context
<br> @tsk: task whose audit context block to free
<br>
<br> Called from copy_process and do_exit
<br> /*
<br>void __audit_free(struct task_structtsk)
<br>{
<br>&#x9;struct audit_contextcontext;
<br>
<br>&#x9;context = audit_take_context(tsk, 0, 0);
<br>&#x9;if (!context)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;*/ Check for system calls that do not go through the exit
<br>&#x9; function (e.g., exit_group), then free context block.
<br>&#x9; We use GFP_ATOMIC here because we might be doing this
<br>&#x9; in the context of the idle thread /*
<br>&#x9;*/ that can happen only if we are called from do_exit() /*
<br>&#x9;if (context-&#x3E;in_syscall &#x26;&#x26; context-&#x3E;current_state == AUDIT_RECORD_CONTEXT)
<br>&#x9;&#x9;audit_log_exit(context, tsk);
<br>&#x9;if (!list_empty(&#x26;context-&#x3E;killed_trees))
<br>&#x9;&#x9;audit_kill_trees(&#x26;context-&#x3E;killed_trees);
<br>
<br>&#x9;audit_free_context(context);
<br>}
<br>
<br>*/
<br> audit_syscall_entry - fill in an audit record at syscall entry
<br> @major: major syscall type (function)
<br> @a1: additional syscall register 1
<br> @a2: additional syscall register 2
<br> @a3: additional syscall register 3
<br> @a4: additional syscall register 4
<br>
<br> Fill in audit context at syscall entry.  This only happens if the
<br> audit context was created when the task was created and the state or
<br> filters demand the audit context be built.  If the state from the
<br> per-task filter or from the per-syscall filter is AUDIT_RECORD_CONTEXT,
<br> then the record will be written at syscall exit time (otherwise, it
<br> will only be written if another part of the kernel requests that it
<br> be written).
<br> /*
<br>void __audit_syscall_entry(int major, unsigned long a1, unsigned long a2,
<br>&#x9;&#x9;&#x9;   unsigned long a3, unsigned long a4)
<br>{
<br>&#x9;struct task_structtsk = current;
<br>&#x9;struct audit_contextcontext = tsk-&#x3E;audit_context;
<br>&#x9;enum audit_state     state;
<br>
<br>&#x9;if (!context)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;BUG_ON(context-&#x3E;in_syscall || context-&#x3E;name_count);
<br>
<br>&#x9;if (!audit_enabled)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;context-&#x3E;arch&#x9;    = syscall_get_arch();
<br>&#x9;context-&#x3E;major      = major;
<br>&#x9;context-&#x3E;argv[0]    = a1;
<br>&#x9;context-&#x3E;argv[1]    = a2;
<br>&#x9;context-&#x3E;argv[2]    = a3;
<br>&#x9;context-&#x3E;argv[3]    = a4;
<br>
<br>&#x9;state = context-&#x3E;state;
<br>&#x9;context-&#x3E;dummy = !audit_n_rules;
<br>&#x9;if (!context-&#x3E;dummy &#x26;&#x26; state == AUDIT_BUILD_CONTEXT) {
<br>&#x9;&#x9;context-&#x3E;prio = 0;
<br>&#x9;&#x9;state = audit_filter_syscall(tsk, context, &#x26;audit_filter_list[AUDIT_FILTER_ENTRY]);
<br>&#x9;}
<br>&#x9;if (state == AUDIT_DISABLED)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;context-&#x3E;serial     = 0;
<br>&#x9;context-&#x3E;ctime      = CURRENT_TIME;
<br>&#x9;context-&#x3E;in_syscall = 1;
<br>&#x9;context-&#x3E;current_state  = state;
<br>&#x9;context-&#x3E;ppid       = 0;
<br>}
<br>
<br>*/
<br> audit_syscall_exit - deallocate audit context after a system call
<br> @success: success value of the syscall
<br> @return_code: return value of the syscall
<br>
<br> Tear down after system call.  If the audit context has been marked as
<br> auditable (either because of the AUDIT_RECORD_CONTEXT state from
<br> filtering, or because some other part of the kernel wrote an audit
<br> message), then write out the syscall information.  In call cases,
<br> free the names stored from getname().
<br> /*
<br>void __audit_syscall_exit(int success, long return_code)
<br>{
<br>&#x9;struct task_structtsk = current;
<br>&#x9;struct audit_contextcontext;
<br>
<br>&#x9;if (success)
<br>&#x9;&#x9;success = AUDITSC_SUCCESS;
<br>&#x9;else
<br>&#x9;&#x9;success = AUDITSC_FAILURE;
<br>
<br>&#x9;context = audit_take_context(tsk, success, return_code);
<br>&#x9;if (!context)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;if (context-&#x3E;in_syscall &#x26;&#x26; context-&#x3E;current_state == AUDIT_RECORD_CONTEXT)
<br>&#x9;&#x9;audit_log_exit(context, tsk);
<br>
<br>&#x9;context-&#x3E;in_syscall = 0;
<br>&#x9;context-&#x3E;prio = context-&#x3E;state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;
<br>
<br>&#x9;if (!list_empty(&#x26;context-&#x3E;killed_trees))
<br>&#x9;&#x9;audit_kill_trees(&#x26;context-&#x3E;killed_trees);
<br>
<br>&#x9;audit_free_names(context);
<br>&#x9;unroll_tree_refs(context, NULL, 0);
<br>&#x9;audit_free_aux(context);
<br>&#x9;context-&#x3E;aux = NULL;
<br>&#x9;context-&#x3E;aux_pids = NULL;
<br>&#x9;context-&#x3E;target_pid = 0;
<br>&#x9;context-&#x3E;target_sid = 0;
<br>&#x9;context-&#x3E;sockaddr_len = 0;
<br>&#x9;context-&#x3E;type = 0;
<br>&#x9;context-&#x3E;fds[0] = -1;
<br>&#x9;if (context-&#x3E;state != AUDIT_RECORD_CONTEXT) {
<br>&#x9;&#x9;kfree(context-&#x3E;filterkey);
<br>&#x9;&#x9;context-&#x3E;filterkey = NULL;
<br>&#x9;}
<br>&#x9;tsk-&#x3E;audit_context = context;
<br>}
<br>
<br>static inline void handle_one(const struct inodeinode)
<br>{
<br>#ifdef CONFIG_AUDIT_TREE
<br>&#x9;struct audit_contextcontext;
<br>&#x9;struct audit_tree_refsp;
<br>&#x9;struct audit_chunkchunk;
<br>&#x9;int count;
<br>&#x9;if (likely(hlist_empty(&#x26;inode-&#x3E;i_fsnotify_marks)))
<br>&#x9;&#x9;return;
<br>&#x9;context = current-&#x3E;audit_context;
<br>&#x9;p = context-&#x3E;trees;
<br>&#x9;count = context-&#x3E;tree_count;
<br>&#x9;rcu_read_lock();
<br>&#x9;chunk = audit_tree_lookup(inode);
<br>&#x9;rcu_read_unlock();
<br>&#x9;if (!chunk)
<br>&#x9;&#x9;return;
<br>&#x9;if (likely(put_tree_ref(context, chunk)))
<br>&#x9;&#x9;return;
<br>&#x9;if (unlikely(!grow_tree_refs(context))) {
<br>&#x9;&#x9;pr_warn(&#x22;out of memory, audit has lost a tree reference\n&#x22;);
<br>&#x9;&#x9;audit_set_auditable(context);
<br>&#x9;&#x9;audit_put_chunk(chunk);
<br>&#x9;&#x9;unroll_tree_refs(context, p, count);
<br>&#x9;&#x9;return;
<br>&#x9;}
<br>&#x9;put_tree_ref(context, chunk);
<br>#endif
<br>}
<br>
<br>static void handle_path(const struct dentrydentry)
<br>{
<br>#ifdef CONFIG_AUDIT_TREE
<br>&#x9;struct audit_contextcontext;
<br>&#x9;struct audit_tree_refsp;
<br>&#x9;const struct dentryd,parent;
<br>&#x9;struct audit_chunkdrop;
<br>&#x9;unsigned long seq;
<br>&#x9;int count;
<br>
<br>&#x9;context = current-&#x3E;audit_context;
<br>&#x9;p = context-&#x3E;trees;
<br>&#x9;count = context-&#x3E;tree_count;
<br>retry:
<br>&#x9;drop = NULL;
<br>&#x9;d = dentry;
<br>&#x9;rcu_read_lock();
<br>&#x9;seq = read_seqbegin(&#x26;rename_lock);
<br>&#x9;for(;;) {
<br>&#x9;&#x9;struct inodeinode = d_backing_inode(d);
<br>&#x9;&#x9;if (inode &#x26;&#x26; unlikely(!hlist_empty(&#x26;inode-&#x3E;i_fsnotify_marks))) {
<br>&#x9;&#x9;&#x9;struct audit_chunkchunk;
<br>&#x9;&#x9;&#x9;chunk = audit_tree_lookup(inode);
<br>&#x9;&#x9;&#x9;if (chunk) {
<br>&#x9;&#x9;&#x9;&#x9;if (unlikely(!put_tree_ref(context, chunk))) {
<br>&#x9;&#x9;&#x9;&#x9;&#x9;drop = chunk;
<br>&#x9;&#x9;&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;}
<br>&#x9;&#x9;parent = d-&#x3E;d_parent;
<br>&#x9;&#x9;if (parent == d)
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;d = parent;
<br>&#x9;}
<br>&#x9;if (unlikely(read_seqretry(&#x26;rename_lock, seq) || drop)) { / in this order /*
<br>&#x9;&#x9;rcu_read_unlock();
<br>&#x9;&#x9;if (!drop) {
<br>&#x9;&#x9;&#x9;*/ just a race with rename /*
<br>&#x9;&#x9;&#x9;unroll_tree_refs(context, p, count);
<br>&#x9;&#x9;&#x9;goto retry;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;audit_put_chunk(drop);
<br>&#x9;&#x9;if (grow_tree_refs(context)) {
<br>&#x9;&#x9;&#x9;*/ OK, got more space /*
<br>&#x9;&#x9;&#x9;unroll_tree_refs(context, p, count);
<br>&#x9;&#x9;&#x9;goto retry;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;*/ too bad /*
<br>&#x9;&#x9;pr_warn(&#x22;out of memory, audit has lost a tree reference\n&#x22;);
<br>&#x9;&#x9;unroll_tree_refs(context, p, count);
<br>&#x9;&#x9;audit_set_auditable(context);
<br>&#x9;&#x9;return;
<br>&#x9;}
<br>&#x9;rcu_read_unlock();
<br>#endif
<br>}
<br>
<br>static struct audit_namesaudit_alloc_name(struct audit_contextcontext,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;unsigned char type)
<br>{
<br>&#x9;struct audit_namesaname;
<br>
<br>&#x9;if (context-&#x3E;name_count &#x3C; AUDIT_NAMES) {
<br>&#x9;&#x9;aname = &#x26;context-&#x3E;preallocated_names[context-&#x3E;name_count];
<br>&#x9;&#x9;memset(aname, 0, sizeof(*aname));
<br>&#x9;} else {
<br>&#x9;&#x9;aname = kzalloc(sizeof(*aname), GFP_NOFS);
<br>&#x9;&#x9;if (!aname)
<br>&#x9;&#x9;&#x9;return NULL;
<br>&#x9;&#x9;aname-&#x3E;should_free = true;
<br>&#x9;}
<br>
<br>&#x9;aname-&#x3E;ino = AUDIT_INO_UNSET;
<br>&#x9;aname-&#x3E;type = type;
<br>&#x9;list_add_tail(&#x26;aname-&#x3E;list, &#x26;context-&#x3E;names_list);
<br>
<br>&#x9;context-&#x3E;name_count++;
<br>&#x9;return aname;
<br>}
<br>
<br>*/
<br> audit_reusename - fill out filename with info from existing entry
<br> @uptr: userland ptr to pathname
<br>
<br> Search the audit_names list for the current audit context. If there is an
<br> existing entry with a matching &#x22;uptr&#x22; then return the filename
<br> associated with that audit_name. If not, return NULL.
<br> /*
<br>struct filename
<br>__audit_reusename(const __user charuptr)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;struct audit_namesn;
<br>
<br>&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
<br>&#x9;&#x9;if (!n-&#x3E;name)
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;if (n-&#x3E;name-&#x3E;uptr == uptr) {
<br>&#x9;&#x9;&#x9;n-&#x3E;name-&#x3E;refcnt++;
<br>&#x9;&#x9;&#x9;return n-&#x3E;name;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;return NULL;
<br>}
<br>
<br>*/
<br> audit_getname - add a name to the list
<br> @name: name to add
<br>
<br> Add a name to the list of audit names for this context.
<br> Called from fs/namei.c:getname().
<br> /*
<br>void __audit_getname(struct filenamename)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;struct audit_namesn;
<br>
<br>&#x9;if (!context-&#x3E;in_syscall)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
<br>&#x9;if (!n)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;n-&#x3E;name = name;
<br>&#x9;n-&#x3E;name_len = AUDIT_NAME_FULL;
<br>&#x9;name-&#x3E;aname = n;
<br>&#x9;name-&#x3E;refcnt++;
<br>
<br>&#x9;if (!context-&#x3E;pwd.dentry)
<br>&#x9;&#x9;get_fs_pwd(current-&#x3E;fs, &#x26;context-&#x3E;pwd);
<br>}
<br>
<br>*/
<br> __audit_inode - store the inode and device from a lookup
<br> @name: name being audited
<br> @dentry: dentry being audited
<br> @flags: attributes for this particular entry
<br> /*
<br>void __audit_inode(struct filenamename, const struct dentrydentry,
<br>&#x9;&#x9;   unsigned int flags)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;struct inodeinode = d_backing_inode(dentry);
<br>&#x9;struct audit_namesn;
<br>&#x9;bool parent = flags &#x26; AUDIT_INODE_PARENT;
<br>
<br>&#x9;if (!context-&#x3E;in_syscall)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;if (!name)
<br>&#x9;&#x9;goto out_alloc;
<br>
<br>&#x9;*/
<br>&#x9; If we have a pointer to an audit_names entry already, then we can
<br>&#x9; just use it directly if the type is correct.
<br>&#x9; /*
<br>&#x9;n = name-&#x3E;aname;
<br>&#x9;if (n) {
<br>&#x9;&#x9;if (parent) {
<br>&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_PARENT ||
<br>&#x9;&#x9;&#x9;    n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
<br>&#x9;&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;if (n-&#x3E;type != AUDIT_TYPE_PARENT)
<br>&#x9;&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;list_for_each_entry_reverse(n, &#x26;context-&#x3E;names_list, list) {
<br>&#x9;&#x9;if (n-&#x3E;ino) {
<br>&#x9;&#x9;&#x9;*/ valid inode number, use that for the comparison /*
<br>&#x9;&#x9;&#x9;if (n-&#x3E;ino != inode-&#x3E;i_ino ||
<br>&#x9;&#x9;&#x9;    n-&#x3E;dev != inode-&#x3E;i_sb-&#x3E;s_dev)
<br>&#x9;&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;} else if (n-&#x3E;name) {
<br>&#x9;&#x9;&#x9;*/ inode number has not been set, check the name /*
<br>&#x9;&#x9;&#x9;if (strcmp(n-&#x3E;name-&#x3E;name, name-&#x3E;name))
<br>&#x9;&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;} else
<br>&#x9;&#x9;&#x9;*/ no inode and no name (?!) ... this is odd ... /*
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;*/ match the correct record type /*
<br>&#x9;&#x9;if (parent) {
<br>&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_PARENT ||
<br>&#x9;&#x9;&#x9;    n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
<br>&#x9;&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;if (n-&#x3E;type != AUDIT_TYPE_PARENT)
<br>&#x9;&#x9;&#x9;&#x9;goto out;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>out_alloc:
<br>&#x9;*/ unable to find an entry with both a matching name and type /*
<br>&#x9;n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
<br>&#x9;if (!n)
<br>&#x9;&#x9;return;
<br>&#x9;if (name) {
<br>&#x9;&#x9;n-&#x3E;name = name;
<br>&#x9;&#x9;name-&#x3E;refcnt++;
<br>&#x9;}
<br>
<br>out:
<br>&#x9;if (parent) {
<br>&#x9;&#x9;n-&#x3E;name_len = n-&#x3E;name ? parent_len(n-&#x3E;name-&#x3E;name) : AUDIT_NAME_FULL;
<br>&#x9;&#x9;n-&#x3E;type = AUDIT_TYPE_PARENT;
<br>&#x9;&#x9;if (flags &#x26; AUDIT_INODE_HIDDEN)
<br>&#x9;&#x9;&#x9;n-&#x3E;hidden = true;
<br>&#x9;} else {
<br>&#x9;&#x9;n-&#x3E;name_len = AUDIT_NAME_FULL;
<br>&#x9;&#x9;n-&#x3E;type = AUDIT_TYPE_NORMAL;
<br>&#x9;}
<br>&#x9;handle_path(dentry);
<br>&#x9;audit_copy_inode(n, dentry, inode);
<br>}
<br>
<br>void __audit_file(const struct filefile)
<br>{
<br>&#x9;__audit_inode(NULL, file-&#x3E;f_path.dentry, 0);
<br>}
<br>
<br>*/
<br> __audit_inode_child - collect inode info for created/removed objects
<br> @parent: inode of dentry parent
<br> @dentry: dentry being audited
<br> @type:   AUDIT_TYPE_* value that we&#x27;re looking for
<br>
<br> For syscalls that create or remove filesystem objects, audit_inode
<br> can only collect information for the filesystem object&#x27;s parent.
<br> This call updates the audit context with the child&#x27;s information.
<br> Syscalls that create a new filesystem object must be hooked after
<br> the object is created.  Syscalls that remove a filesystem object
<br> must be hooked prior, in order to capture the target inode during
<br> unsuccessful attempts.
<br> /*
<br>void __audit_inode_child(struct inodeparent,
<br>&#x9;&#x9;&#x9; const struct dentrydentry,
<br>&#x9;&#x9;&#x9; const unsigned char type)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;struct inodeinode = d_backing_inode(dentry);
<br>&#x9;const chardname = dentry-&#x3E;d_name.name;
<br>&#x9;struct audit_namesn,found_parent = NULL,found_child = NULL;
<br>
<br>&#x9;if (!context-&#x3E;in_syscall)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;if (inode)
<br>&#x9;&#x9;handle_one(inode);
<br>
<br>&#x9;*/ look for a parent entry first /*
<br>&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
<br>&#x9;&#x9;if (!n-&#x3E;name ||
<br>&#x9;&#x9;    (n-&#x3E;type != AUDIT_TYPE_PARENT &#x26;&#x26;
<br>&#x9;&#x9;     n-&#x3E;type != AUDIT_TYPE_UNKNOWN))
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;if (n-&#x3E;ino == parent-&#x3E;i_ino &#x26;&#x26; n-&#x3E;dev == parent-&#x3E;i_sb-&#x3E;s_dev &#x26;&#x26;
<br>&#x9;&#x9;    !audit_compare_dname_path(dname,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;      n-&#x3E;name-&#x3E;name, n-&#x3E;name_len)) {
<br>&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
<br>&#x9;&#x9;&#x9;&#x9;n-&#x3E;type = AUDIT_TYPE_PARENT;
<br>&#x9;&#x9;&#x9;found_parent = n;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;*/ is there a matching child entry? /*
<br>&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
<br>&#x9;&#x9;*/ can only match entries that have a name /*
<br>&#x9;&#x9;if (!n-&#x3E;name ||
<br>&#x9;&#x9;    (n-&#x3E;type != type &#x26;&#x26; n-&#x3E;type != AUDIT_TYPE_UNKNOWN))
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;if (!strcmp(dname, n-&#x3E;name-&#x3E;name) ||
<br>&#x9;&#x9;    !audit_compare_dname_path(dname, n-&#x3E;name-&#x3E;name,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;found_parent ?
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;found_parent-&#x3E;name_len :
<br>&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;AUDIT_NAME_FULL)) {
<br>&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
<br>&#x9;&#x9;&#x9;&#x9;n-&#x3E;type = type;
<br>&#x9;&#x9;&#x9;found_child = n;
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;if (!found_parent) {
<br>&#x9;&#x9;*/ create a new, &#x22;anonymous&#x22; parent record /*
<br>&#x9;&#x9;n = audit_alloc_name(context, AUDIT_TYPE_PARENT);
<br>&#x9;&#x9;if (!n)
<br>&#x9;&#x9;&#x9;return;
<br>&#x9;&#x9;audit_copy_inode(n, NULL, parent);
<br>&#x9;}
<br>
<br>&#x9;if (!found_child) {
<br>&#x9;&#x9;found_child = audit_alloc_name(context, type);
<br>&#x9;&#x9;if (!found_child)
<br>&#x9;&#x9;&#x9;return;
<br>
<br>&#x9;&#x9;*/ Re-use the name belonging to the slot for a matching parent
<br>&#x9;&#x9; directory. All names for this context are relinquished in
<br>&#x9;&#x9; audit_free_names() /*
<br>&#x9;&#x9;if (found_parent) {
<br>&#x9;&#x9;&#x9;found_child-&#x3E;name = found_parent-&#x3E;name;
<br>&#x9;&#x9;&#x9;found_child-&#x3E;name_len = AUDIT_NAME_FULL;
<br>&#x9;&#x9;&#x9;found_child-&#x3E;name-&#x3E;refcnt++;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;if (inode)
<br>&#x9;&#x9;audit_copy_inode(found_child, dentry, inode);
<br>&#x9;else
<br>&#x9;&#x9;found_child-&#x3E;ino = AUDIT_INO_UNSET;
<br>}
<br>EXPORT_SYMBOL_GPL(__audit_inode_child);
<br>
<br>*/
<br> auditsc_get_stamp - get local copies of audit_context values
<br> @ctx: audit_context for the task
<br> @t: timespec to store time recorded in the audit_context
<br> @serial: serial value that is recorded in the audit_context
<br>
<br> Also sets the context as auditable.
<br> /*
<br>int auditsc_get_stamp(struct audit_contextctx,
<br>&#x9;&#x9;       struct timespect, unsigned intserial)
<br>{
<br>&#x9;if (!ctx-&#x3E;in_syscall)
<br>&#x9;&#x9;return 0;
<br>&#x9;if (!ctx-&#x3E;serial)
<br>&#x9;&#x9;ctx-&#x3E;serial = audit_serial();
<br>&#x9;t-&#x3E;tv_sec  = ctx-&#x3E;ctime.tv_sec;
<br>&#x9;t-&#x3E;tv_nsec = ctx-&#x3E;ctime.tv_nsec;
<br>&#x9;*serial    = ctx-&#x3E;serial;
<br>&#x9;if (!ctx-&#x3E;prio) {
<br>&#x9;&#x9;ctx-&#x3E;prio = 1;
<br>&#x9;&#x9;ctx-&#x3E;current_state = AUDIT_RECORD_CONTEXT;
<br>&#x9;}
<br>&#x9;return 1;
<br>}
<br>
<br>*/ global counter which is incremented every time something logs in /*
<br>static atomic_t session_id = ATOMIC_INIT(0);
<br>
<br>static int audit_set_loginuid_perm(kuid_t loginuid)
<br>{
<br>&#x9;*/ if we are unset, we don&#x27;t need privs /*
<br>&#x9;if (!audit_loginuid_set(current))
<br>&#x9;&#x9;return 0;
<br>&#x9;*/ if AUDIT_FEATURE_LOGINUID_IMMUTABLE means never ever allow a change/*
<br>&#x9;if (is_audit_feature_set(AUDIT_FEATURE_LOGINUID_IMMUTABLE))
<br>&#x9;&#x9;return -EPERM;
<br>&#x9;*/ it is set, you need permission /*
<br>&#x9;if (!capable(CAP_AUDIT_CONTROL))
<br>&#x9;&#x9;return -EPERM;
<br>&#x9;*/ reject if this is not an unset and we don&#x27;t allow that /*
<br>&#x9;if (is_audit_feature_set(AUDIT_FEATURE_ONLY_UNSET_LOGINUID) &#x26;&#x26; uid_valid(loginuid))
<br>&#x9;&#x9;return -EPERM;
<br>&#x9;return 0;
<br>}
<br>
<br>static void audit_log_set_loginuid(kuid_t koldloginuid, kuid_t kloginuid,
<br>&#x9;&#x9;&#x9;&#x9;   unsigned int oldsessionid, unsigned int sessionid,
<br>&#x9;&#x9;&#x9;&#x9;   int rc)
<br>{
<br>&#x9;struct audit_bufferab;
<br>&#x9;uid_t uid, oldloginuid, loginuid;
<br>
<br>&#x9;if (!audit_enabled)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;uid = from_kuid(&#x26;init_user_ns, task_uid(current));
<br>&#x9;oldloginuid = from_kuid(&#x26;init_user_ns, koldloginuid);
<br>&#x9;loginuid = from_kuid(&#x26;init_user_ns, kloginuid),
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_LOGIN);
<br>&#x9;if (!ab)
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_format(ab, &#x22;pid=%d uid=%u&#x22;, task_pid_nr(current), uid);
<br>&#x9;audit_log_task_context(ab);
<br>&#x9;audit_log_format(ab, &#x22; old-auid=%u auid=%u old-ses=%u ses=%u res=%d&#x22;,
<br>&#x9;&#x9;&#x9; oldloginuid, loginuid, oldsessionid, sessionid, !rc);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>*/
<br> audit_set_loginuid - set current task&#x27;s audit_context loginuid
<br> @loginuid: loginuid value
<br>
<br> Returns 0.
<br>
<br> Called (set) from fs/proc/base.c::proc_loginuid_write().
<br> /*
<br>int audit_set_loginuid(kuid_t loginuid)
<br>{
<br>&#x9;struct task_structtask = current;
<br>&#x9;unsigned int oldsessionid, sessionid = (unsigned int)-1;
<br>&#x9;kuid_t oldloginuid;
<br>&#x9;int rc;
<br>
<br>&#x9;oldloginuid = audit_get_loginuid(current);
<br>&#x9;oldsessionid = audit_get_sessionid(current);
<br>
<br>&#x9;rc = audit_set_loginuid_perm(loginuid);
<br>&#x9;if (rc)
<br>&#x9;&#x9;goto out;
<br>
<br>&#x9;*/ are we setting or clearing? /*
<br>&#x9;if (uid_valid(loginuid))
<br>&#x9;&#x9;sessionid = (unsigned int)atomic_inc_return(&#x26;session_id);
<br>
<br>&#x9;task-&#x3E;sessionid = sessionid;
<br>&#x9;task-&#x3E;loginuid = loginuid;
<br>out:
<br>&#x9;audit_log_set_loginuid(oldloginuid, loginuid, oldsessionid, sessionid, rc);
<br>&#x9;return rc;
<br>}
<br>
<br>*/
<br> __audit_mq_open - record audit data for a POSIX MQ open
<br> @oflag: open flag
<br> @mode: mode bits
<br> @attr: queue attributes
<br>
<br> /*
<br>void __audit_mq_open(int oflag, umode_t mode, struct mq_attrattr)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;if (attr)
<br>&#x9;&#x9;memcpy(&#x26;context-&#x3E;mq_open.attr, attr, sizeof(struct mq_attr));
<br>&#x9;else
<br>&#x9;&#x9;memset(&#x26;context-&#x3E;mq_open.attr, 0, sizeof(struct mq_attr));
<br>
<br>&#x9;context-&#x3E;mq_open.oflag = oflag;
<br>&#x9;context-&#x3E;mq_open.mode = mode;
<br>
<br>&#x9;context-&#x3E;type = AUDIT_MQ_OPEN;
<br>}
<br>
<br>*/
<br> __audit_mq_sendrecv - record audit data for a POSIX MQ timed send/receive
<br> @mqdes: MQ descriptor
<br> @msg_len: Message length
<br> @msg_prio: Message priority
<br> @abs_timeout: Message timeout in absolute time
<br>
<br> /*
<br>void __audit_mq_sendrecv(mqd_t mqdes, size_t msg_len, unsigned int msg_prio,
<br>&#x9;&#x9;&#x9;const struct timespecabs_timeout)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;struct timespecp = &#x26;context-&#x3E;mq_sendrecv.abs_timeout;
<br>
<br>&#x9;if (abs_timeout)
<br>&#x9;&#x9;memcpy(p, abs_timeout, sizeof(struct timespec));
<br>&#x9;else
<br>&#x9;&#x9;memset(p, 0, sizeof(struct timespec));
<br>
<br>&#x9;context-&#x3E;mq_sendrecv.mqdes = mqdes;
<br>&#x9;context-&#x3E;mq_sendrecv.msg_len = msg_len;
<br>&#x9;context-&#x3E;mq_sendrecv.msg_prio = msg_prio;
<br>
<br>&#x9;context-&#x3E;type = AUDIT_MQ_SENDRECV;
<br>}
<br>
<br>*/
<br> __audit_mq_notify - record audit data for a POSIX MQ notify
<br> @mqdes: MQ descriptor
<br> @notification: Notification event
<br>
<br> /*
<br>
<br>void __audit_mq_notify(mqd_t mqdes, const struct sigeventnotification)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;if (notification)
<br>&#x9;&#x9;context-&#x3E;mq_notify.sigev_signo = notification-&#x3E;sigev_signo;
<br>&#x9;else
<br>&#x9;&#x9;context-&#x3E;mq_notify.sigev_signo = 0;
<br>
<br>&#x9;context-&#x3E;mq_notify.mqdes = mqdes;
<br>&#x9;context-&#x3E;type = AUDIT_MQ_NOTIFY;
<br>}
<br>
<br>*/
<br> __audit_mq_getsetattr - record audit data for a POSIX MQ get/set attribute
<br> @mqdes: MQ descriptor
<br> @mqstat: MQ flags
<br>
<br> /*
<br>void __audit_mq_getsetattr(mqd_t mqdes, struct mq_attrmqstat)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;context-&#x3E;mq_getsetattr.mqdes = mqdes;
<br>&#x9;context-&#x3E;mq_getsetattr.mqstat =mqstat;
<br>&#x9;context-&#x3E;type = AUDIT_MQ_GETSETATTR;
<br>}
<br>
<br>*/
<br> audit_ipc_obj - record audit data for ipc object
<br> @ipcp: ipc permissions
<br>
<br> /*
<br>void __audit_ipc_obj(struct kern_ipc_permipcp)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;context-&#x3E;ipc.uid = ipcp-&#x3E;uid;
<br>&#x9;context-&#x3E;ipc.gid = ipcp-&#x3E;gid;
<br>&#x9;context-&#x3E;ipc.mode = ipcp-&#x3E;mode;
<br>&#x9;context-&#x3E;ipc.has_perm = 0;
<br>&#x9;security_ipc_getsecid(ipcp, &#x26;context-&#x3E;ipc.osid);
<br>&#x9;context-&#x3E;type = AUDIT_IPC;
<br>}
<br>
<br>*/
<br> audit_ipc_set_perm - record audit data for new ipc permissions
<br> @qbytes: msgq bytes
<br> @uid: msgq user id
<br> @gid: msgq group id
<br> @mode: msgq mode (permissions)
<br>
<br> Called only after audit_ipc_obj().
<br> /*
<br>void __audit_ipc_set_perm(unsigned long qbytes, uid_t uid, gid_t gid, umode_t mode)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;context-&#x3E;ipc.qbytes = qbytes;
<br>&#x9;context-&#x3E;ipc.perm_uid = uid;
<br>&#x9;context-&#x3E;ipc.perm_gid = gid;
<br>&#x9;context-&#x3E;ipc.perm_mode = mode;
<br>&#x9;context-&#x3E;ipc.has_perm = 1;
<br>}
<br>
<br>void __audit_bprm(struct linux_binprmbprm)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;context-&#x3E;type = AUDIT_EXECVE;
<br>&#x9;context-&#x3E;execve.argc = bprm-&#x3E;argc;
<br>}
<br>
<br>
<br>*/
<br> audit_socketcall - record audit data for sys_socketcall
<br> @nargs: number of args, which should not be more than AUDITSC_ARGS.
<br> @args: args array
<br>
<br> /*
<br>int __audit_socketcall(int nargs, unsigned longargs)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;if (nargs &#x3C;= 0 || nargs &#x3E; AUDITSC_ARGS || !args)
<br>&#x9;&#x9;return -EINVAL;
<br>&#x9;context-&#x3E;type = AUDIT_SOCKETCALL;
<br>&#x9;context-&#x3E;socketcall.nargs = nargs;
<br>&#x9;memcpy(context-&#x3E;socketcall.args, args, nargs sizeof(unsigned long));
<br>&#x9;return 0;
<br>}
<br>
<br>*/
<br> __audit_fd_pair - record audit data for pipe and socketpair
<br> @fd1: the first file descriptor
<br> @fd2: the second file descriptor
<br>
<br> /*
<br>void __audit_fd_pair(int fd1, int fd2)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;context-&#x3E;fds[0] = fd1;
<br>&#x9;context-&#x3E;fds[1] = fd2;
<br>}
<br>
<br>*/
<br> audit_sockaddr - record audit data for sys_bind, sys_connect, sys_sendto
<br> @len: data length in user space
<br> @a: data address in kernel space
<br>
<br> Returns 0 for success or NULL context or &#x3C; 0 on error.
<br> /*
<br>int __audit_sockaddr(int len, voida)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;if (!context-&#x3E;sockaddr) {
<br>&#x9;&#x9;voidp = kmalloc(sizeof(struct sockaddr_storage), GFP_KERNEL);
<br>&#x9;&#x9;if (!p)
<br>&#x9;&#x9;&#x9;return -ENOMEM;
<br>&#x9;&#x9;context-&#x3E;sockaddr = p;
<br>&#x9;}
<br>
<br>&#x9;context-&#x3E;sockaddr_len = len;
<br>&#x9;memcpy(context-&#x3E;sockaddr, a, len);
<br>&#x9;return 0;
<br>}
<br>
<br>void __audit_ptrace(struct task_structt)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>
<br>&#x9;context-&#x3E;target_pid = task_pid_nr(t);
<br>&#x9;context-&#x3E;target_auid = audit_get_loginuid(t);
<br>&#x9;context-&#x3E;target_uid = task_uid(t);
<br>&#x9;context-&#x3E;target_sessionid = audit_get_sessionid(t);
<br>&#x9;security_task_getsecid(t, &#x26;context-&#x3E;target_sid);
<br>&#x9;memcpy(context-&#x3E;target_comm, t-&#x3E;comm, TASK_COMM_LEN);
<br>}
<br>
<br>*/
<br> audit_signal_info - record signal info for shutting down audit subsystem
<br> @sig: signal value
<br> @t: task being signaled
<br>
<br> If the audit subsystem is being terminated, record the task (pid)
<br> and uid that is doing that.
<br> /*
<br>int __audit_signal_info(int sig, struct task_structt)
<br>{
<br>&#x9;struct audit_aux_data_pidsaxp;
<br>&#x9;struct task_structtsk = current;
<br>&#x9;struct audit_contextctx = tsk-&#x3E;audit_context;
<br>&#x9;kuid_t uid = current_uid(), t_uid = task_uid(t);
<br>
<br>&#x9;if (audit_pid &#x26;&#x26; t-&#x3E;tgid == audit_pid) {
<br>&#x9;&#x9;if (sig == SIGTERM || sig == SIGHUP || sig == SIGUSR1 || sig == SIGUSR2) {
<br>&#x9;&#x9;&#x9;audit_sig_pid = task_pid_nr(tsk);
<br>&#x9;&#x9;&#x9;if (uid_valid(tsk-&#x3E;loginuid))
<br>&#x9;&#x9;&#x9;&#x9;audit_sig_uid = tsk-&#x3E;loginuid;
<br>&#x9;&#x9;&#x9;else
<br>&#x9;&#x9;&#x9;&#x9;audit_sig_uid = uid;
<br>&#x9;&#x9;&#x9;security_task_getsecid(tsk, &#x26;audit_sig_sid);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;if (!audit_signals || audit_dummy_context())
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;}
<br>
<br>&#x9;*/ optimize the common case by putting first signal recipient directly
<br>&#x9; in audit_context /*
<br>&#x9;if (!ctx-&#x3E;target_pid) {
<br>&#x9;&#x9;ctx-&#x3E;target_pid = task_tgid_nr(t);
<br>&#x9;&#x9;ctx-&#x3E;target_auid = audit_get_loginuid(t);
<br>&#x9;&#x9;ctx-&#x3E;target_uid = t_uid;
<br>&#x9;&#x9;ctx-&#x3E;target_sessionid = audit_get_sessionid(t);
<br>&#x9;&#x9;security_task_getsecid(t, &#x26;ctx-&#x3E;target_sid);
<br>&#x9;&#x9;memcpy(ctx-&#x3E;target_comm, t-&#x3E;comm, TASK_COMM_LEN);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>
<br>&#x9;axp = (void)ctx-&#x3E;aux_pids;
<br>&#x9;if (!axp || axp-&#x3E;pid_count == AUDIT_AUX_PIDS) {
<br>&#x9;&#x9;axp = kzalloc(sizeof(*axp), GFP_ATOMIC);
<br>&#x9;&#x9;if (!axp)
<br>&#x9;&#x9;&#x9;return -ENOMEM;
<br>
<br>&#x9;&#x9;axp-&#x3E;d.type = AUDIT_OBJ_PID;
<br>&#x9;&#x9;axp-&#x3E;d.next = ctx-&#x3E;aux_pids;
<br>&#x9;&#x9;ctx-&#x3E;aux_pids = (void)axp;
<br>&#x9;}
<br>&#x9;BUG_ON(axp-&#x3E;pid_count &#x3E;= AUDIT_AUX_PIDS);
<br>
<br>&#x9;axp-&#x3E;target_pid[axp-&#x3E;pid_count] = task_tgid_nr(t);
<br>&#x9;axp-&#x3E;target_auid[axp-&#x3E;pid_count] = audit_get_loginuid(t);
<br>&#x9;axp-&#x3E;target_uid[axp-&#x3E;pid_count] = t_uid;
<br>&#x9;axp-&#x3E;target_sessionid[axp-&#x3E;pid_count] = audit_get_sessionid(t);
<br>&#x9;security_task_getsecid(t, &#x26;axp-&#x3E;target_sid[axp-&#x3E;pid_count]);
<br>&#x9;memcpy(axp-&#x3E;target_comm[axp-&#x3E;pid_count], t-&#x3E;comm, TASK_COMM_LEN);
<br>&#x9;axp-&#x3E;pid_count++;
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>*/
<br> __audit_log_bprm_fcaps - store information about a loading bprm and relevant fcaps
<br> @bprm: pointer to the bprm being processed
<br> @new: the proposed new credentials
<br> @old: the old credentials
<br>
<br> Simply check if the proc already has the caps given by the file and if not
<br> store the priv escalation info for later auditing at the end of the syscall
<br>
<br> -Eric
<br> /*
<br>int __audit_log_bprm_fcaps(struct linux_binprmbprm,
<br>&#x9;&#x9;&#x9;   const struct crednew, const struct credold)
<br>{
<br>&#x9;struct audit_aux_data_bprm_fcapsax;
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;struct cpu_vfs_cap_data vcaps;
<br>
<br>&#x9;ax = kmalloc(sizeof(*ax), GFP_KERNEL);
<br>&#x9;if (!ax)
<br>&#x9;&#x9;return -ENOMEM;
<br>
<br>&#x9;ax-&#x3E;d.type = AUDIT_BPRM_FCAPS;
<br>&#x9;ax-&#x3E;d.next = context-&#x3E;aux;
<br>&#x9;context-&#x3E;aux = (void)ax;
<br>
<br>&#x9;get_vfs_caps_from_disk(bprm-&#x3E;file-&#x3E;f_path.dentry, &#x26;vcaps);
<br>
<br>&#x9;ax-&#x3E;fcap.permitted = vcaps.permitted;
<br>&#x9;ax-&#x3E;fcap.inheritable = vcaps.inheritable;
<br>&#x9;ax-&#x3E;fcap.fE = !!(vcaps.magic_etc &#x26; VFS_CAP_FLAGS_EFFECTIVE);
<br>&#x9;ax-&#x3E;fcap_ver = (vcaps.magic_etc &#x26; VFS_CAP_REVISION_MASK) &#x3E;&#x3E; VFS_CAP_REVISION_SHIFT;
<br>
<br>&#x9;ax-&#x3E;old_pcap.permitted   = old-&#x3E;cap_permitted;
<br>&#x9;ax-&#x3E;old_pcap.inheritable = old-&#x3E;cap_inheritable;
<br>&#x9;ax-&#x3E;old_pcap.effective   = old-&#x3E;cap_effective;
<br>
<br>&#x9;ax-&#x3E;new_pcap.permitted   = new-&#x3E;cap_permitted;
<br>&#x9;ax-&#x3E;new_pcap.inheritable = new-&#x3E;cap_inheritable;
<br>&#x9;ax-&#x3E;new_pcap.effective   = new-&#x3E;cap_effective;
<br>&#x9;return 0;
<br>}
<br>
<br>*/
<br> __audit_log_capset - store information about the arguments to the capset syscall
<br> @new: the new credentials
<br> @old: the old (current) credentials
<br>
<br> Record the arguments userspace sent to sys_capset for later printing by the
<br> audit system if applicable
<br> /*
<br>void __audit_log_capset(const struct crednew, const struct credold)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;context-&#x3E;capset.pid = task_pid_nr(current);
<br>&#x9;context-&#x3E;capset.cap.effective   = new-&#x3E;cap_effective;
<br>&#x9;context-&#x3E;capset.cap.inheritable = new-&#x3E;cap_effective;
<br>&#x9;context-&#x3E;capset.cap.permitted   = new-&#x3E;cap_permitted;
<br>&#x9;context-&#x3E;type = AUDIT_CAPSET;
<br>}
<br>
<br>void __audit_mmap_fd(int fd, int flags)
<br>{
<br>&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
<br>&#x9;context-&#x3E;mmap.fd = fd;
<br>&#x9;context-&#x3E;mmap.flags = flags;
<br>&#x9;context-&#x3E;type = AUDIT_MMAP;
<br>}
<br>
<br>static void audit_log_task(struct audit_bufferab)
<br>{
<br>&#x9;kuid_t auid, uid;
<br>&#x9;kgid_t gid;
<br>&#x9;unsigned int sessionid;
<br>&#x9;char comm[sizeof(current-&#x3E;comm)];
<br>
<br>&#x9;auid = audit_get_loginuid(current);
<br>&#x9;sessionid = audit_get_sessionid(current);
<br>&#x9;current_uid_gid(&#x26;uid, &#x26;gid);
<br>
<br>&#x9;audit_log_format(ab, &#x22;auid=%u uid=%u gid=%u ses=%u&#x22;,
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, auid),
<br>&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, uid),
<br>&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, gid),
<br>&#x9;&#x9;&#x9; sessionid);
<br>&#x9;audit_log_task_context(ab);
<br>&#x9;audit_log_format(ab, &#x22; pid=%d comm=&#x22;, task_pid_nr(current));
<br>&#x9;audit_log_untrustedstring(ab, get_task_comm(comm, current));
<br>&#x9;audit_log_d_path_exe(ab, current-&#x3E;mm);
<br>}
<br>
<br>*/
<br> audit_core_dumps - record information about processes that end abnormally
<br> @signr: signal value
<br>
<br> If a process ends with a core dump, something fishy is going on and we
<br> should record the event for investigation.
<br> /*
<br>void audit_core_dumps(long signr)
<br>{
<br>&#x9;struct audit_bufferab;
<br>
<br>&#x9;if (!audit_enabled)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;if (signr == SIGQUIT)&#x9;*/ don&#x27;t care for those /*
<br>&#x9;&#x9;return;
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_ANOM_ABEND);
<br>&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_task(ab);
<br>&#x9;audit_log_format(ab, &#x22; sig=%ld&#x22;, signr);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>void __audit_seccomp(unsigned long syscall, long signr, int code)
<br>{
<br>&#x9;struct audit_bufferab;
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_SECCOMP);
<br>&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_task(ab);
<br>&#x9;audit_log_format(ab, &#x22; sig=%ld arch=%x syscall=%ld compat=%d ip=0x%lx code=0x%x&#x22;,
<br>&#x9;&#x9;&#x9; signr, syscall_get_arch(), syscall,
<br>&#x9;&#x9;&#x9; in_compat_syscall(), KSTK_EIP(current), code);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>struct list_headaudit_killed_trees(void)
<br>{
<br>&#x9;struct audit_contextctx = current-&#x3E;audit_context;
<br>&#x9;if (likely(!ctx || !ctx-&#x3E;in_syscall))
<br>&#x9;&#x9;return NULL;
<br>&#x9;return &#x26;ctx-&#x3E;killed_trees;
<br>}
<br>*/
<br>/*
<br>#include &#x22;audit.h&#x22;
<br>#include &#x3C;linux/fsnotify_backend.h&#x3E;
<br>#include &#x3C;linux/namei.h&#x3E;
<br>#include &#x3C;linux/mount.h&#x3E;
<br>#include &#x3C;linux/kthread.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>
<br>struct audit_tree;
<br>struct audit_chunk;
<br>
<br>struct audit_tree {
<br>&#x9;atomic_t count;
<br>&#x9;int goner;
<br>&#x9;struct audit_chunkroot;
<br>&#x9;struct list_head chunks;
<br>&#x9;struct list_head rules;
<br>&#x9;struct list_head list;
<br>&#x9;struct list_head same_root;
<br>&#x9;struct rcu_head head;
<br>&#x9;char pathname[];
<br>};
<br>
<br>struct audit_chunk {
<br>&#x9;struct list_head hash;
<br>&#x9;struct fsnotify_mark mark;
<br>&#x9;struct list_head trees;&#x9;&#x9;*/ with root here /*
<br>&#x9;int dead;
<br>&#x9;int count;
<br>&#x9;atomic_long_t refs;
<br>&#x9;struct rcu_head head;
<br>&#x9;struct node {
<br>&#x9;&#x9;struct list_head list;
<br>&#x9;&#x9;struct audit_treeowner;
<br>&#x9;&#x9;unsigned index;&#x9;&#x9;*/ index; upper bit indicates &#x27;will prune&#x27; /*
<br>&#x9;} owners[];
<br>};
<br>
<br>static LIST_HEAD(tree_list);
<br>static LIST_HEAD(prune_list);
<br>static struct task_structprune_thread;
<br>
<br>*/
<br> One struct chunk is attached to each inode of interest.
<br> We replace struct chunk on tagging/untagging.
<br> Rules have pointer to struct audit_tree.
<br> Rules have struct list_head rlist forming a list of rules over
<br> the same tree.
<br> References to struct chunk are collected at audit_inode{,_child}()
<br> time and used in AUDIT_TREE rule matching.
<br> These references are dropped at the same time we are calling
<br> audit_free_names(), etc.
<br>
<br> Cyclic lists galore:
<br> tree.chunks anchors chunk.owners[].list&#x9;&#x9;&#x9;hash_lock
<br> tree.rules anchors rule.rlist&#x9;&#x9;&#x9;&#x9;audit_filter_mutex
<br> chunk.trees anchors tree.same_root&#x9;&#x9;&#x9;&#x9;hash_lock
<br> chunk.hash is a hash with middle bits of watch.inode as
<br> a hash function.&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;RCU, hash_lock
<br>
<br> tree is refcounted; one reference for &#x22;some rules on rules_list refer to
<br> it&#x22;, one for each chunk with pointer to it.
<br>
<br> chunk is refcounted by embedded fsnotify_mark + .refs (non-zero refcount
<br> of watch contributes 1 to .refs).
<br>
<br> node.index allows to get from node.list to containing chunk.
<br> MSB of that sucker is stolen to mark taggings that we might have to
<br> revert - several operations have very unpleasant cleanup logics and
<br> that makes a difference.  Some.
<br> /*
<br>
<br>static struct fsnotify_groupaudit_tree_group;
<br>
<br>static struct audit_treealloc_tree(const chars)
<br>{
<br>&#x9;struct audit_treetree;
<br>
<br>&#x9;tree = kmalloc(sizeof(struct audit_tree) + strlen(s) + 1, GFP_KERNEL);
<br>&#x9;if (tree) {
<br>&#x9;&#x9;atomic_set(&#x26;tree-&#x3E;count, 1);
<br>&#x9;&#x9;tree-&#x3E;goner = 0;
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;chunks);
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;rules);
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;list);
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;same_root);
<br>&#x9;&#x9;tree-&#x3E;root = NULL;
<br>&#x9;&#x9;strcpy(tree-&#x3E;pathname, s);
<br>&#x9;}
<br>&#x9;return tree;
<br>}
<br>
<br>static inline void get_tree(struct audit_treetree)
<br>{
<br>&#x9;atomic_inc(&#x26;tree-&#x3E;count);
<br>}
<br>
<br>static inline void put_tree(struct audit_treetree)
<br>{
<br>&#x9;if (atomic_dec_and_test(&#x26;tree-&#x3E;count))
<br>&#x9;&#x9;kfree_rcu(tree, head);
<br>}
<br>
<br>*/ to avoid bringing the entire thing in audit.h /*
<br>const charaudit_tree_path(struct audit_treetree)
<br>{
<br>&#x9;return tree-&#x3E;pathname;
<br>}
<br>
<br>static void free_chunk(struct audit_chunkchunk)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;for (i = 0; i &#x3C; chunk-&#x3E;count; i++) {
<br>&#x9;&#x9;if (chunk-&#x3E;owners[i].owner)
<br>&#x9;&#x9;&#x9;put_tree(chunk-&#x3E;owners[i].owner);
<br>&#x9;}
<br>&#x9;kfree(chunk);
<br>}
<br>
<br>void audit_put_chunk(struct audit_chunkchunk)
<br>{
<br>&#x9;if (atomic_long_dec_and_test(&#x26;chunk-&#x3E;refs))
<br>&#x9;&#x9;free_chunk(chunk);
<br>}
<br>
<br>static void __put_chunk(struct rcu_headrcu)
<br>{
<br>&#x9;struct audit_chunkchunk = container_of(rcu, struct audit_chunk, head);
<br>&#x9;audit_put_chunk(chunk);
<br>}
<br>
<br>static void audit_tree_destroy_watch(struct fsnotify_markentry)
<br>{
<br>&#x9;struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);
<br>&#x9;call_rcu(&#x26;chunk-&#x3E;head, __put_chunk);
<br>}
<br>
<br>static struct audit_chunkalloc_chunk(int count)
<br>{
<br>&#x9;struct audit_chunkchunk;
<br>&#x9;size_t size;
<br>&#x9;int i;
<br>
<br>&#x9;size = offsetof(struct audit_chunk, owners) + count sizeof(struct node);
<br>&#x9;chunk = kzalloc(size, GFP_KERNEL);
<br>&#x9;if (!chunk)
<br>&#x9;&#x9;return NULL;
<br>
<br>&#x9;INIT_LIST_HEAD(&#x26;chunk-&#x3E;hash);
<br>&#x9;INIT_LIST_HEAD(&#x26;chunk-&#x3E;trees);
<br>&#x9;chunk-&#x3E;count = count;
<br>&#x9;atomic_long_set(&#x26;chunk-&#x3E;refs, 1);
<br>&#x9;for (i = 0; i &#x3C; count; i++) {
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;chunk-&#x3E;owners[i].list);
<br>&#x9;&#x9;chunk-&#x3E;owners[i].index = i;
<br>&#x9;}
<br>&#x9;fsnotify_init_mark(&#x26;chunk-&#x3E;mark, audit_tree_destroy_watch);
<br>&#x9;chunk-&#x3E;mark.mask = FS_IN_IGNORED;
<br>&#x9;return chunk;
<br>}
<br>
<br>enum {HASH_SIZE = 128};
<br>static struct list_head chunk_hash_heads[HASH_SIZE];
<br>static __cacheline_aligned_in_smp DEFINE_SPINLOCK(hash_lock);
<br>
<br>static inline struct list_headchunk_hash(const struct inodeinode)
<br>{
<br>&#x9;unsigned long n = (unsigned long)inode / L1_CACHE_BYTES;
<br>&#x9;return chunk_hash_heads + n % HASH_SIZE;
<br>}
<br>
<br>*/ hash_lock &#x26; entry-&#x3E;lock is held by caller /*
<br>static void insert_hash(struct audit_chunkchunk)
<br>{
<br>&#x9;struct fsnotify_markentry = &#x26;chunk-&#x3E;mark;
<br>&#x9;struct list_headlist;
<br>
<br>&#x9;if (!entry-&#x3E;inode)
<br>&#x9;&#x9;return;
<br>&#x9;list = chunk_hash(entry-&#x3E;inode);
<br>&#x9;list_add_rcu(&#x26;chunk-&#x3E;hash, list);
<br>}
<br>
<br>*/ called under rcu_read_lock /*
<br>struct audit_chunkaudit_tree_lookup(const struct inodeinode)
<br>{
<br>&#x9;struct list_headlist = chunk_hash(inode);
<br>&#x9;struct audit_chunkp;
<br>
<br>&#x9;list_for_each_entry_rcu(p, list, hash) {
<br>&#x9;&#x9;*/ mark.inode may have gone NULL, but who cares? /*
<br>&#x9;&#x9;if (p-&#x3E;mark.inode == inode) {
<br>&#x9;&#x9;&#x9;atomic_long_inc(&#x26;p-&#x3E;refs);
<br>&#x9;&#x9;&#x9;return p;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;return NULL;
<br>}
<br>
<br>bool audit_tree_match(struct audit_chunkchunk, struct audit_treetree)
<br>{
<br>&#x9;int n;
<br>&#x9;for (n = 0; n &#x3C; chunk-&#x3E;count; n++)
<br>&#x9;&#x9;if (chunk-&#x3E;owners[n].owner == tree)
<br>&#x9;&#x9;&#x9;return true;
<br>&#x9;return false;
<br>}
<br>
<br>*/ tagging and untagging inodes with trees /*
<br>
<br>static struct audit_chunkfind_chunk(struct nodep)
<br>{
<br>&#x9;int index = p-&#x3E;index &#x26; ~(1U&#x3C;&#x3C;31);
<br>&#x9;p -= index;
<br>&#x9;return container_of(p, struct audit_chunk, owners[0]);
<br>}
<br>
<br>static void untag_chunk(struct nodep)
<br>{
<br>&#x9;struct audit_chunkchunk = find_chunk(p);
<br>&#x9;struct fsnotify_markentry = &#x26;chunk-&#x3E;mark;
<br>&#x9;struct audit_chunknew = NULL;
<br>&#x9;struct audit_treeowner;
<br>&#x9;int size = chunk-&#x3E;count - 1;
<br>&#x9;int i, j;
<br>
<br>&#x9;fsnotify_get_mark(entry);
<br>
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>
<br>&#x9;if (size)
<br>&#x9;&#x9;new = alloc_chunk(size);
<br>
<br>&#x9;spin_lock(&#x26;entry-&#x3E;lock);
<br>&#x9;if (chunk-&#x3E;dead || !entry-&#x3E;inode) {
<br>&#x9;&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
<br>&#x9;&#x9;if (new)
<br>&#x9;&#x9;&#x9;free_chunk(new);
<br>&#x9;&#x9;goto out;
<br>&#x9;}
<br>
<br>&#x9;owner = p-&#x3E;owner;
<br>
<br>&#x9;if (!size) {
<br>&#x9;&#x9;chunk-&#x3E;dead = 1;
<br>&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;&#x9;list_del_init(&#x26;chunk-&#x3E;trees);
<br>&#x9;&#x9;if (owner-&#x3E;root == chunk)
<br>&#x9;&#x9;&#x9;owner-&#x3E;root = NULL;
<br>&#x9;&#x9;list_del_init(&#x26;p-&#x3E;list);
<br>&#x9;&#x9;list_del_rcu(&#x26;chunk-&#x3E;hash);
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
<br>&#x9;&#x9;fsnotify_destroy_mark(entry, audit_tree_group);
<br>&#x9;&#x9;goto out;
<br>&#x9;}
<br>
<br>&#x9;if (!new)
<br>&#x9;&#x9;goto Fallback;
<br>
<br>&#x9;fsnotify_duplicate_mark(&#x26;new-&#x3E;mark, entry);
<br>&#x9;if (fsnotify_add_mark(&#x26;new-&#x3E;mark, new-&#x3E;mark.group, new-&#x3E;mark.inode, NULL, 1)) {
<br>&#x9;&#x9;fsnotify_put_mark(&#x26;new-&#x3E;mark);
<br>&#x9;&#x9;goto Fallback;
<br>&#x9;}
<br>
<br>&#x9;chunk-&#x3E;dead = 1;
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;list_replace_init(&#x26;chunk-&#x3E;trees, &#x26;new-&#x3E;trees);
<br>&#x9;if (owner-&#x3E;root == chunk) {
<br>&#x9;&#x9;list_del_init(&#x26;owner-&#x3E;same_root);
<br>&#x9;&#x9;owner-&#x3E;root = NULL;
<br>&#x9;}
<br>
<br>&#x9;for (i = j = 0; j &#x3C;= size; i++, j++) {
<br>&#x9;&#x9;struct audit_trees;
<br>&#x9;&#x9;if (&#x26;chunk-&#x3E;owners[j] == p) {
<br>&#x9;&#x9;&#x9;list_del_init(&#x26;p-&#x3E;list);
<br>&#x9;&#x9;&#x9;i--;
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;s = chunk-&#x3E;owners[j].owner;
<br>&#x9;&#x9;new-&#x3E;owners[i].owner = s;
<br>&#x9;&#x9;new-&#x3E;owners[i].index = chunk-&#x3E;owners[j].index - j + i;
<br>&#x9;&#x9;if (!s)/ result of earlier fallback /*
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;get_tree(s);
<br>&#x9;&#x9;list_replace_init(&#x26;chunk-&#x3E;owners[j].list, &#x26;new-&#x3E;owners[i].list);
<br>&#x9;}
<br>
<br>&#x9;list_replace_rcu(&#x26;chunk-&#x3E;hash, &#x26;new-&#x3E;hash);
<br>&#x9;list_for_each_entry(owner, &#x26;new-&#x3E;trees, same_root)
<br>&#x9;&#x9;owner-&#x3E;root = new;
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
<br>&#x9;fsnotify_destroy_mark(entry, audit_tree_group);
<br>&#x9;fsnotify_put_mark(&#x26;new-&#x3E;mark);&#x9;*/ drop initial reference /*
<br>&#x9;goto out;
<br>
<br>Fallback:
<br>&#x9;// do the best we can
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;if (owner-&#x3E;root == chunk) {
<br>&#x9;&#x9;list_del_init(&#x26;owner-&#x3E;same_root);
<br>&#x9;&#x9;owner-&#x3E;root = NULL;
<br>&#x9;}
<br>&#x9;list_del_init(&#x26;p-&#x3E;list);
<br>&#x9;p-&#x3E;owner = NULL;
<br>&#x9;put_tree(owner);
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
<br>out:
<br>&#x9;fsnotify_put_mark(entry);
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>}
<br>
<br>static int create_chunk(struct inodeinode, struct audit_treetree)
<br>{
<br>&#x9;struct fsnotify_markentry;
<br>&#x9;struct audit_chunkchunk = alloc_chunk(1);
<br>&#x9;if (!chunk)
<br>&#x9;&#x9;return -ENOMEM;
<br>
<br>&#x9;entry = &#x26;chunk-&#x3E;mark;
<br>&#x9;if (fsnotify_add_mark(entry, audit_tree_group, inode, NULL, 0)) {
<br>&#x9;&#x9;fsnotify_put_mark(entry);
<br>&#x9;&#x9;return -ENOSPC;
<br>&#x9;}
<br>
<br>&#x9;spin_lock(&#x26;entry-&#x3E;lock);
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;if (tree-&#x3E;goner) {
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;chunk-&#x3E;dead = 1;
<br>&#x9;&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
<br>&#x9;&#x9;fsnotify_destroy_mark(entry, audit_tree_group);
<br>&#x9;&#x9;fsnotify_put_mark(entry);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>&#x9;chunk-&#x3E;owners[0].index = (1U &#x3C;&#x3C; 31);
<br>&#x9;chunk-&#x3E;owners[0].owner = tree;
<br>&#x9;get_tree(tree);
<br>&#x9;list_add(&#x26;chunk-&#x3E;owners[0].list, &#x26;tree-&#x3E;chunks);
<br>&#x9;if (!tree-&#x3E;root) {
<br>&#x9;&#x9;tree-&#x3E;root = chunk;
<br>&#x9;&#x9;list_add(&#x26;tree-&#x3E;same_root, &#x26;chunk-&#x3E;trees);
<br>&#x9;}
<br>&#x9;insert_hash(chunk);
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
<br>&#x9;fsnotify_put_mark(entry);&#x9;*/ drop initial reference /*
<br>&#x9;return 0;
<br>}
<br>
<br>*/ the first tagged inode becomes root of tree /*
<br>static int tag_chunk(struct inodeinode, struct audit_treetree)
<br>{
<br>&#x9;struct fsnotify_markold_entry,chunk_entry;
<br>&#x9;struct audit_treeowner;
<br>&#x9;struct audit_chunkchunk,old;
<br>&#x9;struct nodep;
<br>&#x9;int n;
<br>
<br>&#x9;old_entry = fsnotify_find_inode_mark(audit_tree_group, inode);
<br>&#x9;if (!old_entry)
<br>&#x9;&#x9;return create_chunk(inode, tree);
<br>
<br>&#x9;old = container_of(old_entry, struct audit_chunk, mark);
<br>
<br>&#x9;*/ are we already there? /*
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;for (n = 0; n &#x3C; old-&#x3E;count; n++) {
<br>&#x9;&#x9;if (old-&#x3E;owners[n].owner == tree) {
<br>&#x9;&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;&#x9;fsnotify_put_mark(old_entry);
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>
<br>&#x9;chunk = alloc_chunk(old-&#x3E;count + 1);
<br>&#x9;if (!chunk) {
<br>&#x9;&#x9;fsnotify_put_mark(old_entry);
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;}
<br>
<br>&#x9;chunk_entry = &#x26;chunk-&#x3E;mark;
<br>
<br>&#x9;spin_lock(&#x26;old_entry-&#x3E;lock);
<br>&#x9;if (!old_entry-&#x3E;inode) {
<br>&#x9;&#x9;*/ old_entry is being shot, lets just lie /*
<br>&#x9;&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
<br>&#x9;&#x9;fsnotify_put_mark(old_entry);
<br>&#x9;&#x9;free_chunk(chunk);
<br>&#x9;&#x9;return -ENOENT;
<br>&#x9;}
<br>
<br>&#x9;fsnotify_duplicate_mark(chunk_entry, old_entry);
<br>&#x9;if (fsnotify_add_mark(chunk_entry, chunk_entry-&#x3E;group, chunk_entry-&#x3E;inode, NULL, 1)) {
<br>&#x9;&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
<br>&#x9;&#x9;fsnotify_put_mark(chunk_entry);
<br>&#x9;&#x9;fsnotify_put_mark(old_entry);
<br>&#x9;&#x9;return -ENOSPC;
<br>&#x9;}
<br>
<br>&#x9;*/ even though we hold old_entry-&#x3E;lock, this is safe since chunk_entry-&#x3E;lock could NEVER have been grabbed before /*
<br>&#x9;spin_lock(&#x26;chunk_entry-&#x3E;lock);
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>
<br>&#x9;*/ we now hold old_entry-&#x3E;lock, chunk_entry-&#x3E;lock, and hash_lock /*
<br>&#x9;if (tree-&#x3E;goner) {
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;chunk-&#x3E;dead = 1;
<br>&#x9;&#x9;spin_unlock(&#x26;chunk_entry-&#x3E;lock);
<br>&#x9;&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
<br>
<br>&#x9;&#x9;fsnotify_destroy_mark(chunk_entry, audit_tree_group);
<br>
<br>&#x9;&#x9;fsnotify_put_mark(chunk_entry);
<br>&#x9;&#x9;fsnotify_put_mark(old_entry);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>&#x9;list_replace_init(&#x26;old-&#x3E;trees, &#x26;chunk-&#x3E;trees);
<br>&#x9;for (n = 0, p = chunk-&#x3E;owners; n &#x3C; old-&#x3E;count; n++, p++) {
<br>&#x9;&#x9;struct audit_trees = old-&#x3E;owners[n].owner;
<br>&#x9;&#x9;p-&#x3E;owner = s;
<br>&#x9;&#x9;p-&#x3E;index = old-&#x3E;owners[n].index;
<br>&#x9;&#x9;if (!s)/ result of fallback in untag /*
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;get_tree(s);
<br>&#x9;&#x9;list_replace_init(&#x26;old-&#x3E;owners[n].list, &#x26;p-&#x3E;list);
<br>&#x9;}
<br>&#x9;p-&#x3E;index = (chunk-&#x3E;count - 1) | (1U&#x3C;&#x3C;31);
<br>&#x9;p-&#x3E;owner = tree;
<br>&#x9;get_tree(tree);
<br>&#x9;list_add(&#x26;p-&#x3E;list, &#x26;tree-&#x3E;chunks);
<br>&#x9;list_replace_rcu(&#x26;old-&#x3E;hash, &#x26;chunk-&#x3E;hash);
<br>&#x9;list_for_each_entry(owner, &#x26;chunk-&#x3E;trees, same_root)
<br>&#x9;&#x9;owner-&#x3E;root = chunk;
<br>&#x9;old-&#x3E;dead = 1;
<br>&#x9;if (!tree-&#x3E;root) {
<br>&#x9;&#x9;tree-&#x3E;root = chunk;
<br>&#x9;&#x9;list_add(&#x26;tree-&#x3E;same_root, &#x26;chunk-&#x3E;trees);
<br>&#x9;}
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;spin_unlock(&#x26;chunk_entry-&#x3E;lock);
<br>&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
<br>&#x9;fsnotify_destroy_mark(old_entry, audit_tree_group);
<br>&#x9;fsnotify_put_mark(chunk_entry);&#x9;*/ drop initial reference /*
<br>&#x9;fsnotify_put_mark(old_entry);/ pair to fsnotify_find mark_entry /*
<br>&#x9;return 0;
<br>}
<br>
<br>static void audit_tree_log_remove_rule(struct audit_krulerule)
<br>{
<br>&#x9;struct audit_bufferab;
<br>
<br>&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
<br>&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;return;
<br>&#x9;audit_log_format(ab, &#x22;op=&#x22;);
<br>&#x9;audit_log_string(ab, &#x22;remove_rule&#x22;);
<br>&#x9;audit_log_format(ab, &#x22; dir=&#x22;);
<br>&#x9;audit_log_untrustedstring(ab, rule-&#x3E;tree-&#x3E;pathname);
<br>&#x9;audit_log_key(ab, rule-&#x3E;filterkey);
<br>&#x9;audit_log_format(ab, &#x22; list=%d res=1&#x22;, rule-&#x3E;listnr);
<br>&#x9;audit_log_end(ab);
<br>}
<br>
<br>static void kill_rules(struct audit_treetree)
<br>{
<br>&#x9;struct audit_krulerule,next;
<br>&#x9;struct audit_entryentry;
<br>
<br>&#x9;list_for_each_entry_safe(rule, next, &#x26;tree-&#x3E;rules, rlist) {
<br>&#x9;&#x9;entry = container_of(rule, struct audit_entry, rule);
<br>
<br>&#x9;&#x9;list_del_init(&#x26;rule-&#x3E;rlist);
<br>&#x9;&#x9;if (rule-&#x3E;tree) {
<br>&#x9;&#x9;&#x9;*/ not a half-baked one /*
<br>&#x9;&#x9;&#x9;audit_tree_log_remove_rule(rule);
<br>&#x9;&#x9;&#x9;if (entry-&#x3E;rule.exe)
<br>&#x9;&#x9;&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);
<br>&#x9;&#x9;&#x9;rule-&#x3E;tree = NULL;
<br>&#x9;&#x9;&#x9;list_del_rcu(&#x26;entry-&#x3E;list);
<br>&#x9;&#x9;&#x9;list_del(&#x26;entry-&#x3E;rule.list);
<br>&#x9;&#x9;&#x9;call_rcu(&#x26;entry-&#x3E;rcu, audit_free_rule_rcu);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>}
<br>
<br>*/
<br> finish killing struct audit_tree
<br> /*
<br>static void prune_one(struct audit_treevictim)
<br>{
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;while (!list_empty(&#x26;victim-&#x3E;chunks)) {
<br>&#x9;&#x9;struct nodep;
<br>
<br>&#x9;&#x9;p = list_entry(victim-&#x3E;chunks.next, struct node, list);
<br>
<br>&#x9;&#x9;untag_chunk(p);
<br>&#x9;}
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;put_tree(victim);
<br>}
<br>
<br>*/ trim the uncommitted chunks from tree /*
<br>
<br>static void trim_marked(struct audit_treetree)
<br>{
<br>&#x9;struct list_headp,q;
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;if (tree-&#x3E;goner) {
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;return;
<br>&#x9;}
<br>&#x9;*/ reorder /*
<br>&#x9;for (p = tree-&#x3E;chunks.next; p != &#x26;tree-&#x3E;chunks; p = q) {
<br>&#x9;&#x9;struct nodenode = list_entry(p, struct node, list);
<br>&#x9;&#x9;q = p-&#x3E;next;
<br>&#x9;&#x9;if (node-&#x3E;index &#x26; (1U&#x3C;&#x3C;31)) {
<br>&#x9;&#x9;&#x9;list_del_init(p);
<br>&#x9;&#x9;&#x9;list_add(p, &#x26;tree-&#x3E;chunks);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;while (!list_empty(&#x26;tree-&#x3E;chunks)) {
<br>&#x9;&#x9;struct nodenode;
<br>
<br>&#x9;&#x9;node = list_entry(tree-&#x3E;chunks.next, struct node, list);
<br>
<br>&#x9;&#x9;*/ have we run out of marked? /*
<br>&#x9;&#x9;if (!(node-&#x3E;index &#x26; (1U&#x3C;&#x3C;31)))
<br>&#x9;&#x9;&#x9;break;
<br>
<br>&#x9;&#x9;untag_chunk(node);
<br>&#x9;}
<br>&#x9;if (!tree-&#x3E;root &#x26;&#x26; !tree-&#x3E;goner) {
<br>&#x9;&#x9;tree-&#x3E;goner = 1;
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;kill_rules(tree);
<br>&#x9;&#x9;list_del_init(&#x26;tree-&#x3E;list);
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;prune_one(tree);
<br>&#x9;} else {
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;}
<br>}
<br>
<br>static void audit_schedule_prune(void);
<br>
<br>*/ called with audit_filter_mutex /*
<br>int audit_remove_tree_rule(struct audit_krulerule)
<br>{
<br>&#x9;struct audit_treetree;
<br>&#x9;tree = rule-&#x3E;tree;
<br>&#x9;if (tree) {
<br>&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;&#x9;list_del_init(&#x26;rule-&#x3E;rlist);
<br>&#x9;&#x9;if (list_empty(&#x26;tree-&#x3E;rules) &#x26;&#x26; !tree-&#x3E;goner) {
<br>&#x9;&#x9;&#x9;tree-&#x3E;root = NULL;
<br>&#x9;&#x9;&#x9;list_del_init(&#x26;tree-&#x3E;same_root);
<br>&#x9;&#x9;&#x9;tree-&#x3E;goner = 1;
<br>&#x9;&#x9;&#x9;list_move(&#x26;tree-&#x3E;list, &#x26;prune_list);
<br>&#x9;&#x9;&#x9;rule-&#x3E;tree = NULL;
<br>&#x9;&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;&#x9;audit_schedule_prune();
<br>&#x9;&#x9;&#x9;return 1;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;rule-&#x3E;tree = NULL;
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;return 1;
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>static int compare_root(struct vfsmountmnt, voidarg)
<br>{
<br>&#x9;return d_backing_inode(mnt-&#x3E;mnt_root) == arg;
<br>}
<br>
<br>void audit_trim_trees(void)
<br>{
<br>&#x9;struct list_head cursor;
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;list_add(&#x26;cursor, &#x26;tree_list);
<br>&#x9;while (cursor.next != &#x26;tree_list) {
<br>&#x9;&#x9;struct audit_treetree;
<br>&#x9;&#x9;struct path path;
<br>&#x9;&#x9;struct vfsmountroot_mnt;
<br>&#x9;&#x9;struct nodenode;
<br>&#x9;&#x9;int err;
<br>
<br>&#x9;&#x9;tree = container_of(cursor.next, struct audit_tree, list);
<br>&#x9;&#x9;get_tree(tree);
<br>&#x9;&#x9;list_del(&#x26;cursor);
<br>&#x9;&#x9;list_add(&#x26;cursor, &#x26;tree-&#x3E;list);
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;&#x9;err = kern_path(tree-&#x3E;pathname, 0, &#x26;path);
<br>&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;goto skip_it;
<br>
<br>&#x9;&#x9;root_mnt = collect_mounts(&#x26;path);
<br>&#x9;&#x9;path_put(&#x26;path);
<br>&#x9;&#x9;if (IS_ERR(root_mnt))
<br>&#x9;&#x9;&#x9;goto skip_it;
<br>
<br>&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;&#x9;list_for_each_entry(node, &#x26;tree-&#x3E;chunks, list) {
<br>&#x9;&#x9;&#x9;struct audit_chunkchunk = find_chunk(node);
<br>&#x9;&#x9;&#x9;*/ this could be NULL if the watch is dying else where... /*
<br>&#x9;&#x9;&#x9;struct inodeinode = chunk-&#x3E;mark.inode;
<br>&#x9;&#x9;&#x9;node-&#x3E;index |= 1U&#x3C;&#x3C;31;
<br>&#x9;&#x9;&#x9;if (iterate_mounts(compare_root, inode, root_mnt))
<br>&#x9;&#x9;&#x9;&#x9;node-&#x3E;index &#x26;= ~(1U&#x3C;&#x3C;31);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;trim_marked(tree);
<br>&#x9;&#x9;drop_collected_mounts(root_mnt);
<br>skip_it:
<br>&#x9;&#x9;put_tree(tree);
<br>&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;}
<br>&#x9;list_del(&#x26;cursor);
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>}
<br>
<br>int audit_make_tree(struct audit_krulerule, charpathname, u32 op)
<br>{
<br>
<br>&#x9;if (pathname[0] != &#x27;/&#x27; ||
<br>&#x9;    rule-&#x3E;listnr != AUDIT_FILTER_EXIT ||
<br>&#x9;    op != Audit_equal ||
<br>&#x9;    rule-&#x3E;inode_f || rule-&#x3E;watch || rule-&#x3E;tree)
<br>&#x9;&#x9;return -EINVAL;
<br>&#x9;rule-&#x3E;tree = alloc_tree(pathname);
<br>&#x9;if (!rule-&#x3E;tree)
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;return 0;
<br>}
<br>
<br>void audit_put_tree(struct audit_treetree)
<br>{
<br>&#x9;put_tree(tree);
<br>}
<br>
<br>static int tag_mount(struct vfsmountmnt, voidarg)
<br>{
<br>&#x9;return tag_chunk(d_backing_inode(mnt-&#x3E;mnt_root), arg);
<br>}
<br>
<br>*/
<br> That gets run when evict_chunk() ends up needing to kill audit_tree.
<br> Runs from a separate thread.
<br> /*
<br>static int prune_tree_thread(voidunused)
<br>{
<br>&#x9;for (;;) {
<br>&#x9;&#x9;set_current_state(TASK_INTERRUPTIBLE);
<br>&#x9;&#x9;if (list_empty(&#x26;prune_list))
<br>&#x9;&#x9;&#x9;schedule();
<br>&#x9;&#x9;__set_current_state(TASK_RUNNING);
<br>
<br>&#x9;&#x9;mutex_lock(&#x26;audit_cmd_mutex);
<br>&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;&#x9;while (!list_empty(&#x26;prune_list)) {
<br>&#x9;&#x9;&#x9;struct audit_treevictim;
<br>
<br>&#x9;&#x9;&#x9;victim = list_entry(prune_list.next,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;struct audit_tree, list);
<br>&#x9;&#x9;&#x9;list_del_init(&#x26;victim-&#x3E;list);
<br>
<br>&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;&#x9;&#x9;prune_one(victim);
<br>
<br>&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>
<br>static int audit_launch_prune(void)
<br>{
<br>&#x9;if (prune_thread)
<br>&#x9;&#x9;return 0;
<br>&#x9;prune_thread = kthread_create(prune_tree_thread, NULL,
<br>&#x9;&#x9;&#x9;&#x9;&#x22;audit_prune_tree&#x22;);
<br>&#x9;if (IS_ERR(prune_thread)) {
<br>&#x9;&#x9;pr_err(&#x22;cannot start thread audit_prune_tree&#x22;);
<br>&#x9;&#x9;prune_thread = NULL;
<br>&#x9;&#x9;return -ENOMEM;
<br>&#x9;} else {
<br>&#x9;&#x9;wake_up_process(prune_thread);
<br>&#x9;&#x9;return 0;
<br>&#x9;}
<br>}
<br>
<br>*/ called with audit_filter_mutex /*
<br>int audit_add_tree_rule(struct audit_krulerule)
<br>{
<br>&#x9;struct audit_treeseed = rule-&#x3E;tree,tree;
<br>&#x9;struct path path;
<br>&#x9;struct vfsmountmnt;
<br>&#x9;int err;
<br>
<br>&#x9;rule-&#x3E;tree = NULL;
<br>&#x9;list_for_each_entry(tree, &#x26;tree_list, list) {
<br>&#x9;&#x9;if (!strcmp(seed-&#x3E;pathname, tree-&#x3E;pathname)) {
<br>&#x9;&#x9;&#x9;put_tree(seed);
<br>&#x9;&#x9;&#x9;rule-&#x3E;tree = tree;
<br>&#x9;&#x9;&#x9;list_add(&#x26;rule-&#x3E;rlist, &#x26;tree-&#x3E;rules);
<br>&#x9;&#x9;&#x9;return 0;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>&#x9;tree = seed;
<br>&#x9;list_add(&#x26;tree-&#x3E;list, &#x26;tree_list);
<br>&#x9;list_add(&#x26;rule-&#x3E;rlist, &#x26;tree-&#x3E;rules);
<br>&#x9;*/ do not set rule-&#x3E;tree yet /*
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;if (unlikely(!prune_thread)) {
<br>&#x9;&#x9;err = audit_launch_prune();
<br>&#x9;&#x9;if (err)
<br>&#x9;&#x9;&#x9;goto Err;
<br>&#x9;}
<br>
<br>&#x9;err = kern_path(tree-&#x3E;pathname, 0, &#x26;path);
<br>&#x9;if (err)
<br>&#x9;&#x9;goto Err;
<br>&#x9;mnt = collect_mounts(&#x26;path);
<br>&#x9;path_put(&#x26;path);
<br>&#x9;if (IS_ERR(mnt)) {
<br>&#x9;&#x9;err = PTR_ERR(mnt);
<br>&#x9;&#x9;goto Err;
<br>&#x9;}
<br>
<br>&#x9;get_tree(tree);
<br>&#x9;err = iterate_mounts(tag_mount, tree, mnt);
<br>&#x9;drop_collected_mounts(mnt);
<br>
<br>&#x9;if (!err) {
<br>&#x9;&#x9;struct nodenode;
<br>&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;&#x9;list_for_each_entry(node, &#x26;tree-&#x3E;chunks, list)
<br>&#x9;&#x9;&#x9;node-&#x3E;index &#x26;= ~(1U&#x3C;&#x3C;31);
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;} else {
<br>&#x9;&#x9;trim_marked(tree);
<br>&#x9;&#x9;goto Err;
<br>&#x9;}
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;if (list_empty(&#x26;rule-&#x3E;rlist)) {
<br>&#x9;&#x9;put_tree(tree);
<br>&#x9;&#x9;return -ENOENT;
<br>&#x9;}
<br>&#x9;rule-&#x3E;tree = tree;
<br>&#x9;put_tree(tree);
<br>
<br>&#x9;return 0;
<br>Err:
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;list_del_init(&#x26;tree-&#x3E;list);
<br>&#x9;list_del_init(&#x26;tree-&#x3E;rules);
<br>&#x9;put_tree(tree);
<br>&#x9;return err;
<br>}
<br>
<br>int audit_tag_tree(charold, charnew)
<br>{
<br>&#x9;struct list_head cursor, barrier;
<br>&#x9;int failed = 0;
<br>&#x9;struct path path1, path2;
<br>&#x9;struct vfsmounttagged;
<br>&#x9;int err;
<br>
<br>&#x9;err = kern_path(new, 0, &#x26;path2);
<br>&#x9;if (err)
<br>&#x9;&#x9;return err;
<br>&#x9;tagged = collect_mounts(&#x26;path2);
<br>&#x9;path_put(&#x26;path2);
<br>&#x9;if (IS_ERR(tagged))
<br>&#x9;&#x9;return PTR_ERR(tagged);
<br>
<br>&#x9;err = kern_path(old, 0, &#x26;path1);
<br>&#x9;if (err) {
<br>&#x9;&#x9;drop_collected_mounts(tagged);
<br>&#x9;&#x9;return err;
<br>&#x9;}
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;list_add(&#x26;barrier, &#x26;tree_list);
<br>&#x9;list_add(&#x26;cursor, &#x26;barrier);
<br>
<br>&#x9;while (cursor.next != &#x26;tree_list) {
<br>&#x9;&#x9;struct audit_treetree;
<br>&#x9;&#x9;int good_one = 0;
<br>
<br>&#x9;&#x9;tree = container_of(cursor.next, struct audit_tree, list);
<br>&#x9;&#x9;get_tree(tree);
<br>&#x9;&#x9;list_del(&#x26;cursor);
<br>&#x9;&#x9;list_add(&#x26;cursor, &#x26;tree-&#x3E;list);
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;&#x9;err = kern_path(tree-&#x3E;pathname, 0, &#x26;path2);
<br>&#x9;&#x9;if (!err) {
<br>&#x9;&#x9;&#x9;good_one = path_is_under(&#x26;path1, &#x26;path2);
<br>&#x9;&#x9;&#x9;path_put(&#x26;path2);
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;if (!good_one) {
<br>&#x9;&#x9;&#x9;put_tree(tree);
<br>&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;&#x9;continue;
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;failed = iterate_mounts(tag_mount, tree, tagged);
<br>&#x9;&#x9;if (failed) {
<br>&#x9;&#x9;&#x9;put_tree(tree);
<br>&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;&#x9;break;
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;&#x9;if (!tree-&#x3E;goner) {
<br>&#x9;&#x9;&#x9;list_del(&#x26;tree-&#x3E;list);
<br>&#x9;&#x9;&#x9;list_add(&#x26;tree-&#x3E;list, &#x26;tree_list);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;put_tree(tree);
<br>&#x9;}
<br>
<br>&#x9;while (barrier.prev != &#x26;tree_list) {
<br>&#x9;&#x9;struct audit_treetree;
<br>
<br>&#x9;&#x9;tree = container_of(barrier.prev, struct audit_tree, list);
<br>&#x9;&#x9;get_tree(tree);
<br>&#x9;&#x9;list_del(&#x26;tree-&#x3E;list);
<br>&#x9;&#x9;list_add(&#x26;tree-&#x3E;list, &#x26;barrier);
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;&#x9;if (!failed) {
<br>&#x9;&#x9;&#x9;struct nodenode;
<br>&#x9;&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;&#x9;&#x9;list_for_each_entry(node, &#x26;tree-&#x3E;chunks, list)
<br>&#x9;&#x9;&#x9;&#x9;node-&#x3E;index &#x26;= ~(1U&#x3C;&#x3C;31);
<br>&#x9;&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;trim_marked(tree);
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;put_tree(tree);
<br>&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;}
<br>&#x9;list_del(&#x26;barrier);
<br>&#x9;list_del(&#x26;cursor);
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;path_put(&#x26;path1);
<br>&#x9;drop_collected_mounts(tagged);
<br>&#x9;return failed;
<br>}
<br>
<br>
<br>static void audit_schedule_prune(void)
<br>{
<br>&#x9;wake_up_process(prune_thread);
<br>}
<br>
<br>*/
<br> ... and that one is done if evict_chunk() decides to delay until the end
<br> of syscall.  Runs synchronously.
<br> /*
<br>void audit_kill_trees(struct list_headlist)
<br>{
<br>&#x9;mutex_lock(&#x26;audit_cmd_mutex);
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;while (!list_empty(list)) {
<br>&#x9;&#x9;struct audit_treevictim;
<br>
<br>&#x9;&#x9;victim = list_entry(list-&#x3E;next, struct audit_tree, list);
<br>&#x9;&#x9;kill_rules(victim);
<br>&#x9;&#x9;list_del_init(&#x26;victim-&#x3E;list);
<br>
<br>&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;&#x9;prune_one(victim);
<br>
<br>&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;}
<br>
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
<br>}
<br>
<br>*/
<br>  Here comes the stuff asynchronous to auditctl operations
<br> /*
<br>
<br>static void evict_chunk(struct audit_chunkchunk)
<br>{
<br>&#x9;struct audit_treeowner;
<br>&#x9;struct list_headpostponed = audit_killed_trees();
<br>&#x9;int need_prune = 0;
<br>&#x9;int n;
<br>
<br>&#x9;if (chunk-&#x3E;dead)
<br>&#x9;&#x9;return;
<br>
<br>&#x9;chunk-&#x3E;dead = 1;
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;while (!list_empty(&#x26;chunk-&#x3E;trees)) {
<br>&#x9;&#x9;owner = list_entry(chunk-&#x3E;trees.next,
<br>&#x9;&#x9;&#x9;&#x9;   struct audit_tree, same_root);
<br>&#x9;&#x9;owner-&#x3E;goner = 1;
<br>&#x9;&#x9;owner-&#x3E;root = NULL;
<br>&#x9;&#x9;list_del_init(&#x26;owner-&#x3E;same_root);
<br>&#x9;&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;&#x9;if (!postponed) {
<br>&#x9;&#x9;&#x9;kill_rules(owner);
<br>&#x9;&#x9;&#x9;list_move(&#x26;owner-&#x3E;list, &#x26;prune_list);
<br>&#x9;&#x9;&#x9;need_prune = 1;
<br>&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;list_move(&#x26;owner-&#x3E;list, postponed);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;spin_lock(&#x26;hash_lock);
<br>&#x9;}
<br>&#x9;list_del_rcu(&#x26;chunk-&#x3E;hash);
<br>&#x9;for (n = 0; n &#x3C; chunk-&#x3E;count; n++)
<br>&#x9;&#x9;list_del_init(&#x26;chunk-&#x3E;owners[n].list);
<br>&#x9;spin_unlock(&#x26;hash_lock);
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;if (need_prune)
<br>&#x9;&#x9;audit_schedule_prune();
<br>}
<br>
<br>static int audit_tree_handle_event(struct fsnotify_groupgroup,
<br>&#x9;&#x9;&#x9;&#x9;   struct inodeto_tell,
<br>&#x9;&#x9;&#x9;&#x9;   struct fsnotify_markinode_mark,
<br>&#x9;&#x9;&#x9;&#x9;   struct fsnotify_markvfsmount_mark,
<br>&#x9;&#x9;&#x9;&#x9;   u32 mask, voiddata, int data_type,
<br>&#x9;&#x9;&#x9;&#x9;   const unsigned charfile_name, u32 cookie)
<br>{
<br>&#x9;return 0;
<br>}
<br>
<br>static void audit_tree_freeing_mark(struct fsnotify_markentry, struct fsnotify_groupgroup)
<br>{
<br>&#x9;struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);
<br>
<br>&#x9;evict_chunk(chunk);
<br>
<br>&#x9;*/
<br>&#x9; We are guaranteed to have at least one reference to the mark from
<br>&#x9; either the inode or the caller of fsnotify_destroy_mark().
<br>&#x9; /*
<br>&#x9;BUG_ON(atomic_read(&#x26;entry-&#x3E;refcnt) &#x3C; 1);
<br>}
<br>
<br>static const struct fsnotify_ops audit_tree_ops = {
<br>&#x9;.handle_event = audit_tree_handle_event,
<br>&#x9;.freeing_mark = audit_tree_freeing_mark,
<br>};
<br>
<br>static int __init audit_tree_init(void)
<br>{
<br>&#x9;int i;
<br>
<br>&#x9;audit_tree_group = fsnotify_alloc_group(&#x26;audit_tree_ops);
<br>&#x9;if (IS_ERR(audit_tree_group))
<br>&#x9;&#x9;audit_panic(&#x22;cannot initialize fsnotify group for rectree watches&#x22;);
<br>
<br>&#x9;for (i = 0; i &#x3C; HASH_SIZE; i++)
<br>&#x9;&#x9;INIT_LIST_HEAD(&#x26;chunk_hash_heads[i]);
<br>
<br>&#x9;return 0;
<br>}
<br>__initcall(audit_tree_init);
<br>*/
<br> audit_watch.c -- watching inodes
<br>
<br> Copyright 2003-2009 Red Hat, Inc.
<br> Copyright 2005 Hewlett-Packard Development Company, L.P.
<br> Copyright 2005 IBM Corporation
<br>
<br> This program is free software; you can redistribute it and/or modify
<br> it under the terms of the GNU General Public License as published by
<br> the Free Software Foundation; either version 2 of the License, or
<br> (at your option) any later version.
<br>
<br> This program is distributed in the hope that it will be useful,
<br> but WITHOUT ANY WARRANTY; without even the implied warranty of
<br> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
<br> GNU General Public License for more details.
<br>
<br> You should have received a copy of the GNU General Public License
<br> along with this program; if not, write to the Free Software
<br> Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
<br> /*
<br>
<br>#include &#x3C;linux/kernel.h&#x3E;
<br>#include &#x3C;linux/audit.h&#x3E;
<br>#include &#x3C;linux/kthread.h&#x3E;
<br>#include &#x3C;linux/mutex.h&#x3E;
<br>#include &#x3C;linux/fs.h&#x3E;
<br>#include &#x3C;linux/fsnotify_backend.h&#x3E;
<br>#include &#x3C;linux/namei.h&#x3E;
<br>#include &#x3C;linux/netlink.h&#x3E;
<br>#include &#x3C;linux/sched.h&#x3E;
<br>#include &#x3C;linux/slab.h&#x3E;
<br>#include &#x3C;linux/security.h&#x3E;
<br>#include &#x22;audit.h&#x22;
<br>
<br>*/
<br> Reference counting:
<br>
<br> audit_parent: lifetime is from audit_init_parent() to receipt of an FS_IGNORED
<br> &#x9;event.  Each audit_watch holds a reference to its associated parent.
<br>
<br> audit_watch: if added to lists, lifetime is from audit_init_watch() to
<br> &#x9;audit_remove_watch().  Additionally, an audit_watch may exist
<br> &#x9;temporarily to assist in searching existing filter data.  Each
<br> &#x9;audit_krule holds a reference to its associated watch.
<br> /*
<br>
<br>struct audit_watch {
<br>&#x9;atomic_t&#x9;&#x9;count;&#x9;*/ reference count /*
<br>&#x9;dev_t&#x9;&#x9;&#x9;dev;&#x9;*/ associated superblock device /*
<br>&#x9;char&#x9;&#x9;&#x9;*path;&#x9;*/ insertion path /*
<br>&#x9;unsigned long&#x9;&#x9;ino;&#x9;*/ associated inode number /*
<br>&#x9;struct audit_parent&#x9;*parent;/ associated parent /*
<br>&#x9;struct list_head&#x9;wlist;&#x9;*/ entry in parent-&#x3E;watches list /*
<br>&#x9;struct list_head&#x9;rules;&#x9;*/ anchor for krule-&#x3E;rlist /*
<br>};
<br>
<br>struct audit_parent {
<br>&#x9;struct list_head&#x9;watches;/ anchor for audit_watch-&#x3E;wlist /*
<br>&#x9;struct fsnotify_mark mark;/ fsnotify mark on the inode /*
<br>};
<br>
<br>*/ fsnotify handle. /*
<br>static struct fsnotify_groupaudit_watch_group;
<br>
<br>*/ fsnotify events we care about. /*
<br>#define AUDIT_FS_WATCH (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
<br>&#x9;&#x9;&#x9;FS_MOVE_SELF | FS_EVENT_ON_CHILD)
<br>
<br>static void audit_free_parent(struct audit_parentparent)
<br>{
<br>&#x9;WARN_ON(!list_empty(&#x26;parent-&#x3E;watches));
<br>&#x9;kfree(parent);
<br>}
<br>
<br>static void audit_watch_free_mark(struct fsnotify_markentry)
<br>{
<br>&#x9;struct audit_parentparent;
<br>
<br>&#x9;parent = container_of(entry, struct audit_parent, mark);
<br>&#x9;audit_free_parent(parent);
<br>}
<br>
<br>static void audit_get_parent(struct audit_parentparent)
<br>{
<br>&#x9;if (likely(parent))
<br>&#x9;&#x9;fsnotify_get_mark(&#x26;parent-&#x3E;mark);
<br>}
<br>
<br>static void audit_put_parent(struct audit_parentparent)
<br>{
<br>&#x9;if (likely(parent))
<br>&#x9;&#x9;fsnotify_put_mark(&#x26;parent-&#x3E;mark);
<br>}
<br>
<br>*/
<br> Find and return the audit_parent on the given inode.  If found a reference
<br> is taken on this parent.
<br> /*
<br>static inline struct audit_parentaudit_find_parent(struct inodeinode)
<br>{
<br>&#x9;struct audit_parentparent = NULL;
<br>&#x9;struct fsnotify_markentry;
<br>
<br>&#x9;entry = fsnotify_find_inode_mark(audit_watch_group, inode);
<br>&#x9;if (entry)
<br>&#x9;&#x9;parent = container_of(entry, struct audit_parent, mark);
<br>
<br>&#x9;return parent;
<br>}
<br>
<br>void audit_get_watch(struct audit_watchwatch)
<br>{
<br>&#x9;atomic_inc(&#x26;watch-&#x3E;count);
<br>}
<br>
<br>void audit_put_watch(struct audit_watchwatch)
<br>{
<br>&#x9;if (atomic_dec_and_test(&#x26;watch-&#x3E;count)) {
<br>&#x9;&#x9;WARN_ON(watch-&#x3E;parent);
<br>&#x9;&#x9;WARN_ON(!list_empty(&#x26;watch-&#x3E;rules));
<br>&#x9;&#x9;kfree(watch-&#x3E;path);
<br>&#x9;&#x9;kfree(watch);
<br>&#x9;}
<br>}
<br>
<br>static void audit_remove_watch(struct audit_watchwatch)
<br>{
<br>&#x9;list_del(&#x26;watch-&#x3E;wlist);
<br>&#x9;audit_put_parent(watch-&#x3E;parent);
<br>&#x9;watch-&#x3E;parent = NULL;
<br>&#x9;audit_put_watch(watch);/ match initial get /*
<br>}
<br>
<br>charaudit_watch_path(struct audit_watchwatch)
<br>{
<br>&#x9;return watch-&#x3E;path;
<br>}
<br>
<br>int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev)
<br>{
<br>&#x9;return (watch-&#x3E;ino != AUDIT_INO_UNSET) &#x26;&#x26;
<br>&#x9;&#x9;(watch-&#x3E;ino == ino) &#x26;&#x26;
<br>&#x9;&#x9;(watch-&#x3E;dev == dev);
<br>}
<br>
<br>*/ Initialize a parent watch entry. /*
<br>static struct audit_parentaudit_init_parent(struct pathpath)
<br>{
<br>&#x9;struct inodeinode = d_backing_inode(path-&#x3E;dentry);
<br>&#x9;struct audit_parentparent;
<br>&#x9;int ret;
<br>
<br>&#x9;parent = kzalloc(sizeof(*parent), GFP_KERNEL);
<br>&#x9;if (unlikely(!parent))
<br>&#x9;&#x9;return ERR_PTR(-ENOMEM);
<br>
<br>&#x9;INIT_LIST_HEAD(&#x26;parent-&#x3E;watches);
<br>
<br>&#x9;fsnotify_init_mark(&#x26;parent-&#x3E;mark, audit_watch_free_mark);
<br>&#x9;parent-&#x3E;mark.mask = AUDIT_FS_WATCH;
<br>&#x9;ret = fsnotify_add_mark(&#x26;parent-&#x3E;mark, audit_watch_group, inode, NULL, 0);
<br>&#x9;if (ret &#x3C; 0) {
<br>&#x9;&#x9;audit_free_parent(parent);
<br>&#x9;&#x9;return ERR_PTR(ret);
<br>&#x9;}
<br>
<br>&#x9;return parent;
<br>}
<br>
<br>*/ Initialize a watch entry. /*
<br>static struct audit_watchaudit_init_watch(charpath)
<br>{
<br>&#x9;struct audit_watchwatch;
<br>
<br>&#x9;watch = kzalloc(sizeof(*watch), GFP_KERNEL);
<br>&#x9;if (unlikely(!watch))
<br>&#x9;&#x9;return ERR_PTR(-ENOMEM);
<br>
<br>&#x9;INIT_LIST_HEAD(&#x26;watch-&#x3E;rules);
<br>&#x9;atomic_set(&#x26;watch-&#x3E;count, 1);
<br>&#x9;watch-&#x3E;path = path;
<br>&#x9;watch-&#x3E;dev = AUDIT_DEV_UNSET;
<br>&#x9;watch-&#x3E;ino = AUDIT_INO_UNSET;
<br>
<br>&#x9;return watch;
<br>}
<br>
<br>*/ Translate a watch string to kernel representation. /*
<br>int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op)
<br>{
<br>&#x9;struct audit_watchwatch;
<br>
<br>&#x9;if (!audit_watch_group)
<br>&#x9;&#x9;return -EOPNOTSUPP;
<br>
<br>&#x9;if (path[0] != &#x27;/&#x27; || path[len-1] == &#x27;/&#x27; ||
<br>&#x9;    krule-&#x3E;listnr != AUDIT_FILTER_EXIT ||
<br>&#x9;    op != Audit_equal ||
<br>&#x9;    krule-&#x3E;inode_f || krule-&#x3E;watch || krule-&#x3E;tree)
<br>&#x9;&#x9;return -EINVAL;
<br>
<br>&#x9;watch = audit_init_watch(path);
<br>&#x9;if (IS_ERR(watch))
<br>&#x9;&#x9;return PTR_ERR(watch);
<br>
<br>&#x9;krule-&#x3E;watch = watch;
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>*/ Duplicate the given audit watch.  The new watch&#x27;s rules list is initialized
<br> to an empty list and wlist is undefined. /*
<br>static struct audit_watchaudit_dupe_watch(struct audit_watchold)
<br>{
<br>&#x9;charpath;
<br>&#x9;struct audit_watchnew;
<br>
<br>&#x9;path = kstrdup(old-&#x3E;path, GFP_KERNEL);
<br>&#x9;if (unlikely(!path))
<br>&#x9;&#x9;return ERR_PTR(-ENOMEM);
<br>
<br>&#x9;new = audit_init_watch(path);
<br>&#x9;if (IS_ERR(new)) {
<br>&#x9;&#x9;kfree(path);
<br>&#x9;&#x9;goto out;
<br>&#x9;}
<br>
<br>&#x9;new-&#x3E;dev = old-&#x3E;dev;
<br>&#x9;new-&#x3E;ino = old-&#x3E;ino;
<br>&#x9;audit_get_parent(old-&#x3E;parent);
<br>&#x9;new-&#x3E;parent = old-&#x3E;parent;
<br>
<br>out:
<br>&#x9;return new;
<br>}
<br>
<br>static void audit_watch_log_rule_change(struct audit_kruler, struct audit_watchw, charop)
<br>{
<br>&#x9;if (audit_enabled) {
<br>&#x9;&#x9;struct audit_bufferab;
<br>&#x9;&#x9;ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
<br>&#x9;&#x9;if (unlikely(!ab))
<br>&#x9;&#x9;&#x9;return;
<br>&#x9;&#x9;audit_log_format(ab, &#x22;auid=%u ses=%u op=&#x22;,
<br>&#x9;&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, audit_get_loginuid(current)),
<br>&#x9;&#x9;&#x9;&#x9; audit_get_sessionid(current));
<br>&#x9;&#x9;audit_log_string(ab, op);
<br>&#x9;&#x9;audit_log_format(ab, &#x22; path=&#x22;);
<br>&#x9;&#x9;audit_log_untrustedstring(ab, w-&#x3E;path);
<br>&#x9;&#x9;audit_log_key(ab, r-&#x3E;filterkey);
<br>&#x9;&#x9;audit_log_format(ab, &#x22; list=%d res=1&#x22;, r-&#x3E;listnr);
<br>&#x9;&#x9;audit_log_end(ab);
<br>&#x9;}
<br>}
<br>
<br>*/ Update inode info in audit rules based on filesystem event. /*
<br>static void audit_update_watch(struct audit_parentparent,
<br>&#x9;&#x9;&#x9;       const chardname, dev_t dev,
<br>&#x9;&#x9;&#x9;       unsigned long ino, unsigned invalidating)
<br>{
<br>&#x9;struct audit_watchowatch,nwatch,nextw;
<br>&#x9;struct audit_kruler,nextr;
<br>&#x9;struct audit_entryoentry,nentry;
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;*/ Run all of the watches on this parent looking for the one that
<br>&#x9; matches the given dname /*
<br>&#x9;list_for_each_entry_safe(owatch, nextw, &#x26;parent-&#x3E;watches, wlist) {
<br>&#x9;&#x9;if (audit_compare_dname_path(dname, owatch-&#x3E;path,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;     AUDIT_NAME_FULL))
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;*/ If the update involves invalidating rules, do the inode-based
<br>&#x9;&#x9; filtering now, so we don&#x27;t omit records. /*
<br>&#x9;&#x9;if (invalidating &#x26;&#x26; !audit_dummy_context())
<br>&#x9;&#x9;&#x9;audit_filter_inodes(current, current-&#x3E;audit_context);
<br>
<br>&#x9;&#x9;*/ updating ino will likely change which audit_hash_list we
<br>&#x9;&#x9; are on so we need a new watch for the new list /*
<br>&#x9;&#x9;nwatch = audit_dupe_watch(owatch);
<br>&#x9;&#x9;if (IS_ERR(nwatch)) {
<br>&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;&#x9;&#x9;audit_panic(&#x22;error updating watch, skipping&#x22;);
<br>&#x9;&#x9;&#x9;return;
<br>&#x9;&#x9;}
<br>&#x9;&#x9;nwatch-&#x3E;dev = dev;
<br>&#x9;&#x9;nwatch-&#x3E;ino = ino;
<br>
<br>&#x9;&#x9;list_for_each_entry_safe(r, nextr, &#x26;owatch-&#x3E;rules, rlist) {
<br>
<br>&#x9;&#x9;&#x9;oentry = container_of(r, struct audit_entry, rule);
<br>&#x9;&#x9;&#x9;list_del(&#x26;oentry-&#x3E;rule.rlist);
<br>&#x9;&#x9;&#x9;list_del_rcu(&#x26;oentry-&#x3E;list);
<br>
<br>&#x9;&#x9;&#x9;nentry = audit_dupe_rule(&#x26;oentry-&#x3E;rule);
<br>&#x9;&#x9;&#x9;if (IS_ERR(nentry)) {
<br>&#x9;&#x9;&#x9;&#x9;list_del(&#x26;oentry-&#x3E;rule.list);
<br>&#x9;&#x9;&#x9;&#x9;audit_panic(&#x22;error updating watch, removing&#x22;);
<br>&#x9;&#x9;&#x9;} else {
<br>&#x9;&#x9;&#x9;&#x9;int h = audit_hash_ino((u32)ino);
<br>
<br>&#x9;&#x9;&#x9;&#x9;*/
<br>&#x9;&#x9;&#x9;&#x9; nentry-&#x3E;rule.watch == oentry-&#x3E;rule.watch so
<br>&#x9;&#x9;&#x9;&#x9; we must drop that reference and set it to our
<br>&#x9;&#x9;&#x9;&#x9; new watch.
<br>&#x9;&#x9;&#x9;&#x9; /*
<br>&#x9;&#x9;&#x9;&#x9;audit_put_watch(nentry-&#x3E;rule.watch);
<br>&#x9;&#x9;&#x9;&#x9;audit_get_watch(nwatch);
<br>&#x9;&#x9;&#x9;&#x9;nentry-&#x3E;rule.watch = nwatch;
<br>&#x9;&#x9;&#x9;&#x9;list_add(&#x26;nentry-&#x3E;rule.rlist, &#x26;nwatch-&#x3E;rules);
<br>&#x9;&#x9;&#x9;&#x9;list_add_rcu(&#x26;nentry-&#x3E;list, &#x26;audit_inode_hash[h]);
<br>&#x9;&#x9;&#x9;&#x9;list_replace(&#x26;oentry-&#x3E;rule.list,
<br>&#x9;&#x9;&#x9;&#x9;&#x9;     &#x26;nentry-&#x3E;rule.list);
<br>&#x9;&#x9;&#x9;}
<br>&#x9;&#x9;&#x9;if (oentry-&#x3E;rule.exe)
<br>&#x9;&#x9;&#x9;&#x9;audit_remove_mark(oentry-&#x3E;rule.exe);
<br>
<br>&#x9;&#x9;&#x9;audit_watch_log_rule_change(r, owatch, &#x22;updated_rules&#x22;);
<br>
<br>&#x9;&#x9;&#x9;call_rcu(&#x26;oentry-&#x3E;rcu, audit_free_rule_rcu);
<br>&#x9;&#x9;}
<br>
<br>&#x9;&#x9;audit_remove_watch(owatch);
<br>&#x9;&#x9;goto add_watch_to_parent;/ event applies to a single watch /*
<br>&#x9;}
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;return;
<br>
<br>add_watch_to_parent:
<br>&#x9;list_add(&#x26;nwatch-&#x3E;wlist, &#x26;parent-&#x3E;watches);
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>&#x9;return;
<br>}
<br>
<br>*/ Remove all watches &#x26; rules associated with a parent that is going away. /*
<br>static void audit_remove_parent_watches(struct audit_parentparent)
<br>{
<br>&#x9;struct audit_watchw,nextw;
<br>&#x9;struct audit_kruler,nextr;
<br>&#x9;struct audit_entrye;
<br>
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>&#x9;list_for_each_entry_safe(w, nextw, &#x26;parent-&#x3E;watches, wlist) {
<br>&#x9;&#x9;list_for_each_entry_safe(r, nextr, &#x26;w-&#x3E;rules, rlist) {
<br>&#x9;&#x9;&#x9;e = container_of(r, struct audit_entry, rule);
<br>&#x9;&#x9;&#x9;audit_watch_log_rule_change(r, w, &#x22;remove_rule&#x22;);
<br>&#x9;&#x9;&#x9;if (e-&#x3E;rule.exe)
<br>&#x9;&#x9;&#x9;&#x9;audit_remove_mark(e-&#x3E;rule.exe);
<br>&#x9;&#x9;&#x9;list_del(&#x26;r-&#x3E;rlist);
<br>&#x9;&#x9;&#x9;list_del(&#x26;r-&#x3E;list);
<br>&#x9;&#x9;&#x9;list_del_rcu(&#x26;e-&#x3E;list);
<br>&#x9;&#x9;&#x9;call_rcu(&#x26;e-&#x3E;rcu, audit_free_rule_rcu);
<br>&#x9;&#x9;}
<br>&#x9;&#x9;audit_remove_watch(w);
<br>&#x9;}
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;fsnotify_destroy_mark(&#x26;parent-&#x3E;mark, audit_watch_group);
<br>}
<br>
<br>*/ Get path information necessary for adding watches. /*
<br>static int audit_get_nd(struct audit_watchwatch, struct pathparent)
<br>{
<br>&#x9;struct dentryd = kern_path_locked(watch-&#x3E;path, parent);
<br>&#x9;if (IS_ERR(d))
<br>&#x9;&#x9;return PTR_ERR(d);
<br>&#x9;inode_unlock(d_backing_inode(parent-&#x3E;dentry));
<br>&#x9;if (d_is_positive(d)) {
<br>&#x9;&#x9;*/ update watch filter fields /*
<br>&#x9;&#x9;watch-&#x3E;dev = d_backing_inode(d)-&#x3E;i_sb-&#x3E;s_dev;
<br>&#x9;&#x9;watch-&#x3E;ino = d_backing_inode(d)-&#x3E;i_ino;
<br>&#x9;}
<br>&#x9;dput(d);
<br>&#x9;return 0;
<br>}
<br>
<br>*/ Associate the given rule with an existing parent.
<br> Caller must hold audit_filter_mutex. /*
<br>static void audit_add_to_parent(struct audit_krulekrule,
<br>&#x9;&#x9;&#x9;&#x9;struct audit_parentparent)
<br>{
<br>&#x9;struct audit_watchw,watch = krule-&#x3E;watch;
<br>&#x9;int watch_found = 0;
<br>
<br>&#x9;BUG_ON(!mutex_is_locked(&#x26;audit_filter_mutex));
<br>
<br>&#x9;list_for_each_entry(w, &#x26;parent-&#x3E;watches, wlist) {
<br>&#x9;&#x9;if (strcmp(watch-&#x3E;path, w-&#x3E;path))
<br>&#x9;&#x9;&#x9;continue;
<br>
<br>&#x9;&#x9;watch_found = 1;
<br>
<br>&#x9;&#x9;*/ put krule&#x27;s ref to temporary watch /*
<br>&#x9;&#x9;audit_put_watch(watch);
<br>
<br>&#x9;&#x9;audit_get_watch(w);
<br>&#x9;&#x9;krule-&#x3E;watch = watch = w;
<br>
<br>&#x9;&#x9;audit_put_parent(parent);
<br>&#x9;&#x9;break;
<br>&#x9;}
<br>
<br>&#x9;if (!watch_found) {
<br>&#x9;&#x9;watch-&#x3E;parent = parent;
<br>
<br>&#x9;&#x9;audit_get_watch(watch);
<br>&#x9;&#x9;list_add(&#x26;watch-&#x3E;wlist, &#x26;parent-&#x3E;watches);
<br>&#x9;}
<br>&#x9;list_add(&#x26;krule-&#x3E;rlist, &#x26;watch-&#x3E;rules);
<br>}
<br>
<br>*/ Find a matching watch entry, or add this one.
<br> Caller must hold audit_filter_mutex. /*
<br>int audit_add_watch(struct audit_krulekrule, struct list_head*list)
<br>{
<br>&#x9;struct audit_watchwatch = krule-&#x3E;watch;
<br>&#x9;struct audit_parentparent;
<br>&#x9;struct path parent_path;
<br>&#x9;int h, ret = 0;
<br>
<br>&#x9;mutex_unlock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;*/ Avoid calling path_lookup under audit_filter_mutex. /*
<br>&#x9;ret = audit_get_nd(watch, &#x26;parent_path);
<br>
<br>&#x9;*/ caller expects mutex locked /*
<br>&#x9;mutex_lock(&#x26;audit_filter_mutex);
<br>
<br>&#x9;if (ret)
<br>&#x9;&#x9;return ret;
<br>
<br>&#x9;*/ either find an old parent or attach a new one /*
<br>&#x9;parent = audit_find_parent(d_backing_inode(parent_path.dentry));
<br>&#x9;if (!parent) {
<br>&#x9;&#x9;parent = audit_init_parent(&#x26;parent_path);
<br>&#x9;&#x9;if (IS_ERR(parent)) {
<br>&#x9;&#x9;&#x9;ret = PTR_ERR(parent);
<br>&#x9;&#x9;&#x9;goto error;
<br>&#x9;&#x9;}
<br>&#x9;}
<br>
<br>&#x9;audit_add_to_parent(krule, parent);
<br>
<br>&#x9;h = audit_hash_ino((u32)watch-&#x3E;ino);
<br>&#x9;*list = &#x26;audit_inode_hash[h];
<br>error:
<br>&#x9;path_put(&#x26;parent_path);
<br>&#x9;return ret;
<br>}
<br>
<br>void audit_remove_watch_rule(struct audit_krulekrule)
<br>{
<br>&#x9;struct audit_watchwatch = krule-&#x3E;watch;
<br>&#x9;struct audit_parentparent = watch-&#x3E;parent;
<br>
<br>&#x9;list_del(&#x26;krule-&#x3E;rlist);
<br>
<br>&#x9;if (list_empty(&#x26;watch-&#x3E;rules)) {
<br>&#x9;&#x9;audit_remove_watch(watch);
<br>
<br>&#x9;&#x9;if (list_empty(&#x26;parent-&#x3E;watches)) {
<br>&#x9;&#x9;&#x9;audit_get_parent(parent);
<br>&#x9;&#x9;&#x9;fsnotify_destroy_mark(&#x26;parent-&#x3E;mark, audit_watch_group);
<br>&#x9;&#x9;&#x9;audit_put_parent(parent);
<br>&#x9;&#x9;}
<br>&#x9;}
<br>}
<br>
<br>*/ Update watch data in audit rules based on fsnotify events. /*
<br>static int audit_watch_handle_event(struct fsnotify_groupgroup,
<br>&#x9;&#x9;&#x9;&#x9;    struct inodeto_tell,
<br>&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markinode_mark,
<br>&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markvfsmount_mark,
<br>&#x9;&#x9;&#x9;&#x9;    u32 mask, voiddata, int data_type,
<br>&#x9;&#x9;&#x9;&#x9;    const unsigned chardname, u32 cookie)
<br>{
<br>&#x9;struct inodeinode;
<br>&#x9;struct audit_parentparent;
<br>
<br>&#x9;parent = container_of(inode_mark, struct audit_parent, mark);
<br>
<br>&#x9;BUG_ON(group != audit_watch_group);
<br>
<br>&#x9;switch (data_type) {
<br>&#x9;case (FSNOTIFY_EVENT_PATH):
<br>&#x9;&#x9;inode = d_backing_inode(((struct path)data)-&#x3E;dentry);
<br>&#x9;&#x9;break;
<br>&#x9;case (FSNOTIFY_EVENT_INODE):
<br>&#x9;&#x9;inode = (struct inode)data;
<br>&#x9;&#x9;break;
<br>&#x9;default:
<br>&#x9;&#x9;BUG();
<br>&#x9;&#x9;inode = NULL;
<br>&#x9;&#x9;break;
<br>&#x9;};
<br>
<br>&#x9;if (mask &#x26; (FS_CREATE|FS_MOVED_TO) &#x26;&#x26; inode)
<br>&#x9;&#x9;audit_update_watch(parent, dname, inode-&#x3E;i_sb-&#x3E;s_dev, inode-&#x3E;i_ino, 0);
<br>&#x9;else if (mask &#x26; (FS_DELETE|FS_MOVED_FROM))
<br>&#x9;&#x9;audit_update_watch(parent, dname, AUDIT_DEV_UNSET, AUDIT_INO_UNSET, 1);
<br>&#x9;else if (mask &#x26; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
<br>&#x9;&#x9;audit_remove_parent_watches(parent);
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>static const struct fsnotify_ops audit_watch_fsnotify_ops = {
<br>&#x9;.handle_event = &#x9;audit_watch_handle_event,
<br>};
<br>
<br>static int __init audit_watch_init(void)
<br>{
<br>&#x9;audit_watch_group = fsnotify_alloc_group(&#x26;audit_watch_fsnotify_ops);
<br>&#x9;if (IS_ERR(audit_watch_group)) {
<br>&#x9;&#x9;audit_watch_group = NULL;
<br>&#x9;&#x9;audit_panic(&#x22;cannot create audit fsnotify group&#x22;);
<br>&#x9;}
<br>&#x9;return 0;
<br>}
<br>device_initcall(audit_watch_init);
<br>
<br>int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold)
<br>{
<br>&#x9;struct audit_fsnotify_markaudit_mark;
<br>&#x9;charpathname;
<br>
<br>&#x9;pathname = kstrdup(audit_mark_path(old-&#x3E;exe), GFP_KERNEL);
<br>&#x9;if (!pathname)
<br>&#x9;&#x9;return -ENOMEM;
<br>
<br>&#x9;audit_mark = audit_alloc_mark(new, pathname, strlen(pathname));
<br>&#x9;if (IS_ERR(audit_mark)) {
<br>&#x9;&#x9;kfree(pathname);
<br>&#x9;&#x9;return PTR_ERR(audit_mark);
<br>&#x9;}
<br>&#x9;new-&#x3E;exe = audit_mark;
<br>
<br>&#x9;return 0;
<br>}
<br>
<br>int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark)
<br>{
<br>&#x9;struct fileexe_file;
<br>&#x9;unsigned long ino;
<br>&#x9;dev_t dev;
<br>
<br>&#x9;rcu_read_lock();
<br>&#x9;exe_file = rcu_dereference(tsk-&#x3E;mm-&#x3E;exe_file);
<br>&#x9;ino = exe_file-&#x3E;f_inode-&#x3E;i_ino;
<br>&#x9;dev = exe_file-&#x3E;f_inode-&#x3E;i_sb-&#x3E;s_dev;
<br>&#x9;rcu_read_unlock();
<br>&#x9;return audit_mark_compare(mark, ino, dev);
<br>}
<br>*/
<br>
<br> Simple stack backtrace regression test module
<br>
<br> (C) Copyright 2008 Intel Corporation
<br> Author: Arjan van de Ven &#x3C;arjan@linux.intel.com&#x3E;
<br>
<br> This program is free software; you can redistribute it and/or
<br> modify it under the terms of the GNU General Public License
<br> as published by the Free Software Foundation; version 2
<br> of the License.
<br> /*
<br>
<br>#include &#x3C;linux/completion.h&#x3E;
<br>#include &#x3C;linux/delay.h&#x3E;
<br>#include &#x3C;linux/interrupt.h&#x3E;
<br>#include &#x3C;linux/module.h&#x3E;
<br>#include &#x3C;linux/sched.h&#x3E;
<br>#include &#x3C;linux/stacktrace.h&#x3E;
<br>
<br>static void backtrace_test_normal(void)
<br>{
<br>&#x9;pr_info(&#x22;Testing a backtrace from process context.\n&#x22;);
<br>&#x9;pr_info(&#x22;The following trace is a kernel self test and not a bug!\n&#x22;);
<br>
<br>&#x9;dump_stack();
<br>}
<br>
<br>static DECLARE_COMPLETION(backtrace_work);
<br>
<br>static void backtrace_test_irq_callback(unsigned long data)
<br>{
<br>&#x9;dump_stack();
<br>&#x9;complete(&#x26;backtrace_work);
<br>}
<br>
<br>static DECLARE_TASKLET(backtrace_tasklet, &#x26;backtrace_test_irq_callback, 0);
<br>
<br>static void backtrace_test_irq(void)
<br>{
<br>&#x9;pr_info(&#x22;Testing a backtrace from irq context.\n&#x22;);
<br>&#x9;pr_info(&#x22;The following trace is a kernel self test and not a bug!\n&#x22;);
<br>
<br>&#x9;init_completion(&#x26;backtrace_work);
<br>&#x9;tasklet_schedule(&#x26;backtrace_tasklet);
<br>&#x9;wait_for_completion(&#x26;backtrace_work);
<br>}
<br>
<br>#ifdef CONFIG_STACKTRACE
<br>static void backtrace_test_saved(void)
<br>{
<br>&#x9;struct stack_trace trace;
<br>&#x9;unsigned long entries[8];
<br>
<br>&#x9;pr_info(&#x22;Testing a saved backtrace.\n&#x22;);
<br>&#x9;pr_info(&#x22;The following trace is a kernel self test and not a bug!\n&#x22;);
<br>
<br>&#x9;trace.nr_entries = 0;
<br>&#x9;trace.max_entries = ARRAY_SIZE(entries);
<br>&#x9;trace.entries = entries;
<br>&#x9;trace.skip = 0;
<br>
<br>&#x9;save_stack_trace(&#x26;trace);
<br>&#x9;print_stack_trace(&#x26;trace, 0);
<br>}
<br>#else
<br>static void backtrace_test_saved(void)
<br>{
<br>&#x9;pr_info(&#x22;Saved backtrace test skipped.\n&#x22;);
<br>}
<br>#endif
<br>
<br>static int backtrace_regression_test(void)
<br>{
<br>&#x9;pr_info(&#x22;====[ backtrace testing ]===========\n&#x22;);
<br>
<br>&#x9;backtrace_test_normal();
<br>&#x9;backtrace_test_irq();
<br>&#x9;backtrace_test_saved();
<br>
<br>&#x9;pr_info(&#x22;====[ end of backtrace testing ]====\n&#x22;);
<br>&#x9;return 0;
<br>}
<br>
<br>static void exitf(void)
<br>{
<br>}
<br>
<br>module_init(backtrace_regression_test);
<br>module_exit(exitf);
<br>MODULE_LICENSE(&#x22;GPL&#x22;);
<br>MODULE_AUTHOR(&#x22;Arjan van de Ven &#x3C;arjan@linux.intel.com&#x3E;&#x22;);
<br>*/
