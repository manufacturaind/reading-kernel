
 kexec.c - kexec_load system call
 Copyright (C) 2002-2004 Eric Biederman  <ebiederm@xmission.com>

 This source code is licensed under the GNU General Public License,
 Version 2.  See the file COPYING for more details.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/capability.h>
#include <linux/mm.h>
#include <linux/file.h>
#include <linux/kexec.h>
#include <linux/mutex.h>
#include <linux/list.h>
#include <linux/syscalls.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>

#include "kexec_internal.h"

static int copy_user_segment_list(struct kimageimage,
				  unsigned long nr_segments,
				  struct kexec_segment __usersegments)
{
	int ret;
	size_t segment_bytes;

	*/ Read in the segments /*
	image->nr_segments = nr_segments;
	segment_bytes = nr_segments sizeof(*segments);
	ret = copy_from_user(image->segment, segments, segment_bytes);
	if (ret)
		ret = -EFAULT;

	return ret;
}

static int kimage_alloc_init(struct kimage*rimage, unsigned long entry,
			     unsigned long nr_segments,
			     struct kexec_segment __usersegments,
			     unsigned long flags)
{
	int ret;
	struct kimageimage;
	bool kexec_on_panic = flags & KEXEC_ON_CRASH;

	if (kexec_on_panic) {
		*/ Verify we have a valid entry point /*
		if ((entry < crashk_res.start) || (entry > crashk_res.end))
			return -EADDRNOTAVAIL;
	}

	*/ Allocate and initialize a controlling structure /*
	image = do_kimage_alloc_init();
	if (!image)
		return -ENOMEM;

	image->start = entry;

	ret = copy_user_segment_list(image, nr_segments, segments);
	if (ret)
		goto out_free_image;

	if (kexec_on_panic) {
		*/ Enable special crash kernel control page alloc policy. /*
		image->control_page = crashk_res.start;
		image->type = KEXEC_TYPE_CRASH;
	}

	ret = sanity_check_segment_list(image);
	if (ret)
		goto out_free_image;

	*/
	 Find a location for the control code buffer, and add it
	 the vector of segments so that it's pages will also be
	 counted as destination pages.
	 /*
	ret = -ENOMEM;
	image->control_code_page = kimage_alloc_control_pages(image,
					   get_order(KEXEC_CONTROL_PAGE_SIZE));
	if (!image->control_code_page) {
		pr_err("Could not allocate control_code_buffer\n");
		goto out_free_image;
	}

	if (!kexec_on_panic) {
		image->swap_page = kimage_alloc_control_pages(image, 0);
		if (!image->swap_page) {
			pr_err("Could not allocate swap buffer\n");
			goto out_free_control_pages;
		}
	}

	*rimage = image;
	return 0;
out_free_control_pages:
	kimage_free_page_list(&image->control_pages);
out_free_image:
	kfree(image);
	return ret;
}

*/
 Exec Kernel system call: for obvious reasons only root may call it.

 This call breaks up into three pieces.
 - A generic part which loads the new kernel from the current
   address space, and very carefully places the data in the
   allocated pages.

 - A generic part that interacts with the kernel and tells all of
   the devices to shut down.  Preventing on-going dmas, and placing
   the devices in a consistent state so a later kernel can
   reinitialize them.

 - A machine specific part that includes the syscall number
   and then copies the image to it's final destination.  And
   jumps into the image at entry.

 kexec does not sync, or unmount filesystems so if you need
 that to happen you need to do that yourself.
 /*

SYSCALL_DEFINE4(kexec_load, unsigned long, entry, unsigned long, nr_segments,
		struct kexec_segment __user, segments, unsigned long, flags)
{
	struct kimage*dest_image,image;
	int result;

	*/ We only trust the superuser with rebooting the system. /*
	if (!capable(CAP_SYS_BOOT) || kexec_load_disabled)
		return -EPERM;

	*/
	 Verify we have a legal set of flags
	 This leaves us room for future extensions.
	 /*
	if ((flags & KEXEC_FLAGS) != (flags & ~KEXEC_ARCH_MASK))
		return -EINVAL;

	*/ Verify we are on the appropriate architecture /*
	if (((flags & KEXEC_ARCH_MASK) != KEXEC_ARCH) &&
		((flags & KEXEC_ARCH_MASK) != KEXEC_ARCH_DEFAULT))
		return -EINVAL;

	*/ Put an artificial cap on the number
	 of segments passed to kexec_load.
	 /*
	if (nr_segments > KEXEC_SEGMENT_MAX)
		return -EINVAL;

	image = NULL;
	result = 0;

	*/ Because we write directly to the reserved memory
	 region when loading crash kernels we need a mutex here to
	 prevent multiple crash  kernels from attempting to load
	 simultaneously, and to prevent a crash kernel from loading
	 over the top of a in use crash kernel.
	
	 KISS: always take the mutex.
	 /*
	if (!mutex_trylock(&kexec_mutex))
		return -EBUSY;

	dest_image = &kexec_image;
	if (flags & KEXEC_ON_CRASH)
		dest_image = &kexec_crash_image;
	if (nr_segments > 0) {
		unsigned long i;

		if (flags & KEXEC_ON_CRASH) {
			*/
			 Loading another kernel to switch to if this one
			 crashes.  Free any current crash dump kernel before
			 we corrupt it.
			 /*

			kimage_free(xchg(&kexec_crash_image, NULL));
			result = kimage_alloc_init(&image, entry, nr_segments,
						   segments, flags);
			crash_map_reserved_pages();
		} else {
			*/ Loading another kernel to reboot into. /*

			result = kimage_alloc_init(&image, entry, nr_segments,
						   segments, flags);
		}
		if (result)
			goto out;

		if (flags & KEXEC_PRESERVE_CONTEXT)
			image->preserve_context = 1;
		result = machine_kexec_prepare(image);
		if (result)
			goto out;

		for (i = 0; i < nr_segments; i++) {
			result = kimage_load_segment(image, &image->segment[i]);
			if (result)
				goto out;
		}
		kimage_terminate(image);
		if (flags & KEXEC_ON_CRASH)
			crash_unmap_reserved_pages();
	}
	*/ Install the new kernel, and  Uninstall the old /*
	image = xchg(dest_image, image);

out:
	mutex_unlock(&kexec_mutex);
	kimage_free(image);

	return result;
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE4(kexec_load, compat_ulong_t, entry,
		       compat_ulong_t, nr_segments,
		       struct compat_kexec_segment __user, segments,
		       compat_ulong_t, flags)
{
	struct compat_kexec_segment in;
	struct kexec_segment out, __userksegments;
	unsigned long i, result;

	*/ Don't allow clients that don't understand the native
	 architecture to do anything.
	 /*
	if ((flags & KEXEC_ARCH_MASK) == KEXEC_ARCH_DEFAULT)
		return -EINVAL;

	if (nr_segments > KEXEC_SEGMENT_MAX)
		return -EINVAL;

	ksegments = compat_alloc_user_space(nr_segments sizeof(out));
	for (i = 0; i < nr_segments; i++) {
		result = copy_from_user(&in, &segments[i], sizeof(in));
		if (result)
			return -EFAULT;

		out.buf   = compat_ptr(in.buf);
		out.bufsz = in.bufsz;
		out.mem   = in.mem;
		out.memsz = in.memsz;

		result = copy_to_user(&ksegments[i], &out, sizeof(out));
		if (result)
			return -EFAULT;
	}

	return sys_kexec_load(entry, nr_segments, ksegments, flags);
}
#endif
*/

 kexec.c - kexec system call core code.
 Copyright (C) 2002-2004 Eric Biederman  <ebiederm@xmission.com>

 This source code is licensed under the GNU General Public License,
 Version 2.  See the file COPYING for more details.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/capability.h>
#include <linux/mm.h>
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/kexec.h>
#include <linux/mutex.h>
#include <linux/list.h>
#include <linux/highmem.h>
#include <linux/syscalls.h>
#include <linux/reboot.h>
#include <linux/ioport.h>
#include <linux/hardirq.h>
#include <linux/elf.h>
#include <linux/elfcore.h>
#include <linux/utsname.h>
#include <linux/numa.h>
#include <linux/suspend.h>
#include <linux/device.h>
#include <linux/freezer.h>
#include <linux/pm.h>
#include <linux/cpu.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/console.h>
#include <linux/vmalloc.h>
#include <linux/swap.h>
#include <linux/syscore_ops.h>
#include <linux/compiler.h>
#include <linux/hugetlb.h>

#include <asm/page.h>
#include <asm/sections.h>

#include <crypto/hash.h>
#include <crypto/sha.h>
#include "kexec_internal.h"

DEFINE_MUTEX(kexec_mutex);

*/ Per cpu memory for storing cpu states in case of system crash. /*
note_buf_t __percpucrash_notes;

*/ vmcoreinfo stuff /*
static unsigned char vmcoreinfo_data[VMCOREINFO_BYTES];
u32 vmcoreinfo_note[VMCOREINFO_NOTE_SIZE/4];
size_t vmcoreinfo_size;
size_t vmcoreinfo_max_size = sizeof(vmcoreinfo_data);

*/ Flag to indicate we are going to kexec a new kernel /*
bool kexec_in_progress = false;


*/ Location of the reserved area for the crash kernel /*
struct resource crashk_res = {
	.name  = "Crash kernel",
	.start = 0,
	.end   = 0,
	.flags = IORESOURCE_BUSY | IORESOURCE_SYSTEM_RAM,
	.desc  = IORES_DESC_CRASH_KERNEL
};
struct resource crashk_low_res = {
	.name  = "Crash kernel",
	.start = 0,
	.end   = 0,
	.flags = IORESOURCE_BUSY | IORESOURCE_SYSTEM_RAM,
	.desc  = IORES_DESC_CRASH_KERNEL
};

int kexec_should_crash(struct task_structp)
{
	*/
	 If crash_kexec_post_notifiers is enabled, don't run
	 crash_kexec() here yet, which must be run after panic
	 notifiers in panic().
	 /*
	if (crash_kexec_post_notifiers)
		return 0;
	*/
	 There are 4 panic() calls in do_exit() path, each of which
	 corresponds to each of these 4 conditions.
	 /*
	if (in_interrupt() || !p->pid || is_global_init(p) || panic_on_oops)
		return 1;
	return 0;
}

*/
 When kexec transitions to the new kernel there is a one-to-one
 mapping between physical and virtual addresses.  On processors
 where you can disable the MMU this is trivial, and easy.  For
 others it is still a simple predictable page table to setup.

 In that environment kexec copies the new kernel to its final
 resting place.  This means I can only support memory whose
 physical address can fit in an unsigned long.  In particular
 addresses where (pfn << PAGE_SHIFT) > ULONG_MAX cannot be handled.
 If the assembly stub has more restrictive requirements
 KEXEC_SOURCE_MEMORY_LIMIT and KEXEC_DEST_MEMORY_LIMIT can be
 defined more restrictively in <asm/kexec.h>.

 The code for the transition from the current kernel to the
 the new kernel is placed in the control_code_buffer, whose size
 is given by KEXEC_CONTROL_PAGE_SIZE.  In the best case only a single
 page of memory is necessary, but some architectures require more.
 Because this memory must be identity mapped in the transition from
 virtual to physical addresses it must live in the range
 0 - TASK_SIZE, as only the user space mappings are arbitrarily
 modifiable.

 The assembly stub in the control code buffer is passed a linked list
 of descriptor pages detailing the source pages of the new kernel,
 and the destination addresses of those source pages.  As this data
 structure is not used in the context of the current OS, it must
 be self-contained.

 The code has been made to work with highmem pages and will use a
 destination page in its final resting place (if it happens
 to allocate it).  The end product of this is that most of the
 physical address space, and most of RAM can be used.

 Future directions include:
  - allocating a page table with the control code buffer identity
    mapped, to simplify machine_kexec and make kexec_on_panic more
    reliable.
 /*

*/
 KIMAGE_NO_DEST is an impossible destination address..., for
 allocating pages whose destination address we do not care about.
 /*
#define KIMAGE_NO_DEST (-1UL)

static struct pagekimage_alloc_page(struct kimageimage,
				       gfp_t gfp_mask,
				       unsigned long dest);

int sanity_check_segment_list(struct kimageimage)
{
	int result, i;
	unsigned long nr_segments = image->nr_segments;

	*/
	 Verify we have good destination addresses.  The caller is
	 responsible for making certain we don't attempt to load
	 the new image into invalid or reserved areas of RAM.  This
	 just verifies it is an address we can use.
	
	 Since the kernel does everything in page size chunks ensure
	 the destination addresses are page aligned.  Too many
	 special cases crop of when we don't do this.  The most
	 insidious is getting overlapping destination addresses
	 simply because addresses are changed to page size
	 granularity.
	 /*
	result = -EADDRNOTAVAIL;
	for (i = 0; i < nr_segments; i++) {
		unsigned long mstart, mend;

		mstart = image->segment[i].mem;
		mend   = mstart + image->segment[i].memsz;
		if ((mstart & ~PAGE_MASK) || (mend & ~PAGE_MASK))
			return result;
		if (mend >= KEXEC_DESTINATION_MEMORY_LIMIT)
			return result;
	}

	*/ Verify our destination addresses do not overlap.
	 If we alloed overlapping destination addresses
	 through very weird things can happen with no
	 easy explanation as one segment stops on another.
	 /*
	result = -EINVAL;
	for (i = 0; i < nr_segments; i++) {
		unsigned long mstart, mend;
		unsigned long j;

		mstart = image->segment[i].mem;
		mend   = mstart + image->segment[i].memsz;
		for (j = 0; j < i; j++) {
			unsigned long pstart, pend;

			pstart = image->segment[j].mem;
			pend   = pstart + image->segment[j].memsz;
			*/ Do the segments overlap ? /*
			if ((mend > pstart) && (mstart < pend))
				return result;
		}
	}

	*/ Ensure our buffer sizes are strictly less than
	 our memory sizes.  This should always be the case,
	 and it is easier to check up front than to be surprised
	 later on.
	 /*
	result = -EINVAL;
	for (i = 0; i < nr_segments; i++) {
		if (image->segment[i].bufsz > image->segment[i].memsz)
			return result;
	}

	*/
	 Verify we have good destination addresses.  Normally
	 the caller is responsible for making certain we don't
	 attempt to load the new image into invalid or reserved
	 areas of RAM.  But crash kernels are preloaded into a
	 reserved area of ram.  We must ensure the addresses
	 are in the reserved area otherwise preloading the
	 kernel could corrupt things.
	 /*

	if (image->type == KEXEC_TYPE_CRASH) {
		result = -EADDRNOTAVAIL;
		for (i = 0; i < nr_segments; i++) {
			unsigned long mstart, mend;

			mstart = image->segment[i].mem;
			mend = mstart + image->segment[i].memsz - 1;
			*/ Ensure we are within the crash kernel limits /*
			if ((mstart < crashk_res.start) ||
			    (mend > crashk_res.end))
				return result;
		}
	}

	return 0;
}

struct kimagedo_kimage_alloc_init(void)
{
	struct kimageimage;

	*/ Allocate a controlling structure /*
	image = kzalloc(sizeof(*image), GFP_KERNEL);
	if (!image)
		return NULL;

	image->head = 0;
	image->entry = &image->head;
	image->last_entry = &image->head;
	image->control_page = ~0;/ By default this does not apply /*
	image->type = KEXEC_TYPE_DEFAULT;

	*/ Initialize the list of control pages /*
	INIT_LIST_HEAD(&image->control_pages);

	*/ Initialize the list of destination pages /*
	INIT_LIST_HEAD(&image->dest_pages);

	*/ Initialize the list of unusable pages /*
	INIT_LIST_HEAD(&image->unusable_pages);

	return image;
}

int kimage_is_destination_range(struct kimageimage,
					unsigned long start,
					unsigned long end)
{
	unsigned long i;

	for (i = 0; i < image->nr_segments; i++) {
		unsigned long mstart, mend;

		mstart = image->segment[i].mem;
		mend = mstart + image->segment[i].memsz;
		if ((end > mstart) && (start < mend))
			return 1;
	}

	return 0;
}

static struct pagekimage_alloc_pages(gfp_t gfp_mask, unsigned int order)
{
	struct pagepages;

	pages = alloc_pages(gfp_mask, order);
	if (pages) {
		unsigned int count, i;

		pages->mapping = NULL;
		set_page_private(pages, order);
		count = 1 << order;
		for (i = 0; i < count; i++)
			SetPageReserved(pages + i);
	}

	return pages;
}

static void kimage_free_pages(struct pagepage)
{
	unsigned int order, count, i;

	order = page_private(page);
	count = 1 << order;
	for (i = 0; i < count; i++)
		ClearPageReserved(page + i);
	__free_pages(page, order);
}

void kimage_free_page_list(struct list_headlist)
{
	struct pagepage,next;

	list_for_each_entry_safe(page, next, list, lru) {
		list_del(&page->lru);
		kimage_free_pages(page);
	}
}

static struct pagekimage_alloc_normal_control_pages(struct kimageimage,
							unsigned int order)
{
	*/ Control pages are special, they are the intermediaries
	 that are needed while we copy the rest of the pages
	 to their final resting place.  As such they must
	 not conflict with either the destination addresses
	 or memory the kernel is already using.
	
	 The only case where we really need more than one of
	 these are for architectures where we cannot disable
	 the MMU and must instead generate an identity mapped
	 page table for all of the memory.
	
	 At worst this runs in O(N) of the image size.
	 /*
	struct list_head extra_pages;
	struct pagepages;
	unsigned int count;

	count = 1 << order;
	INIT_LIST_HEAD(&extra_pages);

	*/ Loop while I can allocate a page and the page allocated
	 is a destination page.
	 /*
	do {
		unsigned long pfn, epfn, addr, eaddr;

		pages = kimage_alloc_pages(KEXEC_CONTROL_MEMORY_GFP, order);
		if (!pages)
			break;
		pfn   = page_to_pfn(pages);
		epfn  = pfn + count;
		addr  = pfn << PAGE_SHIFT;
		eaddr = epfn << PAGE_SHIFT;
		if ((epfn >= (KEXEC_CONTROL_MEMORY_LIMIT >> PAGE_SHIFT)) ||
			      kimage_is_destination_range(image, addr, eaddr)) {
			list_add(&pages->lru, &extra_pages);
			pages = NULL;
		}
	} while (!pages);

	if (pages) {
		*/ Remember the allocated page... /*
		list_add(&pages->lru, &image->control_pages);

		*/ Because the page is already in it's destination
		 location we will never allocate another page at
		 that address.  Therefore kimage_alloc_pages
		 will not return it (again) and we don't need
		 to give it an entry in image->segment[].
		 /*
	}
	*/ Deal with the destination pages I have inadvertently allocated.
	
	 Ideally I would convert multi-page allocations into single
	 page allocations, and add everything to image->dest_pages.
	
	 For now it is simpler to just free the pages.
	 /*
	kimage_free_page_list(&extra_pages);

	return pages;
}

static struct pagekimage_alloc_crash_control_pages(struct kimageimage,
						      unsigned int order)
{
	*/ Control pages are special, they are the intermediaries
	 that are needed while we copy the rest of the pages
	 to their final resting place.  As such they must
	 not conflict with either the destination addresses
	 or memory the kernel is already using.
	
	 Control pages are also the only pags we must allocate
	 when loading a crash kernel.  All of the other pages
	 are specified by the segments and we just memcpy
	 into them directly.
	
	 The only case where we really need more than one of
	 these are for architectures where we cannot disable
	 the MMU and must instead generate an identity mapped
	 page table for all of the memory.
	
	 Given the low demand this implements a very simple
	 allocator that finds the first hole of the appropriate
	 size in the reserved memory region, and allocates all
	 of the memory up to and including the hole.
	 /*
	unsigned long hole_start, hole_end, size;
	struct pagepages;

	pages = NULL;
	size = (1 << order) << PAGE_SHIFT;
	hole_start = (image->control_page + (size - 1)) & ~(size - 1);
	hole_end   = hole_start + size - 1;
	while (hole_end <= crashk_res.end) {
		unsigned long i;

		if (hole_end > KEXEC_CRASH_CONTROL_MEMORY_LIMIT)
			break;
		*/ See if I overlap any of the segments /*
		for (i = 0; i < image->nr_segments; i++) {
			unsigned long mstart, mend;

			mstart = image->segment[i].mem;
			mend   = mstart + image->segment[i].memsz - 1;
			if ((hole_end >= mstart) && (hole_start <= mend)) {
				*/ Advance the hole to the end of the segment /*
				hole_start = (mend + (size - 1)) & ~(size - 1);
				hole_end   = hole_start + size - 1;
				break;
			}
		}
		*/ If I don't overlap any segments I have found my hole! /*
		if (i == image->nr_segments) {
			pages = pfn_to_page(hole_start >> PAGE_SHIFT);
			image->control_page = hole_end;
			break;
		}
	}

	return pages;
}


struct pagekimage_alloc_control_pages(struct kimageimage,
					 unsigned int order)
{
	struct pagepages = NULL;

	switch (image->type) {
	case KEXEC_TYPE_DEFAULT:
		pages = kimage_alloc_normal_control_pages(image, order);
		break;
	case KEXEC_TYPE_CRASH:
		pages = kimage_alloc_crash_control_pages(image, order);
		break;
	}

	return pages;
}

static int kimage_add_entry(struct kimageimage, kimage_entry_t entry)
{
	if (*image->entry != 0)
		image->entry++;

	if (image->entry == image->last_entry) {
		kimage_entry_tind_page;
		struct pagepage;

		page = kimage_alloc_page(image, GFP_KERNEL, KIMAGE_NO_DEST);
		if (!page)
			return -ENOMEM;

		ind_page = page_address(page);
		*image->entry = virt_to_phys(ind_page) | IND_INDIRECTION;
		image->entry = ind_page;
		image->last_entry = ind_page +
				      ((PAGE_SIZE/sizeof(kimage_entry_t)) - 1);
	}
	*image->entry = entry;
	image->entry++;
	*image->entry = 0;

	return 0;
}

static int kimage_set_destination(struct kimageimage,
				   unsigned long destination)
{
	int result;

	destination &= PAGE_MASK;
	result = kimage_add_entry(image, destination | IND_DESTINATION);

	return result;
}


static int kimage_add_page(struct kimageimage, unsigned long page)
{
	int result;

	page &= PAGE_MASK;
	result = kimage_add_entry(image, page | IND_SOURCE);

	return result;
}


static void kimage_free_extra_pages(struct kimageimage)
{
	*/ Walk through and free any extra destination pages I may have /*
	kimage_free_page_list(&image->dest_pages);

	*/ Walk through and free any unusable pages I have cached /*
	kimage_free_page_list(&image->unusable_pages);

}
void kimage_terminate(struct kimageimage)
{
	if (*image->entry != 0)
		image->entry++;

	*image->entry = IND_DONE;
}

#define for_each_kimage_entry(image, ptr, entry) \
	for (ptr = &image->head; (entry =ptr) && !(entry & IND_DONE); \
		ptr = (entry & IND_INDIRECTION) ? \
			phys_to_virt((entry & PAGE_MASK)) : ptr + 1)

static void kimage_free_entry(kimage_entry_t entry)
{
	struct pagepage;

	page = pfn_to_page(entry >> PAGE_SHIFT);
	kimage_free_pages(page);
}

void kimage_free(struct kimageimage)
{
	kimage_entry_tptr, entry;
	kimage_entry_t ind = 0;

	if (!image)
		return;

	kimage_free_extra_pages(image);
	for_each_kimage_entry(image, ptr, entry) {
		if (entry & IND_INDIRECTION) {
			*/ Free the previous indirection page /*
			if (ind & IND_INDIRECTION)
				kimage_free_entry(ind);
			*/ Save this indirection page until we are
			 done with it.
			 /*
			ind = entry;
		} else if (entry & IND_SOURCE)
			kimage_free_entry(entry);
	}
	*/ Free the final indirection page /*
	if (ind & IND_INDIRECTION)
		kimage_free_entry(ind);

	*/ Handle any machine specific cleanup /*
	machine_kexec_cleanup(image);

	*/ Free the kexec control pages... /*
	kimage_free_page_list(&image->control_pages);

	*/
	 Free up any temporary buffers allocated. This might hit if
	 error occurred much later after buffer allocation.
	 /*
	if (image->file_mode)
		kimage_file_post_load_cleanup(image);

	kfree(image);
}

static kimage_entry_tkimage_dst_used(struct kimageimage,
					unsigned long page)
{
	kimage_entry_tptr, entry;
	unsigned long destination = 0;

	for_each_kimage_entry(image, ptr, entry) {
		if (entry & IND_DESTINATION)
			destination = entry & PAGE_MASK;
		else if (entry & IND_SOURCE) {
			if (page == destination)
				return ptr;
			destination += PAGE_SIZE;
		}
	}

	return NULL;
}

static struct pagekimage_alloc_page(struct kimageimage,
					gfp_t gfp_mask,
					unsigned long destination)
{
	*/
	 Here we implement safeguards to ensure that a source page
	 is not copied to its destination page before the data on
	 the destination page is no longer useful.
	
	 To do this we maintain the invariant that a source page is
	 either its own destination page, or it is not a
	 destination page at all.
	
	 That is slightly stronger than required, but the proof
	 that no problems will not occur is trivial, and the
	 implementation is simply to verify.
	
	 When allocating all pages normally this algorithm will run
	 in O(N) time, but in the worst case it will run in O(N^2)
	 time.   If the runtime is a problem the data structures can
	 be fixed.
	 /*
	struct pagepage;
	unsigned long addr;

	*/
	 Walk through the list of destination pages, and see if I
	 have a match.
	 /*
	list_for_each_entry(page, &image->dest_pages, lru) {
		addr = page_to_pfn(page) << PAGE_SHIFT;
		if (addr == destination) {
			list_del(&page->lru);
			return page;
		}
	}
	page = NULL;
	while (1) {
		kimage_entry_told;

		*/ Allocate a page, if we run out of memory give up /*
		page = kimage_alloc_pages(gfp_mask, 0);
		if (!page)
			return NULL;
		*/ If the page cannot be used file it away /*
		if (page_to_pfn(page) >
				(KEXEC_SOURCE_MEMORY_LIMIT >> PAGE_SHIFT)) {
			list_add(&page->lru, &image->unusable_pages);
			continue;
		}
		addr = page_to_pfn(page) << PAGE_SHIFT;

		*/ If it is the destination page we want use it /*
		if (addr == destination)
			break;

		*/ If the page is not a destination page use it /*
		if (!kimage_is_destination_range(image, addr,
						  addr + PAGE_SIZE))
			break;

		*/
		 I know that the page is someones destination page.
		 See if there is already a source page for this
		 destination page.  And if so swap the source pages.
		 /*
		old = kimage_dst_used(image, addr);
		if (old) {
			*/ If so move it /*
			unsigned long old_addr;
			struct pageold_page;

			old_addr =old & PAGE_MASK;
			old_page = pfn_to_page(old_addr >> PAGE_SHIFT);
			copy_highpage(page, old_page);
			*old = addr | (*old & ~PAGE_MASK);

			*/ The old page I have found cannot be a
			 destination page, so return it if it's
			 gfp_flags honor the ones passed in.
			 /*
			if (!(gfp_mask & __GFP_HIGHMEM) &&
			    PageHighMem(old_page)) {
				kimage_free_pages(old_page);
				continue;
			}
			addr = old_addr;
			page = old_page;
			break;
		}
		*/ Place the page on the destination list, to be used later /*
		list_add(&page->lru, &image->dest_pages);
	}

	return page;
}

static int kimage_load_normal_segment(struct kimageimage,
					 struct kexec_segmentsegment)
{
	unsigned long maddr;
	size_t ubytes, mbytes;
	int result;
	unsigned char __userbuf = NULL;
	unsigned charkbuf = NULL;

	result = 0;
	if (image->file_mode)
		kbuf = segment->kbuf;
	else
		buf = segment->buf;
	ubytes = segment->bufsz;
	mbytes = segment->memsz;
	maddr = segment->mem;

	result = kimage_set_destination(image, maddr);
	if (result < 0)
		goto out;

	while (mbytes) {
		struct pagepage;
		charptr;
		size_t uchunk, mchunk;

		page = kimage_alloc_page(image, GFP_HIGHUSER, maddr);
		if (!page) {
			result  = -ENOMEM;
			goto out;
		}
		result = kimage_add_page(image, page_to_pfn(page)
								<< PAGE_SHIFT);
		if (result < 0)
			goto out;

		ptr = kmap(page);
		*/ Start with a clear page /*
		clear_page(ptr);
		ptr += maddr & ~PAGE_MASK;
		mchunk = min_t(size_t, mbytes,
				PAGE_SIZE - (maddr & ~PAGE_MASK));
		uchunk = min(ubytes, mchunk);

		*/ For file based kexec, source pages are in kernel memory /*
		if (image->file_mode)
			memcpy(ptr, kbuf, uchunk);
		else
			result = copy_from_user(ptr, buf, uchunk);
		kunmap(page);
		if (result) {
			result = -EFAULT;
			goto out;
		}
		ubytes -= uchunk;
		maddr  += mchunk;
		if (image->file_mode)
			kbuf += mchunk;
		else
			buf += mchunk;
		mbytes -= mchunk;
	}
out:
	return result;
}

static int kimage_load_crash_segment(struct kimageimage,
					struct kexec_segmentsegment)
{
	*/ For crash dumps kernels we simply copy the data from
	 user space to it's destination.
	 We do things a page at a time for the sake of kmap.
	 /*
	unsigned long maddr;
	size_t ubytes, mbytes;
	int result;
	unsigned char __userbuf = NULL;
	unsigned charkbuf = NULL;

	result = 0;
	if (image->file_mode)
		kbuf = segment->kbuf;
	else
		buf = segment->buf;
	ubytes = segment->bufsz;
	mbytes = segment->memsz;
	maddr = segment->mem;
	while (mbytes) {
		struct pagepage;
		charptr;
		size_t uchunk, mchunk;

		page = pfn_to_page(maddr >> PAGE_SHIFT);
		if (!page) {
			result  = -ENOMEM;
			goto out;
		}
		ptr = kmap(page);
		ptr += maddr & ~PAGE_MASK;
		mchunk = min_t(size_t, mbytes,
				PAGE_SIZE - (maddr & ~PAGE_MASK));
		uchunk = min(ubytes, mchunk);
		if (mchunk > uchunk) {
			*/ Zero the trailing part of the page /*
			memset(ptr + uchunk, 0, mchunk - uchunk);
		}

		*/ For file based kexec, source pages are in kernel memory /*
		if (image->file_mode)
			memcpy(ptr, kbuf, uchunk);
		else
			result = copy_from_user(ptr, buf, uchunk);
		kexec_flush_icache_page(page);
		kunmap(page);
		if (result) {
			result = -EFAULT;
			goto out;
		}
		ubytes -= uchunk;
		maddr  += mchunk;
		if (image->file_mode)
			kbuf += mchunk;
		else
			buf += mchunk;
		mbytes -= mchunk;
	}
out:
	return result;
}

int kimage_load_segment(struct kimageimage,
				struct kexec_segmentsegment)
{
	int result = -ENOMEM;

	switch (image->type) {
	case KEXEC_TYPE_DEFAULT:
		result = kimage_load_normal_segment(image, segment);
		break;
	case KEXEC_TYPE_CRASH:
		result = kimage_load_crash_segment(image, segment);
		break;
	}

	return result;
}

struct kimagekexec_image;
struct kimagekexec_crash_image;
int kexec_load_disabled;

*/
 No panic_cpu check version of crash_kexec().  This function is called
 only when panic_cpu holds the current CPU number; this is the only CPU
 which processes crash_kexec routines.
 /*
void __crash_kexec(struct pt_regsregs)
{
	*/ Take the kexec_mutex here to prevent sys_kexec_load
	 running on one cpu from replacing the crash kernel
	 we are using after a panic on a different cpu.
	
	 If the crash kernel was not located in a fixed area
	 of memory the xchg(&kexec_crash_image) would be
	 sufficient.  But since I reuse the memory...
	 /*
	if (mutex_trylock(&kexec_mutex)) {
		if (kexec_crash_image) {
			struct pt_regs fixed_regs;

			crash_setup_regs(&fixed_regs, regs);
			crash_save_vmcoreinfo();
			machine_crash_shutdown(&fixed_regs);
			machine_kexec(kexec_crash_image);
		}
		mutex_unlock(&kexec_mutex);
	}
}

void crash_kexec(struct pt_regsregs)
{
	int old_cpu, this_cpu;

	*/
	 Only one CPU is allowed to execute the crash_kexec() code as with
	 panic().  Otherwise parallel calls of panic() and crash_kexec()
	 may stop each other.  To exclude them, we use panic_cpu here too.
	 /*
	this_cpu = raw_smp_processor_id();
	old_cpu = atomic_cmpxchg(&panic_cpu, PANIC_CPU_INVALID, this_cpu);
	if (old_cpu == PANIC_CPU_INVALID) {
		*/ This is the 1st CPU which comes here, so go ahead. /*
		__crash_kexec(regs);

		*/
		 Reset panic_cpu to allow another panic()/crash_kexec()
		 call.
		 /*
		atomic_set(&panic_cpu, PANIC_CPU_INVALID);
	}
}

size_t crash_get_memory_size(void)
{
	size_t size = 0;

	mutex_lock(&kexec_mutex);
	if (crashk_res.end != crashk_res.start)
		size = resource_size(&crashk_res);
	mutex_unlock(&kexec_mutex);
	return size;
}

void __weak crash_free_reserved_phys_range(unsigned long begin,
					   unsigned long end)
{
	unsigned long addr;

	for (addr = begin; addr < end; addr += PAGE_SIZE)
		free_reserved_page(pfn_to_page(addr >> PAGE_SHIFT));
}

int crash_shrink_memory(unsigned long new_size)
{
	int ret = 0;
	unsigned long start, end;
	unsigned long old_size;
	struct resourceram_res;

	mutex_lock(&kexec_mutex);

	if (kexec_crash_image) {
		ret = -ENOENT;
		goto unlock;
	}
	start = crashk_res.start;
	end = crashk_res.end;
	old_size = (end == 0) ? 0 : end - start + 1;
	if (new_size >= old_size) {
		ret = (new_size == old_size) ? 0 : -EINVAL;
		goto unlock;
	}

	ram_res = kzalloc(sizeof(*ram_res), GFP_KERNEL);
	if (!ram_res) {
		ret = -ENOMEM;
		goto unlock;
	}

	start = roundup(start, KEXEC_CRASH_MEM_ALIGN);
	end = roundup(start + new_size, KEXEC_CRASH_MEM_ALIGN);

	crash_map_reserved_pages();
	crash_free_reserved_phys_range(end, crashk_res.end);

	if ((start == end) && (crashk_res.parent != NULL))
		release_resource(&crashk_res);

	ram_res->start = end;
	ram_res->end = crashk_res.end;
	ram_res->flags = IORESOURCE_BUSY | IORESOURCE_SYSTEM_RAM;
	ram_res->name = "System RAM";

	crashk_res.end = end - 1;

	insert_resource(&iomem_resource, ram_res);
	crash_unmap_reserved_pages();

unlock:
	mutex_unlock(&kexec_mutex);
	return ret;
}

static u32append_elf_note(u32buf, charname, unsigned type, voiddata,
			    size_t data_len)
{
	struct elf_note note;

	note.n_namesz = strlen(name) + 1;
	note.n_descsz = data_len;
	note.n_type   = type;
	memcpy(buf, &note, sizeof(note));
	buf += (sizeof(note) + 3)/4;
	memcpy(buf, name, note.n_namesz);
	buf += (note.n_namesz + 3)/4;
	memcpy(buf, data, note.n_descsz);
	buf += (note.n_descsz + 3)/4;

	return buf;
}

static void final_note(u32buf)
{
	struct elf_note note;

	note.n_namesz = 0;
	note.n_descsz = 0;
	note.n_type   = 0;
	memcpy(buf, &note, sizeof(note));
}

void crash_save_cpu(struct pt_regsregs, int cpu)
{
	struct elf_prstatus prstatus;
	u32buf;

	if ((cpu < 0) || (cpu >= nr_cpu_ids))
		return;

	*/ Using ELF notes here is opportunistic.
	 I need a well defined structure format
	 for the data I pass, and I need tags
	 on the data to indicate what information I have
	 squirrelled away.  ELF notes happen to provide
	 all of that, so there is no need to invent something new.
	 /*
	buf = (u32)per_cpu_ptr(crash_notes, cpu);
	if (!buf)
		return;
	memset(&prstatus, 0, sizeof(prstatus));
	prstatus.pr_pid = current->pid;
	elf_core_copy_kernel_regs(&prstatus.pr_reg, regs);
	buf = append_elf_note(buf, KEXEC_CORE_NOTE_NAME, NT_PRSTATUS,
			      &prstatus, sizeof(prstatus));
	final_note(buf);
}

static int __init crash_notes_memory_init(void)
{
	*/ Allocate memory for saving cpu registers. /*
	size_t size, align;

	*/
	 crash_notes could be allocated across 2 vmalloc pages when percpu
	 is vmalloc based . vmalloc doesn't guarantee 2 continuous vmalloc
	 pages are also on 2 continuous physical pages. In this case the
	 2nd part of crash_notes in 2nd page could be lost since only the
	 starting address and size of crash_notes are exported through sysfs.
	 Here round up the size of crash_notes to the nearest power of two
	 and pass it to __alloc_percpu as align value. This can make sure
	 crash_notes is allocated inside one physical page.
	 /*
	size = sizeof(note_buf_t);
	align = min(roundup_pow_of_two(sizeof(note_buf_t)), PAGE_SIZE);

	*/
	 Break compile if size is bigger than PAGE_SIZE since crash_notes
	 definitely will be in 2 pages with that.
	 /*
	BUILD_BUG_ON(size > PAGE_SIZE);

	crash_notes = __alloc_percpu(size, align);
	if (!crash_notes) {
		pr_warn("Memory allocation for saving cpu register states failed\n");
		return -ENOMEM;
	}
	return 0;
}
subsys_initcall(crash_notes_memory_init);


*/
 parsing the "crashkernel" commandline

 this code is intended to be called from architecture specific code
 /*


*/
 This function parses command lines in the format

   crashkernel=ramsize-range:size[,...][@offset]

 The function returns 0 on success and -EINVAL on failure.
 /*
static int __init parse_crashkernel_mem(charcmdline,
					unsigned long long system_ram,
					unsigned long longcrash_size,
					unsigned long longcrash_base)
{
	charcur = cmdline,tmp;

	*/ for each entry of the comma-separated list /*
	do {
		unsigned long long start, end = ULLONG_MAX, size;

		*/ get the start of the range /*
		start = memparse(cur, &tmp);
		if (cur == tmp) {
			pr_warn("crashkernel: Memory value expected\n");
			return -EINVAL;
		}
		cur = tmp;
		if (*cur != '-') {
			pr_warn("crashkernel: '-' expected\n");
			return -EINVAL;
		}
		cur++;

		*/ if no ':' is here, than we read the end /*
		if (*cur != ':') {
			end = memparse(cur, &tmp);
			if (cur == tmp) {
				pr_warn("crashkernel: Memory value expected\n");
				return -EINVAL;
			}
			cur = tmp;
			if (end <= start) {
				pr_warn("crashkernel: end <= start\n");
				return -EINVAL;
			}
		}

		if (*cur != ':') {
			pr_warn("crashkernel: ':' expected\n");
			return -EINVAL;
		}
		cur++;

		size = memparse(cur, &tmp);
		if (cur == tmp) {
			pr_warn("Memory value expected\n");
			return -EINVAL;
		}
		cur = tmp;
		if (size >= system_ram) {
			pr_warn("crashkernel: invalid size\n");
			return -EINVAL;
		}

		*/ match ? /*
		if (system_ram >= start && system_ram < end) {
			*crash_size = size;
			break;
		}
	} while (*cur++ == ',');

	if (*crash_size > 0) {
		while (*cur &&cur != ' ' &&cur != '@')
			cur++;
		if (*cur == '@') {
			cur++;
			*crash_base = memparse(cur, &tmp);
			if (cur == tmp) {
				pr_warn("Memory value expected after '@'\n");
				return -EINVAL;
			}
		}
	}

	return 0;
}

*/
 That function parses "simple" (old) crashkernel command lines like

	crashkernel=size[@offset]

 It returns 0 on success and -EINVAL on failure.
 /*
static int __init parse_crashkernel_simple(charcmdline,
					   unsigned long longcrash_size,
					   unsigned long longcrash_base)
{
	charcur = cmdline;

	*crash_size = memparse(cmdline, &cur);
	if (cmdline == cur) {
		pr_warn("crashkernel: memory value expected\n");
		return -EINVAL;
	}

	if (*cur == '@')
		*crash_base = memparse(cur+1, &cur);
	else if (*cur != ' ' &&cur != '\0') {
		pr_warn("crashkernel: unrecognized char: %c\n",cur);
		return -EINVAL;
	}

	return 0;
}

#define SUFFIX_HIGH 0
#define SUFFIX_LOW  1
#define SUFFIX_NULL 2
static __initdata charsuffix_tbl[] = {
	[SUFFIX_HIGH] = ",high",
	[SUFFIX_LOW]  = ",low",
	[SUFFIX_NULL] = NULL,
};

*/
 That function parses "suffix"  crashkernel command lines like

	crashkernel=size,[high|low]

 It returns 0 on success and -EINVAL on failure.
 /*
static int __init parse_crashkernel_suffix(charcmdline,
					   unsigned long long	*crash_size,
					   const charsuffix)
{
	charcur = cmdline;

	*crash_size = memparse(cmdline, &cur);
	if (cmdline == cur) {
		pr_warn("crashkernel: memory value expected\n");
		return -EINVAL;
	}

	*/ check with suffix /*
	if (strncmp(cur, suffix, strlen(suffix))) {
		pr_warn("crashkernel: unrecognized char: %c\n",cur);
		return -EINVAL;
	}
	cur += strlen(suffix);
	if (*cur != ' ' &&cur != '\0') {
		pr_warn("crashkernel: unrecognized char: %c\n",cur);
		return -EINVAL;
	}

	return 0;
}

static __init charget_last_crashkernel(charcmdline,
			     const charname,
			     const charsuffix)
{
	charp = cmdline,ck_cmdline = NULL;

	*/ find crashkernel and use the last one if there are more /*
	p = strstr(p, name);
	while (p) {
		charend_p = strchr(p, ' ');
		charq;

		if (!end_p)
			end_p = p + strlen(p);

		if (!suffix) {
			int i;

			*/ skip the one with any known suffix /*
			for (i = 0; suffix_tbl[i]; i++) {
				q = end_p - strlen(suffix_tbl[i]);
				if (!strncmp(q, suffix_tbl[i],
					     strlen(suffix_tbl[i])))
					goto next;
			}
			ck_cmdline = p;
		} else {
			q = end_p - strlen(suffix);
			if (!strncmp(q, suffix, strlen(suffix)))
				ck_cmdline = p;
		}
next:
		p = strstr(p+1, name);
	}

	if (!ck_cmdline)
		return NULL;

	return ck_cmdline;
}

static int __init __parse_crashkernel(charcmdline,
			     unsigned long long system_ram,
			     unsigned long longcrash_size,
			     unsigned long longcrash_base,
			     const charname,
			     const charsuffix)
{
	char	*first_colon,first_space;
	char	*ck_cmdline;

	BUG_ON(!crash_size || !crash_base);
	*crash_size = 0;
	*crash_base = 0;

	ck_cmdline = get_last_crashkernel(cmdline, name, suffix);

	if (!ck_cmdline)
		return -EINVAL;

	ck_cmdline += strlen(name);

	if (suffix)
		return parse_crashkernel_suffix(ck_cmdline, crash_size,
				suffix);
	*/
	 if the commandline contains a ':', then that's the extended
	 syntax -- if not, it must be the classic syntax
	 /*
	first_colon = strchr(ck_cmdline, ':');
	first_space = strchr(ck_cmdline, ' ');
	if (first_colon && (!first_space || first_colon < first_space))
		return parse_crashkernel_mem(ck_cmdline, system_ram,
				crash_size, crash_base);

	return parse_crashkernel_simple(ck_cmdline, crash_size, crash_base);
}

*/
 That function is the entry point for command line parsing and should be
 called from the arch-specific code.
 /*
int __init parse_crashkernel(charcmdline,
			     unsigned long long system_ram,
			     unsigned long longcrash_size,
			     unsigned long longcrash_base)
{
	return __parse_crashkernel(cmdline, system_ram, crash_size, crash_base,
					"crashkernel=", NULL);
}

int __init parse_crashkernel_high(charcmdline,
			     unsigned long long system_ram,
			     unsigned long longcrash_size,
			     unsigned long longcrash_base)
{
	return __parse_crashkernel(cmdline, system_ram, crash_size, crash_base,
				"crashkernel=", suffix_tbl[SUFFIX_HIGH]);
}

int __init parse_crashkernel_low(charcmdline,
			     unsigned long long system_ram,
			     unsigned long longcrash_size,
			     unsigned long longcrash_base)
{
	return __parse_crashkernel(cmdline, system_ram, crash_size, crash_base,
				"crashkernel=", suffix_tbl[SUFFIX_LOW]);
}

static void update_vmcoreinfo_note(void)
{
	u32buf = vmcoreinfo_note;

	if (!vmcoreinfo_size)
		return;
	buf = append_elf_note(buf, VMCOREINFO_NOTE_NAME, 0, vmcoreinfo_data,
			      vmcoreinfo_size);
	final_note(buf);
}

void crash_save_vmcoreinfo(void)
{
	vmcoreinfo_append_str("CRASHTIME=%ld\n", get_seconds());
	update_vmcoreinfo_note();
}

void vmcoreinfo_append_str(const charfmt, ...)
{
	va_list args;
	char buf[0x50];
	size_t r;

	va_start(args, fmt);
	r = vscnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);

	r = min(r, vmcoreinfo_max_size - vmcoreinfo_size);

	memcpy(&vmcoreinfo_data[vmcoreinfo_size], buf, r);

	vmcoreinfo_size += r;
}

*/
 provide an empty default implementation here -- architecture
 code may override this
 /*
void __weak arch_crash_save_vmcoreinfo(void)
{}

unsigned long __weak paddr_vmcoreinfo_note(void)
{
	return __pa((unsigned long)(char)&vmcoreinfo_note);
}

static int __init crash_save_vmcoreinfo_init(void)
{
	VMCOREINFO_OSRELEASE(init_uts_ns.name.release);
	VMCOREINFO_PAGESIZE(PAGE_SIZE);

	VMCOREINFO_SYMBOL(init_uts_ns);
	VMCOREINFO_SYMBOL(node_online_map);
#ifdef CONFIG_MMU
	VMCOREINFO_SYMBOL(swapper_pg_dir);
#endif
	VMCOREINFO_SYMBOL(_stext);
	VMCOREINFO_SYMBOL(vmap_area_list);

#ifndef CONFIG_NEED_MULTIPLE_NODES
	VMCOREINFO_SYMBOL(mem_map);
	VMCOREINFO_SYMBOL(contig_page_data);
#endif
#ifdef CONFIG_SPARSEMEM
	VMCOREINFO_SYMBOL(mem_section);
	VMCOREINFO_LENGTH(mem_section, NR_SECTION_ROOTS);
	VMCOREINFO_STRUCT_SIZE(mem_section);
	VMCOREINFO_OFFSET(mem_section, section_mem_map);
#endif
	VMCOREINFO_STRUCT_SIZE(page);
	VMCOREINFO_STRUCT_SIZE(pglist_data);
	VMCOREINFO_STRUCT_SIZE(zone);
	VMCOREINFO_STRUCT_SIZE(free_area);
	VMCOREINFO_STRUCT_SIZE(list_head);
	VMCOREINFO_SIZE(nodemask_t);
	VMCOREINFO_OFFSET(page, flags);
	VMCOREINFO_OFFSET(page, _count);
	VMCOREINFO_OFFSET(page, mapping);
	VMCOREINFO_OFFSET(page, lru);
	VMCOREINFO_OFFSET(page, _mapcount);
	VMCOREINFO_OFFSET(page, private);
	VMCOREINFO_OFFSET(pglist_data, node_zones);
	VMCOREINFO_OFFSET(pglist_data, nr_zones);
#ifdef CONFIG_FLAT_NODE_MEM_MAP
	VMCOREINFO_OFFSET(pglist_data, node_mem_map);
#endif
	VMCOREINFO_OFFSET(pglist_data, node_start_pfn);
	VMCOREINFO_OFFSET(pglist_data, node_spanned_pages);
	VMCOREINFO_OFFSET(pglist_data, node_id);
	VMCOREINFO_OFFSET(zone, free_area);
	VMCOREINFO_OFFSET(zone, vm_stat);
	VMCOREINFO_OFFSET(zone, spanned_pages);
	VMCOREINFO_OFFSET(free_area, free_list);
	VMCOREINFO_OFFSET(list_head, next);
	VMCOREINFO_OFFSET(list_head, prev);
	VMCOREINFO_OFFSET(vmap_area, va_start);
	VMCOREINFO_OFFSET(vmap_area, list);
	VMCOREINFO_LENGTH(zone.free_area, MAX_ORDER);
	log_buf_kexec_setup();
	VMCOREINFO_LENGTH(free_area.free_list, MIGRATE_TYPES);
	VMCOREINFO_NUMBER(NR_FREE_PAGES);
	VMCOREINFO_NUMBER(PG_lru);
	VMCOREINFO_NUMBER(PG_private);
	VMCOREINFO_NUMBER(PG_swapcache);
	VMCOREINFO_NUMBER(PG_slab);
#ifdef CONFIG_MEMORY_FAILURE
	VMCOREINFO_NUMBER(PG_hwpoison);
#endif
	VMCOREINFO_NUMBER(PG_head_mask);
	VMCOREINFO_NUMBER(PAGE_BUDDY_MAPCOUNT_VALUE);
#ifdef CONFIG_X86
	VMCOREINFO_NUMBER(KERNEL_IMAGE_SIZE);
#endif
#ifdef CONFIG_HUGETLBFS
	VMCOREINFO_SYMBOL(free_huge_page);
#endif

	arch_crash_save_vmcoreinfo();
	update_vmcoreinfo_note();

	return 0;
}

subsys_initcall(crash_save_vmcoreinfo_init);

*/
 Move into place and start executing a preloaded standalone
 executable.  If nothing was preloaded return an error.
 /*
int kernel_kexec(void)
{
	int error = 0;

	if (!mutex_trylock(&kexec_mutex))
		return -EBUSY;
	if (!kexec_image) {
		error = -EINVAL;
		goto Unlock;
	}

#ifdef CONFIG_KEXEC_JUMP
	if (kexec_image->preserve_context) {
		lock_system_sleep();
		pm_prepare_console();
		error = freeze_processes();
		if (error) {
			error = -EBUSY;
			goto Restore_console;
		}
		suspend_console();
		error = dpm_suspend_start(PMSG_FREEZE);
		if (error)
			goto Resume_console;
		*/ At this point, dpm_suspend_start() has been called,
		 butnot* dpm_suspend_end(). Wemust* call
		 dpm_suspend_end() now.  Otherwise, drivers for
		 some devices (e.g. interrupt controllers) become
		 desynchronized with the actual state of the
		 hardware at resume time, and evil weirdness ensues.
		 /*
		error = dpm_suspend_end(PMSG_FREEZE);
		if (error)
			goto Resume_devices;
		error = disable_nonboot_cpus();
		if (error)
			goto Enable_cpus;
		local_irq_disable();
		error = syscore_suspend();
		if (error)
			goto Enable_irqs;
	} else
#endif
	{
		kexec_in_progress = true;
		kernel_restart_prepare(NULL);
		migrate_to_reboot_cpu();

		*/
		 migrate_to_reboot_cpu() disables CPU hotplug assuming that
		 no further code needs to use CPU hotplug (which is true in
		 the reboot case). However, the kexec path depends on using
		 CPU hotplug again; so re-enable it here.
		 /*
		cpu_hotplug_enable();
		pr_emerg("Starting new kernel\n");
		machine_shutdown();
	}

	machine_kexec(kexec_image);

#ifdef CONFIG_KEXEC_JUMP
	if (kexec_image->preserve_context) {
		syscore_resume();
 Enable_irqs:
		local_irq_enable();
 Enable_cpus:
		enable_nonboot_cpus();
		dpm_resume_start(PMSG_RESTORE);
 Resume_devices:
		dpm_resume_end(PMSG_RESTORE);
 Resume_console:
		resume_console();
		thaw_processes();
 Restore_console:
		pm_restore_console();
		unlock_system_sleep();
	}
#endif

 Unlock:
	mutex_unlock(&kexec_mutex);
	return error;
}

*/
 Add and remove page tables for crashkernel memory

 Provide an empty default implementation here -- architecture
 code may override this
 /*
void __weak crash_map_reserved_pages(void)
{}

void __weak crash_unmap_reserved_pages(void)
{}
*/

 kexec: kexec_file_load system call

 Copyright (C) 2014 Red Hat Inc.
 Authors:
      Vivek Goyal <vgoyal@redhat.com>

 This source code is licensed under the GNU General Public License,
 Version 2.  See the file COPYING for more details.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/capability.h>
#include <linux/mm.h>
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/kexec.h>
#include <linux/mutex.h>
#include <linux/list.h>
#include <linux/fs.h>
#include <crypto/hash.h>
#include <crypto/sha.h>
#include <linux/syscalls.h>
#include <linux/vmalloc.h>
#include "kexec_internal.h"

*/
 Declare these symbols weak so that if architecture provides a purgatory,
 these will be overridden.
 /*
char __weak kexec_purgatory[0];
size_t __weak kexec_purgatory_size = 0;

static int kexec_calculate_store_digests(struct kimageimage);

*/ Architectures can provide this probe function /*
int __weak arch_kexec_kernel_image_probe(struct kimageimage, voidbuf,
					 unsigned long buf_len)
{
	return -ENOEXEC;
}

void __weak arch_kexec_kernel_image_load(struct kimageimage)
{
	return ERR_PTR(-ENOEXEC);
}

int __weak arch_kimage_file_post_load_cleanup(struct kimageimage)
{
	return -EINVAL;
}

#ifdef CONFIG_KEXEC_VERIFY_SIG
int __weak arch_kexec_kernel_verify_sig(struct kimageimage, voidbuf,
					unsigned long buf_len)
{
	return -EKEYREJECTED;
}
#endif

*/ Apply relocations of type RELA /*
int __weak
arch_kexec_apply_relocations_add(const Elf_Ehdrehdr, Elf_Shdrsechdrs,
				 unsigned int relsec)
{
	pr_err("RELA relocation unsupported.\n");
	return -ENOEXEC;
}

*/ Apply relocations of type REL /*
int __weak
arch_kexec_apply_relocations(const Elf_Ehdrehdr, Elf_Shdrsechdrs,
			     unsigned int relsec)
{
	pr_err("REL relocation unsupported.\n");
	return -ENOEXEC;
}

*/
 Free up memory used by kernel, initrd, and command line. This is temporary
 memory allocation which is not needed any more after these buffers have
 been loaded into separate segments and have been copied elsewhere.
 /*
void kimage_file_post_load_cleanup(struct kimageimage)
{
	struct purgatory_infopi = &image->purgatory_info;

	vfree(image->kernel_buf);
	image->kernel_buf = NULL;

	vfree(image->initrd_buf);
	image->initrd_buf = NULL;

	kfree(image->cmdline_buf);
	image->cmdline_buf = NULL;

	vfree(pi->purgatory_buf);
	pi->purgatory_buf = NULL;

	vfree(pi->sechdrs);
	pi->sechdrs = NULL;

	*/ See if architecture has anything to cleanup post load /*
	arch_kimage_file_post_load_cleanup(image);

	*/
	 Above call should have called into bootloader to free up
	 any data stored in kimage->image_loader_data. It should
	 be ok now to free it up.
	 /*
	kfree(image->image_loader_data);
	image->image_loader_data = NULL;
}

*/
 In file mode list of segments is prepared by kernel. Copy relevant
 data from user space, do error checking, prepare segment list
 /*
static int
kimage_file_prepare_segments(struct kimageimage, int kernel_fd, int initrd_fd,
			     const char __usercmdline_ptr,
			     unsigned long cmdline_len, unsigned flags)
{
	int ret = 0;
	voidldata;
	loff_t size;

	ret = kernel_read_file_from_fd(kernel_fd, &image->kernel_buf,
				       &size, INT_MAX, READING_KEXEC_IMAGE);
	if (ret)
		return ret;
	image->kernel_buf_len = size;

	*/ Call arch image probe handlers /*
	ret = arch_kexec_kernel_image_probe(image, image->kernel_buf,
					    image->kernel_buf_len);
	if (ret)
		goto out;

#ifdef CONFIG_KEXEC_VERIFY_SIG
	ret = arch_kexec_kernel_verify_sig(image, image->kernel_buf,
					   image->kernel_buf_len);
	if (ret) {
		pr_debug("kernel signature verification failed.\n");
		goto out;
	}
	pr_debug("kernel signature verification successful.\n");
#endif
	*/ It is possible that there no initramfs is being loaded /*
	if (!(flags & KEXEC_FILE_NO_INITRAMFS)) {
		ret = kernel_read_file_from_fd(initrd_fd, &image->initrd_buf,
					       &size, INT_MAX,
					       READING_KEXEC_INITRAMFS);
		if (ret)
			goto out;
		image->initrd_buf_len = size;
	}

	if (cmdline_len) {
		image->cmdline_buf = kzalloc(cmdline_len, GFP_KERNEL);
		if (!image->cmdline_buf) {
			ret = -ENOMEM;
			goto out;
		}

		ret = copy_from_user(image->cmdline_buf, cmdline_ptr,
				     cmdline_len);
		if (ret) {
			ret = -EFAULT;
			goto out;
		}

		image->cmdline_buf_len = cmdline_len;

		*/ command line should be a string with last byte null /*
		if (image->cmdline_buf[cmdline_len - 1] != '\0') {
			ret = -EINVAL;
			goto out;
		}
	}

	*/ Call arch image load handlers /*
	ldata = arch_kexec_kernel_image_load(image);

	if (IS_ERR(ldata)) {
		ret = PTR_ERR(ldata);
		goto out;
	}

	image->image_loader_data = ldata;
out:
	*/ In case of error, free up all allocated memory in this function /*
	if (ret)
		kimage_file_post_load_cleanup(image);
	return ret;
}

static int
kimage_file_alloc_init(struct kimage*rimage, int kernel_fd,
		       int initrd_fd, const char __usercmdline_ptr,
		       unsigned long cmdline_len, unsigned long flags)
{
	int ret;
	struct kimageimage;
	bool kexec_on_panic = flags & KEXEC_FILE_ON_CRASH;

	image = do_kimage_alloc_init();
	if (!image)
		return -ENOMEM;

	image->file_mode = 1;

	if (kexec_on_panic) {
		*/ Enable special crash kernel control page alloc policy. /*
		image->control_page = crashk_res.start;
		image->type = KEXEC_TYPE_CRASH;
	}

	ret = kimage_file_prepare_segments(image, kernel_fd, initrd_fd,
					   cmdline_ptr, cmdline_len, flags);
	if (ret)
		goto out_free_image;

	ret = sanity_check_segment_list(image);
	if (ret)
		goto out_free_post_load_bufs;

	ret = -ENOMEM;
	image->control_code_page = kimage_alloc_control_pages(image,
					   get_order(KEXEC_CONTROL_PAGE_SIZE));
	if (!image->control_code_page) {
		pr_err("Could not allocate control_code_buffer\n");
		goto out_free_post_load_bufs;
	}

	if (!kexec_on_panic) {
		image->swap_page = kimage_alloc_control_pages(image, 0);
		if (!image->swap_page) {
			pr_err("Could not allocate swap buffer\n");
			goto out_free_control_pages;
		}
	}

	*rimage = image;
	return 0;
out_free_control_pages:
	kimage_free_page_list(&image->control_pages);
out_free_post_load_bufs:
	kimage_file_post_load_cleanup(image);
out_free_image:
	kfree(image);
	return ret;
}

SYSCALL_DEFINE5(kexec_file_load, int, kernel_fd, int, initrd_fd,
		unsigned long, cmdline_len, const char __user, cmdline_ptr,
		unsigned long, flags)
{
	int ret = 0, i;
	struct kimage*dest_image,image;

	*/ We only trust the superuser with rebooting the system. /*
	if (!capable(CAP_SYS_BOOT) || kexec_load_disabled)
		return -EPERM;

	*/ Make sure we have a legal set of flags /*
	if (flags != (flags & KEXEC_FILE_FLAGS))
		return -EINVAL;

	image = NULL;

	if (!mutex_trylock(&kexec_mutex))
		return -EBUSY;

	dest_image = &kexec_image;
	if (flags & KEXEC_FILE_ON_CRASH)
		dest_image = &kexec_crash_image;

	if (flags & KEXEC_FILE_UNLOAD)
		goto exchange;

	*/
	 In case of crash, new kernel gets loaded in reserved region. It is
	 same memory where old crash kernel might be loaded. Free any
	 current crash dump kernel before we corrupt it.
	 /*
	if (flags & KEXEC_FILE_ON_CRASH)
		kimage_free(xchg(&kexec_crash_image, NULL));

	ret = kimage_file_alloc_init(&image, kernel_fd, initrd_fd, cmdline_ptr,
				     cmdline_len, flags);
	if (ret)
		goto out;

	ret = machine_kexec_prepare(image);
	if (ret)
		goto out;

	ret = kexec_calculate_store_digests(image);
	if (ret)
		goto out;

	for (i = 0; i < image->nr_segments; i++) {
		struct kexec_segmentksegment;

		ksegment = &image->segment[i];
		pr_debug("Loading segment %d: buf=0x%p bufsz=0x%zx mem=0x%lx memsz=0x%zx\n",
			 i, ksegment->buf, ksegment->bufsz, ksegment->mem,
			 ksegment->memsz);

		ret = kimage_load_segment(image, &image->segment[i]);
		if (ret)
			goto out;
	}

	kimage_terminate(image);

	*/
	 Free up any temporary buffers allocated which are not needed
	 after image has been loaded
	 /*
	kimage_file_post_load_cleanup(image);
exchange:
	image = xchg(dest_image, image);
out:
	mutex_unlock(&kexec_mutex);
	kimage_free(image);
	return ret;
}

static int locate_mem_hole_top_down(unsigned long start, unsigned long end,
				    struct kexec_bufkbuf)
{
	struct kimageimage = kbuf->image;
	unsigned long temp_start, temp_end;

	temp_end = min(end, kbuf->buf_max);
	temp_start = temp_end - kbuf->memsz;

	do {
		*/ align down start /*
		temp_start = temp_start & (~(kbuf->buf_align - 1));

		if (temp_start < start || temp_start < kbuf->buf_min)
			return 0;

		temp_end = temp_start + kbuf->memsz - 1;

		*/
		 Make sure this does not conflict with any of existing
		 segments
		 /*
		if (kimage_is_destination_range(image, temp_start, temp_end)) {
			temp_start = temp_start - PAGE_SIZE;
			continue;
		}

		*/ We found a suitable memory range /*
		break;
	} while (1);

	*/ If we are here, we found a suitable memory range /*
	kbuf->mem = temp_start;

	*/ Success, stop navigating through remaining System RAM ranges /*
	return 1;
}

static int locate_mem_hole_bottom_up(unsigned long start, unsigned long end,
				     struct kexec_bufkbuf)
{
	struct kimageimage = kbuf->image;
	unsigned long temp_start, temp_end;

	temp_start = max(start, kbuf->buf_min);

	do {
		temp_start = ALIGN(temp_start, kbuf->buf_align);
		temp_end = temp_start + kbuf->memsz - 1;

		if (temp_end > end || temp_end > kbuf->buf_max)
			return 0;
		*/
		 Make sure this does not conflict with any of existing
		 segments
		 /*
		if (kimage_is_destination_range(image, temp_start, temp_end)) {
			temp_start = temp_start + PAGE_SIZE;
			continue;
		}

		*/ We found a suitable memory range /*
		break;
	} while (1);

	*/ If we are here, we found a suitable memory range /*
	kbuf->mem = temp_start;

	*/ Success, stop navigating through remaining System RAM ranges /*
	return 1;
}

static int locate_mem_hole_callback(u64 start, u64 end, voidarg)
{
	struct kexec_bufkbuf = (struct kexec_buf)arg;
	unsigned long sz = end - start + 1;

	*/ Returning 0 will take to next memory range /*
	if (sz < kbuf->memsz)
		return 0;

	if (end < kbuf->buf_min || start > kbuf->buf_max)
		return 0;

	*/
	 Allocate memory top down with-in ram range. Otherwise bottom up
	 allocation.
	 /*
	if (kbuf->top_down)
		return locate_mem_hole_top_down(start, end, kbuf);
	return locate_mem_hole_bottom_up(start, end, kbuf);
}

*/
 Helper function for placing a buffer in a kexec segment. This assumes
 that kexec_mutex is held.
 /*
int kexec_add_buffer(struct kimageimage, charbuffer, unsigned long bufsz,
		     unsigned long memsz, unsigned long buf_align,
		     unsigned long buf_min, unsigned long buf_max,
		     bool top_down, unsigned longload_addr)
{

	struct kexec_segmentksegment;
	struct kexec_buf buf,kbuf;
	int ret;

	*/ Currently adding segment this way is allowed only in file mode /*
	if (!image->file_mode)
		return -EINVAL;

	if (image->nr_segments >= KEXEC_SEGMENT_MAX)
		return -EINVAL;

	*/
	 Make sure we are not trying to add buffer after allocating
	 control pages. All segments need to be placed first before
	 any control pages are allocated. As control page allocation
	 logic goes through list of segments to make sure there are
	 no destination overlaps.
	 /*
	if (!list_empty(&image->control_pages)) {
		WARN_ON(1);
		return -EINVAL;
	}

	memset(&buf, 0, sizeof(struct kexec_buf));
	kbuf = &buf;
	kbuf->image = image;
	kbuf->buffer = buffer;
	kbuf->bufsz = bufsz;

	kbuf->memsz = ALIGN(memsz, PAGE_SIZE);
	kbuf->buf_align = max(buf_align, PAGE_SIZE);
	kbuf->buf_min = buf_min;
	kbuf->buf_max = buf_max;
	kbuf->top_down = top_down;

	*/ Walk the RAM ranges and allocate a suitable range for the buffer /*
	if (image->type == KEXEC_TYPE_CRASH)
		ret = walk_iomem_res_desc(crashk_res.desc,
				IORESOURCE_SYSTEM_RAM | IORESOURCE_BUSY,
				crashk_res.start, crashk_res.end, kbuf,
				locate_mem_hole_callback);
	else
		ret = walk_system_ram_res(0, -1, kbuf,
					  locate_mem_hole_callback);
	if (ret != 1) {
		*/ A suitable memory range could not be found for buffer /*
		return -EADDRNOTAVAIL;
	}

	*/ Found a suitable memory range /*
	ksegment = &image->segment[image->nr_segments];
	ksegment->kbuf = kbuf->buffer;
	ksegment->bufsz = kbuf->bufsz;
	ksegment->mem = kbuf->mem;
	ksegment->memsz = kbuf->memsz;
	image->nr_segments++;
	*load_addr = ksegment->mem;
	return 0;
}

*/ Calculate and store the digest of segments /*
static int kexec_calculate_store_digests(struct kimageimage)
{
	struct crypto_shashtfm;
	struct shash_descdesc;
	int ret = 0, i, j, zero_buf_sz, sha_region_sz;
	size_t desc_size, nullsz;
	chardigest;
	voidzero_buf;
	struct kexec_sha_regionsha_regions;
	struct purgatory_infopi = &image->purgatory_info;

	zero_buf = __va(page_to_pfn(ZERO_PAGE(0)) << PAGE_SHIFT);
	zero_buf_sz = PAGE_SIZE;

	tfm = crypto_alloc_shash("sha256", 0, 0);
	if (IS_ERR(tfm)) {
		ret = PTR_ERR(tfm);
		goto out;
	}

	desc_size = crypto_shash_descsize(tfm) + sizeof(*desc);
	desc = kzalloc(desc_size, GFP_KERNEL);
	if (!desc) {
		ret = -ENOMEM;
		goto out_free_tfm;
	}

	sha_region_sz = KEXEC_SEGMENT_MAX sizeof(struct kexec_sha_region);
	sha_regions = vzalloc(sha_region_sz);
	if (!sha_regions)
		goto out_free_desc;

	desc->tfm   = tfm;
	desc->flags = 0;

	ret = crypto_shash_init(desc);
	if (ret < 0)
		goto out_free_sha_regions;

	digest = kzalloc(SHA256_DIGEST_SIZE, GFP_KERNEL);
	if (!digest) {
		ret = -ENOMEM;
		goto out_free_sha_regions;
	}

	for (j = i = 0; i < image->nr_segments; i++) {
		struct kexec_segmentksegment;

		ksegment = &image->segment[i];
		*/
		 Skip purgatory as it will be modified once we put digest
		 info in purgatory.
		 /*
		if (ksegment->kbuf == pi->purgatory_buf)
			continue;

		ret = crypto_shash_update(desc, ksegment->kbuf,
					  ksegment->bufsz);
		if (ret)
			break;

		*/
		 Assume rest of the buffer is filled with zero and
		 update digest accordingly.
		 /*
		nullsz = ksegment->memsz - ksegment->bufsz;
		while (nullsz) {
			unsigned long bytes = nullsz;

			if (bytes > zero_buf_sz)
				bytes = zero_buf_sz;
			ret = crypto_shash_update(desc, zero_buf, bytes);
			if (ret)
				break;
			nullsz -= bytes;
		}

		if (ret)
			break;

		sha_regions[j].start = ksegment->mem;
		sha_regions[j].len = ksegment->memsz;
		j++;
	}

	if (!ret) {
		ret = crypto_shash_final(desc, digest);
		if (ret)
			goto out_free_digest;
		ret = kexec_purgatory_get_set_symbol(image, "sha_regions",
						sha_regions, sha_region_sz, 0);
		if (ret)
			goto out_free_digest;

		ret = kexec_purgatory_get_set_symbol(image, "sha256_digest",
						digest, SHA256_DIGEST_SIZE, 0);
		if (ret)
			goto out_free_digest;
	}

out_free_digest:
	kfree(digest);
out_free_sha_regions:
	vfree(sha_regions);
out_free_desc:
	kfree(desc);
out_free_tfm:
	kfree(tfm);
out:
	return ret;
}

*/ Actually load purgatory. Lot of code taken from kexec-tools /*
static int __kexec_load_purgatory(struct kimageimage, unsigned long min,
				  unsigned long max, int top_down)
{
	struct purgatory_infopi = &image->purgatory_info;
	unsigned long align, buf_align, bss_align, buf_sz, bss_sz, bss_pad;
	unsigned long memsz, entry, load_addr, curr_load_addr, bss_addr, offset;
	unsigned charbuf_addr,src;
	int i, ret = 0, entry_sidx = -1;
	const Elf_Shdrsechdrs_c;
	Elf_Shdrsechdrs = NULL;
	voidpurgatory_buf = NULL;

	*/
	 sechdrs_c points to section headers in purgatory and are read
	 only. No modifications allowed.
	 /*
	sechdrs_c = (void)pi->ehdr + pi->ehdr->e_shoff;

	*/
	 We can not modify sechdrs_c[] and its fields. It is read only.
	 Copy it over to a local copy where one can store some temporary
	 data and free it at the end. We need to modify ->sh_addr and
	 ->sh_offset fields to keep track of permanent and temporary
	 locations of sections.
	 /*
	sechdrs = vzalloc(pi->ehdr->e_shnum sizeof(Elf_Shdr));
	if (!sechdrs)
		return -ENOMEM;

	memcpy(sechdrs, sechdrs_c, pi->ehdr->e_shnum sizeof(Elf_Shdr));

	*/
	 We seem to have multiple copies of sections. First copy is which
	 is embedded in kernel in read only section. Some of these sections
	 will be copied to a temporary buffer and relocated. And these
	 sections will finally be copied to their final destination at
	 segment load time.
	
	 Use ->sh_offset to reflect section address in memory. It will
	 point to original read only copy if section is not allocatable.
	 Otherwise it will point to temporary copy which will be relocated.
	
	 Use ->sh_addr to contain final address of the section where it
	 will go during execution time.
	 /*
	for (i = 0; i < pi->ehdr->e_shnum; i++) {
		if (sechdrs[i].sh_type == SHT_NOBITS)
			continue;

		sechdrs[i].sh_offset = (unsigned long)pi->ehdr +
						sechdrs[i].sh_offset;
	}

	*/
	 Identify entry point section and make entry relative to section
	 start.
	 /*
	entry = pi->ehdr->e_entry;
	for (i = 0; i < pi->ehdr->e_shnum; i++) {
		if (!(sechdrs[i].sh_flags & SHF_ALLOC))
			continue;

		if (!(sechdrs[i].sh_flags & SHF_EXECINSTR))
			continue;

		*/ Make entry section relative /*
		if (sechdrs[i].sh_addr <= pi->ehdr->e_entry &&
		    ((sechdrs[i].sh_addr + sechdrs[i].sh_size) >
		     pi->ehdr->e_entry)) {
			entry_sidx = i;
			entry -= sechdrs[i].sh_addr;
			break;
		}
	}

	*/ Determine how much memory is needed to load relocatable object. /*
	buf_align = 1;
	bss_align = 1;
	buf_sz = 0;
	bss_sz = 0;

	for (i = 0; i < pi->ehdr->e_shnum; i++) {
		if (!(sechdrs[i].sh_flags & SHF_ALLOC))
			continue;

		align = sechdrs[i].sh_addralign;
		if (sechdrs[i].sh_type != SHT_NOBITS) {
			if (buf_align < align)
				buf_align = align;
			buf_sz = ALIGN(buf_sz, align);
			buf_sz += sechdrs[i].sh_size;
		} else {
			*/ bss section /*
			if (bss_align < align)
				bss_align = align;
			bss_sz = ALIGN(bss_sz, align);
			bss_sz += sechdrs[i].sh_size;
		}
	}

	*/ Determine the bss padding required to align bss properly /*
	bss_pad = 0;
	if (buf_sz & (bss_align - 1))
		bss_pad = bss_align - (buf_sz & (bss_align - 1));

	memsz = buf_sz + bss_pad + bss_sz;

	*/ Allocate buffer for purgatory /*
	purgatory_buf = vzalloc(buf_sz);
	if (!purgatory_buf) {
		ret = -ENOMEM;
		goto out;
	}

	if (buf_align < bss_align)
		buf_align = bss_align;

	*/ Add buffer to segment list /*
	ret = kexec_add_buffer(image, purgatory_buf, buf_sz, memsz,
				buf_align, min, max, top_down,
				&pi->purgatory_load_addr);
	if (ret)
		goto out;

	*/ Load SHF_ALLOC sections /*
	buf_addr = purgatory_buf;
	load_addr = curr_load_addr = pi->purgatory_load_addr;
	bss_addr = load_addr + buf_sz + bss_pad;

	for (i = 0; i < pi->ehdr->e_shnum; i++) {
		if (!(sechdrs[i].sh_flags & SHF_ALLOC))
			continue;

		align = sechdrs[i].sh_addralign;
		if (sechdrs[i].sh_type != SHT_NOBITS) {
			curr_load_addr = ALIGN(curr_load_addr, align);
			offset = curr_load_addr - load_addr;
			*/ We already modifed ->sh_offset to keep src addr /*
			src = (char) sechdrs[i].sh_offset;
			memcpy(buf_addr + offset, src, sechdrs[i].sh_size);

			*/ Store load address and source address of section /*
			sechdrs[i].sh_addr = curr_load_addr;

			*/
			 This section got copied to temporary buffer. Update
			 ->sh_offset accordingly.
			 /*
			sechdrs[i].sh_offset = (unsigned long)(buf_addr + offset);

			*/ Advance to the next address /*
			curr_load_addr += sechdrs[i].sh_size;
		} else {
			bss_addr = ALIGN(bss_addr, align);
			sechdrs[i].sh_addr = bss_addr;
			bss_addr += sechdrs[i].sh_size;
		}
	}

	*/ Update entry point based on load address of text section /*
	if (entry_sidx >= 0)
		entry += sechdrs[entry_sidx].sh_addr;

	*/ Make kernel jump to purgatory after shutdown /*
	image->start = entry;

	*/ Used later to get/set symbol values /*
	pi->sechdrs = sechdrs;

	*/
	 Used later to identify which section is purgatory and skip it
	 from checksumming.
	 /*
	pi->purgatory_buf = purgatory_buf;
	return ret;
out:
	vfree(sechdrs);
	vfree(purgatory_buf);
	return ret;
}

static int kexec_apply_relocations(struct kimageimage)
{
	int i, ret;
	struct purgatory_infopi = &image->purgatory_info;
	Elf_Shdrsechdrs = pi->sechdrs;

	*/ Apply relocations /*
	for (i = 0; i < pi->ehdr->e_shnum; i++) {
		Elf_Shdrsection,symtab;

		if (sechdrs[i].sh_type != SHT_RELA &&
		    sechdrs[i].sh_type != SHT_REL)
			continue;

		*/
		 For section of type SHT_RELA/SHT_REL,
		 ->sh_link contains section header index of associated
		 symbol table. And ->sh_info contains section header
		 index of section to which relocations apply.
		 /*
		if (sechdrs[i].sh_info >= pi->ehdr->e_shnum ||
		    sechdrs[i].sh_link >= pi->ehdr->e_shnum)
			return -ENOEXEC;

		section = &sechdrs[sechdrs[i].sh_info];
		symtab = &sechdrs[sechdrs[i].sh_link];

		if (!(section->sh_flags & SHF_ALLOC))
			continue;

		*/
		 symtab->sh_link contain section header index of associated
		 string table.
		 /*
		if (symtab->sh_link >= pi->ehdr->e_shnum)
			*/ Invalid section number? /*
			continue;

		*/
		 Respective architecture needs to provide support for applying
		 relocations of type SHT_RELA/SHT_REL.
		 /*
		if (sechdrs[i].sh_type == SHT_RELA)
			ret = arch_kexec_apply_relocations_add(pi->ehdr,
							       sechdrs, i);
		else if (sechdrs[i].sh_type == SHT_REL)
			ret = arch_kexec_apply_relocations(pi->ehdr,
							   sechdrs, i);
		if (ret)
			return ret;
	}

	return 0;
}

*/ Load relocatable purgatory object and relocate it appropriately /*
int kexec_load_purgatory(struct kimageimage, unsigned long min,
			 unsigned long max, int top_down,
			 unsigned longload_addr)
{
	struct purgatory_infopi = &image->purgatory_info;
	int ret;

	if (kexec_purgatory_size <= 0)
		return -EINVAL;

	if (kexec_purgatory_size < sizeof(Elf_Ehdr))
		return -ENOEXEC;

	pi->ehdr = (Elf_Ehdr)kexec_purgatory;

	if (memcmp(pi->ehdr->e_ident, ELFMAG, SELFMAG) != 0
	    || pi->ehdr->e_type != ET_REL
	    || !elf_check_arch(pi->ehdr)
	    || pi->ehdr->e_shentsize != sizeof(Elf_Shdr))
		return -ENOEXEC;

	if (pi->ehdr->e_shoff >= kexec_purgatory_size
	    || (pi->ehdr->e_shnum sizeof(Elf_Shdr) >
	    kexec_purgatory_size - pi->ehdr->e_shoff))
		return -ENOEXEC;

	ret = __kexec_load_purgatory(image, min, max, top_down);
	if (ret)
		return ret;

	ret = kexec_apply_relocations(image);
	if (ret)
		goto out;

	*load_addr = pi->purgatory_load_addr;
	return 0;
out:
	vfree(pi->sechdrs);
	vfree(pi->purgatory_buf);
	return ret;
}

static Elf_Symkexec_purgatory_find_symbol(struct purgatory_infopi,
					    const charname)
{
	Elf_Symsyms;
	Elf_Shdrsechdrs;
	Elf_Ehdrehdr;
	int i, k;
	const charstrtab;

	if (!pi->sechdrs || !pi->ehdr)
		return NULL;

	sechdrs = pi->sechdrs;
	ehdr = pi->ehdr;

	for (i = 0; i < ehdr->e_shnum; i++) {
		if (sechdrs[i].sh_type != SHT_SYMTAB)
			continue;

		if (sechdrs[i].sh_link >= ehdr->e_shnum)
			*/ Invalid strtab section number /*
			continue;
		strtab = (char)sechdrs[sechdrs[i].sh_link].sh_offset;
		syms = (Elf_Sym)sechdrs[i].sh_offset;

		*/ Go through symbols for a match /*
		for (k = 0; k < sechdrs[i].sh_size/sizeof(Elf_Sym); k++) {
			if (ELF_ST_BIND(syms[k].st_info) != STB_GLOBAL)
				continue;

			if (strcmp(strtab + syms[k].st_name, name) != 0)
				continue;

			if (syms[k].st_shndx == SHN_UNDEF ||
			    syms[k].st_shndx >= ehdr->e_shnum) {
				pr_debug("Symbol: %s has bad section index %d.\n",
						name, syms[k].st_shndx);
				return NULL;
			}

			*/ Found the symbol we are looking for /*
			return &syms[k];
		}
	}

	return NULL;
}

voidkexec_purgatory_get_symbol_addr(struct kimageimage, const charname)
{
	struct purgatory_infopi = &image->purgatory_info;
	Elf_Symsym;
	Elf_Shdrsechdr;

	sym = kexec_purgatory_find_symbol(pi, name);
	if (!sym)
		return ERR_PTR(-EINVAL);

	sechdr = &pi->sechdrs[sym->st_shndx];

	*/
	 Returns the address where symbol will finally be loaded after
	 kexec_load_segment()
	 /*
	return (void)(sechdr->sh_addr + sym->st_value);
}

*/
 Get or set value of a symbol. If "get_value" is true, symbol value is
 returned in buf otherwise symbol value is set based on value in buf.
 /*
int kexec_purgatory_get_set_symbol(struct kimageimage, const charname,
				   voidbuf, unsigned int size, bool get_value)
{
	Elf_Symsym;
	Elf_Shdrsechdrs;
	struct purgatory_infopi = &image->purgatory_info;
	charsym_buf;

	sym = kexec_purgatory_find_symbol(pi, name);
	if (!sym)
		return -EINVAL;

	if (sym->st_size != size) {
		pr_err("symbol %s size mismatch: expected %lu actual %u\n",
		       name, (unsigned long)sym->st_size, size);
		return -EINVAL;
	}

	sechdrs = pi->sechdrs;

	if (sechdrs[sym->st_shndx].sh_type == SHT_NOBITS) {
		pr_err("symbol %s is in a bss section. Cannot %s\n", name,
		       get_value ? "get" : "set");
		return -EINVAL;
	}

	sym_buf = (unsigned char)sechdrs[sym->st_shndx].sh_offset +
					sym->st_value;

	if (get_value)
		memcpy((void)buf, sym_buf, size);
	else
		memcpy((void)sym_buf, buf, size);

	return 0;
}
*/
/*
#ifndef LINUX_KEXEC_INTERNAL_H
#define LINUX_KEXEC_INTERNAL_H

#include <linux/kexec.h>

struct kimagedo_kimage_alloc_init(void);
int sanity_check_segment_list(struct kimageimage);
void kimage_free_page_list(struct list_headlist);
void kimage_free(struct kimageimage);
int kimage_load_segment(struct kimageimage, struct kexec_segmentsegment);
void kimage_terminate(struct kimageimage);
int kimage_is_destination_range(struct kimageimage,
				unsigned long start, unsigned long end);

extern struct mutex kexec_mutex;

#ifdef CONFIG_KEXEC_FILE
struct kexec_sha_region {
	unsigned long start;
	unsigned long len;
};

*/
 Keeps track of buffer parameters as provided by caller for requesting
 memory placement of buffer.
 /*
struct kexec_buf {
	struct kimageimage;
	charbuffer;
	unsigned long bufsz;
	unsigned long mem;
	unsigned long memsz;
	unsigned long buf_align;
	unsigned long buf_min;
	unsigned long buf_max;
	bool top_down;		*/ allocate from top of memory hole /*
};

void kimage_file_post_load_cleanup(struct kimageimage);
#else*/ CONFIG_KEXEC_FILE /*
static inline void kimage_file_post_load_cleanup(struct kimageimage) { }
#endif*/ CONFIG_KEXEC_FILE /*
#endif*/ LINUX_KEXEC_INTERNAL_H

	kmod, the new module loader (replaces kerneld)
	Kirk Petersen

	Reorganized not to be a daemon by Adam Richter, with guidance
	from Greg Zornetzer.

	Modified to avoid chroot and file sharing problems.
	Mikael Pettersson

	Limit the concurrent number of kmod modprobes to catch loops from
	"modprobe needs a service that is in a module".
	Keith Owens <kaos@ocs.com.au> December 1999

	Unblock all signals when we exec a usermode process.
	Shuu Yamaguchi <shuu@wondernetworkresources.com> December 2000

	call_usermodehelper wait flag, and remove exec_usermodehelper.
	Rusty Russell <rusty@rustcorp.com.au>  Jan 2003
/*
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/syscalls.h>
#include <linux/unistd.h>
#include <linux/kmod.h>
#include <linux/slab.h>
#include <linux/completion.h>
#include <linux/cred.h>
#include <linux/file.h>
#include <linux/fdtable.h>
#include <linux/workqueue.h>
#include <linux/security.h>
#include <linux/mount.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/resource.h>
#include <linux/notifier.h>
#include <linux/suspend.h>
#include <linux/rwsem.h>
#include <linux/ptrace.h>
#include <linux/async.h>
#include <asm/uaccess.h>

#include <trace/events/module.h>

extern int max_threads;

#define CAP_BSET	(void)1
#define CAP_PI		(void)2

static kernel_cap_t usermodehelper_bset = CAP_FULL_SET;
static kernel_cap_t usermodehelper_inheritable = CAP_FULL_SET;
static DEFINE_SPINLOCK(umh_sysctl_lock);
static DECLARE_RWSEM(umhelper_sem);

#ifdef CONFIG_MODULES

*/
	modprobe_path is set via /proc/sys.
/*
char modprobe_path[KMOD_PATH_LEN] = "/sbin/modprobe";

static void free_modprobe_argv(struct subprocess_infoinfo)
{
	kfree(info->argv[3]);/ check call_modprobe() /*
	kfree(info->argv);
}

static int call_modprobe(charmodule_name, int wait)
{
	struct subprocess_infoinfo;
	static charenvp[] = {
		"HOME=/",
		"TERM=linux",
		"PATH=/sbin:/usr/sbin:/bin:/usr/bin",
		NULL
	};

	char*argv = kmalloc(sizeof(char[5]), GFP_KERNEL);
	if (!argv)
		goto out;

	module_name = kstrdup(module_name, GFP_KERNEL);
	if (!module_name)
		goto free_argv;

	argv[0] = modprobe_path;
	argv[1] = "-q";
	argv[2] = "--";
	argv[3] = module_name;	*/ check free_modprobe_argv() /*
	argv[4] = NULL;

	info = call_usermodehelper_setup(modprobe_path, argv, envp, GFP_KERNEL,
					 NULL, free_modprobe_argv, NULL);
	if (!info)
		goto free_module_name;

	return call_usermodehelper_exec(info, wait | UMH_KILLABLE);

free_module_name:
	kfree(module_name);
free_argv:
	kfree(argv);
out:
	return -ENOMEM;
}

*/
 __request_module - try to load a kernel module
 @wait: wait (or not) for the operation to complete
 @fmt: printf style format string for the name of the module
 @...: arguments as specified in the format string

 Load a module using the user mode module loader. The function returns
 zero on success or a negative errno code or positive exit code from
 "modprobe" on failure. Note that a successful module load does not mean
 the module did not then unload and exit on an error of its own. Callers
 must check that the service they requested is now available not blindly
 invoke it.

 If module auto-loading support is disabled then this function
 becomes a no-operation.
 /*
int __request_module(bool wait, const charfmt, ...)
{
	va_list args;
	char module_name[MODULE_NAME_LEN];
	unsigned int max_modprobes;
	int ret;
	static atomic_t kmod_concurrent = ATOMIC_INIT(0);
#define MAX_KMOD_CONCURRENT 50	*/ Completely arbitrary value - KAO /*
	static int kmod_loop_msg;

	*/
	 We don't allow synchronous module loading from async.  Module
	 init may invoke async_synchronize_full() which will end up
	 waiting for this task which already is waiting for the module
	 loading to complete, leading to a deadlock.
	 /*
	WARN_ON_ONCE(wait && current_is_async());

	if (!modprobe_path[0])
		return 0;

	va_start(args, fmt);
	ret = vsnprintf(module_name, MODULE_NAME_LEN, fmt, args);
	va_end(args);
	if (ret >= MODULE_NAME_LEN)
		return -ENAMETOOLONG;

	ret = security_kernel_module_request(module_name);
	if (ret)
		return ret;

	*/ If modprobe needs a service that is in a module, we get a recursive
	 loop.  Limit the number of running kmod threads to max_threads/2 or
	 MAX_KMOD_CONCURRENT, whichever is the smaller.  A cleaner method
	 would be to run the parents of this process, counting how many times
	 kmod was invoked.  That would mean accessing the internals of the
	 process tables to get the command line, proc_pid_cmdline is static
	 and it is not worth changing the proc code just to handle this case. 
	 KAO.
	
	 "trace the ppid" is simple, but will fail if someone's
	 parent exits.  I think this is as good as it gets. --RR
	 /*
	max_modprobes = min(max_threads/2, MAX_KMOD_CONCURRENT);
	atomic_inc(&kmod_concurrent);
	if (atomic_read(&kmod_concurrent) > max_modprobes) {
		*/ We may be blaming an innocent here, but unlikely /*
		if (kmod_loop_msg < 5) {
			printk(KERN_ERR
			       "request_module: runaway loop modprobe %s\n",
			       module_name);
			kmod_loop_msg++;
		}
		atomic_dec(&kmod_concurrent);
		return -ENOMEM;
	}

	trace_module_request(module_name, wait, _RET_IP_);

	ret = call_modprobe(module_name, wait ? UMH_WAIT_PROC : UMH_WAIT_EXEC);

	atomic_dec(&kmod_concurrent);
	return ret;
}
EXPORT_SYMBOL(__request_module);
#endif */ CONFIG_MODULES /*

static void call_usermodehelper_freeinfo(struct subprocess_infoinfo)
{
	if (info->cleanup)
		(*info->cleanup)(info);
	kfree(info);
}

static void umh_complete(struct subprocess_infosub_info)
{
	struct completioncomp = xchg(&sub_info->complete, NULL);
	*/
	 See call_usermodehelper_exec(). If xchg() returns NULL
	 we own sub_info, the UMH_KILLABLE caller has gone away
	 or the caller used UMH_NO_WAIT.
	 /*
	if (comp)
		complete(comp);
	else
		call_usermodehelper_freeinfo(sub_info);
}

*/
 This is the task which runs the usermode application
 /*
static int call_usermodehelper_exec_async(voiddata)
{
	struct subprocess_infosub_info = data;
	struct crednew;
	int retval;

	spin_lock_irq(&current->sighand->siglock);
	flush_signal_handlers(current, 1);
	spin_unlock_irq(&current->sighand->siglock);

	*/
	 Our parent (unbound workqueue) runs with elevated scheduling
	 priority. Avoid propagating that into the userspace child.
	 /*
	set_user_nice(current, 0);

	retval = -ENOMEM;
	new = prepare_kernel_cred(current);
	if (!new)
		goto out;

	spin_lock(&umh_sysctl_lock);
	new->cap_bset = cap_intersect(usermodehelper_bset, new->cap_bset);
	new->cap_inheritable = cap_intersect(usermodehelper_inheritable,
					     new->cap_inheritable);
	spin_unlock(&umh_sysctl_lock);

	if (sub_info->init) {
		retval = sub_info->init(sub_info, new);
		if (retval) {
			abort_creds(new);
			goto out;
		}
	}

	commit_creds(new);

	retval = do_execve(getname_kernel(sub_info->path),
			   (const char __userconst __user)sub_info->argv,
			   (const char __userconst __user)sub_info->envp);
out:
	sub_info->retval = retval;
	*/
	 call_usermodehelper_exec_sync() will call umh_complete
	 if UHM_WAIT_PROC.
	 /*
	if (!(sub_info->wait & UMH_WAIT_PROC))
		umh_complete(sub_info);
	if (!retval)
		return 0;
	do_exit(0);
}

*/ Handles UMH_WAIT_PROC.  /*
static void call_usermodehelper_exec_sync(struct subprocess_infosub_info)
{
	pid_t pid;

	*/ If SIGCLD is ignored sys_wait4 won't populate the status. /*
	kernel_sigaction(SIGCHLD, SIG_DFL);
	pid = kernel_thread(call_usermodehelper_exec_async, sub_info, SIGCHLD);
	if (pid < 0) {
		sub_info->retval = pid;
	} else {
		int ret = -ECHILD;
		*/
		 Normally it is bogus to call wait4() from in-kernel because
		 wait4() wants to write the exit code to a userspace address.
		 But call_usermodehelper_exec_sync() always runs as kernel
		 thread (workqueue) and put_user() to a kernel address works
		 OK for kernel threads, due to their having an mm_segment_t
		 which spans the entire address space.
		
		 Thus the __user pointer cast is valid here.
		 /*
		sys_wait4(pid, (int __user)&ret, 0, NULL);

		*/
		 If ret is 0, either call_usermodehelper_exec_async failed and
		 the real error code is already in sub_info->retval or
		 sub_info->retval is 0 anyway, so don't mess with it then.
		 /*
		if (ret)
			sub_info->retval = ret;
	}

	*/ Restore default kernel sig handler /*
	kernel_sigaction(SIGCHLD, SIG_IGN);

	umh_complete(sub_info);
}

*/
 We need to create the usermodehelper kernel thread from a task that is affine
 to an optimized set of CPUs (or nohz housekeeping ones) such that they
 inherit a widest affinity irrespective of call_usermodehelper() callers with
 possibly reduced affinity (eg: per-cpu workqueues). We don't want
 usermodehelper targets to contend a busy CPU.

 Unbound workqueues provide such wide affinity and allow to block on
 UMH_WAIT_PROC requests without blocking pending request (up to some limit).

 Besides, workqueues provide the privilege level that caller might not have
 to perform the usermodehelper request.

 /*
static void call_usermodehelper_exec_work(struct work_structwork)
{
	struct subprocess_infosub_info =
		container_of(work, struct subprocess_info, work);

	if (sub_info->wait & UMH_WAIT_PROC) {
		call_usermodehelper_exec_sync(sub_info);
	} else {
		pid_t pid;
		*/
		 Use CLONE_PARENT to reparent it to kthreadd; we do not
		 want to pollute current->children, and we need a parent
		 that always ignores SIGCHLD to ensure auto-reaping.
		 /*
		pid = kernel_thread(call_usermodehelper_exec_async, sub_info,
				    CLONE_PARENT | SIGCHLD);
		if (pid < 0) {
			sub_info->retval = pid;
			umh_complete(sub_info);
		}
	}
}

*/
 If set, call_usermodehelper_exec() will exit immediately returning -EBUSY
 (used for preventing user land processes from being created after the user
 land has been frozen during a system-wide hibernation or suspend operation).
 Should always be manipulated under umhelper_sem acquired for write.
 /*
static enum umh_disable_depth usermodehelper_disabled = UMH_DISABLED;

*/ Number of helpers running /*
static atomic_t running_helpers = ATOMIC_INIT(0);

*/
 Wait queue head used by usermodehelper_disable() to wait for all running
 helpers to finish.
 /*
static DECLARE_WAIT_QUEUE_HEAD(running_helpers_waitq);

*/
 Used by usermodehelper_read_lock_wait() to wait for usermodehelper_disabled
 to become 'false'.
 /*
static DECLARE_WAIT_QUEUE_HEAD(usermodehelper_disabled_waitq);

*/
 Time to wait for running_helpers to become zero before the setting of
 usermodehelper_disabled in usermodehelper_disable() fails
 /*
#define RUNNING_HELPERS_TIMEOUT	(5 HZ)

int usermodehelper_read_trylock(void)
{
	DEFINE_WAIT(wait);
	int ret = 0;

	down_read(&umhelper_sem);
	for (;;) {
		prepare_to_wait(&usermodehelper_disabled_waitq, &wait,
				TASK_INTERRUPTIBLE);
		if (!usermodehelper_disabled)
			break;

		if (usermodehelper_disabled == UMH_DISABLED)
			ret = -EAGAIN;

		up_read(&umhelper_sem);

		if (ret)
			break;

		schedule();
		try_to_freeze();

		down_read(&umhelper_sem);
	}
	finish_wait(&usermodehelper_disabled_waitq, &wait);
	return ret;
}
EXPORT_SYMBOL_GPL(usermodehelper_read_trylock);

long usermodehelper_read_lock_wait(long timeout)
{
	DEFINE_WAIT(wait);

	if (timeout < 0)
		return -EINVAL;

	down_read(&umhelper_sem);
	for (;;) {
		prepare_to_wait(&usermodehelper_disabled_waitq, &wait,
				TASK_UNINTERRUPTIBLE);
		if (!usermodehelper_disabled)
			break;

		up_read(&umhelper_sem);

		timeout = schedule_timeout(timeout);
		if (!timeout)
			break;

		down_read(&umhelper_sem);
	}
	finish_wait(&usermodehelper_disabled_waitq, &wait);
	return timeout;
}
EXPORT_SYMBOL_GPL(usermodehelper_read_lock_wait);

void usermodehelper_read_unlock(void)
{
	up_read(&umhelper_sem);
}
EXPORT_SYMBOL_GPL(usermodehelper_read_unlock);

*/
 __usermodehelper_set_disable_depth - Modify usermodehelper_disabled.
 @depth: New value to assign to usermodehelper_disabled.

 Change the value of usermodehelper_disabled (under umhelper_sem locked for
 writing) and wakeup tasks waiting for it to change.
 /*
void __usermodehelper_set_disable_depth(enum umh_disable_depth depth)
{
	down_write(&umhelper_sem);
	usermodehelper_disabled = depth;
	wake_up(&usermodehelper_disabled_waitq);
	up_write(&umhelper_sem);
}

*/
 __usermodehelper_disable - Prevent new helpers from being started.
 @depth: New value to assign to usermodehelper_disabled.

 Set usermodehelper_disabled to @depth and wait for running helpers to exit.
 /*
int __usermodehelper_disable(enum umh_disable_depth depth)
{
	long retval;

	if (!depth)
		return -EINVAL;

	down_write(&umhelper_sem);
	usermodehelper_disabled = depth;
	up_write(&umhelper_sem);

	*/
	 From now on call_usermodehelper_exec() won't start any new
	 helpers, so it is sufficient if running_helpers turns out to
	 be zero at one point (it may be increased later, but that
	 doesn't matter).
	 /*
	retval = wait_event_timeout(running_helpers_waitq,
					atomic_read(&running_helpers) == 0,
					RUNNING_HELPERS_TIMEOUT);
	if (retval)
		return 0;

	__usermodehelper_set_disable_depth(UMH_ENABLED);
	return -EAGAIN;
}

static void helper_lock(void)
{
	atomic_inc(&running_helpers);
	smp_mb__after_atomic();
}

static void helper_unlock(void)
{
	if (atomic_dec_and_test(&running_helpers))
		wake_up(&running_helpers_waitq);
}

*/
 call_usermodehelper_setup - prepare to call a usermode helper
 @path: path to usermode executable
 @argv: arg vector for process
 @envp: environment for process
 @gfp_mask: gfp mask for memory allocation
 @cleanup: a cleanup function
 @init: an init function
 @data: arbitrary context sensitive data

 Returns either %NULL on allocation failure, or a subprocess_info
 structure.  This should be passed to call_usermodehelper_exec to
 exec the process and free the structure.

 The init function is used to customize the helper process prior to
 exec.  A non-zero return code causes the process to error out, exit,
 and return the failure to the calling process

 The cleanup function is just before ethe subprocess_info is about to
 be freed.  This can be used for freeing the argv and envp.  The
 Function must be runnable in either a process context or the
 context in which call_usermodehelper_exec is called.
 /*
struct subprocess_infocall_usermodehelper_setup(charpath, char*argv,
		char*envp, gfp_t gfp_mask,
		int (*init)(struct subprocess_infoinfo, struct crednew),
		void (*cleanup)(struct subprocess_infoinfo),
		voiddata)
{
	struct subprocess_infosub_info;
	sub_info = kzalloc(sizeof(struct subprocess_info), gfp_mask);
	if (!sub_info)
		goto out;

	INIT_WORK(&sub_info->work, call_usermodehelper_exec_work);
	sub_info->path = path;
	sub_info->argv = argv;
	sub_info->envp = envp;

	sub_info->cleanup = cleanup;
	sub_info->init = init;
	sub_info->data = data;
  out:
	return sub_info;
}
EXPORT_SYMBOL(call_usermodehelper_setup);

*/
 call_usermodehelper_exec - start a usermode application
 @sub_info: information about the subprocessa
 @wait: wait for the application to finish and return status.
        when UMH_NO_WAIT don't wait at all, but you get no useful error back
        when the program couldn't be exec'ed. This makes it safe to call
        from interrupt context.

 Runs a user-space application.  The application is started
 asynchronously if wait is not set, and runs as a child of system workqueues.
 (ie. it runs with full root capabilities and optimized affinity).
 /*
int call_usermodehelper_exec(struct subprocess_infosub_info, int wait)
{
	DECLARE_COMPLETION_ONSTACK(done);
	int retval = 0;

	if (!sub_info->path) {
		call_usermodehelper_freeinfo(sub_info);
		return -EINVAL;
	}
	helper_lock();
	if (usermodehelper_disabled) {
		retval = -EBUSY;
		goto out;
	}
	*/
	 Set the completion pointer only if there is a waiter.
	 This makes it possible to use umh_complete to free
	 the data structure in case of UMH_NO_WAIT.
	 /*
	sub_info->complete = (wait == UMH_NO_WAIT) ? NULL : &done;
	sub_info->wait = wait;

	queue_work(system_unbound_wq, &sub_info->work);
	if (wait == UMH_NO_WAIT)	*/ task has freed sub_info /*
		goto unlock;

	if (wait & UMH_KILLABLE) {
		retval = wait_for_completion_killable(&done);
		if (!retval)
			goto wait_done;

		*/ umh_complete() will see NULL and free sub_info /*
		if (xchg(&sub_info->complete, NULL))
			goto unlock;
		*/ fallthrough, umh_complete() was already called /*
	}

	wait_for_completion(&done);
wait_done:
	retval = sub_info->retval;
out:
	call_usermodehelper_freeinfo(sub_info);
unlock:
	helper_unlock();
	return retval;
}
EXPORT_SYMBOL(call_usermodehelper_exec);

*/
 call_usermodehelper() - prepare and start a usermode application
 @path: path to usermode executable
 @argv: arg vector for process
 @envp: environment for process
 @wait: wait for the application to finish and return status.
        when UMH_NO_WAIT don't wait at all, but you get no useful error back
        when the program couldn't be exec'ed. This makes it safe to call
        from interrupt context.

 This function is the equivalent to use call_usermodehelper_setup() and
 call_usermodehelper_exec().
 /*
int call_usermodehelper(charpath, char*argv, char*envp, int wait)
{
	struct subprocess_infoinfo;
	gfp_t gfp_mask = (wait == UMH_NO_WAIT) ? GFP_ATOMIC : GFP_KERNEL;

	info = call_usermodehelper_setup(path, argv, envp, gfp_mask,
					 NULL, NULL, NULL);
	if (info == NULL)
		return -ENOMEM;

	return call_usermodehelper_exec(info, wait);
}
EXPORT_SYMBOL(call_usermodehelper);

static int proc_cap_handler(struct ctl_tabletable, int write,
			 void __userbuffer, size_tlenp, loff_tppos)
{
	struct ctl_table t;
	unsigned long cap_array[_KERNEL_CAPABILITY_U32S];
	kernel_cap_t new_cap;
	int err, i;

	if (write && (!capable(CAP_SETPCAP) ||
		      !capable(CAP_SYS_MODULE)))
		return -EPERM;

	*/
	 convert from the global kernel_cap_t to the ulong array to print to
	 userspace if this is a read.
	 /*
	spin_lock(&umh_sysctl_lock);
	for (i = 0; i < _KERNEL_CAPABILITY_U32S; i++)  {
		if (table->data == CAP_BSET)
			cap_array[i] = usermodehelper_bset.cap[i];
		else if (table->data == CAP_PI)
			cap_array[i] = usermodehelper_inheritable.cap[i];
		else
			BUG();
	}
	spin_unlock(&umh_sysctl_lock);

	t =table;
	t.data = &cap_array;

	*/
	 actually read or write and array of ulongs from userspace.  Remember
	 these are least significant 32 bits first
	 /*
	err = proc_doulongvec_minmax(&t, write, buffer, lenp, ppos);
	if (err < 0)
		return err;

	*/
	 convert from the sysctl array of ulongs to the kernel_cap_t
	 internal representation
	 /*
	for (i = 0; i < _KERNEL_CAPABILITY_U32S; i++)
		new_cap.cap[i] = cap_array[i];

	*/
	 Drop everything not in the new_cap (but don't add things)
	 /*
	spin_lock(&umh_sysctl_lock);
	if (write) {
		if (table->data == CAP_BSET)
			usermodehelper_bset = cap_intersect(usermodehelper_bset, new_cap);
		if (table->data == CAP_PI)
			usermodehelper_inheritable = cap_intersect(usermodehelper_inheritable, new_cap);
	}
	spin_unlock(&umh_sysctl_lock);

	return 0;
}

struct ctl_table usermodehelper_table[] = {
	{
		.procname	= "bset",
		.data		= CAP_BSET,
		.maxlen		= _KERNEL_CAPABILITY_U32S sizeof(unsigned long),
		.mode		= 0600,
		.proc_handler	= proc_cap_handler,
	},
	{
		.procname	= "inheritable",
		.data		= CAP_PI,
		.maxlen		= _KERNEL_CAPABILITY_U32S sizeof(unsigned long),
		.mode		= 0600,
		.proc_handler	= proc_cap_handler,
	},
	{ }
};
*/

  Kernel Probes (KProbes)
  kernel/kprobes.c

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 Copyright (C) IBM Corporation, 2002, 2004

 2002-Oct	Created by Vamsi Krishna S <vamsi_krishna@in.ibm.com> Kernel
		Probes initial implementation (includes suggestions from
		Rusty Russell).
 2004-Aug	Updated by Prasanna S Panchamukhi <prasanna@in.ibm.com> with
		hlists and exceptions notifier as suggested by Andi Kleen.
 2004-July	Suparna Bhattacharya <suparna@in.ibm.com> added jumper probes
		interface to access function arguments.
 2004-Sep	Prasanna S Panchamukhi <prasanna@in.ibm.com> Changed Kprobes
		exceptions notifier to be first on the priority list.
 2005-May	Hien Nguyen <hien@us.ibm.com>, Jim Keniston
		<jkenisto@us.ibm.com> and Prasanna S Panchamukhi
		<prasanna@in.ibm.com> added function-return probes.
 /*
#include <linux/kprobes.h>
#include <linux/hash.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/stddef.h>
#include <linux/export.h>
#include <linux/moduleloader.h>
#include <linux/kallsyms.h>
#include <linux/freezer.h>
#include <linux/seq_file.h>
#include <linux/debugfs.h>
#include <linux/sysctl.h>
#include <linux/kdebug.h>
#include <linux/memory.h>
#include <linux/ftrace.h>
#include <linux/cpu.h>
#include <linux/jump_label.h>

#include <asm-generic/sections.h>
#include <asm/cacheflush.h>
#include <asm/errno.h>
#include <asm/uaccess.h>

#define KPROBE_HASH_BITS 6
#define KPROBE_TABLE_SIZE (1 << KPROBE_HASH_BITS)


*/
 Some oddball architectures like 64bit powerpc have function descriptors
 so this must be overridable.
 /*
#ifndef kprobe_lookup_name
#define kprobe_lookup_name(name, addr) \
	addr = ((kprobe_opcode_t)(kallsyms_lookup_name(name)))
#endif

static int kprobes_initialized;
static struct hlist_head kprobe_table[KPROBE_TABLE_SIZE];
static struct hlist_head kretprobe_inst_table[KPROBE_TABLE_SIZE];

*/ NOTE: change this value only with kprobe_mutex held /*
static bool kprobes_all_disarmed;

*/ This protects kprobe_table and optimizing_list /*
static DEFINE_MUTEX(kprobe_mutex);
static DEFINE_PER_CPU(struct kprobe, kprobe_instance) = NULL;
static struct {
	raw_spinlock_t lock ____cacheline_aligned_in_smp;
} kretprobe_table_locks[KPROBE_TABLE_SIZE];

static raw_spinlock_tkretprobe_table_lock_ptr(unsigned long hash)
{
	return &(kretprobe_table_locks[hash].lock);
}

*/ Blacklist -- list of struct kprobe_blacklist_entry /*
static LIST_HEAD(kprobe_blacklist);

#ifdef __ARCH_WANT_KPROBES_INSN_SLOT
*/
 kprobe->ainsn.insn points to the copy of the instruction to be
 single-stepped. x86_64, POWER4 and above have no-exec support and
 stepping on the instruction on a vmalloced/kmalloced/data page
 is a recipe for disaster
 /*
struct kprobe_insn_page {
	struct list_head list;
	kprobe_opcode_tinsns;		*/ Page of instruction slots /*
	struct kprobe_insn_cachecache;
	int nused;
	int ngarbage;
	char slot_used[];
};

#define KPROBE_INSN_PAGE_SIZE(slots)			\
	(offsetof(struct kprobe_insn_page, slot_used) +	\
	 (sizeof(char) (slots)))

static int slots_per_page(struct kprobe_insn_cachec)
{
	return PAGE_SIZE/(c->insn_size sizeof(kprobe_opcode_t));
}

enum kprobe_slot_state {
	SLOT_CLEAN = 0,
	SLOT_DIRTY = 1,
	SLOT_USED = 2,
};

static voidalloc_insn_page(void)
{
	return module_alloc(PAGE_SIZE);
}

static void free_insn_page(voidpage)
{
	module_memfree(page);
}

struct kprobe_insn_cache kprobe_insn_slots = {
	.mutex = __MUTEX_INITIALIZER(kprobe_insn_slots.mutex),
	.alloc = alloc_insn_page,
	.free = free_insn_page,
	.pages = LIST_HEAD_INIT(kprobe_insn_slots.pages),
	.insn_size = MAX_INSN_SIZE,
	.nr_garbage = 0,
};
static int collect_garbage_slots(struct kprobe_insn_cachec);

*/
 __get_insn_slot() - Find a slot on an executable page for an instruction.
 We allocate an executable page if there's no room on existing ones.
 /*
kprobe_opcode_t__get_insn_slot(struct kprobe_insn_cachec)
{
	struct kprobe_insn_pagekip;
	kprobe_opcode_tslot = NULL;

	mutex_lock(&c->mutex);
 retry:
	list_for_each_entry(kip, &c->pages, list) {
		if (kip->nused < slots_per_page(c)) {
			int i;
			for (i = 0; i < slots_per_page(c); i++) {
				if (kip->slot_used[i] == SLOT_CLEAN) {
					kip->slot_used[i] = SLOT_USED;
					kip->nused++;
					slot = kip->insns + (i c->insn_size);
					goto out;
				}
			}
			*/ kip->nused is broken. Fix it. /*
			kip->nused = slots_per_page(c);
			WARN_ON(1);
		}
	}

	*/ If there are any garbage slots, collect it and try again. /*
	if (c->nr_garbage && collect_garbage_slots(c) == 0)
		goto retry;

	*/ All out of space.  Need to allocate a new page. /*
	kip = kmalloc(KPROBE_INSN_PAGE_SIZE(slots_per_page(c)), GFP_KERNEL);
	if (!kip)
		goto out;

	*/
	 Use module_alloc so this page is within +/- 2GB of where the
	 kernel image and loaded module images reside. This is required
	 so x86_64 can correctly handle the %rip-relative fixups.
	 /*
	kip->insns = c->alloc();
	if (!kip->insns) {
		kfree(kip);
		goto out;
	}
	INIT_LIST_HEAD(&kip->list);
	memset(kip->slot_used, SLOT_CLEAN, slots_per_page(c));
	kip->slot_used[0] = SLOT_USED;
	kip->nused = 1;
	kip->ngarbage = 0;
	kip->cache = c;
	list_add(&kip->list, &c->pages);
	slot = kip->insns;
out:
	mutex_unlock(&c->mutex);
	return slot;
}

*/ Return 1 if all garbages are collected, otherwise 0. /*
static int collect_one_slot(struct kprobe_insn_pagekip, int idx)
{
	kip->slot_used[idx] = SLOT_CLEAN;
	kip->nused--;
	if (kip->nused == 0) {
		*/
		 Page is no longer in use.  Free it unless
		 it's the last one.  We keep the last one
		 so as not to have to set it up again the
		 next time somebody inserts a probe.
		 /*
		if (!list_is_singular(&kip->list)) {
			list_del(&kip->list);
			kip->cache->free(kip->insns);
			kfree(kip);
		}
		return 1;
	}
	return 0;
}

static int collect_garbage_slots(struct kprobe_insn_cachec)
{
	struct kprobe_insn_pagekip,next;

	*/ Ensure no-one is interrupted on the garbages /*
	synchronize_sched();

	list_for_each_entry_safe(kip, next, &c->pages, list) {
		int i;
		if (kip->ngarbage == 0)
			continue;
		kip->ngarbage = 0;	*/ we will collect all garbages /*
		for (i = 0; i < slots_per_page(c); i++) {
			if (kip->slot_used[i] == SLOT_DIRTY &&
			    collect_one_slot(kip, i))
				break;
		}
	}
	c->nr_garbage = 0;
	return 0;
}

void __free_insn_slot(struct kprobe_insn_cachec,
		      kprobe_opcode_tslot, int dirty)
{
	struct kprobe_insn_pagekip;

	mutex_lock(&c->mutex);
	list_for_each_entry(kip, &c->pages, list) {
		long idx = ((long)slot - (long)kip->insns) /
				(c->insn_size sizeof(kprobe_opcode_t));
		if (idx >= 0 && idx < slots_per_page(c)) {
			WARN_ON(kip->slot_used[idx] != SLOT_USED);
			if (dirty) {
				kip->slot_used[idx] = SLOT_DIRTY;
				kip->ngarbage++;
				if (++c->nr_garbage > slots_per_page(c))
					collect_garbage_slots(c);
			} else
				collect_one_slot(kip, idx);
			goto out;
		}
	}
	*/ Could not free this slot. /*
	WARN_ON(1);
out:
	mutex_unlock(&c->mutex);
}

#ifdef CONFIG_OPTPROBES
*/ For optimized_kprobe buffer /*
struct kprobe_insn_cache kprobe_optinsn_slots = {
	.mutex = __MUTEX_INITIALIZER(kprobe_optinsn_slots.mutex),
	.alloc = alloc_insn_page,
	.free = free_insn_page,
	.pages = LIST_HEAD_INIT(kprobe_optinsn_slots.pages),
	*/ .insn_size is initialized later /*
	.nr_garbage = 0,
};
#endif
#endif

*/ We have preemption disabled.. so it is safe to use __ versions /*
static inline void set_kprobe_instance(struct kprobekp)
{
	__this_cpu_write(kprobe_instance, kp);
}

static inline void reset_kprobe_instance(void)
{
	__this_cpu_write(kprobe_instance, NULL);
}

*/
 This routine is called either:
 	- under the kprobe_mutex - during kprobe_[un]register()
 				OR
 	- with preemption disabled - from arch/xxx/kernel/kprobes.c
 /*
struct kprobeget_kprobe(voidaddr)
{
	struct hlist_headhead;
	struct kprobep;

	head = &kprobe_table[hash_ptr(addr, KPROBE_HASH_BITS)];
	hlist_for_each_entry_rcu(p, head, hlist) {
		if (p->addr == addr)
			return p;
	}

	return NULL;
}
NOKPROBE_SYMBOL(get_kprobe);

static int aggr_pre_handler(struct kprobep, struct pt_regsregs);

*/ Return true if the kprobe is an aggregator /*
static inline int kprobe_aggrprobe(struct kprobep)
{
	return p->pre_handler == aggr_pre_handler;
}

*/ Return true(!0) if the kprobe is unused /*
static inline int kprobe_unused(struct kprobep)
{
	return kprobe_aggrprobe(p) && kprobe_disabled(p) &&
	       list_empty(&p->list);
}

*/
 Keep all fields in the kprobe consistent
 /*
static inline void copy_kprobe(struct kprobeap, struct kprobep)
{
	memcpy(&p->opcode, &ap->opcode, sizeof(kprobe_opcode_t));
	memcpy(&p->ainsn, &ap->ainsn, sizeof(struct arch_specific_insn));
}

#ifdef CONFIG_OPTPROBES
*/ NOTE: change this value only with kprobe_mutex held /*
static bool kprobes_allow_optimization;

*/
 Call all pre_handler on the list, but ignores its return value.
 This must be called from arch-dep optimized caller.
 /*
void opt_pre_handler(struct kprobep, struct pt_regsregs)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &p->list, list) {
		if (kp->pre_handler && likely(!kprobe_disabled(kp))) {
			set_kprobe_instance(kp);
			kp->pre_handler(kp, regs);
		}
		reset_kprobe_instance();
	}
}
NOKPROBE_SYMBOL(opt_pre_handler);

*/ Free optimized instructions and optimized_kprobe /*
static void free_aggr_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = container_of(p, struct optimized_kprobe, kp);
	arch_remove_optimized_kprobe(op);
	arch_remove_kprobe(p);
	kfree(op);
}

*/ Return true(!0) if the kprobe is ready for optimization. /*
static inline int kprobe_optready(struct kprobep)
{
	struct optimized_kprobeop;

	if (kprobe_aggrprobe(p)) {
		op = container_of(p, struct optimized_kprobe, kp);
		return arch_prepared_optinsn(&op->optinsn);
	}

	return 0;
}

*/ Return true(!0) if the kprobe is disarmed. Note: p must be on hash list /*
static inline int kprobe_disarmed(struct kprobep)
{
	struct optimized_kprobeop;

	*/ If kprobe is not aggr/opt probe, just return kprobe is disabled /*
	if (!kprobe_aggrprobe(p))
		return kprobe_disabled(p);

	op = container_of(p, struct optimized_kprobe, kp);

	return kprobe_disabled(p) && list_empty(&op->list);
}

*/ Return true(!0) if the probe is queued on (un)optimizing lists /*
static int kprobe_queued(struct kprobep)
{
	struct optimized_kprobeop;

	if (kprobe_aggrprobe(p)) {
		op = container_of(p, struct optimized_kprobe, kp);
		if (!list_empty(&op->list))
			return 1;
	}
	return 0;
}

*/
 Return an optimized kprobe whose optimizing code replaces
 instructions including addr (exclude breakpoint).
 /*
static struct kprobeget_optimized_kprobe(unsigned long addr)
{
	int i;
	struct kprobep = NULL;
	struct optimized_kprobeop;

	*/ Don't check i == 0, since that is a breakpoint case. /*
	for (i = 1; !p && i < MAX_OPTIMIZED_LENGTH; i++)
		p = get_kprobe((void)(addr - i));

	if (p && kprobe_optready(p)) {
		op = container_of(p, struct optimized_kprobe, kp);
		if (arch_within_optimized_kprobe(op, addr))
			return p;
	}

	return NULL;
}

*/ Optimization staging list, protected by kprobe_mutex /*
static LIST_HEAD(optimizing_list);
static LIST_HEAD(unoptimizing_list);
static LIST_HEAD(freeing_list);

static void kprobe_optimizer(struct work_structwork);
static DECLARE_DELAYED_WORK(optimizing_work, kprobe_optimizer);
#define OPTIMIZE_DELAY 5

*/
 Optimize (replace a breakpoint with a jump) kprobes listed on
 optimizing_list.
 /*
static void do_optimize_kprobes(void)
{
	*/ Optimization never be done when disarmed /*
	if (kprobes_all_disarmed || !kprobes_allow_optimization ||
	    list_empty(&optimizing_list))
		return;

	*/
	 The optimization/unoptimization refers online_cpus via
	 stop_machine() and cpu-hotplug modifies online_cpus.
	 And same time, text_mutex will be held in cpu-hotplug and here.
	 This combination can cause a deadlock (cpu-hotplug try to lock
	 text_mutex but stop_machine can not be done because online_cpus
	 has been changed)
	 To avoid this deadlock, we need to call get_online_cpus()
	 for preventing cpu-hotplug outside of text_mutex locking.
	 /*
	get_online_cpus();
	mutex_lock(&text_mutex);
	arch_optimize_kprobes(&optimizing_list);
	mutex_unlock(&text_mutex);
	put_online_cpus();
}

*/
 Unoptimize (replace a jump with a breakpoint and remove the breakpoint
 if need) kprobes listed on unoptimizing_list.
 /*
static void do_unoptimize_kprobes(void)
{
	struct optimized_kprobeop,tmp;

	*/ Unoptimization must be done anytime /*
	if (list_empty(&unoptimizing_list))
		return;

	*/ Ditto to do_optimize_kprobes /*
	get_online_cpus();
	mutex_lock(&text_mutex);
	arch_unoptimize_kprobes(&unoptimizing_list, &freeing_list);
	*/ Loop free_list for disarming /*
	list_for_each_entry_safe(op, tmp, &freeing_list, list) {
		*/ Disarm probes if marked disabled /*
		if (kprobe_disabled(&op->kp))
			arch_disarm_kprobe(&op->kp);
		if (kprobe_unused(&op->kp)) {
			*/
			 Remove unused probes from hash list. After waiting
			 for synchronization, these probes are reclaimed.
			 (reclaiming is done by do_free_cleaned_kprobes.)
			 /*
			hlist_del_rcu(&op->kp.hlist);
		} else
			list_del_init(&op->list);
	}
	mutex_unlock(&text_mutex);
	put_online_cpus();
}

*/ Reclaim all kprobes on the free_list /*
static void do_free_cleaned_kprobes(void)
{
	struct optimized_kprobeop,tmp;

	list_for_each_entry_safe(op, tmp, &freeing_list, list) {
		BUG_ON(!kprobe_unused(&op->kp));
		list_del_init(&op->list);
		free_aggr_kprobe(&op->kp);
	}
}

*/ Start optimizer after OPTIMIZE_DELAY passed /*
static void kick_kprobe_optimizer(void)
{
	schedule_delayed_work(&optimizing_work, OPTIMIZE_DELAY);
}

*/ Kprobe jump optimizer /*
static void kprobe_optimizer(struct work_structwork)
{
	mutex_lock(&kprobe_mutex);
	*/ Lock modules while optimizing kprobes /*
	mutex_lock(&module_mutex);

	*/
	 Step 1: Unoptimize kprobes and collect cleaned (unused and disarmed)
	 kprobes before waiting for quiesence period.
	 /*
	do_unoptimize_kprobes();

	*/
	 Step 2: Wait for quiesence period to ensure all running interrupts
	 are done. Because optprobe may modify multiple instructions
	 there is a chance that Nth instruction is interrupted. In that
	 case, running interrupt can return to 2nd-Nth byte of jump
	 instruction. This wait is for avoiding it.
	 /*
	synchronize_sched();

	*/ Step 3: Optimize kprobes after quiesence period /*
	do_optimize_kprobes();

	*/ Step 4: Free cleaned kprobes after quiesence period /*
	do_free_cleaned_kprobes();

	mutex_unlock(&module_mutex);
	mutex_unlock(&kprobe_mutex);

	*/ Step 5: Kick optimizer again if needed /*
	if (!list_empty(&optimizing_list) || !list_empty(&unoptimizing_list))
		kick_kprobe_optimizer();
}

*/ Wait for completing optimization and unoptimization /*
static void wait_for_kprobe_optimizer(void)
{
	mutex_lock(&kprobe_mutex);

	while (!list_empty(&optimizing_list) || !list_empty(&unoptimizing_list)) {
		mutex_unlock(&kprobe_mutex);

		*/ this will also make optimizing_work execute immmediately /*
		flush_delayed_work(&optimizing_work);
		*/ @optimizing_work might not have been queued yet, relax /*
		cpu_relax();

		mutex_lock(&kprobe_mutex);
	}

	mutex_unlock(&kprobe_mutex);
}

*/ Optimize kprobe if p is ready to be optimized /*
static void optimize_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	*/ Check if the kprobe is disabled or not ready for optimization. /*
	if (!kprobe_optready(p) || !kprobes_allow_optimization ||
	    (kprobe_disabled(p) || kprobes_all_disarmed))
		return;

	*/ Both of break_handler and post_handler are not supported. /*
	if (p->break_handler || p->post_handler)
		return;

	op = container_of(p, struct optimized_kprobe, kp);

	*/ Check there is no other kprobes at the optimized instructions /*
	if (arch_check_optimized_kprobe(op) < 0)
		return;

	*/ Check if it is already optimized. /*
	if (op->kp.flags & KPROBE_FLAG_OPTIMIZED)
		return;
	op->kp.flags |= KPROBE_FLAG_OPTIMIZED;

	if (!list_empty(&op->list))
		*/ This is under unoptimizing. Just dequeue the probe /*
		list_del_init(&op->list);
	else {
		list_add(&op->list, &optimizing_list);
		kick_kprobe_optimizer();
	}
}

*/ Short cut to direct unoptimizing /*
static void force_unoptimize_kprobe(struct optimized_kprobeop)
{
	get_online_cpus();
	arch_unoptimize_kprobe(op);
	put_online_cpus();
	if (kprobe_disabled(&op->kp))
		arch_disarm_kprobe(&op->kp);
}

*/ Unoptimize a kprobe if p is optimized /*
static void unoptimize_kprobe(struct kprobep, bool force)
{
	struct optimized_kprobeop;

	if (!kprobe_aggrprobe(p) || kprobe_disarmed(p))
		return;/ This is not an optprobe nor optimized /*

	op = container_of(p, struct optimized_kprobe, kp);
	if (!kprobe_optimized(p)) {
		*/ Unoptimized or unoptimizing case /*
		if (force && !list_empty(&op->list)) {
			*/
			 Only if this is unoptimizing kprobe and forced,
			 forcibly unoptimize it. (No need to unoptimize
			 unoptimized kprobe again :)
			 /*
			list_del_init(&op->list);
			force_unoptimize_kprobe(op);
		}
		return;
	}

	op->kp.flags &= ~KPROBE_FLAG_OPTIMIZED;
	if (!list_empty(&op->list)) {
		*/ Dequeue from the optimization queue /*
		list_del_init(&op->list);
		return;
	}
	*/ Optimized kprobe case /*
	if (force)
		*/ Forcibly update the code: this is a special case /*
		force_unoptimize_kprobe(op);
	else {
		list_add(&op->list, &unoptimizing_list);
		kick_kprobe_optimizer();
	}
}

*/ Cancel unoptimizing for reusing /*
static void reuse_unused_kprobe(struct kprobeap)
{
	struct optimized_kprobeop;

	BUG_ON(!kprobe_unused(ap));
	*/
	 Unused kprobe MUST be on the way of delayed unoptimizing (means
	 there is still a relative jump) and disabled.
	 /*
	op = container_of(ap, struct optimized_kprobe, kp);
	if (unlikely(list_empty(&op->list)))
		printk(KERN_WARNING "Warning: found a stray unused "
			"aggrprobe@%p\n", ap->addr);
	*/ Enable the probe again /*
	ap->flags &= ~KPROBE_FLAG_DISABLED;
	*/ Optimize it again (remove from op->list) /*
	BUG_ON(!kprobe_optready(ap));
	optimize_kprobe(ap);
}

*/ Remove optimized instructions /*
static void kill_optimized_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = container_of(p, struct optimized_kprobe, kp);
	if (!list_empty(&op->list))
		*/ Dequeue from the (un)optimization queue /*
		list_del_init(&op->list);
	op->kp.flags &= ~KPROBE_FLAG_OPTIMIZED;

	if (kprobe_unused(p)) {
		*/ Enqueue if it is unused /*
		list_add(&op->list, &freeing_list);
		*/
		 Remove unused probes from the hash list. After waiting
		 for synchronization, this probe is reclaimed.
		 (reclaiming is done by do_free_cleaned_kprobes().)
		 /*
		hlist_del_rcu(&op->kp.hlist);
	}

	*/ Don't touch the code, because it is already freed. /*
	arch_remove_optimized_kprobe(op);
}

*/ Try to prepare optimized instructions /*
static void prepare_optimized_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = container_of(p, struct optimized_kprobe, kp);
	arch_prepare_optimized_kprobe(op, p);
}

*/ Allocate new optimized_kprobe and try to prepare optimized instructions /*
static struct kprobealloc_aggr_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = kzalloc(sizeof(struct optimized_kprobe), GFP_KERNEL);
	if (!op)
		return NULL;

	INIT_LIST_HEAD(&op->list);
	op->kp.addr = p->addr;
	arch_prepare_optimized_kprobe(op, p);

	return &op->kp;
}

static void init_aggr_kprobe(struct kprobeap, struct kprobep);

*/
 Prepare an optimized_kprobe and optimize it
 NOTE: p must be a normal registered kprobe
 /*
static void try_to_optimize_kprobe(struct kprobep)
{
	struct kprobeap;
	struct optimized_kprobeop;

	*/ Impossible to optimize ftrace-based kprobe /*
	if (kprobe_ftrace(p))
		return;

	*/ For preparing optimization, jump_label_text_reserved() is called /*
	jump_label_lock();
	mutex_lock(&text_mutex);

	ap = alloc_aggr_kprobe(p);
	if (!ap)
		goto out;

	op = container_of(ap, struct optimized_kprobe, kp);
	if (!arch_prepared_optinsn(&op->optinsn)) {
		*/ If failed to setup optimizing, fallback to kprobe /*
		arch_remove_optimized_kprobe(op);
		kfree(op);
		goto out;
	}

	init_aggr_kprobe(ap, p);
	optimize_kprobe(ap);	*/ This just kicks optimizer thread /*

out:
	mutex_unlock(&text_mutex);
	jump_label_unlock();
}

#ifdef CONFIG_SYSCTL
static void optimize_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&kprobe_mutex);
	*/ If optimization is already allowed, just return /*
	if (kprobes_allow_optimization)
		goto out;

	kprobes_allow_optimization = true;
	for (i = 0; i < KPROBE_TABLE_SIZE; i++) {
		head = &kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist)
			if (!kprobe_disabled(p))
				optimize_kprobe(p);
	}
	printk(KERN_INFO "Kprobes globally optimized\n");
out:
	mutex_unlock(&kprobe_mutex);
}

static void unoptimize_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&kprobe_mutex);
	*/ If optimization is already prohibited, just return /*
	if (!kprobes_allow_optimization) {
		mutex_unlock(&kprobe_mutex);
		return;
	}

	kprobes_allow_optimization = false;
	for (i = 0; i < KPROBE_TABLE_SIZE; i++) {
		head = &kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist) {
			if (!kprobe_disabled(p))
				unoptimize_kprobe(p, false);
		}
	}
	mutex_unlock(&kprobe_mutex);

	*/ Wait for unoptimizing completion /*
	wait_for_kprobe_optimizer();
	printk(KERN_INFO "Kprobes globally unoptimized\n");
}

static DEFINE_MUTEX(kprobe_sysctl_mutex);
int sysctl_kprobes_optimization;
int proc_kprobes_optimization_handler(struct ctl_tabletable, int write,
				      void __userbuffer, size_tlength,
				      loff_tppos)
{
	int ret;

	mutex_lock(&kprobe_sysctl_mutex);
	sysctl_kprobes_optimization = kprobes_allow_optimization ? 1 : 0;
	ret = proc_dointvec_minmax(table, write, buffer, length, ppos);

	if (sysctl_kprobes_optimization)
		optimize_all_kprobes();
	else
		unoptimize_all_kprobes();
	mutex_unlock(&kprobe_sysctl_mutex);

	return ret;
}
#endif */ CONFIG_SYSCTL /*

*/ Put a breakpoint for a probe. Must be called with text_mutex locked /*
static void __arm_kprobe(struct kprobep)
{
	struct kprobe_p;

	*/ Check collision with other optimized kprobes /*
	_p = get_optimized_kprobe((unsigned long)p->addr);
	if (unlikely(_p))
		*/ Fallback to unoptimized kprobe /*
		unoptimize_kprobe(_p, true);

	arch_arm_kprobe(p);
	optimize_kprobe(p);	*/ Try to optimize (add kprobe to a list) /*
}

*/ Remove the breakpoint of a probe. Must be called with text_mutex locked /*
static void __disarm_kprobe(struct kprobep, bool reopt)
{
	struct kprobe_p;

	*/ Try to unoptimize /*
	unoptimize_kprobe(p, kprobes_all_disarmed);

	if (!kprobe_queued(p)) {
		arch_disarm_kprobe(p);
		*/ If another kprobe was blocked, optimize it. /*
		_p = get_optimized_kprobe((unsigned long)p->addr);
		if (unlikely(_p) && reopt)
			optimize_kprobe(_p);
	}
	*/ TODO: reoptimize others after unoptimized this probe /*
}

#else */ !CONFIG_OPTPROBES /*

#define optimize_kprobe(p)			do {} while (0)
#define unoptimize_kprobe(p, f)			do {} while (0)
#define kill_optimized_kprobe(p)		do {} while (0)
#define prepare_optimized_kprobe(p)		do {} while (0)
#define try_to_optimize_kprobe(p)		do {} while (0)
#define __arm_kprobe(p)				arch_arm_kprobe(p)
#define __disarm_kprobe(p, o)			arch_disarm_kprobe(p)
#define kprobe_disarmed(p)			kprobe_disabled(p)
#define wait_for_kprobe_optimizer()		do {} while (0)

*/ There should be no unused kprobes can be reused without optimization /*
static void reuse_unused_kprobe(struct kprobeap)
{
	printk(KERN_ERR "Error: There should be no unused kprobe here.\n");
	BUG_ON(kprobe_unused(ap));
}

static void free_aggr_kprobe(struct kprobep)
{
	arch_remove_kprobe(p);
	kfree(p);
}

static struct kprobealloc_aggr_kprobe(struct kprobep)
{
	return kzalloc(sizeof(struct kprobe), GFP_KERNEL);
}
#endif */ CONFIG_OPTPROBES /*

#ifdef CONFIG_KPROBES_ON_FTRACE
static struct ftrace_ops kprobe_ftrace_ops __read_mostly = {
	.func = kprobe_ftrace_handler,
	.flags = FTRACE_OPS_FL_SAVE_REGS | FTRACE_OPS_FL_IPMODIFY,
};
static int kprobe_ftrace_enabled;

*/ Must ensure p->addr is really on ftrace /*
static int prepare_kprobe(struct kprobep)
{
	if (!kprobe_ftrace(p))
		return arch_prepare_kprobe(p);

	return arch_prepare_kprobe_ftrace(p);
}

*/ Caller must lock kprobe_mutex /*
static void arm_kprobe_ftrace(struct kprobep)
{
	int ret;

	ret = ftrace_set_filter_ip(&kprobe_ftrace_ops,
				   (unsigned long)p->addr, 0, 0);
	WARN(ret < 0, "Failed to arm kprobe-ftrace at %p (%d)\n", p->addr, ret);
	kprobe_ftrace_enabled++;
	if (kprobe_ftrace_enabled == 1) {
		ret = register_ftrace_function(&kprobe_ftrace_ops);
		WARN(ret < 0, "Failed to init kprobe-ftrace (%d)\n", ret);
	}
}

*/ Caller must lock kprobe_mutex /*
static void disarm_kprobe_ftrace(struct kprobep)
{
	int ret;

	kprobe_ftrace_enabled--;
	if (kprobe_ftrace_enabled == 0) {
		ret = unregister_ftrace_function(&kprobe_ftrace_ops);
		WARN(ret < 0, "Failed to init kprobe-ftrace (%d)\n", ret);
	}
	ret = ftrace_set_filter_ip(&kprobe_ftrace_ops,
			   (unsigned long)p->addr, 1, 0);
	WARN(ret < 0, "Failed to disarm kprobe-ftrace at %p (%d)\n", p->addr, ret);
}
#else	*/ !CONFIG_KPROBES_ON_FTRACE /*
#define prepare_kprobe(p)	arch_prepare_kprobe(p)
#define arm_kprobe_ftrace(p)	do {} while (0)
#define disarm_kprobe_ftrace(p)	do {} while (0)
#endif

*/ Arm a kprobe with text_mutex /*
static void arm_kprobe(struct kprobekp)
{
	if (unlikely(kprobe_ftrace(kp))) {
		arm_kprobe_ftrace(kp);
		return;
	}
	*/
	 Here, since __arm_kprobe() doesn't use stop_machine(),
	 this doesn't cause deadlock on text_mutex. So, we don't
	 need get_online_cpus().
	 /*
	mutex_lock(&text_mutex);
	__arm_kprobe(kp);
	mutex_unlock(&text_mutex);
}

*/ Disarm a kprobe with text_mutex /*
static void disarm_kprobe(struct kprobekp, bool reopt)
{
	if (unlikely(kprobe_ftrace(kp))) {
		disarm_kprobe_ftrace(kp);
		return;
	}
	*/ Ditto /*
	mutex_lock(&text_mutex);
	__disarm_kprobe(kp, reopt);
	mutex_unlock(&text_mutex);
}

*/
 Aggregate handlers for multiple kprobes support - these handlers
 take care of invoking the individual kprobe handlers on p->list
 /*
static int aggr_pre_handler(struct kprobep, struct pt_regsregs)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &p->list, list) {
		if (kp->pre_handler && likely(!kprobe_disabled(kp))) {
			set_kprobe_instance(kp);
			if (kp->pre_handler(kp, regs))
				return 1;
		}
		reset_kprobe_instance();
	}
	return 0;
}
NOKPROBE_SYMBOL(aggr_pre_handler);

static void aggr_post_handler(struct kprobep, struct pt_regsregs,
			      unsigned long flags)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &p->list, list) {
		if (kp->post_handler && likely(!kprobe_disabled(kp))) {
			set_kprobe_instance(kp);
			kp->post_handler(kp, regs, flags);
			reset_kprobe_instance();
		}
	}
}
NOKPROBE_SYMBOL(aggr_post_handler);

static int aggr_fault_handler(struct kprobep, struct pt_regsregs,
			      int trapnr)
{
	struct kprobecur = __this_cpu_read(kprobe_instance);

	*/
	 if we faulted "during" the execution of a user specified
	 probe handler, invoke just that probe's fault handler
	 /*
	if (cur && cur->fault_handler) {
		if (cur->fault_handler(cur, regs, trapnr))
			return 1;
	}
	return 0;
}
NOKPROBE_SYMBOL(aggr_fault_handler);

static int aggr_break_handler(struct kprobep, struct pt_regsregs)
{
	struct kprobecur = __this_cpu_read(kprobe_instance);
	int ret = 0;

	if (cur && cur->break_handler) {
		if (cur->break_handler(cur, regs))
			ret = 1;
	}
	reset_kprobe_instance();
	return ret;
}
NOKPROBE_SYMBOL(aggr_break_handler);

*/ Walks the list and increments nmissed count for multiprobe case /*
void kprobes_inc_nmissed_count(struct kprobep)
{
	struct kprobekp;
	if (!kprobe_aggrprobe(p)) {
		p->nmissed++;
	} else {
		list_for_each_entry_rcu(kp, &p->list, list)
			kp->nmissed++;
	}
	return;
}
NOKPROBE_SYMBOL(kprobes_inc_nmissed_count);

void recycle_rp_inst(struct kretprobe_instanceri,
		     struct hlist_headhead)
{
	struct kretproberp = ri->rp;

	*/ remove rp inst off the rprobe_inst_table /*
	hlist_del(&ri->hlist);
	INIT_HLIST_NODE(&ri->hlist);
	if (likely(rp)) {
		raw_spin_lock(&rp->lock);
		hlist_add_head(&ri->hlist, &rp->free_instances);
		raw_spin_unlock(&rp->lock);
	} else
		*/ Unregistering /*
		hlist_add_head(&ri->hlist, head);
}
NOKPROBE_SYMBOL(recycle_rp_inst);

void kretprobe_hash_lock(struct task_structtsk,
			 struct hlist_head*head, unsigned longflags)
__acquires(hlist_lock)
{
	unsigned long hash = hash_ptr(tsk, KPROBE_HASH_BITS);
	raw_spinlock_thlist_lock;

	*head = &kretprobe_inst_table[hash];
	hlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_lock_irqsave(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_hash_lock);

static void kretprobe_table_lock(unsigned long hash,
				 unsigned longflags)
__acquires(hlist_lock)
{
	raw_spinlock_thlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_lock_irqsave(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_table_lock);

void kretprobe_hash_unlock(struct task_structtsk,
			   unsigned longflags)
__releases(hlist_lock)
{
	unsigned long hash = hash_ptr(tsk, KPROBE_HASH_BITS);
	raw_spinlock_thlist_lock;

	hlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_unlock_irqrestore(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_hash_unlock);

static void kretprobe_table_unlock(unsigned long hash,
				   unsigned longflags)
__releases(hlist_lock)
{
	raw_spinlock_thlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_unlock_irqrestore(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_table_unlock);

*/
 This function is called from finish_task_switch when task tk becomes dead,
 so that we can recycle any function-return probe instances associated
 with this task. These left over instances represent probed functions
 that have been called but will never return.
 /*
void kprobe_flush_task(struct task_structtk)
{
	struct kretprobe_instanceri;
	struct hlist_headhead, empty_rp;
	struct hlist_nodetmp;
	unsigned long hash, flags = 0;

	if (unlikely(!kprobes_initialized))
		*/ Early boot.  kretprobe_table_locks not yet initialized. /*
		return;

	INIT_HLIST_HEAD(&empty_rp);
	hash = hash_ptr(tk, KPROBE_HASH_BITS);
	head = &kretprobe_inst_table[hash];
	kretprobe_table_lock(hash, &flags);
	hlist_for_each_entry_safe(ri, tmp, head, hlist) {
		if (ri->task == tk)
			recycle_rp_inst(ri, &empty_rp);
	}
	kretprobe_table_unlock(hash, &flags);
	hlist_for_each_entry_safe(ri, tmp, &empty_rp, hlist) {
		hlist_del(&ri->hlist);
		kfree(ri);
	}
}
NOKPROBE_SYMBOL(kprobe_flush_task);

static inline void free_rp_inst(struct kretproberp)
{
	struct kretprobe_instanceri;
	struct hlist_nodenext;

	hlist_for_each_entry_safe(ri, next, &rp->free_instances, hlist) {
		hlist_del(&ri->hlist);
		kfree(ri);
	}
}

static void cleanup_rp_inst(struct kretproberp)
{
	unsigned long flags, hash;
	struct kretprobe_instanceri;
	struct hlist_nodenext;
	struct hlist_headhead;

	*/ No race here /*
	for (hash = 0; hash < KPROBE_TABLE_SIZE; hash++) {
		kretprobe_table_lock(hash, &flags);
		head = &kretprobe_inst_table[hash];
		hlist_for_each_entry_safe(ri, next, head, hlist) {
			if (ri->rp == rp)
				ri->rp = NULL;
		}
		kretprobe_table_unlock(hash, &flags);
	}
	free_rp_inst(rp);
}
NOKPROBE_SYMBOL(cleanup_rp_inst);

*/
* Add the new probe to ap->list. Fail if this is the
* second jprobe at the address - two jprobes can't coexist
/*
static int add_new_kprobe(struct kprobeap, struct kprobep)
{
	BUG_ON(kprobe_gone(ap) || kprobe_gone(p));

	if (p->break_handler || p->post_handler)
		unoptimize_kprobe(ap, true);	*/ Fall back to normal kprobe /*

	if (p->break_handler) {
		if (ap->break_handler)
			return -EEXIST;
		list_add_tail_rcu(&p->list, &ap->list);
		ap->break_handler = aggr_break_handler;
	} else
		list_add_rcu(&p->list, &ap->list);
	if (p->post_handler && !ap->post_handler)
		ap->post_handler = aggr_post_handler;

	return 0;
}

*/
 Fill in the required fields of the "manager kprobe". Replace the
 earlier kprobe in the hlist with the manager kprobe
 /*
static void init_aggr_kprobe(struct kprobeap, struct kprobep)
{
	*/ Copy p's insn slot to ap /*
	copy_kprobe(p, ap);
	flush_insn_slot(ap);
	ap->addr = p->addr;
	ap->flags = p->flags & ~KPROBE_FLAG_OPTIMIZED;
	ap->pre_handler = aggr_pre_handler;
	ap->fault_handler = aggr_fault_handler;
	*/ We don't care the kprobe which has gone. /*
	if (p->post_handler && !kprobe_gone(p))
		ap->post_handler = aggr_post_handler;
	if (p->break_handler && !kprobe_gone(p))
		ap->break_handler = aggr_break_handler;

	INIT_LIST_HEAD(&ap->list);
	INIT_HLIST_NODE(&ap->hlist);

	list_add_rcu(&p->list, &ap->list);
	hlist_replace_rcu(&p->hlist, &ap->hlist);
}

*/
 This is the second or subsequent kprobe at the address - handle
 the intricacies
 /*
static int register_aggr_kprobe(struct kprobeorig_p, struct kprobep)
{
	int ret = 0;
	struct kprobeap = orig_p;

	*/ For preparing optimization, jump_label_text_reserved() is called /*
	jump_label_lock();
	*/
	 Get online CPUs to avoid text_mutex deadlock.with stop machine,
	 which is invoked by unoptimize_kprobe() in add_new_kprobe()
	 /*
	get_online_cpus();
	mutex_lock(&text_mutex);

	if (!kprobe_aggrprobe(orig_p)) {
		*/ If orig_p is not an aggr_kprobe, create new aggr_kprobe. /*
		ap = alloc_aggr_kprobe(orig_p);
		if (!ap) {
			ret = -ENOMEM;
			goto out;
		}
		init_aggr_kprobe(ap, orig_p);
	} else if (kprobe_unused(ap))
		*/ This probe is going to die. Rescue it /*
		reuse_unused_kprobe(ap);

	if (kprobe_gone(ap)) {
		*/
		 Attempting to insert new probe at the same location that
		 had a probe in the module vaddr area which already
		 freed. So, the instruction slot has already been
		 released. We need a new slot for the new probe.
		 /*
		ret = arch_prepare_kprobe(ap);
		if (ret)
			*/
			 Even if fail to allocate new slot, don't need to
			 free aggr_probe. It will be used next time, or
			 freed by unregister_kprobe.
			 /*
			goto out;

		*/ Prepare optimized instructions if possible. /*
		prepare_optimized_kprobe(ap);

		*/
		 Clear gone flag to prevent allocating new slot again, and
		 set disabled flag because it is not armed yet.
		 /*
		ap->flags = (ap->flags & ~KPROBE_FLAG_GONE)
			    | KPROBE_FLAG_DISABLED;
	}

	*/ Copy ap's insn slot to p /*
	copy_kprobe(ap, p);
	ret = add_new_kprobe(ap, p);

out:
	mutex_unlock(&text_mutex);
	put_online_cpus();
	jump_label_unlock();

	if (ret == 0 && kprobe_disabled(ap) && !kprobe_disabled(p)) {
		ap->flags &= ~KPROBE_FLAG_DISABLED;
		if (!kprobes_all_disarmed)
			*/ Arm the breakpoint again. /*
			arm_kprobe(ap);
	}
	return ret;
}

bool __weak arch_within_kprobe_blacklist(unsigned long addr)
{
	*/ The __kprobes marked functions and entry code must not be probed /*
	return addr >= (unsigned long)__kprobes_text_start &&
	       addr < (unsigned long)__kprobes_text_end;
}

bool within_kprobe_blacklist(unsigned long addr)
{
	struct kprobe_blacklist_entryent;

	if (arch_within_kprobe_blacklist(addr))
		return true;
	*/
	 If there exists a kprobe_blacklist, verify and
	 fail any probe registration in the prohibited area
	 /*
	list_for_each_entry(ent, &kprobe_blacklist, list) {
		if (addr >= ent->start_addr && addr < ent->end_addr)
			return true;
	}

	return false;
}

*/
 If we have a symbol_name argument, look it up and add the offset field
 to it. This way, we can specify a relative address to a symbol.
 This returns encoded errors if it fails to look up symbol or invalid
 combination of parameters.
 /*
static kprobe_opcode_tkprobe_addr(struct kprobep)
{
	kprobe_opcode_taddr = p->addr;

	if ((p->symbol_name && p->addr) ||
	    (!p->symbol_name && !p->addr))
		goto invalid;

	if (p->symbol_name) {
		kprobe_lookup_name(p->symbol_name, addr);
		if (!addr)
			return ERR_PTR(-ENOENT);
	}

	addr = (kprobe_opcode_t)(((char)addr) + p->offset);
	if (addr)
		return addr;

invalid:
	return ERR_PTR(-EINVAL);
}

*/ Check passed kprobe is valid and return kprobe in kprobe_table. /*
static struct kprobe__get_valid_kprobe(struct kprobep)
{
	struct kprobeap,list_p;

	ap = get_kprobe(p->addr);
	if (unlikely(!ap))
		return NULL;

	if (p != ap) {
		list_for_each_entry_rcu(list_p, &ap->list, list)
			if (list_p == p)
			*/ kprobe p is a valid probe /*
				goto valid;
		return NULL;
	}
valid:
	return ap;
}

*/ Return error if the kprobe is being re-registered /*
static inline int check_kprobe_rereg(struct kprobep)
{
	int ret = 0;

	mutex_lock(&kprobe_mutex);
	if (__get_valid_kprobe(p))
		ret = -EINVAL;
	mutex_unlock(&kprobe_mutex);

	return ret;
}

int __weak arch_check_ftrace_location(struct kprobep)
{
	unsigned long ftrace_addr;

	ftrace_addr = ftrace_location((unsigned long)p->addr);
	if (ftrace_addr) {
#ifdef CONFIG_KPROBES_ON_FTRACE
		*/ Given address is not on the instruction boundary /*
		if ((unsigned long)p->addr != ftrace_addr)
			return -EILSEQ;
		p->flags |= KPROBE_FLAG_FTRACE;
#else	*/ !CONFIG_KPROBES_ON_FTRACE /*
		return -EINVAL;
#endif
	}
	return 0;
}

static int check_kprobe_address_safe(struct kprobep,
				     struct module*probed_mod)
{
	int ret;

	ret = arch_check_ftrace_location(p);
	if (ret)
		return ret;
	jump_label_lock();
	preempt_disable();

	*/ Ensure it is not in reserved area nor out of text /*
	if (!kernel_text_address((unsigned long) p->addr) ||
	    within_kprobe_blacklist((unsigned long) p->addr) ||
	    jump_label_text_reserved(p->addr, p->addr)) {
		ret = -EINVAL;
		goto out;
	}

	*/ Check if are we probing a module /*
	*probed_mod = __module_text_address((unsigned long) p->addr);
	if (*probed_mod) {
		*/
		 We must hold a refcount of the probed module while updating
		 its code to prohibit unexpected unloading.
		 /*
		if (unlikely(!try_module_get(*probed_mod))) {
			ret = -ENOENT;
			goto out;
		}

		*/
		 If the module freed .init.text, we couldn't insert
		 kprobes in there.
		 /*
		if (within_module_init((unsigned long)p->addr,probed_mod) &&
		    (*probed_mod)->state != MODULE_STATE_COMING) {
			module_put(*probed_mod);
			*probed_mod = NULL;
			ret = -ENOENT;
		}
	}
out:
	preempt_enable();
	jump_label_unlock();

	return ret;
}

int register_kprobe(struct kprobep)
{
	int ret;
	struct kprobeold_p;
	struct moduleprobed_mod;
	kprobe_opcode_taddr;

	*/ Adjust probe address from symbol /*
	addr = kprobe_addr(p);
	if (IS_ERR(addr))
		return PTR_ERR(addr);
	p->addr = addr;

	ret = check_kprobe_rereg(p);
	if (ret)
		return ret;

	*/ User can pass only KPROBE_FLAG_DISABLED to register_kprobe /*
	p->flags &= KPROBE_FLAG_DISABLED;
	p->nmissed = 0;
	INIT_LIST_HEAD(&p->list);

	ret = check_kprobe_address_safe(p, &probed_mod);
	if (ret)
		return ret;

	mutex_lock(&kprobe_mutex);

	old_p = get_kprobe(p->addr);
	if (old_p) {
		*/ Since this may unoptimize old_p, locking text_mutex. /*
		ret = register_aggr_kprobe(old_p, p);
		goto out;
	}

	mutex_lock(&text_mutex);	*/ Avoiding text modification /*
	ret = prepare_kprobe(p);
	mutex_unlock(&text_mutex);
	if (ret)
		goto out;

	INIT_HLIST_NODE(&p->hlist);
	hlist_add_head_rcu(&p->hlist,
		       &kprobe_table[hash_ptr(p->addr, KPROBE_HASH_BITS)]);

	if (!kprobes_all_disarmed && !kprobe_disabled(p))
		arm_kprobe(p);

	*/ Try to optimize kprobe /*
	try_to_optimize_kprobe(p);

out:
	mutex_unlock(&kprobe_mutex);

	if (probed_mod)
		module_put(probed_mod);

	return ret;
}
EXPORT_SYMBOL_GPL(register_kprobe);

*/ Check if all probes on the aggrprobe are disabled /*
static int aggr_kprobe_disabled(struct kprobeap)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &ap->list, list)
		if (!kprobe_disabled(kp))
			*/
			 There is an active probe on the list.
			 We can't disable this ap.
			 /*
			return 0;

	return 1;
}

*/ Disable one kprobe: Make sure called under kprobe_mutex is locked /*
static struct kprobe__disable_kprobe(struct kprobep)
{
	struct kprobeorig_p;

	*/ Get an original kprobe for return /*
	orig_p = __get_valid_kprobe(p);
	if (unlikely(orig_p == NULL))
		return NULL;

	if (!kprobe_disabled(p)) {
		*/ Disable probe if it is a child probe /*
		if (p != orig_p)
			p->flags |= KPROBE_FLAG_DISABLED;

		*/ Try to disarm and disable this/parent probe /*
		if (p == orig_p || aggr_kprobe_disabled(orig_p)) {
			*/
			 If kprobes_all_disarmed is set, orig_p
			 should have already been disarmed, so
			 skip unneed disarming process.
			 /*
			if (!kprobes_all_disarmed)
				disarm_kprobe(orig_p, true);
			orig_p->flags |= KPROBE_FLAG_DISABLED;
		}
	}

	return orig_p;
}

*/
 Unregister a kprobe without a scheduler synchronization.
 /*
static int __unregister_kprobe_top(struct kprobep)
{
	struct kprobeap,list_p;

	*/ Disable kprobe. This will disarm it if needed. /*
	ap = __disable_kprobe(p);
	if (ap == NULL)
		return -EINVAL;

	if (ap == p)
		*/
		 This probe is an independent(and non-optimized) kprobe
		 (not an aggrprobe). Remove from the hash list.
		 /*
		goto disarmed;

	*/ Following process expects this probe is an aggrprobe /*
	WARN_ON(!kprobe_aggrprobe(ap));

	if (list_is_singular(&ap->list) && kprobe_disarmed(ap))
		*/
		 !disarmed could be happen if the probe is under delayed
		 unoptimizing.
		 /*
		goto disarmed;
	else {
		*/ If disabling probe has special handlers, update aggrprobe /*
		if (p->break_handler && !kprobe_gone(p))
			ap->break_handler = NULL;
		if (p->post_handler && !kprobe_gone(p)) {
			list_for_each_entry_rcu(list_p, &ap->list, list) {
				if ((list_p != p) && (list_p->post_handler))
					goto noclean;
			}
			ap->post_handler = NULL;
		}
noclean:
		*/
		 Remove from the aggrprobe: this path will do nothing in
		 __unregister_kprobe_bottom().
		 /*
		list_del_rcu(&p->list);
		if (!kprobe_disabled(ap) && !kprobes_all_disarmed)
			*/
			 Try to optimize this probe again, because post
			 handler may have been changed.
			 /*
			optimize_kprobe(ap);
	}
	return 0;

disarmed:
	BUG_ON(!kprobe_disarmed(ap));
	hlist_del_rcu(&ap->hlist);
	return 0;
}

static void __unregister_kprobe_bottom(struct kprobep)
{
	struct kprobeap;

	if (list_empty(&p->list))
		*/ This is an independent kprobe /*
		arch_remove_kprobe(p);
	else if (list_is_singular(&p->list)) {
		*/ This is the last child of an aggrprobe /*
		ap = list_entry(p->list.next, struct kprobe, list);
		list_del(&p->list);
		free_aggr_kprobe(ap);
	}
	*/ Otherwise, do nothing. /*
}

int register_kprobes(struct kprobe*kps, int num)
{
	int i, ret = 0;

	if (num <= 0)
		return -EINVAL;
	for (i = 0; i < num; i++) {
		ret = register_kprobe(kps[i]);
		if (ret < 0) {
			if (i > 0)
				unregister_kprobes(kps, i);
			break;
		}
	}
	return ret;
}
EXPORT_SYMBOL_GPL(register_kprobes);

void unregister_kprobe(struct kprobep)
{
	unregister_kprobes(&p, 1);
}
EXPORT_SYMBOL_GPL(unregister_kprobe);

void unregister_kprobes(struct kprobe*kps, int num)
{
	int i;

	if (num <= 0)
		return;
	mutex_lock(&kprobe_mutex);
	for (i = 0; i < num; i++)
		if (__unregister_kprobe_top(kps[i]) < 0)
			kps[i]->addr = NULL;
	mutex_unlock(&kprobe_mutex);

	synchronize_sched();
	for (i = 0; i < num; i++)
		if (kps[i]->addr)
			__unregister_kprobe_bottom(kps[i]);
}
EXPORT_SYMBOL_GPL(unregister_kprobes);

static struct notifier_block kprobe_exceptions_nb = {
	.notifier_call = kprobe_exceptions_notify,
	.priority = 0x7fffffff/ we need to be notified first /*
};

unsigned long __weak arch_deref_entry_point(voidentry)
{
	return (unsigned long)entry;
}

int register_jprobes(struct jprobe*jps, int num)
{
	struct jprobejp;
	int ret = 0, i;

	if (num <= 0)
		return -EINVAL;
	for (i = 0; i < num; i++) {
		unsigned long addr, offset;
		jp = jps[i];
		addr = arch_deref_entry_point(jp->entry);

		*/ Verify probepoint is a function entry point /*
		if (kallsyms_lookup_size_offset(addr, NULL, &offset) &&
		    offset == 0) {
			jp->kp.pre_handler = setjmp_pre_handler;
			jp->kp.break_handler = longjmp_break_handler;
			ret = register_kprobe(&jp->kp);
		} else
			ret = -EINVAL;

		if (ret < 0) {
			if (i > 0)
				unregister_jprobes(jps, i);
			break;
		}
	}
	return ret;
}
EXPORT_SYMBOL_GPL(register_jprobes);

int register_jprobe(struct jprobejp)
{
	return register_jprobes(&jp, 1);
}
EXPORT_SYMBOL_GPL(register_jprobe);

void unregister_jprobe(struct jprobejp)
{
	unregister_jprobes(&jp, 1);
}
EXPORT_SYMBOL_GPL(unregister_jprobe);

void unregister_jprobes(struct jprobe*jps, int num)
{
	int i;

	if (num <= 0)
		return;
	mutex_lock(&kprobe_mutex);
	for (i = 0; i < num; i++)
		if (__unregister_kprobe_top(&jps[i]->kp) < 0)
			jps[i]->kp.addr = NULL;
	mutex_unlock(&kprobe_mutex);

	synchronize_sched();
	for (i = 0; i < num; i++) {
		if (jps[i]->kp.addr)
			__unregister_kprobe_bottom(&jps[i]->kp);
	}
}
EXPORT_SYMBOL_GPL(unregister_jprobes);

#ifdef CONFIG_KRETPROBES
*/
 This kprobe pre_handler is registered with every kretprobe. When probe
 hits it will set up the return probe.
 /*
static int pre_handler_kretprobe(struct kprobep, struct pt_regsregs)
{
	struct kretproberp = container_of(p, struct kretprobe, kp);
	unsigned long hash, flags = 0;
	struct kretprobe_instanceri;

	*/
	 To avoid deadlocks, prohibit return probing in NMI contexts,
	 just skip the probe and increase the (inexact) 'nmissed'
	 statistical counter, so that the user is informed that
	 something happened:
	 /*
	if (unlikely(in_nmi())) {
		rp->nmissed++;
		return 0;
	}

	*/ TODO: consider to only swap the RA after the last pre_handler fired /*
	hash = hash_ptr(current, KPROBE_HASH_BITS);
	raw_spin_lock_irqsave(&rp->lock, flags);
	if (!hlist_empty(&rp->free_instances)) {
		ri = hlist_entry(rp->free_instances.first,
				struct kretprobe_instance, hlist);
		hlist_del(&ri->hlist);
		raw_spin_unlock_irqrestore(&rp->lock, flags);

		ri->rp = rp;
		ri->task = current;

		if (rp->entry_handler && rp->entry_handler(ri, regs)) {
			raw_spin_lock_irqsave(&rp->lock, flags);
			hlist_add_head(&ri->hlist, &rp->free_instances);
			raw_spin_unlock_irqrestore(&rp->lock, flags);
			return 0;
		}

		arch_prepare_kretprobe(ri, regs);

		*/ XXX(hch): why is there no hlist_move_head? /*
		INIT_HLIST_NODE(&ri->hlist);
		kretprobe_table_lock(hash, &flags);
		hlist_add_head(&ri->hlist, &kretprobe_inst_table[hash]);
		kretprobe_table_unlock(hash, &flags);
	} else {
		rp->nmissed++;
		raw_spin_unlock_irqrestore(&rp->lock, flags);
	}
	return 0;
}
NOKPROBE_SYMBOL(pre_handler_kretprobe);

int register_kretprobe(struct kretproberp)
{
	int ret = 0;
	struct kretprobe_instanceinst;
	int i;
	voidaddr;

	if (kretprobe_blacklist_size) {
		addr = kprobe_addr(&rp->kp);
		if (IS_ERR(addr))
			return PTR_ERR(addr);

		for (i = 0; kretprobe_blacklist[i].name != NULL; i++) {
			if (kretprobe_blacklist[i].addr == addr)
				return -EINVAL;
		}
	}

	rp->kp.pre_handler = pre_handler_kretprobe;
	rp->kp.post_handler = NULL;
	rp->kp.fault_handler = NULL;
	rp->kp.break_handler = NULL;

	*/ Pre-allocate memory for max kretprobe instances /*
	if (rp->maxactive <= 0) {
#ifdef CONFIG_PREEMPT
		rp->maxactive = max_t(unsigned int, 10, 2*num_possible_cpus());
#else
		rp->maxactive = num_possible_cpus();
#endif
	}
	raw_spin_lock_init(&rp->lock);
	INIT_HLIST_HEAD(&rp->free_instances);
	for (i = 0; i < rp->maxactive; i++) {
		inst = kmalloc(sizeof(struct kretprobe_instance) +
			       rp->data_size, GFP_KERNEL);
		if (inst == NULL) {
			free_rp_inst(rp);
			return -ENOMEM;
		}
		INIT_HLIST_NODE(&inst->hlist);
		hlist_add_head(&inst->hlist, &rp->free_instances);
	}

	rp->nmissed = 0;
	*/ Establish function entry probe point /*
	ret = register_kprobe(&rp->kp);
	if (ret != 0)
		free_rp_inst(rp);
	return ret;
}
EXPORT_SYMBOL_GPL(register_kretprobe);

int register_kretprobes(struct kretprobe*rps, int num)
{
	int ret = 0, i;

	if (num <= 0)
		return -EINVAL;
	for (i = 0; i < num; i++) {
		ret = register_kretprobe(rps[i]);
		if (ret < 0) {
			if (i > 0)
				unregister_kretprobes(rps, i);
			break;
		}
	}
	return ret;
}
EXPORT_SYMBOL_GPL(register_kretprobes);

void unregister_kretprobe(struct kretproberp)
{
	unregister_kretprobes(&rp, 1);
}
EXPORT_SYMBOL_GPL(unregister_kretprobe);

void unregister_kretprobes(struct kretprobe*rps, int num)
{
	int i;

	if (num <= 0)
		return;
	mutex_lock(&kprobe_mutex);
	for (i = 0; i < num; i++)
		if (__unregister_kprobe_top(&rps[i]->kp) < 0)
			rps[i]->kp.addr = NULL;
	mutex_unlock(&kprobe_mutex);

	synchronize_sched();
	for (i = 0; i < num; i++) {
		if (rps[i]->kp.addr) {
			__unregister_kprobe_bottom(&rps[i]->kp);
			cleanup_rp_inst(rps[i]);
		}
	}
}
EXPORT_SYMBOL_GPL(unregister_kretprobes);

#else */ CONFIG_KRETPROBES /*
int register_kretprobe(struct kretproberp)
{
	return -ENOSYS;
}
EXPORT_SYMBOL_GPL(register_kretprobe);

int register_kretprobes(struct kretprobe*rps, int num)
{
	return -ENOSYS;
}
EXPORT_SYMBOL_GPL(register_kretprobes);

void unregister_kretprobe(struct kretproberp)
{
}
EXPORT_SYMBOL_GPL(unregister_kretprobe);

void unregister_kretprobes(struct kretprobe*rps, int num)
{
}
EXPORT_SYMBOL_GPL(unregister_kretprobes);

static int pre_handler_kretprobe(struct kprobep, struct pt_regsregs)
{
	return 0;
}
NOKPROBE_SYMBOL(pre_handler_kretprobe);

#endif */ CONFIG_KRETPROBES /*

*/ Set the kprobe gone and remove its instruction buffer. /*
static void kill_kprobe(struct kprobep)
{
	struct kprobekp;

	p->flags |= KPROBE_FLAG_GONE;
	if (kprobe_aggrprobe(p)) {
		*/
		 If this is an aggr_kprobe, we have to list all the
		 chained probes and mark them GONE.
		 /*
		list_for_each_entry_rcu(kp, &p->list, list)
			kp->flags |= KPROBE_FLAG_GONE;
		p->post_handler = NULL;
		p->break_handler = NULL;
		kill_optimized_kprobe(p);
	}
	*/
	 Here, we can remove insn_slot safely, because no thread calls
	 the original probed function (which will be freed soon) any more.
	 /*
	arch_remove_kprobe(p);
}

*/ Disable one kprobe /*
int disable_kprobe(struct kprobekp)
{
	int ret = 0;

	mutex_lock(&kprobe_mutex);

	*/ Disable this kprobe /*
	if (__disable_kprobe(kp) == NULL)
		ret = -EINVAL;

	mutex_unlock(&kprobe_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(disable_kprobe);

*/ Enable one kprobe /*
int enable_kprobe(struct kprobekp)
{
	int ret = 0;
	struct kprobep;

	mutex_lock(&kprobe_mutex);

	*/ Check whether specified probe is valid. /*
	p = __get_valid_kprobe(kp);
	if (unlikely(p == NULL)) {
		ret = -EINVAL;
		goto out;
	}

	if (kprobe_gone(kp)) {
		*/ This kprobe has gone, we couldn't enable it. /*
		ret = -EINVAL;
		goto out;
	}

	if (p != kp)
		kp->flags &= ~KPROBE_FLAG_DISABLED;

	if (!kprobes_all_disarmed && kprobe_disabled(p)) {
		p->flags &= ~KPROBE_FLAG_DISABLED;
		arm_kprobe(p);
	}
out:
	mutex_unlock(&kprobe_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(enable_kprobe);

void dump_kprobe(struct kprobekp)
{
	printk(KERN_WARNING "Dumping kprobe:\n");
	printk(KERN_WARNING "Name: %s\nAddress: %p\nOffset: %x\n",
	       kp->symbol_name, kp->addr, kp->offset);
}
NOKPROBE_SYMBOL(dump_kprobe);

*/
 Lookup and populate the kprobe_blacklist.

 Unlike the kretprobe blacklist, we'll need to determine
 the range of addresses that belong to the said functions,
 since a kprobe need not necessarily be at the beginning
 of a function.
 /*
static int __init populate_kprobe_blacklist(unsigned longstart,
					     unsigned longend)
{
	unsigned longiter;
	struct kprobe_blacklist_entryent;
	unsigned long entry, offset = 0, size = 0;

	for (iter = start; iter < end; iter++) {
		entry = arch_deref_entry_point((void)*iter);

		if (!kernel_text_address(entry) ||
		    !kallsyms_lookup_size_offset(entry, &size, &offset)) {
			pr_err("Failed to find blacklist at %p\n",
				(void)entry);
			continue;
		}

		ent = kmalloc(sizeof(*ent), GFP_KERNEL);
		if (!ent)
			return -ENOMEM;
		ent->start_addr = entry;
		ent->end_addr = entry + size;
		INIT_LIST_HEAD(&ent->list);
		list_add_tail(&ent->list, &kprobe_blacklist);
	}
	return 0;
}

*/ Module notifier call back, checking kprobes on the module /*
static int kprobes_module_callback(struct notifier_blocknb,
				   unsigned long val, voiddata)
{
	struct modulemod = data;
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;
	int checkcore = (val == MODULE_STATE_GOING);

	if (val != MODULE_STATE_GOING && val != MODULE_STATE_LIVE)
		return NOTIFY_DONE;

	*/
	 When MODULE_STATE_GOING was notified, both of module .text and
	 .init.text sections would be freed. When MODULE_STATE_LIVE was
	 notified, only .init.text section would be freed. We need to
	 disable kprobes which have been inserted in the sections.
	 /*
	mutex_lock(&kprobe_mutex);
	for (i = 0; i < KPROBE_TABLE_SIZE; i++) {
		head = &kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist)
			if (within_module_init((unsigned long)p->addr, mod) ||
			    (checkcore &&
			     within_module_core((unsigned long)p->addr, mod))) {
				*/
				 The vaddr this probe is installed will soon
				 be vfreed buy not synced to disk. Hence,
				 disarming the breakpoint isn't needed.
				 /*
				kill_kprobe(p);
			}
	}
	mutex_unlock(&kprobe_mutex);
	return NOTIFY_DONE;
}

static struct notifier_block kprobe_module_nb = {
	.notifier_call = kprobes_module_callback,
	.priority = 0
};

*/ Markers of _kprobe_blacklist section /*
extern unsigned long __start_kprobe_blacklist[];
extern unsigned long __stop_kprobe_blacklist[];

static int __init init_kprobes(void)
{
	int i, err = 0;

	*/ FIXME allocate the probe table, currently defined statically /*
	*/ initialize all list heads /*
	for (i = 0; i < KPROBE_TABLE_SIZE; i++) {
		INIT_HLIST_HEAD(&kprobe_table[i]);
		INIT_HLIST_HEAD(&kretprobe_inst_table[i]);
		raw_spin_lock_init(&(kretprobe_table_locks[i].lock));
	}

	err = populate_kprobe_blacklist(__start_kprobe_blacklist,
					__stop_kprobe_blacklist);
	if (err) {
		pr_err("kprobes: failed to populate blacklist: %d\n", err);
		pr_err("Please take care of using kprobes.\n");
	}

	if (kretprobe_blacklist_size) {
		*/ lookup the function address from its name /*
		for (i = 0; kretprobe_blacklist[i].name != NULL; i++) {
			kprobe_lookup_name(kretprobe_blacklist[i].name,
					   kretprobe_blacklist[i].addr);
			if (!kretprobe_blacklist[i].addr)
				printk("kretprobe: lookup failed: %s\n",
				       kretprobe_blacklist[i].name);
		}
	}

#if defined(CONFIG_OPTPROBES)
#if defined(__ARCH_WANT_KPROBES_INSN_SLOT)
	*/ Init kprobe_optinsn_slots /*
	kprobe_optinsn_slots.insn_size = MAX_OPTINSN_SIZE;
#endif
	*/ By default, kprobes can be optimized /*
	kprobes_allow_optimization = true;
#endif

	*/ By default, kprobes are armed /*
	kprobes_all_disarmed = false;

	err = arch_init_kprobes();
	if (!err)
		err = register_die_notifier(&kprobe_exceptions_nb);
	if (!err)
		err = register_module_notifier(&kprobe_module_nb);

	kprobes_initialized = (err == 0);

	if (!err)
		init_test_probes();
	return err;
}

#ifdef CONFIG_DEBUG_FS
static void report_probe(struct seq_filepi, struct kprobep,
		const charsym, int offset, charmodname, struct kprobepp)
{
	charkprobe_type;

	if (p->pre_handler == pre_handler_kretprobe)
		kprobe_type = "r";
	else if (p->pre_handler == setjmp_pre_handler)
		kprobe_type = "j";
	else
		kprobe_type = "k";

	if (sym)
		seq_printf(pi, "%p  %s  %s+0x%x  %s ",
			p->addr, kprobe_type, sym, offset,
			(modname ? modname : " "));
	else
		seq_printf(pi, "%p  %s  %p ",
			p->addr, kprobe_type, p->addr);

	if (!pp)
		pp = p;
	seq_printf(pi, "%s%s%s%s\n",
		(kprobe_gone(p) ? "[GONE]" : ""),
		((kprobe_disabled(p) && !kprobe_gone(p)) ?  "[DISABLED]" : ""),
		(kprobe_optimized(pp) ? "[OPTIMIZED]" : ""),
		(kprobe_ftrace(pp) ? "[FTRACE]" : ""));
}

static voidkprobe_seq_start(struct seq_filef, loff_tpos)
{
	return (*pos < KPROBE_TABLE_SIZE) ? pos : NULL;
}

static voidkprobe_seq_next(struct seq_filef, voidv, loff_tpos)
{
	(*pos)++;
	if (*pos >= KPROBE_TABLE_SIZE)
		return NULL;
	return pos;
}

static void kprobe_seq_stop(struct seq_filef, voidv)
{
	*/ Nothing to do /*
}

static int show_kprobe_addr(struct seq_filepi, voidv)
{
	struct hlist_headhead;
	struct kprobep,kp;
	const charsym = NULL;
	unsigned int i =(loff_t) v;
	unsigned long offset = 0;
	charmodname, namebuf[KSYM_NAME_LEN];

	head = &kprobe_table[i];
	preempt_disable();
	hlist_for_each_entry_rcu(p, head, hlist) {
		sym = kallsyms_lookup((unsigned long)p->addr, NULL,
					&offset, &modname, namebuf);
		if (kprobe_aggrprobe(p)) {
			list_for_each_entry_rcu(kp, &p->list, list)
				report_probe(pi, kp, sym, offset, modname, p);
		} else
			report_probe(pi, p, sym, offset, modname, NULL);
	}
	preempt_enable();
	return 0;
}

static const struct seq_operations kprobes_seq_ops = {
	.start = kprobe_seq_start,
	.next  = kprobe_seq_next,
	.stop  = kprobe_seq_stop,
	.show  = show_kprobe_addr
};

static int kprobes_open(struct inodeinode, struct filefilp)
{
	return seq_open(filp, &kprobes_seq_ops);
}

static const struct file_operations debugfs_kprobes_operations = {
	.open           = kprobes_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = seq_release,
};

*/ kprobes/blacklist -- shows which functions can not be probed /*
static voidkprobe_blacklist_seq_start(struct seq_filem, loff_tpos)
{
	return seq_list_start(&kprobe_blacklist,pos);
}

static voidkprobe_blacklist_seq_next(struct seq_filem, voidv, loff_tpos)
{
	return seq_list_next(v, &kprobe_blacklist, pos);
}

static int kprobe_blacklist_seq_show(struct seq_filem, voidv)
{
	struct kprobe_blacklist_entryent =
		list_entry(v, struct kprobe_blacklist_entry, list);

	seq_printf(m, "0x%p-0x%p\t%ps\n", (void)ent->start_addr,
		   (void)ent->end_addr, (void)ent->start_addr);
	return 0;
}

static const struct seq_operations kprobe_blacklist_seq_ops = {
	.start = kprobe_blacklist_seq_start,
	.next  = kprobe_blacklist_seq_next,
	.stop  = kprobe_seq_stop,	*/ Reuse void function /*
	.show  = kprobe_blacklist_seq_show,
};

static int kprobe_blacklist_open(struct inodeinode, struct filefilp)
{
	return seq_open(filp, &kprobe_blacklist_seq_ops);
}

static const struct file_operations debugfs_kprobe_blacklist_ops = {
	.open           = kprobe_blacklist_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = seq_release,
};

static void arm_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&kprobe_mutex);

	*/ If kprobes are armed, just return /*
	if (!kprobes_all_disarmed)
		goto already_enabled;

	*/
	 optimize_kprobe() called by arm_kprobe() checks
	 kprobes_all_disarmed, so set kprobes_all_disarmed before
	 arm_kprobe.
	 /*
	kprobes_all_disarmed = false;
	*/ Arming kprobes doesn't optimize kprobe itself /*
	for (i = 0; i < KPROBE_TABLE_SIZE; i++) {
		head = &kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist)
			if (!kprobe_disabled(p))
				arm_kprobe(p);
	}

	printk(KERN_INFO "Kprobes globally enabled\n");

already_enabled:
	mutex_unlock(&kprobe_mutex);
	return;
}

static void disarm_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&kprobe_mutex);

	*/ If kprobes are already disarmed, just return /*
	if (kprobes_all_disarmed) {
		mutex_unlock(&kprobe_mutex);
		return;
	}

	kprobes_all_disarmed = true;
	printk(KERN_INFO "Kprobes globally disabled\n");

	for (i = 0; i < KPROBE_TABLE_SIZE; i++) {
		head = &kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist) {
			if (!arch_trampoline_kprobe(p) && !kprobe_disabled(p))
				disarm_kprobe(p, false);
		}
	}
	mutex_unlock(&kprobe_mutex);

	*/ Wait for disarming all kprobes by optimizer /*
	wait_for_kprobe_optimizer();
}

*/
 XXX: The debugfs bool file interface doesn't allow for callbacks
 when the bool state is switched. We can reuse that facility when
 available
 /*
static ssize_t read_enabled_file_bool(struct filefile,
	       char __useruser_buf, size_t count, loff_tppos)
{
	char buf[3];

	if (!kprobes_all_disarmed)
		buf[0] = '1';
	else
		buf[0] = '0';
	buf[1] = '\n';
	buf[2] = 0x00;
	return simple_read_from_buffer(user_buf, count, ppos, buf, 2);
}

static ssize_t write_enabled_file_bool(struct filefile,
	       const char __useruser_buf, size_t count, loff_tppos)
{
	char buf[32];
	size_t buf_size;

	buf_size = min(count, (sizeof(buf)-1));
	if (copy_from_user(buf, user_buf, buf_size))
		return -EFAULT;

	buf[buf_size] = '\0';
	switch (buf[0]) {
	case 'y':
	case 'Y':
	case '1':
		arm_all_kprobes();
		break;
	case 'n':
	case 'N':
	case '0':
		disarm_all_kprobes();
		break;
	default:
		return -EINVAL;
	}

	return count;
}

static const struct file_operations fops_kp = {
	.read =         read_enabled_file_bool,
	.write =        write_enabled_file_bool,
	.llseek =	default_llseek,
};

static int __init debugfs_kprobe_init(void)
{
	struct dentrydir,file;
	unsigned int value = 1;

	dir = debugfs_create_dir("kprobes", NULL);
	if (!dir)
		return -ENOMEM;

	file = debugfs_create_file("list", 0444, dir, NULL,
				&debugfs_kprobes_operations);
	if (!file)
		goto error;

	file = debugfs_create_file("enabled", 0600, dir,
					&value, &fops_kp);
	if (!file)
		goto error;

	file = debugfs_create_file("blacklist", 0444, dir, NULL,
				&debugfs_kprobe_blacklist_ops);
	if (!file)
		goto error;

	return 0;

error:
	debugfs_remove(dir);
	return -ENOMEM;
}

late_initcall(debugfs_kprobe_init);
#endif*/ CONFIG_DEBUG_FS /*

module_init(init_kprobes);

*/ defined in arch/.../kernel/kprobes.c /*
EXPORT_SYMBOL_GPL(jprobe_return);
*/

 kernel/ksysfs.c - sysfs attributes in /sys/kernel, which
 		     are not related to any other subsystem

 Copyright (C) 2004 Kay Sievers <kay.sievers@vrfy.org>
 
 This file is release under the GPLv2

 /*

#include <linux/kobject.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/export.h>
#include <linux/init.h>
#include <linux/kexec.h>
#include <linux/profile.h>
#include <linux/stat.h>
#include <linux/sched.h>
#include <linux/capability.h>
#include <linux/compiler.h>

#include <linux/rcupdate.h>	*/ rcu_expedited and rcu_normal /*

#define KERNEL_ATTR_RO(_name) \
static struct kobj_attribute _name##_attr = __ATTR_RO(_name)

#define KERNEL_ATTR_RW(_name) \
static struct kobj_attribute _name##_attr = \
	__ATTR(_name, 0644, _name##_show, _name##_store)

*/ current uevent sequence number /*
static ssize_t uevent_seqnum_show(struct kobjectkobj,
				  struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%llu\n", (unsigned long long)uevent_seqnum);
}
KERNEL_ATTR_RO(uevent_seqnum);

#ifdef CONFIG_UEVENT_HELPER
*/ uevent helper program, used during early boot /*
static ssize_t uevent_helper_show(struct kobjectkobj,
				  struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%s\n", uevent_helper);
}
static ssize_t uevent_helper_store(struct kobjectkobj,
				   struct kobj_attributeattr,
				   const charbuf, size_t count)
{
	if (count+1 > UEVENT_HELPER_PATH_LEN)
		return -ENOENT;
	memcpy(uevent_helper, buf, count);
	uevent_helper[count] = '\0';
	if (count && uevent_helper[count-1] == '\n')
		uevent_helper[count-1] = '\0';
	return count;
}
KERNEL_ATTR_RW(uevent_helper);
#endif

#ifdef CONFIG_PROFILING
static ssize_t profiling_show(struct kobjectkobj,
				  struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%d\n", prof_on);
}
static ssize_t profiling_store(struct kobjectkobj,
				   struct kobj_attributeattr,
				   const charbuf, size_t count)
{
	int ret;

	if (prof_on)
		return -EEXIST;
	*/
	 This eventually calls into get_option() which
	 has a ton of callers and is not const.  It is
	 easiest to cast it away here.
	 /*
	profile_setup((char)buf);
	ret = profile_init();
	if (ret)
		return ret;
	ret = create_proc_profile();
	if (ret)
		return ret;
	return count;
}
KERNEL_ATTR_RW(profiling);
#endif

#ifdef CONFIG_KEXEC_CORE
static ssize_t kexec_loaded_show(struct kobjectkobj,
				 struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%d\n", !!kexec_image);
}
KERNEL_ATTR_RO(kexec_loaded);

static ssize_t kexec_crash_loaded_show(struct kobjectkobj,
				       struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%d\n", !!kexec_crash_image);
}
KERNEL_ATTR_RO(kexec_crash_loaded);

static ssize_t kexec_crash_size_show(struct kobjectkobj,
				       struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%zu\n", crash_get_memory_size());
}
static ssize_t kexec_crash_size_store(struct kobjectkobj,
				   struct kobj_attributeattr,
				   const charbuf, size_t count)
{
	unsigned long cnt;
	int ret;

	if (kstrtoul(buf, 0, &cnt))
		return -EINVAL;

	ret = crash_shrink_memory(cnt);
	return ret < 0 ? ret : count;
}
KERNEL_ATTR_RW(kexec_crash_size);

static ssize_t vmcoreinfo_show(struct kobjectkobj,
			       struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%lx %x\n",
		       paddr_vmcoreinfo_note(),
		       (unsigned int)sizeof(vmcoreinfo_note));
}
KERNEL_ATTR_RO(vmcoreinfo);

#endif */ CONFIG_KEXEC_CORE /*

*/ whether file capabilities are enabled /*
static ssize_t fscaps_show(struct kobjectkobj,
				  struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%d\n", file_caps_enabled);
}
KERNEL_ATTR_RO(fscaps);

#ifndef CONFIG_TINY_RCU
int rcu_expedited;
static ssize_t rcu_expedited_show(struct kobjectkobj,
				  struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%d\n", READ_ONCE(rcu_expedited));
}
static ssize_t rcu_expedited_store(struct kobjectkobj,
				   struct kobj_attributeattr,
				   const charbuf, size_t count)
{
	if (kstrtoint(buf, 0, &rcu_expedited))
		return -EINVAL;

	return count;
}
KERNEL_ATTR_RW(rcu_expedited);

int rcu_normal;
static ssize_t rcu_normal_show(struct kobjectkobj,
			       struct kobj_attributeattr, charbuf)
{
	return sprintf(buf, "%d\n", READ_ONCE(rcu_normal));
}
static ssize_t rcu_normal_store(struct kobjectkobj,
				struct kobj_attributeattr,
				const charbuf, size_t count)
{
	if (kstrtoint(buf, 0, &rcu_normal))
		return -EINVAL;

	return count;
}
KERNEL_ATTR_RW(rcu_normal);
#endif */ #ifndef CONFIG_TINY_RCU /*

*/
 Make /sys/kernel/notes give the raw contents of our kernel .notes section.
 /*
extern const void __start_notes __weak;
extern const void __stop_notes __weak;
#define	notes_size (&__stop_notes - &__start_notes)

static ssize_t notes_read(struct filefilp, struct kobjectkobj,
			  struct bin_attributebin_attr,
			  charbuf, loff_t off, size_t count)
{
	memcpy(buf, &__start_notes + off, count);
	return count;
}

static struct bin_attribute notes_attr = {
	.attr = {
		.name = "notes",
		.mode = S_IRUGO,
	},
	.read = &notes_read,
};

struct kobjectkernel_kobj;
EXPORT_SYMBOL_GPL(kernel_kobj);

static struct attribute kernel_attrs[] = {
	&fscaps_attr.attr,
	&uevent_seqnum_attr.attr,
#ifdef CONFIG_UEVENT_HELPER
	&uevent_helper_attr.attr,
#endif
#ifdef CONFIG_PROFILING
	&profiling_attr.attr,
#endif
#ifdef CONFIG_KEXEC_CORE
	&kexec_loaded_attr.attr,
	&kexec_crash_loaded_attr.attr,
	&kexec_crash_size_attr.attr,
	&vmcoreinfo_attr.attr,
#endif
#ifndef CONFIG_TINY_RCU
	&rcu_expedited_attr.attr,
	&rcu_normal_attr.attr,
#endif
	NULL
};

static struct attribute_group kernel_attr_group = {
	.attrs = kernel_attrs,
};

static int __init ksysfs_init(void)
{
	int error;

	kernel_kobj = kobject_create_and_add("kernel", NULL);
	if (!kernel_kobj) {
		error = -ENOMEM;
		goto exit;
	}
	error = sysfs_create_group(kernel_kobj, &kernel_attr_group);
	if (error)
		goto kset_exit;

	if (notes_size > 0) {
		notes_attr.size = notes_size;
		error = sysfs_create_bin_file(kernel_kobj, &notes_attr);
		if (error)
			goto group_exit;
	}

	return 0;

group_exit:
	sysfs_remove_group(kernel_kobj, &kernel_attr_group);
kset_exit:
	kobject_put(kernel_kobj);
exit:
	return error;
}

core_initcall(ksysfs_init);
*/
   Kernel thread helper functions.
   Copyright (C) 2004 IBM Corporation, Rusty Russell.

 Creation is done via kthreadd, so that we get a clean environment
 even if we're invoked from userspace (think modprobe, hotplug cpu,
 etc.).
 /*
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/completion.h>
#include <linux/err.h>
#include <linux/cpuset.h>
#include <linux/unistd.h>
#include <linux/file.h>
#include <linux/export.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/freezer.h>
#include <linux/ptrace.h>
#include <linux/uaccess.h>
#include <trace/events/sched.h>

static DEFINE_SPINLOCK(kthread_create_lock);
static LIST_HEAD(kthread_create_list);
struct task_structkthreadd_task;

struct kthread_create_info
{
	*/ Information passed to kthread() from kthreadd. /*
	int (*threadfn)(voiddata);
	voiddata;
	int node;

	*/ Result passed back to kthread_create() from kthreadd. /*
	struct task_structresult;
	struct completiondone;

	struct list_head list;
};

struct kthread {
	unsigned long flags;
	unsigned int cpu;
	voiddata;
	struct completion parked;
	struct completion exited;
};

enum KTHREAD_BITS {
	KTHREAD_IS_PER_CPU = 0,
	KTHREAD_SHOULD_STOP,
	KTHREAD_SHOULD_PARK,
	KTHREAD_IS_PARKED,
};

#define __to_kthread(vfork)	\
	container_of(vfork, struct kthread, exited)

static inline struct kthreadto_kthread(struct task_structk)
{
	return __to_kthread(k->vfork_done);
}

static struct kthreadto_live_kthread(struct task_structk)
{
	struct completionvfork = ACCESS_ONCE(k->vfork_done);
	if (likely(vfork))
		return __to_kthread(vfork);
	return NULL;
}

*/
 kthread_should_stop - should this kthread return now?

 When someone calls kthread_stop() on your kthread, it will be woken
 and this will return true.  You should then return, and your return
 value will be passed through to kthread_stop().
 /*
bool kthread_should_stop(void)
{
	return test_bit(KTHREAD_SHOULD_STOP, &to_kthread(current)->flags);
}
EXPORT_SYMBOL(kthread_should_stop);

*/
 kthread_should_park - should this kthread park now?

 When someone calls kthread_park() on your kthread, it will be woken
 and this will return true.  You should then do the necessary
 cleanup and call kthread_parkme()

 Similar to kthread_should_stop(), but this keeps the thread alive
 and in a park position. kthread_unpark() "restarts" the thread and
 calls the thread function again.
 /*
bool kthread_should_park(void)
{
	return test_bit(KTHREAD_SHOULD_PARK, &to_kthread(current)->flags);
}
EXPORT_SYMBOL_GPL(kthread_should_park);

*/
 kthread_freezable_should_stop - should this freezable kthread return now?
 @was_frozen: optional out parameter, indicates whether %current was frozen

 kthread_should_stop() for freezable kthreads, which will enter
 refrigerator if necessary.  This function is safe from kthread_stop() /
 freezer deadlock and freezable kthreads should use this function instead
 of calling try_to_freeze() directly.
 /*
bool kthread_freezable_should_stop(boolwas_frozen)
{
	bool frozen = false;

	might_sleep();

	if (unlikely(freezing(current)))
		frozen = __refrigerator(true);

	if (was_frozen)
		*was_frozen = frozen;

	return kthread_should_stop();
}
EXPORT_SYMBOL_GPL(kthread_freezable_should_stop);

*/
 kthread_data - return data value specified on kthread creation
 @task: kthread task in question

 Return the data value specified when kthread @task was created.
 The caller is responsible for ensuring the validity of @task when
 calling this function.
 /*
voidkthread_data(struct task_structtask)
{
	return to_kthread(task)->data;
}

*/
 probe_kthread_data - speculative version of kthread_data()
 @task: possible kthread task in question

 @task could be a kthread task.  Return the data value specified when it
 was created if accessible.  If @task isn't a kthread task or its data is
 inaccessible for any reason, %NULL is returned.  This function requires
 that @task itself is safe to dereference.
 /*
voidprobe_kthread_data(struct task_structtask)
{
	struct kthreadkthread = to_kthread(task);
	voiddata = NULL;

	probe_kernel_read(&data, &kthread->data, sizeof(data));
	return data;
}

static void __kthread_parkme(struct kthreadself)
{
	__set_current_state(TASK_PARKED);
	while (test_bit(KTHREAD_SHOULD_PARK, &self->flags)) {
		if (!test_and_set_bit(KTHREAD_IS_PARKED, &self->flags))
			complete(&self->parked);
		schedule();
		__set_current_state(TASK_PARKED);
	}
	clear_bit(KTHREAD_IS_PARKED, &self->flags);
	__set_current_state(TASK_RUNNING);
}

void kthread_parkme(void)
{
	__kthread_parkme(to_kthread(current));
}
EXPORT_SYMBOL_GPL(kthread_parkme);

static int kthread(void_create)
{
	*/ Copy data: it's on kthread's stack /*
	struct kthread_create_infocreate = _create;
	int (*threadfn)(voiddata) = create->threadfn;
	voiddata = create->data;
	struct completiondone;
	struct kthread self;
	int ret;

	self.flags = 0;
	self.data = data;
	init_completion(&self.exited);
	init_completion(&self.parked);
	current->vfork_done = &self.exited;

	*/ If user was SIGKILLed, I release the structure. /*
	done = xchg(&create->done, NULL);
	if (!done) {
		kfree(create);
		do_exit(-EINTR);
	}
	*/ OK, tell user we're spawned, wait for stop or wakeup /*
	__set_current_state(TASK_UNINTERRUPTIBLE);
	create->result = current;
	complete(done);
	schedule();

	ret = -EINTR;

	if (!test_bit(KTHREAD_SHOULD_STOP, &self.flags)) {
		__kthread_parkme(&self);
		ret = threadfn(data);
	}
	*/ we can't just return, we must preserve "self" on stack /*
	do_exit(ret);
}

*/ called from do_fork() to get node information for about to be created task /*
int tsk_fork_get_node(struct task_structtsk)
{
#ifdef CONFIG_NUMA
	if (tsk == kthreadd_task)
		return tsk->pref_node_fork;
#endif
	return NUMA_NO_NODE;
}

static void create_kthread(struct kthread_create_infocreate)
{
	int pid;

#ifdef CONFIG_NUMA
	current->pref_node_fork = create->node;
#endif
	*/ We want our own signal handler (we take no signals by default). /*
	pid = kernel_thread(kthread, create, CLONE_FS | CLONE_FILES | SIGCHLD);
	if (pid < 0) {
		*/ If user was SIGKILLed, I release the structure. /*
		struct completiondone = xchg(&create->done, NULL);

		if (!done) {
			kfree(create);
			return;
		}
		create->result = ERR_PTR(pid);
		complete(done);
	}
}

*/
 kthread_create_on_node - create a kthread.
 @threadfn: the function to run until signal_pending(current).
 @data: data ptr for @threadfn.
 @node: task and thread structures for the thread are allocated on this node
 @namefmt: printf-style name for the thread.

 Description: This helper function creates and names a kernel
 thread.  The thread will be stopped: use wake_up_process() to start
 it.  See also kthread_run().  The new thread has SCHED_NORMAL policy and
 is affine to all CPUs.

 If thread is going to be bound on a particular cpu, give its node
 in @node, to get NUMA affinity for kthread stack, or else give NUMA_NO_NODE.
 When woken, the thread will run @threadfn() with @data as its
 argument. @threadfn() can either call do_exit() directly if it is a
 standalone thread for which no one will call kthread_stop(), or
 return when 'kthread_should_stop()' is true (which means
 kthread_stop() has been called).  The return value should be zero
 or a negative error number; it will be passed to kthread_stop().

 Returns a task_struct or ERR_PTR(-ENOMEM) or ERR_PTR(-EINTR).
 /*
struct task_structkthread_create_on_node(int (*threadfn)(voiddata),
					   voiddata, int node,
					   const char namefmt[],
					   ...)
{
	DECLARE_COMPLETION_ONSTACK(done);
	struct task_structtask;
	struct kthread_create_infocreate = kmalloc(sizeof(*create),
						     GFP_KERNEL);

	if (!create)
		return ERR_PTR(-ENOMEM);
	create->threadfn = threadfn;
	create->data = data;
	create->node = node;
	create->done = &done;

	spin_lock(&kthread_create_lock);
	list_add_tail(&create->list, &kthread_create_list);
	spin_unlock(&kthread_create_lock);

	wake_up_process(kthreadd_task);
	*/
	 Wait for completion in killable state, for I might be chosen by
	 the OOM killer while kthreadd is trying to allocate memory for
	 new kernel thread.
	 /*
	if (unlikely(wait_for_completion_killable(&done))) {
		*/
		 If I was SIGKILLed before kthreadd (or new kernel thread)
		 calls complete(), leave the cleanup of this structure to
		 that thread.
		 /*
		if (xchg(&create->done, NULL))
			return ERR_PTR(-EINTR);
		*/
		 kthreadd (or new kernel thread) will call complete()
		 shortly.
		 /*
		wait_for_completion(&done);
	}
	task = create->result;
	if (!IS_ERR(task)) {
		static const struct sched_param param = { .sched_priority = 0 };
		va_list args;

		va_start(args, namefmt);
		vsnprintf(task->comm, sizeof(task->comm), namefmt, args);
		va_end(args);
		*/
		 root may have changed our (kthreadd's) priority or CPU mask.
		 The kernel thread should not inherit these properties.
		 /*
		sched_setscheduler_nocheck(task, SCHED_NORMAL, &param);
		set_cpus_allowed_ptr(task, cpu_all_mask);
	}
	kfree(create);
	return task;
}
EXPORT_SYMBOL(kthread_create_on_node);

static void __kthread_bind_mask(struct task_structp, const struct cpumaskmask, long state)
{
	unsigned long flags;

	if (!wait_task_inactive(p, state)) {
		WARN_ON(1);
		return;
	}

	*/ It's safe because the task is inactive. /*
	raw_spin_lock_irqsave(&p->pi_lock, flags);
	do_set_cpus_allowed(p, mask);
	p->flags |= PF_NO_SETAFFINITY;
	raw_spin_unlock_irqrestore(&p->pi_lock, flags);
}

static void __kthread_bind(struct task_structp, unsigned int cpu, long state)
{
	__kthread_bind_mask(p, cpumask_of(cpu), state);
}

void kthread_bind_mask(struct task_structp, const struct cpumaskmask)
{
	__kthread_bind_mask(p, mask, TASK_UNINTERRUPTIBLE);
}

*/
 kthread_bind - bind a just-created kthread to a cpu.
 @p: thread created by kthread_create().
 @cpu: cpu (might not be online, must be possible) for @k to run on.

 Description: This function is equivalent to set_cpus_allowed(),
 except that @cpu doesn't need to be online, and the thread must be
 stopped (i.e., just returned from kthread_create()).
 /*
void kthread_bind(struct task_structp, unsigned int cpu)
{
	__kthread_bind(p, cpu, TASK_UNINTERRUPTIBLE);
}
EXPORT_SYMBOL(kthread_bind);

*/
 kthread_create_on_cpu - Create a cpu bound kthread
 @threadfn: the function to run until signal_pending(current).
 @data: data ptr for @threadfn.
 @cpu: The cpu on which the thread should be bound,
 @namefmt: printf-style name for the thread. Format is restricted
	     to "name.*%u". Code fills in cpu number.

 Description: This helper function creates and names a kernel thread
 The thread will be woken and put into park mode.
 /*
struct task_structkthread_create_on_cpu(int (*threadfn)(voiddata),
					  voiddata, unsigned int cpu,
					  const charnamefmt)
{
	struct task_structp;

	p = kthread_create_on_node(threadfn, data, cpu_to_node(cpu), namefmt,
				   cpu);
	if (IS_ERR(p))
		return p;
	set_bit(KTHREAD_IS_PER_CPU, &to_kthread(p)->flags);
	to_kthread(p)->cpu = cpu;
	*/ Park the thread to get it out of TASK_UNINTERRUPTIBLE state /*
	kthread_park(p);
	return p;
}

static void __kthread_unpark(struct task_structk, struct kthreadkthread)
{
	clear_bit(KTHREAD_SHOULD_PARK, &kthread->flags);
	*/
	 We clear the IS_PARKED bit here as we don't wait
	 until the task has left the park code. So if we'd
	 park before that happens we'd see the IS_PARKED bit
	 which might be about to be cleared.
	 /*
	if (test_and_clear_bit(KTHREAD_IS_PARKED, &kthread->flags)) {
		if (test_bit(KTHREAD_IS_PER_CPU, &kthread->flags))
			__kthread_bind(k, kthread->cpu, TASK_PARKED);
		wake_up_state(k, TASK_PARKED);
	}
}

*/
 kthread_unpark - unpark a thread created by kthread_create().
 @k:		thread created by kthread_create().

 Sets kthread_should_park() for @k to return false, wakes it, and
 waits for it to return. If the thread is marked percpu then its
 bound to the cpu again.
 /*
void kthread_unpark(struct task_structk)
{
	struct kthreadkthread = to_live_kthread(k);

	if (kthread)
		__kthread_unpark(k, kthread);
}
EXPORT_SYMBOL_GPL(kthread_unpark);

*/
 kthread_park - park a thread created by kthread_create().
 @k: thread created by kthread_create().

 Sets kthread_should_park() for @k to return true, wakes it, and
 waits for it to return. This can also be called after kthread_create()
 instead of calling wake_up_process(): the thread will park without
 calling threadfn().

 Returns 0 if the thread is parked, -ENOSYS if the thread exited.
 If called by the kthread itself just the park bit is set.
 /*
int kthread_park(struct task_structk)
{
	struct kthreadkthread = to_live_kthread(k);
	int ret = -ENOSYS;

	if (kthread) {
		if (!test_bit(KTHREAD_IS_PARKED, &kthread->flags)) {
			set_bit(KTHREAD_SHOULD_PARK, &kthread->flags);
			if (k != current) {
				wake_up_process(k);
				wait_for_completion(&kthread->parked);
			}
		}
		ret = 0;
	}
	return ret;
}
EXPORT_SYMBOL_GPL(kthread_park);

*/
 kthread_stop - stop a thread created by kthread_create().
 @k: thread created by kthread_create().

 Sets kthread_should_stop() for @k to return true, wakes it, and
 waits for it to exit. This can also be called after kthread_create()
 instead of calling wake_up_process(): the thread will exit without
 calling threadfn().

 If threadfn() may call do_exit() itself, the caller must ensure
 task_struct can't go away.

 Returns the result of threadfn(), or %-EINTR if wake_up_process()
 was never called.
 /*
int kthread_stop(struct task_structk)
{
	struct kthreadkthread;
	int ret;

	trace_sched_kthread_stop(k);

	get_task_struct(k);
	kthread = to_live_kthread(k);
	if (kthread) {
		set_bit(KTHREAD_SHOULD_STOP, &kthread->flags);
		__kthread_unpark(k, kthread);
		wake_up_process(k);
		wait_for_completion(&kthread->exited);
	}
	ret = k->exit_code;
	put_task_struct(k);

	trace_sched_kthread_stop_ret(ret);
	return ret;
}
EXPORT_SYMBOL(kthread_stop);

int kthreadd(voidunused)
{
	struct task_structtsk = current;

	*/ Setup a clean context for our children to inherit. /*
	set_task_comm(tsk, "kthreadd");
	ignore_signals(tsk);
	set_cpus_allowed_ptr(tsk, cpu_all_mask);
	set_mems_allowed(node_states[N_MEMORY]);

	current->flags |= PF_NOFREEZE;

	for (;;) {
		set_current_state(TASK_INTERRUPTIBLE);
		if (list_empty(&kthread_create_list))
			schedule();
		__set_current_state(TASK_RUNNING);

		spin_lock(&kthread_create_lock);
		while (!list_empty(&kthread_create_list)) {
			struct kthread_create_infocreate;

			create = list_entry(kthread_create_list.next,
					    struct kthread_create_info, list);
			list_del_init(&create->list);
			spin_unlock(&kthread_create_lock);

			create_kthread(create);

			spin_lock(&kthread_create_lock);
		}
		spin_unlock(&kthread_create_lock);
	}

	return 0;
}

void __init_kthread_worker(struct kthread_workerworker,
				const charname,
				struct lock_class_keykey)
{
	spin_lock_init(&worker->lock);
	lockdep_set_class_and_name(&worker->lock, key, name);
	INIT_LIST_HEAD(&worker->work_list);
	worker->task = NULL;
}
EXPORT_SYMBOL_GPL(__init_kthread_worker);

*/
 kthread_worker_fn - kthread function to process kthread_worker
 @worker_ptr: pointer to initialized kthread_worker

 This function can be used as @threadfn to kthread_create() or
 kthread_run() with @worker_ptr argument pointing to an initialized
 kthread_worker.  The started kthread will process work_list until
 the it is stopped with kthread_stop().  A kthread can also call
 this function directly after extra initialization.

 Different kthreads can be used for the same kthread_worker as long
 as there's only one kthread attached to it at any given time.  A
 kthread_worker without an attached kthread simply collects queued
 kthread_works.
 /*
int kthread_worker_fn(voidworker_ptr)
{
	struct kthread_workerworker = worker_ptr;
	struct kthread_workwork;

	WARN_ON(worker->task);
	worker->task = current;
repeat:
	set_current_state(TASK_INTERRUPTIBLE);	*/ mb paired w/ kthread_stop /*

	if (kthread_should_stop()) {
		__set_current_state(TASK_RUNNING);
		spin_lock_irq(&worker->lock);
		worker->task = NULL;
		spin_unlock_irq(&worker->lock);
		return 0;
	}

	work = NULL;
	spin_lock_irq(&worker->lock);
	if (!list_empty(&worker->work_list)) {
		work = list_first_entry(&worker->work_list,
					struct kthread_work, node);
		list_del_init(&work->node);
	}
	worker->current_work = work;
	spin_unlock_irq(&worker->lock);

	if (work) {
		__set_current_state(TASK_RUNNING);
		work->func(work);
	} else if (!freezing(current))
		schedule();

	try_to_freeze();
	goto repeat;
}
EXPORT_SYMBOL_GPL(kthread_worker_fn);

*/ insert @work before @pos in @worker /*
static void insert_kthread_work(struct kthread_workerworker,
			       struct kthread_workwork,
			       struct list_headpos)
{
	lockdep_assert_held(&worker->lock);

	list_add_tail(&work->node, pos);
	work->worker = worker;
	if (!worker->current_work && likely(worker->task))
		wake_up_process(worker->task);
}

*/
 queue_kthread_work - queue a kthread_work
 @worker: target kthread_worker
 @work: kthread_work to queue

 Queue @work to work processor @task for async execution.  @task
 must have been created with kthread_worker_create().  Returns %true
 if @work was successfully queued, %false if it was already pending.
 /*
bool queue_kthread_work(struct kthread_workerworker,
			struct kthread_workwork)
{
	bool ret = false;
	unsigned long flags;

	spin_lock_irqsave(&worker->lock, flags);
	if (list_empty(&work->node)) {
		insert_kthread_work(worker, work, &worker->work_list);
		ret = true;
	}
	spin_unlock_irqrestore(&worker->lock, flags);
	return ret;
}
EXPORT_SYMBOL_GPL(queue_kthread_work);

struct kthread_flush_work {
	struct kthread_work	work;
	struct completion	done;
};

static void kthread_flush_work_fn(struct kthread_workwork)
{
	struct kthread_flush_workfwork =
		container_of(work, struct kthread_flush_work, work);
	complete(&fwork->done);
}

*/
 flush_kthread_work - flush a kthread_work
 @work: work to flush

 If @work is queued or executing, wait for it to finish execution.
 /*
void flush_kthread_work(struct kthread_workwork)
{
	struct kthread_flush_work fwork = {
		KTHREAD_WORK_INIT(fwork.work, kthread_flush_work_fn),
		COMPLETION_INITIALIZER_ONSTACK(fwork.done),
	};
	struct kthread_workerworker;
	bool noop = false;

retry:
	worker = work->worker;
	if (!worker)
		return;

	spin_lock_irq(&worker->lock);
	if (work->worker != worker) {
		spin_unlock_irq(&worker->lock);
		goto retry;
	}

	if (!list_empty(&work->node))
		insert_kthread_work(worker, &fwork.work, work->node.next);
	else if (worker->current_work == work)
		insert_kthread_work(worker, &fwork.work, worker->work_list.next);
	else
		noop = true;

	spin_unlock_irq(&worker->lock);

	if (!noop)
		wait_for_completion(&fwork.done);
}
EXPORT_SYMBOL_GPL(flush_kthread_work);

*/
 flush_kthread_worker - flush all current works on a kthread_worker
 @worker: worker to flush

 Wait until all currently executing or pending works on @worker are
 finished.
 /*
void flush_kthread_worker(struct kthread_workerworker)
{
	struct kthread_flush_work fwork = {
		KTHREAD_WORK_INIT(fwork.work, kthread_flush_work_fn),
		COMPLETION_INITIALIZER_ONSTACK(fwork.done),
	};

	queue_kthread_work(worker, &fwork.work);
	wait_for_completion(&fwork.done);
}
EXPORT_SYMBOL_GPL(flush_kthread_worker);
*/

 latencytop.c: Latency display infrastructure

 (C) Copyright 2008 Intel Corporation
 Author: Arjan van de Ven <arjan@linux.intel.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*

*/
 CONFIG_LATENCYTOP enables a kernel latency tracking infrastructure that is
 used by the "latencytop" userspace tool. The latency that is tracked is not
 the 'traditional' interrupt latency (which is primarily caused by something
 else consuming CPU), but instead, it is the latency an application encounters
 because the kernel sleeps on its behalf for various reasons.

 This code tracks 2 levels of statistics:
 1) System level latency
 2) Per process latency

 The latency is stored in fixed sized data structures in an accumulated form;
 if the "same" latency cause is hit twice, this will be tracked as one entry
 in the data structure. Both the count, total accumulated latency and maximum
 latency are tracked in this data structure. When the fixed size structure is
 full, no new causes are tracked until the buffer is flushed by writing to
 the /proc file; the userspace tool does this on a regular basis.

 A latency cause is identified by a stringified backtrace at the point that
 the scheduler gets invoked. The userland tool will use this string to
 identify the cause of the latency in human readable form.

 The information is exported via /proc/latency_stats and /proc/<pid>/latency.
 These files look like this:

 Latency Top version : v0.1
 70 59433 4897 i915_irq_wait drm_ioctl vfs_ioctl do_vfs_ioctl sys_ioctl
 |    |    |    |
 |    |    |    +----> the stringified backtrace
 |    |    +---------> The maximum latency for this entry in microseconds
 |    +--------------> The accumulated latency for this entry (microseconds)
 +-------------------> The number of times this entry is hit

 (note: the average latency is the accumulated latency divided by the number
 of times)
 /*

#include <linux/kallsyms.h>
#include <linux/seq_file.h>
#include <linux/notifier.h>
#include <linux/spinlock.h>
#include <linux/proc_fs.h>
#include <linux/latencytop.h>
#include <linux/export.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/stacktrace.h>

static DEFINE_RAW_SPINLOCK(latency_lock);

#define MAXLR 128
static struct latency_record latency_record[MAXLR];

int latencytop_enabled;

void clear_all_latency_tracing(struct task_structp)
{
	unsigned long flags;

	if (!latencytop_enabled)
		return;

	raw_spin_lock_irqsave(&latency_lock, flags);
	memset(&p->latency_record, 0, sizeof(p->latency_record));
	p->latency_record_count = 0;
	raw_spin_unlock_irqrestore(&latency_lock, flags);
}

static void clear_global_latency_tracing(void)
{
	unsigned long flags;

	raw_spin_lock_irqsave(&latency_lock, flags);
	memset(&latency_record, 0, sizeof(latency_record));
	raw_spin_unlock_irqrestore(&latency_lock, flags);
}

static void __sched
account_global_scheduler_latency(struct task_structtsk,
				 struct latency_recordlat)
{
	int firstnonnull = MAXLR + 1;
	int i;

	if (!latencytop_enabled)
		return;

	*/ skip kernel threads for now /*
	if (!tsk->mm)
		return;

	for (i = 0; i < MAXLR; i++) {
		int q, same = 1;

		*/ Nothing stored: /*
		if (!latency_record[i].backtrace[0]) {
			if (firstnonnull > i)
				firstnonnull = i;
			continue;
		}
		for (q = 0; q < LT_BACKTRACEDEPTH; q++) {
			unsigned long record = lat->backtrace[q];

			if (latency_record[i].backtrace[q] != record) {
				same = 0;
				break;
			}

			*/ 0 and ULONG_MAX entries mean end of backtrace: /*
			if (record == 0 || record == ULONG_MAX)
				break;
		}
		if (same) {
			latency_record[i].count++;
			latency_record[i].time += lat->time;
			if (lat->time > latency_record[i].max)
				latency_record[i].max = lat->time;
			return;
		}
	}

	i = firstnonnull;
	if (i >= MAXLR - 1)
		return;

	*/ Allocted a new one: /*
	memcpy(&latency_record[i], lat, sizeof(struct latency_record));
}

*/
 Iterator to store a backtrace into a latency record entry
 /*
static inline void store_stacktrace(struct task_structtsk,
					struct latency_recordlat)
{
	struct stack_trace trace;

	memset(&trace, 0, sizeof(trace));
	trace.max_entries = LT_BACKTRACEDEPTH;
	trace.entries = &lat->backtrace[0];
	save_stack_trace_tsk(tsk, &trace);
}

*/
 __account_scheduler_latency - record an occurred latency
 @tsk - the task struct of the task hitting the latency
 @usecs - the duration of the latency in microseconds
 @inter - 1 if the sleep was interruptible, 0 if uninterruptible

 This function is the main entry point for recording latency entries
 as called by the scheduler.

 This function has a few special cases to deal with normal 'non-latency'
 sleeps: specifically, interruptible sleep longer than 5 msec is skipped
 since this usually is caused by waiting for events via select() and co.

 Negative latencies (caused by time going backwards) are also explicitly
 skipped.
 /*
void __sched
__account_scheduler_latency(struct task_structtsk, int usecs, int inter)
{
	unsigned long flags;
	int i, q;
	struct latency_record lat;

	*/ Long interruptible waits are generally user requested... /*
	if (inter && usecs > 5000)
		return;

	*/ Negative sleeps are time going backwards /*
	*/ Zero-time sleeps are non-interesting /*
	if (usecs <= 0)
		return;

	memset(&lat, 0, sizeof(lat));
	lat.count = 1;
	lat.time = usecs;
	lat.max = usecs;
	store_stacktrace(tsk, &lat);

	raw_spin_lock_irqsave(&latency_lock, flags);

	account_global_scheduler_latency(tsk, &lat);

	for (i = 0; i < tsk->latency_record_count; i++) {
		struct latency_recordmylat;
		int same = 1;

		mylat = &tsk->latency_record[i];
		for (q = 0; q < LT_BACKTRACEDEPTH; q++) {
			unsigned long record = lat.backtrace[q];

			if (mylat->backtrace[q] != record) {
				same = 0;
				break;
			}

			*/ 0 and ULONG_MAX entries mean end of backtrace: /*
			if (record == 0 || record == ULONG_MAX)
				break;
		}
		if (same) {
			mylat->count++;
			mylat->time += lat.time;
			if (lat.time > mylat->max)
				mylat->max = lat.time;
			goto out_unlock;
		}
	}

	*/
	 short term hack; if we're > 32 we stop; future we recycle:
	 /*
	if (tsk->latency_record_count >= LT_SAVECOUNT)
		goto out_unlock;

	*/ Allocated a new one: /*
	i = tsk->latency_record_count++;
	memcpy(&tsk->latency_record[i], &lat, sizeof(struct latency_record));

out_unlock:
	raw_spin_unlock_irqrestore(&latency_lock, flags);
}

static int lstats_show(struct seq_filem, voidv)
{
	int i;

	seq_puts(m, "Latency Top version : v0.1\n");

	for (i = 0; i < MAXLR; i++) {
		struct latency_recordlr = &latency_record[i];

		if (lr->backtrace[0]) {
			int q;
			seq_printf(m, "%i %lu %lu",
				   lr->count, lr->time, lr->max);
			for (q = 0; q < LT_BACKTRACEDEPTH; q++) {
				unsigned long bt = lr->backtrace[q];
				if (!bt)
					break;
				if (bt == ULONG_MAX)
					break;
				seq_printf(m, " %ps", (void)bt);
			}
			seq_puts(m, "\n");
		}
	}
	return 0;
}

static ssize_t
lstats_write(struct filefile, const char __userbuf, size_t count,
	     loff_toffs)
{
	clear_global_latency_tracing();

	return count;
}

static int lstats_open(struct inodeinode, struct filefilp)
{
	return single_open(filp, lstats_show, NULL);
}

static const struct file_operations lstats_fops = {
	.open		= lstats_open,
	.read		= seq_read,
	.write		= lstats_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init init_lstats_procfs(void)
{
	proc_create("latency_stats", 0644, NULL, &lstats_fops);
	return 0;
}

int sysctl_latencytop(struct ctl_tabletable, int write,
			void __userbuffer, size_tlenp, loff_tppos)
{
	int err;

	err = proc_dointvec(table, write, buffer, lenp, ppos);
	if (latencytop_enabled)
		force_schedstat_enabled();

	return err;
}
device_initcall(init_lstats_procfs);
*/

 Makefile for the linux kernel.

# 
# obj-y     = fork.o exec_domain.o panic.o \
# 	    cpu.o exit.o softirq.o resource.o \
# 	    sysctl.o sysctl_binary.o capability.o ptrace.o user.o \
# 	    signal.o sys.o kmod.o workqueue.o pid.o task_work.o \
# 	    extable.o params.o \
# 	    kthread.o sys_ni.o nsproxy.o \
# 	    notifier.o ksysfs.o cred.o reboot.o \
# 	    async.o range.o smpboot.o
# 
# obj-$(CONFIG_MULTIUSER) += groups.o
# 
# ifdef CONFIG_FUNCTION_TRACER
 Do not trace internal ftrace files
# CFLAGS_REMOVE_irq_work.o = $(CC_FLAGS_FTRACE)
# endif
# 
 Prevents flicker of uninteresting __do_softirq()/__local_bh_disable_ip()
 in coverage traces.
# KCOV_INSTRUMENT_softirq.o := n
 These are called from save_stack_trace() on slub debug path,
 and produce insane amounts of uninteresting coverage.
# KCOV_INSTRUMENT_module.o := n
# KCOV_INSTRUMENT_extable.o := n
 Don't self-instrument.
# KCOV_INSTRUMENT_kcov.o := n
# KASAN_SANITIZE_kcov.o := n
# 
 cond_syscall is currently not LTO compatible
# CFLAGS_sys_ni.o = $(DISABLE_LTO)
# 
# obj-y += sched/
# obj-y += locking/
# obj-y += power/
# obj-y += printk/
# obj-y += irq/
# obj-y += rcu/
# obj-y += livepatch/
# 
# obj-$(CONFIG_CHECKPOINT_RESTORE) += kcmp.o
# obj-$(CONFIG_FREEZER) += freezer.o
# obj-$(CONFIG_PROFILING) += profile.o
# obj-$(CONFIG_STACKTRACE) += stacktrace.o
# obj-y += time/
# obj-$(CONFIG_FUTEX) += futex.o
# ifeq ($(CONFIG_COMPAT),y)
# obj-$(CONFIG_FUTEX) += futex_compat.o
# endif
# obj-$(CONFIG_GENERIC_ISA_DMA) += dma.o
# obj-$(CONFIG_SMP) += smp.o
# ifneq ($(CONFIG_SMP),y)
# obj-y += up.o
# endif
# obj-$(CONFIG_UID16) += uid16.o
# obj-$(CONFIG_MODULES) += module.o
# obj-$(CONFIG_MODULE_SIG) += module_signing.o
# obj-$(CONFIG_KALLSYMS) += kallsyms.o
# obj-$(CONFIG_BSD_PROCESS_ACCT) += acct.o
# obj-$(CONFIG_KEXEC_CORE) += kexec_core.o
# obj-$(CONFIG_KEXEC) += kexec.o
# obj-$(CONFIG_KEXEC_FILE) += kexec_file.o
# obj-$(CONFIG_BACKTRACE_SELF_TEST) += backtracetest.o
# obj-$(CONFIG_COMPAT) += compat.o
# obj-$(CONFIG_CGROUPS) += cgroup.o
# obj-$(CONFIG_CGROUP_FREEZER) += cgroup_freezer.o
# obj-$(CONFIG_CGROUP_PIDS) += cgroup_pids.o
# obj-$(CONFIG_CPUSETS) += cpuset.o
# obj-$(CONFIG_UTS_NS) += utsname.o
# obj-$(CONFIG_USER_NS) += user_namespace.o
# obj-$(CONFIG_PID_NS) += pid_namespace.o
# obj-$(CONFIG_IKCONFIG) += configs.o
# obj-$(CONFIG_SMP) += stop_machine.o
# obj-$(CONFIG_KPROBES_SANITY_TEST) += test_kprobes.o
# obj-$(CONFIG_AUDIT) += audit.o auditfilter.o
# obj-$(CONFIG_AUDITSYSCALL) += auditsc.o
# obj-$(CONFIG_AUDIT_WATCH) += audit_watch.o audit_fsnotify.o
# obj-$(CONFIG_AUDIT_TREE) += audit_tree.o
# obj-$(CONFIG_GCOV_KERNEL) += gcov/
# obj-$(CONFIG_KCOV) += kcov.o
# obj-$(CONFIG_KPROBES) += kprobes.o
# obj-$(CONFIG_KGDB) += debug/
# obj-$(CONFIG_DETECT_HUNG_TASK) += hung_task.o
# obj-$(CONFIG_LOCKUP_DETECTOR) += watchdog.o
# obj-$(CONFIG_SECCOMP) += seccomp.o
# obj-$(CONFIG_RELAY) += relay.o
# obj-$(CONFIG_SYSCTL) += utsname_sysctl.o
# obj-$(CONFIG_TASK_DELAY_ACCT) += delayacct.o
# obj-$(CONFIG_TASKSTATS) += taskstats.o tsacct.o
# obj-$(CONFIG_TRACEPOINTS) += tracepoint.o
# obj-$(CONFIG_LATENCYTOP) += latencytop.o
# obj-$(CONFIG_BINFMT_ELF) += elfcore.o
# obj-$(CONFIG_COMPAT_BINFMT_ELF) += elfcore.o
# obj-$(CONFIG_BINFMT_ELF_FDPIC) += elfcore.o
# obj-$(CONFIG_FUNCTION_TRACER) += trace/
# obj-$(CONFIG_TRACING) += trace/
# obj-$(CONFIG_TRACE_CLOCK) += trace/
# obj-$(CONFIG_RING_BUFFER) += trace/
# obj-$(CONFIG_TRACEPOINTS) += trace/
# obj-$(CONFIG_IRQ_WORK) += irq_work.o
# obj-$(CONFIG_CPU_PM) += cpu_pm.o
# obj-$(CONFIG_BPF) += bpf/
# 
# obj-$(CONFIG_PERF_EVENTS) += events/
# 
# obj-$(CONFIG_USER_RETURN_NOTIFIER) += user-return-notifier.o
# obj-$(CONFIG_PADATA) += padata.o
# obj-$(CONFIG_CRASH_DUMP) += crash_dump.o
# obj-$(CONFIG_JUMP_LABEL) += jump_label.o
# obj-$(CONFIG_CONTEXT_TRACKING) += context_tracking.o
# obj-$(CONFIG_TORTURE_TEST) += torture.o
# obj-$(CONFIG_MEMBARRIER) += membarrier.o
# 
# obj-$(CONFIG_HAS_IOMEM) += memremap.o
# 
# $(obj)/configs.o: $(obj)/config_data.h
# 
 config_data.h contains the same information as ikconfig.h but gzipped.
 Info from config_data can be extracted from /proc/config*
# targets += config_data.gz
# $(obj)/config_data.gz: $(KCONFIG_CONFIG) FORCE
# 	$(call if_changed,gzip)
# 
#       filechk_ikconfiggz = (echo "static const char kernel_config_data[] __used = MAGIC_START"; cat $< | scripts/basic/bin2c; echo "MAGIC_END;")
# targets += config_data.h
# $(obj)/config_data.h: $(obj)/config_data.gz FORCE
# 	$(call filechk,ikconfiggz)
