
  linux/kernel/acct.c

  BSD Process Accounting for Linux

  Author: Marco van Wieringen &#x3C;mvw@planets.elm.net&#x3E;

  Some code based on ideas and code from:
  Thomas K. Dyas &#x3C;tdyas@eden.rutgers.edu&#x3E;

  This file implements BSD-style process accounting. Whenever any
  process exits, an accounting record of type &#x22;struct acct&#x22; is
  written to the file specified with the acct() system call. It is
  up to user-level programs to do useful things with the accounting
  log. The kernel just provides the raw accounting information.

 (C) Copyright 1995 - 1997 Marco van Wieringen - ELM Consultancy B.V.

  Plugged two leaks. 1) It didn&#x27;t return acct_file into the free_filps if
  the file happened to be read-only. 2) If the accounting was suspended
  due to the lack of space it happily allowed to reopen it and completely
  lost the old acct_file. 3/10/98, Al Viro.

  Now we silently close acct_file on attempt to reopen. Cleaned sys_acct().
  XTerms and EMACS are manifestations of pure evil. 21/10/98, AV.

  Fixed a nasty interaction with with sys_umount(). If the accointing
  was suspeneded we failed to stop it on umount(). Messy.
  Another one: remount to readonly didn&#x27;t stop accounting.
&#x9;Question: what should we do if we have CAP_SYS_ADMIN but not
  CAP_SYS_PACCT? Current code does the following: umount returns -EBUSY
  unless we are messing with the root. In that case we are getting a
  real mess with do_remount_sb(). 9/11/98, AV.

  Fixed a bunch of races (and pair of leaks). Probably not the best way,
  but this one obviously doesn&#x27;t introduce deadlocks. Later. BTW, found
  one race (and leak) in BSD implementation.
  OK, that&#x27;s better. ANOTHER race and leak in BSD variant. There always
  is one more bug... 10/11/98, AV.

&#x9;Oh, fsck... Oopsable SMP race in do_process_acct() - we must hold
 -&#x3E;mmap_sem to walk the vma list of current-&#x3E;mm. Nasty, since it leaks
 a struct file opened for write. Fixed. 2/6/2000, AV.
 /*

#include &#x3C;linux/mm.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/acct.h&#x3E;
#include &#x3C;linux/capability.h&#x3E;
#include &#x3C;linux/file.h&#x3E;
#include &#x3C;linux/tty.h&#x3E;
#include &#x3C;linux/security.h&#x3E;
#include &#x3C;linux/vfs.h&#x3E;
#include &#x3C;linux/jiffies.h&#x3E;
#include &#x3C;linux/times.h&#x3E;
#include &#x3C;linux/syscalls.h&#x3E;
#include &#x3C;linux/mount.h&#x3E;
#include &#x3C;linux/uaccess.h&#x3E;
#include &#x3C;asm/div64.h&#x3E;
#include &#x3C;linux/blkdev.h&#x3E;/ sector_div /*
#include &#x3C;linux/pid_namespace.h&#x3E;
#include &#x3C;linux/fs_pin.h&#x3E;

*/
 These constants control the amount of freespace that suspend and
 resume the process accounting system, and the time delay between
 each check.
 Turned into sysctl-controllable parameters. AV, 12/11/98
 /*

int acct_parm[3] = {4, 2, 30};
#define RESUME&#x9;&#x9;(acct_parm[0])&#x9;*/ &#x3E;foo% free space - resume /*
#define SUSPEND&#x9;&#x9;(acct_parm[1])&#x9;*/ &#x3C;foo% free space - suspend /*
#define ACCT_TIMEOUT&#x9;(acct_parm[2])&#x9;*/ foo second timeout between checks /*

*/
 External references and all of the globals.
 /*

struct bsd_acct_struct {
&#x9;struct fs_pin&#x9;&#x9;pin;
&#x9;atomic_long_t&#x9;&#x9;count;
&#x9;struct rcu_head&#x9;&#x9;rcu;
&#x9;struct mutex&#x9;&#x9;lock;
&#x9;int&#x9;&#x9;&#x9;active;
&#x9;unsigned long&#x9;&#x9;needcheck;
&#x9;struct file&#x9;&#x9;*file;
&#x9;struct pid_namespace&#x9;*ns;
&#x9;struct work_struct&#x9;work;
&#x9;struct completion&#x9;done;
};

static void do_acct_process(struct bsd_acct_structacct);

*/
 Check the amount of free space and suspend/resume accordingly.
 /*
static int check_free_space(struct bsd_acct_structacct)
{
&#x9;struct kstatfs sbuf;

&#x9;if (time_is_before_jiffies(acct-&#x3E;needcheck))
&#x9;&#x9;goto out;

&#x9;*/ May block /*
&#x9;if (vfs_statfs(&#x26;acct-&#x3E;file-&#x3E;f_path, &#x26;sbuf))
&#x9;&#x9;goto out;

&#x9;if (acct-&#x3E;active) {
&#x9;&#x9;u64 suspend = sbuf.f_blocks SUSPEND;
&#x9;&#x9;do_div(suspend, 100);
&#x9;&#x9;if (sbuf.f_bavail &#x3C;= suspend) {
&#x9;&#x9;&#x9;acct-&#x3E;active = 0;
&#x9;&#x9;&#x9;pr_info(&#x22;Process accounting paused\n&#x22;);
&#x9;&#x9;}
&#x9;} else {
&#x9;&#x9;u64 resume = sbuf.f_blocks RESUME;
&#x9;&#x9;do_div(resume, 100);
&#x9;&#x9;if (sbuf.f_bavail &#x3E;= resume) {
&#x9;&#x9;&#x9;acct-&#x3E;active = 1;
&#x9;&#x9;&#x9;pr_info(&#x22;Process accounting resumed\n&#x22;);
&#x9;&#x9;}
&#x9;}

&#x9;acct-&#x3E;needcheck = jiffies + ACCT_TIMEOUT*HZ;
out:
&#x9;return acct-&#x3E;active;
}

static void acct_put(struct bsd_acct_structp)
{
&#x9;if (atomic_long_dec_and_test(&#x26;p-&#x3E;count))
&#x9;&#x9;kfree_rcu(p, rcu);
}

static inline struct bsd_acct_structto_acct(struct fs_pinp)
{
&#x9;return p ? container_of(p, struct bsd_acct_struct, pin) : NULL;
}

static struct bsd_acct_structacct_get(struct pid_namespacens)
{
&#x9;struct bsd_acct_structres;
again:
&#x9;smp_rmb();
&#x9;rcu_read_lock();
&#x9;res = to_acct(ACCESS_ONCE(ns-&#x3E;bacct));
&#x9;if (!res) {
&#x9;&#x9;rcu_read_unlock();
&#x9;&#x9;return NULL;
&#x9;}
&#x9;if (!atomic_long_inc_not_zero(&#x26;res-&#x3E;count)) {
&#x9;&#x9;rcu_read_unlock();
&#x9;&#x9;cpu_relax();
&#x9;&#x9;goto again;
&#x9;}
&#x9;rcu_read_unlock();
&#x9;mutex_lock(&#x26;res-&#x3E;lock);
&#x9;if (res != to_acct(ACCESS_ONCE(ns-&#x3E;bacct))) {
&#x9;&#x9;mutex_unlock(&#x26;res-&#x3E;lock);
&#x9;&#x9;acct_put(res);
&#x9;&#x9;goto again;
&#x9;}
&#x9;return res;
}

static void acct_pin_kill(struct fs_pinpin)
{
&#x9;struct bsd_acct_structacct = to_acct(pin);
&#x9;mutex_lock(&#x26;acct-&#x3E;lock);
&#x9;do_acct_process(acct);
&#x9;schedule_work(&#x26;acct-&#x3E;work);
&#x9;wait_for_completion(&#x26;acct-&#x3E;done);
&#x9;cmpxchg(&#x26;acct-&#x3E;ns-&#x3E;bacct, pin, NULL);
&#x9;mutex_unlock(&#x26;acct-&#x3E;lock);
&#x9;pin_remove(pin);
&#x9;acct_put(acct);
}

static void close_work(struct work_structwork)
{
&#x9;struct bsd_acct_structacct = container_of(work, struct bsd_acct_struct, work);
&#x9;struct filefile = acct-&#x3E;file;
&#x9;if (file-&#x3E;f_op-&#x3E;flush)
&#x9;&#x9;file-&#x3E;f_op-&#x3E;flush(file, NULL);
&#x9;__fput_sync(file);
&#x9;complete(&#x26;acct-&#x3E;done);
}

static int acct_on(struct filenamepathname)
{
&#x9;struct filefile;
&#x9;struct vfsmountmnt,internal;
&#x9;struct pid_namespacens = task_active_pid_ns(current);
&#x9;struct bsd_acct_structacct;
&#x9;struct fs_pinold;
&#x9;int err;

&#x9;acct = kzalloc(sizeof(struct bsd_acct_struct), GFP_KERNEL);
&#x9;if (!acct)
&#x9;&#x9;return -ENOMEM;

&#x9;*/ Difference from BSD - they don&#x27;t do O_APPEND /*
&#x9;file = file_open_name(pathname, O_WRONLY|O_APPEND|O_LARGEFILE, 0);
&#x9;if (IS_ERR(file)) {
&#x9;&#x9;kfree(acct);
&#x9;&#x9;return PTR_ERR(file);
&#x9;}

&#x9;if (!S_ISREG(file_inode(file)-&#x3E;i_mode)) {
&#x9;&#x9;kfree(acct);
&#x9;&#x9;filp_close(file, NULL);
&#x9;&#x9;return -EACCES;
&#x9;}

&#x9;if (!(file-&#x3E;f_mode &#x26; FMODE_CAN_WRITE)) {
&#x9;&#x9;kfree(acct);
&#x9;&#x9;filp_close(file, NULL);
&#x9;&#x9;return -EIO;
&#x9;}
&#x9;internal = mnt_clone_internal(&#x26;file-&#x3E;f_path);
&#x9;if (IS_ERR(internal)) {
&#x9;&#x9;kfree(acct);
&#x9;&#x9;filp_close(file, NULL);
&#x9;&#x9;return PTR_ERR(internal);
&#x9;}
&#x9;err = mnt_want_write(internal);
&#x9;if (err) {
&#x9;&#x9;mntput(internal);
&#x9;&#x9;kfree(acct);
&#x9;&#x9;filp_close(file, NULL);
&#x9;&#x9;return err;
&#x9;}
&#x9;mnt = file-&#x3E;f_path.mnt;
&#x9;file-&#x3E;f_path.mnt = internal;

&#x9;atomic_long_set(&#x26;acct-&#x3E;count, 1);
&#x9;init_fs_pin(&#x26;acct-&#x3E;pin, acct_pin_kill);
&#x9;acct-&#x3E;file = file;
&#x9;acct-&#x3E;needcheck = jiffies;
&#x9;acct-&#x3E;ns = ns;
&#x9;mutex_init(&#x26;acct-&#x3E;lock);
&#x9;INIT_WORK(&#x26;acct-&#x3E;work, close_work);
&#x9;init_completion(&#x26;acct-&#x3E;done);
&#x9;mutex_lock_nested(&#x26;acct-&#x3E;lock, 1);&#x9;*/ nobody has seen it yet /*
&#x9;pin_insert(&#x26;acct-&#x3E;pin, mnt);

&#x9;rcu_read_lock();
&#x9;old = xchg(&#x26;ns-&#x3E;bacct, &#x26;acct-&#x3E;pin);
&#x9;mutex_unlock(&#x26;acct-&#x3E;lock);
&#x9;pin_kill(old);
&#x9;mnt_drop_write(mnt);
&#x9;mntput(mnt);
&#x9;return 0;
}

static DEFINE_MUTEX(acct_on_mutex);

*/
 sys_acct - enable/disable process accounting
 @name: file name for accounting records or NULL to shutdown accounting

 Returns 0 for success or negative errno values for failure.

 sys_acct() is the only system call needed to implement process
 accounting. It takes the name of the file where accounting records
 should be written. If the filename is NULL, accounting will be
 shutdown.
 /*
SYSCALL_DEFINE1(acct, const char __user, name)
{
&#x9;int error = 0;

&#x9;if (!capable(CAP_SYS_PACCT))
&#x9;&#x9;return -EPERM;

&#x9;if (name) {
&#x9;&#x9;struct filenametmp = getname(name);

&#x9;&#x9;if (IS_ERR(tmp))
&#x9;&#x9;&#x9;return PTR_ERR(tmp);
&#x9;&#x9;mutex_lock(&#x26;acct_on_mutex);
&#x9;&#x9;error = acct_on(tmp);
&#x9;&#x9;mutex_unlock(&#x26;acct_on_mutex);
&#x9;&#x9;putname(tmp);
&#x9;} else {
&#x9;&#x9;rcu_read_lock();
&#x9;&#x9;pin_kill(task_active_pid_ns(current)-&#x3E;bacct);
&#x9;}

&#x9;return error;
}

void acct_exit_ns(struct pid_namespacens)
{
&#x9;rcu_read_lock();
&#x9;pin_kill(ns-&#x3E;bacct);
}

*/
  encode an unsigned long into a comp_t

  This routine has been adopted from the encode_comp_t() function in
  the kern_acct.c file of the FreeBSD operating system. The encoding
  is a 13-bit fraction with a 3-bit (base 8) exponent.
 /*

#define&#x9;MANTSIZE&#x9;13&#x9;&#x9;&#x9;*/ 13 bit mantissa. /*
#define&#x9;EXPSIZE&#x9;&#x9;3&#x9;&#x9;&#x9;*/ Base 8 (3 bit) exponent. /*
#define&#x9;MAXFRACT&#x9;((1 &#x3C;&#x3C; MANTSIZE) - 1)&#x9;*/ Maximum fractional value. /*

static comp_t encode_comp_t(unsigned long value)
{
&#x9;int exp, rnd;

&#x9;exp = rnd = 0;
&#x9;while (value &#x3E; MAXFRACT) {
&#x9;&#x9;rnd = value &#x26; (1 &#x3C;&#x3C; (EXPSIZE - 1));&#x9;*/ Round up? /*
&#x9;&#x9;value &#x3E;&#x3E;= EXPSIZE;&#x9;*/ Base 8 exponent == 3 bit shift. /*
&#x9;&#x9;exp++;
&#x9;}

&#x9;*/
&#x9; If we need to round up, do it (and handle overflow correctly).
&#x9; /*
&#x9;if (rnd &#x26;&#x26; (++value &#x3E; MAXFRACT)) {
&#x9;&#x9;value &#x3E;&#x3E;= EXPSIZE;
&#x9;&#x9;exp++;
&#x9;}

&#x9;*/
&#x9; Clean it up and polish it off.
&#x9; /*
&#x9;exp &#x3C;&#x3C;= MANTSIZE;&#x9;&#x9;*/ Shift the exponent into place /*
&#x9;exp += value;&#x9;&#x9;&#x9;*/ and add on the mantissa. /*
&#x9;return exp;
}

#if ACCT_VERSION == 1 || ACCT_VERSION == 2
*/
 encode an u64 into a comp2_t (24 bits)

 Format: 5 bit base 2 exponent, 20 bits mantissa.
 The leading bit of the mantissa is not stored, but implied for
 non-zero exponents.
 Largest encodable value is 50 bits.
 /*

#define MANTSIZE2       20                     / 20 bit mantissa. /*
#define EXPSIZE2        5                      / 5 bit base 2 exponent. /*
#define MAXFRACT2       ((1ul &#x3C;&#x3C; MANTSIZE2) - 1)/ Maximum fractional value. /*
#define MAXEXP2         ((1 &#x3C;&#x3C; EXPSIZE2) - 1)   / Maximum exponent. /*

static comp2_t encode_comp2_t(u64 value)
{
&#x9;int exp, rnd;

&#x9;exp = (value &#x3E; (MAXFRACT2&#x3E;&#x3E;1));
&#x9;rnd = 0;
&#x9;while (value &#x3E; MAXFRACT2) {
&#x9;&#x9;rnd = value &#x26; 1;
&#x9;&#x9;value &#x3E;&#x3E;= 1;
&#x9;&#x9;exp++;
&#x9;}

&#x9;*/
&#x9; If we need to round up, do it (and handle overflow correctly).
&#x9; /*
&#x9;if (rnd &#x26;&#x26; (++value &#x3E; MAXFRACT2)) {
&#x9;&#x9;value &#x3E;&#x3E;= 1;
&#x9;&#x9;exp++;
&#x9;}

&#x9;if (exp &#x3E; MAXEXP2) {
&#x9;&#x9;*/ Overflow. Return largest representable number instead. /*
&#x9;&#x9;return (1ul &#x3C;&#x3C; (MANTSIZE2+EXPSIZE2-1)) - 1;
&#x9;} else {
&#x9;&#x9;return (value &#x26; (MAXFRACT2&#x3E;&#x3E;1)) | (exp &#x3C;&#x3C; (MANTSIZE2-1));
&#x9;}
}
#endif

#if ACCT_VERSION == 3
*/
 encode an u64 into a 32 bit IEEE float
 /*
static u32 encode_float(u64 value)
{
&#x9;unsigned exp = 190;
&#x9;unsigned u;

&#x9;if (value == 0)
&#x9;&#x9;return 0;
&#x9;while ((s64)value &#x3E; 0) {
&#x9;&#x9;value &#x3C;&#x3C;= 1;
&#x9;&#x9;exp--;
&#x9;}
&#x9;u = (u32)(value &#x3E;&#x3E; 40) &#x26; 0x7fffffu;
&#x9;return u | (exp &#x3C;&#x3C; 23);
}
#endif

*/
  Write an accounting entry for an exiting process

  The acct_process() call is the workhorse of the process
  accounting system. The struct acct is built here and then written
  into the accounting file. This function should only be called from
  do_exit() or when switching to a different output file.
 /*

static void fill_ac(acct_tac)
{
&#x9;struct pacct_structpacct = &#x26;current-&#x3E;signal-&#x3E;pacct;
&#x9;u64 elapsed, run_time;
&#x9;struct tty_structtty;

&#x9;*/
&#x9; Fill the accounting struct with the needed info as recorded
&#x9; by the different kernel functions.
&#x9; /*
&#x9;memset(ac, 0, sizeof(acct_t));

&#x9;ac-&#x3E;ac_version = ACCT_VERSION | ACCT_BYTEORDER;
&#x9;strlcpy(ac-&#x3E;ac_comm, current-&#x3E;comm, sizeof(ac-&#x3E;ac_comm));

&#x9;*/ calculate run_time in nsec/*
&#x9;run_time = ktime_get_ns();
&#x9;run_time -= current-&#x3E;group_leader-&#x3E;start_time;
&#x9;*/ convert nsec -&#x3E; AHZ /*
&#x9;elapsed = nsec_to_AHZ(run_time);
#if ACCT_VERSION == 3
&#x9;ac-&#x3E;ac_etime = encode_float(elapsed);
#else
&#x9;ac-&#x3E;ac_etime = encode_comp_t(elapsed &#x3C; (unsigned long) -1l ?
&#x9;&#x9;&#x9;&#x9;(unsigned long) elapsed : (unsigned long) -1l);
#endif
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
&#x9;{
&#x9;&#x9;*/ new enlarged etime field /*
&#x9;&#x9;comp2_t etime = encode_comp2_t(elapsed);

&#x9;&#x9;ac-&#x3E;ac_etime_hi = etime &#x3E;&#x3E; 16;
&#x9;&#x9;ac-&#x3E;ac_etime_lo = (u16) etime;
&#x9;}
#endif
&#x9;do_div(elapsed, AHZ);
&#x9;ac-&#x3E;ac_btime = get_seconds() - elapsed;
#if ACCT_VERSION==2
&#x9;ac-&#x3E;ac_ahz = AHZ;
#endif

&#x9;spin_lock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
&#x9;tty = current-&#x3E;signal-&#x3E;tty;&#x9;*/ Safe as we hold the siglock /*
&#x9;ac-&#x3E;ac_tty = tty ? old_encode_dev(tty_devnum(tty)) : 0;
&#x9;ac-&#x3E;ac_utime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&#x3E;ac_utime)));
&#x9;ac-&#x3E;ac_stime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&#x3E;ac_stime)));
&#x9;ac-&#x3E;ac_flag = pacct-&#x3E;ac_flag;
&#x9;ac-&#x3E;ac_mem = encode_comp_t(pacct-&#x3E;ac_mem);
&#x9;ac-&#x3E;ac_minflt = encode_comp_t(pacct-&#x3E;ac_minflt);
&#x9;ac-&#x3E;ac_majflt = encode_comp_t(pacct-&#x3E;ac_majflt);
&#x9;ac-&#x3E;ac_exitcode = pacct-&#x3E;ac_exitcode;
&#x9;spin_unlock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
}
*/
  do_acct_process does all actual work. Caller holds the reference to file.
 /*
static void do_acct_process(struct bsd_acct_structacct)
{
&#x9;acct_t ac;
&#x9;unsigned long flim;
&#x9;const struct credorig_cred;
&#x9;struct filefile = acct-&#x3E;file;

&#x9;*/
&#x9; Accounting records are not subject to resource limits.
&#x9; /*
&#x9;flim = current-&#x3E;signal-&#x3E;rlim[RLIMIT_FSIZE].rlim_cur;
&#x9;current-&#x3E;signal-&#x3E;rlim[RLIMIT_FSIZE].rlim_cur = RLIM_INFINITY;
&#x9;*/ Perform file operations on behalf of whoever enabled accounting /*
&#x9;orig_cred = override_creds(file-&#x3E;f_cred);

&#x9;*/
&#x9; First check to see if there is enough free_space to continue
&#x9; the process accounting system.
&#x9; /*
&#x9;if (!check_free_space(acct))
&#x9;&#x9;goto out;

&#x9;fill_ac(&#x26;ac);
&#x9;*/ we really need to bite the bullet and change layout /*
&#x9;ac.ac_uid = from_kuid_munged(file-&#x3E;f_cred-&#x3E;user_ns, orig_cred-&#x3E;uid);
&#x9;ac.ac_gid = from_kgid_munged(file-&#x3E;f_cred-&#x3E;user_ns, orig_cred-&#x3E;gid);
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
&#x9;*/ backward-compatible 16 bit fields /*
&#x9;ac.ac_uid16 = ac.ac_uid;
&#x9;ac.ac_gid16 = ac.ac_gid;
#endif
#if ACCT_VERSION == 3
&#x9;{
&#x9;&#x9;struct pid_namespacens = acct-&#x3E;ns;

&#x9;&#x9;ac.ac_pid = task_tgid_nr_ns(current, ns);
&#x9;&#x9;rcu_read_lock();
&#x9;&#x9;ac.ac_ppid = task_tgid_nr_ns(rcu_dereference(current-&#x3E;real_parent),
&#x9;&#x9;&#x9;&#x9;&#x9;     ns);
&#x9;&#x9;rcu_read_unlock();
&#x9;}
#endif
&#x9;*/
&#x9; Get freeze protection. If the fs is frozen, just skip the write
&#x9; as we could deadlock the system otherwise.
&#x9; /*
&#x9;if (file_start_write_trylock(file)) {
&#x9;&#x9;*/ it&#x27;s been opened O_APPEND, so position is irrelevant /*
&#x9;&#x9;loff_t pos = 0;
&#x9;&#x9;__kernel_write(file, (char)&#x26;ac, sizeof(acct_t), &#x26;pos);
&#x9;&#x9;file_end_write(file);
&#x9;}
out:
&#x9;current-&#x3E;signal-&#x3E;rlim[RLIMIT_FSIZE].rlim_cur = flim;
&#x9;revert_creds(orig_cred);
}

*/
 acct_collect - collect accounting information into pacct_struct
 @exitcode: task exit code
 @group_dead: not 0, if this thread is the last one in the process.
 /*
void acct_collect(long exitcode, int group_dead)
{
&#x9;struct pacct_structpacct = &#x26;current-&#x3E;signal-&#x3E;pacct;
&#x9;cputime_t utime, stime;
&#x9;unsigned long vsize = 0;

&#x9;if (group_dead &#x26;&#x26; current-&#x3E;mm) {
&#x9;&#x9;struct vm_area_structvma;

&#x9;&#x9;down_read(&#x26;current-&#x3E;mm-&#x3E;mmap_sem);
&#x9;&#x9;vma = current-&#x3E;mm-&#x3E;mmap;
&#x9;&#x9;while (vma) {
&#x9;&#x9;&#x9;vsize += vma-&#x3E;vm_end - vma-&#x3E;vm_start;
&#x9;&#x9;&#x9;vma = vma-&#x3E;vm_next;
&#x9;&#x9;}
&#x9;&#x9;up_read(&#x26;current-&#x3E;mm-&#x3E;mmap_sem);
&#x9;}

&#x9;spin_lock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
&#x9;if (group_dead)
&#x9;&#x9;pacct-&#x3E;ac_mem = vsize / 1024;
&#x9;if (thread_group_leader(current)) {
&#x9;&#x9;pacct-&#x3E;ac_exitcode = exitcode;
&#x9;&#x9;if (current-&#x3E;flags &#x26; PF_FORKNOEXEC)
&#x9;&#x9;&#x9;pacct-&#x3E;ac_flag |= AFORK;
&#x9;}
&#x9;if (current-&#x3E;flags &#x26; PF_SUPERPRIV)
&#x9;&#x9;pacct-&#x3E;ac_flag |= ASU;
&#x9;if (current-&#x3E;flags &#x26; PF_DUMPCORE)
&#x9;&#x9;pacct-&#x3E;ac_flag |= ACORE;
&#x9;if (current-&#x3E;flags &#x26; PF_SIGNALED)
&#x9;&#x9;pacct-&#x3E;ac_flag |= AXSIG;
&#x9;task_cputime(current, &#x26;utime, &#x26;stime);
&#x9;pacct-&#x3E;ac_utime += utime;
&#x9;pacct-&#x3E;ac_stime += stime;
&#x9;pacct-&#x3E;ac_minflt += current-&#x3E;min_flt;
&#x9;pacct-&#x3E;ac_majflt += current-&#x3E;maj_flt;
&#x9;spin_unlock_irq(&#x26;current-&#x3E;sighand-&#x3E;siglock);
}

static void slow_acct_process(struct pid_namespacens)
{
&#x9;for ( ; ns; ns = ns-&#x3E;parent) {
&#x9;&#x9;struct bsd_acct_structacct = acct_get(ns);
&#x9;&#x9;if (acct) {
&#x9;&#x9;&#x9;do_acct_process(acct);
&#x9;&#x9;&#x9;mutex_unlock(&#x26;acct-&#x3E;lock);
&#x9;&#x9;&#x9;acct_put(acct);
&#x9;&#x9;}
&#x9;}
}

*/
 acct_process

 handles process accounting for an exiting task
 /*
void acct_process(void)
{
&#x9;struct pid_namespacens;

&#x9;*/
&#x9; This loop is safe lockless, since current is still
&#x9; alive and holds its namespace, which in turn holds
&#x9; its parent.
&#x9; /*
&#x9;for (ns = task_active_pid_ns(current); ns != NULL; ns = ns-&#x3E;parent) {
&#x9;&#x9;if (ns-&#x3E;bacct)
&#x9;&#x9;&#x9;break;
&#x9;}
&#x9;if (unlikely(ns))
&#x9;&#x9;slow_acct_process(ns);
}
*/

 async.c: Asynchronous function calls for boot performance

 (C) Copyright 2009 Intel Corporation
 Author: Arjan van de Ven &#x3C;arjan@linux.intel.com&#x3E;

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*


*/

Goals and Theory of Operation

The primary goal of this feature is to reduce the kernel boot time,
by doing various independent hardware delays and discovery operations
decoupled and not strictly serialized.

More specifically, the asynchronous function call concept allows
certain operations (primarily during system boot) to happen
asynchronously, out of order, while these operations still
have their externally visible parts happen sequentially and in-order.
(not unlike how out-of-order CPUs retire their instructions in order)

Key to the asynchronous function call implementation is the concept of
a &#x22;sequence cookie&#x22; (which, although it has an abstracted type, can be
thought of as a monotonically incrementing number).

The async core will assign each scheduled event such a sequence cookie and
pass this to the called functions.

The asynchronously called function should before doing a globally visible
operation, such as registering device numbers, call the
async_synchronize_cookie() function and pass in its own cookie. The
async_synchronize_cookie() function will make sure that all asynchronous
operations that were scheduled prior to the operation corresponding with the
cookie have completed.

Subsystem/driver initialization code that scheduled asynchronous probe
functions, but which shares global resources with other drivers/subsystems
that do not use the asynchronous call feature, need to do a full
synchronization with the async_synchronize_full() function, before returning
from their init function. This is to maintain strict ordering between the
asynchronous and synchronous parts of the kernel.

/*

#include &#x3C;linux/async.h&#x3E;
#include &#x3C;linux/atomic.h&#x3E;
#include &#x3C;linux/ktime.h&#x3E;
#include &#x3C;linux/export.h&#x3E;
#include &#x3C;linux/wait.h&#x3E;
#include &#x3C;linux/sched.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/workqueue.h&#x3E;

#include &#x22;workqueue_internal.h&#x22;

static async_cookie_t next_cookie = 1;

#define MAX_WORK&#x9;&#x9;32768
#define ASYNC_COOKIE_MAX&#x9;ULLONG_MAX&#x9;*/ infinity cookie /*

static LIST_HEAD(async_global_pending);&#x9;*/ pending from all registered doms /*
static ASYNC_DOMAIN(async_dfl_domain);
static DEFINE_SPINLOCK(async_lock);

struct async_entry {
&#x9;struct list_head&#x9;domain_list;
&#x9;struct list_head&#x9;global_list;
&#x9;struct work_struct&#x9;work;
&#x9;async_cookie_t&#x9;&#x9;cookie;
&#x9;async_func_t&#x9;&#x9;func;
&#x9;void&#x9;&#x9;&#x9;*data;
&#x9;struct async_domain&#x9;*domain;
};

static DECLARE_WAIT_QUEUE_HEAD(async_done);

static atomic_t entry_count;

static async_cookie_t lowest_in_progress(struct async_domaindomain)
{
&#x9;struct list_headpending;
&#x9;async_cookie_t ret = ASYNC_COOKIE_MAX;
&#x9;unsigned long flags;

&#x9;spin_lock_irqsave(&#x26;async_lock, flags);

&#x9;if (domain)
&#x9;&#x9;pending = &#x26;domain-&#x3E;pending;
&#x9;else
&#x9;&#x9;pending = &#x26;async_global_pending;

&#x9;if (!list_empty(pending))
&#x9;&#x9;ret = list_first_entry(pending, struct async_entry,
&#x9;&#x9;&#x9;&#x9;       domain_list)-&#x3E;cookie;

&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);
&#x9;return ret;
}

*/
 pick the first pending entry and run it
 /*
static void async_run_entry_fn(struct work_structwork)
{
&#x9;struct async_entryentry =
&#x9;&#x9;container_of(work, struct async_entry, work);
&#x9;unsigned long flags;
&#x9;ktime_t uninitialized_var(calltime), delta, rettime;

&#x9;*/ 1) run (and print duration) /*
&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
&#x9;&#x9;pr_debug(&#x22;calling  %lli_%pF @ %i\n&#x22;,
&#x9;&#x9;&#x9;(long long)entry-&#x3E;cookie,
&#x9;&#x9;&#x9;entry-&#x3E;func, task_pid_nr(current));
&#x9;&#x9;calltime = ktime_get();
&#x9;}
&#x9;entry-&#x3E;func(entry-&#x3E;data, entry-&#x3E;cookie);
&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
&#x9;&#x9;rettime = ktime_get();
&#x9;&#x9;delta = ktime_sub(rettime, calltime);
&#x9;&#x9;pr_debug(&#x22;initcall %lli_%pF returned 0 after %lld usecs\n&#x22;,
&#x9;&#x9;&#x9;(long long)entry-&#x3E;cookie,
&#x9;&#x9;&#x9;entry-&#x3E;func,
&#x9;&#x9;&#x9;(long long)ktime_to_ns(delta) &#x3E;&#x3E; 10);
&#x9;}

&#x9;*/ 2) remove self from the pending queues /*
&#x9;spin_lock_irqsave(&#x26;async_lock, flags);
&#x9;list_del_init(&#x26;entry-&#x3E;domain_list);
&#x9;list_del_init(&#x26;entry-&#x3E;global_list);

&#x9;*/ 3) free the entry /*
&#x9;kfree(entry);
&#x9;atomic_dec(&#x26;entry_count);

&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);

&#x9;*/ 4) wake up any waiters /*
&#x9;wake_up(&#x26;async_done);
}

static async_cookie_t __async_schedule(async_func_t func, voiddata, struct async_domaindomain)
{
&#x9;struct async_entryentry;
&#x9;unsigned long flags;
&#x9;async_cookie_t newcookie;

&#x9;*/ allow irq-off callers /*
&#x9;entry = kzalloc(sizeof(struct async_entry), GFP_ATOMIC);

&#x9;*/
&#x9; If we&#x27;re out of memory or if there&#x27;s too much work
&#x9; pending already, we execute synchronously.
&#x9; /*
&#x9;if (!entry || atomic_read(&#x26;entry_count) &#x3E; MAX_WORK) {
&#x9;&#x9;kfree(entry);
&#x9;&#x9;spin_lock_irqsave(&#x26;async_lock, flags);
&#x9;&#x9;newcookie = next_cookie++;
&#x9;&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);

&#x9;&#x9;*/ low on memory.. run synchronously /*
&#x9;&#x9;func(data, newcookie);
&#x9;&#x9;return newcookie;
&#x9;}
&#x9;INIT_LIST_HEAD(&#x26;entry-&#x3E;domain_list);
&#x9;INIT_LIST_HEAD(&#x26;entry-&#x3E;global_list);
&#x9;INIT_WORK(&#x26;entry-&#x3E;work, async_run_entry_fn);
&#x9;entry-&#x3E;func = func;
&#x9;entry-&#x3E;data = data;
&#x9;entry-&#x3E;domain = domain;

&#x9;spin_lock_irqsave(&#x26;async_lock, flags);

&#x9;*/ allocate cookie and queue /*
&#x9;newcookie = entry-&#x3E;cookie = next_cookie++;

&#x9;list_add_tail(&#x26;entry-&#x3E;domain_list, &#x26;domain-&#x3E;pending);
&#x9;if (domain-&#x3E;registered)
&#x9;&#x9;list_add_tail(&#x26;entry-&#x3E;global_list, &#x26;async_global_pending);

&#x9;atomic_inc(&#x26;entry_count);
&#x9;spin_unlock_irqrestore(&#x26;async_lock, flags);

&#x9;*/ mark that this task has queued an async job, used by module init /*
&#x9;current-&#x3E;flags |= PF_USED_ASYNC;

&#x9;*/ schedule for execution /*
&#x9;queue_work(system_unbound_wq, &#x26;entry-&#x3E;work);

&#x9;return newcookie;
}

*/
 async_schedule - schedule a function for asynchronous execution
 @func: function to execute asynchronously
 @data: data pointer to pass to the function

 Returns an async_cookie_t that may be used for checkpointing later.
 Note: This function may be called from atomic or non-atomic contexts.
 /*
async_cookie_t async_schedule(async_func_t func, voiddata)
{
&#x9;return __async_schedule(func, data, &#x26;async_dfl_domain);
}
EXPORT_SYMBOL_GPL(async_schedule);

*/
 async_schedule_domain - schedule a function for asynchronous execution within a certain domain
 @func: function to execute asynchronously
 @data: data pointer to pass to the function
 @domain: the domain

 Returns an async_cookie_t that may be used for checkpointing later.
 @domain may be used in the async_synchronize_*_domain() functions to
 wait within a certain synchronization domain rather than globally.  A
 synchronization domain is specified via @domain.  Note: This function
 may be called from atomic or non-atomic contexts.
 /*
async_cookie_t async_schedule_domain(async_func_t func, voiddata,
&#x9;&#x9;&#x9;&#x9;     struct async_domaindomain)
{
&#x9;return __async_schedule(func, data, domain);
}
EXPORT_SYMBOL_GPL(async_schedule_domain);

*/
 async_synchronize_full - synchronize all asynchronous function calls

 This function waits until all asynchronous function calls have been done.
 /*
void async_synchronize_full(void)
{
&#x9;async_synchronize_full_domain(NULL);
}
EXPORT_SYMBOL_GPL(async_synchronize_full);

*/
 async_unregister_domain - ensure no more anonymous waiters on this domain
 @domain: idle domain to flush out of any async_synchronize_full instances

 async_synchronize_{cookie|full}_domain() are not flushed since callers
 of these routines should know the lifetime of @domain

 Prefer ASYNC_DOMAIN_EXCLUSIVE() declarations over flushing
 /*
void async_unregister_domain(struct async_domaindomain)
{
&#x9;spin_lock_irq(&#x26;async_lock);
&#x9;WARN_ON(!domain-&#x3E;registered || !list_empty(&#x26;domain-&#x3E;pending));
&#x9;domain-&#x3E;registered = 0;
&#x9;spin_unlock_irq(&#x26;async_lock);
}
EXPORT_SYMBOL_GPL(async_unregister_domain);

*/
 async_synchronize_full_domain - synchronize all asynchronous function within a certain domain
 @domain: the domain to synchronize

 This function waits until all asynchronous function calls for the
 synchronization domain specified by @domain have been done.
 /*
void async_synchronize_full_domain(struct async_domaindomain)
{
&#x9;async_synchronize_cookie_domain(ASYNC_COOKIE_MAX, domain);
}
EXPORT_SYMBOL_GPL(async_synchronize_full_domain);

*/
 async_synchronize_cookie_domain - synchronize asynchronous function calls within a certain domain with cookie checkpointing
 @cookie: async_cookie_t to use as checkpoint
 @domain: the domain to synchronize (%NULL for all registered domains)

 This function waits until all asynchronous function calls for the
 synchronization domain specified by @domain submitted prior to @cookie
 have been done.
 /*
void async_synchronize_cookie_domain(async_cookie_t cookie, struct async_domaindomain)
{
&#x9;ktime_t uninitialized_var(starttime), delta, endtime;

&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
&#x9;&#x9;pr_debug(&#x22;async_waiting @ %i\n&#x22;, task_pid_nr(current));
&#x9;&#x9;starttime = ktime_get();
&#x9;}

&#x9;wait_event(async_done, lowest_in_progress(domain) &#x3E;= cookie);

&#x9;if (initcall_debug &#x26;&#x26; system_state == SYSTEM_BOOTING) {
&#x9;&#x9;endtime = ktime_get();
&#x9;&#x9;delta = ktime_sub(endtime, starttime);

&#x9;&#x9;pr_debug(&#x22;async_continuing @ %i after %lli usec\n&#x22;,
&#x9;&#x9;&#x9;task_pid_nr(current),
&#x9;&#x9;&#x9;(long long)ktime_to_ns(delta) &#x3E;&#x3E; 10);
&#x9;}
}
EXPORT_SYMBOL_GPL(async_synchronize_cookie_domain);

*/
 async_synchronize_cookie - synchronize asynchronous function calls with cookie checkpointing
 @cookie: async_cookie_t to use as checkpoint

 This function waits until all asynchronous function calls prior to @cookie
 have been done.
 /*
void async_synchronize_cookie(async_cookie_t cookie)
{
&#x9;async_synchronize_cookie_domain(cookie, &#x26;async_dfl_domain);
}
EXPORT_SYMBOL_GPL(async_synchronize_cookie);

*/
 current_is_async - is %current an async worker task?

 Returns %true if %current is an async worker task.
 /*
bool current_is_async(void)
{
&#x9;struct workerworker = current_wq_worker();

&#x9;return worker &#x26;&#x26; worker-&#x3E;current_func == async_run_entry_fn;
}
EXPORT_SYMBOL_GPL(current_is_async);
*/
 audit.c -- Auditing support
 Gateway between the kernel (e.g., selinux) and the user-space audit daemon.
 System-call specific features have moved to auditsc.c

 Copyright 2003-2007 Red Hat Inc., Durham, North Carolina.
 All Rights Reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Written by Rickard E. (Rik) Faith &#x3C;faith@redhat.com&#x3E;

 Goals: 1) Integrate fully with Security Modules.
&#x9;  2) Minimal run-time overhead:
&#x9;     a) Minimal when syscall auditing is disabled (audit_enable=0).
&#x9;     b) Small when syscall auditing is enabled and no audit record
&#x9;&#x9;is generated (defer as much work as possible to record
&#x9;&#x9;generation time):
&#x9;&#x9;i) context is allocated,
&#x9;&#x9;ii) names from getname are stored without a copy, and
&#x9;&#x9;iii) inode information stored from path_lookup.
&#x9;  3) Ability to disable syscall auditing at boot time (audit=0).
&#x9;  4) Usable by other parts of the kernel (if audit_log* is called,
&#x9;     then a syscall record will be generated automatically for the
&#x9;     current syscall).
&#x9;  5) Netlink interface to user-space.
&#x9;  6) Support low-overhead kernel-based filtering to minimize the
&#x9;     information that must be passed to user-space.

 Example user-space utilities: http://people.redhat.com/sgrubb/audit/
 /*

#define pr_fmt(fmt) KBUILD_MODNAME &#x22;: &#x22; fmt

#include &#x3C;linux/file.h&#x3E;
#include &#x3C;linux/init.h&#x3E;
#include &#x3C;linux/types.h&#x3E;
#include &#x3C;linux/atomic.h&#x3E;
#include &#x3C;linux/mm.h&#x3E;
#include &#x3C;linux/export.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/err.h&#x3E;
#include &#x3C;linux/kthread.h&#x3E;
#include &#x3C;linux/kernel.h&#x3E;
#include &#x3C;linux/syscalls.h&#x3E;

#include &#x3C;linux/audit.h&#x3E;

#include &#x3C;net/sock.h&#x3E;
#include &#x3C;net/netlink.h&#x3E;
#include &#x3C;linux/skbuff.h&#x3E;
#ifdef CONFIG_SECURITY
#include &#x3C;linux/security.h&#x3E;
#endif
#include &#x3C;linux/freezer.h&#x3E;
#include &#x3C;linux/tty.h&#x3E;
#include &#x3C;linux/pid_namespace.h&#x3E;
#include &#x3C;net/netns/generic.h&#x3E;

#include &#x22;audit.h&#x22;

*/ No auditing will take place until audit_initialized == AUDIT_INITIALIZED.
 (Initialization happens after skb_init is called.) /*
#define AUDIT_DISABLED&#x9;&#x9;-1
#define AUDIT_UNINITIALIZED&#x9;0
#define AUDIT_INITIALIZED&#x9;1
static int&#x9;audit_initialized;

#define AUDIT_OFF&#x9;0
#define AUDIT_ON&#x9;1
#define AUDIT_LOCKED&#x9;2
u32&#x9;&#x9;audit_enabled;
u32&#x9;&#x9;audit_ever_enabled;

EXPORT_SYMBOL_GPL(audit_enabled);

*/ Default state when kernel boots without any parameters. /*
static u32&#x9;audit_default;

*/ If auditing cannot proceed, audit_failure selects what happens. /*
static u32&#x9;audit_failure = AUDIT_FAIL_PRINTK;

*/
 If audit records are to be written to the netlink socket, audit_pid
 contains the pid of the auditd process and audit_nlk_portid contains
 the portid to use to send netlink messages to that process.
 /*
int&#x9;&#x9;audit_pid;
static __u32&#x9;audit_nlk_portid;

*/ If audit_rate_limit is non-zero, limit the rate of sending audit records
 to that number per second.  This prevents DoS attacks, but results in
 audit records being dropped. /*
static u32&#x9;audit_rate_limit;

*/ Number of outstanding audit_buffers allowed.
 When set to zero, this means unlimited. /*
static u32&#x9;audit_backlog_limit = 64;
#define AUDIT_BACKLOG_WAIT_TIME (60 HZ)
static u32&#x9;audit_backlog_wait_time_master = AUDIT_BACKLOG_WAIT_TIME;
static u32&#x9;audit_backlog_wait_time = AUDIT_BACKLOG_WAIT_TIME;

*/ The identity of the user shutting down the audit system. /*
kuid_t&#x9;&#x9;audit_sig_uid = INVALID_UID;
pid_t&#x9;&#x9;audit_sig_pid = -1;
u32&#x9;&#x9;audit_sig_sid = 0;

*/ Records can be lost in several ways:
   0) [suppressed in audit_alloc]
   1) out of memory in audit_log_start [kmalloc of struct audit_buffer]
   2) out of memory in audit_log_move [alloc_skb]
   3) suppressed due to audit_rate_limit
   4) suppressed due to audit_backlog_limit
/*
static atomic_t    audit_lost = ATOMIC_INIT(0);

*/ The netlink socket. /*
static struct sockaudit_sock;
static int audit_net_id;

*/ Hash for inode-based rules /*
struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];

*/ The audit_freelist is a list of pre-allocated audit buffers (if more
 than AUDIT_MAXFREE are in use, the audit buffer is freed instead of
 being placed on the freelist). /*
static DEFINE_SPINLOCK(audit_freelist_lock);
static int&#x9;   audit_freelist_count;
static LIST_HEAD(audit_freelist);

static struct sk_buff_head audit_skb_queue;
*/ queue of skbs to send to auditd when/if it comes back /*
static struct sk_buff_head audit_skb_hold_queue;
static struct task_structkauditd_task;
static DECLARE_WAIT_QUEUE_HEAD(kauditd_wait);
static DECLARE_WAIT_QUEUE_HEAD(audit_backlog_wait);

static struct audit_features af = {.vers = AUDIT_FEATURE_VERSION,
&#x9;&#x9;&#x9;&#x9;   .mask = -1,
&#x9;&#x9;&#x9;&#x9;   .features = 0,
&#x9;&#x9;&#x9;&#x9;   .lock = 0,};

static charaudit_feature_names[2] = {
&#x9;&#x22;only_unset_loginuid&#x22;,
&#x9;&#x22;loginuid_immutable&#x22;,
};


*/ Serialize requests from userspace. /*
DEFINE_MUTEX(audit_cmd_mutex);

*/ AUDIT_BUFSIZ is the size of the temporary buffer used for formatting
 audit records.  Since printk uses a 1024 byte buffer, this buffer
 should be at least that large. /*
#define AUDIT_BUFSIZ 1024

*/ AUDIT_MAXFREE is the number of empty audit_buffers we keep on the
 audit_freelist.  Doing so eliminates many kmalloc/kfree calls. /*
#define AUDIT_MAXFREE  (2*NR_CPUS)

*/ The audit_buffer is used when formatting an audit record.  The caller
 locks briefly to get the record off the freelist or to allocate the
 buffer, and locks briefly to send the buffer to the netlink layer or
 to place it on a transmit queue.  Multiple audit_buffers can be in
 use simultaneously. /*
struct audit_buffer {
&#x9;struct list_head     list;
&#x9;struct sk_buff      skb;&#x9;*/ formatted skb ready to send /*
&#x9;struct audit_contextctx;&#x9;*/ NULL or associated context /*
&#x9;gfp_t&#x9;&#x9;     gfp_mask;
};

struct audit_reply {
&#x9;__u32 portid;
&#x9;struct netnet;
&#x9;struct sk_buffskb;
};

static void audit_set_portid(struct audit_bufferab, __u32 portid)
{
&#x9;if (ab) {
&#x9;&#x9;struct nlmsghdrnlh = nlmsg_hdr(ab-&#x3E;skb);
&#x9;&#x9;nlh-&#x3E;nlmsg_pid = portid;
&#x9;}
}

void audit_panic(const charmessage)
{
&#x9;switch (audit_failure) {
&#x9;case AUDIT_FAIL_SILENT:
&#x9;&#x9;break;
&#x9;case AUDIT_FAIL_PRINTK:
&#x9;&#x9;if (printk_ratelimit())
&#x9;&#x9;&#x9;pr_err(&#x22;%s\n&#x22;, message);
&#x9;&#x9;break;
&#x9;case AUDIT_FAIL_PANIC:
&#x9;&#x9;*/ test audit_pid since printk is always losey, why bother? /*
&#x9;&#x9;if (audit_pid)
&#x9;&#x9;&#x9;panic(&#x22;audit: %s\n&#x22;, message);
&#x9;&#x9;break;
&#x9;}
}

static inline int audit_rate_check(void)
{
&#x9;static unsigned long&#x9;last_check = 0;
&#x9;static int&#x9;&#x9;messages   = 0;
&#x9;static DEFINE_SPINLOCK(lock);
&#x9;unsigned long&#x9;&#x9;flags;
&#x9;unsigned long&#x9;&#x9;now;
&#x9;unsigned long&#x9;&#x9;elapsed;
&#x9;int&#x9;&#x9;&#x9;retval&#x9;   = 0;

&#x9;if (!audit_rate_limit) return 1;

&#x9;spin_lock_irqsave(&#x26;lock, flags);
&#x9;if (++messages &#x3C; audit_rate_limit) {
&#x9;&#x9;retval = 1;
&#x9;} else {
&#x9;&#x9;now     = jiffies;
&#x9;&#x9;elapsed = now - last_check;
&#x9;&#x9;if (elapsed &#x3E; HZ) {
&#x9;&#x9;&#x9;last_check = now;
&#x9;&#x9;&#x9;messages   = 0;
&#x9;&#x9;&#x9;retval     = 1;
&#x9;&#x9;}
&#x9;}
&#x9;spin_unlock_irqrestore(&#x26;lock, flags);

&#x9;return retval;
}

*/
 audit_log_lost - conditionally log lost audit message event
 @message: the message stating reason for lost audit message

 Emit at least 1 message per second, even if audit_rate_check is
 throttling.
 Always increment the lost messages counter.
/*
void audit_log_lost(const charmessage)
{
&#x9;static unsigned long&#x9;last_msg = 0;
&#x9;static DEFINE_SPINLOCK(lock);
&#x9;unsigned long&#x9;&#x9;flags;
&#x9;unsigned long&#x9;&#x9;now;
&#x9;int&#x9;&#x9;&#x9;print;

&#x9;atomic_inc(&#x26;audit_lost);

&#x9;print = (audit_failure == AUDIT_FAIL_PANIC || !audit_rate_limit);

&#x9;if (!print) {
&#x9;&#x9;spin_lock_irqsave(&#x26;lock, flags);
&#x9;&#x9;now = jiffies;
&#x9;&#x9;if (now - last_msg &#x3E; HZ) {
&#x9;&#x9;&#x9;print = 1;
&#x9;&#x9;&#x9;last_msg = now;
&#x9;&#x9;}
&#x9;&#x9;spin_unlock_irqrestore(&#x26;lock, flags);
&#x9;}

&#x9;if (print) {
&#x9;&#x9;if (printk_ratelimit())
&#x9;&#x9;&#x9;pr_warn(&#x22;audit_lost=%u audit_rate_limit=%u audit_backlog_limit=%u\n&#x22;,
&#x9;&#x9;&#x9;&#x9;atomic_read(&#x26;audit_lost),
&#x9;&#x9;&#x9;&#x9;audit_rate_limit,
&#x9;&#x9;&#x9;&#x9;audit_backlog_limit);
&#x9;&#x9;audit_panic(message);
&#x9;}
}

static int audit_log_config_change(charfunction_name, u32 new, u32 old,
&#x9;&#x9;&#x9;&#x9;   int allow_changes)
{
&#x9;struct audit_bufferab;
&#x9;int rc = 0;

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
&#x9;if (unlikely(!ab))
&#x9;&#x9;return rc;
&#x9;audit_log_format(ab, &#x22;%s=%u old=%u&#x22;, function_name, new, old);
&#x9;audit_log_session_info(ab);
&#x9;rc = audit_log_task_context(ab);
&#x9;if (rc)
&#x9;&#x9;allow_changes = 0;/ Something weird, deny request /*
&#x9;audit_log_format(ab, &#x22; res=%d&#x22;, allow_changes);
&#x9;audit_log_end(ab);
&#x9;return rc;
}

static int audit_do_config_change(charfunction_name, u32to_change, u32 new)
{
&#x9;int allow_changes, rc = 0;
&#x9;u32 old =to_change;

&#x9;*/ check if we are locked /*
&#x9;if (audit_enabled == AUDIT_LOCKED)
&#x9;&#x9;allow_changes = 0;
&#x9;else
&#x9;&#x9;allow_changes = 1;

&#x9;if (audit_enabled != AUDIT_OFF) {
&#x9;&#x9;rc = audit_log_config_change(function_name, new, old, allow_changes);
&#x9;&#x9;if (rc)
&#x9;&#x9;&#x9;allow_changes = 0;
&#x9;}

&#x9;*/ If we are allowed, make the change /*
&#x9;if (allow_changes == 1)
&#x9;&#x9;*to_change = new;
&#x9;*/ Not allowed, update reason /*
&#x9;else if (rc == 0)
&#x9;&#x9;rc = -EPERM;
&#x9;return rc;
}

static int audit_set_rate_limit(u32 limit)
{
&#x9;return audit_do_config_change(&#x22;audit_rate_limit&#x22;, &#x26;audit_rate_limit, limit);
}

static int audit_set_backlog_limit(u32 limit)
{
&#x9;return audit_do_config_change(&#x22;audit_backlog_limit&#x22;, &#x26;audit_backlog_limit, limit);
}

static int audit_set_backlog_wait_time(u32 timeout)
{
&#x9;return audit_do_config_change(&#x22;audit_backlog_wait_time&#x22;,
&#x9;&#x9;&#x9;&#x9;      &#x26;audit_backlog_wait_time_master, timeout);
}

static int audit_set_enabled(u32 state)
{
&#x9;int rc;
&#x9;if (state &#x3E; AUDIT_LOCKED)
&#x9;&#x9;return -EINVAL;

&#x9;rc =  audit_do_config_change(&#x22;audit_enabled&#x22;, &#x26;audit_enabled, state);
&#x9;if (!rc)
&#x9;&#x9;audit_ever_enabled |= !!state;

&#x9;return rc;
}

static int audit_set_failure(u32 state)
{
&#x9;if (state != AUDIT_FAIL_SILENT
&#x9;    &#x26;&#x26; state != AUDIT_FAIL_PRINTK
&#x9;    &#x26;&#x26; state != AUDIT_FAIL_PANIC)
&#x9;&#x9;return -EINVAL;

&#x9;return audit_do_config_change(&#x22;audit_failure&#x22;, &#x26;audit_failure, state);
}

*/
 Queue skbs to be sent to auditd when/if it comes back.  These skbs should
 already have been sent via prink/syslog and so if these messages are dropped
 it is not a huge concern since we already passed the audit_log_lost()
 notification and stuff.  This is just nice to get audit messages during
 boot before auditd is running or messages generated while auditd is stopped.
 This only holds messages is audit_default is set, aka booting with audit=1
 or building your kernel that way.
 /*
static void audit_hold_skb(struct sk_buffskb)
{
&#x9;if (audit_default &#x26;&#x26;
&#x9;    (!audit_backlog_limit ||
&#x9;     skb_queue_len(&#x26;audit_skb_hold_queue) &#x3C; audit_backlog_limit))
&#x9;&#x9;skb_queue_tail(&#x26;audit_skb_hold_queue, skb);
&#x9;else
&#x9;&#x9;kfree_skb(skb);
}

*/
 For one reason or another this nlh isn&#x27;t getting delivered to the userspace
 audit daemon, just send it to printk.
 /*
static void audit_printk_skb(struct sk_buffskb)
{
&#x9;struct nlmsghdrnlh = nlmsg_hdr(skb);
&#x9;chardata = nlmsg_data(nlh);

&#x9;if (nlh-&#x3E;nlmsg_type != AUDIT_EOE) {
&#x9;&#x9;if (printk_ratelimit())
&#x9;&#x9;&#x9;pr_notice(&#x22;type=%d %s\n&#x22;, nlh-&#x3E;nlmsg_type, data);
&#x9;&#x9;else
&#x9;&#x9;&#x9;audit_log_lost(&#x22;printk limit exceeded&#x22;);
&#x9;}

&#x9;audit_hold_skb(skb);
}

static void kauditd_send_skb(struct sk_buffskb)
{
&#x9;int err;
&#x9;int attempts = 0;
#define AUDITD_RETRIES 5

restart:
&#x9;*/ take a reference in case we can&#x27;t send it and we want to hold it /*
&#x9;skb_get(skb);
&#x9;err = netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
&#x9;if (err &#x3C; 0) {
&#x9;&#x9;pr_err(&#x22;netlink_unicast sending to audit_pid=%d returned error: %d\n&#x22;,
&#x9;&#x9;       audit_pid, err);
&#x9;&#x9;if (audit_pid) {
&#x9;&#x9;&#x9;if (err == -ECONNREFUSED || err == -EPERM
&#x9;&#x9;&#x9;    || ++attempts &#x3E;= AUDITD_RETRIES) {
&#x9;&#x9;&#x9;&#x9;char s[32];

&#x9;&#x9;&#x9;&#x9;snprintf(s, sizeof(s), &#x22;audit_pid=%d reset&#x22;, audit_pid);
&#x9;&#x9;&#x9;&#x9;audit_log_lost(s);
&#x9;&#x9;&#x9;&#x9;audit_pid = 0;
&#x9;&#x9;&#x9;&#x9;audit_sock = NULL;
&#x9;&#x9;&#x9;} else {
&#x9;&#x9;&#x9;&#x9;pr_warn(&#x22;re-scheduling(#%d) write to audit_pid=%d\n&#x22;,
&#x9;&#x9;&#x9;&#x9;&#x9;attempts, audit_pid);
&#x9;&#x9;&#x9;&#x9;set_current_state(TASK_INTERRUPTIBLE);
&#x9;&#x9;&#x9;&#x9;schedule();
&#x9;&#x9;&#x9;&#x9;__set_current_state(TASK_RUNNING);
&#x9;&#x9;&#x9;&#x9;goto restart;
&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;&#x9;*/ we might get lucky and get this in the next auditd /*
&#x9;&#x9;audit_hold_skb(skb);
&#x9;} else
&#x9;&#x9;*/ drop the extra reference if sent ok /*
&#x9;&#x9;consume_skb(skb);
}

*/
 kauditd_send_multicast_skb - send the skb to multicast userspace listeners

 This function doesn&#x27;t consume an skb as might be expected since it has to
 copy it anyways.
 /*
static void kauditd_send_multicast_skb(struct sk_buffskb, gfp_t gfp_mask)
{
&#x9;struct sk_buff&#x9;&#x9;*copy;
&#x9;struct audit_net&#x9;*aunet = net_generic(&#x26;init_net, audit_net_id);
&#x9;struct sock&#x9;&#x9;*sock = aunet-&#x3E;nlsk;

&#x9;if (!netlink_has_listeners(sock, AUDIT_NLGRP_READLOG))
&#x9;&#x9;return;

&#x9;*/
&#x9; The seemingly wasteful skb_copy() rather than bumping the refcount
&#x9; using skb_get() is necessary because non-standard mods are made to
&#x9; the skb by the original kaudit unicast socket send routine.  The
&#x9; existing auditd daemon assumes this breakage.  Fixing this would
&#x9; require co-ordinating a change in the established protocol between
&#x9; the kaudit kernel subsystem and the auditd userspace code.  There is
&#x9; no reason for new multicast clients to continue with this
&#x9; non-compliance.
&#x9; /*
&#x9;copy = skb_copy(skb, gfp_mask);
&#x9;if (!copy)
&#x9;&#x9;return;

&#x9;nlmsg_multicast(sock, copy, 0, AUDIT_NLGRP_READLOG, gfp_mask);
}

*/
 flush_hold_queue - empty the hold queue if auditd appears

 If auditd just started, drain the queue of messages already
 sent to syslog/printk.  Remember loss here is ok.  We already
 called audit_log_lost() if it didn&#x27;t go out normally.  so the
 race between the skb_dequeue and the next check for audit_pid
 doesn&#x27;t matter.

 If you ever find kauditd to be too slow we can get a perf win
 by doing our own locking and keeping better track if there
 are messages in this queue.  I don&#x27;t see the need now, but
 in 5 years when I want to play with this again I&#x27;ll see this
 note and still have no friggin idea what i&#x27;m thinking today.
 /*
static void flush_hold_queue(void)
{
&#x9;struct sk_buffskb;

&#x9;if (!audit_default || !audit_pid)
&#x9;&#x9;return;

&#x9;skb = skb_dequeue(&#x26;audit_skb_hold_queue);
&#x9;if (likely(!skb))
&#x9;&#x9;return;

&#x9;while (skb &#x26;&#x26; audit_pid) {
&#x9;&#x9;kauditd_send_skb(skb);
&#x9;&#x9;skb = skb_dequeue(&#x26;audit_skb_hold_queue);
&#x9;}

&#x9;*/
&#x9; if auditd just disappeared but we
&#x9; dequeued an skb we need to drop ref
&#x9; /*
&#x9;consume_skb(skb);
}

static int kauditd_thread(voiddummy)
{
&#x9;set_freezable();
&#x9;while (!kthread_should_stop()) {
&#x9;&#x9;struct sk_buffskb;

&#x9;&#x9;flush_hold_queue();

&#x9;&#x9;skb = skb_dequeue(&#x26;audit_skb_queue);

&#x9;&#x9;if (skb) {
&#x9;&#x9;&#x9;if (!audit_backlog_limit ||
&#x9;&#x9;&#x9;    (skb_queue_len(&#x26;audit_skb_queue) &#x3C;= audit_backlog_limit))
&#x9;&#x9;&#x9;&#x9;wake_up(&#x26;audit_backlog_wait);
&#x9;&#x9;&#x9;if (audit_pid)
&#x9;&#x9;&#x9;&#x9;kauditd_send_skb(skb);
&#x9;&#x9;&#x9;else
&#x9;&#x9;&#x9;&#x9;audit_printk_skb(skb);
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;}

&#x9;&#x9;wait_event_freezable(kauditd_wait, skb_queue_len(&#x26;audit_skb_queue));
&#x9;}
&#x9;return 0;
}

int audit_send_list(void_dest)
{
&#x9;struct audit_netlink_listdest = _dest;
&#x9;struct sk_buffskb;
&#x9;struct netnet = dest-&#x3E;net;
&#x9;struct audit_netaunet = net_generic(net, audit_net_id);

&#x9;*/ wait for parent to finish and send an ACK /*
&#x9;mutex_lock(&#x26;audit_cmd_mutex);
&#x9;mutex_unlock(&#x26;audit_cmd_mutex);

&#x9;while ((skb = __skb_dequeue(&#x26;dest-&#x3E;q)) != NULL)
&#x9;&#x9;netlink_unicast(aunet-&#x3E;nlsk, skb, dest-&#x3E;portid, 0);

&#x9;put_net(net);
&#x9;kfree(dest);

&#x9;return 0;
}

struct sk_buffaudit_make_reply(__u32 portid, int seq, int type, int done,
&#x9;&#x9;&#x9;&#x9; int multi, const voidpayload, int size)
{
&#x9;struct sk_buff&#x9;*skb;
&#x9;struct nlmsghdr&#x9;*nlh;
&#x9;void&#x9;&#x9;*data;
&#x9;int&#x9;&#x9;flags = multi ? NLM_F_MULTI : 0;
&#x9;int&#x9;&#x9;t     = done  ? NLMSG_DONE  : type;

&#x9;skb = nlmsg_new(size, GFP_KERNEL);
&#x9;if (!skb)
&#x9;&#x9;return NULL;

&#x9;nlh&#x9;= nlmsg_put(skb, portid, seq, t, size, flags);
&#x9;if (!nlh)
&#x9;&#x9;goto out_kfree_skb;
&#x9;data = nlmsg_data(nlh);
&#x9;memcpy(data, payload, size);
&#x9;return skb;

out_kfree_skb:
&#x9;kfree_skb(skb);
&#x9;return NULL;
}

static int audit_send_reply_thread(voidarg)
{
&#x9;struct audit_replyreply = (struct audit_reply)arg;
&#x9;struct netnet = reply-&#x3E;net;
&#x9;struct audit_netaunet = net_generic(net, audit_net_id);

&#x9;mutex_lock(&#x26;audit_cmd_mutex);
&#x9;mutex_unlock(&#x26;audit_cmd_mutex);

&#x9;*/ Ignore failure. It&#x27;ll only happen if the sender goes away,
&#x9;   because our timeout is set to infinite. /*
&#x9;netlink_unicast(aunet-&#x3E;nlsk , reply-&#x3E;skb, reply-&#x3E;portid, 0);
&#x9;put_net(net);
&#x9;kfree(reply);
&#x9;return 0;
}
*/
 audit_send_reply - send an audit reply message via netlink
 @request_skb: skb of request we are replying to (used to target the reply)
 @seq: sequence number
 @type: audit message type
 @done: done (last) flag
 @multi: multi-part message flag
 @payload: payload data
 @size: payload size

 Allocates an skb, builds the netlink message, and sends it to the port id.
 No failure notifications.
 /*
static void audit_send_reply(struct sk_buffrequest_skb, int seq, int type, int done,
&#x9;&#x9;&#x9;     int multi, const voidpayload, int size)
{
&#x9;u32 portid = NETLINK_CB(request_skb).portid;
&#x9;struct netnet = sock_net(NETLINK_CB(request_skb).sk);
&#x9;struct sk_buffskb;
&#x9;struct task_structtsk;
&#x9;struct audit_replyreply = kmalloc(sizeof(struct audit_reply),
&#x9;&#x9;&#x9;&#x9;&#x9;    GFP_KERNEL);

&#x9;if (!reply)
&#x9;&#x9;return;

&#x9;skb = audit_make_reply(portid, seq, type, done, multi, payload, size);
&#x9;if (!skb)
&#x9;&#x9;goto out;

&#x9;reply-&#x3E;net = get_net(net);
&#x9;reply-&#x3E;portid = portid;
&#x9;reply-&#x3E;skb = skb;

&#x9;tsk = kthread_run(audit_send_reply_thread, reply, &#x22;audit_send_reply&#x22;);
&#x9;if (!IS_ERR(tsk))
&#x9;&#x9;return;
&#x9;kfree_skb(skb);
out:
&#x9;kfree(reply);
}

*/
 Check for appropriate CAP_AUDIT_ capabilities on incoming audit
 control messages.
 /*
static int audit_netlink_ok(struct sk_buffskb, u16 msg_type)
{
&#x9;int err = 0;

&#x9;*/ Only support initial user namespace for now. /*
&#x9;*/
&#x9; We return ECONNREFUSED because it tricks userspace into thinking
&#x9; that audit was not configured into the kernel.  Lots of users
&#x9; configure their PAM stack (because that&#x27;s what the distro does)
&#x9; to reject login if unable to send messages to audit.  If we return
&#x9; ECONNREFUSED the PAM stack thinks the kernel does not have audit
&#x9; configured in and will let login proceed.  If we return EPERM
&#x9; userspace will reject all logins.  This should be removed when we
&#x9; support non init namespaces!!
&#x9; /*
&#x9;if (current_user_ns() != &#x26;init_user_ns)
&#x9;&#x9;return -ECONNREFUSED;

&#x9;switch (msg_type) {
&#x9;case AUDIT_LIST:
&#x9;case AUDIT_ADD:
&#x9;case AUDIT_DEL:
&#x9;&#x9;return -EOPNOTSUPP;
&#x9;case AUDIT_GET:
&#x9;case AUDIT_SET:
&#x9;case AUDIT_GET_FEATURE:
&#x9;case AUDIT_SET_FEATURE:
&#x9;case AUDIT_LIST_RULES:
&#x9;case AUDIT_ADD_RULE:
&#x9;case AUDIT_DEL_RULE:
&#x9;case AUDIT_SIGNAL_INFO:
&#x9;case AUDIT_TTY_GET:
&#x9;case AUDIT_TTY_SET:
&#x9;case AUDIT_TRIM:
&#x9;case AUDIT_MAKE_EQUIV:
&#x9;&#x9;*/ Only support auditd and auditctl in initial pid namespace
&#x9;&#x9; for now. /*
&#x9;&#x9;if (task_active_pid_ns(current) != &#x26;init_pid_ns)
&#x9;&#x9;&#x9;return -EPERM;

&#x9;&#x9;if (!netlink_capable(skb, CAP_AUDIT_CONTROL))
&#x9;&#x9;&#x9;err = -EPERM;
&#x9;&#x9;break;
&#x9;case AUDIT_USER:
&#x9;case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
&#x9;case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
&#x9;&#x9;if (!netlink_capable(skb, CAP_AUDIT_WRITE))
&#x9;&#x9;&#x9;err = -EPERM;
&#x9;&#x9;break;
&#x9;default: / bad msg /*
&#x9;&#x9;err = -EINVAL;
&#x9;}

&#x9;return err;
}

static void audit_log_common_recv_msg(struct audit_buffer*ab, u16 msg_type)
{
&#x9;uid_t uid = from_kuid(&#x26;init_user_ns, current_uid());
&#x9;pid_t pid = task_tgid_nr(current);

&#x9;if (!audit_enabled &#x26;&#x26; msg_type != AUDIT_USER_AVC) {
&#x9;&#x9;*ab = NULL;
&#x9;&#x9;return;
&#x9;}

&#x9;*ab = audit_log_start(NULL, GFP_KERNEL, msg_type);
&#x9;if (unlikely(!*ab))
&#x9;&#x9;return;
&#x9;audit_log_format(*ab, &#x22;pid=%d uid=%u&#x22;, pid, uid);
&#x9;audit_log_session_info(*ab);
&#x9;audit_log_task_context(*ab);
}

int is_audit_feature_set(int i)
{
&#x9;return af.features &#x26; AUDIT_FEATURE_TO_MASK(i);
}


static int audit_get_feature(struct sk_buffskb)
{
&#x9;u32 seq;

&#x9;seq = nlmsg_hdr(skb)-&#x3E;nlmsg_seq;

&#x9;audit_send_reply(skb, seq, AUDIT_GET_FEATURE, 0, 0, &#x26;af, sizeof(af));

&#x9;return 0;
}

static void audit_log_feature_change(int which, u32 old_feature, u32 new_feature,
&#x9;&#x9;&#x9;&#x9;     u32 old_lock, u32 new_lock, int res)
{
&#x9;struct audit_bufferab;

&#x9;if (audit_enabled == AUDIT_OFF)
&#x9;&#x9;return;

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_FEATURE_CHANGE);
&#x9;audit_log_task_info(ab, current);
&#x9;audit_log_format(ab, &#x22; feature=%s old=%u new=%u old_lock=%u new_lock=%u res=%d&#x22;,
&#x9;&#x9;&#x9; audit_feature_names[which], !!old_feature, !!new_feature,
&#x9;&#x9;&#x9; !!old_lock, !!new_lock, res);
&#x9;audit_log_end(ab);
}

static int audit_set_feature(struct sk_buffskb)
{
&#x9;struct audit_featuresuaf;
&#x9;int i;

&#x9;BUILD_BUG_ON(AUDIT_LAST_FEATURE + 1 &#x3E; ARRAY_SIZE(audit_feature_names));
&#x9;uaf = nlmsg_data(nlmsg_hdr(skb));

&#x9;*/ if there is ever a version 2 we should handle that here /*

&#x9;for (i = 0; i &#x3C;= AUDIT_LAST_FEATURE; i++) {
&#x9;&#x9;u32 feature = AUDIT_FEATURE_TO_MASK(i);
&#x9;&#x9;u32 old_feature, new_feature, old_lock, new_lock;

&#x9;&#x9;*/ if we are not changing this feature, move along /*
&#x9;&#x9;if (!(feature &#x26; uaf-&#x3E;mask))
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;old_feature = af.features &#x26; feature;
&#x9;&#x9;new_feature = uaf-&#x3E;features &#x26; feature;
&#x9;&#x9;new_lock = (uaf-&#x3E;lock | af.lock) &#x26; feature;
&#x9;&#x9;old_lock = af.lock &#x26; feature;

&#x9;&#x9;*/ are we changing a locked feature? /*
&#x9;&#x9;if (old_lock &#x26;&#x26; (new_feature != old_feature)) {
&#x9;&#x9;&#x9;audit_log_feature_change(i, old_feature, new_feature,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; old_lock, new_lock, 0);
&#x9;&#x9;&#x9;return -EPERM;
&#x9;&#x9;}
&#x9;}
&#x9;*/ nothing invalid, do the changes /*
&#x9;for (i = 0; i &#x3C;= AUDIT_LAST_FEATURE; i++) {
&#x9;&#x9;u32 feature = AUDIT_FEATURE_TO_MASK(i);
&#x9;&#x9;u32 old_feature, new_feature, old_lock, new_lock;

&#x9;&#x9;*/ if we are not changing this feature, move along /*
&#x9;&#x9;if (!(feature &#x26; uaf-&#x3E;mask))
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;old_feature = af.features &#x26; feature;
&#x9;&#x9;new_feature = uaf-&#x3E;features &#x26; feature;
&#x9;&#x9;old_lock = af.lock &#x26; feature;
&#x9;&#x9;new_lock = (uaf-&#x3E;lock | af.lock) &#x26; feature;

&#x9;&#x9;if (new_feature != old_feature)
&#x9;&#x9;&#x9;audit_log_feature_change(i, old_feature, new_feature,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; old_lock, new_lock, 1);

&#x9;&#x9;if (new_feature)
&#x9;&#x9;&#x9;af.features |= feature;
&#x9;&#x9;else
&#x9;&#x9;&#x9;af.features &#x26;= ~feature;
&#x9;&#x9;af.lock |= new_lock;
&#x9;}

&#x9;return 0;
}

static int audit_replace(pid_t pid)
{
&#x9;struct sk_buffskb = audit_make_reply(0, 0, AUDIT_REPLACE, 0, 0,
&#x9;&#x9;&#x9;&#x9;&#x9;       &#x26;pid, sizeof(pid));

&#x9;if (!skb)
&#x9;&#x9;return -ENOMEM;
&#x9;return netlink_unicast(audit_sock, skb, audit_nlk_portid, 0);
}

static int audit_receive_msg(struct sk_buffskb, struct nlmsghdrnlh)
{
&#x9;u32&#x9;&#x9;&#x9;seq;
&#x9;void&#x9;&#x9;&#x9;*data;
&#x9;int&#x9;&#x9;&#x9;err;
&#x9;struct audit_buffer&#x9;*ab;
&#x9;u16&#x9;&#x9;&#x9;msg_type = nlh-&#x3E;nlmsg_type;
&#x9;struct audit_sig_info  sig_data;
&#x9;char&#x9;&#x9;&#x9;*ctx = NULL;
&#x9;u32&#x9;&#x9;&#x9;len;

&#x9;err = audit_netlink_ok(skb, msg_type);
&#x9;if (err)
&#x9;&#x9;return err;

&#x9;*/ As soon as there&#x27;s any sign of userspace auditd,
&#x9; start kauditd to talk to it /*
&#x9;if (!kauditd_task) {
&#x9;&#x9;kauditd_task = kthread_run(kauditd_thread, NULL, &#x22;kauditd&#x22;);
&#x9;&#x9;if (IS_ERR(kauditd_task)) {
&#x9;&#x9;&#x9;err = PTR_ERR(kauditd_task);
&#x9;&#x9;&#x9;kauditd_task = NULL;
&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;}
&#x9;seq  = nlh-&#x3E;nlmsg_seq;
&#x9;data = nlmsg_data(nlh);

&#x9;switch (msg_type) {
&#x9;case AUDIT_GET: {
&#x9;&#x9;struct audit_status&#x9;s;
&#x9;&#x9;memset(&#x26;s, 0, sizeof(s));
&#x9;&#x9;s.enabled&#x9;&#x9;= audit_enabled;
&#x9;&#x9;s.failure&#x9;&#x9;= audit_failure;
&#x9;&#x9;s.pid&#x9;&#x9;&#x9;= audit_pid;
&#x9;&#x9;s.rate_limit&#x9;&#x9;= audit_rate_limit;
&#x9;&#x9;s.backlog_limit&#x9;&#x9;= audit_backlog_limit;
&#x9;&#x9;s.lost&#x9;&#x9;&#x9;= atomic_read(&#x26;audit_lost);
&#x9;&#x9;s.backlog&#x9;&#x9;= skb_queue_len(&#x26;audit_skb_queue);
&#x9;&#x9;s.feature_bitmap&#x9;= AUDIT_FEATURE_BITMAP_ALL;
&#x9;&#x9;s.backlog_wait_time&#x9;= audit_backlog_wait_time_master;
&#x9;&#x9;audit_send_reply(skb, seq, AUDIT_GET, 0, 0, &#x26;s, sizeof(s));
&#x9;&#x9;break;
&#x9;}
&#x9;case AUDIT_SET: {
&#x9;&#x9;struct audit_status&#x9;s;
&#x9;&#x9;memset(&#x26;s, 0, sizeof(s));
&#x9;&#x9;*/ guard against past and future API changes /*
&#x9;&#x9;memcpy(&#x26;s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_ENABLED) {
&#x9;&#x9;&#x9;err = audit_set_enabled(s.enabled);
&#x9;&#x9;&#x9;if (err &#x3C; 0)
&#x9;&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_FAILURE) {
&#x9;&#x9;&#x9;err = audit_set_failure(s.failure);
&#x9;&#x9;&#x9;if (err &#x3C; 0)
&#x9;&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_PID) {
&#x9;&#x9;&#x9;int new_pid = s.pid;
&#x9;&#x9;&#x9;pid_t requesting_pid = task_tgid_vnr(current);

&#x9;&#x9;&#x9;if ((!new_pid) &#x26;&#x26; (requesting_pid != audit_pid)) {
&#x9;&#x9;&#x9;&#x9;audit_log_config_change(&#x22;audit_pid&#x22;, new_pid, audit_pid, 0);
&#x9;&#x9;&#x9;&#x9;return -EACCES;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;if (audit_pid &#x26;&#x26; new_pid &#x26;&#x26;
&#x9;&#x9;&#x9;    audit_replace(requesting_pid) != -ECONNREFUSED) {
&#x9;&#x9;&#x9;&#x9;audit_log_config_change(&#x22;audit_pid&#x22;, new_pid, audit_pid, 0);
&#x9;&#x9;&#x9;&#x9;return -EEXIST;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;if (audit_enabled != AUDIT_OFF)
&#x9;&#x9;&#x9;&#x9;audit_log_config_change(&#x22;audit_pid&#x22;, new_pid, audit_pid, 1);
&#x9;&#x9;&#x9;audit_pid = new_pid;
&#x9;&#x9;&#x9;audit_nlk_portid = NETLINK_CB(skb).portid;
&#x9;&#x9;&#x9;audit_sock = skb-&#x3E;sk;
&#x9;&#x9;}
&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_RATE_LIMIT) {
&#x9;&#x9;&#x9;err = audit_set_rate_limit(s.rate_limit);
&#x9;&#x9;&#x9;if (err &#x3C; 0)
&#x9;&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_BACKLOG_LIMIT) {
&#x9;&#x9;&#x9;err = audit_set_backlog_limit(s.backlog_limit);
&#x9;&#x9;&#x9;if (err &#x3C; 0)
&#x9;&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;&#x9;if (s.mask &#x26; AUDIT_STATUS_BACKLOG_WAIT_TIME) {
&#x9;&#x9;&#x9;if (sizeof(s) &#x3E; (size_t)nlh-&#x3E;nlmsg_len)
&#x9;&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;&#x9;if (s.backlog_wait_time &#x3E; 10*AUDIT_BACKLOG_WAIT_TIME)
&#x9;&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;&#x9;err = audit_set_backlog_wait_time(s.backlog_wait_time);
&#x9;&#x9;&#x9;if (err &#x3C; 0)
&#x9;&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;&#x9;break;
&#x9;}
&#x9;case AUDIT_GET_FEATURE:
&#x9;&#x9;err = audit_get_feature(skb);
&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;return err;
&#x9;&#x9;break;
&#x9;case AUDIT_SET_FEATURE:
&#x9;&#x9;err = audit_set_feature(skb);
&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;return err;
&#x9;&#x9;break;
&#x9;case AUDIT_USER:
&#x9;case AUDIT_FIRST_USER_MSG ... AUDIT_LAST_USER_MSG:
&#x9;case AUDIT_FIRST_USER_MSG2 ... AUDIT_LAST_USER_MSG2:
&#x9;&#x9;if (!audit_enabled &#x26;&#x26; msg_type != AUDIT_USER_AVC)
&#x9;&#x9;&#x9;return 0;

&#x9;&#x9;err = audit_filter_user(msg_type);
&#x9;&#x9;if (err == 1) {/ match or error /*
&#x9;&#x9;&#x9;err = 0;
&#x9;&#x9;&#x9;if (msg_type == AUDIT_USER_TTY) {
&#x9;&#x9;&#x9;&#x9;err = tty_audit_push();
&#x9;&#x9;&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
&#x9;&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, msg_type);
&#x9;&#x9;&#x9;if (msg_type != AUDIT_USER_TTY)
&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; msg=&#x27;%.*s&#x27;&#x22;,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; AUDIT_MESSAGE_TEXT_MAX,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9; (char)data);
&#x9;&#x9;&#x9;else {
&#x9;&#x9;&#x9;&#x9;int size;

&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; data=&#x22;);
&#x9;&#x9;&#x9;&#x9;size = nlmsg_len(nlh);
&#x9;&#x9;&#x9;&#x9;if (size &#x3E; 0 &#x26;&#x26;
&#x9;&#x9;&#x9;&#x9;    ((unsigned char)data)[size - 1] == &#x27;\0&#x27;)
&#x9;&#x9;&#x9;&#x9;&#x9;size--;
&#x9;&#x9;&#x9;&#x9;audit_log_n_untrustedstring(ab, data, size);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;audit_set_portid(ab, NETLINK_CB(skb).portid);
&#x9;&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_cmd_mutex);
&#x9;&#x9;}
&#x9;&#x9;break;
&#x9;case AUDIT_ADD_RULE:
&#x9;case AUDIT_DEL_RULE:
&#x9;&#x9;if (nlmsg_len(nlh) &#x3C; sizeof(struct audit_rule_data))
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;if (audit_enabled == AUDIT_LOCKED) {
&#x9;&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; audit_enabled=%d res=0&#x22;, audit_enabled);
&#x9;&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;&#x9;return -EPERM;
&#x9;&#x9;}
&#x9;&#x9;err = audit_rule_change(msg_type, NETLINK_CB(skb).portid,
&#x9;&#x9;&#x9;&#x9;&#x9;   seq, data, nlmsg_len(nlh));
&#x9;&#x9;break;
&#x9;case AUDIT_LIST_RULES:
&#x9;&#x9;err = audit_list_rules_send(skb, seq);
&#x9;&#x9;break;
&#x9;case AUDIT_TRIM:
&#x9;&#x9;audit_trim_trees();
&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
&#x9;&#x9;audit_log_format(ab, &#x22; op=trim res=1&#x22;);
&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;break;
&#x9;case AUDIT_MAKE_EQUIV: {
&#x9;&#x9;voidbufp = data;
&#x9;&#x9;u32 sizes[2];
&#x9;&#x9;size_t msglen = nlmsg_len(nlh);
&#x9;&#x9;charold,new;

&#x9;&#x9;err = -EINVAL;
&#x9;&#x9;if (msglen &#x3C; 2 sizeof(u32))
&#x9;&#x9;&#x9;break;
&#x9;&#x9;memcpy(sizes, bufp, 2 sizeof(u32));
&#x9;&#x9;bufp += 2 sizeof(u32);
&#x9;&#x9;msglen -= 2 sizeof(u32);
&#x9;&#x9;old = audit_unpack_string(&#x26;bufp, &#x26;msglen, sizes[0]);
&#x9;&#x9;if (IS_ERR(old)) {
&#x9;&#x9;&#x9;err = PTR_ERR(old);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;&#x9;new = audit_unpack_string(&#x26;bufp, &#x26;msglen, sizes[1]);
&#x9;&#x9;if (IS_ERR(new)) {
&#x9;&#x9;&#x9;err = PTR_ERR(new);
&#x9;&#x9;&#x9;kfree(old);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;&#x9;*/ OK, here comes... /*
&#x9;&#x9;err = audit_tag_tree(old, new);

&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);

&#x9;&#x9;audit_log_format(ab, &#x22; op=make_equiv old=&#x22;);
&#x9;&#x9;audit_log_untrustedstring(ab, old);
&#x9;&#x9;audit_log_format(ab, &#x22; new=&#x22;);
&#x9;&#x9;audit_log_untrustedstring(ab, new);
&#x9;&#x9;audit_log_format(ab, &#x22; res=%d&#x22;, !err);
&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;kfree(old);
&#x9;&#x9;kfree(new);
&#x9;&#x9;break;
&#x9;}
&#x9;case AUDIT_SIGNAL_INFO:
&#x9;&#x9;len = 0;
&#x9;&#x9;if (audit_sig_sid) {
&#x9;&#x9;&#x9;err = security_secid_to_secctx(audit_sig_sid, &#x26;ctx, &#x26;len);
&#x9;&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;&#x9;sig_data = kmalloc(sizeof(*sig_data) + len, GFP_KERNEL);
&#x9;&#x9;if (!sig_data) {
&#x9;&#x9;&#x9;if (audit_sig_sid)
&#x9;&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
&#x9;&#x9;&#x9;return -ENOMEM;
&#x9;&#x9;}
&#x9;&#x9;sig_data-&#x3E;uid = from_kuid(&#x26;init_user_ns, audit_sig_uid);
&#x9;&#x9;sig_data-&#x3E;pid = audit_sig_pid;
&#x9;&#x9;if (audit_sig_sid) {
&#x9;&#x9;&#x9;memcpy(sig_data-&#x3E;ctx, ctx, len);
&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
&#x9;&#x9;}
&#x9;&#x9;audit_send_reply(skb, seq, AUDIT_SIGNAL_INFO, 0, 0,
&#x9;&#x9;&#x9;&#x9; sig_data, sizeof(*sig_data) + len);
&#x9;&#x9;kfree(sig_data);
&#x9;&#x9;break;
&#x9;case AUDIT_TTY_GET: {
&#x9;&#x9;struct audit_tty_status s;
&#x9;&#x9;unsigned int t;

&#x9;&#x9;t = READ_ONCE(current-&#x3E;signal-&#x3E;audit_tty);
&#x9;&#x9;s.enabled = t &#x26; AUDIT_TTY_ENABLE;
&#x9;&#x9;s.log_passwd = !!(t &#x26; AUDIT_TTY_LOG_PASSWD);

&#x9;&#x9;audit_send_reply(skb, seq, AUDIT_TTY_GET, 0, 0, &#x26;s, sizeof(s));
&#x9;&#x9;break;
&#x9;}
&#x9;case AUDIT_TTY_SET: {
&#x9;&#x9;struct audit_tty_status s, old;
&#x9;&#x9;struct audit_buffer&#x9;*ab;
&#x9;&#x9;unsigned int t;

&#x9;&#x9;memset(&#x26;s, 0, sizeof(s));
&#x9;&#x9;*/ guard against past and future API changes /*
&#x9;&#x9;memcpy(&#x26;s, data, min_t(size_t, sizeof(s), nlmsg_len(nlh)));
&#x9;&#x9;*/ check if new data is valid /*
&#x9;&#x9;if ((s.enabled != 0 &#x26;&#x26; s.enabled != 1) ||
&#x9;&#x9;    (s.log_passwd != 0 &#x26;&#x26; s.log_passwd != 1))
&#x9;&#x9;&#x9;err = -EINVAL;

&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;t = READ_ONCE(current-&#x3E;signal-&#x3E;audit_tty);
&#x9;&#x9;else {
&#x9;&#x9;&#x9;t = s.enabled | (-s.log_passwd &#x26; AUDIT_TTY_LOG_PASSWD);
&#x9;&#x9;&#x9;t = xchg(&#x26;current-&#x3E;signal-&#x3E;audit_tty, t);
&#x9;&#x9;}
&#x9;&#x9;old.enabled = t &#x26; AUDIT_TTY_ENABLE;
&#x9;&#x9;old.log_passwd = !!(t &#x26; AUDIT_TTY_LOG_PASSWD);

&#x9;&#x9;audit_log_common_recv_msg(&#x26;ab, AUDIT_CONFIG_CHANGE);
&#x9;&#x9;audit_log_format(ab, &#x22; op=tty_set old-enabled=%d new-enabled=%d&#x22;
&#x9;&#x9;&#x9;&#x9; &#x22; old-log_passwd=%d new-log_passwd=%d res=%d&#x22;,
&#x9;&#x9;&#x9;&#x9; old.enabled, s.enabled, old.log_passwd,
&#x9;&#x9;&#x9;&#x9; s.log_passwd, !err);
&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;break;
&#x9;}
&#x9;default:
&#x9;&#x9;err = -EINVAL;
&#x9;&#x9;break;
&#x9;}

&#x9;return err &#x3C; 0 ? err : 0;
}

*/
 Get message from skb.  Each message is processed by audit_receive_msg.
 Malformed skbs with wrong length are discarded silently.
 /*
static void audit_receive_skb(struct sk_buffskb)
{
&#x9;struct nlmsghdrnlh;
&#x9;*/
&#x9; len MUST be signed for nlmsg_next to be able to dec it below 0
&#x9; if the nlmsg_len was not aligned
&#x9; /*
&#x9;int len;
&#x9;int err;

&#x9;nlh = nlmsg_hdr(skb);
&#x9;len = skb-&#x3E;len;

&#x9;while (nlmsg_ok(nlh, len)) {
&#x9;&#x9;err = audit_receive_msg(skb, nlh);
&#x9;&#x9;*/ if err or if this message says it wants a response /*
&#x9;&#x9;if (err || (nlh-&#x3E;nlmsg_flags &#x26; NLM_F_ACK))
&#x9;&#x9;&#x9;netlink_ack(skb, nlh, err);

&#x9;&#x9;nlh = nlmsg_next(nlh, &#x26;len);
&#x9;}
}

*/ Receive messages from netlink socket. /*
static void audit_receive(struct sk_buff skb)
{
&#x9;mutex_lock(&#x26;audit_cmd_mutex);
&#x9;audit_receive_skb(skb);
&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
}

*/ Run custom bind function on netlink socket group connect or bind requests. /*
static int audit_bind(struct netnet, int group)
{
&#x9;if (!capable(CAP_AUDIT_READ))
&#x9;&#x9;return -EPERM;

&#x9;return 0;
}

static int __net_init audit_net_init(struct netnet)
{
&#x9;struct netlink_kernel_cfg cfg = {
&#x9;&#x9;.input&#x9;= audit_receive,
&#x9;&#x9;.bind&#x9;= audit_bind,
&#x9;&#x9;.flags&#x9;= NL_CFG_F_NONROOT_RECV,
&#x9;&#x9;.groups&#x9;= AUDIT_NLGRP_MAX,
&#x9;};

&#x9;struct audit_netaunet = net_generic(net, audit_net_id);

&#x9;aunet-&#x3E;nlsk = netlink_kernel_create(net, NETLINK_AUDIT, &#x26;cfg);
&#x9;if (aunet-&#x3E;nlsk == NULL) {
&#x9;&#x9;audit_panic(&#x22;cannot initialize netlink socket in namespace&#x22;);
&#x9;&#x9;return -ENOMEM;
&#x9;}
&#x9;aunet-&#x3E;nlsk-&#x3E;sk_sndtimeo = MAX_SCHEDULE_TIMEOUT;
&#x9;return 0;
}

static void __net_exit audit_net_exit(struct netnet)
{
&#x9;struct audit_netaunet = net_generic(net, audit_net_id);
&#x9;struct socksock = aunet-&#x3E;nlsk;
&#x9;if (sock == audit_sock) {
&#x9;&#x9;audit_pid = 0;
&#x9;&#x9;audit_sock = NULL;
&#x9;}

&#x9;RCU_INIT_POINTER(aunet-&#x3E;nlsk, NULL);
&#x9;synchronize_net();
&#x9;netlink_kernel_release(sock);
}

static struct pernet_operations audit_net_ops __net_initdata = {
&#x9;.init = audit_net_init,
&#x9;.exit = audit_net_exit,
&#x9;.id = &#x26;audit_net_id,
&#x9;.size = sizeof(struct audit_net),
};

*/ Initialize audit support at boot time. /*
static int __init audit_init(void)
{
&#x9;int i;

&#x9;if (audit_initialized == AUDIT_DISABLED)
&#x9;&#x9;return 0;

&#x9;pr_info(&#x22;initializing netlink subsys (%s)\n&#x22;,
&#x9;&#x9;audit_default ? &#x22;enabled&#x22; : &#x22;disabled&#x22;);
&#x9;register_pernet_subsys(&#x26;audit_net_ops);

&#x9;skb_queue_head_init(&#x26;audit_skb_queue);
&#x9;skb_queue_head_init(&#x26;audit_skb_hold_queue);
&#x9;audit_initialized = AUDIT_INITIALIZED;
&#x9;audit_enabled = audit_default;
&#x9;audit_ever_enabled |= !!audit_default;

&#x9;audit_log(NULL, GFP_KERNEL, AUDIT_KERNEL, &#x22;initialized&#x22;);

&#x9;for (i = 0; i &#x3C; AUDIT_INODE_BUCKETS; i++)
&#x9;&#x9;INIT_LIST_HEAD(&#x26;audit_inode_hash[i]);

&#x9;return 0;
}
__initcall(audit_init);

*/ Process kernel command-line parameter at boot time.  audit=0 or audit=1. /*
static int __init audit_enable(charstr)
{
&#x9;audit_default = !!simple_strtol(str, NULL, 0);
&#x9;if (!audit_default)
&#x9;&#x9;audit_initialized = AUDIT_DISABLED;

&#x9;pr_info(&#x22;%s\n&#x22;, audit_default ?
&#x9;&#x9;&#x22;enabled (after initialization)&#x22; : &#x22;disabled (until reboot)&#x22;);

&#x9;return 1;
}
__setup(&#x22;audit=&#x22;, audit_enable);

*/ Process kernel command-line parameter at boot time.
 audit_backlog_limit=&#x3C;n&#x3E; /*
static int __init audit_backlog_limit_set(charstr)
{
&#x9;u32 audit_backlog_limit_arg;

&#x9;pr_info(&#x22;audit_backlog_limit: &#x22;);
&#x9;if (kstrtouint(str, 0, &#x26;audit_backlog_limit_arg)) {
&#x9;&#x9;pr_cont(&#x22;using default of %u, unable to parse %s\n&#x22;,
&#x9;&#x9;&#x9;audit_backlog_limit, str);
&#x9;&#x9;return 1;
&#x9;}

&#x9;audit_backlog_limit = audit_backlog_limit_arg;
&#x9;pr_cont(&#x22;%d\n&#x22;, audit_backlog_limit);

&#x9;return 1;
}
__setup(&#x22;audit_backlog_limit=&#x22;, audit_backlog_limit_set);

static void audit_buffer_free(struct audit_bufferab)
{
&#x9;unsigned long flags;

&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;kfree_skb(ab-&#x3E;skb);
&#x9;spin_lock_irqsave(&#x26;audit_freelist_lock, flags);
&#x9;if (audit_freelist_count &#x3E; AUDIT_MAXFREE)
&#x9;&#x9;kfree(ab);
&#x9;else {
&#x9;&#x9;audit_freelist_count++;
&#x9;&#x9;list_add(&#x26;ab-&#x3E;list, &#x26;audit_freelist);
&#x9;}
&#x9;spin_unlock_irqrestore(&#x26;audit_freelist_lock, flags);
}

static struct audit_buffer audit_buffer_alloc(struct audit_contextctx,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;gfp_t gfp_mask, int type)
{
&#x9;unsigned long flags;
&#x9;struct audit_bufferab = NULL;
&#x9;struct nlmsghdrnlh;

&#x9;spin_lock_irqsave(&#x26;audit_freelist_lock, flags);
&#x9;if (!list_empty(&#x26;audit_freelist)) {
&#x9;&#x9;ab = list_entry(audit_freelist.next,
&#x9;&#x9;&#x9;&#x9;struct audit_buffer, list);
&#x9;&#x9;list_del(&#x26;ab-&#x3E;list);
&#x9;&#x9;--audit_freelist_count;
&#x9;}
&#x9;spin_unlock_irqrestore(&#x26;audit_freelist_lock, flags);

&#x9;if (!ab) {
&#x9;&#x9;ab = kmalloc(sizeof(*ab), gfp_mask);
&#x9;&#x9;if (!ab)
&#x9;&#x9;&#x9;goto err;
&#x9;}

&#x9;ab-&#x3E;ctx = ctx;
&#x9;ab-&#x3E;gfp_mask = gfp_mask;

&#x9;ab-&#x3E;skb = nlmsg_new(AUDIT_BUFSIZ, gfp_mask);
&#x9;if (!ab-&#x3E;skb)
&#x9;&#x9;goto err;

&#x9;nlh = nlmsg_put(ab-&#x3E;skb, 0, 0, type, 0, 0);
&#x9;if (!nlh)
&#x9;&#x9;goto out_kfree_skb;

&#x9;return ab;

out_kfree_skb:
&#x9;kfree_skb(ab-&#x3E;skb);
&#x9;ab-&#x3E;skb = NULL;
err:
&#x9;audit_buffer_free(ab);
&#x9;return NULL;
}

*/
 audit_serial - compute a serial number for the audit record

 Compute a serial number for the audit record.  Audit records are
 written to user-space as soon as they are generated, so a complete
 audit record may be written in several pieces.  The timestamp of the
 record and this serial number are used by the user-space tools to
 determine which pieces belong to the same audit record.  The
 (timestamp,serial) tuple is unique for each syscall and is live from
 syscall entry to syscall exit.

 NOTE: Another possibility is to store the formatted records off the
 audit context (for those records that have a context), and emit them
 all at syscall exit.  However, this could delay the reporting of
 significant errors until syscall exit (or never, if the system
 halts).
 /*
unsigned int audit_serial(void)
{
&#x9;static atomic_t serial = ATOMIC_INIT(0);

&#x9;return atomic_add_return(1, &#x26;serial);
}

static inline void audit_get_stamp(struct audit_contextctx,
&#x9;&#x9;&#x9;&#x9;   struct timespect, unsigned intserial)
{
&#x9;if (!ctx || !auditsc_get_stamp(ctx, t, serial)) {
&#x9;&#x9;*t = CURRENT_TIME;
&#x9;&#x9;*serial = audit_serial();
&#x9;}
}

*/
 Wait for auditd to drain the queue a little
 /*
static long wait_for_auditd(long sleep_time)
{
&#x9;DECLARE_WAITQUEUE(wait, current);
&#x9;set_current_state(TASK_UNINTERRUPTIBLE);
&#x9;add_wait_queue_exclusive(&#x26;audit_backlog_wait, &#x26;wait);

&#x9;if (audit_backlog_limit &#x26;&#x26;
&#x9;    skb_queue_len(&#x26;audit_skb_queue) &#x3E; audit_backlog_limit)
&#x9;&#x9;sleep_time = schedule_timeout(sleep_time);

&#x9;__set_current_state(TASK_RUNNING);
&#x9;remove_wait_queue(&#x26;audit_backlog_wait, &#x26;wait);

&#x9;return sleep_time;
}

*/
 audit_log_start - obtain an audit buffer
 @ctx: audit_context (may be NULL)
 @gfp_mask: type of allocation
 @type: audit message type

 Returns audit_buffer pointer on success or NULL on error.

 Obtain an audit buffer.  This routine does locking to obtain the
 audit buffer, but then no locking is required for calls to
 audit_log_*format.  If the task (ctx) is a task that is currently in a
 syscall, then the syscall is marked as auditable and an audit record
 will be written at syscall exit.  If there is no associated task, then
 task context (ctx) should be NULL.
 /*
struct audit_bufferaudit_log_start(struct audit_contextctx, gfp_t gfp_mask,
&#x9;&#x9;&#x9;&#x9;     int type)
{
&#x9;struct audit_buffer&#x9;*ab&#x9;= NULL;
&#x9;struct timespec&#x9;&#x9;t;
&#x9;unsigned int&#x9;&#x9;uninitialized_var(serial);
&#x9;int reserve = 5;/ Allow atomic callers to go up to five
&#x9;&#x9;&#x9;    entries over the normal backlog limit /*
&#x9;unsigned long timeout_start = jiffies;

&#x9;if (audit_initialized != AUDIT_INITIALIZED)
&#x9;&#x9;return NULL;

&#x9;if (unlikely(audit_filter_type(type)))
&#x9;&#x9;return NULL;

&#x9;if (gfp_mask &#x26; __GFP_DIRECT_RECLAIM) {
&#x9;&#x9;if (audit_pid &#x26;&#x26; audit_pid == current-&#x3E;tgid)
&#x9;&#x9;&#x9;gfp_mask &#x26;= ~__GFP_DIRECT_RECLAIM;
&#x9;&#x9;else
&#x9;&#x9;&#x9;reserve = 0;
&#x9;}

&#x9;while (audit_backlog_limit
&#x9;       &#x26;&#x26; skb_queue_len(&#x26;audit_skb_queue) &#x3E; audit_backlog_limit + reserve) {
&#x9;&#x9;if (gfp_mask &#x26; __GFP_DIRECT_RECLAIM &#x26;&#x26; audit_backlog_wait_time) {
&#x9;&#x9;&#x9;long sleep_time;

&#x9;&#x9;&#x9;sleep_time = timeout_start + audit_backlog_wait_time - jiffies;
&#x9;&#x9;&#x9;if (sleep_time &#x3E; 0) {
&#x9;&#x9;&#x9;&#x9;sleep_time = wait_for_auditd(sleep_time);
&#x9;&#x9;&#x9;&#x9;if (sleep_time &#x3E; 0)
&#x9;&#x9;&#x9;&#x9;&#x9;continue;
&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;&#x9;if (audit_rate_check() &#x26;&#x26; printk_ratelimit())
&#x9;&#x9;&#x9;pr_warn(&#x22;audit_backlog=%d &#x3E; audit_backlog_limit=%d\n&#x22;,
&#x9;&#x9;&#x9;&#x9;skb_queue_len(&#x26;audit_skb_queue),
&#x9;&#x9;&#x9;&#x9;audit_backlog_limit);
&#x9;&#x9;audit_log_lost(&#x22;backlog limit exceeded&#x22;);
&#x9;&#x9;audit_backlog_wait_time = 0;
&#x9;&#x9;wake_up(&#x26;audit_backlog_wait);
&#x9;&#x9;return NULL;
&#x9;}

&#x9;if (!reserve &#x26;&#x26; !audit_backlog_wait_time)
&#x9;&#x9;audit_backlog_wait_time = audit_backlog_wait_time_master;

&#x9;ab = audit_buffer_alloc(ctx, gfp_mask, type);
&#x9;if (!ab) {
&#x9;&#x9;audit_log_lost(&#x22;out of memory in audit_log_start&#x22;);
&#x9;&#x9;return NULL;
&#x9;}

&#x9;audit_get_stamp(ab-&#x3E;ctx, &#x26;t, &#x26;serial);

&#x9;audit_log_format(ab, &#x22;audit(%lu.%03lu:%u): &#x22;,
&#x9;&#x9;&#x9; t.tv_sec, t.tv_nsec/1000000, serial);
&#x9;return ab;
}

*/
 audit_expand - expand skb in the audit buffer
 @ab: audit_buffer
 @extra: space to add at tail of the skb

 Returns 0 (no space) on failed expansion, or available space if
 successful.
 /*
static inline int audit_expand(struct audit_bufferab, int extra)
{
&#x9;struct sk_buffskb = ab-&#x3E;skb;
&#x9;int oldtail = skb_tailroom(skb);
&#x9;int ret = pskb_expand_head(skb, 0, extra, ab-&#x3E;gfp_mask);
&#x9;int newtail = skb_tailroom(skb);

&#x9;if (ret &#x3C; 0) {
&#x9;&#x9;audit_log_lost(&#x22;out of memory in audit_expand&#x22;);
&#x9;&#x9;return 0;
&#x9;}

&#x9;skb-&#x3E;truesize += newtail - oldtail;
&#x9;return newtail;
}

*/
 Format an audit message into the audit buffer.  If there isn&#x27;t enough
 room in the audit buffer, more room will be allocated and vsnprint
 will be called a second time.  Currently, we assume that a printk
 can&#x27;t format message larger than 1024 bytes, so we don&#x27;t either.
 /*
static void audit_log_vformat(struct audit_bufferab, const charfmt,
&#x9;&#x9;&#x9;      va_list args)
{
&#x9;int len, avail;
&#x9;struct sk_buffskb;
&#x9;va_list args2;

&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;BUG_ON(!ab-&#x3E;skb);
&#x9;skb = ab-&#x3E;skb;
&#x9;avail = skb_tailroom(skb);
&#x9;if (avail == 0) {
&#x9;&#x9;avail = audit_expand(ab, AUDIT_BUFSIZ);
&#x9;&#x9;if (!avail)
&#x9;&#x9;&#x9;goto out;
&#x9;}
&#x9;va_copy(args2, args);
&#x9;len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args);
&#x9;if (len &#x3E;= avail) {
&#x9;&#x9;*/ The printk buffer is 1024 bytes long, so if we get
&#x9;&#x9; here and AUDIT_BUFSIZ is at least 1024, then we can
&#x9;&#x9; log everything that printk could have logged. /*
&#x9;&#x9;avail = audit_expand(ab,
&#x9;&#x9;&#x9;max_t(unsigned, AUDIT_BUFSIZ, 1+len-avail));
&#x9;&#x9;if (!avail)
&#x9;&#x9;&#x9;goto out_va_end;
&#x9;&#x9;len = vsnprintf(skb_tail_pointer(skb), avail, fmt, args2);
&#x9;}
&#x9;if (len &#x3E; 0)
&#x9;&#x9;skb_put(skb, len);
out_va_end:
&#x9;va_end(args2);
out:
&#x9;return;
}

*/
 audit_log_format - format a message into the audit buffer.
 @ab: audit_buffer
 @fmt: format string
 @...: optional parameters matching @fmt string

 All the work is done in audit_log_vformat.
 /*
void audit_log_format(struct audit_bufferab, const charfmt, ...)
{
&#x9;va_list args;

&#x9;if (!ab)
&#x9;&#x9;return;
&#x9;va_start(args, fmt);
&#x9;audit_log_vformat(ab, fmt, args);
&#x9;va_end(args);
}

*/
 audit_log_hex - convert a buffer to hex and append it to the audit skb
 @ab: the audit_buffer
 @buf: buffer to convert to hex
 @len: length of @buf to be converted

 No return value; failure to expand is silently ignored.

 This function will take the passed buf and convert it into a string of
 ascii hex digits. The new string is placed onto the skb.
 /*
void audit_log_n_hex(struct audit_bufferab, const unsigned charbuf,
&#x9;&#x9;size_t len)
{
&#x9;int i, avail, new_len;
&#x9;unsigned charptr;
&#x9;struct sk_buffskb;

&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;BUG_ON(!ab-&#x3E;skb);
&#x9;skb = ab-&#x3E;skb;
&#x9;avail = skb_tailroom(skb);
&#x9;new_len = len&#x3C;&#x3C;1;
&#x9;if (new_len &#x3E;= avail) {
&#x9;&#x9;*/ Round the buffer request up to the next multiple /*
&#x9;&#x9;new_len = AUDIT_BUFSIZ*(((new_len-avail)/AUDIT_BUFSIZ) + 1);
&#x9;&#x9;avail = audit_expand(ab, new_len);
&#x9;&#x9;if (!avail)
&#x9;&#x9;&#x9;return;
&#x9;}

&#x9;ptr = skb_tail_pointer(skb);
&#x9;for (i = 0; i &#x3C; len; i++)
&#x9;&#x9;ptr = hex_byte_pack_upper(ptr, buf[i]);
&#x9;*ptr = 0;
&#x9;skb_put(skb, len &#x3C;&#x3C; 1);/ new string is twice the old string /*
}

*/
 Format a string of no more than slen characters into the audit buffer,
 enclosed in quote marks.
 /*
void audit_log_n_string(struct audit_bufferab, const charstring,
&#x9;&#x9;&#x9;size_t slen)
{
&#x9;int avail, new_len;
&#x9;unsigned charptr;
&#x9;struct sk_buffskb;

&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;BUG_ON(!ab-&#x3E;skb);
&#x9;skb = ab-&#x3E;skb;
&#x9;avail = skb_tailroom(skb);
&#x9;new_len = slen + 3;&#x9;*/ enclosing quotes + null terminator /*
&#x9;if (new_len &#x3E; avail) {
&#x9;&#x9;avail = audit_expand(ab, new_len);
&#x9;&#x9;if (!avail)
&#x9;&#x9;&#x9;return;
&#x9;}
&#x9;ptr = skb_tail_pointer(skb);
&#x9;*ptr++ = &#x27;&#x22;&#x27;;
&#x9;memcpy(ptr, string, slen);
&#x9;ptr += slen;
&#x9;*ptr++ = &#x27;&#x22;&#x27;;
&#x9;*ptr = 0;
&#x9;skb_put(skb, slen + 2);&#x9;*/ don&#x27;t include null terminator /*
}

*/
 audit_string_contains_control - does a string need to be logged in hex
 @string: string to be checked
 @len: max length of the string to check
 /*
bool audit_string_contains_control(const charstring, size_t len)
{
&#x9;const unsigned charp;
&#x9;for (p = string; p &#x3C; (const unsigned char)string + len; p++) {
&#x9;&#x9;if (*p == &#x27;&#x22;&#x27; ||p &#x3C; 0x21 ||p &#x3E; 0x7e)
&#x9;&#x9;&#x9;return true;
&#x9;}
&#x9;return false;
}

*/
 audit_log_n_untrustedstring - log a string that may contain random characters
 @ab: audit_buffer
 @len: length of string (not including trailing null)
 @string: string to be logged

 This code will escape a string that is passed to it if the string
 contains a control character, unprintable character, double quote mark,
 or a space. Unescaped strings will start and end with a double quote mark.
 Strings that are escaped are printed in hex (2 digits per char).

 The caller specifies the number of characters in the string to log, which may
 or may not be the entire string.
 /*
void audit_log_n_untrustedstring(struct audit_bufferab, const charstring,
&#x9;&#x9;&#x9;&#x9; size_t len)
{
&#x9;if (audit_string_contains_control(string, len))
&#x9;&#x9;audit_log_n_hex(ab, string, len);
&#x9;else
&#x9;&#x9;audit_log_n_string(ab, string, len);
}

*/
 audit_log_untrustedstring - log a string that may contain random characters
 @ab: audit_buffer
 @string: string to be logged

 Same as audit_log_n_untrustedstring(), except that strlen is used to
 determine string length.
 /*
void audit_log_untrustedstring(struct audit_bufferab, const charstring)
{
&#x9;audit_log_n_untrustedstring(ab, string, strlen(string));
}

*/ This is a helper-function to print the escaped d_path /*
void audit_log_d_path(struct audit_bufferab, const charprefix,
&#x9;&#x9;      const struct pathpath)
{
&#x9;charp,pathname;

&#x9;if (prefix)
&#x9;&#x9;audit_log_format(ab, &#x22;%s&#x22;, prefix);

&#x9;*/ We will allow 11 spaces for &#x27; (deleted)&#x27; to be appended /*
&#x9;pathname = kmalloc(PATH_MAX+11, ab-&#x3E;gfp_mask);
&#x9;if (!pathname) {
&#x9;&#x9;audit_log_string(ab, &#x22;&#x3C;no_memory&#x3E;&#x22;);
&#x9;&#x9;return;
&#x9;}
&#x9;p = d_path(path, pathname, PATH_MAX+11);
&#x9;if (IS_ERR(p)) {/ Should never happen since we send PATH_MAX /*
&#x9;&#x9;*/ FIXME: can we save some information here? /*
&#x9;&#x9;audit_log_string(ab, &#x22;&#x3C;too_long&#x3E;&#x22;);
&#x9;} else
&#x9;&#x9;audit_log_untrustedstring(ab, p);
&#x9;kfree(pathname);
}

void audit_log_session_info(struct audit_bufferab)
{
&#x9;unsigned int sessionid = audit_get_sessionid(current);
&#x9;uid_t auid = from_kuid(&#x26;init_user_ns, audit_get_loginuid(current));

&#x9;audit_log_format(ab, &#x22; auid=%u ses=%u&#x22;, auid, sessionid);
}

void audit_log_key(struct audit_bufferab, charkey)
{
&#x9;audit_log_format(ab, &#x22; key=&#x22;);
&#x9;if (key)
&#x9;&#x9;audit_log_untrustedstring(ab, key);
&#x9;else
&#x9;&#x9;audit_log_format(ab, &#x22;(null)&#x22;);
}

void audit_log_cap(struct audit_bufferab, charprefix, kernel_cap_tcap)
{
&#x9;int i;

&#x9;audit_log_format(ab, &#x22; %s=&#x22;, prefix);
&#x9;CAP_FOR_EACH_U32(i) {
&#x9;&#x9;audit_log_format(ab, &#x22;%08x&#x22;,
&#x9;&#x9;&#x9;&#x9; cap-&#x3E;cap[CAP_LAST_U32 - i]);
&#x9;}
}

static void audit_log_fcaps(struct audit_bufferab, struct audit_namesname)
{
&#x9;kernel_cap_tperm = &#x26;name-&#x3E;fcap.permitted;
&#x9;kernel_cap_tinh = &#x26;name-&#x3E;fcap.inheritable;
&#x9;int log = 0;

&#x9;if (!cap_isclear(*perm)) {
&#x9;&#x9;audit_log_cap(ab, &#x22;cap_fp&#x22;, perm);
&#x9;&#x9;log = 1;
&#x9;}
&#x9;if (!cap_isclear(*inh)) {
&#x9;&#x9;audit_log_cap(ab, &#x22;cap_fi&#x22;, inh);
&#x9;&#x9;log = 1;
&#x9;}

&#x9;if (log)
&#x9;&#x9;audit_log_format(ab, &#x22; cap_fe=%d cap_fver=%x&#x22;,
&#x9;&#x9;&#x9;&#x9; name-&#x3E;fcap.fE, name-&#x3E;fcap_ver);
}

static inline int audit_copy_fcaps(struct audit_namesname,
&#x9;&#x9;&#x9;&#x9;   const struct dentrydentry)
{
&#x9;struct cpu_vfs_cap_data caps;
&#x9;int rc;

&#x9;if (!dentry)
&#x9;&#x9;return 0;

&#x9;rc = get_vfs_caps_from_disk(dentry, &#x26;caps);
&#x9;if (rc)
&#x9;&#x9;return rc;

&#x9;name-&#x3E;fcap.permitted = caps.permitted;
&#x9;name-&#x3E;fcap.inheritable = caps.inheritable;
&#x9;name-&#x3E;fcap.fE = !!(caps.magic_etc &#x26; VFS_CAP_FLAGS_EFFECTIVE);
&#x9;name-&#x3E;fcap_ver = (caps.magic_etc &#x26; VFS_CAP_REVISION_MASK) &#x3E;&#x3E;
&#x9;&#x9;&#x9;&#x9;VFS_CAP_REVISION_SHIFT;

&#x9;return 0;
}

*/ Copy inode data into an audit_names. /*
void audit_copy_inode(struct audit_namesname, const struct dentrydentry,
&#x9;&#x9;      struct inodeinode)
{
&#x9;name-&#x3E;ino   = inode-&#x3E;i_ino;
&#x9;name-&#x3E;dev   = inode-&#x3E;i_sb-&#x3E;s_dev;
&#x9;name-&#x3E;mode  = inode-&#x3E;i_mode;
&#x9;name-&#x3E;uid   = inode-&#x3E;i_uid;
&#x9;name-&#x3E;gid   = inode-&#x3E;i_gid;
&#x9;name-&#x3E;rdev  = inode-&#x3E;i_rdev;
&#x9;security_inode_getsecid(inode, &#x26;name-&#x3E;osid);
&#x9;audit_copy_fcaps(name, dentry);
}

*/
 audit_log_name - produce AUDIT_PATH record from struct audit_names
 @context: audit_context for the task
 @n: audit_names structure with reportable details
 @path: optional path to report instead of audit_names-&#x3E;name
 @record_num: record number to report when handling a list of names
 @call_panic: optional pointer to int that will be updated if secid fails
 /*
void audit_log_name(struct audit_contextcontext, struct audit_namesn,
&#x9;&#x9;    struct pathpath, int record_num, intcall_panic)
{
&#x9;struct audit_bufferab;
&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_PATH);
&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;audit_log_format(ab, &#x22;item=%d&#x22;, record_num);

&#x9;if (path)
&#x9;&#x9;audit_log_d_path(ab, &#x22; name=&#x22;, path);
&#x9;else if (n-&#x3E;name) {
&#x9;&#x9;switch (n-&#x3E;name_len) {
&#x9;&#x9;case AUDIT_NAME_FULL:
&#x9;&#x9;&#x9;*/ log the full path /*
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; name=&#x22;);
&#x9;&#x9;&#x9;audit_log_untrustedstring(ab, n-&#x3E;name-&#x3E;name);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case 0:
&#x9;&#x9;&#x9;*/ name was specified as a relative path and the
&#x9;&#x9;&#x9; directory component is the cwd /*
&#x9;&#x9;&#x9;audit_log_d_path(ab, &#x22; name=&#x22;, &#x26;context-&#x3E;pwd);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;default:
&#x9;&#x9;&#x9;*/ log the name&#x27;s directory component /*
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; name=&#x22;);
&#x9;&#x9;&#x9;audit_log_n_untrustedstring(ab, n-&#x3E;name-&#x3E;name,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;    n-&#x3E;name_len);
&#x9;&#x9;}
&#x9;} else
&#x9;&#x9;audit_log_format(ab, &#x22; name=(null)&#x22;);

&#x9;if (n-&#x3E;ino != AUDIT_INO_UNSET)
&#x9;&#x9;audit_log_format(ab, &#x22; inode=%lu&#x22;
&#x9;&#x9;&#x9;&#x9; &#x22; dev=%02x:%02x mode=%#ho&#x22;
&#x9;&#x9;&#x9;&#x9; &#x22; ouid=%u ogid=%u rdev=%02x:%02x&#x22;,
&#x9;&#x9;&#x9;&#x9; n-&#x3E;ino,
&#x9;&#x9;&#x9;&#x9; MAJOR(n-&#x3E;dev),
&#x9;&#x9;&#x9;&#x9; MINOR(n-&#x3E;dev),
&#x9;&#x9;&#x9;&#x9; n-&#x3E;mode,
&#x9;&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, n-&#x3E;uid),
&#x9;&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, n-&#x3E;gid),
&#x9;&#x9;&#x9;&#x9; MAJOR(n-&#x3E;rdev),
&#x9;&#x9;&#x9;&#x9; MINOR(n-&#x3E;rdev));
&#x9;if (n-&#x3E;osid != 0) {
&#x9;&#x9;charctx = NULL;
&#x9;&#x9;u32 len;
&#x9;&#x9;if (security_secid_to_secctx(
&#x9;&#x9;&#x9;n-&#x3E;osid, &#x26;ctx, &#x26;len)) {
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; osid=%u&#x22;, n-&#x3E;osid);
&#x9;&#x9;&#x9;if (call_panic)
&#x9;&#x9;&#x9;&#x9;*call_panic = 2;
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, ctx);
&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
&#x9;&#x9;}
&#x9;}

&#x9;*/ log the audit_names record type /*
&#x9;audit_log_format(ab, &#x22; nametype=&#x22;);
&#x9;switch(n-&#x3E;type) {
&#x9;case AUDIT_TYPE_NORMAL:
&#x9;&#x9;audit_log_format(ab, &#x22;NORMAL&#x22;);
&#x9;&#x9;break;
&#x9;case AUDIT_TYPE_PARENT:
&#x9;&#x9;audit_log_format(ab, &#x22;PARENT&#x22;);
&#x9;&#x9;break;
&#x9;case AUDIT_TYPE_CHILD_DELETE:
&#x9;&#x9;audit_log_format(ab, &#x22;DELETE&#x22;);
&#x9;&#x9;break;
&#x9;case AUDIT_TYPE_CHILD_CREATE:
&#x9;&#x9;audit_log_format(ab, &#x22;CREATE&#x22;);
&#x9;&#x9;break;
&#x9;default:
&#x9;&#x9;audit_log_format(ab, &#x22;UNKNOWN&#x22;);
&#x9;&#x9;break;
&#x9;}

&#x9;audit_log_fcaps(ab, n);
&#x9;audit_log_end(ab);
}

int audit_log_task_context(struct audit_bufferab)
{
&#x9;charctx = NULL;
&#x9;unsigned len;
&#x9;int error;
&#x9;u32 sid;

&#x9;security_task_getsecid(current, &#x26;sid);
&#x9;if (!sid)
&#x9;&#x9;return 0;

&#x9;error = security_secid_to_secctx(sid, &#x26;ctx, &#x26;len);
&#x9;if (error) {
&#x9;&#x9;if (error != -EINVAL)
&#x9;&#x9;&#x9;goto error_path;
&#x9;&#x9;return 0;
&#x9;}

&#x9;audit_log_format(ab, &#x22; subj=%s&#x22;, ctx);
&#x9;security_release_secctx(ctx, len);
&#x9;return 0;

error_path:
&#x9;audit_panic(&#x22;error in audit_log_task_context&#x22;);
&#x9;return error;
}
EXPORT_SYMBOL(audit_log_task_context);

void audit_log_d_path_exe(struct audit_bufferab,
&#x9;&#x9;&#x9;  struct mm_structmm)
{
&#x9;struct fileexe_file;

&#x9;if (!mm)
&#x9;&#x9;goto out_null;

&#x9;exe_file = get_mm_exe_file(mm);
&#x9;if (!exe_file)
&#x9;&#x9;goto out_null;

&#x9;audit_log_d_path(ab, &#x22; exe=&#x22;, &#x26;exe_file-&#x3E;f_path);
&#x9;fput(exe_file);
&#x9;return;
out_null:
&#x9;audit_log_format(ab, &#x22; exe=(null)&#x22;);
}

void audit_log_task_info(struct audit_bufferab, struct task_structtsk)
{
&#x9;const struct credcred;
&#x9;char comm[sizeof(tsk-&#x3E;comm)];
&#x9;chartty;

&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;*/ tsk == current /*
&#x9;cred = current_cred();

&#x9;spin_lock_irq(&#x26;tsk-&#x3E;sighand-&#x3E;siglock);
&#x9;if (tsk-&#x3E;signal &#x26;&#x26; tsk-&#x3E;signal-&#x3E;tty &#x26;&#x26; tsk-&#x3E;signal-&#x3E;tty-&#x3E;name)
&#x9;&#x9;tty = tsk-&#x3E;signal-&#x3E;tty-&#x3E;name;
&#x9;else
&#x9;&#x9;tty = &#x22;(none)&#x22;;
&#x9;spin_unlock_irq(&#x26;tsk-&#x3E;sighand-&#x3E;siglock);

&#x9;audit_log_format(ab,
&#x9;&#x9;&#x9; &#x22; ppid=%d pid=%d auid=%u uid=%u gid=%u&#x22;
&#x9;&#x9;&#x9; &#x22; euid=%u suid=%u fsuid=%u&#x22;
&#x9;&#x9;&#x9; &#x22; egid=%u sgid=%u fsgid=%u tty=%s ses=%u&#x22;,
&#x9;&#x9;&#x9; task_ppid_nr(tsk),
&#x9;&#x9;&#x9; task_pid_nr(tsk),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, audit_get_loginuid(tsk)),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;uid),
&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;gid),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;euid),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;suid),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, cred-&#x3E;fsuid),
&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;egid),
&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;sgid),
&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, cred-&#x3E;fsgid),
&#x9;&#x9;&#x9; tty, audit_get_sessionid(tsk));

&#x9;audit_log_format(ab, &#x22; comm=&#x22;);
&#x9;audit_log_untrustedstring(ab, get_task_comm(comm, tsk));

&#x9;audit_log_d_path_exe(ab, tsk-&#x3E;mm);
&#x9;audit_log_task_context(ab);
}
EXPORT_SYMBOL(audit_log_task_info);

*/
 audit_log_link_denied - report a link restriction denial
 @operation: specific link operation
 @link: the path that triggered the restriction
 /*
void audit_log_link_denied(const charoperation, struct pathlink)
{
&#x9;struct audit_bufferab;
&#x9;struct audit_namesname;

&#x9;name = kzalloc(sizeof(*name), GFP_NOFS);
&#x9;if (!name)
&#x9;&#x9;return;

&#x9;*/ Generate AUDIT_ANOM_LINK with subject, operation, outcome. /*
&#x9;ab = audit_log_start(current-&#x3E;audit_context, GFP_KERNEL,
&#x9;&#x9;&#x9;     AUDIT_ANOM_LINK);
&#x9;if (!ab)
&#x9;&#x9;goto out;
&#x9;audit_log_format(ab, &#x22;op=%s&#x22;, operation);
&#x9;audit_log_task_info(ab, current);
&#x9;audit_log_format(ab, &#x22; res=0&#x22;);
&#x9;audit_log_end(ab);

&#x9;*/ Generate AUDIT_PATH record with object. /*
&#x9;name-&#x3E;type = AUDIT_TYPE_NORMAL;
&#x9;audit_copy_inode(name, link-&#x3E;dentry, d_backing_inode(link-&#x3E;dentry));
&#x9;audit_log_name(current-&#x3E;audit_context, name, link, 0, NULL);
out:
&#x9;kfree(name);
}

*/
 audit_log_end - end one audit record
 @ab: the audit_buffer

 netlink_unicast() cannot be called inside an irq context because it blocks
 (last arg, flags, is not set to MSG_DONTWAIT), so the audit buffer is placed
 on a queue and a tasklet is scheduled to remove them from the queue outside
 the irq context.  May be called in any context.
 /*
void audit_log_end(struct audit_bufferab)
{
&#x9;if (!ab)
&#x9;&#x9;return;
&#x9;if (!audit_rate_check()) {
&#x9;&#x9;audit_log_lost(&#x22;rate limit exceeded&#x22;);
&#x9;} else {
&#x9;&#x9;struct nlmsghdrnlh = nlmsg_hdr(ab-&#x3E;skb);

&#x9;&#x9;nlh-&#x3E;nlmsg_len = ab-&#x3E;skb-&#x3E;len;
&#x9;&#x9;kauditd_send_multicast_skb(ab-&#x3E;skb, ab-&#x3E;gfp_mask);

&#x9;&#x9;*/
&#x9;&#x9; The original kaudit unicast socket sends up messages with
&#x9;&#x9; nlmsg_len set to the payload length rather than the entire
&#x9;&#x9; message length.  This breaks the standard set by netlink.
&#x9;&#x9; The existing auditd daemon assumes this breakage.  Fixing
&#x9;&#x9; this would require co-ordinating a change in the established
&#x9;&#x9; protocol between the kaudit kernel subsystem and the auditd
&#x9;&#x9; userspace code.
&#x9;&#x9; /*
&#x9;&#x9;nlh-&#x3E;nlmsg_len -= NLMSG_HDRLEN;

&#x9;&#x9;if (audit_pid) {
&#x9;&#x9;&#x9;skb_queue_tail(&#x26;audit_skb_queue, ab-&#x3E;skb);
&#x9;&#x9;&#x9;wake_up_interruptible(&#x26;kauditd_wait);
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;audit_printk_skb(ab-&#x3E;skb);
&#x9;&#x9;}
&#x9;&#x9;ab-&#x3E;skb = NULL;
&#x9;}
&#x9;audit_buffer_free(ab);
}

*/
 audit_log - Log an audit record
 @ctx: audit context
 @gfp_mask: type of allocation
 @type: audit message type
 @fmt: format string to use
 @...: variable parameters matching the format string

 This is a convenience function that calls audit_log_start,
 audit_log_vformat, and audit_log_end.  It may be called
 in any context.
 /*
void audit_log(struct audit_contextctx, gfp_t gfp_mask, int type,
&#x9;       const charfmt, ...)
{
&#x9;struct audit_bufferab;
&#x9;va_list args;

&#x9;ab = audit_log_start(ctx, gfp_mask, type);
&#x9;if (ab) {
&#x9;&#x9;va_start(args, fmt);
&#x9;&#x9;audit_log_vformat(ab, fmt, args);
&#x9;&#x9;va_end(args);
&#x9;&#x9;audit_log_end(ab);
&#x9;}
}

#ifdef CONFIG_SECURITY
*/
 audit_log_secctx - Converts and logs SELinux context
 @ab: audit_buffer
 @secid: security number

 This is a helper function that calls security_secid_to_secctx to convert
 secid to secctx and then adds the (converted) SELinux context to the audit
 log by calling audit_log_format, thus also preventing leak of internal secid
 to userspace. If secid cannot be converted audit_panic is called.
 /*
void audit_log_secctx(struct audit_bufferab, u32 secid)
{
&#x9;u32 len;
&#x9;charsecctx;

&#x9;if (security_secid_to_secctx(secid, &#x26;secctx, &#x26;len)) {
&#x9;&#x9;audit_panic(&#x22;Cannot convert secid to context&#x22;);
&#x9;} else {
&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, secctx);
&#x9;&#x9;security_release_secctx(secctx, len);
&#x9;}
}
EXPORT_SYMBOL(audit_log_secctx);
#endif

EXPORT_SYMBOL(audit_log_start);
EXPORT_SYMBOL(audit_log_end);
EXPORT_SYMBOL(audit_log_format);
EXPORT_SYMBOL(audit_log);
*/
 auditfilter.c -- filtering of audit events

 Copyright 2003-2004 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#define pr_fmt(fmt) KBUILD_MODNAME &#x22;: &#x22; fmt

#include &#x3C;linux/kernel.h&#x3E;
#include &#x3C;linux/audit.h&#x3E;
#include &#x3C;linux/kthread.h&#x3E;
#include &#x3C;linux/mutex.h&#x3E;
#include &#x3C;linux/fs.h&#x3E;
#include &#x3C;linux/namei.h&#x3E;
#include &#x3C;linux/netlink.h&#x3E;
#include &#x3C;linux/sched.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/security.h&#x3E;
#include &#x3C;net/net_namespace.h&#x3E;
#include &#x3C;net/sock.h&#x3E;
#include &#x22;audit.h&#x22;

*/
 Locking model:

 audit_filter_mutex:
&#x9;&#x9;Synchronizes writes and blocking reads of audit&#x27;s filterlist
&#x9;&#x9;data.  Rcu is used to traverse the filterlist and access
&#x9;&#x9;contents of structs audit_entry, audit_watch and opaque
&#x9;&#x9;LSM rules during filtering.  If modified, these structures
&#x9;&#x9;must be copied and replace their counterparts in the filterlist.
&#x9;&#x9;An audit_parent struct is not accessed during filtering, so may
&#x9;&#x9;be written directly provided audit_filter_mutex is held.
 /*

*/ Audit filter lists, defined in &#x3C;linux/audit.h&#x3E; /*
struct list_head audit_filter_list[AUDIT_NR_FILTERS] = {
&#x9;LIST_HEAD_INIT(audit_filter_list[0]),
&#x9;LIST_HEAD_INIT(audit_filter_list[1]),
&#x9;LIST_HEAD_INIT(audit_filter_list[2]),
&#x9;LIST_HEAD_INIT(audit_filter_list[3]),
&#x9;LIST_HEAD_INIT(audit_filter_list[4]),
&#x9;LIST_HEAD_INIT(audit_filter_list[5]),
#if AUDIT_NR_FILTERS != 6
#error Fix audit_filter_list initialiser
#endif
};
static struct list_head audit_rules_list[AUDIT_NR_FILTERS] = {
&#x9;LIST_HEAD_INIT(audit_rules_list[0]),
&#x9;LIST_HEAD_INIT(audit_rules_list[1]),
&#x9;LIST_HEAD_INIT(audit_rules_list[2]),
&#x9;LIST_HEAD_INIT(audit_rules_list[3]),
&#x9;LIST_HEAD_INIT(audit_rules_list[4]),
&#x9;LIST_HEAD_INIT(audit_rules_list[5]),
};

DEFINE_MUTEX(audit_filter_mutex);

static void audit_free_lsm_field(struct audit_fieldf)
{
&#x9;switch (f-&#x3E;type) {
&#x9;case AUDIT_SUBJ_USER:
&#x9;case AUDIT_SUBJ_ROLE:
&#x9;case AUDIT_SUBJ_TYPE:
&#x9;case AUDIT_SUBJ_SEN:
&#x9;case AUDIT_SUBJ_CLR:
&#x9;case AUDIT_OBJ_USER:
&#x9;case AUDIT_OBJ_ROLE:
&#x9;case AUDIT_OBJ_TYPE:
&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;&#x9;kfree(f-&#x3E;lsm_str);
&#x9;&#x9;security_audit_rule_free(f-&#x3E;lsm_rule);
&#x9;}
}

static inline void audit_free_rule(struct audit_entrye)
{
&#x9;int i;
&#x9;struct audit_kruleerule = &#x26;e-&#x3E;rule;

&#x9;*/ some rules don&#x27;t have associated watches /*
&#x9;if (erule-&#x3E;watch)
&#x9;&#x9;audit_put_watch(erule-&#x3E;watch);
&#x9;if (erule-&#x3E;fields)
&#x9;&#x9;for (i = 0; i &#x3C; erule-&#x3E;field_count; i++)
&#x9;&#x9;&#x9;audit_free_lsm_field(&#x26;erule-&#x3E;fields[i]);
&#x9;kfree(erule-&#x3E;fields);
&#x9;kfree(erule-&#x3E;filterkey);
&#x9;kfree(e);
}

void audit_free_rule_rcu(struct rcu_headhead)
{
&#x9;struct audit_entrye = container_of(head, struct audit_entry, rcu);
&#x9;audit_free_rule(e);
}

*/ Initialize an audit filterlist entry. /*
static inline struct audit_entryaudit_init_entry(u32 field_count)
{
&#x9;struct audit_entryentry;
&#x9;struct audit_fieldfields;

&#x9;entry = kzalloc(sizeof(*entry), GFP_KERNEL);
&#x9;if (unlikely(!entry))
&#x9;&#x9;return NULL;

&#x9;fields = kcalloc(field_count, sizeof(*fields), GFP_KERNEL);
&#x9;if (unlikely(!fields)) {
&#x9;&#x9;kfree(entry);
&#x9;&#x9;return NULL;
&#x9;}
&#x9;entry-&#x3E;rule.fields = fields;

&#x9;return entry;
}

*/ Unpack a filter field&#x27;s string representation from user-space
 buffer. /*
charaudit_unpack_string(void*bufp, size_tremain, size_t len)
{
&#x9;charstr;

&#x9;if (!*bufp || (len == 0) || (len &#x3E;remain))
&#x9;&#x9;return ERR_PTR(-EINVAL);

&#x9;*/ Of the currently implemented string fields, PATH_MAX
&#x9; defines the longest valid length.
&#x9; /*
&#x9;if (len &#x3E; PATH_MAX)
&#x9;&#x9;return ERR_PTR(-ENAMETOOLONG);

&#x9;str = kmalloc(len + 1, GFP_KERNEL);
&#x9;if (unlikely(!str))
&#x9;&#x9;return ERR_PTR(-ENOMEM);

&#x9;memcpy(str,bufp, len);
&#x9;str[len] = 0;
&#x9;*bufp += len;
&#x9;*remain -= len;

&#x9;return str;
}

*/ Translate an inode field to kernel representation. /*
static inline int audit_to_inode(struct audit_krulekrule,
&#x9;&#x9;&#x9;&#x9; struct audit_fieldf)
{
&#x9;if (krule-&#x3E;listnr != AUDIT_FILTER_EXIT ||
&#x9;    krule-&#x3E;inode_f || krule-&#x3E;watch || krule-&#x3E;tree ||
&#x9;    (f-&#x3E;op != Audit_equal &#x26;&#x26; f-&#x3E;op != Audit_not_equal))
&#x9;&#x9;return -EINVAL;

&#x9;krule-&#x3E;inode_f = f;
&#x9;return 0;
}

static __u32classes[AUDIT_SYSCALL_CLASSES];

int __init audit_register_class(int class, unsignedlist)
{
&#x9;__u32p = kcalloc(AUDIT_BITMASK_SIZE, sizeof(__u32), GFP_KERNEL);
&#x9;if (!p)
&#x9;&#x9;return -ENOMEM;
&#x9;while (*list != ~0U) {
&#x9;&#x9;unsigned n =list++;
&#x9;&#x9;if (n &#x3E;= AUDIT_BITMASK_SIZE 32 - AUDIT_SYSCALL_CLASSES) {
&#x9;&#x9;&#x9;kfree(p);
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;}
&#x9;&#x9;p[AUDIT_WORD(n)] |= AUDIT_BIT(n);
&#x9;}
&#x9;if (class &#x3E;= AUDIT_SYSCALL_CLASSES || classes[class]) {
&#x9;&#x9;kfree(p);
&#x9;&#x9;return -EINVAL;
&#x9;}
&#x9;classes[class] = p;
&#x9;return 0;
}

int audit_match_class(int class, unsigned syscall)
{
&#x9;if (unlikely(syscall &#x3E;= AUDIT_BITMASK_SIZE 32))
&#x9;&#x9;return 0;
&#x9;if (unlikely(class &#x3E;= AUDIT_SYSCALL_CLASSES || !classes[class]))
&#x9;&#x9;return 0;
&#x9;return classes[class][AUDIT_WORD(syscall)] &#x26; AUDIT_BIT(syscall);
}

#ifdef CONFIG_AUDITSYSCALL
static inline int audit_match_class_bits(int class, u32mask)
{
&#x9;int i;

&#x9;if (classes[class]) {
&#x9;&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
&#x9;&#x9;&#x9;if (mask[i] &#x26; classes[class][i])
&#x9;&#x9;&#x9;&#x9;return 0;
&#x9;}
&#x9;return 1;
}

static int audit_match_signal(struct audit_entryentry)
{
&#x9;struct audit_fieldarch = entry-&#x3E;rule.arch_f;

&#x9;if (!arch) {
&#x9;&#x9;*/ When arch is unspecified, we must check both masks on biarch
&#x9;&#x9; as syscall number alone is ambiguous. /*
&#x9;&#x9;return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask) &#x26;&#x26;
&#x9;&#x9;&#x9;audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask));
&#x9;}

&#x9;switch(audit_classify_arch(arch-&#x3E;val)) {
&#x9;case 0:/ native /*
&#x9;&#x9;return (audit_match_class_bits(AUDIT_CLASS_SIGNAL,
&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask));
&#x9;case 1:/ 32bit on biarch /*
&#x9;&#x9;return (audit_match_class_bits(AUDIT_CLASS_SIGNAL_32,
&#x9;&#x9;&#x9;&#x9;&#x9;       entry-&#x3E;rule.mask));
&#x9;default:
&#x9;&#x9;return 1;
&#x9;}
}
#endif

*/ Common user-space to kernel rule translation. /*
static inline struct audit_entryaudit_to_entry_common(struct audit_rule_datarule)
{
&#x9;unsigned listnr;
&#x9;struct audit_entryentry;
&#x9;int i, err;

&#x9;err = -EINVAL;
&#x9;listnr = rule-&#x3E;flags &#x26; ~AUDIT_FILTER_PREPEND;
&#x9;switch(listnr) {
&#x9;default:
&#x9;&#x9;goto exit_err;
#ifdef CONFIG_AUDITSYSCALL
&#x9;case AUDIT_FILTER_ENTRY:
&#x9;&#x9;if (rule-&#x3E;action == AUDIT_ALWAYS)
&#x9;&#x9;&#x9;goto exit_err;
&#x9;case AUDIT_FILTER_EXIT:
&#x9;case AUDIT_FILTER_TASK:
#endif
&#x9;case AUDIT_FILTER_USER:
&#x9;case AUDIT_FILTER_TYPE:
&#x9;&#x9;;
&#x9;}
&#x9;if (unlikely(rule-&#x3E;action == AUDIT_POSSIBLE)) {
&#x9;&#x9;pr_err(&#x22;AUDIT_POSSIBLE is deprecated\n&#x22;);
&#x9;&#x9;goto exit_err;
&#x9;}
&#x9;if (rule-&#x3E;action != AUDIT_NEVER &#x26;&#x26; rule-&#x3E;action != AUDIT_ALWAYS)
&#x9;&#x9;goto exit_err;
&#x9;if (rule-&#x3E;field_count &#x3E; AUDIT_MAX_FIELDS)
&#x9;&#x9;goto exit_err;

&#x9;err = -ENOMEM;
&#x9;entry = audit_init_entry(rule-&#x3E;field_count);
&#x9;if (!entry)
&#x9;&#x9;goto exit_err;

&#x9;entry-&#x3E;rule.flags = rule-&#x3E;flags &#x26; AUDIT_FILTER_PREPEND;
&#x9;entry-&#x3E;rule.listnr = listnr;
&#x9;entry-&#x3E;rule.action = rule-&#x3E;action;
&#x9;entry-&#x3E;rule.field_count = rule-&#x3E;field_count;

&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
&#x9;&#x9;entry-&#x3E;rule.mask[i] = rule-&#x3E;mask[i];

&#x9;for (i = 0; i &#x3C; AUDIT_SYSCALL_CLASSES; i++) {
&#x9;&#x9;int bit = AUDIT_BITMASK_SIZE 32 - i - 1;
&#x9;&#x9;__u32p = &#x26;entry-&#x3E;rule.mask[AUDIT_WORD(bit)];
&#x9;&#x9;__u32class;

&#x9;&#x9;if (!(*p &#x26; AUDIT_BIT(bit)))
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;*p &#x26;= ~AUDIT_BIT(bit);
&#x9;&#x9;class = classes[i];
&#x9;&#x9;if (class) {
&#x9;&#x9;&#x9;int j;
&#x9;&#x9;&#x9;for (j = 0; j &#x3C; AUDIT_BITMASK_SIZE; j++)
&#x9;&#x9;&#x9;&#x9;entry-&#x3E;rule.mask[j] |= class[j];
&#x9;&#x9;}
&#x9;}

&#x9;return entry;

exit_err:
&#x9;return ERR_PTR(err);
}

static u32 audit_ops[] =
{
&#x9;[Audit_equal] = AUDIT_EQUAL,
&#x9;[Audit_not_equal] = AUDIT_NOT_EQUAL,
&#x9;[Audit_bitmask] = AUDIT_BIT_MASK,
&#x9;[Audit_bittest] = AUDIT_BIT_TEST,
&#x9;[Audit_lt] = AUDIT_LESS_THAN,
&#x9;[Audit_gt] = AUDIT_GREATER_THAN,
&#x9;[Audit_le] = AUDIT_LESS_THAN_OR_EQUAL,
&#x9;[Audit_ge] = AUDIT_GREATER_THAN_OR_EQUAL,
};

static u32 audit_to_op(u32 op)
{
&#x9;u32 n;
&#x9;for (n = Audit_equal; n &#x3C; Audit_bad &#x26;&#x26; audit_ops[n] != op; n++)
&#x9;&#x9;;
&#x9;return n;
}

*/ check if an audit field is valid /*
static int audit_field_valid(struct audit_entryentry, struct audit_fieldf)
{
&#x9;switch(f-&#x3E;type) {
&#x9;case AUDIT_MSGTYPE:
&#x9;&#x9;if (entry-&#x3E;rule.listnr != AUDIT_FILTER_TYPE &#x26;&#x26;
&#x9;&#x9;    entry-&#x3E;rule.listnr != AUDIT_FILTER_USER)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;};

&#x9;switch(f-&#x3E;type) {
&#x9;default:
&#x9;&#x9;return -EINVAL;
&#x9;case AUDIT_UID:
&#x9;case AUDIT_EUID:
&#x9;case AUDIT_SUID:
&#x9;case AUDIT_FSUID:
&#x9;case AUDIT_LOGINUID:
&#x9;case AUDIT_OBJ_UID:
&#x9;case AUDIT_GID:
&#x9;case AUDIT_EGID:
&#x9;case AUDIT_SGID:
&#x9;case AUDIT_FSGID:
&#x9;case AUDIT_OBJ_GID:
&#x9;case AUDIT_PID:
&#x9;case AUDIT_PERS:
&#x9;case AUDIT_MSGTYPE:
&#x9;case AUDIT_PPID:
&#x9;case AUDIT_DEVMAJOR:
&#x9;case AUDIT_DEVMINOR:
&#x9;case AUDIT_EXIT:
&#x9;case AUDIT_SUCCESS:
&#x9;case AUDIT_INODE:
&#x9;&#x9;*/ bit ops are only useful on syscall args /*
&#x9;&#x9;if (f-&#x3E;op == Audit_bitmask || f-&#x3E;op == Audit_bittest)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;case AUDIT_ARG0:
&#x9;case AUDIT_ARG1:
&#x9;case AUDIT_ARG2:
&#x9;case AUDIT_ARG3:
&#x9;case AUDIT_SUBJ_USER:
&#x9;case AUDIT_SUBJ_ROLE:
&#x9;case AUDIT_SUBJ_TYPE:
&#x9;case AUDIT_SUBJ_SEN:
&#x9;case AUDIT_SUBJ_CLR:
&#x9;case AUDIT_OBJ_USER:
&#x9;case AUDIT_OBJ_ROLE:
&#x9;case AUDIT_OBJ_TYPE:
&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;case AUDIT_WATCH:
&#x9;case AUDIT_DIR:
&#x9;case AUDIT_FILTERKEY:
&#x9;&#x9;break;
&#x9;case AUDIT_LOGINUID_SET:
&#x9;&#x9;if ((f-&#x3E;val != 0) &#x26;&#x26; (f-&#x3E;val != 1))
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;*/ FALL THROUGH /*
&#x9;case AUDIT_ARCH:
&#x9;&#x9;if (f-&#x3E;op != Audit_not_equal &#x26;&#x26; f-&#x3E;op != Audit_equal)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;case AUDIT_PERM:
&#x9;&#x9;if (f-&#x3E;val &#x26; ~15)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;case AUDIT_FILETYPE:
&#x9;&#x9;if (f-&#x3E;val &#x26; ~S_IFMT)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;case AUDIT_FIELD_COMPARE:
&#x9;&#x9;if (f-&#x3E;val &#x3E; AUDIT_MAX_FIELD_COMPARE)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;case AUDIT_EXE:
&#x9;&#x9;if (f-&#x3E;op != Audit_equal)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;if (entry-&#x3E;rule.listnr != AUDIT_FILTER_EXIT)
&#x9;&#x9;&#x9;return -EINVAL;
&#x9;&#x9;break;
&#x9;};
&#x9;return 0;
}

*/ Translate struct audit_rule_data to kernel&#x27;s rule representation. /*
static struct audit_entryaudit_data_to_entry(struct audit_rule_datadata,
&#x9;&#x9;&#x9;&#x9;&#x9;       size_t datasz)
{
&#x9;int err = 0;
&#x9;struct audit_entryentry;
&#x9;voidbufp;
&#x9;size_t remain = datasz - sizeof(struct audit_rule_data);
&#x9;int i;
&#x9;charstr;
&#x9;struct audit_fsnotify_markaudit_mark;

&#x9;entry = audit_to_entry_common(data);
&#x9;if (IS_ERR(entry))
&#x9;&#x9;goto exit_nofree;

&#x9;bufp = data-&#x3E;buf;
&#x9;for (i = 0; i &#x3C; data-&#x3E;field_count; i++) {
&#x9;&#x9;struct audit_fieldf = &#x26;entry-&#x3E;rule.fields[i];

&#x9;&#x9;err = -EINVAL;

&#x9;&#x9;f-&#x3E;op = audit_to_op(data-&#x3E;fieldflags[i]);
&#x9;&#x9;if (f-&#x3E;op == Audit_bad)
&#x9;&#x9;&#x9;goto exit_free;

&#x9;&#x9;f-&#x3E;type = data-&#x3E;fields[i];
&#x9;&#x9;f-&#x3E;val = data-&#x3E;values[i];

&#x9;&#x9;*/ Support legacy tests for a valid loginuid /*
&#x9;&#x9;if ((f-&#x3E;type == AUDIT_LOGINUID) &#x26;&#x26; (f-&#x3E;val == AUDIT_UID_UNSET)) {
&#x9;&#x9;&#x9;f-&#x3E;type = AUDIT_LOGINUID_SET;
&#x9;&#x9;&#x9;f-&#x3E;val = 0;
&#x9;&#x9;&#x9;entry-&#x3E;rule.pflags |= AUDIT_LOGINUID_LEGACY;
&#x9;&#x9;}

&#x9;&#x9;err = audit_field_valid(entry, f);
&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;goto exit_free;

&#x9;&#x9;err = -EINVAL;
&#x9;&#x9;switch (f-&#x3E;type) {
&#x9;&#x9;case AUDIT_LOGINUID:
&#x9;&#x9;case AUDIT_UID:
&#x9;&#x9;case AUDIT_EUID:
&#x9;&#x9;case AUDIT_SUID:
&#x9;&#x9;case AUDIT_FSUID:
&#x9;&#x9;case AUDIT_OBJ_UID:
&#x9;&#x9;&#x9;f-&#x3E;uid = make_kuid(current_user_ns(), f-&#x3E;val);
&#x9;&#x9;&#x9;if (!uid_valid(f-&#x3E;uid))
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_GID:
&#x9;&#x9;case AUDIT_EGID:
&#x9;&#x9;case AUDIT_SGID:
&#x9;&#x9;case AUDIT_FSGID:
&#x9;&#x9;case AUDIT_OBJ_GID:
&#x9;&#x9;&#x9;f-&#x3E;gid = make_kgid(current_user_ns(), f-&#x3E;val);
&#x9;&#x9;&#x9;if (!gid_valid(f-&#x3E;gid))
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_ARCH:
&#x9;&#x9;&#x9;entry-&#x3E;rule.arch_f = f;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_SUBJ_USER:
&#x9;&#x9;case AUDIT_SUBJ_ROLE:
&#x9;&#x9;case AUDIT_SUBJ_TYPE:
&#x9;&#x9;case AUDIT_SUBJ_SEN:
&#x9;&#x9;case AUDIT_SUBJ_CLR:
&#x9;&#x9;case AUDIT_OBJ_USER:
&#x9;&#x9;case AUDIT_OBJ_ROLE:
&#x9;&#x9;case AUDIT_OBJ_TYPE:
&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
&#x9;&#x9;&#x9;if (IS_ERR(str))
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;

&#x9;&#x9;&#x9;err = security_audit_rule_init(f-&#x3E;type, f-&#x3E;op, str,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;       (void*)&#x26;f-&#x3E;lsm_rule);
&#x9;&#x9;&#x9;*/ Keep currently invalid fields around in case they
&#x9;&#x9;&#x9; become valid after a policy reload. /*
&#x9;&#x9;&#x9;if (err == -EINVAL) {
&#x9;&#x9;&#x9;&#x9;pr_warn(&#x22;audit rule for LSM \&#x27;%s\&#x27; is invalid\n&#x22;,
&#x9;&#x9;&#x9;&#x9;&#x9;str);
&#x9;&#x9;&#x9;&#x9;err = 0;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;if (err) {
&#x9;&#x9;&#x9;&#x9;kfree(str);
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;} else
&#x9;&#x9;&#x9;&#x9;f-&#x3E;lsm_str = str;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_WATCH:
&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
&#x9;&#x9;&#x9;if (IS_ERR(str))
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;

&#x9;&#x9;&#x9;err = audit_to_watch(&#x26;entry-&#x3E;rule, str, f-&#x3E;val, f-&#x3E;op);
&#x9;&#x9;&#x9;if (err) {
&#x9;&#x9;&#x9;&#x9;kfree(str);
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_DIR:
&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
&#x9;&#x9;&#x9;if (IS_ERR(str))
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;

&#x9;&#x9;&#x9;err = audit_make_tree(&#x26;entry-&#x3E;rule, str, f-&#x3E;op);
&#x9;&#x9;&#x9;kfree(str);
&#x9;&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_INODE:
&#x9;&#x9;&#x9;err = audit_to_inode(&#x26;entry-&#x3E;rule, f);
&#x9;&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FILTERKEY:
&#x9;&#x9;&#x9;if (entry-&#x3E;rule.filterkey || f-&#x3E;val &#x3E; AUDIT_MAX_KEY_LEN)
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
&#x9;&#x9;&#x9;if (IS_ERR(str))
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;
&#x9;&#x9;&#x9;entry-&#x3E;rule.filterkey = str;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EXE:
&#x9;&#x9;&#x9;if (entry-&#x3E;rule.exe || f-&#x3E;val &#x3E; PATH_MAX)
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;str = audit_unpack_string(&#x26;bufp, &#x26;remain, f-&#x3E;val);
&#x9;&#x9;&#x9;if (IS_ERR(str)) {
&#x9;&#x9;&#x9;&#x9;err = PTR_ERR(str);
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;entry-&#x3E;rule.buflen += f-&#x3E;val;

&#x9;&#x9;&#x9;audit_mark = audit_alloc_mark(&#x26;entry-&#x3E;rule, str, f-&#x3E;val);
&#x9;&#x9;&#x9;if (IS_ERR(audit_mark)) {
&#x9;&#x9;&#x9;&#x9;kfree(str);
&#x9;&#x9;&#x9;&#x9;err = PTR_ERR(audit_mark);
&#x9;&#x9;&#x9;&#x9;goto exit_free;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;entry-&#x3E;rule.exe = audit_mark;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;}

&#x9;if (entry-&#x3E;rule.inode_f &#x26;&#x26; entry-&#x3E;rule.inode_f-&#x3E;op == Audit_not_equal)
&#x9;&#x9;entry-&#x3E;rule.inode_f = NULL;

exit_nofree:
&#x9;return entry;

exit_free:
&#x9;if (entry-&#x3E;rule.tree)
&#x9;&#x9;audit_put_tree(entry-&#x3E;rule.tree);/ that&#x27;s the temporary one /*
&#x9;if (entry-&#x3E;rule.exe)
&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);/ that&#x27;s the template one /*
&#x9;audit_free_rule(entry);
&#x9;return ERR_PTR(err);
}

*/ Pack a filter field&#x27;s string representation into data block. /*
static inline size_t audit_pack_string(void*bufp, const charstr)
{
&#x9;size_t len = strlen(str);

&#x9;memcpy(*bufp, str, len);
&#x9;*bufp += len;

&#x9;return len;
}

*/ Translate kernel rule representation to struct audit_rule_data. /*
static struct audit_rule_dataaudit_krule_to_data(struct audit_krulekrule)
{
&#x9;struct audit_rule_datadata;
&#x9;voidbufp;
&#x9;int i;

&#x9;data = kmalloc(sizeof(*data) + krule-&#x3E;buflen, GFP_KERNEL);
&#x9;if (unlikely(!data))
&#x9;&#x9;return NULL;
&#x9;memset(data, 0, sizeof(*data));

&#x9;data-&#x3E;flags = krule-&#x3E;flags | krule-&#x3E;listnr;
&#x9;data-&#x3E;action = krule-&#x3E;action;
&#x9;data-&#x3E;field_count = krule-&#x3E;field_count;
&#x9;bufp = data-&#x3E;buf;
&#x9;for (i = 0; i &#x3C; data-&#x3E;field_count; i++) {
&#x9;&#x9;struct audit_fieldf = &#x26;krule-&#x3E;fields[i];

&#x9;&#x9;data-&#x3E;fields[i] = f-&#x3E;type;
&#x9;&#x9;data-&#x3E;fieldflags[i] = audit_ops[f-&#x3E;op];
&#x9;&#x9;switch(f-&#x3E;type) {
&#x9;&#x9;case AUDIT_SUBJ_USER:
&#x9;&#x9;case AUDIT_SUBJ_ROLE:
&#x9;&#x9;case AUDIT_SUBJ_TYPE:
&#x9;&#x9;case AUDIT_SUBJ_SEN:
&#x9;&#x9;case AUDIT_SUBJ_CLR:
&#x9;&#x9;case AUDIT_OBJ_USER:
&#x9;&#x9;case AUDIT_OBJ_ROLE:
&#x9;&#x9;case AUDIT_OBJ_TYPE:
&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp, f-&#x3E;lsm_str);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_WATCH:
&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  audit_watch_path(krule-&#x3E;watch));
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_DIR:
&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  audit_tree_path(krule-&#x3E;tree));
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FILTERKEY:
&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp, krule-&#x3E;filterkey);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EXE:
&#x9;&#x9;&#x9;data-&#x3E;buflen += data-&#x3E;values[i] =
&#x9;&#x9;&#x9;&#x9;audit_pack_string(&#x26;bufp, audit_mark_path(krule-&#x3E;exe));
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_LOGINUID_SET:
&#x9;&#x9;&#x9;if (krule-&#x3E;pflags &#x26; AUDIT_LOGINUID_LEGACY &#x26;&#x26; !f-&#x3E;val) {
&#x9;&#x9;&#x9;&#x9;data-&#x3E;fields[i] = AUDIT_LOGINUID;
&#x9;&#x9;&#x9;&#x9;data-&#x3E;values[i] = AUDIT_UID_UNSET;
&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;*/ fallthrough if set /*
&#x9;&#x9;default:
&#x9;&#x9;&#x9;data-&#x3E;values[i] = f-&#x3E;val;
&#x9;&#x9;}
&#x9;}
&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++) data-&#x3E;mask[i] = krule-&#x3E;mask[i];

&#x9;return data;
}

*/ Compare two rules in kernel format.  Considered success if rules
 don&#x27;t match. /*
static int audit_compare_rule(struct audit_krulea, struct audit_kruleb)
{
&#x9;int i;

&#x9;if (a-&#x3E;flags != b-&#x3E;flags ||
&#x9;    a-&#x3E;pflags != b-&#x3E;pflags ||
&#x9;    a-&#x3E;listnr != b-&#x3E;listnr ||
&#x9;    a-&#x3E;action != b-&#x3E;action ||
&#x9;    a-&#x3E;field_count != b-&#x3E;field_count)
&#x9;&#x9;return 1;

&#x9;for (i = 0; i &#x3C; a-&#x3E;field_count; i++) {
&#x9;&#x9;if (a-&#x3E;fields[i].type != b-&#x3E;fields[i].type ||
&#x9;&#x9;    a-&#x3E;fields[i].op != b-&#x3E;fields[i].op)
&#x9;&#x9;&#x9;return 1;

&#x9;&#x9;switch(a-&#x3E;fields[i].type) {
&#x9;&#x9;case AUDIT_SUBJ_USER:
&#x9;&#x9;case AUDIT_SUBJ_ROLE:
&#x9;&#x9;case AUDIT_SUBJ_TYPE:
&#x9;&#x9;case AUDIT_SUBJ_SEN:
&#x9;&#x9;case AUDIT_SUBJ_CLR:
&#x9;&#x9;case AUDIT_OBJ_USER:
&#x9;&#x9;case AUDIT_OBJ_ROLE:
&#x9;&#x9;case AUDIT_OBJ_TYPE:
&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;&#x9;&#x9;if (strcmp(a-&#x3E;fields[i].lsm_str, b-&#x3E;fields[i].lsm_str))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_WATCH:
&#x9;&#x9;&#x9;if (strcmp(audit_watch_path(a-&#x3E;watch),
&#x9;&#x9;&#x9;&#x9;   audit_watch_path(b-&#x3E;watch)))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_DIR:
&#x9;&#x9;&#x9;if (strcmp(audit_tree_path(a-&#x3E;tree),
&#x9;&#x9;&#x9;&#x9;   audit_tree_path(b-&#x3E;tree)))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FILTERKEY:
&#x9;&#x9;&#x9;*/ both filterkeys exist based on above type compare /*
&#x9;&#x9;&#x9;if (strcmp(a-&#x3E;filterkey, b-&#x3E;filterkey))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EXE:
&#x9;&#x9;&#x9;*/ both paths exist based on above type compare /*
&#x9;&#x9;&#x9;if (strcmp(audit_mark_path(a-&#x3E;exe),
&#x9;&#x9;&#x9;&#x9;   audit_mark_path(b-&#x3E;exe)))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_UID:
&#x9;&#x9;case AUDIT_EUID:
&#x9;&#x9;case AUDIT_SUID:
&#x9;&#x9;case AUDIT_FSUID:
&#x9;&#x9;case AUDIT_LOGINUID:
&#x9;&#x9;case AUDIT_OBJ_UID:
&#x9;&#x9;&#x9;if (!uid_eq(a-&#x3E;fields[i].uid, b-&#x3E;fields[i].uid))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_GID:
&#x9;&#x9;case AUDIT_EGID:
&#x9;&#x9;case AUDIT_SGID:
&#x9;&#x9;case AUDIT_FSGID:
&#x9;&#x9;case AUDIT_OBJ_GID:
&#x9;&#x9;&#x9;if (!gid_eq(a-&#x3E;fields[i].gid, b-&#x3E;fields[i].gid))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;default:
&#x9;&#x9;&#x9;if (a-&#x3E;fields[i].val != b-&#x3E;fields[i].val)
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;}
&#x9;}

&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
&#x9;&#x9;if (a-&#x3E;mask[i] != b-&#x3E;mask[i])
&#x9;&#x9;&#x9;return 1;

&#x9;return 0;
}

*/ Duplicate LSM field information.  The lsm_rule is opaque, so must be
 re-initialized. /*
static inline int audit_dupe_lsm_field(struct audit_fielddf,
&#x9;&#x9;&#x9;&#x9;&#x9;   struct audit_fieldsf)
{
&#x9;int ret = 0;
&#x9;charlsm_str;

&#x9;*/ our own copy of lsm_str /*
&#x9;lsm_str = kstrdup(sf-&#x3E;lsm_str, GFP_KERNEL);
&#x9;if (unlikely(!lsm_str))
&#x9;&#x9;return -ENOMEM;
&#x9;df-&#x3E;lsm_str = lsm_str;

&#x9;*/ our own (refreshed) copy of lsm_rule /*
&#x9;ret = security_audit_rule_init(df-&#x3E;type, df-&#x3E;op, df-&#x3E;lsm_str,
&#x9;&#x9;&#x9;&#x9;       (void*)&#x26;df-&#x3E;lsm_rule);
&#x9;*/ Keep currently invalid fields around in case they
&#x9; become valid after a policy reload. /*
&#x9;if (ret == -EINVAL) {
&#x9;&#x9;pr_warn(&#x22;audit rule for LSM \&#x27;%s\&#x27; is invalid\n&#x22;,
&#x9;&#x9;&#x9;df-&#x3E;lsm_str);
&#x9;&#x9;ret = 0;
&#x9;}

&#x9;return ret;
}

*/ Duplicate an audit rule.  This will be a deep copy with the exception
 of the watch - that pointer is carried over.  The LSM specific fields
 will be updated in the copy.  The point is to be able to replace the old
 rule with the new rule in the filterlist, then free the old rule.
 The rlist element is undefined; list manipulations are handled apart from
 the initial copy. /*
struct audit_entryaudit_dupe_rule(struct audit_kruleold)
{
&#x9;u32 fcount = old-&#x3E;field_count;
&#x9;struct audit_entryentry;
&#x9;struct audit_krulenew;
&#x9;charfk;
&#x9;int i, err = 0;

&#x9;entry = audit_init_entry(fcount);
&#x9;if (unlikely(!entry))
&#x9;&#x9;return ERR_PTR(-ENOMEM);

&#x9;new = &#x26;entry-&#x3E;rule;
&#x9;new-&#x3E;flags = old-&#x3E;flags;
&#x9;new-&#x3E;pflags = old-&#x3E;pflags;
&#x9;new-&#x3E;listnr = old-&#x3E;listnr;
&#x9;new-&#x3E;action = old-&#x3E;action;
&#x9;for (i = 0; i &#x3C; AUDIT_BITMASK_SIZE; i++)
&#x9;&#x9;new-&#x3E;mask[i] = old-&#x3E;mask[i];
&#x9;new-&#x3E;prio = old-&#x3E;prio;
&#x9;new-&#x3E;buflen = old-&#x3E;buflen;
&#x9;new-&#x3E;inode_f = old-&#x3E;inode_f;
&#x9;new-&#x3E;field_count = old-&#x3E;field_count;

&#x9;*/
&#x9; note that we are OK with not refcounting here; audit_match_tree()
&#x9; never dereferences tree and we can&#x27;t get false positives there
&#x9; since we&#x27;d have to have rule gone from the listand* removed
&#x9; before the chunks found by lookup had been allocated, i.e. before
&#x9; the beginning of list scan.
&#x9; /*
&#x9;new-&#x3E;tree = old-&#x3E;tree;
&#x9;memcpy(new-&#x3E;fields, old-&#x3E;fields, sizeof(struct audit_field) fcount);

&#x9;*/ deep copy this information, updating the lsm_rule fields, because
&#x9; the originals will all be freed when the old rule is freed. /*
&#x9;for (i = 0; i &#x3C; fcount; i++) {
&#x9;&#x9;switch (new-&#x3E;fields[i].type) {
&#x9;&#x9;case AUDIT_SUBJ_USER:
&#x9;&#x9;case AUDIT_SUBJ_ROLE:
&#x9;&#x9;case AUDIT_SUBJ_TYPE:
&#x9;&#x9;case AUDIT_SUBJ_SEN:
&#x9;&#x9;case AUDIT_SUBJ_CLR:
&#x9;&#x9;case AUDIT_OBJ_USER:
&#x9;&#x9;case AUDIT_OBJ_ROLE:
&#x9;&#x9;case AUDIT_OBJ_TYPE:
&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;&#x9;&#x9;err = audit_dupe_lsm_field(&#x26;new-&#x3E;fields[i],
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;       &#x26;old-&#x3E;fields[i]);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FILTERKEY:
&#x9;&#x9;&#x9;fk = kstrdup(old-&#x3E;filterkey, GFP_KERNEL);
&#x9;&#x9;&#x9;if (unlikely(!fk))
&#x9;&#x9;&#x9;&#x9;err = -ENOMEM;
&#x9;&#x9;&#x9;else
&#x9;&#x9;&#x9;&#x9;new-&#x3E;filterkey = fk;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EXE:
&#x9;&#x9;&#x9;err = audit_dupe_exe(new, old);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;&#x9;if (err) {
&#x9;&#x9;&#x9;if (new-&#x3E;exe)
&#x9;&#x9;&#x9;&#x9;audit_remove_mark(new-&#x3E;exe);
&#x9;&#x9;&#x9;audit_free_rule(entry);
&#x9;&#x9;&#x9;return ERR_PTR(err);
&#x9;&#x9;}
&#x9;}

&#x9;if (old-&#x3E;watch) {
&#x9;&#x9;audit_get_watch(old-&#x3E;watch);
&#x9;&#x9;new-&#x3E;watch = old-&#x3E;watch;
&#x9;}

&#x9;return entry;
}

*/ Find an existing audit rule.
 Caller must hold audit_filter_mutex to prevent stale rule data. /*
static struct audit_entryaudit_find_rule(struct audit_entryentry,
&#x9;&#x9;&#x9;&#x9;&#x9;   struct list_head*p)
{
&#x9;struct audit_entrye,found = NULL;
&#x9;struct list_headlist;
&#x9;int h;

&#x9;if (entry-&#x3E;rule.inode_f) {
&#x9;&#x9;h = audit_hash_ino(entry-&#x3E;rule.inode_f-&#x3E;val);
&#x9;&#x9;*p = list = &#x26;audit_inode_hash[h];
&#x9;} else if (entry-&#x3E;rule.watch) {
&#x9;&#x9;*/ we don&#x27;t know the inode number, so must walk entire hash /*
&#x9;&#x9;for (h = 0; h &#x3C; AUDIT_INODE_BUCKETS; h++) {
&#x9;&#x9;&#x9;list = &#x26;audit_inode_hash[h];
&#x9;&#x9;&#x9;list_for_each_entry(e, list, list)
&#x9;&#x9;&#x9;&#x9;if (!audit_compare_rule(&#x26;entry-&#x3E;rule, &#x26;e-&#x3E;rule)) {
&#x9;&#x9;&#x9;&#x9;&#x9;found = e;
&#x9;&#x9;&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;&#x9;goto out;
&#x9;} else {
&#x9;&#x9;*p = list = &#x26;audit_filter_list[entry-&#x3E;rule.listnr];
&#x9;}

&#x9;list_for_each_entry(e, list, list)
&#x9;&#x9;if (!audit_compare_rule(&#x26;entry-&#x3E;rule, &#x26;e-&#x3E;rule)) {
&#x9;&#x9;&#x9;found = e;
&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;}

out:
&#x9;return found;
}

static u64 prio_low = ~0ULL/2;
static u64 prio_high = ~0ULL/2 - 1;

*/ Add rule to given filterlist if not a duplicate. /*
static inline int audit_add_rule(struct audit_entryentry)
{
&#x9;struct audit_entrye;
&#x9;struct audit_watchwatch = entry-&#x3E;rule.watch;
&#x9;struct audit_treetree = entry-&#x3E;rule.tree;
&#x9;struct list_headlist;
&#x9;int err = 0;
#ifdef CONFIG_AUDITSYSCALL
&#x9;int dont_count = 0;

&#x9;*/ If either of these, don&#x27;t count towards total /*
&#x9;if (entry-&#x3E;rule.listnr == AUDIT_FILTER_USER ||
&#x9;&#x9;entry-&#x3E;rule.listnr == AUDIT_FILTER_TYPE)
&#x9;&#x9;dont_count = 1;
#endif

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;e = audit_find_rule(entry, &#x26;list);
&#x9;if (e) {
&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;&#x9;err = -EEXIST;
&#x9;&#x9;*/ normally audit_add_tree_rule() will free it on failure /*
&#x9;&#x9;if (tree)
&#x9;&#x9;&#x9;audit_put_tree(tree);
&#x9;&#x9;return err;
&#x9;}

&#x9;if (watch) {
&#x9;&#x9;*/ audit_filter_mutex is dropped and re-taken during this call /*
&#x9;&#x9;err = audit_add_watch(&#x26;entry-&#x3E;rule, &#x26;list);
&#x9;&#x9;if (err) {
&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;&#x9;&#x9;*/
&#x9;&#x9;&#x9; normally audit_add_tree_rule() will free it
&#x9;&#x9;&#x9; on failure
&#x9;&#x9;&#x9; /*
&#x9;&#x9;&#x9;if (tree)
&#x9;&#x9;&#x9;&#x9;audit_put_tree(tree);
&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;}
&#x9;if (tree) {
&#x9;&#x9;err = audit_add_tree_rule(&#x26;entry-&#x3E;rule);
&#x9;&#x9;if (err) {
&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;&#x9;&#x9;return err;
&#x9;&#x9;}
&#x9;}

&#x9;entry-&#x3E;rule.prio = ~0ULL;
&#x9;if (entry-&#x3E;rule.listnr == AUDIT_FILTER_EXIT) {
&#x9;&#x9;if (entry-&#x3E;rule.flags &#x26; AUDIT_FILTER_PREPEND)
&#x9;&#x9;&#x9;entry-&#x3E;rule.prio = ++prio_high;
&#x9;&#x9;else
&#x9;&#x9;&#x9;entry-&#x3E;rule.prio = --prio_low;
&#x9;}

&#x9;if (entry-&#x3E;rule.flags &#x26; AUDIT_FILTER_PREPEND) {
&#x9;&#x9;list_add(&#x26;entry-&#x3E;rule.list,
&#x9;&#x9;&#x9; &#x26;audit_rules_list[entry-&#x3E;rule.listnr]);
&#x9;&#x9;list_add_rcu(&#x26;entry-&#x3E;list, list);
&#x9;&#x9;entry-&#x3E;rule.flags &#x26;= ~AUDIT_FILTER_PREPEND;
&#x9;} else {
&#x9;&#x9;list_add_tail(&#x26;entry-&#x3E;rule.list,
&#x9;&#x9;&#x9;      &#x26;audit_rules_list[entry-&#x3E;rule.listnr]);
&#x9;&#x9;list_add_tail_rcu(&#x26;entry-&#x3E;list, list);
&#x9;}
#ifdef CONFIG_AUDITSYSCALL
&#x9;if (!dont_count)
&#x9;&#x9;audit_n_rules++;

&#x9;if (!audit_match_signal(entry))
&#x9;&#x9;audit_signals++;
#endif
&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;return err;
}

*/ Remove an existing rule from filterlist. /*
int audit_del_rule(struct audit_entryentry)
{
&#x9;struct audit_entry e;
&#x9;struct audit_treetree = entry-&#x3E;rule.tree;
&#x9;struct list_headlist;
&#x9;int ret = 0;
#ifdef CONFIG_AUDITSYSCALL
&#x9;int dont_count = 0;

&#x9;*/ If either of these, don&#x27;t count towards total /*
&#x9;if (entry-&#x3E;rule.listnr == AUDIT_FILTER_USER ||
&#x9;&#x9;entry-&#x3E;rule.listnr == AUDIT_FILTER_TYPE)
&#x9;&#x9;dont_count = 1;
#endif

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;e = audit_find_rule(entry, &#x26;list);
&#x9;if (!e) {
&#x9;&#x9;ret = -ENOENT;
&#x9;&#x9;goto out;
&#x9;}

&#x9;if (e-&#x3E;rule.watch)
&#x9;&#x9;audit_remove_watch_rule(&#x26;e-&#x3E;rule);

&#x9;if (e-&#x3E;rule.tree)
&#x9;&#x9;audit_remove_tree_rule(&#x26;e-&#x3E;rule);

&#x9;if (e-&#x3E;rule.exe)
&#x9;&#x9;audit_remove_mark_rule(&#x26;e-&#x3E;rule);

#ifdef CONFIG_AUDITSYSCALL
&#x9;if (!dont_count)
&#x9;&#x9;audit_n_rules--;

&#x9;if (!audit_match_signal(entry))
&#x9;&#x9;audit_signals--;
#endif

&#x9;list_del_rcu(&#x26;e-&#x3E;list);
&#x9;list_del(&#x26;e-&#x3E;rule.list);
&#x9;call_rcu(&#x26;e-&#x3E;rcu, audit_free_rule_rcu);

out:
&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;if (tree)
&#x9;&#x9;audit_put_tree(tree);&#x9;*/ that&#x27;s the temporary one /*

&#x9;return ret;
}

*/ List rules using struct audit_rule_data. /*
static void audit_list_rules(__u32 portid, int seq, struct sk_buff_headq)
{
&#x9;struct sk_buffskb;
&#x9;struct audit_kruler;
&#x9;int i;

&#x9;*/ This is a blocking read, so use audit_filter_mutex instead of rcu
&#x9; iterator to sync with list writers. /*
&#x9;for (i=0; i&#x3C;AUDIT_NR_FILTERS; i++) {
&#x9;&#x9;list_for_each_entry(r, &#x26;audit_rules_list[i], list) {
&#x9;&#x9;&#x9;struct audit_rule_datadata;

&#x9;&#x9;&#x9;data = audit_krule_to_data(r);
&#x9;&#x9;&#x9;if (unlikely(!data))
&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES,
&#x9;&#x9;&#x9;&#x9;&#x9;       0, 1, data,
&#x9;&#x9;&#x9;&#x9;&#x9;       sizeof(*data) + data-&#x3E;buflen);
&#x9;&#x9;&#x9;if (skb)
&#x9;&#x9;&#x9;&#x9;skb_queue_tail(q, skb);
&#x9;&#x9;&#x9;kfree(data);
&#x9;&#x9;}
&#x9;}
&#x9;skb = audit_make_reply(portid, seq, AUDIT_LIST_RULES, 1, 1, NULL, 0);
&#x9;if (skb)
&#x9;&#x9;skb_queue_tail(q, skb);
}

*/ Log rule additions and removals /*
static void audit_log_rule_change(charaction, struct audit_krulerule, int res)
{
&#x9;struct audit_bufferab;
&#x9;uid_t loginuid = from_kuid(&#x26;init_user_ns, audit_get_loginuid(current));
&#x9;unsigned int sessionid = audit_get_sessionid(current);

&#x9;if (!audit_enabled)
&#x9;&#x9;return;

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
&#x9;if (!ab)
&#x9;&#x9;return;
&#x9;audit_log_format(ab, &#x22;auid=%u ses=%u&#x22; ,loginuid, sessionid);
&#x9;audit_log_task_context(ab);
&#x9;audit_log_format(ab, &#x22; op=&#x22;);
&#x9;audit_log_string(ab, action);
&#x9;audit_log_key(ab, rule-&#x3E;filterkey);
&#x9;audit_log_format(ab, &#x22; list=%d res=%d&#x22;, rule-&#x3E;listnr, res);
&#x9;audit_log_end(ab);
}

*/
 audit_rule_change - apply all rules to the specified message type
 @type: audit message type
 @portid: target port id for netlink audit messages
 @seq: netlink audit message sequence (serial) number
 @data: payload data
 @datasz: size of payload data
 /*
int audit_rule_change(int type, __u32 portid, int seq, voiddata,
&#x9;&#x9;&#x9;size_t datasz)
{
&#x9;int err = 0;
&#x9;struct audit_entryentry;

&#x9;entry = audit_data_to_entry(data, datasz);
&#x9;if (IS_ERR(entry))
&#x9;&#x9;return PTR_ERR(entry);

&#x9;switch (type) {
&#x9;case AUDIT_ADD_RULE:
&#x9;&#x9;err = audit_add_rule(entry);
&#x9;&#x9;audit_log_rule_change(&#x22;add_rule&#x22;, &#x26;entry-&#x3E;rule, !err);
&#x9;&#x9;break;
&#x9;case AUDIT_DEL_RULE:
&#x9;&#x9;err = audit_del_rule(entry);
&#x9;&#x9;audit_log_rule_change(&#x22;remove_rule&#x22;, &#x26;entry-&#x3E;rule, !err);
&#x9;&#x9;break;
&#x9;default:
&#x9;&#x9;err = -EINVAL;
&#x9;&#x9;WARN_ON(1);
&#x9;}

&#x9;if (err || type == AUDIT_DEL_RULE) {
&#x9;&#x9;if (entry-&#x3E;rule.exe)
&#x9;&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);
&#x9;&#x9;audit_free_rule(entry);
&#x9;}

&#x9;return err;
}

*/
 audit_list_rules_send - list the audit rules
 @request_skb: skb of request we are replying to (used to target the reply)
 @seq: netlink audit message sequence (serial) number
 /*
int audit_list_rules_send(struct sk_buffrequest_skb, int seq)
{
&#x9;u32 portid = NETLINK_CB(request_skb).portid;
&#x9;struct netnet = sock_net(NETLINK_CB(request_skb).sk);
&#x9;struct task_structtsk;
&#x9;struct audit_netlink_listdest;
&#x9;int err = 0;

&#x9;*/ We can&#x27;t just spew out the rules here because we might fill
&#x9; the available socket buffer space and deadlock waiting for
&#x9; auditctl to read from it... which isn&#x27;t ever going to
&#x9; happen if we&#x27;re actually running in the context of auditctl
&#x9; trying to _send_ the stuff /*

&#x9;dest = kmalloc(sizeof(struct audit_netlink_list), GFP_KERNEL);
&#x9;if (!dest)
&#x9;&#x9;return -ENOMEM;
&#x9;dest-&#x3E;net = get_net(net);
&#x9;dest-&#x3E;portid = portid;
&#x9;skb_queue_head_init(&#x26;dest-&#x3E;q);

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;audit_list_rules(portid, seq, &#x26;dest-&#x3E;q);
&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;tsk = kthread_run(audit_send_list, dest, &#x22;audit_send_list&#x22;);
&#x9;if (IS_ERR(tsk)) {
&#x9;&#x9;skb_queue_purge(&#x26;dest-&#x3E;q);
&#x9;&#x9;kfree(dest);
&#x9;&#x9;err = PTR_ERR(tsk);
&#x9;}

&#x9;return err;
}

int audit_comparator(u32 left, u32 op, u32 right)
{
&#x9;switch (op) {
&#x9;case Audit_equal:
&#x9;&#x9;return (left == right);
&#x9;case Audit_not_equal:
&#x9;&#x9;return (left != right);
&#x9;case Audit_lt:
&#x9;&#x9;return (left &#x3C; right);
&#x9;case Audit_le:
&#x9;&#x9;return (left &#x3C;= right);
&#x9;case Audit_gt:
&#x9;&#x9;return (left &#x3E; right);
&#x9;case Audit_ge:
&#x9;&#x9;return (left &#x3E;= right);
&#x9;case Audit_bitmask:
&#x9;&#x9;return (left &#x26; right);
&#x9;case Audit_bittest:
&#x9;&#x9;return ((left &#x26; right) == right);
&#x9;default:
&#x9;&#x9;BUG();
&#x9;&#x9;return 0;
&#x9;}
}

int audit_uid_comparator(kuid_t left, u32 op, kuid_t right)
{
&#x9;switch (op) {
&#x9;case Audit_equal:
&#x9;&#x9;return uid_eq(left, right);
&#x9;case Audit_not_equal:
&#x9;&#x9;return !uid_eq(left, right);
&#x9;case Audit_lt:
&#x9;&#x9;return uid_lt(left, right);
&#x9;case Audit_le:
&#x9;&#x9;return uid_lte(left, right);
&#x9;case Audit_gt:
&#x9;&#x9;return uid_gt(left, right);
&#x9;case Audit_ge:
&#x9;&#x9;return uid_gte(left, right);
&#x9;case Audit_bitmask:
&#x9;case Audit_bittest:
&#x9;default:
&#x9;&#x9;BUG();
&#x9;&#x9;return 0;
&#x9;}
}

int audit_gid_comparator(kgid_t left, u32 op, kgid_t right)
{
&#x9;switch (op) {
&#x9;case Audit_equal:
&#x9;&#x9;return gid_eq(left, right);
&#x9;case Audit_not_equal:
&#x9;&#x9;return !gid_eq(left, right);
&#x9;case Audit_lt:
&#x9;&#x9;return gid_lt(left, right);
&#x9;case Audit_le:
&#x9;&#x9;return gid_lte(left, right);
&#x9;case Audit_gt:
&#x9;&#x9;return gid_gt(left, right);
&#x9;case Audit_ge:
&#x9;&#x9;return gid_gte(left, right);
&#x9;case Audit_bitmask:
&#x9;case Audit_bittest:
&#x9;default:
&#x9;&#x9;BUG();
&#x9;&#x9;return 0;
&#x9;}
}

*/
 parent_len - find the length of the parent portion of a pathname
 @path: pathname of which to determine length
 /*
int parent_len(const charpath)
{
&#x9;int plen;
&#x9;const charp;

&#x9;plen = strlen(path);

&#x9;if (plen == 0)
&#x9;&#x9;return plen;

&#x9;*/ disregard trailing slashes /*
&#x9;p = path + plen - 1;
&#x9;while ((*p == &#x27;/&#x27;) &#x26;&#x26; (p &#x3E; path))
&#x9;&#x9;p--;

&#x9;*/ walk backward until we find the next slash or hit beginning /*
&#x9;while ((*p != &#x27;/&#x27;) &#x26;&#x26; (p &#x3E; path))
&#x9;&#x9;p--;

&#x9;*/ did we find a slash? Then increment to include it in path /*
&#x9;if (*p == &#x27;/&#x27;)
&#x9;&#x9;p++;

&#x9;return p - path;
}

*/
 audit_compare_dname_path - compare given dentry name with last component in
 &#x9;&#x9;&#x9;      given path. Return of 0 indicates a match.
 @dname:&#x9;dentry name that we&#x27;re comparing
 @path:&#x9;full pathname that we&#x27;re comparing
 @parentlen:&#x9;length of the parent if known. Passing in AUDIT_NAME_FULL
 &#x9;&#x9;here indicates that we must compute this value.
 /*
int audit_compare_dname_path(const chardname, const charpath, int parentlen)
{
&#x9;int dlen, pathlen;
&#x9;const charp;

&#x9;dlen = strlen(dname);
&#x9;pathlen = strlen(path);
&#x9;if (pathlen &#x3C; dlen)
&#x9;&#x9;return 1;

&#x9;parentlen = parentlen == AUDIT_NAME_FULL ? parent_len(path) : parentlen;
&#x9;if (pathlen - parentlen != dlen)
&#x9;&#x9;return 1;

&#x9;p = path + parentlen;

&#x9;return strncmp(p, dname, dlen);
}

static int audit_filter_user_rules(struct audit_krulerule, int type,
&#x9;&#x9;&#x9;&#x9;   enum audit_statestate)
{
&#x9;int i;

&#x9;for (i = 0; i &#x3C; rule-&#x3E;field_count; i++) {
&#x9;&#x9;struct audit_fieldf = &#x26;rule-&#x3E;fields[i];
&#x9;&#x9;pid_t pid;
&#x9;&#x9;int result = 0;
&#x9;&#x9;u32 sid;

&#x9;&#x9;switch (f-&#x3E;type) {
&#x9;&#x9;case AUDIT_PID:
&#x9;&#x9;&#x9;pid = task_pid_nr(current);
&#x9;&#x9;&#x9;result = audit_comparator(pid, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_UID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(current_uid(), f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_GID:
&#x9;&#x9;&#x9;result = audit_gid_comparator(current_gid(), f-&#x3E;op, f-&#x3E;gid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_LOGINUID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(audit_get_loginuid(current),
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_LOGINUID_SET:
&#x9;&#x9;&#x9;result = audit_comparator(audit_loginuid_set(current),
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_MSGTYPE:
&#x9;&#x9;&#x9;result = audit_comparator(type, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_SUBJ_USER:
&#x9;&#x9;case AUDIT_SUBJ_ROLE:
&#x9;&#x9;case AUDIT_SUBJ_TYPE:
&#x9;&#x9;case AUDIT_SUBJ_SEN:
&#x9;&#x9;case AUDIT_SUBJ_CLR:
&#x9;&#x9;&#x9;if (f-&#x3E;lsm_rule) {
&#x9;&#x9;&#x9;&#x9;security_task_getsecid(current, &#x26;sid);
&#x9;&#x9;&#x9;&#x9;result = security_audit_rule_match(sid,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   f-&#x3E;type,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   f-&#x3E;op,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   f-&#x3E;lsm_rule,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;   NULL);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}

&#x9;&#x9;if (!result)
&#x9;&#x9;&#x9;return 0;
&#x9;}
&#x9;switch (rule-&#x3E;action) {
&#x9;case AUDIT_NEVER:   state = AUDIT_DISABLED;&#x9;    break;
&#x9;case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
&#x9;}
&#x9;return 1;
}

int audit_filter_user(int type)
{
&#x9;enum audit_state state = AUDIT_DISABLED;
&#x9;struct audit_entrye;
&#x9;int rc, ret;

&#x9;ret = 1;/ Audit by default /*

&#x9;rcu_read_lock();
&#x9;list_for_each_entry_rcu(e, &#x26;audit_filter_list[AUDIT_FILTER_USER], list) {
&#x9;&#x9;rc = audit_filter_user_rules(&#x26;e-&#x3E;rule, type, &#x26;state);
&#x9;&#x9;if (rc) {
&#x9;&#x9;&#x9;if (rc &#x3E; 0 &#x26;&#x26; state == AUDIT_DISABLED)
&#x9;&#x9;&#x9;&#x9;ret = 0;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;}
&#x9;rcu_read_unlock();

&#x9;return ret;
}

int audit_filter_type(int type)
{
&#x9;struct audit_entrye;
&#x9;int result = 0;

&#x9;rcu_read_lock();
&#x9;if (list_empty(&#x26;audit_filter_list[AUDIT_FILTER_TYPE]))
&#x9;&#x9;goto unlock_and_return;

&#x9;list_for_each_entry_rcu(e, &#x26;audit_filter_list[AUDIT_FILTER_TYPE],
&#x9;&#x9;&#x9;&#x9;list) {
&#x9;&#x9;int i;
&#x9;&#x9;for (i = 0; i &#x3C; e-&#x3E;rule.field_count; i++) {
&#x9;&#x9;&#x9;struct audit_fieldf = &#x26;e-&#x3E;rule.fields[i];
&#x9;&#x9;&#x9;if (f-&#x3E;type == AUDIT_MSGTYPE) {
&#x9;&#x9;&#x9;&#x9;result = audit_comparator(type, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;&#x9;if (!result)
&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;&#x9;if (result)
&#x9;&#x9;&#x9;goto unlock_and_return;
&#x9;}
unlock_and_return:
&#x9;rcu_read_unlock();
&#x9;return result;
}

static int update_lsm_rule(struct audit_kruler)
{
&#x9;struct audit_entryentry = container_of(r, struct audit_entry, rule);
&#x9;struct audit_entrynentry;
&#x9;int err = 0;

&#x9;if (!security_audit_rule_known(r))
&#x9;&#x9;return 0;

&#x9;nentry = audit_dupe_rule(r);
&#x9;if (entry-&#x3E;rule.exe)
&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);
&#x9;if (IS_ERR(nentry)) {
&#x9;&#x9;*/ save the first error encountered for the
&#x9;&#x9; return value /*
&#x9;&#x9;err = PTR_ERR(nentry);
&#x9;&#x9;audit_panic(&#x22;error updating LSM filters&#x22;);
&#x9;&#x9;if (r-&#x3E;watch)
&#x9;&#x9;&#x9;list_del(&#x26;r-&#x3E;rlist);
&#x9;&#x9;list_del_rcu(&#x26;entry-&#x3E;list);
&#x9;&#x9;list_del(&#x26;r-&#x3E;list);
&#x9;} else {
&#x9;&#x9;if (r-&#x3E;watch || r-&#x3E;tree)
&#x9;&#x9;&#x9;list_replace_init(&#x26;r-&#x3E;rlist, &#x26;nentry-&#x3E;rule.rlist);
&#x9;&#x9;list_replace_rcu(&#x26;entry-&#x3E;list, &#x26;nentry-&#x3E;list);
&#x9;&#x9;list_replace(&#x26;r-&#x3E;list, &#x26;nentry-&#x3E;rule.list);
&#x9;}
&#x9;call_rcu(&#x26;entry-&#x3E;rcu, audit_free_rule_rcu);

&#x9;return err;
}

*/ This function will re-initialize the lsm_rule field of all applicable rules.
 It will traverse the filter lists serarching for rules that contain LSM
 specific filter fields.  When such a rule is found, it is copied, the
 LSM field is re-initialized, and the old rule is replaced with the
 updated rule. /*
int audit_update_lsm_rules(void)
{
&#x9;struct audit_kruler,n;
&#x9;int i, err = 0;

&#x9;*/ audit_filter_mutex synchronizes the writers /*
&#x9;mutex_lock(&#x26;audit_filter_mutex);

&#x9;for (i = 0; i &#x3C; AUDIT_NR_FILTERS; i++) {
&#x9;&#x9;list_for_each_entry_safe(r, n, &#x26;audit_rules_list[i], list) {
&#x9;&#x9;&#x9;int res = update_lsm_rule(r);
&#x9;&#x9;&#x9;if (!err)
&#x9;&#x9;&#x9;&#x9;err = res;
&#x9;&#x9;}
&#x9;}
&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;return err;
}
*/
 audit_fsnotify.c -- tracking inodes

 Copyright 2003-2009,2014-2015 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 /*

#include &#x3C;linux/kernel.h&#x3E;
#include &#x3C;linux/audit.h&#x3E;
#include &#x3C;linux/kthread.h&#x3E;
#include &#x3C;linux/mutex.h&#x3E;
#include &#x3C;linux/fs.h&#x3E;
#include &#x3C;linux/fsnotify_backend.h&#x3E;
#include &#x3C;linux/namei.h&#x3E;
#include &#x3C;linux/netlink.h&#x3E;
#include &#x3C;linux/sched.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/security.h&#x3E;
#include &#x22;audit.h&#x22;

*/
 this mark lives on the parent directory of the inode in question.
 but dev, ino, and path are about the child
 /*
struct audit_fsnotify_mark {
&#x9;dev_t dev;&#x9;&#x9;*/ associated superblock device /*
&#x9;unsigned long ino;&#x9;*/ associated inode number /*
&#x9;charpath;&#x9;&#x9;*/ insertion path /*
&#x9;struct fsnotify_mark mark;/ fsnotify mark on the inode /*
&#x9;struct audit_krulerule;
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_fsnotify_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_EVENTS (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
&#x9;&#x9;&#x9; FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_fsnotify_mark_free(struct audit_fsnotify_markaudit_mark)
{
&#x9;kfree(audit_mark-&#x3E;path);
&#x9;kfree(audit_mark);
}

static void audit_fsnotify_free_mark(struct fsnotify_markmark)
{
&#x9;struct audit_fsnotify_markaudit_mark;

&#x9;audit_mark = container_of(mark, struct audit_fsnotify_mark, mark);
&#x9;audit_fsnotify_mark_free(audit_mark);
}

charaudit_mark_path(struct audit_fsnotify_markmark)
{
&#x9;return mark-&#x3E;path;
}

int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev)
{
&#x9;if (mark-&#x3E;ino == AUDIT_INO_UNSET)
&#x9;&#x9;return 0;
&#x9;return (mark-&#x3E;ino == ino) &#x26;&#x26; (mark-&#x3E;dev == dev);
}

static void audit_update_mark(struct audit_fsnotify_markaudit_mark,
&#x9;&#x9;&#x9;     struct inodeinode)
{
&#x9;audit_mark-&#x3E;dev = inode ? inode-&#x3E;i_sb-&#x3E;s_dev : AUDIT_DEV_UNSET;
&#x9;audit_mark-&#x3E;ino = inode ? inode-&#x3E;i_ino : AUDIT_INO_UNSET;
}

struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len)
{
&#x9;struct audit_fsnotify_markaudit_mark;
&#x9;struct path path;
&#x9;struct dentrydentry;
&#x9;struct inodeinode;
&#x9;int ret;

&#x9;if (pathname[0] != &#x27;/&#x27; || pathname[len-1] == &#x27;/&#x27;)
&#x9;&#x9;return ERR_PTR(-EINVAL);

&#x9;dentry = kern_path_locked(pathname, &#x26;path);
&#x9;if (IS_ERR(dentry))
&#x9;&#x9;return (void)dentry;/ returning an error /*
&#x9;inode = path.dentry-&#x3E;d_inode;
&#x9;inode_unlock(inode);

&#x9;audit_mark = kzalloc(sizeof(*audit_mark), GFP_KERNEL);
&#x9;if (unlikely(!audit_mark)) {
&#x9;&#x9;audit_mark = ERR_PTR(-ENOMEM);
&#x9;&#x9;goto out;
&#x9;}

&#x9;fsnotify_init_mark(&#x26;audit_mark-&#x3E;mark, audit_fsnotify_free_mark);
&#x9;audit_mark-&#x3E;mark.mask = AUDIT_FS_EVENTS;
&#x9;audit_mark-&#x3E;path = pathname;
&#x9;audit_update_mark(audit_mark, dentry-&#x3E;d_inode);
&#x9;audit_mark-&#x3E;rule = krule;

&#x9;ret = fsnotify_add_mark(&#x26;audit_mark-&#x3E;mark, audit_fsnotify_group, inode, NULL, true);
&#x9;if (ret &#x3C; 0) {
&#x9;&#x9;audit_fsnotify_mark_free(audit_mark);
&#x9;&#x9;audit_mark = ERR_PTR(ret);
&#x9;}
out:
&#x9;dput(dentry);
&#x9;path_put(&#x26;path);
&#x9;return audit_mark;
}

static void audit_mark_log_rule_change(struct audit_fsnotify_markaudit_mark, charop)
{
&#x9;struct audit_bufferab;
&#x9;struct audit_krulerule = audit_mark-&#x3E;rule;

&#x9;if (!audit_enabled)
&#x9;&#x9;return;
&#x9;ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
&#x9;if (unlikely(!ab))
&#x9;&#x9;return;
&#x9;audit_log_format(ab, &#x22;auid=%u ses=%u op=&#x22;,
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, audit_get_loginuid(current)),
&#x9;&#x9;&#x9; audit_get_sessionid(current));
&#x9;audit_log_string(ab, op);
&#x9;audit_log_format(ab, &#x22; path=&#x22;);
&#x9;audit_log_untrustedstring(ab, audit_mark-&#x3E;path);
&#x9;audit_log_key(ab, rule-&#x3E;filterkey);
&#x9;audit_log_format(ab, &#x22; list=%d res=1&#x22;, rule-&#x3E;listnr);
&#x9;audit_log_end(ab);
}

void audit_remove_mark(struct audit_fsnotify_markaudit_mark)
{
&#x9;fsnotify_destroy_mark(&#x26;audit_mark-&#x3E;mark, audit_fsnotify_group);
&#x9;fsnotify_put_mark(&#x26;audit_mark-&#x3E;mark);
}

void audit_remove_mark_rule(struct audit_krulekrule)
{
&#x9;struct audit_fsnotify_markmark = krule-&#x3E;exe;

&#x9;audit_remove_mark(mark);
}

static void audit_autoremove_mark_rule(struct audit_fsnotify_markaudit_mark)
{
&#x9;struct audit_krulerule = audit_mark-&#x3E;rule;
&#x9;struct audit_entryentry = container_of(rule, struct audit_entry, rule);

&#x9;audit_mark_log_rule_change(audit_mark, &#x22;autoremove_rule&#x22;);
&#x9;audit_del_rule(entry);
}

*/ Update mark data in audit rules based on fsnotify events. /*
static int audit_mark_handle_event(struct fsnotify_groupgroup,
&#x9;&#x9;&#x9;&#x9;    struct inodeto_tell,
&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markinode_mark,
&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markvfsmount_mark,
&#x9;&#x9;&#x9;&#x9;    u32 mask, voiddata, int data_type,
&#x9;&#x9;&#x9;&#x9;    const unsigned chardname, u32 cookie)
{
&#x9;struct audit_fsnotify_markaudit_mark;
&#x9;struct inodeinode = NULL;

&#x9;audit_mark = container_of(inode_mark, struct audit_fsnotify_mark, mark);

&#x9;BUG_ON(group != audit_fsnotify_group);

&#x9;switch (data_type) {
&#x9;case (FSNOTIFY_EVENT_PATH):
&#x9;&#x9;inode = ((struct path)data)-&#x3E;dentry-&#x3E;d_inode;
&#x9;&#x9;break;
&#x9;case (FSNOTIFY_EVENT_INODE):
&#x9;&#x9;inode = (struct inode)data;
&#x9;&#x9;break;
&#x9;default:
&#x9;&#x9;BUG();
&#x9;&#x9;return 0;
&#x9;};

&#x9;if (mask &#x26; (FS_CREATE|FS_MOVED_TO|FS_DELETE|FS_MOVED_FROM)) {
&#x9;&#x9;if (audit_compare_dname_path(dname, audit_mark-&#x3E;path, AUDIT_NAME_FULL))
&#x9;&#x9;&#x9;return 0;
&#x9;&#x9;audit_update_mark(audit_mark, inode);
&#x9;} else if (mask &#x26; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
&#x9;&#x9;audit_autoremove_mark_rule(audit_mark);

&#x9;return 0;
}

static const struct fsnotify_ops audit_mark_fsnotify_ops = {
&#x9;.handle_event =&#x9;audit_mark_handle_event,
};

static int __init audit_fsnotify_init(void)
{
&#x9;audit_fsnotify_group = fsnotify_alloc_group(&#x26;audit_mark_fsnotify_ops);
&#x9;if (IS_ERR(audit_fsnotify_group)) {
&#x9;&#x9;audit_fsnotify_group = NULL;
&#x9;&#x9;audit_panic(&#x22;cannot create audit fsnotify group&#x22;);
&#x9;}
&#x9;return 0;
}
device_initcall(audit_fsnotify_init);
*/
 audit -- definition of audit_context structure and supporting types 

 Copyright 2003-2004 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include &#x3C;linux/fs.h&#x3E;
#include &#x3C;linux/audit.h&#x3E;
#include &#x3C;linux/skbuff.h&#x3E;
#include &#x3C;uapi/linux/mqueue.h&#x3E;

*/ AUDIT_NAMES is the number of slots we reserve in the audit_context
 for saving names from getname().  If we get more names we will allocate
 a name dynamically and also add those to the list anchored by names_list. /*
#define AUDIT_NAMES&#x9;5

*/ At task start time, the audit_state is set in the audit_context using
   a per-task filter.  At syscall entry, the audit_state is augmented by
   the syscall filter. /*
enum audit_state {
&#x9;AUDIT_DISABLED,&#x9;&#x9;*/ Do not create per-task audit_context.
&#x9;&#x9;&#x9;&#x9; No syscall-specific audit records can
&#x9;&#x9;&#x9;&#x9; be generated. /*
&#x9;AUDIT_BUILD_CONTEXT,&#x9;*/ Create the per-task audit_context,
&#x9;&#x9;&#x9;&#x9; and fill it in at syscall
&#x9;&#x9;&#x9;&#x9; entry time.  This makes a full
&#x9;&#x9;&#x9;&#x9; syscall record available if some
&#x9;&#x9;&#x9;&#x9; other part of the kernel decides it
&#x9;&#x9;&#x9;&#x9; should be recorded. /*
&#x9;AUDIT_RECORD_CONTEXT&#x9;*/ Create the per-task audit_context,
&#x9;&#x9;&#x9;&#x9; always fill it in at syscall entry
&#x9;&#x9;&#x9;&#x9; time, and always write out the audit
&#x9;&#x9;&#x9;&#x9; record at syscall exit time.  /*
};

*/ Rule lists /*
struct audit_watch;
struct audit_fsnotify_mark;
struct audit_tree;
struct audit_chunk;

struct audit_entry {
&#x9;struct list_head&#x9;list;
&#x9;struct rcu_head&#x9;&#x9;rcu;
&#x9;struct audit_krule&#x9;rule;
};

struct audit_cap_data {
&#x9;kernel_cap_t&#x9;&#x9;permitted;
&#x9;kernel_cap_t&#x9;&#x9;inheritable;
&#x9;union {
&#x9;&#x9;unsigned int&#x9;fE;&#x9;&#x9;*/ effective bit of file cap /*
&#x9;&#x9;kernel_cap_t&#x9;effective;&#x9;*/ effective set of process /*
&#x9;};
};

*/ When fs/namei.c:getname() is called, we store the pointer in name and bump
 the refcnt in the associated filename struct.

 Further, in fs/namei.c:path_lookup() we store the inode and device.
 /*
struct audit_names {
&#x9;struct list_head&#x9;list;&#x9;&#x9;*/ audit_context-&#x3E;names_list /*

&#x9;struct filename&#x9;&#x9;*name;
&#x9;int&#x9;&#x9;&#x9;name_len;&#x9;*/ number of chars to log /*
&#x9;bool&#x9;&#x9;&#x9;hidden;&#x9;&#x9;*/ don&#x27;t log this record /*

&#x9;unsigned long&#x9;&#x9;ino;
&#x9;dev_t&#x9;&#x9;&#x9;dev;
&#x9;umode_t&#x9;&#x9;&#x9;mode;
&#x9;kuid_t&#x9;&#x9;&#x9;uid;
&#x9;kgid_t&#x9;&#x9;&#x9;gid;
&#x9;dev_t&#x9;&#x9;&#x9;rdev;
&#x9;u32&#x9;&#x9;&#x9;osid;
&#x9;struct audit_cap_data&#x9;fcap;
&#x9;unsigned int&#x9;&#x9;fcap_ver;
&#x9;unsigned char&#x9;&#x9;type;&#x9;&#x9;*/ record type /*
&#x9;*/
&#x9; This was an allocated audit_names and not from the array of
&#x9; names allocated in the task audit context.  Thus this name
&#x9; should be freed on syscall exit.
&#x9; /*
&#x9;bool&#x9;&#x9;&#x9;should_free;
};

struct audit_proctitle {
&#x9;int&#x9;len;&#x9;*/ length of the cmdline field. /*
&#x9;char&#x9;*value;&#x9;*/ the cmdline field /*
};

*/ The per-task audit context. /*
struct audit_context {
&#x9;int&#x9;&#x9;    dummy;&#x9;*/ must be the first element /*
&#x9;int&#x9;&#x9;    in_syscall;&#x9;*/ 1 if task is in a syscall /*
&#x9;enum audit_state    state, current_state;
&#x9;unsigned int&#x9;    serial;    / serial number for record /*
&#x9;int&#x9;&#x9;    major;     / syscall number /*
&#x9;struct timespec&#x9;    ctime;     / time of syscall entry /*
&#x9;unsigned long&#x9;    argv[4];   / syscall arguments /*
&#x9;long&#x9;&#x9;    return_code;*/ syscall return code /*
&#x9;u64&#x9;&#x9;    prio;
&#x9;int&#x9;&#x9;    return_valid;/ return code is valid /*
&#x9;*/
&#x9; The names_list is the list of all audit_names collected during this
&#x9; syscall.  The first AUDIT_NAMES entries in the names_list will
&#x9; actually be from the preallocated_names array for performance
&#x9; reasons.  Except during allocation they should never be referenced
&#x9; through the preallocated_names array and should only be found/used
&#x9; by running the names_list.
&#x9; /*
&#x9;struct audit_names  preallocated_names[AUDIT_NAMES];
&#x9;int&#x9;&#x9;    name_count;/ total records in names_list /*
&#x9;struct list_head    names_list;&#x9;*/ struct audit_names-&#x3E;list anchor /*
&#x9;char&#x9;&#x9;   filterkey;&#x9;*/ key for rule that triggered record /*
&#x9;struct path&#x9;    pwd;
&#x9;struct audit_aux_dataaux;
&#x9;struct audit_aux_dataaux_pids;
&#x9;struct sockaddr_storagesockaddr;
&#x9;size_t sockaddr_len;
&#x9;&#x9;&#x9;&#x9;*/ Save things to print about task_struct /*
&#x9;pid_t&#x9;&#x9;    pid, ppid;
&#x9;kuid_t&#x9;&#x9;    uid, euid, suid, fsuid;
&#x9;kgid_t&#x9;&#x9;    gid, egid, sgid, fsgid;
&#x9;unsigned long&#x9;    personality;
&#x9;int&#x9;&#x9;    arch;

&#x9;pid_t&#x9;&#x9;    target_pid;
&#x9;kuid_t&#x9;&#x9;    target_auid;
&#x9;kuid_t&#x9;&#x9;    target_uid;
&#x9;unsigned int&#x9;    target_sessionid;
&#x9;u32&#x9;&#x9;    target_sid;
&#x9;char&#x9;&#x9;    target_comm[TASK_COMM_LEN];

&#x9;struct audit_tree_refstrees,first_trees;
&#x9;struct list_head killed_trees;
&#x9;int tree_count;

&#x9;int type;
&#x9;union {
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;int nargs;
&#x9;&#x9;&#x9;long args[6];
&#x9;&#x9;} socketcall;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;kuid_t&#x9;&#x9;&#x9;uid;
&#x9;&#x9;&#x9;kgid_t&#x9;&#x9;&#x9;gid;
&#x9;&#x9;&#x9;umode_t&#x9;&#x9;&#x9;mode;
&#x9;&#x9;&#x9;u32&#x9;&#x9;&#x9;osid;
&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;has_perm;
&#x9;&#x9;&#x9;uid_t&#x9;&#x9;&#x9;perm_uid;
&#x9;&#x9;&#x9;gid_t&#x9;&#x9;&#x9;perm_gid;
&#x9;&#x9;&#x9;umode_t&#x9;&#x9;&#x9;perm_mode;
&#x9;&#x9;&#x9;unsigned long&#x9;&#x9;qbytes;
&#x9;&#x9;} ipc;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;mqd_t&#x9;&#x9;&#x9;mqdes;
&#x9;&#x9;&#x9;struct mq_attr&#x9;&#x9;mqstat;
&#x9;&#x9;} mq_getsetattr;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;mqd_t&#x9;&#x9;&#x9;mqdes;
&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;sigev_signo;
&#x9;&#x9;} mq_notify;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;mqd_t&#x9;&#x9;&#x9;mqdes;
&#x9;&#x9;&#x9;size_t&#x9;&#x9;&#x9;msg_len;
&#x9;&#x9;&#x9;unsigned int&#x9;&#x9;msg_prio;
&#x9;&#x9;&#x9;struct timespec&#x9;&#x9;abs_timeout;
&#x9;&#x9;} mq_sendrecv;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;oflag;
&#x9;&#x9;&#x9;umode_t&#x9;&#x9;&#x9;mode;
&#x9;&#x9;&#x9;struct mq_attr&#x9;&#x9;attr;
&#x9;&#x9;} mq_open;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;pid_t&#x9;&#x9;&#x9;pid;
&#x9;&#x9;&#x9;struct audit_cap_data&#x9;cap;
&#x9;&#x9;} capset;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;fd;
&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;flags;
&#x9;&#x9;} mmap;
&#x9;&#x9;struct {
&#x9;&#x9;&#x9;int&#x9;&#x9;&#x9;argc;
&#x9;&#x9;} execve;
&#x9;};
&#x9;int fds[2];
&#x9;struct audit_proctitle proctitle;
};

extern u32 audit_ever_enabled;

extern void audit_copy_inode(struct audit_namesname,
&#x9;&#x9;&#x9;     const struct dentrydentry,
&#x9;&#x9;&#x9;     struct inodeinode);
extern void audit_log_cap(struct audit_bufferab, charprefix,
&#x9;&#x9;&#x9;  kernel_cap_tcap);
extern void audit_log_name(struct audit_contextcontext,
&#x9;&#x9;&#x9;   struct audit_namesn, struct pathpath,
&#x9;&#x9;&#x9;   int record_num, intcall_panic);

extern int audit_pid;

#define AUDIT_INODE_BUCKETS&#x9;32
extern struct list_head audit_inode_hash[AUDIT_INODE_BUCKETS];

static inline int audit_hash_ino(u32 ino)
{
&#x9;return (ino &#x26; (AUDIT_INODE_BUCKETS-1));
}

*/ Indicates that audit should log the full pathname. /*
#define AUDIT_NAME_FULL -1

extern int audit_match_class(int class, unsigned syscall);
extern int audit_comparator(const u32 left, const u32 op, const u32 right);
extern int audit_uid_comparator(kuid_t left, u32 op, kuid_t right);
extern int audit_gid_comparator(kgid_t left, u32 op, kgid_t right);
extern int parent_len(const charpath);
extern int audit_compare_dname_path(const chardname, const charpath, int plen);
extern struct sk_buffaudit_make_reply(__u32 portid, int seq, int type,
&#x9;&#x9;&#x9;&#x9;&#x9;int done, int multi,
&#x9;&#x9;&#x9;&#x9;&#x9;const voidpayload, int size);
extern void&#x9;&#x9;    audit_panic(const charmessage);

struct audit_netlink_list {
&#x9;__u32 portid;
&#x9;struct netnet;
&#x9;struct sk_buff_head q;
};

int audit_send_list(void);

struct audit_net {
&#x9;struct socknlsk;
};

extern int selinux_audit_rule_update(void);

extern struct mutex audit_filter_mutex;
extern int audit_del_rule(struct audit_entry);
extern void audit_free_rule_rcu(struct rcu_head);
extern struct list_head audit_filter_list[];

extern struct audit_entryaudit_dupe_rule(struct audit_kruleold);

extern void audit_log_d_path_exe(struct audit_bufferab,
&#x9;&#x9;&#x9;&#x9; struct mm_structmm);

*/ audit watch functions /*
#ifdef CONFIG_AUDIT_WATCH
extern void audit_put_watch(struct audit_watchwatch);
extern void audit_get_watch(struct audit_watchwatch);
extern int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op);
extern int audit_add_watch(struct audit_krulekrule, struct list_head*list);
extern void audit_remove_watch_rule(struct audit_krulekrule);
extern charaudit_watch_path(struct audit_watchwatch);
extern int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev);

extern struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len);
extern charaudit_mark_path(struct audit_fsnotify_markmark);
extern void audit_remove_mark(struct audit_fsnotify_markaudit_mark);
extern void audit_remove_mark_rule(struct audit_krulekrule);
extern int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev);
extern int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold);
extern int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark);

#else
#define audit_put_watch(w) {}
#define audit_get_watch(w) {}
#define audit_to_watch(k, p, l, o) (-EINVAL)
#define audit_add_watch(k, l) (-EINVAL)
#define audit_remove_watch_rule(k) BUG()
#define audit_watch_path(w) &#x22;&#x22;
#define audit_watch_compare(w, i, d) 0

#define audit_alloc_mark(k, p, l) (ERR_PTR(-EINVAL))
#define audit_mark_path(m) &#x22;&#x22;
#define audit_remove_mark(m)
#define audit_remove_mark_rule(k)
#define audit_mark_compare(m, i, d) 0
#define audit_exe_compare(t, m) (-EINVAL)
#define audit_dupe_exe(n, o) (-EINVAL)
#endif */ CONFIG_AUDIT_WATCH /*

#ifdef CONFIG_AUDIT_TREE
extern struct audit_chunkaudit_tree_lookup(const struct inode);
extern void audit_put_chunk(struct audit_chunk);
extern bool audit_tree_match(struct audit_chunk, struct audit_tree);
extern int audit_make_tree(struct audit_krule, char, u32);
extern int audit_add_tree_rule(struct audit_krule);
extern int audit_remove_tree_rule(struct audit_krule);
extern void audit_trim_trees(void);
extern int audit_tag_tree(charold, charnew);
extern const charaudit_tree_path(struct audit_tree);
extern void audit_put_tree(struct audit_tree);
extern void audit_kill_trees(struct list_head);
#else
#define audit_remove_tree_rule(rule) BUG()
#define audit_add_tree_rule(rule) -EINVAL
#define audit_make_tree(rule, str, op) -EINVAL
#define audit_trim_trees() (void)0
#define audit_put_tree(tree) (void)0
#define audit_tag_tree(old, new) -EINVAL
#define audit_tree_path(rule) &#x22;&#x22;&#x9;*/ never called /*
#define audit_kill_trees(list) BUG()
#endif

extern charaudit_unpack_string(void*, size_t, size_t);

extern pid_t audit_sig_pid;
extern kuid_t audit_sig_uid;
extern u32 audit_sig_sid;

#ifdef CONFIG_AUDITSYSCALL
extern int __audit_signal_info(int sig, struct task_structt);
static inline int audit_signal_info(int sig, struct task_structt)
{
&#x9;if (unlikely((audit_pid &#x26;&#x26; t-&#x3E;tgid == audit_pid) ||
&#x9;&#x9;     (audit_signals &#x26;&#x26; !audit_dummy_context())))
&#x9;&#x9;return __audit_signal_info(sig, t);
&#x9;return 0;
}
extern void audit_filter_inodes(struct task_struct, struct audit_context);
extern struct list_headaudit_killed_trees(void);
#else
#define audit_signal_info(s,t) AUDIT_DISABLED
#define audit_filter_inodes(t,c) AUDIT_DISABLED
#endif

extern struct mutex audit_cmd_mutex;
*/
 auditsc.c -- System-call auditing support
 Handles all system-call specific auditing features.

 Copyright 2003-2004 Red Hat Inc., Durham, North Carolina.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright (C) 2005, 2006 IBM Corporation
 All Rights Reserved.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Written by Rickard E. (Rik) Faith &#x3C;faith@redhat.com&#x3E;

 Many of the ideas implemented here are from Stephen C. Tweedie,
 especially the idea of avoiding a copy by using getname.

 The method for actual interception of syscall entry and exit (not in
 this file -- see entry.S) is based on a GPL&#x27;d patch written by
 okir@suse.de and Copyright 2003 SuSE Linux AG.

 POSIX message queue support added by George Wilson &#x3C;ltcgcw@us.ibm.com&#x3E;,
 2006.

 The support of additional filter rules compares (&#x3E;, &#x3C;, &#x3E;=, &#x3C;=) was
 added by Dustin Kirkland &#x3C;dustin.kirkland@us.ibm.com&#x3E;, 2005.

 Modified by Amy Griffis &#x3C;amy.griffis@hp.com&#x3E; to collect additional
 filesystem information.

 Subject and object context labeling support added by &#x3C;danjones@us.ibm.com&#x3E;
 and &#x3C;dustin.kirkland@us.ibm.com&#x3E; for LSPP certification compliance.
 /*

#define pr_fmt(fmt) KBUILD_MODNAME &#x22;: &#x22; fmt

#include &#x3C;linux/init.h&#x3E;
#include &#x3C;asm/types.h&#x3E;
#include &#x3C;linux/atomic.h&#x3E;
#include &#x3C;linux/fs.h&#x3E;
#include &#x3C;linux/namei.h&#x3E;
#include &#x3C;linux/mm.h&#x3E;
#include &#x3C;linux/export.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/mount.h&#x3E;
#include &#x3C;linux/socket.h&#x3E;
#include &#x3C;linux/mqueue.h&#x3E;
#include &#x3C;linux/audit.h&#x3E;
#include &#x3C;linux/personality.h&#x3E;
#include &#x3C;linux/time.h&#x3E;
#include &#x3C;linux/netlink.h&#x3E;
#include &#x3C;linux/compiler.h&#x3E;
#include &#x3C;asm/unistd.h&#x3E;
#include &#x3C;linux/security.h&#x3E;
#include &#x3C;linux/list.h&#x3E;
#include &#x3C;linux/tty.h&#x3E;
#include &#x3C;linux/binfmts.h&#x3E;
#include &#x3C;linux/highmem.h&#x3E;
#include &#x3C;linux/syscalls.h&#x3E;
#include &#x3C;asm/syscall.h&#x3E;
#include &#x3C;linux/capability.h&#x3E;
#include &#x3C;linux/fs_struct.h&#x3E;
#include &#x3C;linux/compat.h&#x3E;
#include &#x3C;linux/ctype.h&#x3E;
#include &#x3C;linux/string.h&#x3E;
#include &#x3C;uapi/linux/limits.h&#x3E;

#include &#x22;audit.h&#x22;

*/ flags stating the success for a syscall /*
#define AUDITSC_INVALID 0
#define AUDITSC_SUCCESS 1
#define AUDITSC_FAILURE 2

*/ no execve audit message should be longer than this (userspace limits) /*
#define MAX_EXECVE_AUDIT_LEN 7500

*/ max length to print of cmdline/proctitle value during audit /*
#define MAX_PROCTITLE_AUDIT_LEN 128

*/ number of audit rules /*
int audit_n_rules;

*/ determines whether we collect data for signals sent /*
int audit_signals;

struct audit_aux_data {
&#x9;struct audit_aux_data&#x9;*next;
&#x9;int&#x9;&#x9;&#x9;type;
};

#define AUDIT_AUX_IPCPERM&#x9;0

*/ Number of target pids per aux struct. /*
#define AUDIT_AUX_PIDS&#x9;16

struct audit_aux_data_pids {
&#x9;struct audit_aux_data&#x9;d;
&#x9;pid_t&#x9;&#x9;&#x9;target_pid[AUDIT_AUX_PIDS];
&#x9;kuid_t&#x9;&#x9;&#x9;target_auid[AUDIT_AUX_PIDS];
&#x9;kuid_t&#x9;&#x9;&#x9;target_uid[AUDIT_AUX_PIDS];
&#x9;unsigned int&#x9;&#x9;target_sessionid[AUDIT_AUX_PIDS];
&#x9;u32&#x9;&#x9;&#x9;target_sid[AUDIT_AUX_PIDS];
&#x9;char &#x9;&#x9;&#x9;target_comm[AUDIT_AUX_PIDS][TASK_COMM_LEN];
&#x9;int&#x9;&#x9;&#x9;pid_count;
};

struct audit_aux_data_bprm_fcaps {
&#x9;struct audit_aux_data&#x9;d;
&#x9;struct audit_cap_data&#x9;fcap;
&#x9;unsigned int&#x9;&#x9;fcap_ver;
&#x9;struct audit_cap_data&#x9;old_pcap;
&#x9;struct audit_cap_data&#x9;new_pcap;
};

struct audit_tree_refs {
&#x9;struct audit_tree_refsnext;
&#x9;struct audit_chunkc[31];
};

static int audit_match_perm(struct audit_contextctx, int mask)
{
&#x9;unsigned n;
&#x9;if (unlikely(!ctx))
&#x9;&#x9;return 0;
&#x9;n = ctx-&#x3E;major;

&#x9;switch (audit_classify_syscall(ctx-&#x3E;arch, n)) {
&#x9;case 0:&#x9;*/ native /*
&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_WRITE) &#x26;&#x26;
&#x9;&#x9;     audit_match_class(AUDIT_CLASS_WRITE, n))
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_READ) &#x26;&#x26;
&#x9;&#x9;     audit_match_class(AUDIT_CLASS_READ, n))
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_ATTR) &#x26;&#x26;
&#x9;&#x9;     audit_match_class(AUDIT_CLASS_CHATTR, n))
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;return 0;
&#x9;case 1:/ 32bit on biarch /*
&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_WRITE) &#x26;&#x26;
&#x9;&#x9;     audit_match_class(AUDIT_CLASS_WRITE_32, n))
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_READ) &#x26;&#x26;
&#x9;&#x9;     audit_match_class(AUDIT_CLASS_READ_32, n))
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;if ((mask &#x26; AUDIT_PERM_ATTR) &#x26;&#x26;
&#x9;&#x9;     audit_match_class(AUDIT_CLASS_CHATTR_32, n))
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;return 0;
&#x9;case 2:/ open /*
&#x9;&#x9;return mask &#x26; ACC_MODE(ctx-&#x3E;argv[1]);
&#x9;case 3:/ openat /*
&#x9;&#x9;return mask &#x26; ACC_MODE(ctx-&#x3E;argv[2]);
&#x9;case 4:/ socketcall /*
&#x9;&#x9;return ((mask &#x26; AUDIT_PERM_WRITE) &#x26;&#x26; ctx-&#x3E;argv[0] == SYS_BIND);
&#x9;case 5:/ execve /*
&#x9;&#x9;return mask &#x26; AUDIT_PERM_EXEC;
&#x9;default:
&#x9;&#x9;return 0;
&#x9;}
}

static int audit_match_filetype(struct audit_contextctx, int val)
{
&#x9;struct audit_namesn;
&#x9;umode_t mode = (umode_t)val;

&#x9;if (unlikely(!ctx))
&#x9;&#x9;return 0;

&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;if ((n-&#x3E;ino != AUDIT_INO_UNSET) &#x26;&#x26;
&#x9;&#x9;    ((n-&#x3E;mode &#x26; S_IFMT) == mode))
&#x9;&#x9;&#x9;return 1;
&#x9;}

&#x9;return 0;
}

*/
 We keep a linked list of fixed-sized (31 pointer) arrays of audit_chunk;
 -&#x3E;first_trees points to its beginning, -&#x3E;trees - to the current end of data.
 -&#x3E;tree_count is the number of free entries in array pointed to by -&#x3E;trees.
 Original condition is (NULL, NULL, 0); as soon as it grows we never revert to NULL,
 &#x22;empty&#x22; becomes (p, p, 31) afterwards.  We don&#x27;t shrink the list (and seriously,
 it&#x27;s going to remain 1-element for almost any setup) until we free context itself.
 References in it _are_ dropped - at the same time we free/drop aux stuff.
 /*

#ifdef CONFIG_AUDIT_TREE
static void audit_set_auditable(struct audit_contextctx)
{
&#x9;if (!ctx-&#x3E;prio) {
&#x9;&#x9;ctx-&#x3E;prio = 1;
&#x9;&#x9;ctx-&#x3E;current_state = AUDIT_RECORD_CONTEXT;
&#x9;}
}

static int put_tree_ref(struct audit_contextctx, struct audit_chunkchunk)
{
&#x9;struct audit_tree_refsp = ctx-&#x3E;trees;
&#x9;int left = ctx-&#x3E;tree_count;
&#x9;if (likely(left)) {
&#x9;&#x9;p-&#x3E;c[--left] = chunk;
&#x9;&#x9;ctx-&#x3E;tree_count = left;
&#x9;&#x9;return 1;
&#x9;}
&#x9;if (!p)
&#x9;&#x9;return 0;
&#x9;p = p-&#x3E;next;
&#x9;if (p) {
&#x9;&#x9;p-&#x3E;c[30] = chunk;
&#x9;&#x9;ctx-&#x3E;trees = p;
&#x9;&#x9;ctx-&#x3E;tree_count = 30;
&#x9;&#x9;return 1;
&#x9;}
&#x9;return 0;
}

static int grow_tree_refs(struct audit_contextctx)
{
&#x9;struct audit_tree_refsp = ctx-&#x3E;trees;
&#x9;ctx-&#x3E;trees = kzalloc(sizeof(struct audit_tree_refs), GFP_KERNEL);
&#x9;if (!ctx-&#x3E;trees) {
&#x9;&#x9;ctx-&#x3E;trees = p;
&#x9;&#x9;return 0;
&#x9;}
&#x9;if (p)
&#x9;&#x9;p-&#x3E;next = ctx-&#x3E;trees;
&#x9;else
&#x9;&#x9;ctx-&#x3E;first_trees = ctx-&#x3E;trees;
&#x9;ctx-&#x3E;tree_count = 31;
&#x9;return 1;
}
#endif

static void unroll_tree_refs(struct audit_contextctx,
&#x9;&#x9;      struct audit_tree_refsp, int count)
{
#ifdef CONFIG_AUDIT_TREE
&#x9;struct audit_tree_refsq;
&#x9;int n;
&#x9;if (!p) {
&#x9;&#x9;*/ we started with empty chain /*
&#x9;&#x9;p = ctx-&#x3E;first_trees;
&#x9;&#x9;count = 31;
&#x9;&#x9;*/ if the very first allocation has failed, nothing to do /*
&#x9;&#x9;if (!p)
&#x9;&#x9;&#x9;return;
&#x9;}
&#x9;n = count;
&#x9;for (q = p; q != ctx-&#x3E;trees; q = q-&#x3E;next, n = 31) {
&#x9;&#x9;while (n--) {
&#x9;&#x9;&#x9;audit_put_chunk(q-&#x3E;c[n]);
&#x9;&#x9;&#x9;q-&#x3E;c[n] = NULL;
&#x9;&#x9;}
&#x9;}
&#x9;while (n-- &#x3E; ctx-&#x3E;tree_count) {
&#x9;&#x9;audit_put_chunk(q-&#x3E;c[n]);
&#x9;&#x9;q-&#x3E;c[n] = NULL;
&#x9;}
&#x9;ctx-&#x3E;trees = p;
&#x9;ctx-&#x3E;tree_count = count;
#endif
}

static void free_tree_refs(struct audit_contextctx)
{
&#x9;struct audit_tree_refsp,q;
&#x9;for (p = ctx-&#x3E;first_trees; p; p = q) {
&#x9;&#x9;q = p-&#x3E;next;
&#x9;&#x9;kfree(p);
&#x9;}
}

static int match_tree_refs(struct audit_contextctx, struct audit_treetree)
{
#ifdef CONFIG_AUDIT_TREE
&#x9;struct audit_tree_refsp;
&#x9;int n;
&#x9;if (!tree)
&#x9;&#x9;return 0;
&#x9;*/ full ones /*
&#x9;for (p = ctx-&#x3E;first_trees; p != ctx-&#x3E;trees; p = p-&#x3E;next) {
&#x9;&#x9;for (n = 0; n &#x3C; 31; n++)
&#x9;&#x9;&#x9;if (audit_tree_match(p-&#x3E;c[n], tree))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;}
&#x9;*/ partial /*
&#x9;if (p) {
&#x9;&#x9;for (n = ctx-&#x3E;tree_count; n &#x3C; 31; n++)
&#x9;&#x9;&#x9;if (audit_tree_match(p-&#x3E;c[n], tree))
&#x9;&#x9;&#x9;&#x9;return 1;
&#x9;}
#endif
&#x9;return 0;
}

static int audit_compare_uid(kuid_t uid,
&#x9;&#x9;&#x9;     struct audit_namesname,
&#x9;&#x9;&#x9;     struct audit_fieldf,
&#x9;&#x9;&#x9;     struct audit_contextctx)
{
&#x9;struct audit_namesn;
&#x9;int rc;
 
&#x9;if (name) {
&#x9;&#x9;rc = audit_uid_comparator(uid, f-&#x3E;op, name-&#x3E;uid);
&#x9;&#x9;if (rc)
&#x9;&#x9;&#x9;return rc;
&#x9;}
 
&#x9;if (ctx) {
&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;rc = audit_uid_comparator(uid, f-&#x3E;op, n-&#x3E;uid);
&#x9;&#x9;&#x9;if (rc)
&#x9;&#x9;&#x9;&#x9;return rc;
&#x9;&#x9;}
&#x9;}
&#x9;return 0;
}

static int audit_compare_gid(kgid_t gid,
&#x9;&#x9;&#x9;     struct audit_namesname,
&#x9;&#x9;&#x9;     struct audit_fieldf,
&#x9;&#x9;&#x9;     struct audit_contextctx)
{
&#x9;struct audit_namesn;
&#x9;int rc;
 
&#x9;if (name) {
&#x9;&#x9;rc = audit_gid_comparator(gid, f-&#x3E;op, name-&#x3E;gid);
&#x9;&#x9;if (rc)
&#x9;&#x9;&#x9;return rc;
&#x9;}
 
&#x9;if (ctx) {
&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;rc = audit_gid_comparator(gid, f-&#x3E;op, n-&#x3E;gid);
&#x9;&#x9;&#x9;if (rc)
&#x9;&#x9;&#x9;&#x9;return rc;
&#x9;&#x9;}
&#x9;}
&#x9;return 0;
}

static int audit_field_compare(struct task_structtsk,
&#x9;&#x9;&#x9;       const struct credcred,
&#x9;&#x9;&#x9;       struct audit_fieldf,
&#x9;&#x9;&#x9;       struct audit_contextctx,
&#x9;&#x9;&#x9;       struct audit_namesname)
{
&#x9;switch (f-&#x3E;val) {
&#x9;*/ process to file object comparisons /*
&#x9;case AUDIT_COMPARE_UID_TO_OBJ_UID:
&#x9;&#x9;return audit_compare_uid(cred-&#x3E;uid, name, f, ctx);
&#x9;case AUDIT_COMPARE_GID_TO_OBJ_GID:
&#x9;&#x9;return audit_compare_gid(cred-&#x3E;gid, name, f, ctx);
&#x9;case AUDIT_COMPARE_EUID_TO_OBJ_UID:
&#x9;&#x9;return audit_compare_uid(cred-&#x3E;euid, name, f, ctx);
&#x9;case AUDIT_COMPARE_EGID_TO_OBJ_GID:
&#x9;&#x9;return audit_compare_gid(cred-&#x3E;egid, name, f, ctx);
&#x9;case AUDIT_COMPARE_AUID_TO_OBJ_UID:
&#x9;&#x9;return audit_compare_uid(tsk-&#x3E;loginuid, name, f, ctx);
&#x9;case AUDIT_COMPARE_SUID_TO_OBJ_UID:
&#x9;&#x9;return audit_compare_uid(cred-&#x3E;suid, name, f, ctx);
&#x9;case AUDIT_COMPARE_SGID_TO_OBJ_GID:
&#x9;&#x9;return audit_compare_gid(cred-&#x3E;sgid, name, f, ctx);
&#x9;case AUDIT_COMPARE_FSUID_TO_OBJ_UID:
&#x9;&#x9;return audit_compare_uid(cred-&#x3E;fsuid, name, f, ctx);
&#x9;case AUDIT_COMPARE_FSGID_TO_OBJ_GID:
&#x9;&#x9;return audit_compare_gid(cred-&#x3E;fsgid, name, f, ctx);
&#x9;*/ uid comparisons /*
&#x9;case AUDIT_COMPARE_UID_TO_AUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, tsk-&#x3E;loginuid);
&#x9;case AUDIT_COMPARE_UID_TO_EUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, cred-&#x3E;euid);
&#x9;case AUDIT_COMPARE_UID_TO_SUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, cred-&#x3E;suid);
&#x9;case AUDIT_COMPARE_UID_TO_FSUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, cred-&#x3E;fsuid);
&#x9;*/ auid comparisons /*
&#x9;case AUDIT_COMPARE_AUID_TO_EUID:
&#x9;&#x9;return audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, cred-&#x3E;euid);
&#x9;case AUDIT_COMPARE_AUID_TO_SUID:
&#x9;&#x9;return audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, cred-&#x3E;suid);
&#x9;case AUDIT_COMPARE_AUID_TO_FSUID:
&#x9;&#x9;return audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, cred-&#x3E;fsuid);
&#x9;*/ euid comparisons /*
&#x9;case AUDIT_COMPARE_EUID_TO_SUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;euid, f-&#x3E;op, cred-&#x3E;suid);
&#x9;case AUDIT_COMPARE_EUID_TO_FSUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;euid, f-&#x3E;op, cred-&#x3E;fsuid);
&#x9;*/ suid comparisons /*
&#x9;case AUDIT_COMPARE_SUID_TO_FSUID:
&#x9;&#x9;return audit_uid_comparator(cred-&#x3E;suid, f-&#x3E;op, cred-&#x3E;fsuid);
&#x9;*/ gid comparisons /*
&#x9;case AUDIT_COMPARE_GID_TO_EGID:
&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, cred-&#x3E;egid);
&#x9;case AUDIT_COMPARE_GID_TO_SGID:
&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, cred-&#x3E;sgid);
&#x9;case AUDIT_COMPARE_GID_TO_FSGID:
&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, cred-&#x3E;fsgid);
&#x9;*/ egid comparisons /*
&#x9;case AUDIT_COMPARE_EGID_TO_SGID:
&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;egid, f-&#x3E;op, cred-&#x3E;sgid);
&#x9;case AUDIT_COMPARE_EGID_TO_FSGID:
&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;egid, f-&#x3E;op, cred-&#x3E;fsgid);
&#x9;*/ sgid comparison /*
&#x9;case AUDIT_COMPARE_SGID_TO_FSGID:
&#x9;&#x9;return audit_gid_comparator(cred-&#x3E;sgid, f-&#x3E;op, cred-&#x3E;fsgid);
&#x9;default:
&#x9;&#x9;WARN(1, &#x22;Missing AUDIT_COMPARE define.  Report as a bug\n&#x22;);
&#x9;&#x9;return 0;
&#x9;}
&#x9;return 0;
}

*/ Determine if any context name data matches a rule&#x27;s watch data /*
*/ Compare a task_struct with an audit_rule.  Return 1 on match, 0
 otherwise.

 If task_creation is true, this is an explicit indication that we are
 filtering a task rule at task creation time.  This and tsk == current are
 the only situations where tsk-&#x3E;cred may be accessed without an rcu read lock.
 /*
static int audit_filter_rules(struct task_structtsk,
&#x9;&#x9;&#x9;      struct audit_krulerule,
&#x9;&#x9;&#x9;      struct audit_contextctx,
&#x9;&#x9;&#x9;      struct audit_namesname,
&#x9;&#x9;&#x9;      enum audit_statestate,
&#x9;&#x9;&#x9;      bool task_creation)
{
&#x9;const struct credcred;
&#x9;int i, need_sid = 1;
&#x9;u32 sid;

&#x9;cred = rcu_dereference_check(tsk-&#x3E;cred, tsk == current || task_creation);

&#x9;for (i = 0; i &#x3C; rule-&#x3E;field_count; i++) {
&#x9;&#x9;struct audit_fieldf = &#x26;rule-&#x3E;fields[i];
&#x9;&#x9;struct audit_namesn;
&#x9;&#x9;int result = 0;
&#x9;&#x9;pid_t pid;

&#x9;&#x9;switch (f-&#x3E;type) {
&#x9;&#x9;case AUDIT_PID:
&#x9;&#x9;&#x9;pid = task_pid_nr(tsk);
&#x9;&#x9;&#x9;result = audit_comparator(pid, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_PPID:
&#x9;&#x9;&#x9;if (ctx) {
&#x9;&#x9;&#x9;&#x9;if (!ctx-&#x3E;ppid)
&#x9;&#x9;&#x9;&#x9;&#x9;ctx-&#x3E;ppid = task_ppid_nr(tsk);
&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;ppid, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EXE:
&#x9;&#x9;&#x9;result = audit_exe_compare(tsk, rule-&#x3E;exe);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_UID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;uid, f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EUID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;euid, f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_SUID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;suid, f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FSUID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(cred-&#x3E;fsuid, f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_GID:
&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;gid, f-&#x3E;op, f-&#x3E;gid);
&#x9;&#x9;&#x9;if (f-&#x3E;op == Audit_equal) {
&#x9;&#x9;&#x9;&#x9;if (!result)
&#x9;&#x9;&#x9;&#x9;&#x9;result = in_group_p(f-&#x3E;gid);
&#x9;&#x9;&#x9;} else if (f-&#x3E;op == Audit_not_equal) {
&#x9;&#x9;&#x9;&#x9;if (result)
&#x9;&#x9;&#x9;&#x9;&#x9;result = !in_group_p(f-&#x3E;gid);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_EGID:
&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;egid, f-&#x3E;op, f-&#x3E;gid);
&#x9;&#x9;&#x9;if (f-&#x3E;op == Audit_equal) {
&#x9;&#x9;&#x9;&#x9;if (!result)
&#x9;&#x9;&#x9;&#x9;&#x9;result = in_egroup_p(f-&#x3E;gid);
&#x9;&#x9;&#x9;} else if (f-&#x3E;op == Audit_not_equal) {
&#x9;&#x9;&#x9;&#x9;if (result)
&#x9;&#x9;&#x9;&#x9;&#x9;result = !in_egroup_p(f-&#x3E;gid);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_SGID:
&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;sgid, f-&#x3E;op, f-&#x3E;gid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FSGID:
&#x9;&#x9;&#x9;result = audit_gid_comparator(cred-&#x3E;fsgid, f-&#x3E;op, f-&#x3E;gid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_PERS:
&#x9;&#x9;&#x9;result = audit_comparator(tsk-&#x3E;personality, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_ARCH:
&#x9;&#x9;&#x9;if (ctx)
&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;arch, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;

&#x9;&#x9;case AUDIT_EXIT:
&#x9;&#x9;&#x9;if (ctx &#x26;&#x26; ctx-&#x3E;return_valid)
&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;return_code, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_SUCCESS:
&#x9;&#x9;&#x9;if (ctx &#x26;&#x26; ctx-&#x3E;return_valid) {
&#x9;&#x9;&#x9;&#x9;if (f-&#x3E;val)
&#x9;&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;return_valid, f-&#x3E;op, AUDITSC_SUCCESS);
&#x9;&#x9;&#x9;&#x9;else
&#x9;&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;return_valid, f-&#x3E;op, AUDITSC_FAILURE);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_DEVMAJOR:
&#x9;&#x9;&#x9;if (name) {
&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MAJOR(name-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
&#x9;&#x9;&#x9;&#x9;    audit_comparator(MAJOR(name-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val))
&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;} else if (ctx) {
&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MAJOR(n-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
&#x9;&#x9;&#x9;&#x9;&#x9;    audit_comparator(MAJOR(n-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val)) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_DEVMINOR:
&#x9;&#x9;&#x9;if (name) {
&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MINOR(name-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
&#x9;&#x9;&#x9;&#x9;    audit_comparator(MINOR(name-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val))
&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;} else if (ctx) {
&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_comparator(MINOR(n-&#x3E;dev), f-&#x3E;op, f-&#x3E;val) ||
&#x9;&#x9;&#x9;&#x9;&#x9;    audit_comparator(MINOR(n-&#x3E;rdev), f-&#x3E;op, f-&#x3E;val)) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_INODE:
&#x9;&#x9;&#x9;if (name)
&#x9;&#x9;&#x9;&#x9;result = audit_comparator(name-&#x3E;ino, f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;else if (ctx) {
&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_comparator(n-&#x3E;ino, f-&#x3E;op, f-&#x3E;val)) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_OBJ_UID:
&#x9;&#x9;&#x9;if (name) {
&#x9;&#x9;&#x9;&#x9;result = audit_uid_comparator(name-&#x3E;uid, f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;} else if (ctx) {
&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_uid_comparator(n-&#x3E;uid, f-&#x3E;op, f-&#x3E;uid)) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_OBJ_GID:
&#x9;&#x9;&#x9;if (name) {
&#x9;&#x9;&#x9;&#x9;result = audit_gid_comparator(name-&#x3E;gid, f-&#x3E;op, f-&#x3E;gid);
&#x9;&#x9;&#x9;} else if (ctx) {
&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;&#x9;&#x9;if (audit_gid_comparator(n-&#x3E;gid, f-&#x3E;op, f-&#x3E;gid)) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_WATCH:
&#x9;&#x9;&#x9;if (name)
&#x9;&#x9;&#x9;&#x9;result = audit_watch_compare(rule-&#x3E;watch, name-&#x3E;ino, name-&#x3E;dev);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_DIR:
&#x9;&#x9;&#x9;if (ctx)
&#x9;&#x9;&#x9;&#x9;result = match_tree_refs(ctx, rule-&#x3E;tree);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_LOGINUID:
&#x9;&#x9;&#x9;result = audit_uid_comparator(tsk-&#x3E;loginuid, f-&#x3E;op, f-&#x3E;uid);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_LOGINUID_SET:
&#x9;&#x9;&#x9;result = audit_comparator(audit_loginuid_set(tsk), f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_SUBJ_USER:
&#x9;&#x9;case AUDIT_SUBJ_ROLE:
&#x9;&#x9;case AUDIT_SUBJ_TYPE:
&#x9;&#x9;case AUDIT_SUBJ_SEN:
&#x9;&#x9;case AUDIT_SUBJ_CLR:
&#x9;&#x9;&#x9;*/ NOTE: this may return negative values indicating
&#x9;&#x9;&#x9;   a temporary error.  We simply treat this as a
&#x9;&#x9;&#x9;   match for now to avoid losing information that
&#x9;&#x9;&#x9;   may be wanted.   An error message will also be
&#x9;&#x9;&#x9;   logged upon error /*
&#x9;&#x9;&#x9;if (f-&#x3E;lsm_rule) {
&#x9;&#x9;&#x9;&#x9;if (need_sid) {
&#x9;&#x9;&#x9;&#x9;&#x9;security_task_getsecid(tsk, &#x26;sid);
&#x9;&#x9;&#x9;&#x9;&#x9;need_sid = 0;
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;result = security_audit_rule_match(sid, f-&#x3E;type,
&#x9;&#x9;&#x9;&#x9;                                  f-&#x3E;op,
&#x9;&#x9;&#x9;&#x9;                                  f-&#x3E;lsm_rule,
&#x9;&#x9;&#x9;&#x9;                                  ctx);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_OBJ_USER:
&#x9;&#x9;case AUDIT_OBJ_ROLE:
&#x9;&#x9;case AUDIT_OBJ_TYPE:
&#x9;&#x9;case AUDIT_OBJ_LEV_LOW:
&#x9;&#x9;case AUDIT_OBJ_LEV_HIGH:
&#x9;&#x9;&#x9;*/ The above note for AUDIT_SUBJ_USER...AUDIT_SUBJ_CLR
&#x9;&#x9;&#x9;   also applies here /*
&#x9;&#x9;&#x9;if (f-&#x3E;lsm_rule) {
&#x9;&#x9;&#x9;&#x9;*/ Find files that match /*
&#x9;&#x9;&#x9;&#x9;if (name) {
&#x9;&#x9;&#x9;&#x9;&#x9;result = security_audit_rule_match(
&#x9;&#x9;&#x9;&#x9;&#x9;           name-&#x3E;osid, f-&#x3E;type, f-&#x3E;op,
&#x9;&#x9;&#x9;&#x9;&#x9;           f-&#x3E;lsm_rule, ctx);
&#x9;&#x9;&#x9;&#x9;} else if (ctx) {
&#x9;&#x9;&#x9;&#x9;&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;if (security_audit_rule_match(n-&#x3E;osid, f-&#x3E;type,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      f-&#x3E;op, f-&#x3E;lsm_rule,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      ctx)) {
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;&#x9;*/ Find ipc objects that match /*
&#x9;&#x9;&#x9;&#x9;if (!ctx || ctx-&#x3E;type != AUDIT_IPC)
&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;if (security_audit_rule_match(ctx-&#x3E;ipc.osid,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      f-&#x3E;type, f-&#x3E;op,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      f-&#x3E;lsm_rule, ctx))
&#x9;&#x9;&#x9;&#x9;&#x9;++result;
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_ARG0:
&#x9;&#x9;case AUDIT_ARG1:
&#x9;&#x9;case AUDIT_ARG2:
&#x9;&#x9;case AUDIT_ARG3:
&#x9;&#x9;&#x9;if (ctx)
&#x9;&#x9;&#x9;&#x9;result = audit_comparator(ctx-&#x3E;argv[f-&#x3E;type-AUDIT_ARG0], f-&#x3E;op, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FILTERKEY:
&#x9;&#x9;&#x9;*/ ignore this field for filtering /*
&#x9;&#x9;&#x9;result = 1;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_PERM:
&#x9;&#x9;&#x9;result = audit_match_perm(ctx, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FILETYPE:
&#x9;&#x9;&#x9;result = audit_match_filetype(ctx, f-&#x3E;val);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;case AUDIT_FIELD_COMPARE:
&#x9;&#x9;&#x9;result = audit_field_compare(tsk, cred, f, ctx, name);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;&#x9;if (!result)
&#x9;&#x9;&#x9;return 0;
&#x9;}

&#x9;if (ctx) {
&#x9;&#x9;if (rule-&#x3E;prio &#x3C;= ctx-&#x3E;prio)
&#x9;&#x9;&#x9;return 0;
&#x9;&#x9;if (rule-&#x3E;filterkey) {
&#x9;&#x9;&#x9;kfree(ctx-&#x3E;filterkey);
&#x9;&#x9;&#x9;ctx-&#x3E;filterkey = kstrdup(rule-&#x3E;filterkey, GFP_ATOMIC);
&#x9;&#x9;}
&#x9;&#x9;ctx-&#x3E;prio = rule-&#x3E;prio;
&#x9;}
&#x9;switch (rule-&#x3E;action) {
&#x9;case AUDIT_NEVER:   state = AUDIT_DISABLED;&#x9;    break;
&#x9;case AUDIT_ALWAYS:  state = AUDIT_RECORD_CONTEXT; break;
&#x9;}
&#x9;return 1;
}

*/ At process creation time, we can determine if system-call auditing is
 completely disabled for this task.  Since we only have the task
 structure at this point, we can only check uid and gid.
 /*
static enum audit_state audit_filter_task(struct task_structtsk, char*key)
{
&#x9;struct audit_entrye;
&#x9;enum audit_state   state;

&#x9;rcu_read_lock();
&#x9;list_for_each_entry_rcu(e, &#x26;audit_filter_list[AUDIT_FILTER_TASK], list) {
&#x9;&#x9;if (audit_filter_rules(tsk, &#x26;e-&#x3E;rule, NULL, NULL,
&#x9;&#x9;&#x9;&#x9;       &#x26;state, true)) {
&#x9;&#x9;&#x9;if (state == AUDIT_RECORD_CONTEXT)
&#x9;&#x9;&#x9;&#x9;*key = kstrdup(e-&#x3E;rule.filterkey, GFP_ATOMIC);
&#x9;&#x9;&#x9;rcu_read_unlock();
&#x9;&#x9;&#x9;return state;
&#x9;&#x9;}
&#x9;}
&#x9;rcu_read_unlock();
&#x9;return AUDIT_BUILD_CONTEXT;
}

static int audit_in_mask(const struct audit_krulerule, unsigned long val)
{
&#x9;int word, bit;

&#x9;if (val &#x3E; 0xffffffff)
&#x9;&#x9;return false;

&#x9;word = AUDIT_WORD(val);
&#x9;if (word &#x3E;= AUDIT_BITMASK_SIZE)
&#x9;&#x9;return false;

&#x9;bit = AUDIT_BIT(val);

&#x9;return rule-&#x3E;mask[word] &#x26; bit;
}

*/ At syscall entry and exit time, this filter is called if the
 audit_state is not low enough that auditing cannot take place, but is
 also not high enough that we already know we have to write an audit
 record (i.e., the state is AUDIT_SETUP_CONTEXT or AUDIT_BUILD_CONTEXT).
 /*
static enum audit_state audit_filter_syscall(struct task_structtsk,
&#x9;&#x9;&#x9;&#x9;&#x9;     struct audit_contextctx,
&#x9;&#x9;&#x9;&#x9;&#x9;     struct list_headlist)
{
&#x9;struct audit_entrye;
&#x9;enum audit_state state;

&#x9;if (audit_pid &#x26;&#x26; tsk-&#x3E;tgid == audit_pid)
&#x9;&#x9;return AUDIT_DISABLED;

&#x9;rcu_read_lock();
&#x9;if (!list_empty(list)) {
&#x9;&#x9;list_for_each_entry_rcu(e, list, list) {
&#x9;&#x9;&#x9;if (audit_in_mask(&#x26;e-&#x3E;rule, ctx-&#x3E;major) &#x26;&#x26;
&#x9;&#x9;&#x9;    audit_filter_rules(tsk, &#x26;e-&#x3E;rule, ctx, NULL,
&#x9;&#x9;&#x9;&#x9;&#x9;       &#x26;state, false)) {
&#x9;&#x9;&#x9;&#x9;rcu_read_unlock();
&#x9;&#x9;&#x9;&#x9;ctx-&#x3E;current_state = state;
&#x9;&#x9;&#x9;&#x9;return state;
&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;}
&#x9;rcu_read_unlock();
&#x9;return AUDIT_BUILD_CONTEXT;
}

*/
 Given an audit_name check the inode hash table to see if they match.
 Called holding the rcu read lock to protect the use of audit_inode_hash
 /*
static int audit_filter_inode_name(struct task_structtsk,
&#x9;&#x9;&#x9;&#x9;   struct audit_namesn,
&#x9;&#x9;&#x9;&#x9;   struct audit_contextctx) {
&#x9;int h = audit_hash_ino((u32)n-&#x3E;ino);
&#x9;struct list_headlist = &#x26;audit_inode_hash[h];
&#x9;struct audit_entrye;
&#x9;enum audit_state state;

&#x9;if (list_empty(list))
&#x9;&#x9;return 0;

&#x9;list_for_each_entry_rcu(e, list, list) {
&#x9;&#x9;if (audit_in_mask(&#x26;e-&#x3E;rule, ctx-&#x3E;major) &#x26;&#x26;
&#x9;&#x9;    audit_filter_rules(tsk, &#x26;e-&#x3E;rule, ctx, n, &#x26;state, false)) {
&#x9;&#x9;&#x9;ctx-&#x3E;current_state = state;
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;}
&#x9;}

&#x9;return 0;
}

*/ At syscall exit time, this filter is called if any audit_names have been
 collected during syscall processing.  We only check rules in sublists at hash
 buckets applicable to the inode numbers in audit_names.
 Regarding audit_state, same rules apply as for audit_filter_syscall().
 /*
void audit_filter_inodes(struct task_structtsk, struct audit_contextctx)
{
&#x9;struct audit_namesn;

&#x9;if (audit_pid &#x26;&#x26; tsk-&#x3E;tgid == audit_pid)
&#x9;&#x9;return;

&#x9;rcu_read_lock();

&#x9;list_for_each_entry(n, &#x26;ctx-&#x3E;names_list, list) {
&#x9;&#x9;if (audit_filter_inode_name(tsk, n, ctx))
&#x9;&#x9;&#x9;break;
&#x9;}
&#x9;rcu_read_unlock();
}

*/ Transfer the audit context pointer to the caller, clearing it in the tsk&#x27;s struct /*
static inline struct audit_contextaudit_take_context(struct task_structtsk,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      int return_valid,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;      long return_code)
{
&#x9;struct audit_contextcontext = tsk-&#x3E;audit_context;

&#x9;if (!context)
&#x9;&#x9;return NULL;
&#x9;context-&#x3E;return_valid = return_valid;

&#x9;*/
&#x9; we need to fix up the return code in the audit logs if the actual
&#x9; return codes are later going to be fixed up by the arch specific
&#x9; signal handlers
&#x9;
&#x9; This is actually a test for:
&#x9; (rc == ERESTARTSYS ) || (rc == ERESTARTNOINTR) ||
&#x9; (rc == ERESTARTNOHAND) || (rc == ERESTART_RESTARTBLOCK)
&#x9;
&#x9; but is faster than a bunch of ||
&#x9; /*
&#x9;if (unlikely(return_code &#x3C;= -ERESTARTSYS) &#x26;&#x26;
&#x9;    (return_code &#x3E;= -ERESTART_RESTARTBLOCK) &#x26;&#x26;
&#x9;    (return_code != -ENOIOCTLCMD))
&#x9;&#x9;context-&#x3E;return_code = -EINTR;
&#x9;else
&#x9;&#x9;context-&#x3E;return_code  = return_code;

&#x9;if (context-&#x3E;in_syscall &#x26;&#x26; !context-&#x3E;dummy) {
&#x9;&#x9;audit_filter_syscall(tsk, context, &#x26;audit_filter_list[AUDIT_FILTER_EXIT]);
&#x9;&#x9;audit_filter_inodes(tsk, context);
&#x9;}

&#x9;tsk-&#x3E;audit_context = NULL;
&#x9;return context;
}

static inline void audit_proctitle_free(struct audit_contextcontext)
{
&#x9;kfree(context-&#x3E;proctitle.value);
&#x9;context-&#x3E;proctitle.value = NULL;
&#x9;context-&#x3E;proctitle.len = 0;
}

static inline void audit_free_names(struct audit_contextcontext)
{
&#x9;struct audit_namesn,next;

&#x9;list_for_each_entry_safe(n, next, &#x26;context-&#x3E;names_list, list) {
&#x9;&#x9;list_del(&#x26;n-&#x3E;list);
&#x9;&#x9;if (n-&#x3E;name)
&#x9;&#x9;&#x9;putname(n-&#x3E;name);
&#x9;&#x9;if (n-&#x3E;should_free)
&#x9;&#x9;&#x9;kfree(n);
&#x9;}
&#x9;context-&#x3E;name_count = 0;
&#x9;path_put(&#x26;context-&#x3E;pwd);
&#x9;context-&#x3E;pwd.dentry = NULL;
&#x9;context-&#x3E;pwd.mnt = NULL;
}

static inline void audit_free_aux(struct audit_contextcontext)
{
&#x9;struct audit_aux_dataaux;

&#x9;while ((aux = context-&#x3E;aux)) {
&#x9;&#x9;context-&#x3E;aux = aux-&#x3E;next;
&#x9;&#x9;kfree(aux);
&#x9;}
&#x9;while ((aux = context-&#x3E;aux_pids)) {
&#x9;&#x9;context-&#x3E;aux_pids = aux-&#x3E;next;
&#x9;&#x9;kfree(aux);
&#x9;}
}

static inline struct audit_contextaudit_alloc_context(enum audit_state state)
{
&#x9;struct audit_contextcontext;

&#x9;context = kzalloc(sizeof(*context), GFP_KERNEL);
&#x9;if (!context)
&#x9;&#x9;return NULL;
&#x9;context-&#x3E;state = state;
&#x9;context-&#x3E;prio = state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;
&#x9;INIT_LIST_HEAD(&#x26;context-&#x3E;killed_trees);
&#x9;INIT_LIST_HEAD(&#x26;context-&#x3E;names_list);
&#x9;return context;
}

*/
 audit_alloc - allocate an audit context block for a task
 @tsk: task

 Filter on the task information and allocate a per-task audit context
 if necessary.  Doing so turns on system call auditing for the
 specified task.  This is called from copy_process, so no lock is
 needed.
 /*
int audit_alloc(struct task_structtsk)
{
&#x9;struct audit_contextcontext;
&#x9;enum audit_state     state;
&#x9;charkey = NULL;

&#x9;if (likely(!audit_ever_enabled))
&#x9;&#x9;return 0;/ Return if not auditing. /*

&#x9;state = audit_filter_task(tsk, &#x26;key);
&#x9;if (state == AUDIT_DISABLED) {
&#x9;&#x9;clear_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
&#x9;&#x9;return 0;
&#x9;}

&#x9;if (!(context = audit_alloc_context(state))) {
&#x9;&#x9;kfree(key);
&#x9;&#x9;audit_log_lost(&#x22;out of memory in audit_alloc&#x22;);
&#x9;&#x9;return -ENOMEM;
&#x9;}
&#x9;context-&#x3E;filterkey = key;

&#x9;tsk-&#x3E;audit_context  = context;
&#x9;set_tsk_thread_flag(tsk, TIF_SYSCALL_AUDIT);
&#x9;return 0;
}

static inline void audit_free_context(struct audit_contextcontext)
{
&#x9;audit_free_names(context);
&#x9;unroll_tree_refs(context, NULL, 0);
&#x9;free_tree_refs(context);
&#x9;audit_free_aux(context);
&#x9;kfree(context-&#x3E;filterkey);
&#x9;kfree(context-&#x3E;sockaddr);
&#x9;audit_proctitle_free(context);
&#x9;kfree(context);
}

static int audit_log_pid_context(struct audit_contextcontext, pid_t pid,
&#x9;&#x9;&#x9;&#x9; kuid_t auid, kuid_t uid, unsigned int sessionid,
&#x9;&#x9;&#x9;&#x9; u32 sid, charcomm)
{
&#x9;struct audit_bufferab;
&#x9;charctx = NULL;
&#x9;u32 len;
&#x9;int rc = 0;

&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_OBJ_PID);
&#x9;if (!ab)
&#x9;&#x9;return rc;

&#x9;audit_log_format(ab, &#x22;opid=%d oauid=%d ouid=%d oses=%d&#x22;, pid,
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, auid),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, uid), sessionid);
&#x9;if (sid) {
&#x9;&#x9;if (security_secid_to_secctx(sid, &#x26;ctx, &#x26;len)) {
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=(none)&#x22;);
&#x9;&#x9;&#x9;rc = 1;
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, ctx);
&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
&#x9;&#x9;}
&#x9;}
&#x9;audit_log_format(ab, &#x22; ocomm=&#x22;);
&#x9;audit_log_untrustedstring(ab, comm);
&#x9;audit_log_end(ab);

&#x9;return rc;
}

*/
 to_send and len_sent accounting are very loose estimates.  We aren&#x27;t
 really worried about a hard cap to MAX_EXECVE_AUDIT_LEN so much as being
 within about 500 bytes (next page boundary)

 why snprintf?  an int is up to 12 digits long.  if we just assumed when
 logging that a[%d]= was going to be 16 characters long we would be wasting
 space in every audit message.  In one 7500 byte message we can log up to
 about 1000 min size arguments.  That comes down to about 50% waste of space
 if we didn&#x27;t do the snprintf to find out how long arg_num_len was.
 /*
static int audit_log_single_execve_arg(struct audit_contextcontext,
&#x9;&#x9;&#x9;&#x9;&#x9;struct audit_buffer*ab,
&#x9;&#x9;&#x9;&#x9;&#x9;int arg_num,
&#x9;&#x9;&#x9;&#x9;&#x9;size_tlen_sent,
&#x9;&#x9;&#x9;&#x9;&#x9;const char __userp,
&#x9;&#x9;&#x9;&#x9;&#x9;charbuf)
{
&#x9;char arg_num_len_buf[12];
&#x9;const char __usertmp_p = p;
&#x9;*/ how many digits are in arg_num? 5 is the length of &#x27; a=&#x22;&#x22;&#x27; /*
&#x9;size_t arg_num_len = snprintf(arg_num_len_buf, 12, &#x22;%d&#x22;, arg_num) + 5;
&#x9;size_t len, len_left, to_send;
&#x9;size_t max_execve_audit_len = MAX_EXECVE_AUDIT_LEN;
&#x9;unsigned int i, has_cntl = 0, too_long = 0;
&#x9;int ret;

&#x9;*/ strnlen_user includes the null we don&#x27;t want to send /*
&#x9;len_left = len = strnlen_user(p, MAX_ARG_STRLEN) - 1;

&#x9;*/
&#x9; We just created this mm, if we can&#x27;t find the strings
&#x9; we just copied into it something is _very_ wrong. Similar
&#x9; for strings that are too long, we should not have created
&#x9; any.
&#x9; /*
&#x9;if (WARN_ON_ONCE(len &#x3C; 0 || len &#x3E; MAX_ARG_STRLEN - 1)) {
&#x9;&#x9;send_sig(SIGKILL, current, 0);
&#x9;&#x9;return -1;
&#x9;}

&#x9;*/ walk the whole argument looking for non-ascii chars /*
&#x9;do {
&#x9;&#x9;if (len_left &#x3E; MAX_EXECVE_AUDIT_LEN)
&#x9;&#x9;&#x9;to_send = MAX_EXECVE_AUDIT_LEN;
&#x9;&#x9;else
&#x9;&#x9;&#x9;to_send = len_left;
&#x9;&#x9;ret = copy_from_user(buf, tmp_p, to_send);
&#x9;&#x9;*/
&#x9;&#x9; There is no reason for this copy to be short. We just
&#x9;&#x9; copied them here, and the mm hasn&#x27;t been exposed to user-
&#x9;&#x9; space yet.
&#x9;&#x9; /*
&#x9;&#x9;if (ret) {
&#x9;&#x9;&#x9;WARN_ON(1);
&#x9;&#x9;&#x9;send_sig(SIGKILL, current, 0);
&#x9;&#x9;&#x9;return -1;
&#x9;&#x9;}
&#x9;&#x9;buf[to_send] = &#x27;\0&#x27;;
&#x9;&#x9;has_cntl = audit_string_contains_control(buf, to_send);
&#x9;&#x9;if (has_cntl) {
&#x9;&#x9;&#x9;*/
&#x9;&#x9;&#x9; hex messages get logged as 2 bytes, so we can only
&#x9;&#x9;&#x9; send half as much in each message
&#x9;&#x9;&#x9; /*
&#x9;&#x9;&#x9;max_execve_audit_len = MAX_EXECVE_AUDIT_LEN / 2;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;&#x9;len_left -= to_send;
&#x9;&#x9;tmp_p += to_send;
&#x9;} while (len_left &#x3E; 0);

&#x9;len_left = len;

&#x9;if (len &#x3E; max_execve_audit_len)
&#x9;&#x9;too_long = 1;

&#x9;*/ rewalk the argument actually logging the message /*
&#x9;for (i = 0; len_left &#x3E; 0; i++) {
&#x9;&#x9;int room_left;

&#x9;&#x9;if (len_left &#x3E; max_execve_audit_len)
&#x9;&#x9;&#x9;to_send = max_execve_audit_len;
&#x9;&#x9;else
&#x9;&#x9;&#x9;to_send = len_left;

&#x9;&#x9;*/ do we have space left to send this argument in this ab? /*
&#x9;&#x9;room_left = MAX_EXECVE_AUDIT_LEN - arg_num_len -len_sent;
&#x9;&#x9;if (has_cntl)
&#x9;&#x9;&#x9;room_left -= (to_send 2);
&#x9;&#x9;else
&#x9;&#x9;&#x9;room_left -= to_send;
&#x9;&#x9;if (room_left &#x3C; 0) {
&#x9;&#x9;&#x9;*len_sent = 0;
&#x9;&#x9;&#x9;audit_log_end(*ab);
&#x9;&#x9;&#x9;*ab = audit_log_start(context, GFP_KERNEL, AUDIT_EXECVE);
&#x9;&#x9;&#x9;if (!*ab)
&#x9;&#x9;&#x9;&#x9;return 0;
&#x9;&#x9;}

&#x9;&#x9;*/
&#x9;&#x9; first record needs to say how long the original string was
&#x9;&#x9; so we can be sure nothing was lost.
&#x9;&#x9; /*
&#x9;&#x9;if ((i == 0) &#x26;&#x26; (too_long))
&#x9;&#x9;&#x9;audit_log_format(*ab, &#x22; a%d_len=%zu&#x22;, arg_num,
&#x9;&#x9;&#x9;&#x9;&#x9; has_cntl ? 2*len : len);

&#x9;&#x9;*/
&#x9;&#x9; normally arguments are small enough to fit and we already
&#x9;&#x9; filled buf above when we checked for control characters
&#x9;&#x9; so don&#x27;t bother with another copy_from_user
&#x9;&#x9; /*
&#x9;&#x9;if (len &#x3E;= max_execve_audit_len)
&#x9;&#x9;&#x9;ret = copy_from_user(buf, p, to_send);
&#x9;&#x9;else
&#x9;&#x9;&#x9;ret = 0;
&#x9;&#x9;if (ret) {
&#x9;&#x9;&#x9;WARN_ON(1);
&#x9;&#x9;&#x9;send_sig(SIGKILL, current, 0);
&#x9;&#x9;&#x9;return -1;
&#x9;&#x9;}
&#x9;&#x9;buf[to_send] = &#x27;\0&#x27;;

&#x9;&#x9;*/ actually log it /*
&#x9;&#x9;audit_log_format(*ab, &#x22; a%d&#x22;, arg_num);
&#x9;&#x9;if (too_long)
&#x9;&#x9;&#x9;audit_log_format(*ab, &#x22;[%d]&#x22;, i);
&#x9;&#x9;audit_log_format(*ab, &#x22;=&#x22;);
&#x9;&#x9;if (has_cntl)
&#x9;&#x9;&#x9;audit_log_n_hex(*ab, buf, to_send);
&#x9;&#x9;else
&#x9;&#x9;&#x9;audit_log_string(*ab, buf);

&#x9;&#x9;p += to_send;
&#x9;&#x9;len_left -= to_send;
&#x9;&#x9;*len_sent += arg_num_len;
&#x9;&#x9;if (has_cntl)
&#x9;&#x9;&#x9;*len_sent += to_send 2;
&#x9;&#x9;else
&#x9;&#x9;&#x9;*len_sent += to_send;
&#x9;}
&#x9;*/ include the null we didn&#x27;t log /*
&#x9;return len + 1;
}

static void audit_log_execve_info(struct audit_contextcontext,
&#x9;&#x9;&#x9;&#x9;  struct audit_buffer*ab)
{
&#x9;int i, len;
&#x9;size_t len_sent = 0;
&#x9;const char __userp;
&#x9;charbuf;

&#x9;p = (const char __user)current-&#x3E;mm-&#x3E;arg_start;

&#x9;audit_log_format(*ab, &#x22;argc=%d&#x22;, context-&#x3E;execve.argc);

&#x9;*/
&#x9; we need some kernel buffer to hold the userspace args.  Just
&#x9; allocate one big one rather than allocating one of the right size
&#x9; for every single argument inside audit_log_single_execve_arg()
&#x9; should be &#x3C;8k allocation so should be pretty safe.
&#x9; /*
&#x9;buf = kmalloc(MAX_EXECVE_AUDIT_LEN + 1, GFP_KERNEL);
&#x9;if (!buf) {
&#x9;&#x9;audit_panic(&#x22;out of memory for argv string&#x22;);
&#x9;&#x9;return;
&#x9;}

&#x9;for (i = 0; i &#x3C; context-&#x3E;execve.argc; i++) {
&#x9;&#x9;len = audit_log_single_execve_arg(context, ab, i,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  &#x26;len_sent, p, buf);
&#x9;&#x9;if (len &#x3C;= 0)
&#x9;&#x9;&#x9;break;
&#x9;&#x9;p += len;
&#x9;}
&#x9;kfree(buf);
}

static void show_special(struct audit_contextcontext, intcall_panic)
{
&#x9;struct audit_bufferab;
&#x9;int i;

&#x9;ab = audit_log_start(context, GFP_KERNEL, context-&#x3E;type);
&#x9;if (!ab)
&#x9;&#x9;return;

&#x9;switch (context-&#x3E;type) {
&#x9;case AUDIT_SOCKETCALL: {
&#x9;&#x9;int nargs = context-&#x3E;socketcall.nargs;
&#x9;&#x9;audit_log_format(ab, &#x22;nargs=%d&#x22;, nargs);
&#x9;&#x9;for (i = 0; i &#x3C; nargs; i++)
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; a%d=%lx&#x22;, i,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;socketcall.args[i]);
&#x9;&#x9;break; }
&#x9;case AUDIT_IPC: {
&#x9;&#x9;u32 osid = context-&#x3E;ipc.osid;

&#x9;&#x9;audit_log_format(ab, &#x22;ouid=%u ogid=%u mode=%#ho&#x22;,
&#x9;&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, context-&#x3E;ipc.uid),
&#x9;&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, context-&#x3E;ipc.gid),
&#x9;&#x9;&#x9;&#x9; context-&#x3E;ipc.mode);
&#x9;&#x9;if (osid) {
&#x9;&#x9;&#x9;charctx = NULL;
&#x9;&#x9;&#x9;u32 len;
&#x9;&#x9;&#x9;if (security_secid_to_secctx(osid, &#x26;ctx, &#x26;len)) {
&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; osid=%u&#x22;, osid);
&#x9;&#x9;&#x9;&#x9;*call_panic = 1;
&#x9;&#x9;&#x9;} else {
&#x9;&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; obj=%s&#x22;, ctx);
&#x9;&#x9;&#x9;&#x9;security_release_secctx(ctx, len);
&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;&#x9;if (context-&#x3E;ipc.has_perm) {
&#x9;&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL,
&#x9;&#x9;&#x9;&#x9;&#x9;     AUDIT_IPC_SET_PERM);
&#x9;&#x9;&#x9;if (unlikely(!ab))
&#x9;&#x9;&#x9;&#x9;return;
&#x9;&#x9;&#x9;audit_log_format(ab,
&#x9;&#x9;&#x9;&#x9;&#x22;qbytes=%lx ouid=%u ogid=%u mode=%#ho&#x22;,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.qbytes,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.perm_uid,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.perm_gid,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;ipc.perm_mode);
&#x9;&#x9;}
&#x9;&#x9;break; }
&#x9;case AUDIT_MQ_OPEN: {
&#x9;&#x9;audit_log_format(ab,
&#x9;&#x9;&#x9;&#x22;oflag=0x%x mode=%#ho mq_flags=0x%lx mq_maxmsg=%ld &#x22;
&#x9;&#x9;&#x9;&#x22;mq_msgsize=%ld mq_curmsgs=%ld&#x22;,
&#x9;&#x9;&#x9;context-&#x3E;mq_open.oflag, context-&#x3E;mq_open.mode,
&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_flags,
&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_maxmsg,
&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_msgsize,
&#x9;&#x9;&#x9;context-&#x3E;mq_open.attr.mq_curmsgs);
&#x9;&#x9;break; }
&#x9;case AUDIT_MQ_SENDRECV: {
&#x9;&#x9;audit_log_format(ab,
&#x9;&#x9;&#x9;&#x22;mqdes=%d msg_len=%zd msg_prio=%u &#x22;
&#x9;&#x9;&#x9;&#x22;abs_timeout_sec=%ld abs_timeout_nsec=%ld&#x22;,
&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.mqdes,
&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.msg_len,
&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.msg_prio,
&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.abs_timeout.tv_sec,
&#x9;&#x9;&#x9;context-&#x3E;mq_sendrecv.abs_timeout.tv_nsec);
&#x9;&#x9;break; }
&#x9;case AUDIT_MQ_NOTIFY: {
&#x9;&#x9;audit_log_format(ab, &#x22;mqdes=%d sigev_signo=%d&#x22;,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;mq_notify.mqdes,
&#x9;&#x9;&#x9;&#x9;context-&#x3E;mq_notify.sigev_signo);
&#x9;&#x9;break; }
&#x9;case AUDIT_MQ_GETSETATTR: {
&#x9;&#x9;struct mq_attrattr = &#x26;context-&#x3E;mq_getsetattr.mqstat;
&#x9;&#x9;audit_log_format(ab,
&#x9;&#x9;&#x9;&#x22;mqdes=%d mq_flags=0x%lx mq_maxmsg=%ld mq_msgsize=%ld &#x22;
&#x9;&#x9;&#x9;&#x22;mq_curmsgs=%ld &#x22;,
&#x9;&#x9;&#x9;context-&#x3E;mq_getsetattr.mqdes,
&#x9;&#x9;&#x9;attr-&#x3E;mq_flags, attr-&#x3E;mq_maxmsg,
&#x9;&#x9;&#x9;attr-&#x3E;mq_msgsize, attr-&#x3E;mq_curmsgs);
&#x9;&#x9;break; }
&#x9;case AUDIT_CAPSET: {
&#x9;&#x9;audit_log_format(ab, &#x22;pid=%d&#x22;, context-&#x3E;capset.pid);
&#x9;&#x9;audit_log_cap(ab, &#x22;cap_pi&#x22;, &#x26;context-&#x3E;capset.cap.inheritable);
&#x9;&#x9;audit_log_cap(ab, &#x22;cap_pp&#x22;, &#x26;context-&#x3E;capset.cap.permitted);
&#x9;&#x9;audit_log_cap(ab, &#x22;cap_pe&#x22;, &#x26;context-&#x3E;capset.cap.effective);
&#x9;&#x9;break; }
&#x9;case AUDIT_MMAP: {
&#x9;&#x9;audit_log_format(ab, &#x22;fd=%d flags=0x%x&#x22;, context-&#x3E;mmap.fd,
&#x9;&#x9;&#x9;&#x9; context-&#x3E;mmap.flags);
&#x9;&#x9;break; }
&#x9;case AUDIT_EXECVE: {
&#x9;&#x9;audit_log_execve_info(context, &#x26;ab);
&#x9;&#x9;break; }
&#x9;}
&#x9;audit_log_end(ab);
}

static inline int audit_proctitle_rtrim(charproctitle, int len)
{
&#x9;charend = proctitle + len - 1;
&#x9;while (end &#x3E; proctitle &#x26;&#x26; !isprint(*end))
&#x9;&#x9;end--;

&#x9;*/ catch the case where proctitle is only 1 non-print character /*
&#x9;len = end - proctitle + 1;
&#x9;len -= isprint(proctitle[len-1]) == 0;
&#x9;return len;
}

static void audit_log_proctitle(struct task_structtsk,
&#x9;&#x9;&#x9; struct audit_contextcontext)
{
&#x9;int res;
&#x9;charbuf;
&#x9;charmsg = &#x22;(null)&#x22;;
&#x9;int len = strlen(msg);
&#x9;struct audit_bufferab;

&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_PROCTITLE);
&#x9;if (!ab)
&#x9;&#x9;return;&#x9;*/ audit_panic or being filtered /*

&#x9;audit_log_format(ab, &#x22;proctitle=&#x22;);

&#x9;*/ Not  cached /*
&#x9;if (!context-&#x3E;proctitle.value) {
&#x9;&#x9;buf = kmalloc(MAX_PROCTITLE_AUDIT_LEN, GFP_KERNEL);
&#x9;&#x9;if (!buf)
&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;*/ Historically called this from procfs naming /*
&#x9;&#x9;res = get_cmdline(tsk, buf, MAX_PROCTITLE_AUDIT_LEN);
&#x9;&#x9;if (res == 0) {
&#x9;&#x9;&#x9;kfree(buf);
&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;}
&#x9;&#x9;res = audit_proctitle_rtrim(buf, res);
&#x9;&#x9;if (res == 0) {
&#x9;&#x9;&#x9;kfree(buf);
&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;}
&#x9;&#x9;context-&#x3E;proctitle.value = buf;
&#x9;&#x9;context-&#x3E;proctitle.len = res;
&#x9;}
&#x9;msg = context-&#x3E;proctitle.value;
&#x9;len = context-&#x3E;proctitle.len;
out:
&#x9;audit_log_n_untrustedstring(ab, msg, len);
&#x9;audit_log_end(ab);
}

static void audit_log_exit(struct audit_contextcontext, struct task_structtsk)
{
&#x9;int i, call_panic = 0;
&#x9;struct audit_bufferab;
&#x9;struct audit_aux_dataaux;
&#x9;struct audit_namesn;

&#x9;*/ tsk == current /*
&#x9;context-&#x3E;personality = tsk-&#x3E;personality;

&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_SYSCALL);
&#x9;if (!ab)
&#x9;&#x9;return;&#x9;&#x9;*/ audit_panic has been called /*
&#x9;audit_log_format(ab, &#x22;arch=%x syscall=%d&#x22;,
&#x9;&#x9;&#x9; context-&#x3E;arch, context-&#x3E;major);
&#x9;if (context-&#x3E;personality != PER_LINUX)
&#x9;&#x9;audit_log_format(ab, &#x22; per=%lx&#x22;, context-&#x3E;personality);
&#x9;if (context-&#x3E;return_valid)
&#x9;&#x9;audit_log_format(ab, &#x22; success=%s exit=%ld&#x22;,
&#x9;&#x9;&#x9;&#x9; (context-&#x3E;return_valid==AUDITSC_SUCCESS)?&#x22;yes&#x22;:&#x22;no&#x22;,
&#x9;&#x9;&#x9;&#x9; context-&#x3E;return_code);

&#x9;audit_log_format(ab,
&#x9;&#x9;&#x9; &#x22; a0=%lx a1=%lx a2=%lx a3=%lx items=%d&#x22;,
&#x9;&#x9;&#x9; context-&#x3E;argv[0],
&#x9;&#x9;&#x9; context-&#x3E;argv[1],
&#x9;&#x9;&#x9; context-&#x3E;argv[2],
&#x9;&#x9;&#x9; context-&#x3E;argv[3],
&#x9;&#x9;&#x9; context-&#x3E;name_count);

&#x9;audit_log_task_info(ab, tsk);
&#x9;audit_log_key(ab, context-&#x3E;filterkey);
&#x9;audit_log_end(ab);

&#x9;for (aux = context-&#x3E;aux; aux; aux = aux-&#x3E;next) {

&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, aux-&#x3E;type);
&#x9;&#x9;if (!ab)
&#x9;&#x9;&#x9;continue;/ audit_panic has been called /*

&#x9;&#x9;switch (aux-&#x3E;type) {

&#x9;&#x9;case AUDIT_BPRM_FCAPS: {
&#x9;&#x9;&#x9;struct audit_aux_data_bprm_fcapsaxs = (void)aux;
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22;fver=%x&#x22;, axs-&#x3E;fcap_ver);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;fp&#x22;, &#x26;axs-&#x3E;fcap.permitted);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;fi&#x22;, &#x26;axs-&#x3E;fcap.inheritable);
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22; fe=%d&#x22;, axs-&#x3E;fcap.fE);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;old_pp&#x22;, &#x26;axs-&#x3E;old_pcap.permitted);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;old_pi&#x22;, &#x26;axs-&#x3E;old_pcap.inheritable);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;old_pe&#x22;, &#x26;axs-&#x3E;old_pcap.effective);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;new_pp&#x22;, &#x26;axs-&#x3E;new_pcap.permitted);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;new_pi&#x22;, &#x26;axs-&#x3E;new_pcap.inheritable);
&#x9;&#x9;&#x9;audit_log_cap(ab, &#x22;new_pe&#x22;, &#x26;axs-&#x3E;new_pcap.effective);
&#x9;&#x9;&#x9;break; }

&#x9;&#x9;}
&#x9;&#x9;audit_log_end(ab);
&#x9;}

&#x9;if (context-&#x3E;type)
&#x9;&#x9;show_special(context, &#x26;call_panic);

&#x9;if (context-&#x3E;fds[0] &#x3E;= 0) {
&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_FD_PAIR);
&#x9;&#x9;if (ab) {
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22;fd0=%d fd1=%d&#x22;,
&#x9;&#x9;&#x9;&#x9;&#x9;context-&#x3E;fds[0], context-&#x3E;fds[1]);
&#x9;&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;}
&#x9;}

&#x9;if (context-&#x3E;sockaddr_len) {
&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_SOCKADDR);
&#x9;&#x9;if (ab) {
&#x9;&#x9;&#x9;audit_log_format(ab, &#x22;saddr=&#x22;);
&#x9;&#x9;&#x9;audit_log_n_hex(ab, (void)context-&#x3E;sockaddr,
&#x9;&#x9;&#x9;&#x9;&#x9;context-&#x3E;sockaddr_len);
&#x9;&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;}
&#x9;}

&#x9;for (aux = context-&#x3E;aux_pids; aux; aux = aux-&#x3E;next) {
&#x9;&#x9;struct audit_aux_data_pidsaxs = (void)aux;

&#x9;&#x9;for (i = 0; i &#x3C; axs-&#x3E;pid_count; i++)
&#x9;&#x9;&#x9;if (audit_log_pid_context(context, axs-&#x3E;target_pid[i],
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_auid[i],
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_uid[i],
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_sessionid[i],
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_sid[i],
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;  axs-&#x3E;target_comm[i]))
&#x9;&#x9;&#x9;&#x9;call_panic = 1;
&#x9;}

&#x9;if (context-&#x3E;target_pid &#x26;&#x26;
&#x9;    audit_log_pid_context(context, context-&#x3E;target_pid,
&#x9;&#x9;&#x9;&#x9;  context-&#x3E;target_auid, context-&#x3E;target_uid,
&#x9;&#x9;&#x9;&#x9;  context-&#x3E;target_sessionid,
&#x9;&#x9;&#x9;&#x9;  context-&#x3E;target_sid, context-&#x3E;target_comm))
&#x9;&#x9;&#x9;call_panic = 1;

&#x9;if (context-&#x3E;pwd.dentry &#x26;&#x26; context-&#x3E;pwd.mnt) {
&#x9;&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_CWD);
&#x9;&#x9;if (ab) {
&#x9;&#x9;&#x9;audit_log_d_path(ab, &#x22; cwd=&#x22;, &#x26;context-&#x3E;pwd);
&#x9;&#x9;&#x9;audit_log_end(ab);
&#x9;&#x9;}
&#x9;}

&#x9;i = 0;
&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
&#x9;&#x9;if (n-&#x3E;hidden)
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;audit_log_name(context, n, NULL, i++, &#x26;call_panic);
&#x9;}

&#x9;audit_log_proctitle(tsk, context);

&#x9;*/ Send end of event record to help user space know we are finished /*
&#x9;ab = audit_log_start(context, GFP_KERNEL, AUDIT_EOE);
&#x9;if (ab)
&#x9;&#x9;audit_log_end(ab);
&#x9;if (call_panic)
&#x9;&#x9;audit_panic(&#x22;error converting sid to string&#x22;);
}

*/
 audit_free - free a per-task audit context
 @tsk: task whose audit context block to free

 Called from copy_process and do_exit
 /*
void __audit_free(struct task_structtsk)
{
&#x9;struct audit_contextcontext;

&#x9;context = audit_take_context(tsk, 0, 0);
&#x9;if (!context)
&#x9;&#x9;return;

&#x9;*/ Check for system calls that do not go through the exit
&#x9; function (e.g., exit_group), then free context block.
&#x9; We use GFP_ATOMIC here because we might be doing this
&#x9; in the context of the idle thread /*
&#x9;*/ that can happen only if we are called from do_exit() /*
&#x9;if (context-&#x3E;in_syscall &#x26;&#x26; context-&#x3E;current_state == AUDIT_RECORD_CONTEXT)
&#x9;&#x9;audit_log_exit(context, tsk);
&#x9;if (!list_empty(&#x26;context-&#x3E;killed_trees))
&#x9;&#x9;audit_kill_trees(&#x26;context-&#x3E;killed_trees);

&#x9;audit_free_context(context);
}

*/
 audit_syscall_entry - fill in an audit record at syscall entry
 @major: major syscall type (function)
 @a1: additional syscall register 1
 @a2: additional syscall register 2
 @a3: additional syscall register 3
 @a4: additional syscall register 4

 Fill in audit context at syscall entry.  This only happens if the
 audit context was created when the task was created and the state or
 filters demand the audit context be built.  If the state from the
 per-task filter or from the per-syscall filter is AUDIT_RECORD_CONTEXT,
 then the record will be written at syscall exit time (otherwise, it
 will only be written if another part of the kernel requests that it
 be written).
 /*
void __audit_syscall_entry(int major, unsigned long a1, unsigned long a2,
&#x9;&#x9;&#x9;   unsigned long a3, unsigned long a4)
{
&#x9;struct task_structtsk = current;
&#x9;struct audit_contextcontext = tsk-&#x3E;audit_context;
&#x9;enum audit_state     state;

&#x9;if (!context)
&#x9;&#x9;return;

&#x9;BUG_ON(context-&#x3E;in_syscall || context-&#x3E;name_count);

&#x9;if (!audit_enabled)
&#x9;&#x9;return;

&#x9;context-&#x3E;arch&#x9;    = syscall_get_arch();
&#x9;context-&#x3E;major      = major;
&#x9;context-&#x3E;argv[0]    = a1;
&#x9;context-&#x3E;argv[1]    = a2;
&#x9;context-&#x3E;argv[2]    = a3;
&#x9;context-&#x3E;argv[3]    = a4;

&#x9;state = context-&#x3E;state;
&#x9;context-&#x3E;dummy = !audit_n_rules;
&#x9;if (!context-&#x3E;dummy &#x26;&#x26; state == AUDIT_BUILD_CONTEXT) {
&#x9;&#x9;context-&#x3E;prio = 0;
&#x9;&#x9;state = audit_filter_syscall(tsk, context, &#x26;audit_filter_list[AUDIT_FILTER_ENTRY]);
&#x9;}
&#x9;if (state == AUDIT_DISABLED)
&#x9;&#x9;return;

&#x9;context-&#x3E;serial     = 0;
&#x9;context-&#x3E;ctime      = CURRENT_TIME;
&#x9;context-&#x3E;in_syscall = 1;
&#x9;context-&#x3E;current_state  = state;
&#x9;context-&#x3E;ppid       = 0;
}

*/
 audit_syscall_exit - deallocate audit context after a system call
 @success: success value of the syscall
 @return_code: return value of the syscall

 Tear down after system call.  If the audit context has been marked as
 auditable (either because of the AUDIT_RECORD_CONTEXT state from
 filtering, or because some other part of the kernel wrote an audit
 message), then write out the syscall information.  In call cases,
 free the names stored from getname().
 /*
void __audit_syscall_exit(int success, long return_code)
{
&#x9;struct task_structtsk = current;
&#x9;struct audit_contextcontext;

&#x9;if (success)
&#x9;&#x9;success = AUDITSC_SUCCESS;
&#x9;else
&#x9;&#x9;success = AUDITSC_FAILURE;

&#x9;context = audit_take_context(tsk, success, return_code);
&#x9;if (!context)
&#x9;&#x9;return;

&#x9;if (context-&#x3E;in_syscall &#x26;&#x26; context-&#x3E;current_state == AUDIT_RECORD_CONTEXT)
&#x9;&#x9;audit_log_exit(context, tsk);

&#x9;context-&#x3E;in_syscall = 0;
&#x9;context-&#x3E;prio = context-&#x3E;state == AUDIT_RECORD_CONTEXT ? ~0ULL : 0;

&#x9;if (!list_empty(&#x26;context-&#x3E;killed_trees))
&#x9;&#x9;audit_kill_trees(&#x26;context-&#x3E;killed_trees);

&#x9;audit_free_names(context);
&#x9;unroll_tree_refs(context, NULL, 0);
&#x9;audit_free_aux(context);
&#x9;context-&#x3E;aux = NULL;
&#x9;context-&#x3E;aux_pids = NULL;
&#x9;context-&#x3E;target_pid = 0;
&#x9;context-&#x3E;target_sid = 0;
&#x9;context-&#x3E;sockaddr_len = 0;
&#x9;context-&#x3E;type = 0;
&#x9;context-&#x3E;fds[0] = -1;
&#x9;if (context-&#x3E;state != AUDIT_RECORD_CONTEXT) {
&#x9;&#x9;kfree(context-&#x3E;filterkey);
&#x9;&#x9;context-&#x3E;filterkey = NULL;
&#x9;}
&#x9;tsk-&#x3E;audit_context = context;
}

static inline void handle_one(const struct inodeinode)
{
#ifdef CONFIG_AUDIT_TREE
&#x9;struct audit_contextcontext;
&#x9;struct audit_tree_refsp;
&#x9;struct audit_chunkchunk;
&#x9;int count;
&#x9;if (likely(hlist_empty(&#x26;inode-&#x3E;i_fsnotify_marks)))
&#x9;&#x9;return;
&#x9;context = current-&#x3E;audit_context;
&#x9;p = context-&#x3E;trees;
&#x9;count = context-&#x3E;tree_count;
&#x9;rcu_read_lock();
&#x9;chunk = audit_tree_lookup(inode);
&#x9;rcu_read_unlock();
&#x9;if (!chunk)
&#x9;&#x9;return;
&#x9;if (likely(put_tree_ref(context, chunk)))
&#x9;&#x9;return;
&#x9;if (unlikely(!grow_tree_refs(context))) {
&#x9;&#x9;pr_warn(&#x22;out of memory, audit has lost a tree reference\n&#x22;);
&#x9;&#x9;audit_set_auditable(context);
&#x9;&#x9;audit_put_chunk(chunk);
&#x9;&#x9;unroll_tree_refs(context, p, count);
&#x9;&#x9;return;
&#x9;}
&#x9;put_tree_ref(context, chunk);
#endif
}

static void handle_path(const struct dentrydentry)
{
#ifdef CONFIG_AUDIT_TREE
&#x9;struct audit_contextcontext;
&#x9;struct audit_tree_refsp;
&#x9;const struct dentryd,parent;
&#x9;struct audit_chunkdrop;
&#x9;unsigned long seq;
&#x9;int count;

&#x9;context = current-&#x3E;audit_context;
&#x9;p = context-&#x3E;trees;
&#x9;count = context-&#x3E;tree_count;
retry:
&#x9;drop = NULL;
&#x9;d = dentry;
&#x9;rcu_read_lock();
&#x9;seq = read_seqbegin(&#x26;rename_lock);
&#x9;for(;;) {
&#x9;&#x9;struct inodeinode = d_backing_inode(d);
&#x9;&#x9;if (inode &#x26;&#x26; unlikely(!hlist_empty(&#x26;inode-&#x3E;i_fsnotify_marks))) {
&#x9;&#x9;&#x9;struct audit_chunkchunk;
&#x9;&#x9;&#x9;chunk = audit_tree_lookup(inode);
&#x9;&#x9;&#x9;if (chunk) {
&#x9;&#x9;&#x9;&#x9;if (unlikely(!put_tree_ref(context, chunk))) {
&#x9;&#x9;&#x9;&#x9;&#x9;drop = chunk;
&#x9;&#x9;&#x9;&#x9;&#x9;break;
&#x9;&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;}
&#x9;&#x9;}
&#x9;&#x9;parent = d-&#x3E;d_parent;
&#x9;&#x9;if (parent == d)
&#x9;&#x9;&#x9;break;
&#x9;&#x9;d = parent;
&#x9;}
&#x9;if (unlikely(read_seqretry(&#x26;rename_lock, seq) || drop)) { / in this order /*
&#x9;&#x9;rcu_read_unlock();
&#x9;&#x9;if (!drop) {
&#x9;&#x9;&#x9;*/ just a race with rename /*
&#x9;&#x9;&#x9;unroll_tree_refs(context, p, count);
&#x9;&#x9;&#x9;goto retry;
&#x9;&#x9;}
&#x9;&#x9;audit_put_chunk(drop);
&#x9;&#x9;if (grow_tree_refs(context)) {
&#x9;&#x9;&#x9;*/ OK, got more space /*
&#x9;&#x9;&#x9;unroll_tree_refs(context, p, count);
&#x9;&#x9;&#x9;goto retry;
&#x9;&#x9;}
&#x9;&#x9;*/ too bad /*
&#x9;&#x9;pr_warn(&#x22;out of memory, audit has lost a tree reference\n&#x22;);
&#x9;&#x9;unroll_tree_refs(context, p, count);
&#x9;&#x9;audit_set_auditable(context);
&#x9;&#x9;return;
&#x9;}
&#x9;rcu_read_unlock();
#endif
}

static struct audit_namesaudit_alloc_name(struct audit_contextcontext,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;unsigned char type)
{
&#x9;struct audit_namesaname;

&#x9;if (context-&#x3E;name_count &#x3C; AUDIT_NAMES) {
&#x9;&#x9;aname = &#x26;context-&#x3E;preallocated_names[context-&#x3E;name_count];
&#x9;&#x9;memset(aname, 0, sizeof(*aname));
&#x9;} else {
&#x9;&#x9;aname = kzalloc(sizeof(*aname), GFP_NOFS);
&#x9;&#x9;if (!aname)
&#x9;&#x9;&#x9;return NULL;
&#x9;&#x9;aname-&#x3E;should_free = true;
&#x9;}

&#x9;aname-&#x3E;ino = AUDIT_INO_UNSET;
&#x9;aname-&#x3E;type = type;
&#x9;list_add_tail(&#x26;aname-&#x3E;list, &#x26;context-&#x3E;names_list);

&#x9;context-&#x3E;name_count++;
&#x9;return aname;
}

*/
 audit_reusename - fill out filename with info from existing entry
 @uptr: userland ptr to pathname

 Search the audit_names list for the current audit context. If there is an
 existing entry with a matching &#x22;uptr&#x22; then return the filename
 associated with that audit_name. If not, return NULL.
 /*
struct filename
__audit_reusename(const __user charuptr)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;struct audit_namesn;

&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
&#x9;&#x9;if (!n-&#x3E;name)
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;if (n-&#x3E;name-&#x3E;uptr == uptr) {
&#x9;&#x9;&#x9;n-&#x3E;name-&#x3E;refcnt++;
&#x9;&#x9;&#x9;return n-&#x3E;name;
&#x9;&#x9;}
&#x9;}
&#x9;return NULL;
}

*/
 audit_getname - add a name to the list
 @name: name to add

 Add a name to the list of audit names for this context.
 Called from fs/namei.c:getname().
 /*
void __audit_getname(struct filenamename)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;struct audit_namesn;

&#x9;if (!context-&#x3E;in_syscall)
&#x9;&#x9;return;

&#x9;n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
&#x9;if (!n)
&#x9;&#x9;return;

&#x9;n-&#x3E;name = name;
&#x9;n-&#x3E;name_len = AUDIT_NAME_FULL;
&#x9;name-&#x3E;aname = n;
&#x9;name-&#x3E;refcnt++;

&#x9;if (!context-&#x3E;pwd.dentry)
&#x9;&#x9;get_fs_pwd(current-&#x3E;fs, &#x26;context-&#x3E;pwd);
}

*/
 __audit_inode - store the inode and device from a lookup
 @name: name being audited
 @dentry: dentry being audited
 @flags: attributes for this particular entry
 /*
void __audit_inode(struct filenamename, const struct dentrydentry,
&#x9;&#x9;   unsigned int flags)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;struct inodeinode = d_backing_inode(dentry);
&#x9;struct audit_namesn;
&#x9;bool parent = flags &#x26; AUDIT_INODE_PARENT;

&#x9;if (!context-&#x3E;in_syscall)
&#x9;&#x9;return;

&#x9;if (!name)
&#x9;&#x9;goto out_alloc;

&#x9;*/
&#x9; If we have a pointer to an audit_names entry already, then we can
&#x9; just use it directly if the type is correct.
&#x9; /*
&#x9;n = name-&#x3E;aname;
&#x9;if (n) {
&#x9;&#x9;if (parent) {
&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_PARENT ||
&#x9;&#x9;&#x9;    n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
&#x9;&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;if (n-&#x3E;type != AUDIT_TYPE_PARENT)
&#x9;&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;}
&#x9;}

&#x9;list_for_each_entry_reverse(n, &#x26;context-&#x3E;names_list, list) {
&#x9;&#x9;if (n-&#x3E;ino) {
&#x9;&#x9;&#x9;*/ valid inode number, use that for the comparison /*
&#x9;&#x9;&#x9;if (n-&#x3E;ino != inode-&#x3E;i_ino ||
&#x9;&#x9;&#x9;    n-&#x3E;dev != inode-&#x3E;i_sb-&#x3E;s_dev)
&#x9;&#x9;&#x9;&#x9;continue;
&#x9;&#x9;} else if (n-&#x3E;name) {
&#x9;&#x9;&#x9;*/ inode number has not been set, check the name /*
&#x9;&#x9;&#x9;if (strcmp(n-&#x3E;name-&#x3E;name, name-&#x3E;name))
&#x9;&#x9;&#x9;&#x9;continue;
&#x9;&#x9;} else
&#x9;&#x9;&#x9;*/ no inode and no name (?!) ... this is odd ... /*
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;*/ match the correct record type /*
&#x9;&#x9;if (parent) {
&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_PARENT ||
&#x9;&#x9;&#x9;    n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
&#x9;&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;if (n-&#x3E;type != AUDIT_TYPE_PARENT)
&#x9;&#x9;&#x9;&#x9;goto out;
&#x9;&#x9;}
&#x9;}

out_alloc:
&#x9;*/ unable to find an entry with both a matching name and type /*
&#x9;n = audit_alloc_name(context, AUDIT_TYPE_UNKNOWN);
&#x9;if (!n)
&#x9;&#x9;return;
&#x9;if (name) {
&#x9;&#x9;n-&#x3E;name = name;
&#x9;&#x9;name-&#x3E;refcnt++;
&#x9;}

out:
&#x9;if (parent) {
&#x9;&#x9;n-&#x3E;name_len = n-&#x3E;name ? parent_len(n-&#x3E;name-&#x3E;name) : AUDIT_NAME_FULL;
&#x9;&#x9;n-&#x3E;type = AUDIT_TYPE_PARENT;
&#x9;&#x9;if (flags &#x26; AUDIT_INODE_HIDDEN)
&#x9;&#x9;&#x9;n-&#x3E;hidden = true;
&#x9;} else {
&#x9;&#x9;n-&#x3E;name_len = AUDIT_NAME_FULL;
&#x9;&#x9;n-&#x3E;type = AUDIT_TYPE_NORMAL;
&#x9;}
&#x9;handle_path(dentry);
&#x9;audit_copy_inode(n, dentry, inode);
}

void __audit_file(const struct filefile)
{
&#x9;__audit_inode(NULL, file-&#x3E;f_path.dentry, 0);
}

*/
 __audit_inode_child - collect inode info for created/removed objects
 @parent: inode of dentry parent
 @dentry: dentry being audited
 @type:   AUDIT_TYPE_* value that we&#x27;re looking for

 For syscalls that create or remove filesystem objects, audit_inode
 can only collect information for the filesystem object&#x27;s parent.
 This call updates the audit context with the child&#x27;s information.
 Syscalls that create a new filesystem object must be hooked after
 the object is created.  Syscalls that remove a filesystem object
 must be hooked prior, in order to capture the target inode during
 unsuccessful attempts.
 /*
void __audit_inode_child(struct inodeparent,
&#x9;&#x9;&#x9; const struct dentrydentry,
&#x9;&#x9;&#x9; const unsigned char type)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;struct inodeinode = d_backing_inode(dentry);
&#x9;const chardname = dentry-&#x3E;d_name.name;
&#x9;struct audit_namesn,found_parent = NULL,found_child = NULL;

&#x9;if (!context-&#x3E;in_syscall)
&#x9;&#x9;return;

&#x9;if (inode)
&#x9;&#x9;handle_one(inode);

&#x9;*/ look for a parent entry first /*
&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
&#x9;&#x9;if (!n-&#x3E;name ||
&#x9;&#x9;    (n-&#x3E;type != AUDIT_TYPE_PARENT &#x26;&#x26;
&#x9;&#x9;     n-&#x3E;type != AUDIT_TYPE_UNKNOWN))
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;if (n-&#x3E;ino == parent-&#x3E;i_ino &#x26;&#x26; n-&#x3E;dev == parent-&#x3E;i_sb-&#x3E;s_dev &#x26;&#x26;
&#x9;&#x9;    !audit_compare_dname_path(dname,
&#x9;&#x9;&#x9;&#x9;&#x9;      n-&#x3E;name-&#x3E;name, n-&#x3E;name_len)) {
&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
&#x9;&#x9;&#x9;&#x9;n-&#x3E;type = AUDIT_TYPE_PARENT;
&#x9;&#x9;&#x9;found_parent = n;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;}

&#x9;*/ is there a matching child entry? /*
&#x9;list_for_each_entry(n, &#x26;context-&#x3E;names_list, list) {
&#x9;&#x9;*/ can only match entries that have a name /*
&#x9;&#x9;if (!n-&#x3E;name ||
&#x9;&#x9;    (n-&#x3E;type != type &#x26;&#x26; n-&#x3E;type != AUDIT_TYPE_UNKNOWN))
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;if (!strcmp(dname, n-&#x3E;name-&#x3E;name) ||
&#x9;&#x9;    !audit_compare_dname_path(dname, n-&#x3E;name-&#x3E;name,
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;found_parent ?
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;found_parent-&#x3E;name_len :
&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;AUDIT_NAME_FULL)) {
&#x9;&#x9;&#x9;if (n-&#x3E;type == AUDIT_TYPE_UNKNOWN)
&#x9;&#x9;&#x9;&#x9;n-&#x3E;type = type;
&#x9;&#x9;&#x9;found_child = n;
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}
&#x9;}

&#x9;if (!found_parent) {
&#x9;&#x9;*/ create a new, &#x22;anonymous&#x22; parent record /*
&#x9;&#x9;n = audit_alloc_name(context, AUDIT_TYPE_PARENT);
&#x9;&#x9;if (!n)
&#x9;&#x9;&#x9;return;
&#x9;&#x9;audit_copy_inode(n, NULL, parent);
&#x9;}

&#x9;if (!found_child) {
&#x9;&#x9;found_child = audit_alloc_name(context, type);
&#x9;&#x9;if (!found_child)
&#x9;&#x9;&#x9;return;

&#x9;&#x9;*/ Re-use the name belonging to the slot for a matching parent
&#x9;&#x9; directory. All names for this context are relinquished in
&#x9;&#x9; audit_free_names() /*
&#x9;&#x9;if (found_parent) {
&#x9;&#x9;&#x9;found_child-&#x3E;name = found_parent-&#x3E;name;
&#x9;&#x9;&#x9;found_child-&#x3E;name_len = AUDIT_NAME_FULL;
&#x9;&#x9;&#x9;found_child-&#x3E;name-&#x3E;refcnt++;
&#x9;&#x9;}
&#x9;}

&#x9;if (inode)
&#x9;&#x9;audit_copy_inode(found_child, dentry, inode);
&#x9;else
&#x9;&#x9;found_child-&#x3E;ino = AUDIT_INO_UNSET;
}
EXPORT_SYMBOL_GPL(__audit_inode_child);

*/
 auditsc_get_stamp - get local copies of audit_context values
 @ctx: audit_context for the task
 @t: timespec to store time recorded in the audit_context
 @serial: serial value that is recorded in the audit_context

 Also sets the context as auditable.
 /*
int auditsc_get_stamp(struct audit_contextctx,
&#x9;&#x9;       struct timespect, unsigned intserial)
{
&#x9;if (!ctx-&#x3E;in_syscall)
&#x9;&#x9;return 0;
&#x9;if (!ctx-&#x3E;serial)
&#x9;&#x9;ctx-&#x3E;serial = audit_serial();
&#x9;t-&#x3E;tv_sec  = ctx-&#x3E;ctime.tv_sec;
&#x9;t-&#x3E;tv_nsec = ctx-&#x3E;ctime.tv_nsec;
&#x9;*serial    = ctx-&#x3E;serial;
&#x9;if (!ctx-&#x3E;prio) {
&#x9;&#x9;ctx-&#x3E;prio = 1;
&#x9;&#x9;ctx-&#x3E;current_state = AUDIT_RECORD_CONTEXT;
&#x9;}
&#x9;return 1;
}

*/ global counter which is incremented every time something logs in /*
static atomic_t session_id = ATOMIC_INIT(0);

static int audit_set_loginuid_perm(kuid_t loginuid)
{
&#x9;*/ if we are unset, we don&#x27;t need privs /*
&#x9;if (!audit_loginuid_set(current))
&#x9;&#x9;return 0;
&#x9;*/ if AUDIT_FEATURE_LOGINUID_IMMUTABLE means never ever allow a change/*
&#x9;if (is_audit_feature_set(AUDIT_FEATURE_LOGINUID_IMMUTABLE))
&#x9;&#x9;return -EPERM;
&#x9;*/ it is set, you need permission /*
&#x9;if (!capable(CAP_AUDIT_CONTROL))
&#x9;&#x9;return -EPERM;
&#x9;*/ reject if this is not an unset and we don&#x27;t allow that /*
&#x9;if (is_audit_feature_set(AUDIT_FEATURE_ONLY_UNSET_LOGINUID) &#x26;&#x26; uid_valid(loginuid))
&#x9;&#x9;return -EPERM;
&#x9;return 0;
}

static void audit_log_set_loginuid(kuid_t koldloginuid, kuid_t kloginuid,
&#x9;&#x9;&#x9;&#x9;   unsigned int oldsessionid, unsigned int sessionid,
&#x9;&#x9;&#x9;&#x9;   int rc)
{
&#x9;struct audit_bufferab;
&#x9;uid_t uid, oldloginuid, loginuid;

&#x9;if (!audit_enabled)
&#x9;&#x9;return;

&#x9;uid = from_kuid(&#x26;init_user_ns, task_uid(current));
&#x9;oldloginuid = from_kuid(&#x26;init_user_ns, koldloginuid);
&#x9;loginuid = from_kuid(&#x26;init_user_ns, kloginuid),

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_LOGIN);
&#x9;if (!ab)
&#x9;&#x9;return;
&#x9;audit_log_format(ab, &#x22;pid=%d uid=%u&#x22;, task_pid_nr(current), uid);
&#x9;audit_log_task_context(ab);
&#x9;audit_log_format(ab, &#x22; old-auid=%u auid=%u old-ses=%u ses=%u res=%d&#x22;,
&#x9;&#x9;&#x9; oldloginuid, loginuid, oldsessionid, sessionid, !rc);
&#x9;audit_log_end(ab);
}

*/
 audit_set_loginuid - set current task&#x27;s audit_context loginuid
 @loginuid: loginuid value

 Returns 0.

 Called (set) from fs/proc/base.c::proc_loginuid_write().
 /*
int audit_set_loginuid(kuid_t loginuid)
{
&#x9;struct task_structtask = current;
&#x9;unsigned int oldsessionid, sessionid = (unsigned int)-1;
&#x9;kuid_t oldloginuid;
&#x9;int rc;

&#x9;oldloginuid = audit_get_loginuid(current);
&#x9;oldsessionid = audit_get_sessionid(current);

&#x9;rc = audit_set_loginuid_perm(loginuid);
&#x9;if (rc)
&#x9;&#x9;goto out;

&#x9;*/ are we setting or clearing? /*
&#x9;if (uid_valid(loginuid))
&#x9;&#x9;sessionid = (unsigned int)atomic_inc_return(&#x26;session_id);

&#x9;task-&#x3E;sessionid = sessionid;
&#x9;task-&#x3E;loginuid = loginuid;
out:
&#x9;audit_log_set_loginuid(oldloginuid, loginuid, oldsessionid, sessionid, rc);
&#x9;return rc;
}

*/
 __audit_mq_open - record audit data for a POSIX MQ open
 @oflag: open flag
 @mode: mode bits
 @attr: queue attributes

 /*
void __audit_mq_open(int oflag, umode_t mode, struct mq_attrattr)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;if (attr)
&#x9;&#x9;memcpy(&#x26;context-&#x3E;mq_open.attr, attr, sizeof(struct mq_attr));
&#x9;else
&#x9;&#x9;memset(&#x26;context-&#x3E;mq_open.attr, 0, sizeof(struct mq_attr));

&#x9;context-&#x3E;mq_open.oflag = oflag;
&#x9;context-&#x3E;mq_open.mode = mode;

&#x9;context-&#x3E;type = AUDIT_MQ_OPEN;
}

*/
 __audit_mq_sendrecv - record audit data for a POSIX MQ timed send/receive
 @mqdes: MQ descriptor
 @msg_len: Message length
 @msg_prio: Message priority
 @abs_timeout: Message timeout in absolute time

 /*
void __audit_mq_sendrecv(mqd_t mqdes, size_t msg_len, unsigned int msg_prio,
&#x9;&#x9;&#x9;const struct timespecabs_timeout)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;struct timespecp = &#x26;context-&#x3E;mq_sendrecv.abs_timeout;

&#x9;if (abs_timeout)
&#x9;&#x9;memcpy(p, abs_timeout, sizeof(struct timespec));
&#x9;else
&#x9;&#x9;memset(p, 0, sizeof(struct timespec));

&#x9;context-&#x3E;mq_sendrecv.mqdes = mqdes;
&#x9;context-&#x3E;mq_sendrecv.msg_len = msg_len;
&#x9;context-&#x3E;mq_sendrecv.msg_prio = msg_prio;

&#x9;context-&#x3E;type = AUDIT_MQ_SENDRECV;
}

*/
 __audit_mq_notify - record audit data for a POSIX MQ notify
 @mqdes: MQ descriptor
 @notification: Notification event

 /*

void __audit_mq_notify(mqd_t mqdes, const struct sigeventnotification)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;if (notification)
&#x9;&#x9;context-&#x3E;mq_notify.sigev_signo = notification-&#x3E;sigev_signo;
&#x9;else
&#x9;&#x9;context-&#x3E;mq_notify.sigev_signo = 0;

&#x9;context-&#x3E;mq_notify.mqdes = mqdes;
&#x9;context-&#x3E;type = AUDIT_MQ_NOTIFY;
}

*/
 __audit_mq_getsetattr - record audit data for a POSIX MQ get/set attribute
 @mqdes: MQ descriptor
 @mqstat: MQ flags

 /*
void __audit_mq_getsetattr(mqd_t mqdes, struct mq_attrmqstat)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;context-&#x3E;mq_getsetattr.mqdes = mqdes;
&#x9;context-&#x3E;mq_getsetattr.mqstat =mqstat;
&#x9;context-&#x3E;type = AUDIT_MQ_GETSETATTR;
}

*/
 audit_ipc_obj - record audit data for ipc object
 @ipcp: ipc permissions

 /*
void __audit_ipc_obj(struct kern_ipc_permipcp)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;context-&#x3E;ipc.uid = ipcp-&#x3E;uid;
&#x9;context-&#x3E;ipc.gid = ipcp-&#x3E;gid;
&#x9;context-&#x3E;ipc.mode = ipcp-&#x3E;mode;
&#x9;context-&#x3E;ipc.has_perm = 0;
&#x9;security_ipc_getsecid(ipcp, &#x26;context-&#x3E;ipc.osid);
&#x9;context-&#x3E;type = AUDIT_IPC;
}

*/
 audit_ipc_set_perm - record audit data for new ipc permissions
 @qbytes: msgq bytes
 @uid: msgq user id
 @gid: msgq group id
 @mode: msgq mode (permissions)

 Called only after audit_ipc_obj().
 /*
void __audit_ipc_set_perm(unsigned long qbytes, uid_t uid, gid_t gid, umode_t mode)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;context-&#x3E;ipc.qbytes = qbytes;
&#x9;context-&#x3E;ipc.perm_uid = uid;
&#x9;context-&#x3E;ipc.perm_gid = gid;
&#x9;context-&#x3E;ipc.perm_mode = mode;
&#x9;context-&#x3E;ipc.has_perm = 1;
}

void __audit_bprm(struct linux_binprmbprm)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;context-&#x3E;type = AUDIT_EXECVE;
&#x9;context-&#x3E;execve.argc = bprm-&#x3E;argc;
}


*/
 audit_socketcall - record audit data for sys_socketcall
 @nargs: number of args, which should not be more than AUDITSC_ARGS.
 @args: args array

 /*
int __audit_socketcall(int nargs, unsigned longargs)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;if (nargs &#x3C;= 0 || nargs &#x3E; AUDITSC_ARGS || !args)
&#x9;&#x9;return -EINVAL;
&#x9;context-&#x3E;type = AUDIT_SOCKETCALL;
&#x9;context-&#x3E;socketcall.nargs = nargs;
&#x9;memcpy(context-&#x3E;socketcall.args, args, nargs sizeof(unsigned long));
&#x9;return 0;
}

*/
 __audit_fd_pair - record audit data for pipe and socketpair
 @fd1: the first file descriptor
 @fd2: the second file descriptor

 /*
void __audit_fd_pair(int fd1, int fd2)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;context-&#x3E;fds[0] = fd1;
&#x9;context-&#x3E;fds[1] = fd2;
}

*/
 audit_sockaddr - record audit data for sys_bind, sys_connect, sys_sendto
 @len: data length in user space
 @a: data address in kernel space

 Returns 0 for success or NULL context or &#x3C; 0 on error.
 /*
int __audit_sockaddr(int len, voida)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;if (!context-&#x3E;sockaddr) {
&#x9;&#x9;voidp = kmalloc(sizeof(struct sockaddr_storage), GFP_KERNEL);
&#x9;&#x9;if (!p)
&#x9;&#x9;&#x9;return -ENOMEM;
&#x9;&#x9;context-&#x3E;sockaddr = p;
&#x9;}

&#x9;context-&#x3E;sockaddr_len = len;
&#x9;memcpy(context-&#x3E;sockaddr, a, len);
&#x9;return 0;
}

void __audit_ptrace(struct task_structt)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;

&#x9;context-&#x3E;target_pid = task_pid_nr(t);
&#x9;context-&#x3E;target_auid = audit_get_loginuid(t);
&#x9;context-&#x3E;target_uid = task_uid(t);
&#x9;context-&#x3E;target_sessionid = audit_get_sessionid(t);
&#x9;security_task_getsecid(t, &#x26;context-&#x3E;target_sid);
&#x9;memcpy(context-&#x3E;target_comm, t-&#x3E;comm, TASK_COMM_LEN);
}

*/
 audit_signal_info - record signal info for shutting down audit subsystem
 @sig: signal value
 @t: task being signaled

 If the audit subsystem is being terminated, record the task (pid)
 and uid that is doing that.
 /*
int __audit_signal_info(int sig, struct task_structt)
{
&#x9;struct audit_aux_data_pidsaxp;
&#x9;struct task_structtsk = current;
&#x9;struct audit_contextctx = tsk-&#x3E;audit_context;
&#x9;kuid_t uid = current_uid(), t_uid = task_uid(t);

&#x9;if (audit_pid &#x26;&#x26; t-&#x3E;tgid == audit_pid) {
&#x9;&#x9;if (sig == SIGTERM || sig == SIGHUP || sig == SIGUSR1 || sig == SIGUSR2) {
&#x9;&#x9;&#x9;audit_sig_pid = task_pid_nr(tsk);
&#x9;&#x9;&#x9;if (uid_valid(tsk-&#x3E;loginuid))
&#x9;&#x9;&#x9;&#x9;audit_sig_uid = tsk-&#x3E;loginuid;
&#x9;&#x9;&#x9;else
&#x9;&#x9;&#x9;&#x9;audit_sig_uid = uid;
&#x9;&#x9;&#x9;security_task_getsecid(tsk, &#x26;audit_sig_sid);
&#x9;&#x9;}
&#x9;&#x9;if (!audit_signals || audit_dummy_context())
&#x9;&#x9;&#x9;return 0;
&#x9;}

&#x9;*/ optimize the common case by putting first signal recipient directly
&#x9; in audit_context /*
&#x9;if (!ctx-&#x3E;target_pid) {
&#x9;&#x9;ctx-&#x3E;target_pid = task_tgid_nr(t);
&#x9;&#x9;ctx-&#x3E;target_auid = audit_get_loginuid(t);
&#x9;&#x9;ctx-&#x3E;target_uid = t_uid;
&#x9;&#x9;ctx-&#x3E;target_sessionid = audit_get_sessionid(t);
&#x9;&#x9;security_task_getsecid(t, &#x26;ctx-&#x3E;target_sid);
&#x9;&#x9;memcpy(ctx-&#x3E;target_comm, t-&#x3E;comm, TASK_COMM_LEN);
&#x9;&#x9;return 0;
&#x9;}

&#x9;axp = (void)ctx-&#x3E;aux_pids;
&#x9;if (!axp || axp-&#x3E;pid_count == AUDIT_AUX_PIDS) {
&#x9;&#x9;axp = kzalloc(sizeof(*axp), GFP_ATOMIC);
&#x9;&#x9;if (!axp)
&#x9;&#x9;&#x9;return -ENOMEM;

&#x9;&#x9;axp-&#x3E;d.type = AUDIT_OBJ_PID;
&#x9;&#x9;axp-&#x3E;d.next = ctx-&#x3E;aux_pids;
&#x9;&#x9;ctx-&#x3E;aux_pids = (void)axp;
&#x9;}
&#x9;BUG_ON(axp-&#x3E;pid_count &#x3E;= AUDIT_AUX_PIDS);

&#x9;axp-&#x3E;target_pid[axp-&#x3E;pid_count] = task_tgid_nr(t);
&#x9;axp-&#x3E;target_auid[axp-&#x3E;pid_count] = audit_get_loginuid(t);
&#x9;axp-&#x3E;target_uid[axp-&#x3E;pid_count] = t_uid;
&#x9;axp-&#x3E;target_sessionid[axp-&#x3E;pid_count] = audit_get_sessionid(t);
&#x9;security_task_getsecid(t, &#x26;axp-&#x3E;target_sid[axp-&#x3E;pid_count]);
&#x9;memcpy(axp-&#x3E;target_comm[axp-&#x3E;pid_count], t-&#x3E;comm, TASK_COMM_LEN);
&#x9;axp-&#x3E;pid_count++;

&#x9;return 0;
}

*/
 __audit_log_bprm_fcaps - store information about a loading bprm and relevant fcaps
 @bprm: pointer to the bprm being processed
 @new: the proposed new credentials
 @old: the old credentials

 Simply check if the proc already has the caps given by the file and if not
 store the priv escalation info for later auditing at the end of the syscall

 -Eric
 /*
int __audit_log_bprm_fcaps(struct linux_binprmbprm,
&#x9;&#x9;&#x9;   const struct crednew, const struct credold)
{
&#x9;struct audit_aux_data_bprm_fcapsax;
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;struct cpu_vfs_cap_data vcaps;

&#x9;ax = kmalloc(sizeof(*ax), GFP_KERNEL);
&#x9;if (!ax)
&#x9;&#x9;return -ENOMEM;

&#x9;ax-&#x3E;d.type = AUDIT_BPRM_FCAPS;
&#x9;ax-&#x3E;d.next = context-&#x3E;aux;
&#x9;context-&#x3E;aux = (void)ax;

&#x9;get_vfs_caps_from_disk(bprm-&#x3E;file-&#x3E;f_path.dentry, &#x26;vcaps);

&#x9;ax-&#x3E;fcap.permitted = vcaps.permitted;
&#x9;ax-&#x3E;fcap.inheritable = vcaps.inheritable;
&#x9;ax-&#x3E;fcap.fE = !!(vcaps.magic_etc &#x26; VFS_CAP_FLAGS_EFFECTIVE);
&#x9;ax-&#x3E;fcap_ver = (vcaps.magic_etc &#x26; VFS_CAP_REVISION_MASK) &#x3E;&#x3E; VFS_CAP_REVISION_SHIFT;

&#x9;ax-&#x3E;old_pcap.permitted   = old-&#x3E;cap_permitted;
&#x9;ax-&#x3E;old_pcap.inheritable = old-&#x3E;cap_inheritable;
&#x9;ax-&#x3E;old_pcap.effective   = old-&#x3E;cap_effective;

&#x9;ax-&#x3E;new_pcap.permitted   = new-&#x3E;cap_permitted;
&#x9;ax-&#x3E;new_pcap.inheritable = new-&#x3E;cap_inheritable;
&#x9;ax-&#x3E;new_pcap.effective   = new-&#x3E;cap_effective;
&#x9;return 0;
}

*/
 __audit_log_capset - store information about the arguments to the capset syscall
 @new: the new credentials
 @old: the old (current) credentials

 Record the arguments userspace sent to sys_capset for later printing by the
 audit system if applicable
 /*
void __audit_log_capset(const struct crednew, const struct credold)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;context-&#x3E;capset.pid = task_pid_nr(current);
&#x9;context-&#x3E;capset.cap.effective   = new-&#x3E;cap_effective;
&#x9;context-&#x3E;capset.cap.inheritable = new-&#x3E;cap_effective;
&#x9;context-&#x3E;capset.cap.permitted   = new-&#x3E;cap_permitted;
&#x9;context-&#x3E;type = AUDIT_CAPSET;
}

void __audit_mmap_fd(int fd, int flags)
{
&#x9;struct audit_contextcontext = current-&#x3E;audit_context;
&#x9;context-&#x3E;mmap.fd = fd;
&#x9;context-&#x3E;mmap.flags = flags;
&#x9;context-&#x3E;type = AUDIT_MMAP;
}

static void audit_log_task(struct audit_bufferab)
{
&#x9;kuid_t auid, uid;
&#x9;kgid_t gid;
&#x9;unsigned int sessionid;
&#x9;char comm[sizeof(current-&#x3E;comm)];

&#x9;auid = audit_get_loginuid(current);
&#x9;sessionid = audit_get_sessionid(current);
&#x9;current_uid_gid(&#x26;uid, &#x26;gid);

&#x9;audit_log_format(ab, &#x22;auid=%u uid=%u gid=%u ses=%u&#x22;,
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, auid),
&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, uid),
&#x9;&#x9;&#x9; from_kgid(&#x26;init_user_ns, gid),
&#x9;&#x9;&#x9; sessionid);
&#x9;audit_log_task_context(ab);
&#x9;audit_log_format(ab, &#x22; pid=%d comm=&#x22;, task_pid_nr(current));
&#x9;audit_log_untrustedstring(ab, get_task_comm(comm, current));
&#x9;audit_log_d_path_exe(ab, current-&#x3E;mm);
}

*/
 audit_core_dumps - record information about processes that end abnormally
 @signr: signal value

 If a process ends with a core dump, something fishy is going on and we
 should record the event for investigation.
 /*
void audit_core_dumps(long signr)
{
&#x9;struct audit_bufferab;

&#x9;if (!audit_enabled)
&#x9;&#x9;return;

&#x9;if (signr == SIGQUIT)&#x9;*/ don&#x27;t care for those /*
&#x9;&#x9;return;

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_ANOM_ABEND);
&#x9;if (unlikely(!ab))
&#x9;&#x9;return;
&#x9;audit_log_task(ab);
&#x9;audit_log_format(ab, &#x22; sig=%ld&#x22;, signr);
&#x9;audit_log_end(ab);
}

void __audit_seccomp(unsigned long syscall, long signr, int code)
{
&#x9;struct audit_bufferab;

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_SECCOMP);
&#x9;if (unlikely(!ab))
&#x9;&#x9;return;
&#x9;audit_log_task(ab);
&#x9;audit_log_format(ab, &#x22; sig=%ld arch=%x syscall=%ld compat=%d ip=0x%lx code=0x%x&#x22;,
&#x9;&#x9;&#x9; signr, syscall_get_arch(), syscall,
&#x9;&#x9;&#x9; in_compat_syscall(), KSTK_EIP(current), code);
&#x9;audit_log_end(ab);
}

struct list_headaudit_killed_trees(void)
{
&#x9;struct audit_contextctx = current-&#x3E;audit_context;
&#x9;if (likely(!ctx || !ctx-&#x3E;in_syscall))
&#x9;&#x9;return NULL;
&#x9;return &#x26;ctx-&#x3E;killed_trees;
}
*/
/*
#include &#x22;audit.h&#x22;
#include &#x3C;linux/fsnotify_backend.h&#x3E;
#include &#x3C;linux/namei.h&#x3E;
#include &#x3C;linux/mount.h&#x3E;
#include &#x3C;linux/kthread.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;

struct audit_tree;
struct audit_chunk;

struct audit_tree {
&#x9;atomic_t count;
&#x9;int goner;
&#x9;struct audit_chunkroot;
&#x9;struct list_head chunks;
&#x9;struct list_head rules;
&#x9;struct list_head list;
&#x9;struct list_head same_root;
&#x9;struct rcu_head head;
&#x9;char pathname[];
};

struct audit_chunk {
&#x9;struct list_head hash;
&#x9;struct fsnotify_mark mark;
&#x9;struct list_head trees;&#x9;&#x9;*/ with root here /*
&#x9;int dead;
&#x9;int count;
&#x9;atomic_long_t refs;
&#x9;struct rcu_head head;
&#x9;struct node {
&#x9;&#x9;struct list_head list;
&#x9;&#x9;struct audit_treeowner;
&#x9;&#x9;unsigned index;&#x9;&#x9;*/ index; upper bit indicates &#x27;will prune&#x27; /*
&#x9;} owners[];
};

static LIST_HEAD(tree_list);
static LIST_HEAD(prune_list);
static struct task_structprune_thread;

*/
 One struct chunk is attached to each inode of interest.
 We replace struct chunk on tagging/untagging.
 Rules have pointer to struct audit_tree.
 Rules have struct list_head rlist forming a list of rules over
 the same tree.
 References to struct chunk are collected at audit_inode{,_child}()
 time and used in AUDIT_TREE rule matching.
 These references are dropped at the same time we are calling
 audit_free_names(), etc.

 Cyclic lists galore:
 tree.chunks anchors chunk.owners[].list&#x9;&#x9;&#x9;hash_lock
 tree.rules anchors rule.rlist&#x9;&#x9;&#x9;&#x9;audit_filter_mutex
 chunk.trees anchors tree.same_root&#x9;&#x9;&#x9;&#x9;hash_lock
 chunk.hash is a hash with middle bits of watch.inode as
 a hash function.&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;RCU, hash_lock

 tree is refcounted; one reference for &#x22;some rules on rules_list refer to
 it&#x22;, one for each chunk with pointer to it.

 chunk is refcounted by embedded fsnotify_mark + .refs (non-zero refcount
 of watch contributes 1 to .refs).

 node.index allows to get from node.list to containing chunk.
 MSB of that sucker is stolen to mark taggings that we might have to
 revert - several operations have very unpleasant cleanup logics and
 that makes a difference.  Some.
 /*

static struct fsnotify_groupaudit_tree_group;

static struct audit_treealloc_tree(const chars)
{
&#x9;struct audit_treetree;

&#x9;tree = kmalloc(sizeof(struct audit_tree) + strlen(s) + 1, GFP_KERNEL);
&#x9;if (tree) {
&#x9;&#x9;atomic_set(&#x26;tree-&#x3E;count, 1);
&#x9;&#x9;tree-&#x3E;goner = 0;
&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;chunks);
&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;rules);
&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;list);
&#x9;&#x9;INIT_LIST_HEAD(&#x26;tree-&#x3E;same_root);
&#x9;&#x9;tree-&#x3E;root = NULL;
&#x9;&#x9;strcpy(tree-&#x3E;pathname, s);
&#x9;}
&#x9;return tree;
}

static inline void get_tree(struct audit_treetree)
{
&#x9;atomic_inc(&#x26;tree-&#x3E;count);
}

static inline void put_tree(struct audit_treetree)
{
&#x9;if (atomic_dec_and_test(&#x26;tree-&#x3E;count))
&#x9;&#x9;kfree_rcu(tree, head);
}

*/ to avoid bringing the entire thing in audit.h /*
const charaudit_tree_path(struct audit_treetree)
{
&#x9;return tree-&#x3E;pathname;
}

static void free_chunk(struct audit_chunkchunk)
{
&#x9;int i;

&#x9;for (i = 0; i &#x3C; chunk-&#x3E;count; i++) {
&#x9;&#x9;if (chunk-&#x3E;owners[i].owner)
&#x9;&#x9;&#x9;put_tree(chunk-&#x3E;owners[i].owner);
&#x9;}
&#x9;kfree(chunk);
}

void audit_put_chunk(struct audit_chunkchunk)
{
&#x9;if (atomic_long_dec_and_test(&#x26;chunk-&#x3E;refs))
&#x9;&#x9;free_chunk(chunk);
}

static void __put_chunk(struct rcu_headrcu)
{
&#x9;struct audit_chunkchunk = container_of(rcu, struct audit_chunk, head);
&#x9;audit_put_chunk(chunk);
}

static void audit_tree_destroy_watch(struct fsnotify_markentry)
{
&#x9;struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);
&#x9;call_rcu(&#x26;chunk-&#x3E;head, __put_chunk);
}

static struct audit_chunkalloc_chunk(int count)
{
&#x9;struct audit_chunkchunk;
&#x9;size_t size;
&#x9;int i;

&#x9;size = offsetof(struct audit_chunk, owners) + count sizeof(struct node);
&#x9;chunk = kzalloc(size, GFP_KERNEL);
&#x9;if (!chunk)
&#x9;&#x9;return NULL;

&#x9;INIT_LIST_HEAD(&#x26;chunk-&#x3E;hash);
&#x9;INIT_LIST_HEAD(&#x26;chunk-&#x3E;trees);
&#x9;chunk-&#x3E;count = count;
&#x9;atomic_long_set(&#x26;chunk-&#x3E;refs, 1);
&#x9;for (i = 0; i &#x3C; count; i++) {
&#x9;&#x9;INIT_LIST_HEAD(&#x26;chunk-&#x3E;owners[i].list);
&#x9;&#x9;chunk-&#x3E;owners[i].index = i;
&#x9;}
&#x9;fsnotify_init_mark(&#x26;chunk-&#x3E;mark, audit_tree_destroy_watch);
&#x9;chunk-&#x3E;mark.mask = FS_IN_IGNORED;
&#x9;return chunk;
}

enum {HASH_SIZE = 128};
static struct list_head chunk_hash_heads[HASH_SIZE];
static __cacheline_aligned_in_smp DEFINE_SPINLOCK(hash_lock);

static inline struct list_headchunk_hash(const struct inodeinode)
{
&#x9;unsigned long n = (unsigned long)inode / L1_CACHE_BYTES;
&#x9;return chunk_hash_heads + n % HASH_SIZE;
}

*/ hash_lock &#x26; entry-&#x3E;lock is held by caller /*
static void insert_hash(struct audit_chunkchunk)
{
&#x9;struct fsnotify_markentry = &#x26;chunk-&#x3E;mark;
&#x9;struct list_headlist;

&#x9;if (!entry-&#x3E;inode)
&#x9;&#x9;return;
&#x9;list = chunk_hash(entry-&#x3E;inode);
&#x9;list_add_rcu(&#x26;chunk-&#x3E;hash, list);
}

*/ called under rcu_read_lock /*
struct audit_chunkaudit_tree_lookup(const struct inodeinode)
{
&#x9;struct list_headlist = chunk_hash(inode);
&#x9;struct audit_chunkp;

&#x9;list_for_each_entry_rcu(p, list, hash) {
&#x9;&#x9;*/ mark.inode may have gone NULL, but who cares? /*
&#x9;&#x9;if (p-&#x3E;mark.inode == inode) {
&#x9;&#x9;&#x9;atomic_long_inc(&#x26;p-&#x3E;refs);
&#x9;&#x9;&#x9;return p;
&#x9;&#x9;}
&#x9;}
&#x9;return NULL;
}

bool audit_tree_match(struct audit_chunkchunk, struct audit_treetree)
{
&#x9;int n;
&#x9;for (n = 0; n &#x3C; chunk-&#x3E;count; n++)
&#x9;&#x9;if (chunk-&#x3E;owners[n].owner == tree)
&#x9;&#x9;&#x9;return true;
&#x9;return false;
}

*/ tagging and untagging inodes with trees /*

static struct audit_chunkfind_chunk(struct nodep)
{
&#x9;int index = p-&#x3E;index &#x26; ~(1U&#x3C;&#x3C;31);
&#x9;p -= index;
&#x9;return container_of(p, struct audit_chunk, owners[0]);
}

static void untag_chunk(struct nodep)
{
&#x9;struct audit_chunkchunk = find_chunk(p);
&#x9;struct fsnotify_markentry = &#x26;chunk-&#x3E;mark;
&#x9;struct audit_chunknew = NULL;
&#x9;struct audit_treeowner;
&#x9;int size = chunk-&#x3E;count - 1;
&#x9;int i, j;

&#x9;fsnotify_get_mark(entry);

&#x9;spin_unlock(&#x26;hash_lock);

&#x9;if (size)
&#x9;&#x9;new = alloc_chunk(size);

&#x9;spin_lock(&#x26;entry-&#x3E;lock);
&#x9;if (chunk-&#x3E;dead || !entry-&#x3E;inode) {
&#x9;&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
&#x9;&#x9;if (new)
&#x9;&#x9;&#x9;free_chunk(new);
&#x9;&#x9;goto out;
&#x9;}

&#x9;owner = p-&#x3E;owner;

&#x9;if (!size) {
&#x9;&#x9;chunk-&#x3E;dead = 1;
&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;&#x9;list_del_init(&#x26;chunk-&#x3E;trees);
&#x9;&#x9;if (owner-&#x3E;root == chunk)
&#x9;&#x9;&#x9;owner-&#x3E;root = NULL;
&#x9;&#x9;list_del_init(&#x26;p-&#x3E;list);
&#x9;&#x9;list_del_rcu(&#x26;chunk-&#x3E;hash);
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
&#x9;&#x9;fsnotify_destroy_mark(entry, audit_tree_group);
&#x9;&#x9;goto out;
&#x9;}

&#x9;if (!new)
&#x9;&#x9;goto Fallback;

&#x9;fsnotify_duplicate_mark(&#x26;new-&#x3E;mark, entry);
&#x9;if (fsnotify_add_mark(&#x26;new-&#x3E;mark, new-&#x3E;mark.group, new-&#x3E;mark.inode, NULL, 1)) {
&#x9;&#x9;fsnotify_put_mark(&#x26;new-&#x3E;mark);
&#x9;&#x9;goto Fallback;
&#x9;}

&#x9;chunk-&#x3E;dead = 1;
&#x9;spin_lock(&#x26;hash_lock);
&#x9;list_replace_init(&#x26;chunk-&#x3E;trees, &#x26;new-&#x3E;trees);
&#x9;if (owner-&#x3E;root == chunk) {
&#x9;&#x9;list_del_init(&#x26;owner-&#x3E;same_root);
&#x9;&#x9;owner-&#x3E;root = NULL;
&#x9;}

&#x9;for (i = j = 0; j &#x3C;= size; i++, j++) {
&#x9;&#x9;struct audit_trees;
&#x9;&#x9;if (&#x26;chunk-&#x3E;owners[j] == p) {
&#x9;&#x9;&#x9;list_del_init(&#x26;p-&#x3E;list);
&#x9;&#x9;&#x9;i--;
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;}
&#x9;&#x9;s = chunk-&#x3E;owners[j].owner;
&#x9;&#x9;new-&#x3E;owners[i].owner = s;
&#x9;&#x9;new-&#x3E;owners[i].index = chunk-&#x3E;owners[j].index - j + i;
&#x9;&#x9;if (!s)/ result of earlier fallback /*
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;get_tree(s);
&#x9;&#x9;list_replace_init(&#x26;chunk-&#x3E;owners[j].list, &#x26;new-&#x3E;owners[i].list);
&#x9;}

&#x9;list_replace_rcu(&#x26;chunk-&#x3E;hash, &#x26;new-&#x3E;hash);
&#x9;list_for_each_entry(owner, &#x26;new-&#x3E;trees, same_root)
&#x9;&#x9;owner-&#x3E;root = new;
&#x9;spin_unlock(&#x26;hash_lock);
&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
&#x9;fsnotify_destroy_mark(entry, audit_tree_group);
&#x9;fsnotify_put_mark(&#x26;new-&#x3E;mark);&#x9;*/ drop initial reference /*
&#x9;goto out;

Fallback:
&#x9;// do the best we can
&#x9;spin_lock(&#x26;hash_lock);
&#x9;if (owner-&#x3E;root == chunk) {
&#x9;&#x9;list_del_init(&#x26;owner-&#x3E;same_root);
&#x9;&#x9;owner-&#x3E;root = NULL;
&#x9;}
&#x9;list_del_init(&#x26;p-&#x3E;list);
&#x9;p-&#x3E;owner = NULL;
&#x9;put_tree(owner);
&#x9;spin_unlock(&#x26;hash_lock);
&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
out:
&#x9;fsnotify_put_mark(entry);
&#x9;spin_lock(&#x26;hash_lock);
}

static int create_chunk(struct inodeinode, struct audit_treetree)
{
&#x9;struct fsnotify_markentry;
&#x9;struct audit_chunkchunk = alloc_chunk(1);
&#x9;if (!chunk)
&#x9;&#x9;return -ENOMEM;

&#x9;entry = &#x26;chunk-&#x3E;mark;
&#x9;if (fsnotify_add_mark(entry, audit_tree_group, inode, NULL, 0)) {
&#x9;&#x9;fsnotify_put_mark(entry);
&#x9;&#x9;return -ENOSPC;
&#x9;}

&#x9;spin_lock(&#x26;entry-&#x3E;lock);
&#x9;spin_lock(&#x26;hash_lock);
&#x9;if (tree-&#x3E;goner) {
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;chunk-&#x3E;dead = 1;
&#x9;&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
&#x9;&#x9;fsnotify_destroy_mark(entry, audit_tree_group);
&#x9;&#x9;fsnotify_put_mark(entry);
&#x9;&#x9;return 0;
&#x9;}
&#x9;chunk-&#x3E;owners[0].index = (1U &#x3C;&#x3C; 31);
&#x9;chunk-&#x3E;owners[0].owner = tree;
&#x9;get_tree(tree);
&#x9;list_add(&#x26;chunk-&#x3E;owners[0].list, &#x26;tree-&#x3E;chunks);
&#x9;if (!tree-&#x3E;root) {
&#x9;&#x9;tree-&#x3E;root = chunk;
&#x9;&#x9;list_add(&#x26;tree-&#x3E;same_root, &#x26;chunk-&#x3E;trees);
&#x9;}
&#x9;insert_hash(chunk);
&#x9;spin_unlock(&#x26;hash_lock);
&#x9;spin_unlock(&#x26;entry-&#x3E;lock);
&#x9;fsnotify_put_mark(entry);&#x9;*/ drop initial reference /*
&#x9;return 0;
}

*/ the first tagged inode becomes root of tree /*
static int tag_chunk(struct inodeinode, struct audit_treetree)
{
&#x9;struct fsnotify_markold_entry,chunk_entry;
&#x9;struct audit_treeowner;
&#x9;struct audit_chunkchunk,old;
&#x9;struct nodep;
&#x9;int n;

&#x9;old_entry = fsnotify_find_inode_mark(audit_tree_group, inode);
&#x9;if (!old_entry)
&#x9;&#x9;return create_chunk(inode, tree);

&#x9;old = container_of(old_entry, struct audit_chunk, mark);

&#x9;*/ are we already there? /*
&#x9;spin_lock(&#x26;hash_lock);
&#x9;for (n = 0; n &#x3C; old-&#x3E;count; n++) {
&#x9;&#x9;if (old-&#x3E;owners[n].owner == tree) {
&#x9;&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;&#x9;fsnotify_put_mark(old_entry);
&#x9;&#x9;&#x9;return 0;
&#x9;&#x9;}
&#x9;}
&#x9;spin_unlock(&#x26;hash_lock);

&#x9;chunk = alloc_chunk(old-&#x3E;count + 1);
&#x9;if (!chunk) {
&#x9;&#x9;fsnotify_put_mark(old_entry);
&#x9;&#x9;return -ENOMEM;
&#x9;}

&#x9;chunk_entry = &#x26;chunk-&#x3E;mark;

&#x9;spin_lock(&#x26;old_entry-&#x3E;lock);
&#x9;if (!old_entry-&#x3E;inode) {
&#x9;&#x9;*/ old_entry is being shot, lets just lie /*
&#x9;&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
&#x9;&#x9;fsnotify_put_mark(old_entry);
&#x9;&#x9;free_chunk(chunk);
&#x9;&#x9;return -ENOENT;
&#x9;}

&#x9;fsnotify_duplicate_mark(chunk_entry, old_entry);
&#x9;if (fsnotify_add_mark(chunk_entry, chunk_entry-&#x3E;group, chunk_entry-&#x3E;inode, NULL, 1)) {
&#x9;&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
&#x9;&#x9;fsnotify_put_mark(chunk_entry);
&#x9;&#x9;fsnotify_put_mark(old_entry);
&#x9;&#x9;return -ENOSPC;
&#x9;}

&#x9;*/ even though we hold old_entry-&#x3E;lock, this is safe since chunk_entry-&#x3E;lock could NEVER have been grabbed before /*
&#x9;spin_lock(&#x26;chunk_entry-&#x3E;lock);
&#x9;spin_lock(&#x26;hash_lock);

&#x9;*/ we now hold old_entry-&#x3E;lock, chunk_entry-&#x3E;lock, and hash_lock /*
&#x9;if (tree-&#x3E;goner) {
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;chunk-&#x3E;dead = 1;
&#x9;&#x9;spin_unlock(&#x26;chunk_entry-&#x3E;lock);
&#x9;&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);

&#x9;&#x9;fsnotify_destroy_mark(chunk_entry, audit_tree_group);

&#x9;&#x9;fsnotify_put_mark(chunk_entry);
&#x9;&#x9;fsnotify_put_mark(old_entry);
&#x9;&#x9;return 0;
&#x9;}
&#x9;list_replace_init(&#x26;old-&#x3E;trees, &#x26;chunk-&#x3E;trees);
&#x9;for (n = 0, p = chunk-&#x3E;owners; n &#x3C; old-&#x3E;count; n++, p++) {
&#x9;&#x9;struct audit_trees = old-&#x3E;owners[n].owner;
&#x9;&#x9;p-&#x3E;owner = s;
&#x9;&#x9;p-&#x3E;index = old-&#x3E;owners[n].index;
&#x9;&#x9;if (!s)/ result of fallback in untag /*
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;get_tree(s);
&#x9;&#x9;list_replace_init(&#x26;old-&#x3E;owners[n].list, &#x26;p-&#x3E;list);
&#x9;}
&#x9;p-&#x3E;index = (chunk-&#x3E;count - 1) | (1U&#x3C;&#x3C;31);
&#x9;p-&#x3E;owner = tree;
&#x9;get_tree(tree);
&#x9;list_add(&#x26;p-&#x3E;list, &#x26;tree-&#x3E;chunks);
&#x9;list_replace_rcu(&#x26;old-&#x3E;hash, &#x26;chunk-&#x3E;hash);
&#x9;list_for_each_entry(owner, &#x26;chunk-&#x3E;trees, same_root)
&#x9;&#x9;owner-&#x3E;root = chunk;
&#x9;old-&#x3E;dead = 1;
&#x9;if (!tree-&#x3E;root) {
&#x9;&#x9;tree-&#x3E;root = chunk;
&#x9;&#x9;list_add(&#x26;tree-&#x3E;same_root, &#x26;chunk-&#x3E;trees);
&#x9;}
&#x9;spin_unlock(&#x26;hash_lock);
&#x9;spin_unlock(&#x26;chunk_entry-&#x3E;lock);
&#x9;spin_unlock(&#x26;old_entry-&#x3E;lock);
&#x9;fsnotify_destroy_mark(old_entry, audit_tree_group);
&#x9;fsnotify_put_mark(chunk_entry);&#x9;*/ drop initial reference /*
&#x9;fsnotify_put_mark(old_entry);/ pair to fsnotify_find mark_entry /*
&#x9;return 0;
}

static void audit_tree_log_remove_rule(struct audit_krulerule)
{
&#x9;struct audit_bufferab;

&#x9;ab = audit_log_start(NULL, GFP_KERNEL, AUDIT_CONFIG_CHANGE);
&#x9;if (unlikely(!ab))
&#x9;&#x9;return;
&#x9;audit_log_format(ab, &#x22;op=&#x22;);
&#x9;audit_log_string(ab, &#x22;remove_rule&#x22;);
&#x9;audit_log_format(ab, &#x22; dir=&#x22;);
&#x9;audit_log_untrustedstring(ab, rule-&#x3E;tree-&#x3E;pathname);
&#x9;audit_log_key(ab, rule-&#x3E;filterkey);
&#x9;audit_log_format(ab, &#x22; list=%d res=1&#x22;, rule-&#x3E;listnr);
&#x9;audit_log_end(ab);
}

static void kill_rules(struct audit_treetree)
{
&#x9;struct audit_krulerule,next;
&#x9;struct audit_entryentry;

&#x9;list_for_each_entry_safe(rule, next, &#x26;tree-&#x3E;rules, rlist) {
&#x9;&#x9;entry = container_of(rule, struct audit_entry, rule);

&#x9;&#x9;list_del_init(&#x26;rule-&#x3E;rlist);
&#x9;&#x9;if (rule-&#x3E;tree) {
&#x9;&#x9;&#x9;*/ not a half-baked one /*
&#x9;&#x9;&#x9;audit_tree_log_remove_rule(rule);
&#x9;&#x9;&#x9;if (entry-&#x3E;rule.exe)
&#x9;&#x9;&#x9;&#x9;audit_remove_mark(entry-&#x3E;rule.exe);
&#x9;&#x9;&#x9;rule-&#x3E;tree = NULL;
&#x9;&#x9;&#x9;list_del_rcu(&#x26;entry-&#x3E;list);
&#x9;&#x9;&#x9;list_del(&#x26;entry-&#x3E;rule.list);
&#x9;&#x9;&#x9;call_rcu(&#x26;entry-&#x3E;rcu, audit_free_rule_rcu);
&#x9;&#x9;}
&#x9;}
}

*/
 finish killing struct audit_tree
 /*
static void prune_one(struct audit_treevictim)
{
&#x9;spin_lock(&#x26;hash_lock);
&#x9;while (!list_empty(&#x26;victim-&#x3E;chunks)) {
&#x9;&#x9;struct nodep;

&#x9;&#x9;p = list_entry(victim-&#x3E;chunks.next, struct node, list);

&#x9;&#x9;untag_chunk(p);
&#x9;}
&#x9;spin_unlock(&#x26;hash_lock);
&#x9;put_tree(victim);
}

*/ trim the uncommitted chunks from tree /*

static void trim_marked(struct audit_treetree)
{
&#x9;struct list_headp,q;
&#x9;spin_lock(&#x26;hash_lock);
&#x9;if (tree-&#x3E;goner) {
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;return;
&#x9;}
&#x9;*/ reorder /*
&#x9;for (p = tree-&#x3E;chunks.next; p != &#x26;tree-&#x3E;chunks; p = q) {
&#x9;&#x9;struct nodenode = list_entry(p, struct node, list);
&#x9;&#x9;q = p-&#x3E;next;
&#x9;&#x9;if (node-&#x3E;index &#x26; (1U&#x3C;&#x3C;31)) {
&#x9;&#x9;&#x9;list_del_init(p);
&#x9;&#x9;&#x9;list_add(p, &#x26;tree-&#x3E;chunks);
&#x9;&#x9;}
&#x9;}

&#x9;while (!list_empty(&#x26;tree-&#x3E;chunks)) {
&#x9;&#x9;struct nodenode;

&#x9;&#x9;node = list_entry(tree-&#x3E;chunks.next, struct node, list);

&#x9;&#x9;*/ have we run out of marked? /*
&#x9;&#x9;if (!(node-&#x3E;index &#x26; (1U&#x3C;&#x3C;31)))
&#x9;&#x9;&#x9;break;

&#x9;&#x9;untag_chunk(node);
&#x9;}
&#x9;if (!tree-&#x3E;root &#x26;&#x26; !tree-&#x3E;goner) {
&#x9;&#x9;tree-&#x3E;goner = 1;
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;&#x9;kill_rules(tree);
&#x9;&#x9;list_del_init(&#x26;tree-&#x3E;list);
&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;&#x9;prune_one(tree);
&#x9;} else {
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;}
}

static void audit_schedule_prune(void);

*/ called with audit_filter_mutex /*
int audit_remove_tree_rule(struct audit_krulerule)
{
&#x9;struct audit_treetree;
&#x9;tree = rule-&#x3E;tree;
&#x9;if (tree) {
&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;&#x9;list_del_init(&#x26;rule-&#x3E;rlist);
&#x9;&#x9;if (list_empty(&#x26;tree-&#x3E;rules) &#x26;&#x26; !tree-&#x3E;goner) {
&#x9;&#x9;&#x9;tree-&#x3E;root = NULL;
&#x9;&#x9;&#x9;list_del_init(&#x26;tree-&#x3E;same_root);
&#x9;&#x9;&#x9;tree-&#x3E;goner = 1;
&#x9;&#x9;&#x9;list_move(&#x26;tree-&#x3E;list, &#x26;prune_list);
&#x9;&#x9;&#x9;rule-&#x3E;tree = NULL;
&#x9;&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;&#x9;audit_schedule_prune();
&#x9;&#x9;&#x9;return 1;
&#x9;&#x9;}
&#x9;&#x9;rule-&#x3E;tree = NULL;
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;return 1;
&#x9;}
&#x9;return 0;
}

static int compare_root(struct vfsmountmnt, voidarg)
{
&#x9;return d_backing_inode(mnt-&#x3E;mnt_root) == arg;
}

void audit_trim_trees(void)
{
&#x9;struct list_head cursor;

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;list_add(&#x26;cursor, &#x26;tree_list);
&#x9;while (cursor.next != &#x26;tree_list) {
&#x9;&#x9;struct audit_treetree;
&#x9;&#x9;struct path path;
&#x9;&#x9;struct vfsmountroot_mnt;
&#x9;&#x9;struct nodenode;
&#x9;&#x9;int err;

&#x9;&#x9;tree = container_of(cursor.next, struct audit_tree, list);
&#x9;&#x9;get_tree(tree);
&#x9;&#x9;list_del(&#x26;cursor);
&#x9;&#x9;list_add(&#x26;cursor, &#x26;tree-&#x3E;list);
&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;&#x9;err = kern_path(tree-&#x3E;pathname, 0, &#x26;path);
&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;goto skip_it;

&#x9;&#x9;root_mnt = collect_mounts(&#x26;path);
&#x9;&#x9;path_put(&#x26;path);
&#x9;&#x9;if (IS_ERR(root_mnt))
&#x9;&#x9;&#x9;goto skip_it;

&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;&#x9;list_for_each_entry(node, &#x26;tree-&#x3E;chunks, list) {
&#x9;&#x9;&#x9;struct audit_chunkchunk = find_chunk(node);
&#x9;&#x9;&#x9;*/ this could be NULL if the watch is dying else where... /*
&#x9;&#x9;&#x9;struct inodeinode = chunk-&#x3E;mark.inode;
&#x9;&#x9;&#x9;node-&#x3E;index |= 1U&#x3C;&#x3C;31;
&#x9;&#x9;&#x9;if (iterate_mounts(compare_root, inode, root_mnt))
&#x9;&#x9;&#x9;&#x9;node-&#x3E;index &#x26;= ~(1U&#x3C;&#x3C;31);
&#x9;&#x9;}
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;trim_marked(tree);
&#x9;&#x9;drop_collected_mounts(root_mnt);
skip_it:
&#x9;&#x9;put_tree(tree);
&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;}
&#x9;list_del(&#x26;cursor);
&#x9;mutex_unlock(&#x26;audit_filter_mutex);
}

int audit_make_tree(struct audit_krulerule, charpathname, u32 op)
{

&#x9;if (pathname[0] != &#x27;/&#x27; ||
&#x9;    rule-&#x3E;listnr != AUDIT_FILTER_EXIT ||
&#x9;    op != Audit_equal ||
&#x9;    rule-&#x3E;inode_f || rule-&#x3E;watch || rule-&#x3E;tree)
&#x9;&#x9;return -EINVAL;
&#x9;rule-&#x3E;tree = alloc_tree(pathname);
&#x9;if (!rule-&#x3E;tree)
&#x9;&#x9;return -ENOMEM;
&#x9;return 0;
}

void audit_put_tree(struct audit_treetree)
{
&#x9;put_tree(tree);
}

static int tag_mount(struct vfsmountmnt, voidarg)
{
&#x9;return tag_chunk(d_backing_inode(mnt-&#x3E;mnt_root), arg);
}

*/
 That gets run when evict_chunk() ends up needing to kill audit_tree.
 Runs from a separate thread.
 /*
static int prune_tree_thread(voidunused)
{
&#x9;for (;;) {
&#x9;&#x9;set_current_state(TASK_INTERRUPTIBLE);
&#x9;&#x9;if (list_empty(&#x26;prune_list))
&#x9;&#x9;&#x9;schedule();
&#x9;&#x9;__set_current_state(TASK_RUNNING);

&#x9;&#x9;mutex_lock(&#x26;audit_cmd_mutex);
&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);

&#x9;&#x9;while (!list_empty(&#x26;prune_list)) {
&#x9;&#x9;&#x9;struct audit_treevictim;

&#x9;&#x9;&#x9;victim = list_entry(prune_list.next,
&#x9;&#x9;&#x9;&#x9;&#x9;struct audit_tree, list);
&#x9;&#x9;&#x9;list_del_init(&#x26;victim-&#x3E;list);

&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;&#x9;&#x9;prune_one(victim);

&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;&#x9;}

&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
&#x9;}
&#x9;return 0;
}

static int audit_launch_prune(void)
{
&#x9;if (prune_thread)
&#x9;&#x9;return 0;
&#x9;prune_thread = kthread_create(prune_tree_thread, NULL,
&#x9;&#x9;&#x9;&#x9;&#x22;audit_prune_tree&#x22;);
&#x9;if (IS_ERR(prune_thread)) {
&#x9;&#x9;pr_err(&#x22;cannot start thread audit_prune_tree&#x22;);
&#x9;&#x9;prune_thread = NULL;
&#x9;&#x9;return -ENOMEM;
&#x9;} else {
&#x9;&#x9;wake_up_process(prune_thread);
&#x9;&#x9;return 0;
&#x9;}
}

*/ called with audit_filter_mutex /*
int audit_add_tree_rule(struct audit_krulerule)
{
&#x9;struct audit_treeseed = rule-&#x3E;tree,tree;
&#x9;struct path path;
&#x9;struct vfsmountmnt;
&#x9;int err;

&#x9;rule-&#x3E;tree = NULL;
&#x9;list_for_each_entry(tree, &#x26;tree_list, list) {
&#x9;&#x9;if (!strcmp(seed-&#x3E;pathname, tree-&#x3E;pathname)) {
&#x9;&#x9;&#x9;put_tree(seed);
&#x9;&#x9;&#x9;rule-&#x3E;tree = tree;
&#x9;&#x9;&#x9;list_add(&#x26;rule-&#x3E;rlist, &#x26;tree-&#x3E;rules);
&#x9;&#x9;&#x9;return 0;
&#x9;&#x9;}
&#x9;}
&#x9;tree = seed;
&#x9;list_add(&#x26;tree-&#x3E;list, &#x26;tree_list);
&#x9;list_add(&#x26;rule-&#x3E;rlist, &#x26;tree-&#x3E;rules);
&#x9;*/ do not set rule-&#x3E;tree yet /*
&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;if (unlikely(!prune_thread)) {
&#x9;&#x9;err = audit_launch_prune();
&#x9;&#x9;if (err)
&#x9;&#x9;&#x9;goto Err;
&#x9;}

&#x9;err = kern_path(tree-&#x3E;pathname, 0, &#x26;path);
&#x9;if (err)
&#x9;&#x9;goto Err;
&#x9;mnt = collect_mounts(&#x26;path);
&#x9;path_put(&#x26;path);
&#x9;if (IS_ERR(mnt)) {
&#x9;&#x9;err = PTR_ERR(mnt);
&#x9;&#x9;goto Err;
&#x9;}

&#x9;get_tree(tree);
&#x9;err = iterate_mounts(tag_mount, tree, mnt);
&#x9;drop_collected_mounts(mnt);

&#x9;if (!err) {
&#x9;&#x9;struct nodenode;
&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;&#x9;list_for_each_entry(node, &#x26;tree-&#x3E;chunks, list)
&#x9;&#x9;&#x9;node-&#x3E;index &#x26;= ~(1U&#x3C;&#x3C;31);
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;} else {
&#x9;&#x9;trim_marked(tree);
&#x9;&#x9;goto Err;
&#x9;}

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;if (list_empty(&#x26;rule-&#x3E;rlist)) {
&#x9;&#x9;put_tree(tree);
&#x9;&#x9;return -ENOENT;
&#x9;}
&#x9;rule-&#x3E;tree = tree;
&#x9;put_tree(tree);

&#x9;return 0;
Err:
&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;list_del_init(&#x26;tree-&#x3E;list);
&#x9;list_del_init(&#x26;tree-&#x3E;rules);
&#x9;put_tree(tree);
&#x9;return err;
}

int audit_tag_tree(charold, charnew)
{
&#x9;struct list_head cursor, barrier;
&#x9;int failed = 0;
&#x9;struct path path1, path2;
&#x9;struct vfsmounttagged;
&#x9;int err;

&#x9;err = kern_path(new, 0, &#x26;path2);
&#x9;if (err)
&#x9;&#x9;return err;
&#x9;tagged = collect_mounts(&#x26;path2);
&#x9;path_put(&#x26;path2);
&#x9;if (IS_ERR(tagged))
&#x9;&#x9;return PTR_ERR(tagged);

&#x9;err = kern_path(old, 0, &#x26;path1);
&#x9;if (err) {
&#x9;&#x9;drop_collected_mounts(tagged);
&#x9;&#x9;return err;
&#x9;}

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;list_add(&#x26;barrier, &#x26;tree_list);
&#x9;list_add(&#x26;cursor, &#x26;barrier);

&#x9;while (cursor.next != &#x26;tree_list) {
&#x9;&#x9;struct audit_treetree;
&#x9;&#x9;int good_one = 0;

&#x9;&#x9;tree = container_of(cursor.next, struct audit_tree, list);
&#x9;&#x9;get_tree(tree);
&#x9;&#x9;list_del(&#x26;cursor);
&#x9;&#x9;list_add(&#x26;cursor, &#x26;tree-&#x3E;list);
&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;&#x9;err = kern_path(tree-&#x3E;pathname, 0, &#x26;path2);
&#x9;&#x9;if (!err) {
&#x9;&#x9;&#x9;good_one = path_is_under(&#x26;path1, &#x26;path2);
&#x9;&#x9;&#x9;path_put(&#x26;path2);
&#x9;&#x9;}

&#x9;&#x9;if (!good_one) {
&#x9;&#x9;&#x9;put_tree(tree);
&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;&#x9;&#x9;continue;
&#x9;&#x9;}

&#x9;&#x9;failed = iterate_mounts(tag_mount, tree, tagged);
&#x9;&#x9;if (failed) {
&#x9;&#x9;&#x9;put_tree(tree);
&#x9;&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;&#x9;&#x9;break;
&#x9;&#x9;}

&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;&#x9;if (!tree-&#x3E;goner) {
&#x9;&#x9;&#x9;list_del(&#x26;tree-&#x3E;list);
&#x9;&#x9;&#x9;list_add(&#x26;tree-&#x3E;list, &#x26;tree_list);
&#x9;&#x9;}
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;put_tree(tree);
&#x9;}

&#x9;while (barrier.prev != &#x26;tree_list) {
&#x9;&#x9;struct audit_treetree;

&#x9;&#x9;tree = container_of(barrier.prev, struct audit_tree, list);
&#x9;&#x9;get_tree(tree);
&#x9;&#x9;list_del(&#x26;tree-&#x3E;list);
&#x9;&#x9;list_add(&#x26;tree-&#x3E;list, &#x26;barrier);
&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;&#x9;if (!failed) {
&#x9;&#x9;&#x9;struct nodenode;
&#x9;&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;&#x9;&#x9;list_for_each_entry(node, &#x26;tree-&#x3E;chunks, list)
&#x9;&#x9;&#x9;&#x9;node-&#x3E;index &#x26;= ~(1U&#x3C;&#x3C;31);
&#x9;&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;trim_marked(tree);
&#x9;&#x9;}

&#x9;&#x9;put_tree(tree);
&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;}
&#x9;list_del(&#x26;barrier);
&#x9;list_del(&#x26;cursor);
&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;path_put(&#x26;path1);
&#x9;drop_collected_mounts(tagged);
&#x9;return failed;
}


static void audit_schedule_prune(void)
{
&#x9;wake_up_process(prune_thread);
}

*/
 ... and that one is done if evict_chunk() decides to delay until the end
 of syscall.  Runs synchronously.
 /*
void audit_kill_trees(struct list_headlist)
{
&#x9;mutex_lock(&#x26;audit_cmd_mutex);
&#x9;mutex_lock(&#x26;audit_filter_mutex);

&#x9;while (!list_empty(list)) {
&#x9;&#x9;struct audit_treevictim;

&#x9;&#x9;victim = list_entry(list-&#x3E;next, struct audit_tree, list);
&#x9;&#x9;kill_rules(victim);
&#x9;&#x9;list_del_init(&#x26;victim-&#x3E;list);

&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;&#x9;prune_one(victim);

&#x9;&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;}

&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;mutex_unlock(&#x26;audit_cmd_mutex);
}

*/
  Here comes the stuff asynchronous to auditctl operations
 /*

static void evict_chunk(struct audit_chunkchunk)
{
&#x9;struct audit_treeowner;
&#x9;struct list_headpostponed = audit_killed_trees();
&#x9;int need_prune = 0;
&#x9;int n;

&#x9;if (chunk-&#x3E;dead)
&#x9;&#x9;return;

&#x9;chunk-&#x3E;dead = 1;
&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;spin_lock(&#x26;hash_lock);
&#x9;while (!list_empty(&#x26;chunk-&#x3E;trees)) {
&#x9;&#x9;owner = list_entry(chunk-&#x3E;trees.next,
&#x9;&#x9;&#x9;&#x9;   struct audit_tree, same_root);
&#x9;&#x9;owner-&#x3E;goner = 1;
&#x9;&#x9;owner-&#x3E;root = NULL;
&#x9;&#x9;list_del_init(&#x26;owner-&#x3E;same_root);
&#x9;&#x9;spin_unlock(&#x26;hash_lock);
&#x9;&#x9;if (!postponed) {
&#x9;&#x9;&#x9;kill_rules(owner);
&#x9;&#x9;&#x9;list_move(&#x26;owner-&#x3E;list, &#x26;prune_list);
&#x9;&#x9;&#x9;need_prune = 1;
&#x9;&#x9;} else {
&#x9;&#x9;&#x9;list_move(&#x26;owner-&#x3E;list, postponed);
&#x9;&#x9;}
&#x9;&#x9;spin_lock(&#x26;hash_lock);
&#x9;}
&#x9;list_del_rcu(&#x26;chunk-&#x3E;hash);
&#x9;for (n = 0; n &#x3C; chunk-&#x3E;count; n++)
&#x9;&#x9;list_del_init(&#x26;chunk-&#x3E;owners[n].list);
&#x9;spin_unlock(&#x26;hash_lock);
&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;if (need_prune)
&#x9;&#x9;audit_schedule_prune();
}

static int audit_tree_handle_event(struct fsnotify_groupgroup,
&#x9;&#x9;&#x9;&#x9;   struct inodeto_tell,
&#x9;&#x9;&#x9;&#x9;   struct fsnotify_markinode_mark,
&#x9;&#x9;&#x9;&#x9;   struct fsnotify_markvfsmount_mark,
&#x9;&#x9;&#x9;&#x9;   u32 mask, voiddata, int data_type,
&#x9;&#x9;&#x9;&#x9;   const unsigned charfile_name, u32 cookie)
{
&#x9;return 0;
}

static void audit_tree_freeing_mark(struct fsnotify_markentry, struct fsnotify_groupgroup)
{
&#x9;struct audit_chunkchunk = container_of(entry, struct audit_chunk, mark);

&#x9;evict_chunk(chunk);

&#x9;*/
&#x9; We are guaranteed to have at least one reference to the mark from
&#x9; either the inode or the caller of fsnotify_destroy_mark().
&#x9; /*
&#x9;BUG_ON(atomic_read(&#x26;entry-&#x3E;refcnt) &#x3C; 1);
}

static const struct fsnotify_ops audit_tree_ops = {
&#x9;.handle_event = audit_tree_handle_event,
&#x9;.freeing_mark = audit_tree_freeing_mark,
};

static int __init audit_tree_init(void)
{
&#x9;int i;

&#x9;audit_tree_group = fsnotify_alloc_group(&#x26;audit_tree_ops);
&#x9;if (IS_ERR(audit_tree_group))
&#x9;&#x9;audit_panic(&#x22;cannot initialize fsnotify group for rectree watches&#x22;);

&#x9;for (i = 0; i &#x3C; HASH_SIZE; i++)
&#x9;&#x9;INIT_LIST_HEAD(&#x26;chunk_hash_heads[i]);

&#x9;return 0;
}
__initcall(audit_tree_init);
*/
 audit_watch.c -- watching inodes

 Copyright 2003-2009 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include &#x3C;linux/kernel.h&#x3E;
#include &#x3C;linux/audit.h&#x3E;
#include &#x3C;linux/kthread.h&#x3E;
#include &#x3C;linux/mutex.h&#x3E;
#include &#x3C;linux/fs.h&#x3E;
#include &#x3C;linux/fsnotify_backend.h&#x3E;
#include &#x3C;linux/namei.h&#x3E;
#include &#x3C;linux/netlink.h&#x3E;
#include &#x3C;linux/sched.h&#x3E;
#include &#x3C;linux/slab.h&#x3E;
#include &#x3C;linux/security.h&#x3E;
#include &#x22;audit.h&#x22;

*/
 Reference counting:

 audit_parent: lifetime is from audit_init_parent() to receipt of an FS_IGNORED
 &#x9;event.  Each audit_watch holds a reference to its associated parent.

 audit_watch: if added to lists, lifetime is from audit_init_watch() to
 &#x9;audit_remove_watch().  Additionally, an audit_watch may exist
 &#x9;temporarily to assist in searching existing filter data.  Each
 &#x9;audit_krule holds a reference to its associated watch.
 /*

struct audit_watch {
&#x9;atomic_t&#x9;&#x9;count;&#x9;*/ reference count /*
&#x9;dev_t&#x9;&#x9;&#x9;dev;&#x9;*/ associated superblock device /*
&#x9;char&#x9;&#x9;&#x9;*path;&#x9;*/ insertion path /*
&#x9;unsigned long&#x9;&#x9;ino;&#x9;*/ associated inode number /*
&#x9;struct audit_parent&#x9;*parent;/ associated parent /*
&#x9;struct list_head&#x9;wlist;&#x9;*/ entry in parent-&#x3E;watches list /*
&#x9;struct list_head&#x9;rules;&#x9;*/ anchor for krule-&#x3E;rlist /*
};

struct audit_parent {
&#x9;struct list_head&#x9;watches;/ anchor for audit_watch-&#x3E;wlist /*
&#x9;struct fsnotify_mark mark;/ fsnotify mark on the inode /*
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_watch_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_WATCH (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
&#x9;&#x9;&#x9;FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_free_parent(struct audit_parentparent)
{
&#x9;WARN_ON(!list_empty(&#x26;parent-&#x3E;watches));
&#x9;kfree(parent);
}

static void audit_watch_free_mark(struct fsnotify_markentry)
{
&#x9;struct audit_parentparent;

&#x9;parent = container_of(entry, struct audit_parent, mark);
&#x9;audit_free_parent(parent);
}

static void audit_get_parent(struct audit_parentparent)
{
&#x9;if (likely(parent))
&#x9;&#x9;fsnotify_get_mark(&#x26;parent-&#x3E;mark);
}

static void audit_put_parent(struct audit_parentparent)
{
&#x9;if (likely(parent))
&#x9;&#x9;fsnotify_put_mark(&#x26;parent-&#x3E;mark);
}

*/
 Find and return the audit_parent on the given inode.  If found a reference
 is taken on this parent.
 /*
static inline struct audit_parentaudit_find_parent(struct inodeinode)
{
&#x9;struct audit_parentparent = NULL;
&#x9;struct fsnotify_markentry;

&#x9;entry = fsnotify_find_inode_mark(audit_watch_group, inode);
&#x9;if (entry)
&#x9;&#x9;parent = container_of(entry, struct audit_parent, mark);

&#x9;return parent;
}

void audit_get_watch(struct audit_watchwatch)
{
&#x9;atomic_inc(&#x26;watch-&#x3E;count);
}

void audit_put_watch(struct audit_watchwatch)
{
&#x9;if (atomic_dec_and_test(&#x26;watch-&#x3E;count)) {
&#x9;&#x9;WARN_ON(watch-&#x3E;parent);
&#x9;&#x9;WARN_ON(!list_empty(&#x26;watch-&#x3E;rules));
&#x9;&#x9;kfree(watch-&#x3E;path);
&#x9;&#x9;kfree(watch);
&#x9;}
}

static void audit_remove_watch(struct audit_watchwatch)
{
&#x9;list_del(&#x26;watch-&#x3E;wlist);
&#x9;audit_put_parent(watch-&#x3E;parent);
&#x9;watch-&#x3E;parent = NULL;
&#x9;audit_put_watch(watch);/ match initial get /*
}

charaudit_watch_path(struct audit_watchwatch)
{
&#x9;return watch-&#x3E;path;
}

int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev)
{
&#x9;return (watch-&#x3E;ino != AUDIT_INO_UNSET) &#x26;&#x26;
&#x9;&#x9;(watch-&#x3E;ino == ino) &#x26;&#x26;
&#x9;&#x9;(watch-&#x3E;dev == dev);
}

*/ Initialize a parent watch entry. /*
static struct audit_parentaudit_init_parent(struct pathpath)
{
&#x9;struct inodeinode = d_backing_inode(path-&#x3E;dentry);
&#x9;struct audit_parentparent;
&#x9;int ret;

&#x9;parent = kzalloc(sizeof(*parent), GFP_KERNEL);
&#x9;if (unlikely(!parent))
&#x9;&#x9;return ERR_PTR(-ENOMEM);

&#x9;INIT_LIST_HEAD(&#x26;parent-&#x3E;watches);

&#x9;fsnotify_init_mark(&#x26;parent-&#x3E;mark, audit_watch_free_mark);
&#x9;parent-&#x3E;mark.mask = AUDIT_FS_WATCH;
&#x9;ret = fsnotify_add_mark(&#x26;parent-&#x3E;mark, audit_watch_group, inode, NULL, 0);
&#x9;if (ret &#x3C; 0) {
&#x9;&#x9;audit_free_parent(parent);
&#x9;&#x9;return ERR_PTR(ret);
&#x9;}

&#x9;return parent;
}

*/ Initialize a watch entry. /*
static struct audit_watchaudit_init_watch(charpath)
{
&#x9;struct audit_watchwatch;

&#x9;watch = kzalloc(sizeof(*watch), GFP_KERNEL);
&#x9;if (unlikely(!watch))
&#x9;&#x9;return ERR_PTR(-ENOMEM);

&#x9;INIT_LIST_HEAD(&#x26;watch-&#x3E;rules);
&#x9;atomic_set(&#x26;watch-&#x3E;count, 1);
&#x9;watch-&#x3E;path = path;
&#x9;watch-&#x3E;dev = AUDIT_DEV_UNSET;
&#x9;watch-&#x3E;ino = AUDIT_INO_UNSET;

&#x9;return watch;
}

*/ Translate a watch string to kernel representation. /*
int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op)
{
&#x9;struct audit_watchwatch;

&#x9;if (!audit_watch_group)
&#x9;&#x9;return -EOPNOTSUPP;

&#x9;if (path[0] != &#x27;/&#x27; || path[len-1] == &#x27;/&#x27; ||
&#x9;    krule-&#x3E;listnr != AUDIT_FILTER_EXIT ||
&#x9;    op != Audit_equal ||
&#x9;    krule-&#x3E;inode_f || krule-&#x3E;watch || krule-&#x3E;tree)
&#x9;&#x9;return -EINVAL;

&#x9;watch = audit_init_watch(path);
&#x9;if (IS_ERR(watch))
&#x9;&#x9;return PTR_ERR(watch);

&#x9;krule-&#x3E;watch = watch;

&#x9;return 0;
}

*/ Duplicate the given audit watch.  The new watch&#x27;s rules list is initialized
 to an empty list and wlist is undefined. /*
static struct audit_watchaudit_dupe_watch(struct audit_watchold)
{
&#x9;charpath;
&#x9;struct audit_watchnew;

&#x9;path = kstrdup(old-&#x3E;path, GFP_KERNEL);
&#x9;if (unlikely(!path))
&#x9;&#x9;return ERR_PTR(-ENOMEM);

&#x9;new = audit_init_watch(path);
&#x9;if (IS_ERR(new)) {
&#x9;&#x9;kfree(path);
&#x9;&#x9;goto out;
&#x9;}

&#x9;new-&#x3E;dev = old-&#x3E;dev;
&#x9;new-&#x3E;ino = old-&#x3E;ino;
&#x9;audit_get_parent(old-&#x3E;parent);
&#x9;new-&#x3E;parent = old-&#x3E;parent;

out:
&#x9;return new;
}

static void audit_watch_log_rule_change(struct audit_kruler, struct audit_watchw, charop)
{
&#x9;if (audit_enabled) {
&#x9;&#x9;struct audit_bufferab;
&#x9;&#x9;ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
&#x9;&#x9;if (unlikely(!ab))
&#x9;&#x9;&#x9;return;
&#x9;&#x9;audit_log_format(ab, &#x22;auid=%u ses=%u op=&#x22;,
&#x9;&#x9;&#x9;&#x9; from_kuid(&#x26;init_user_ns, audit_get_loginuid(current)),
&#x9;&#x9;&#x9;&#x9; audit_get_sessionid(current));
&#x9;&#x9;audit_log_string(ab, op);
&#x9;&#x9;audit_log_format(ab, &#x22; path=&#x22;);
&#x9;&#x9;audit_log_untrustedstring(ab, w-&#x3E;path);
&#x9;&#x9;audit_log_key(ab, r-&#x3E;filterkey);
&#x9;&#x9;audit_log_format(ab, &#x22; list=%d res=1&#x22;, r-&#x3E;listnr);
&#x9;&#x9;audit_log_end(ab);
&#x9;}
}

*/ Update inode info in audit rules based on filesystem event. /*
static void audit_update_watch(struct audit_parentparent,
&#x9;&#x9;&#x9;       const chardname, dev_t dev,
&#x9;&#x9;&#x9;       unsigned long ino, unsigned invalidating)
{
&#x9;struct audit_watchowatch,nwatch,nextw;
&#x9;struct audit_kruler,nextr;
&#x9;struct audit_entryoentry,nentry;

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;*/ Run all of the watches on this parent looking for the one that
&#x9; matches the given dname /*
&#x9;list_for_each_entry_safe(owatch, nextw, &#x26;parent-&#x3E;watches, wlist) {
&#x9;&#x9;if (audit_compare_dname_path(dname, owatch-&#x3E;path,
&#x9;&#x9;&#x9;&#x9;&#x9;     AUDIT_NAME_FULL))
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;*/ If the update involves invalidating rules, do the inode-based
&#x9;&#x9; filtering now, so we don&#x27;t omit records. /*
&#x9;&#x9;if (invalidating &#x26;&#x26; !audit_dummy_context())
&#x9;&#x9;&#x9;audit_filter_inodes(current, current-&#x3E;audit_context);

&#x9;&#x9;*/ updating ino will likely change which audit_hash_list we
&#x9;&#x9; are on so we need a new watch for the new list /*
&#x9;&#x9;nwatch = audit_dupe_watch(owatch);
&#x9;&#x9;if (IS_ERR(nwatch)) {
&#x9;&#x9;&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;&#x9;&#x9;audit_panic(&#x22;error updating watch, skipping&#x22;);
&#x9;&#x9;&#x9;return;
&#x9;&#x9;}
&#x9;&#x9;nwatch-&#x3E;dev = dev;
&#x9;&#x9;nwatch-&#x3E;ino = ino;

&#x9;&#x9;list_for_each_entry_safe(r, nextr, &#x26;owatch-&#x3E;rules, rlist) {

&#x9;&#x9;&#x9;oentry = container_of(r, struct audit_entry, rule);
&#x9;&#x9;&#x9;list_del(&#x26;oentry-&#x3E;rule.rlist);
&#x9;&#x9;&#x9;list_del_rcu(&#x26;oentry-&#x3E;list);

&#x9;&#x9;&#x9;nentry = audit_dupe_rule(&#x26;oentry-&#x3E;rule);
&#x9;&#x9;&#x9;if (IS_ERR(nentry)) {
&#x9;&#x9;&#x9;&#x9;list_del(&#x26;oentry-&#x3E;rule.list);
&#x9;&#x9;&#x9;&#x9;audit_panic(&#x22;error updating watch, removing&#x22;);
&#x9;&#x9;&#x9;} else {
&#x9;&#x9;&#x9;&#x9;int h = audit_hash_ino((u32)ino);

&#x9;&#x9;&#x9;&#x9;*/
&#x9;&#x9;&#x9;&#x9; nentry-&#x3E;rule.watch == oentry-&#x3E;rule.watch so
&#x9;&#x9;&#x9;&#x9; we must drop that reference and set it to our
&#x9;&#x9;&#x9;&#x9; new watch.
&#x9;&#x9;&#x9;&#x9; /*
&#x9;&#x9;&#x9;&#x9;audit_put_watch(nentry-&#x3E;rule.watch);
&#x9;&#x9;&#x9;&#x9;audit_get_watch(nwatch);
&#x9;&#x9;&#x9;&#x9;nentry-&#x3E;rule.watch = nwatch;
&#x9;&#x9;&#x9;&#x9;list_add(&#x26;nentry-&#x3E;rule.rlist, &#x26;nwatch-&#x3E;rules);
&#x9;&#x9;&#x9;&#x9;list_add_rcu(&#x26;nentry-&#x3E;list, &#x26;audit_inode_hash[h]);
&#x9;&#x9;&#x9;&#x9;list_replace(&#x26;oentry-&#x3E;rule.list,
&#x9;&#x9;&#x9;&#x9;&#x9;     &#x26;nentry-&#x3E;rule.list);
&#x9;&#x9;&#x9;}
&#x9;&#x9;&#x9;if (oentry-&#x3E;rule.exe)
&#x9;&#x9;&#x9;&#x9;audit_remove_mark(oentry-&#x3E;rule.exe);

&#x9;&#x9;&#x9;audit_watch_log_rule_change(r, owatch, &#x22;updated_rules&#x22;);

&#x9;&#x9;&#x9;call_rcu(&#x26;oentry-&#x3E;rcu, audit_free_rule_rcu);
&#x9;&#x9;}

&#x9;&#x9;audit_remove_watch(owatch);
&#x9;&#x9;goto add_watch_to_parent;/ event applies to a single watch /*
&#x9;}
&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;return;

add_watch_to_parent:
&#x9;list_add(&#x26;nwatch-&#x3E;wlist, &#x26;parent-&#x3E;watches);
&#x9;mutex_unlock(&#x26;audit_filter_mutex);
&#x9;return;
}

*/ Remove all watches &#x26; rules associated with a parent that is going away. /*
static void audit_remove_parent_watches(struct audit_parentparent)
{
&#x9;struct audit_watchw,nextw;
&#x9;struct audit_kruler,nextr;
&#x9;struct audit_entrye;

&#x9;mutex_lock(&#x26;audit_filter_mutex);
&#x9;list_for_each_entry_safe(w, nextw, &#x26;parent-&#x3E;watches, wlist) {
&#x9;&#x9;list_for_each_entry_safe(r, nextr, &#x26;w-&#x3E;rules, rlist) {
&#x9;&#x9;&#x9;e = container_of(r, struct audit_entry, rule);
&#x9;&#x9;&#x9;audit_watch_log_rule_change(r, w, &#x22;remove_rule&#x22;);
&#x9;&#x9;&#x9;if (e-&#x3E;rule.exe)
&#x9;&#x9;&#x9;&#x9;audit_remove_mark(e-&#x3E;rule.exe);
&#x9;&#x9;&#x9;list_del(&#x26;r-&#x3E;rlist);
&#x9;&#x9;&#x9;list_del(&#x26;r-&#x3E;list);
&#x9;&#x9;&#x9;list_del_rcu(&#x26;e-&#x3E;list);
&#x9;&#x9;&#x9;call_rcu(&#x26;e-&#x3E;rcu, audit_free_rule_rcu);
&#x9;&#x9;}
&#x9;&#x9;audit_remove_watch(w);
&#x9;}
&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;fsnotify_destroy_mark(&#x26;parent-&#x3E;mark, audit_watch_group);
}

*/ Get path information necessary for adding watches. /*
static int audit_get_nd(struct audit_watchwatch, struct pathparent)
{
&#x9;struct dentryd = kern_path_locked(watch-&#x3E;path, parent);
&#x9;if (IS_ERR(d))
&#x9;&#x9;return PTR_ERR(d);
&#x9;inode_unlock(d_backing_inode(parent-&#x3E;dentry));
&#x9;if (d_is_positive(d)) {
&#x9;&#x9;*/ update watch filter fields /*
&#x9;&#x9;watch-&#x3E;dev = d_backing_inode(d)-&#x3E;i_sb-&#x3E;s_dev;
&#x9;&#x9;watch-&#x3E;ino = d_backing_inode(d)-&#x3E;i_ino;
&#x9;}
&#x9;dput(d);
&#x9;return 0;
}

*/ Associate the given rule with an existing parent.
 Caller must hold audit_filter_mutex. /*
static void audit_add_to_parent(struct audit_krulekrule,
&#x9;&#x9;&#x9;&#x9;struct audit_parentparent)
{
&#x9;struct audit_watchw,watch = krule-&#x3E;watch;
&#x9;int watch_found = 0;

&#x9;BUG_ON(!mutex_is_locked(&#x26;audit_filter_mutex));

&#x9;list_for_each_entry(w, &#x26;parent-&#x3E;watches, wlist) {
&#x9;&#x9;if (strcmp(watch-&#x3E;path, w-&#x3E;path))
&#x9;&#x9;&#x9;continue;

&#x9;&#x9;watch_found = 1;

&#x9;&#x9;*/ put krule&#x27;s ref to temporary watch /*
&#x9;&#x9;audit_put_watch(watch);

&#x9;&#x9;audit_get_watch(w);
&#x9;&#x9;krule-&#x3E;watch = watch = w;

&#x9;&#x9;audit_put_parent(parent);
&#x9;&#x9;break;
&#x9;}

&#x9;if (!watch_found) {
&#x9;&#x9;watch-&#x3E;parent = parent;

&#x9;&#x9;audit_get_watch(watch);
&#x9;&#x9;list_add(&#x26;watch-&#x3E;wlist, &#x26;parent-&#x3E;watches);
&#x9;}
&#x9;list_add(&#x26;krule-&#x3E;rlist, &#x26;watch-&#x3E;rules);
}

*/ Find a matching watch entry, or add this one.
 Caller must hold audit_filter_mutex. /*
int audit_add_watch(struct audit_krulekrule, struct list_head*list)
{
&#x9;struct audit_watchwatch = krule-&#x3E;watch;
&#x9;struct audit_parentparent;
&#x9;struct path parent_path;
&#x9;int h, ret = 0;

&#x9;mutex_unlock(&#x26;audit_filter_mutex);

&#x9;*/ Avoid calling path_lookup under audit_filter_mutex. /*
&#x9;ret = audit_get_nd(watch, &#x26;parent_path);

&#x9;*/ caller expects mutex locked /*
&#x9;mutex_lock(&#x26;audit_filter_mutex);

&#x9;if (ret)
&#x9;&#x9;return ret;

&#x9;*/ either find an old parent or attach a new one /*
&#x9;parent = audit_find_parent(d_backing_inode(parent_path.dentry));
&#x9;if (!parent) {
&#x9;&#x9;parent = audit_init_parent(&#x26;parent_path);
&#x9;&#x9;if (IS_ERR(parent)) {
&#x9;&#x9;&#x9;ret = PTR_ERR(parent);
&#x9;&#x9;&#x9;goto error;
&#x9;&#x9;}
&#x9;}

&#x9;audit_add_to_parent(krule, parent);

&#x9;h = audit_hash_ino((u32)watch-&#x3E;ino);
&#x9;*list = &#x26;audit_inode_hash[h];
error:
&#x9;path_put(&#x26;parent_path);
&#x9;return ret;
}

void audit_remove_watch_rule(struct audit_krulekrule)
{
&#x9;struct audit_watchwatch = krule-&#x3E;watch;
&#x9;struct audit_parentparent = watch-&#x3E;parent;

&#x9;list_del(&#x26;krule-&#x3E;rlist);

&#x9;if (list_empty(&#x26;watch-&#x3E;rules)) {
&#x9;&#x9;audit_remove_watch(watch);

&#x9;&#x9;if (list_empty(&#x26;parent-&#x3E;watches)) {
&#x9;&#x9;&#x9;audit_get_parent(parent);
&#x9;&#x9;&#x9;fsnotify_destroy_mark(&#x26;parent-&#x3E;mark, audit_watch_group);
&#x9;&#x9;&#x9;audit_put_parent(parent);
&#x9;&#x9;}
&#x9;}
}

*/ Update watch data in audit rules based on fsnotify events. /*
static int audit_watch_handle_event(struct fsnotify_groupgroup,
&#x9;&#x9;&#x9;&#x9;    struct inodeto_tell,
&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markinode_mark,
&#x9;&#x9;&#x9;&#x9;    struct fsnotify_markvfsmount_mark,
&#x9;&#x9;&#x9;&#x9;    u32 mask, voiddata, int data_type,
&#x9;&#x9;&#x9;&#x9;    const unsigned chardname, u32 cookie)
{
&#x9;struct inodeinode;
&#x9;struct audit_parentparent;

&#x9;parent = container_of(inode_mark, struct audit_parent, mark);

&#x9;BUG_ON(group != audit_watch_group);

&#x9;switch (data_type) {
&#x9;case (FSNOTIFY_EVENT_PATH):
&#x9;&#x9;inode = d_backing_inode(((struct path)data)-&#x3E;dentry);
&#x9;&#x9;break;
&#x9;case (FSNOTIFY_EVENT_INODE):
&#x9;&#x9;inode = (struct inode)data;
&#x9;&#x9;break;
&#x9;default:
&#x9;&#x9;BUG();
&#x9;&#x9;inode = NULL;
&#x9;&#x9;break;
&#x9;};

&#x9;if (mask &#x26; (FS_CREATE|FS_MOVED_TO) &#x26;&#x26; inode)
&#x9;&#x9;audit_update_watch(parent, dname, inode-&#x3E;i_sb-&#x3E;s_dev, inode-&#x3E;i_ino, 0);
&#x9;else if (mask &#x26; (FS_DELETE|FS_MOVED_FROM))
&#x9;&#x9;audit_update_watch(parent, dname, AUDIT_DEV_UNSET, AUDIT_INO_UNSET, 1);
&#x9;else if (mask &#x26; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
&#x9;&#x9;audit_remove_parent_watches(parent);

&#x9;return 0;
}

static const struct fsnotify_ops audit_watch_fsnotify_ops = {
&#x9;.handle_event = &#x9;audit_watch_handle_event,
};

static int __init audit_watch_init(void)
{
&#x9;audit_watch_group = fsnotify_alloc_group(&#x26;audit_watch_fsnotify_ops);
&#x9;if (IS_ERR(audit_watch_group)) {
&#x9;&#x9;audit_watch_group = NULL;
&#x9;&#x9;audit_panic(&#x22;cannot create audit fsnotify group&#x22;);
&#x9;}
&#x9;return 0;
}
device_initcall(audit_watch_init);

int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold)
{
&#x9;struct audit_fsnotify_markaudit_mark;
&#x9;charpathname;

&#x9;pathname = kstrdup(audit_mark_path(old-&#x3E;exe), GFP_KERNEL);
&#x9;if (!pathname)
&#x9;&#x9;return -ENOMEM;

&#x9;audit_mark = audit_alloc_mark(new, pathname, strlen(pathname));
&#x9;if (IS_ERR(audit_mark)) {
&#x9;&#x9;kfree(pathname);
&#x9;&#x9;return PTR_ERR(audit_mark);
&#x9;}
&#x9;new-&#x3E;exe = audit_mark;

&#x9;return 0;
}

int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark)
{
&#x9;struct fileexe_file;
&#x9;unsigned long ino;
&#x9;dev_t dev;

&#x9;rcu_read_lock();
&#x9;exe_file = rcu_dereference(tsk-&#x3E;mm-&#x3E;exe_file);
&#x9;ino = exe_file-&#x3E;f_inode-&#x3E;i_ino;
&#x9;dev = exe_file-&#x3E;f_inode-&#x3E;i_sb-&#x3E;s_dev;
&#x9;rcu_read_unlock();
&#x9;return audit_mark_compare(mark, ino, dev);
}
*/

 Simple stack backtrace regression test module

 (C) Copyright 2008 Intel Corporation
 Author: Arjan van de Ven &#x3C;arjan@linux.intel.com&#x3E;

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*

#include &#x3C;linux/completion.h&#x3E;
#include &#x3C;linux/delay.h&#x3E;
#include &#x3C;linux/interrupt.h&#x3E;
#include &#x3C;linux/module.h&#x3E;
#include &#x3C;linux/sched.h&#x3E;
#include &#x3C;linux/stacktrace.h&#x3E;

static void backtrace_test_normal(void)
{
&#x9;pr_info(&#x22;Testing a backtrace from process context.\n&#x22;);
&#x9;pr_info(&#x22;The following trace is a kernel self test and not a bug!\n&#x22;);

&#x9;dump_stack();
}

static DECLARE_COMPLETION(backtrace_work);

static void backtrace_test_irq_callback(unsigned long data)
{
&#x9;dump_stack();
&#x9;complete(&#x26;backtrace_work);
}

static DECLARE_TASKLET(backtrace_tasklet, &#x26;backtrace_test_irq_callback, 0);

static void backtrace_test_irq(void)
{
&#x9;pr_info(&#x22;Testing a backtrace from irq context.\n&#x22;);
&#x9;pr_info(&#x22;The following trace is a kernel self test and not a bug!\n&#x22;);

&#x9;init_completion(&#x26;backtrace_work);
&#x9;tasklet_schedule(&#x26;backtrace_tasklet);
&#x9;wait_for_completion(&#x26;backtrace_work);
}

#ifdef CONFIG_STACKTRACE
static void backtrace_test_saved(void)
{
&#x9;struct stack_trace trace;
&#x9;unsigned long entries[8];

&#x9;pr_info(&#x22;Testing a saved backtrace.\n&#x22;);
&#x9;pr_info(&#x22;The following trace is a kernel self test and not a bug!\n&#x22;);

&#x9;trace.nr_entries = 0;
&#x9;trace.max_entries = ARRAY_SIZE(entries);
&#x9;trace.entries = entries;
&#x9;trace.skip = 0;

&#x9;save_stack_trace(&#x26;trace);
&#x9;print_stack_trace(&#x26;trace, 0);
}
#else
static void backtrace_test_saved(void)
{
&#x9;pr_info(&#x22;Saved backtrace test skipped.\n&#x22;);
}
#endif

static int backtrace_regression_test(void)
{
&#x9;pr_info(&#x22;====[ backtrace testing ]===========\n&#x22;);

&#x9;backtrace_test_normal();
&#x9;backtrace_test_irq();
&#x9;backtrace_test_saved();

&#x9;pr_info(&#x22;====[ end of backtrace testing ]====\n&#x22;);
&#x9;return 0;
}

static void exitf(void)
{
}

module_init(backtrace_regression_test);
module_exit(exitf);
MODULE_LICENSE(&#x22;GPL&#x22;);
MODULE_AUTHOR(&#x22;Arjan van de Ven &#x3C;arjan@linux.intel.com&#x3E;&#x22;);
*/
