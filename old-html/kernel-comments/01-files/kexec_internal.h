/*
#ifndef LINUX_KEXEC_INTERNAL_H
#define LINUX_KEXEC_INTERNAL_H

#include <linux/kexec.h>

struct kimagedo_kimage_alloc_init(void);
int sanity_check_segment_list(struct kimageimage);
void kimage_free_page_list(struct list_headlist);
void kimage_free(struct kimageimage);
int kimage_load_segment(struct kimageimage, struct kexec_segmentsegment);
void kimage_terminate(struct kimageimage);
int kimage_is_destination_range(struct kimageimage,
				unsigned long start, unsigned long end);

extern struct mutex kexec_mutex;

#ifdef CONFIG_KEXEC_FILE
struct kexec_sha_region {
	unsigned long start;
	unsigned long len;
};

*/
 Keeps track of buffer parameters as provided by caller for requesting
 memory placement of buffer.
 /*
struct kexec_buf {
	struct kimageimage;
	charbuffer;
	unsigned long bufsz;
	unsigned long mem;
	unsigned long memsz;
	unsigned long buf_align;
	unsigned long buf_min;
	unsigned long buf_max;
	bool top_down;		*/ allocate from top of memory hole /*
};

void kimage_file_post_load_cleanup(struct kimageimage);
#else*/ CONFIG_KEXEC_FILE /*
static inline void kimage_file_post_load_cleanup(struct kimageimage) { }
#endif*/ CONFIG_KEXEC_FILE /*
#endif*/ LINUX_KEXEC_INTERNAL_H
