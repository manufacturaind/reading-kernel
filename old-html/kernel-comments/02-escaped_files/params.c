   Helpers for initial module or kernel cmdline parsing
   Copyright (C) 2001 Rusty Russell.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/*
#include &lt;linux/kernel.h&gt;
#include &lt;linux/string.h&gt;
#include &lt;linux/errno.h&gt;
#include &lt;linux/module.h&gt;
#include &lt;linux/moduleparam.h&gt;
#include &lt;linux/device.h&gt;
#include &lt;linux/err.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/ctype.h&gt;

#ifdef CONFIG_SYSFS
*/ Protects all built-in parameters, modules use their own param_lock /*
static DEFINE_MUTEX(param_lock);

*/ Use the module&#39;s mutex, or if built-in use the built-in mutex /*
#ifdef CONFIG_MODULES
#define KPARAM_MUTEX(mod)	((mod) ? &amp;(mod)-&gt;param_lock : &amp;param_lock)
#else
#define KPARAM_MUTEX(mod)	(&amp;param_lock)
#endif

static inline void check_kparam_locked(struct modulemod)
{
	BUG_ON(!mutex_is_locked(KPARAM_MUTEX(mod)));
}
#else
static inline void check_kparam_locked(struct modulemod)
{
}
#endif */ !CONFIG_SYSFS /*

*/ This just allows us to keep track of which parameters are kmalloced. /*
struct kmalloced_param {
	struct list_head list;
	char val[];
};
static LIST_HEAD(kmalloced_params);
static DEFINE_SPINLOCK(kmalloced_params_lock);

static voidkmalloc_parameter(unsigned int size)
{
	struct kmalloced_paramp;

	p = kmalloc(sizeof(*p) + size, GFP_KERNEL);
	if (!p)
		return NULL;

	spin_lock(&amp;kmalloced_params_lock);
	list_add(&amp;p-&gt;list, &amp;kmalloced_params);
	spin_unlock(&amp;kmalloced_params_lock);

	return p-&gt;val;
}

*/ Does nothing if parameter wasn&#39;t kmalloced above. /*
static void maybe_kfree_parameter(voidparam)
{
	struct kmalloced_paramp;

	spin_lock(&amp;kmalloced_params_lock);
	list_for_each_entry(p, &amp;kmalloced_params, list) {
		if (p-&gt;val == param) {
			list_del(&amp;p-&gt;list);
			kfree(p);
			break;
		}
	}
	spin_unlock(&amp;kmalloced_params_lock);
}

static char dash2underscore(char c)
{
	if (c == &#39;-&#39;)
		return &#39;_&#39;;
	return c;
}

bool parameqn(const chara, const charb, size_t n)
{
	size_t i;

	for (i = 0; i &lt; n; i++) {
		if (dash2underscore(a[i]) != dash2underscore(b[i]))
			return false;
	}
	return true;
}

bool parameq(const chara, const charb)
{
	return parameqn(a, b, strlen(a)+1);
}

static void param_check_unsafe(const struct kernel_paramkp)
{
	if (kp-&gt;flags &amp; KERNEL_PARAM_FL_UNSAFE) {
		pr_warn(&quot;Setting dangerous option %s - tainting kernel\n&quot;,
			kp-&gt;name);
		add_taint(TAINT_USER, LOCKDEP_STILL_OK);
	}
}

static int parse_one(charparam,
		     charval,
		     const chardoing,
		     const struct kernel_paramparams,
		     unsigned num_params,
		     s16 min_level,
		     s16 max_level,
		     voidarg,
		     int (*handle_unknown)(charparam, charval,
				     const chardoing, voidarg))
{
	unsigned int i;
	int err;

	*/ Find parameter /*
	for (i = 0; i &lt; num_params; i++) {
		if (parameq(param, params[i].name)) {
			if (params[i].level &lt; min_level
			    || params[i].level &gt; max_level)
				return 0;
			*/ No one handled NULL, so do it here. /*
			if (!val &amp;&amp;
			    !(params[i].ops-&gt;flags &amp; KERNEL_PARAM_OPS_FL_NOARG))
				return -EINVAL;
			pr_debug(&quot;handling %s with %p\n&quot;, param,
				params[i].ops-&gt;set);
			kernel_param_lock(params[i].mod);
			param_check_unsafe(&amp;params[i]);
			err = params[i].ops-&gt;set(val, &amp;params[i]);
			kernel_param_unlock(params[i].mod);
			return err;
		}
	}

	if (handle_unknown) {
		pr_debug(&quot;doing %s: %s=&#39;%s&#39;\n&quot;, doing, param, val);
		return handle_unknown(param, val, doing, arg);
	}

	pr_debug(&quot;Unknown argument &#39;%s&#39;\n&quot;, param);
	return -ENOENT;
}

*/ You can use &quot; around spaces, but can&#39;t escape &quot;. /*
*/ Hyphens and underscores equivalent in parameter names. /*
static charnext_arg(charargs, char*param, char*val)
{
	unsigned int i, equals = 0;
	int in_quote = 0, quoted = 0;
	charnext;

	if (*args == &#39;&quot;&#39;) {
		args++;
		in_quote = 1;
		quoted = 1;
	}

	for (i = 0; args[i]; i++) {
		if (isspace(args[i]) &amp;&amp; !in_quote)
			break;
		if (equals == 0) {
			if (args[i] == &#39;=&#39;)
				equals = i;
		}
		if (args[i] == &#39;&quot;&#39;)
			in_quote = !in_quote;
	}

	*param = args;
	if (!equals)
		*val = NULL;
	else {
		args[equals] = &#39;\0&#39;;
		*val = args + equals + 1;

		*/ Don&#39;t include quotes in value. /*
		if (**val == &#39;&quot;&#39;) {
			(*val)++;
			if (args[i-1] == &#39;&quot;&#39;)
				args[i-1] = &#39;\0&#39;;
		}
	}
	if (quoted &amp;&amp; args[i-1] == &#39;&quot;&#39;)
		args[i-1] = &#39;\0&#39;;

	if (args[i]) {
		args[i] = &#39;\0&#39;;
		next = args + i + 1;
	} else
		next = args + i;

	*/ Chew up trailing spaces. /*
	return skip_spaces(next);
}

*/ Args looks like &quot;foo=bar,bar2 baz=fuz wiz&quot;. /*
charparse_args(const chardoing,
		 charargs,
		 const struct kernel_paramparams,
		 unsigned num,
		 s16 min_level,
		 s16 max_level,
		 voidarg,
		 int (*unknown)(charparam, charval,
				const chardoing, voidarg))
{
	charparam,val,err = NULL;

	*/ Chew leading spaces /*
	args = skip_spaces(args);

	if (*args)
		pr_debug(&quot;doing %s, parsing ARGS: &#39;%s&#39;\n&quot;, doing, args);

	while (*args) {
		int ret;
		int irq_was_disabled;

		args = next_arg(args, &amp;param, &amp;val);
		*/ Stop at -- /*
		if (!val &amp;&amp; strcmp(param, &quot;--&quot;) == 0)
			return err ?: args;
		irq_was_disabled = irqs_disabled();
		ret = parse_one(param, val, doing, params, num,
				min_level, max_level, arg, unknown);
		if (irq_was_disabled &amp;&amp; !irqs_disabled())
			pr_warn(&quot;%s: option &#39;%s&#39; enabled irq&#39;s!\n&quot;,
				doing, param);

		switch (ret) {
		case 0:
			continue;
		case -ENOENT:
			pr_err(&quot;%s: Unknown parameter `%s&#39;\n&quot;, doing, param);
			break;
		case -ENOSPC:
			pr_err(&quot;%s: `%s&#39; too large for parameter `%s&#39;\n&quot;,
			       doing, val ?: &quot;&quot;, param);
			break;
		default:
			pr_err(&quot;%s: `%s&#39; invalid for parameter `%s&#39;\n&quot;,
			       doing, val ?: &quot;&quot;, param);
			break;
		}

		err = ERR_PTR(ret);
	}

	return err;
}

*/ Lazy bastard, eh? /*
#define STANDARD_PARAM_DEF(name, type, format, strtolfn)      		\
	int param_set_##name(const charval, const struct kernel_paramkp) \
	{								\
		return strtolfn(val, 0, (type)kp-&gt;arg);		\
	}								\
	int param_get_##name(charbuffer, const struct kernel_paramkp) \
	{								\
		return scnprintf(buffer, PAGE_SIZE, format,		\
				*((type)kp-&gt;arg));			\
	}								\
	const struct kernel_param_ops param_ops_##name = {			\
		.set = param_set_##name,				\
		.get = param_get_##name,				\
	};								\
	EXPORT_SYMBOL(param_set_##name);				\
	EXPORT_SYMBOL(param_get_##name);				\
	EXPORT_SYMBOL(param_ops_##name)


STANDARD_PARAM_DEF(byte, unsigned char, &quot;%hhu&quot;, kstrtou8);
STANDARD_PARAM_DEF(short, short, &quot;%hi&quot;, kstrtos16);
STANDARD_PARAM_DEF(ushort, unsigned short, &quot;%hu&quot;, kstrtou16);
STANDARD_PARAM_DEF(int, int, &quot;%i&quot;, kstrtoint);
STANDARD_PARAM_DEF(uint, unsigned int, &quot;%u&quot;, kstrtouint);
STANDARD_PARAM_DEF(long, long, &quot;%li&quot;, kstrtol);
STANDARD_PARAM_DEF(ulong, unsigned long, &quot;%lu&quot;, kstrtoul);
STANDARD_PARAM_DEF(ullong, unsigned long long, &quot;%llu&quot;, kstrtoull);

int param_set_charp(const charval, const struct kernel_paramkp)
{
	if (strlen(val) &gt; 1024) {
		pr_err(&quot;%s: string parameter too long\n&quot;, kp-&gt;name);
		return -ENOSPC;
	}

	maybe_kfree_parameter(*(char*)kp-&gt;arg);

	*/ This is a hack.  We can&#39;t kmalloc in early boot, and we
	 don&#39;t need to; this mangled commandline is preserved. /*
	if (slab_is_available()) {
		*(char*)kp-&gt;arg = kmalloc_parameter(strlen(val)+1);
		if (!*(char*)kp-&gt;arg)
			return -ENOMEM;
		strcpy(*(char*)kp-&gt;arg, val);
	} else
		*(const char*)kp-&gt;arg = val;

	return 0;
}
EXPORT_SYMBOL(param_set_charp);

int param_get_charp(charbuffer, const struct kernel_paramkp)
{
	return scnprintf(buffer, PAGE_SIZE, &quot;%s&quot;,((char*)kp-&gt;arg));
}
EXPORT_SYMBOL(param_get_charp);

void param_free_charp(voidarg)
{
	maybe_kfree_parameter(*((char*)arg));
}
EXPORT_SYMBOL(param_free_charp);

const struct kernel_param_ops param_ops_charp = {
	.set = param_set_charp,
	.get = param_get_charp,
	.free = param_free_charp,
};
EXPORT_SYMBOL(param_ops_charp);

*/ Actually could be a bool or an int, for historical reasons. /*
int param_set_bool(const charval, const struct kernel_paramkp)
{
	*/ No equals means &quot;set&quot;... /*
	if (!val) val = &quot;1&quot;;

	*/ One of =[yYnN01] /*
	return strtobool(val, kp-&gt;arg);
}
EXPORT_SYMBOL(param_set_bool);

int param_get_bool(charbuffer, const struct kernel_paramkp)
{
	*/ Y and N chosen as being relatively non-coder friendly /*
	return sprintf(buffer, &quot;%c&quot;,(bool)kp-&gt;arg ? &#39;Y&#39; : &#39;N&#39;);
}
EXPORT_SYMBOL(param_get_bool);

const struct kernel_param_ops param_ops_bool = {
	.flags = KERNEL_PARAM_OPS_FL_NOARG,
	.set = param_set_bool,
	.get = param_get_bool,
};
EXPORT_SYMBOL(param_ops_bool);

int param_set_bool_enable_only(const charval, const struct kernel_paramkp)
{
	int err = 0;
	bool new_value;
	bool orig_value =(bool)kp-&gt;arg;
	struct kernel_param dummy_kp =kp;

	dummy_kp.arg = &amp;new_value;

	err = param_set_bool(val, &amp;dummy_kp);
	if (err)
		return err;

	*/ Don&#39;t let them unset it once it&#39;s set! /*
	if (!new_value &amp;&amp; orig_value)
		return -EROFS;

	if (new_value)
		err = param_set_bool(val, kp);

	return err;
}
EXPORT_SYMBOL_GPL(param_set_bool_enable_only);

const struct kernel_param_ops param_ops_bool_enable_only = {
	.flags = KERNEL_PARAM_OPS_FL_NOARG,
	.set = param_set_bool_enable_only,
	.get = param_get_bool,
};
EXPORT_SYMBOL_GPL(param_ops_bool_enable_only);

*/ This one must be bool. /*
int param_set_invbool(const charval, const struct kernel_paramkp)
{
	int ret;
	bool boolval;
	struct kernel_param dummy;

	dummy.arg = &amp;boolval;
	ret = param_set_bool(val, &amp;dummy);
	if (ret == 0)
		*(bool)kp-&gt;arg = !boolval;
	return ret;
}
EXPORT_SYMBOL(param_set_invbool);

int param_get_invbool(charbuffer, const struct kernel_paramkp)
{
	return sprintf(buffer, &quot;%c&quot;, (*(bool)kp-&gt;arg) ? &#39;N&#39; : &#39;Y&#39;);
}
EXPORT_SYMBOL(param_get_invbool);

const struct kernel_param_ops param_ops_invbool = {
	.set = param_set_invbool,
	.get = param_get_invbool,
};
EXPORT_SYMBOL(param_ops_invbool);

int param_set_bint(const charval, const struct kernel_paramkp)
{
	*/ Match bool exactly, by re-using it. /*
	struct kernel_param boolkp =kp;
	bool v;
	int ret;

	boolkp.arg = &amp;v;

	ret = param_set_bool(val, &amp;boolkp);
	if (ret == 0)
		*(int)kp-&gt;arg = v;
	return ret;
}
EXPORT_SYMBOL(param_set_bint);

const struct kernel_param_ops param_ops_bint = {
	.flags = KERNEL_PARAM_OPS_FL_NOARG,
	.set = param_set_bint,
	.get = param_get_int,
};
EXPORT_SYMBOL(param_ops_bint);

*/ We break the rule and mangle the string. /*
static int param_array(struct modulemod,
		       const charname,
		       const charval,
		       unsigned int min, unsigned int max,
		       voidelem, int elemsize,
		       int (*set)(const char, const struct kernel_paramkp),
		       s16 level,
		       unsigned intnum)
{
	int ret;
	struct kernel_param kp;
	char save;

	*/ Get the name right for errors. /*
	kp.name = name;
	kp.arg = elem;
	kp.level = level;

	*num = 0;
	*/ We expect a comma-separated list of values. /*
	do {
		int len;

		if (*num == max) {
			pr_err(&quot;%s: can only take %i arguments\n&quot;, name, max);
			return -EINVAL;
		}
		len = strcspn(val, &quot;,&quot;);

		*/ nul-terminate and parse /*
		save = val[len];
		((char)val)[len] = &#39;\0&#39;;
		check_kparam_locked(mod);
		ret = set(val, &amp;kp);

		if (ret != 0)
			return ret;
		kp.arg += elemsize;
		val += len+1;
		(*num)++;
	} while (save == &#39;,&#39;);

	if (*num &lt; min) {
		pr_err(&quot;%s: needs at least %i arguments\n&quot;, name, min);
		return -EINVAL;
	}
	return 0;
}

static int param_array_set(const charval, const struct kernel_paramkp)
{
	const struct kparam_arrayarr = kp-&gt;arr;
	unsigned int temp_num;

	return param_array(kp-&gt;mod, kp-&gt;name, val, 1, arr-&gt;max, arr-&gt;elem,
			   arr-&gt;elemsize, arr-&gt;ops-&gt;set, kp-&gt;level,
			   arr-&gt;num ?: &amp;temp_num);
}

static int param_array_get(charbuffer, const struct kernel_paramkp)
{
	int i, off, ret;
	const struct kparam_arrayarr = kp-&gt;arr;
	struct kernel_param p =kp;

	for (i = off = 0; i &lt; (arr-&gt;num ?arr-&gt;num : arr-&gt;max); i++) {
		if (i)
			buffer[off++] = &#39;,&#39;;
		p.arg = arr-&gt;elem + arr-&gt;elemsize i;
		check_kparam_locked(p.mod);
		ret = arr-&gt;ops-&gt;get(buffer + off, &amp;p);
		if (ret &lt; 0)
			return ret;
		off += ret;
	}
	buffer[off] = &#39;\0&#39;;
	return off;
}

static void param_array_free(voidarg)
{
	unsigned int i;
	const struct kparam_arrayarr = arg;

	if (arr-&gt;ops-&gt;free)
		for (i = 0; i &lt; (arr-&gt;num ?arr-&gt;num : arr-&gt;max); i++)
			arr-&gt;ops-&gt;free(arr-&gt;elem + arr-&gt;elemsize i);
}

const struct kernel_param_ops param_array_ops = {
	.set = param_array_set,
	.get = param_array_get,
	.free = param_array_free,
};
EXPORT_SYMBOL(param_array_ops);

int param_set_copystring(const charval, const struct kernel_paramkp)
{
	const struct kparam_stringkps = kp-&gt;str;

	if (strlen(val)+1 &gt; kps-&gt;maxlen) {
		pr_err(&quot;%s: string doesn&#39;t fit in %u chars.\n&quot;,
		       kp-&gt;name, kps-&gt;maxlen-1);
		return -ENOSPC;
	}
	strcpy(kps-&gt;string, val);
	return 0;
}
EXPORT_SYMBOL(param_set_copystring);

int param_get_string(charbuffer, const struct kernel_paramkp)
{
	const struct kparam_stringkps = kp-&gt;str;
	return strlcpy(buffer, kps-&gt;string, kps-&gt;maxlen);
}
EXPORT_SYMBOL(param_get_string);

const struct kernel_param_ops param_ops_string = {
	.set = param_set_copystring,
	.get = param_get_string,
};
EXPORT_SYMBOL(param_ops_string);

*/ sysfs output in /sys/modules/XYZ/parameters/ /*
#define to_module_attr(n) container_of(n, struct module_attribute, attr)
#define to_module_kobject(n) container_of(n, struct module_kobject, kobj)

struct param_attribute
{
	struct module_attribute mattr;
	const struct kernel_paramparam;
};

struct module_param_attrs
{
	unsigned int num;
	struct attribute_group grp;
	struct param_attribute attrs[0];
};

#ifdef CONFIG_SYSFS
#define to_param_attr(n) container_of(n, struct param_attribute, mattr)

static ssize_t param_attr_show(struct module_attributemattr,
			       struct module_kobjectmk, charbuf)
{
	int count;
	struct param_attributeattribute = to_param_attr(mattr);

	if (!attribute-&gt;param-&gt;ops-&gt;get)
		return -EPERM;

	kernel_param_lock(mk-&gt;mod);
	count = attribute-&gt;param-&gt;ops-&gt;get(buf, attribute-&gt;param);
	kernel_param_unlock(mk-&gt;mod);
	if (count &gt; 0) {
		strcat(buf, &quot;\n&quot;);
		++count;
	}
	return count;
}

*/ sysfs always hands a nul-terminated string in buf.  We rely on that. /*
static ssize_t param_attr_store(struct module_attributemattr,
				struct module_kobjectmk,
				const charbuf, size_t len)
{
 	int err;
	struct param_attributeattribute = to_param_attr(mattr);

	if (!attribute-&gt;param-&gt;ops-&gt;set)
		return -EPERM;

	kernel_param_lock(mk-&gt;mod);
	param_check_unsafe(attribute-&gt;param);
	err = attribute-&gt;param-&gt;ops-&gt;set(buf, attribute-&gt;param);
	kernel_param_unlock(mk-&gt;mod);
	if (!err)
		return len;
	return err;
}
#endif

#ifdef CONFIG_MODULES
#define __modinit
#else
#define __modinit __init
#endif

#ifdef CONFIG_SYSFS
void kernel_param_lock(struct modulemod)
{
	mutex_lock(KPARAM_MUTEX(mod));
}

void kernel_param_unlock(struct modulemod)
{
	mutex_unlock(KPARAM_MUTEX(mod));
}

EXPORT_SYMBOL(kernel_param_lock);
EXPORT_SYMBOL(kernel_param_unlock);

*/
 add_sysfs_param - add a parameter to sysfs
 @mk: struct module_kobject
 @kparam: the actual parameter definition to add to sysfs
 @name: name of parameter

 Create a kobject if for a (per-module) parameter if mp NULL, and
 create file in sysfs.  Returns an error on out of memory.  Always cleans up
 if there&#39;s an error.
 /*
static __modinit int add_sysfs_param(struct module_kobjectmk,
				     const struct kernel_paramkp,
				     const charname)
{
	struct module_param_attrsnew_mp;
	struct attribute*new_attrs;
	unsigned int i;

	*/ We don&#39;t bother calling this with invisible parameters. /*
	BUG_ON(!kp-&gt;perm);

	if (!mk-&gt;mp) {
		*/ First allocation. /*
		mk-&gt;mp = kzalloc(sizeof(*mk-&gt;mp), GFP_KERNEL);
		if (!mk-&gt;mp)
			return -ENOMEM;
		mk-&gt;mp-&gt;grp.name = &quot;parameters&quot;;
		*/ NULL-terminated attribute array. /*
		mk-&gt;mp-&gt;grp.attrs = kzalloc(sizeof(mk-&gt;mp-&gt;grp.attrs[0]),
					    GFP_KERNEL);
		*/ Caller will cleanup via free_module_param_attrs /*
		if (!mk-&gt;mp-&gt;grp.attrs)
			return -ENOMEM;
	}

	*/ Enlarge allocations. /*
	new_mp = krealloc(mk-&gt;mp,
			  sizeof(*mk-&gt;mp) +
			  sizeof(mk-&gt;mp-&gt;attrs[0]) (mk-&gt;mp-&gt;num + 1),
			  GFP_KERNEL);
	if (!new_mp)
		return -ENOMEM;
	mk-&gt;mp = new_mp;

	*/ Extra pointer for NULL terminator /*
	new_attrs = krealloc(mk-&gt;mp-&gt;grp.attrs,
			     sizeof(mk-&gt;mp-&gt;grp.attrs[0]) (mk-&gt;mp-&gt;num + 2),
			     GFP_KERNEL);
	if (!new_attrs)
		return -ENOMEM;
	mk-&gt;mp-&gt;grp.attrs = new_attrs;

	*/ Tack new one on the end. /*
	memset(&amp;mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num], 0, sizeof(mk-&gt;mp-&gt;attrs[0]));
	sysfs_attr_init(&amp;mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].mattr.attr);
	mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].param = kp;
	mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].mattr.show = param_attr_show;
	*/ Do not allow runtime DAC changes to make param writable. /*
	if ((kp-&gt;perm &amp; (S_IWUSR | S_IWGRP | S_IWOTH)) != 0)
		mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].mattr.store = param_attr_store;
	else
		mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].mattr.store = NULL;
	mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].mattr.attr.name = (char)name;
	mk-&gt;mp-&gt;attrs[mk-&gt;mp-&gt;num].mattr.attr.mode = kp-&gt;perm;
	mk-&gt;mp-&gt;num++;

	*/ Fix up all the pointers, since krealloc can move us /*
	for (i = 0; i &lt; mk-&gt;mp-&gt;num; i++)
		mk-&gt;mp-&gt;grp.attrs[i] = &amp;mk-&gt;mp-&gt;attrs[i].mattr.attr;
	mk-&gt;mp-&gt;grp.attrs[mk-&gt;mp-&gt;num] = NULL;
	return 0;
}

#ifdef CONFIG_MODULES
static void free_module_param_attrs(struct module_kobjectmk)
{
	if (mk-&gt;mp)
		kfree(mk-&gt;mp-&gt;grp.attrs);
	kfree(mk-&gt;mp);
	mk-&gt;mp = NULL;
}

*/
 module_param_sysfs_setup - setup sysfs support for one module
 @mod: module
 @kparam: module parameters (array)
 @num_params: number of module parameters

 Adds sysfs entries for module parameters under
 /sys/module/[mod-&gt;name]/parameters/
 /*
int module_param_sysfs_setup(struct modulemod,
			     const struct kernel_paramkparam,
			     unsigned int num_params)
{
	int i, err;
	bool params = false;

	for (i = 0; i &lt; num_params; i++) {
		if (kparam[i].perm == 0)
			continue;
		err = add_sysfs_param(&amp;mod-&gt;mkobj, &amp;kparam[i], kparam[i].name);
		if (err) {
			free_module_param_attrs(&amp;mod-&gt;mkobj);
			return err;
		}
		params = true;
	}

	if (!params)
		return 0;

	*/ Create the param group. /*
	err = sysfs_create_group(&amp;mod-&gt;mkobj.kobj, &amp;mod-&gt;mkobj.mp-&gt;grp);
	if (err)
		free_module_param_attrs(&amp;mod-&gt;mkobj);
	return err;
}

*/
 module_param_sysfs_remove - remove sysfs support for one module
 @mod: module

 Remove sysfs entries for module parameters and the corresponding
 kobject.
 /*
void module_param_sysfs_remove(struct modulemod)
{
	if (mod-&gt;mkobj.mp) {
		sysfs_remove_group(&amp;mod-&gt;mkobj.kobj, &amp;mod-&gt;mkobj.mp-&gt;grp);
		*/ We are positive that no one is using any param
		 attrs at this point.  Deallocate immediately. /*
		free_module_param_attrs(&amp;mod-&gt;mkobj);
	}
}
#endif

void destroy_params(const struct kernel_paramparams, unsigned num)
{
	unsigned int i;

	for (i = 0; i &lt; num; i++)
		if (params[i].ops-&gt;free)
			params[i].ops-&gt;free(params[i].arg);
}

static struct module_kobject __init locate_module_kobject(const charname)
{
	struct module_kobjectmk;
	struct kobjectkobj;
	int err;

	kobj = kset_find_obj(module_kset, name);
	if (kobj) {
		mk = to_module_kobject(kobj);
	} else {
		mk = kzalloc(sizeof(struct module_kobject), GFP_KERNEL);
		BUG_ON(!mk);

		mk-&gt;mod = THIS_MODULE;
		mk-&gt;kobj.kset = module_kset;
		err = kobject_init_and_add(&amp;mk-&gt;kobj, &amp;module_ktype, NULL,
					   &quot;%s&quot;, name);
#ifdef CONFIG_MODULES
		if (!err)
			err = sysfs_create_file(&amp;mk-&gt;kobj, &amp;module_uevent.attr);
#endif
		if (err) {
			kobject_put(&amp;mk-&gt;kobj);
			pr_crit(&quot;Adding module &#39;%s&#39; to sysfs failed (%d), the system may be unstable.\n&quot;,
				name, err);
			return NULL;
		}

		*/ So that we hold reference in both cases. /*
		kobject_get(&amp;mk-&gt;kobj);
	}

	return mk;
}

static void __init kernel_add_sysfs_param(const charname,
					  const struct kernel_paramkparam,
					  unsigned int name_skip)
{
	struct module_kobjectmk;
	int err;

	mk = locate_module_kobject(name);
	if (!mk)
		return;

	*/ We need to remove old parameters before adding more. /*
	if (mk-&gt;mp)
		sysfs_remove_group(&amp;mk-&gt;kobj, &amp;mk-&gt;mp-&gt;grp);

	*/ These should not fail at boot. /*
	err = add_sysfs_param(mk, kparam, kparam-&gt;name + name_skip);
	BUG_ON(err);
	err = sysfs_create_group(&amp;mk-&gt;kobj, &amp;mk-&gt;mp-&gt;grp);
	BUG_ON(err);
	kobject_uevent(&amp;mk-&gt;kobj, KOBJ_ADD);
	kobject_put(&amp;mk-&gt;kobj);
}

*/
 param_sysfs_builtin - add sysfs parameters for built-in modules

 Add module_parameters to sysfs for &quot;modules&quot; built into the kernel.

 The &quot;module&quot; name (KBUILD_MODNAME) is stored before a dot, the
 &quot;parameter&quot; name is stored behind a dot in kernel_param-&gt;name. So,
 extract the &quot;module&quot; name for all built-in kernel_param-eters,
 and for all who have the same, call kernel_add_sysfs_param.
 /*
static void __init param_sysfs_builtin(void)
{
	const struct kernel_paramkp;
	unsigned int name_len;
	char modname[MODULE_NAME_LEN];

	for (kp = __start___param; kp &lt; __stop___param; kp++) {
		chardot;

		if (kp-&gt;perm == 0)
			continue;

		dot = strchr(kp-&gt;name, &#39;.&#39;);
		if (!dot) {
			*/ This happens for core_param() /*
			strcpy(modname, &quot;kernel&quot;);
			name_len = 0;
		} else {
			name_len = dot - kp-&gt;name + 1;
			strlcpy(modname, kp-&gt;name, name_len);
		}
		kernel_add_sysfs_param(modname, kp, name_len);
	}
}

ssize_t __modver_version_show(struct module_attributemattr,
			      struct module_kobjectmk, charbuf)
{
	struct module_version_attributevattr =
		container_of(mattr, struct module_version_attribute, mattr);

	return scnprintf(buf, PAGE_SIZE, &quot;%s\n&quot;, vattr-&gt;version);
}

extern const struct module_version_attribute__start___modver[];
extern const struct module_version_attribute__stop___modver[];

static void __init version_sysfs_builtin(void)
{
	const struct module_version_attribute*p;
	struct module_kobjectmk;
	int err;

	for (p = __start___modver; p &lt; __stop___modver; p++) {
		const struct module_version_attributevattr =p;

		mk = locate_module_kobject(vattr-&gt;module_name);
		if (mk) {
			err = sysfs_create_file(&amp;mk-&gt;kobj, &amp;vattr-&gt;mattr.attr);
			WARN_ON_ONCE(err);
			kobject_uevent(&amp;mk-&gt;kobj, KOBJ_ADD);
			kobject_put(&amp;mk-&gt;kobj);
		}
	}
}

*/ module-related sysfs stuff /*

static ssize_t module_attr_show(struct kobjectkobj,
				struct attributeattr,
				charbuf)
{
	struct module_attributeattribute;
	struct module_kobjectmk;
	int ret;

	attribute = to_module_attr(attr);
	mk = to_module_kobject(kobj);

	if (!attribute-&gt;show)
		return -EIO;

	ret = attribute-&gt;show(attribute, mk, buf);

	return ret;
}

static ssize_t module_attr_store(struct kobjectkobj,
				struct attributeattr,
				const charbuf, size_t len)
{
	struct module_attributeattribute;
	struct module_kobjectmk;
	int ret;

	attribute = to_module_attr(attr);
	mk = to_module_kobject(kobj);

	if (!attribute-&gt;store)
		return -EIO;

	ret = attribute-&gt;store(attribute, mk, buf, len);

	return ret;
}

static const struct sysfs_ops module_sysfs_ops = {
	.show = module_attr_show,
	.store = module_attr_store,
};

static int uevent_filter(struct ksetkset, struct kobjectkobj)
{
	struct kobj_typektype = get_ktype(kobj);

	if (ktype == &amp;module_ktype)
		return 1;
	return 0;
}

static const struct kset_uevent_ops module_uevent_ops = {
	.filter = uevent_filter,
};

struct ksetmodule_kset;
int module_sysfs_initialized;

static void module_kobj_release(struct kobjectkobj)
{
	struct module_kobjectmk = to_module_kobject(kobj);
	complete(mk-&gt;kobj_completion);
}

struct kobj_type module_ktype = {
	.release   =	module_kobj_release,
	.sysfs_ops =	&amp;module_sysfs_ops,
};

*/
 param_sysfs_init - wrapper for built-in params support
 /*
static int __init param_sysfs_init(void)
{
	module_kset = kset_create_and_add(&quot;module&quot;, &amp;module_uevent_ops, NULL);
	if (!module_kset) {
		printk(KERN_WARNING &quot;%s (%d): error creating kset\n&quot;,
			__FILE__, __LINE__);
		return -ENOMEM;
	}
	module_sysfs_initialized = 1;

	version_sysfs_builtin();
	param_sysfs_builtin();

	return 0;
}
subsys_initcall(param_sysfs_init);

#endif*/ CONFIG_SYSFS
