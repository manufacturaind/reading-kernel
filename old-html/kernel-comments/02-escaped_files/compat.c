
  linux/kernel/compat.c

  Kernel compatibililty routines for e.g. 32 bit syscall support
  on 64 bit kernels.

  Copyright (C) 2002-2003 Stephen Rothwell, IBM Corporation

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.
 /*

#include &lt;linux/linkage.h&gt;
#include &lt;linux/compat.h&gt;
#include &lt;linux/errno.h&gt;
#include &lt;linux/time.h&gt;
#include &lt;linux/signal.h&gt;
#include &lt;linux/sched.h&gt;	*/ for MAX_SCHEDULE_TIMEOUT /*
#include &lt;linux/syscalls.h&gt;
#include &lt;linux/unistd.h&gt;
#include &lt;linux/security.h&gt;
#include &lt;linux/timex.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/migrate.h&gt;
#include &lt;linux/posix-timers.h&gt;
#include &lt;linux/times.h&gt;
#include &lt;linux/ptrace.h&gt;
#include &lt;linux/gfp.h&gt;

#include &lt;asm/uaccess.h&gt;

static int compat_get_timex(struct timextxc, struct compat_timex __userutp)
{
	memset(txc, 0, sizeof(struct timex));

	if (!access_ok(VERIFY_READ, utp, sizeof(struct compat_timex)) ||
			__get_user(txc-&gt;modes, &amp;utp-&gt;modes) ||
			__get_user(txc-&gt;offset, &amp;utp-&gt;offset) ||
			__get_user(txc-&gt;freq, &amp;utp-&gt;freq) ||
			__get_user(txc-&gt;maxerror, &amp;utp-&gt;maxerror) ||
			__get_user(txc-&gt;esterror, &amp;utp-&gt;esterror) ||
			__get_user(txc-&gt;status, &amp;utp-&gt;status) ||
			__get_user(txc-&gt;constant, &amp;utp-&gt;constant) ||
			__get_user(txc-&gt;precision, &amp;utp-&gt;precision) ||
			__get_user(txc-&gt;tolerance, &amp;utp-&gt;tolerance) ||
			__get_user(txc-&gt;time.tv_sec, &amp;utp-&gt;time.tv_sec) ||
			__get_user(txc-&gt;time.tv_usec, &amp;utp-&gt;time.tv_usec) ||
			__get_user(txc-&gt;tick, &amp;utp-&gt;tick) ||
			__get_user(txc-&gt;ppsfreq, &amp;utp-&gt;ppsfreq) ||
			__get_user(txc-&gt;jitter, &amp;utp-&gt;jitter) ||
			__get_user(txc-&gt;shift, &amp;utp-&gt;shift) ||
			__get_user(txc-&gt;stabil, &amp;utp-&gt;stabil) ||
			__get_user(txc-&gt;jitcnt, &amp;utp-&gt;jitcnt) ||
			__get_user(txc-&gt;calcnt, &amp;utp-&gt;calcnt) ||
			__get_user(txc-&gt;errcnt, &amp;utp-&gt;errcnt) ||
			__get_user(txc-&gt;stbcnt, &amp;utp-&gt;stbcnt))
		return -EFAULT;

	return 0;
}

static int compat_put_timex(struct compat_timex __userutp, struct timextxc)
{
	if (!access_ok(VERIFY_WRITE, utp, sizeof(struct compat_timex)) ||
			__put_user(txc-&gt;modes, &amp;utp-&gt;modes) ||
			__put_user(txc-&gt;offset, &amp;utp-&gt;offset) ||
			__put_user(txc-&gt;freq, &amp;utp-&gt;freq) ||
			__put_user(txc-&gt;maxerror, &amp;utp-&gt;maxerror) ||
			__put_user(txc-&gt;esterror, &amp;utp-&gt;esterror) ||
			__put_user(txc-&gt;status, &amp;utp-&gt;status) ||
			__put_user(txc-&gt;constant, &amp;utp-&gt;constant) ||
			__put_user(txc-&gt;precision, &amp;utp-&gt;precision) ||
			__put_user(txc-&gt;tolerance, &amp;utp-&gt;tolerance) ||
			__put_user(txc-&gt;time.tv_sec, &amp;utp-&gt;time.tv_sec) ||
			__put_user(txc-&gt;time.tv_usec, &amp;utp-&gt;time.tv_usec) ||
			__put_user(txc-&gt;tick, &amp;utp-&gt;tick) ||
			__put_user(txc-&gt;ppsfreq, &amp;utp-&gt;ppsfreq) ||
			__put_user(txc-&gt;jitter, &amp;utp-&gt;jitter) ||
			__put_user(txc-&gt;shift, &amp;utp-&gt;shift) ||
			__put_user(txc-&gt;stabil, &amp;utp-&gt;stabil) ||
			__put_user(txc-&gt;jitcnt, &amp;utp-&gt;jitcnt) ||
			__put_user(txc-&gt;calcnt, &amp;utp-&gt;calcnt) ||
			__put_user(txc-&gt;errcnt, &amp;utp-&gt;errcnt) ||
			__put_user(txc-&gt;stbcnt, &amp;utp-&gt;stbcnt) ||
			__put_user(txc-&gt;tai, &amp;utp-&gt;tai))
		return -EFAULT;
	return 0;
}

COMPAT_SYSCALL_DEFINE2(gettimeofday, struct compat_timeval __user, tv,
		       struct timezone __user, tz)
{
	if (tv) {
		struct timeval ktv;
		do_gettimeofday(&amp;ktv);
		if (compat_put_timeval(&amp;ktv, tv))
			return -EFAULT;
	}
	if (tz) {
		if (copy_to_user(tz, &amp;sys_tz, sizeof(sys_tz)))
			return -EFAULT;
	}

	return 0;
}

COMPAT_SYSCALL_DEFINE2(settimeofday, struct compat_timeval __user, tv,
		       struct timezone __user, tz)
{
	struct timeval user_tv;
	struct timespec	new_ts;
	struct timezone new_tz;

	if (tv) {
		if (compat_get_timeval(&amp;user_tv, tv))
			return -EFAULT;
		new_ts.tv_sec = user_tv.tv_sec;
		new_ts.tv_nsec = user_tv.tv_usec NSEC_PER_USEC;
	}
	if (tz) {
		if (copy_from_user(&amp;new_tz, tz, sizeof(*tz)))
			return -EFAULT;
	}

	return do_sys_settimeofday(tv ? &amp;new_ts : NULL, tz ? &amp;new_tz : NULL);
}

static int __compat_get_timeval(struct timevaltv, const struct compat_timeval __userctv)
{
	return (!access_ok(VERIFY_READ, ctv, sizeof(*ctv)) ||
			__get_user(tv-&gt;tv_sec, &amp;ctv-&gt;tv_sec) ||
			__get_user(tv-&gt;tv_usec, &amp;ctv-&gt;tv_usec)) ? -EFAULT : 0;
}

static int __compat_put_timeval(const struct timevaltv, struct compat_timeval __userctv)
{
	return (!access_ok(VERIFY_WRITE, ctv, sizeof(*ctv)) ||
			__put_user(tv-&gt;tv_sec, &amp;ctv-&gt;tv_sec) ||
			__put_user(tv-&gt;tv_usec, &amp;ctv-&gt;tv_usec)) ? -EFAULT : 0;
}

static int __compat_get_timespec(struct timespects, const struct compat_timespec __usercts)
{
	return (!access_ok(VERIFY_READ, cts, sizeof(*cts)) ||
			__get_user(ts-&gt;tv_sec, &amp;cts-&gt;tv_sec) ||
			__get_user(ts-&gt;tv_nsec, &amp;cts-&gt;tv_nsec)) ? -EFAULT : 0;
}

static int __compat_put_timespec(const struct timespects, struct compat_timespec __usercts)
{
	return (!access_ok(VERIFY_WRITE, cts, sizeof(*cts)) ||
			__put_user(ts-&gt;tv_sec, &amp;cts-&gt;tv_sec) ||
			__put_user(ts-&gt;tv_nsec, &amp;cts-&gt;tv_nsec)) ? -EFAULT : 0;
}

int compat_get_timeval(struct timevaltv, const void __userutv)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_from_user(tv, utv, sizeof(*tv)) ? -EFAULT : 0;
	else
		return __compat_get_timeval(tv, utv);
}
EXPORT_SYMBOL_GPL(compat_get_timeval);

int compat_put_timeval(const struct timevaltv, void __userutv)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_to_user(utv, tv, sizeof(*tv)) ? -EFAULT : 0;
	else
		return __compat_put_timeval(tv, utv);
}
EXPORT_SYMBOL_GPL(compat_put_timeval);

int compat_get_timespec(struct timespects, const void __useruts)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_from_user(ts, uts, sizeof(*ts)) ? -EFAULT : 0;
	else
		return __compat_get_timespec(ts, uts);
}
EXPORT_SYMBOL_GPL(compat_get_timespec);

int compat_put_timespec(const struct timespects, void __useruts)
{
	if (COMPAT_USE_64BIT_TIME)
		return copy_to_user(uts, ts, sizeof(*ts)) ? -EFAULT : 0;
	else
		return __compat_put_timespec(ts, uts);
}
EXPORT_SYMBOL_GPL(compat_put_timespec);

int compat_convert_timespec(struct timespec __user*kts,
			    const void __usercts)
{
	struct timespec ts;
	struct timespec __useruts;

	if (!cts || COMPAT_USE_64BIT_TIME) {
		*kts = (struct timespec __user)cts;
		return 0;
	}

	uts = compat_alloc_user_space(sizeof(ts));
	if (!uts)
		return -EFAULT;
	if (compat_get_timespec(&amp;ts, cts))
		return -EFAULT;
	if (copy_to_user(uts, &amp;ts, sizeof(ts)))
		return -EFAULT;

	*kts = uts;
	return 0;
}

static long compat_nanosleep_restart(struct restart_blockrestart)
{
	struct compat_timespec __userrmtp;
	struct timespec rmt;
	mm_segment_t oldfs;
	long ret;

	restart-&gt;nanosleep.rmtp = (struct timespec __user) &amp;rmt;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	ret = hrtimer_nanosleep_restart(restart);
	set_fs(oldfs);

	if (ret == -ERESTART_RESTARTBLOCK) {
		rmtp = restart-&gt;nanosleep.compat_rmtp;

		if (rmtp &amp;&amp; compat_put_timespec(&amp;rmt, rmtp))
			return -EFAULT;
	}

	return ret;
}

COMPAT_SYSCALL_DEFINE2(nanosleep, struct compat_timespec __user, rqtp,
		       struct compat_timespec __user, rmtp)
{
	struct timespec tu, rmt;
	mm_segment_t oldfs;
	long ret;

	if (compat_get_timespec(&amp;tu, rqtp))
		return -EFAULT;

	if (!timespec_valid(&amp;tu))
		return -EINVAL;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	ret = hrtimer_nanosleep(&amp;tu,
				rmtp ? (struct timespec __user)&amp;rmt : NULL,
				HRTIMER_MODE_REL, CLOCK_MONOTONIC);
	set_fs(oldfs);

	*/
	 hrtimer_nanosleep() can only return 0 or
	 -ERESTART_RESTARTBLOCK here because:
	
	 - we call it with HRTIMER_MODE_REL and therefor exclude the
	   -ERESTARTNOHAND return path.
	
	 - we supply the rmtp argument from the task stack (due to
	   the necessary compat conversion. So the update cannot
	   fail, which excludes the -EFAULT return path as well. If
	   it fails nevertheless we have a bigger problem and wont
	   reach this place anymore.
	
	 - if the return value is 0, we do not have to update rmtp
	    because there is no remaining time.
	
	 We check for -ERESTART_RESTARTBLOCK nevertheless if the
	 core implementation decides to return random nonsense.
	 /*
	if (ret == -ERESTART_RESTARTBLOCK) {
		struct restart_blockrestart = &amp;current-&gt;restart_block;

		restart-&gt;fn = compat_nanosleep_restart;
		restart-&gt;nanosleep.compat_rmtp = rmtp;

		if (rmtp &amp;&amp; compat_put_timespec(&amp;rmt, rmtp))
			return -EFAULT;
	}
	return ret;
}

static inline long get_compat_itimerval(struct itimervalo,
		struct compat_itimerval __useri)
{
	return (!access_ok(VERIFY_READ, i, sizeof(*i)) ||
		(__get_user(o-&gt;it_interval.tv_sec, &amp;i-&gt;it_interval.tv_sec) |
		 __get_user(o-&gt;it_interval.tv_usec, &amp;i-&gt;it_interval.tv_usec) |
		 __get_user(o-&gt;it_value.tv_sec, &amp;i-&gt;it_value.tv_sec) |
		 __get_user(o-&gt;it_value.tv_usec, &amp;i-&gt;it_value.tv_usec)));
}

static inline long put_compat_itimerval(struct compat_itimerval __usero,
		struct itimervali)
{
	return (!access_ok(VERIFY_WRITE, o, sizeof(*o)) ||
		(__put_user(i-&gt;it_interval.tv_sec, &amp;o-&gt;it_interval.tv_sec) |
		 __put_user(i-&gt;it_interval.tv_usec, &amp;o-&gt;it_interval.tv_usec) |
		 __put_user(i-&gt;it_value.tv_sec, &amp;o-&gt;it_value.tv_sec) |
		 __put_user(i-&gt;it_value.tv_usec, &amp;o-&gt;it_value.tv_usec)));
}

COMPAT_SYSCALL_DEFINE2(getitimer, int, which,
		struct compat_itimerval __user, it)
{
	struct itimerval kit;
	int error;

	error = do_getitimer(which, &amp;kit);
	if (!error &amp;&amp; put_compat_itimerval(it, &amp;kit))
		error = -EFAULT;
	return error;
}

COMPAT_SYSCALL_DEFINE3(setitimer, int, which,
		struct compat_itimerval __user, in,
		struct compat_itimerval __user, out)
{
	struct itimerval kin, kout;
	int error;

	if (in) {
		if (get_compat_itimerval(&amp;kin, in))
			return -EFAULT;
	} else
		memset(&amp;kin, 0, sizeof(kin));

	error = do_setitimer(which, &amp;kin, out ? &amp;kout : NULL);
	if (error || !out)
		return error;
	if (put_compat_itimerval(out, &amp;kout))
		return -EFAULT;
	return 0;
}

static compat_clock_t clock_t_to_compat_clock_t(clock_t x)
{
	return compat_jiffies_to_clock_t(clock_t_to_jiffies(x));
}

COMPAT_SYSCALL_DEFINE1(times, struct compat_tms __user, tbuf)
{
	if (tbuf) {
		struct tms tms;
		struct compat_tms tmp;

		do_sys_times(&amp;tms);
		*/ Convert our struct tms to the compat version. /*
		tmp.tms_utime = clock_t_to_compat_clock_t(tms.tms_utime);
		tmp.tms_stime = clock_t_to_compat_clock_t(tms.tms_stime);
		tmp.tms_cutime = clock_t_to_compat_clock_t(tms.tms_cutime);
		tmp.tms_cstime = clock_t_to_compat_clock_t(tms.tms_cstime);
		if (copy_to_user(tbuf, &amp;tmp, sizeof(tmp)))
			return -EFAULT;
	}
	force_successful_syscall_return();
	return compat_jiffies_to_clock_t(jiffies);
}

#ifdef __ARCH_WANT_SYS_SIGPENDING

*/
 Assumption: old_sigset_t and compat_old_sigset_t are both
 types that can be passed to put_user()/get_user().
 /*

COMPAT_SYSCALL_DEFINE1(sigpending, compat_old_sigset_t __user, set)
{
	old_sigset_t s;
	long ret;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	ret = sys_sigpending((old_sigset_t __user) &amp;s);
	set_fs(old_fs);
	if (ret == 0)
		ret = put_user(s, set);
	return ret;
}

#endif

#ifdef __ARCH_WANT_SYS_SIGPROCMASK

*/
 sys_sigprocmask SIG_SETMASK sets the first (compat) word of the
 blocked set of signals to the supplied signal set
 /*
static inline void compat_sig_setmask(sigset_tblocked, compat_sigset_word set)
{
	memcpy(blocked-&gt;sig, &amp;set, sizeof(set));
}

COMPAT_SYSCALL_DEFINE3(sigprocmask, int, how,
		       compat_old_sigset_t __user, nset,
		       compat_old_sigset_t __user, oset)
{
	old_sigset_t old_set, new_set;
	sigset_t new_blocked;

	old_set = current-&gt;blocked.sig[0];

	if (nset) {
		if (get_user(new_set, nset))
			return -EFAULT;
		new_set &amp;= ~(sigmask(SIGKILL) | sigmask(SIGSTOP));

		new_blocked = current-&gt;blocked;

		switch (how) {
		case SIG_BLOCK:
			sigaddsetmask(&amp;new_blocked, new_set);
			break;
		case SIG_UNBLOCK:
			sigdelsetmask(&amp;new_blocked, new_set);
			break;
		case SIG_SETMASK:
			compat_sig_setmask(&amp;new_blocked, new_set);
			break;
		default:
			return -EINVAL;
		}

		set_current_blocked(&amp;new_blocked);
	}

	if (oset) {
		if (put_user(old_set, oset))
			return -EFAULT;
	}

	return 0;
}

#endif

COMPAT_SYSCALL_DEFINE2(setrlimit, unsigned int, resource,
		       struct compat_rlimit __user, rlim)
{
	struct rlimit r;

	if (!access_ok(VERIFY_READ, rlim, sizeof(*rlim)) ||
	    __get_user(r.rlim_cur, &amp;rlim-&gt;rlim_cur) ||
	    __get_user(r.rlim_max, &amp;rlim-&gt;rlim_max))
		return -EFAULT;

	if (r.rlim_cur == COMPAT_RLIM_INFINITY)
		r.rlim_cur = RLIM_INFINITY;
	if (r.rlim_max == COMPAT_RLIM_INFINITY)
		r.rlim_max = RLIM_INFINITY;
	return do_prlimit(current, resource, &amp;r, NULL);
}

#ifdef COMPAT_RLIM_OLD_INFINITY

COMPAT_SYSCALL_DEFINE2(old_getrlimit, unsigned int, resource,
		       struct compat_rlimit __user, rlim)
{
	struct rlimit r;
	int ret;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	ret = sys_old_getrlimit(resource, (struct rlimit __user)&amp;r);
	set_fs(old_fs);

	if (!ret) {
		if (r.rlim_cur &gt; COMPAT_RLIM_OLD_INFINITY)
			r.rlim_cur = COMPAT_RLIM_INFINITY;
		if (r.rlim_max &gt; COMPAT_RLIM_OLD_INFINITY)
			r.rlim_max = COMPAT_RLIM_INFINITY;

		if (!access_ok(VERIFY_WRITE, rlim, sizeof(*rlim)) ||
		    __put_user(r.rlim_cur, &amp;rlim-&gt;rlim_cur) ||
		    __put_user(r.rlim_max, &amp;rlim-&gt;rlim_max))
			return -EFAULT;
	}
	return ret;
}

#endif

COMPAT_SYSCALL_DEFINE2(getrlimit, unsigned int, resource,
		       struct compat_rlimit __user, rlim)
{
	struct rlimit r;
	int ret;

	ret = do_prlimit(current, resource, NULL, &amp;r);
	if (!ret) {
		if (r.rlim_cur &gt; COMPAT_RLIM_INFINITY)
			r.rlim_cur = COMPAT_RLIM_INFINITY;
		if (r.rlim_max &gt; COMPAT_RLIM_INFINITY)
			r.rlim_max = COMPAT_RLIM_INFINITY;

		if (!access_ok(VERIFY_WRITE, rlim, sizeof(*rlim)) ||
		    __put_user(r.rlim_cur, &amp;rlim-&gt;rlim_cur) ||
		    __put_user(r.rlim_max, &amp;rlim-&gt;rlim_max))
			return -EFAULT;
	}
	return ret;
}

int put_compat_rusage(const struct rusager, struct compat_rusage __userru)
{
	if (!access_ok(VERIFY_WRITE, ru, sizeof(*ru)) ||
	    __put_user(r-&gt;ru_utime.tv_sec, &amp;ru-&gt;ru_utime.tv_sec) ||
	    __put_user(r-&gt;ru_utime.tv_usec, &amp;ru-&gt;ru_utime.tv_usec) ||
	    __put_user(r-&gt;ru_stime.tv_sec, &amp;ru-&gt;ru_stime.tv_sec) ||
	    __put_user(r-&gt;ru_stime.tv_usec, &amp;ru-&gt;ru_stime.tv_usec) ||
	    __put_user(r-&gt;ru_maxrss, &amp;ru-&gt;ru_maxrss) ||
	    __put_user(r-&gt;ru_ixrss, &amp;ru-&gt;ru_ixrss) ||
	    __put_user(r-&gt;ru_idrss, &amp;ru-&gt;ru_idrss) ||
	    __put_user(r-&gt;ru_isrss, &amp;ru-&gt;ru_isrss) ||
	    __put_user(r-&gt;ru_minflt, &amp;ru-&gt;ru_minflt) ||
	    __put_user(r-&gt;ru_majflt, &amp;ru-&gt;ru_majflt) ||
	    __put_user(r-&gt;ru_nswap, &amp;ru-&gt;ru_nswap) ||
	    __put_user(r-&gt;ru_inblock, &amp;ru-&gt;ru_inblock) ||
	    __put_user(r-&gt;ru_oublock, &amp;ru-&gt;ru_oublock) ||
	    __put_user(r-&gt;ru_msgsnd, &amp;ru-&gt;ru_msgsnd) ||
	    __put_user(r-&gt;ru_msgrcv, &amp;ru-&gt;ru_msgrcv) ||
	    __put_user(r-&gt;ru_nsignals, &amp;ru-&gt;ru_nsignals) ||
	    __put_user(r-&gt;ru_nvcsw, &amp;ru-&gt;ru_nvcsw) ||
	    __put_user(r-&gt;ru_nivcsw, &amp;ru-&gt;ru_nivcsw))
		return -EFAULT;
	return 0;
}

COMPAT_SYSCALL_DEFINE4(wait4,
	compat_pid_t, pid,
	compat_uint_t __user, stat_addr,
	int, options,
	struct compat_rusage __user, ru)
{
	if (!ru) {
		return sys_wait4(pid, stat_addr, options, NULL);
	} else {
		struct rusage r;
		int ret;
		unsigned int status;
		mm_segment_t old_fs = get_fs();

		set_fs (KERNEL_DS);
		ret = sys_wait4(pid,
				(stat_addr ?
				 (unsigned int __user) &amp;status : NULL),
				options, (struct rusage __user) &amp;r);
		set_fs (old_fs);

		if (ret &gt; 0) {
			if (put_compat_rusage(&amp;r, ru))
				return -EFAULT;
			if (stat_addr &amp;&amp; put_user(status, stat_addr))
				return -EFAULT;
		}
		return ret;
	}
}

COMPAT_SYSCALL_DEFINE5(waitid,
		int, which, compat_pid_t, pid,
		struct compat_siginfo __user, uinfo, int, options,
		struct compat_rusage __user, uru)
{
	siginfo_t info;
	struct rusage ru;
	long ret;
	mm_segment_t old_fs = get_fs();

	memset(&amp;info, 0, sizeof(info));

	set_fs(KERNEL_DS);
	ret = sys_waitid(which, pid, (siginfo_t __user)&amp;info, options,
			 uru ? (struct rusage __user)&amp;ru : NULL);
	set_fs(old_fs);

	if ((ret &lt; 0) || (info.si_signo == 0))
		return ret;

	if (uru) {
		*/ sys_waitid() overwrites everything in ru /*
		if (COMPAT_USE_64BIT_TIME)
			ret = copy_to_user(uru, &amp;ru, sizeof(ru));
		else
			ret = put_compat_rusage(&amp;ru, uru);
		if (ret)
			return -EFAULT;
	}

	BUG_ON(info.si_code &amp; __SI_MASK);
	info.si_code |= __SI_CHLD;
	return copy_siginfo_to_user32(uinfo, &amp;info);
}

static int compat_get_user_cpu_mask(compat_ulong_t __useruser_mask_ptr,
				    unsigned len, struct cpumasknew_mask)
{
	unsigned longk;

	if (len &lt; cpumask_size())
		memset(new_mask, 0, cpumask_size());
	else if (len &gt; cpumask_size())
		len = cpumask_size();

	k = cpumask_bits(new_mask);
	return compat_get_bitmap(k, user_mask_ptr, len 8);
}

COMPAT_SYSCALL_DEFINE3(sched_setaffinity, compat_pid_t, pid,
		       unsigned int, len,
		       compat_ulong_t __user, user_mask_ptr)
{
	cpumask_var_t new_mask;
	int retval;

	if (!alloc_cpumask_var(&amp;new_mask, GFP_KERNEL))
		return -ENOMEM;

	retval = compat_get_user_cpu_mask(user_mask_ptr, len, new_mask);
	if (retval)
		goto out;

	retval = sched_setaffinity(pid, new_mask);
out:
	free_cpumask_var(new_mask);
	return retval;
}

COMPAT_SYSCALL_DEFINE3(sched_getaffinity, compat_pid_t,  pid, unsigned int, len,
		       compat_ulong_t __user, user_mask_ptr)
{
	int ret;
	cpumask_var_t mask;

	if ((len BITS_PER_BYTE) &lt; nr_cpu_ids)
		return -EINVAL;
	if (len &amp; (sizeof(compat_ulong_t)-1))
		return -EINVAL;

	if (!alloc_cpumask_var(&amp;mask, GFP_KERNEL))
		return -ENOMEM;

	ret = sched_getaffinity(pid, mask);
	if (ret == 0) {
		size_t retlen = min_t(size_t, len, cpumask_size());

		if (compat_put_bitmap(user_mask_ptr, cpumask_bits(mask), retlen 8))
			ret = -EFAULT;
		else
			ret = retlen;
	}
	free_cpumask_var(mask);

	return ret;
}

int get_compat_itimerspec(struct itimerspecdst,
			  const struct compat_itimerspec __usersrc)
{
	if (__compat_get_timespec(&amp;dst-&gt;it_interval, &amp;src-&gt;it_interval) ||
	    __compat_get_timespec(&amp;dst-&gt;it_value, &amp;src-&gt;it_value))
		return -EFAULT;
	return 0;
}

int put_compat_itimerspec(struct compat_itimerspec __userdst,
			  const struct itimerspecsrc)
{
	if (__compat_put_timespec(&amp;src-&gt;it_interval, &amp;dst-&gt;it_interval) ||
	    __compat_put_timespec(&amp;src-&gt;it_value, &amp;dst-&gt;it_value))
		return -EFAULT;
	return 0;
}

COMPAT_SYSCALL_DEFINE3(timer_create, clockid_t, which_clock,
		       struct compat_sigevent __user, timer_event_spec,
		       timer_t __user, created_timer_id)
{
	struct sigevent __userevent = NULL;

	if (timer_event_spec) {
		struct sigevent kevent;

		event = compat_alloc_user_space(sizeof(*event));
		if (get_compat_sigevent(&amp;kevent, timer_event_spec) ||
		    copy_to_user(event, &amp;kevent, sizeof(*event)))
			return -EFAULT;
	}

	return sys_timer_create(which_clock, event, created_timer_id);
}

COMPAT_SYSCALL_DEFINE4(timer_settime, timer_t, timer_id, int, flags,
		       struct compat_itimerspec __user, new,
		       struct compat_itimerspec __user, old)
{
	long err;
	mm_segment_t oldfs;
	struct itimerspec newts, oldts;

	if (!new)
		return -EINVAL;
	if (get_compat_itimerspec(&amp;newts, new))
		return -EFAULT;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_timer_settime(timer_id, flags,
				(struct itimerspec __user) &amp;newts,
				(struct itimerspec __user) &amp;oldts);
	set_fs(oldfs);
	if (!err &amp;&amp; old &amp;&amp; put_compat_itimerspec(old, &amp;oldts))
		return -EFAULT;
	return err;
}

COMPAT_SYSCALL_DEFINE2(timer_gettime, timer_t, timer_id,
		       struct compat_itimerspec __user, setting)
{
	long err;
	mm_segment_t oldfs;
	struct itimerspec ts;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_timer_gettime(timer_id,
				(struct itimerspec __user) &amp;ts);
	set_fs(oldfs);
	if (!err &amp;&amp; put_compat_itimerspec(setting, &amp;ts))
		return -EFAULT;
	return err;
}

COMPAT_SYSCALL_DEFINE2(clock_settime, clockid_t, which_clock,
		       struct compat_timespec __user, tp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec ts;

	if (compat_get_timespec(&amp;ts, tp))
		return -EFAULT;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_settime(which_clock,
				(struct timespec __user) &amp;ts);
	set_fs(oldfs);
	return err;
}

COMPAT_SYSCALL_DEFINE2(clock_gettime, clockid_t, which_clock,
		       struct compat_timespec __user, tp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec ts;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_gettime(which_clock,
				(struct timespec __user) &amp;ts);
	set_fs(oldfs);
	if (!err &amp;&amp; compat_put_timespec(&amp;ts, tp))
		return -EFAULT;
	return err;
}

COMPAT_SYSCALL_DEFINE2(clock_adjtime, clockid_t, which_clock,
		       struct compat_timex __user, utp)
{
	struct timex txc;
	mm_segment_t oldfs;
	int err, ret;

	err = compat_get_timex(&amp;txc, utp);
	if (err)
		return err;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	ret = sys_clock_adjtime(which_clock, (struct timex __user) &amp;txc);
	set_fs(oldfs);

	err = compat_put_timex(utp, &amp;txc);
	if (err)
		return err;

	return ret;
}

COMPAT_SYSCALL_DEFINE2(clock_getres, clockid_t, which_clock,
		       struct compat_timespec __user, tp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec ts;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_getres(which_clock,
			       (struct timespec __user) &amp;ts);
	set_fs(oldfs);
	if (!err &amp;&amp; tp &amp;&amp; compat_put_timespec(&amp;ts, tp))
		return -EFAULT;
	return err;
}

static long compat_clock_nanosleep_restart(struct restart_blockrestart)
{
	long err;
	mm_segment_t oldfs;
	struct timespec tu;
	struct compat_timespec __userrmtp = restart-&gt;nanosleep.compat_rmtp;

	restart-&gt;nanosleep.rmtp = (struct timespec __user) &amp;tu;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = clock_nanosleep_restart(restart);
	set_fs(oldfs);

	if ((err == -ERESTART_RESTARTBLOCK) &amp;&amp; rmtp &amp;&amp;
	    compat_put_timespec(&amp;tu, rmtp))
		return -EFAULT;

	if (err == -ERESTART_RESTARTBLOCK) {
		restart-&gt;fn = compat_clock_nanosleep_restart;
		restart-&gt;nanosleep.compat_rmtp = rmtp;
	}
	return err;
}

COMPAT_SYSCALL_DEFINE4(clock_nanosleep, clockid_t, which_clock, int, flags,
		       struct compat_timespec __user, rqtp,
		       struct compat_timespec __user, rmtp)
{
	long err;
	mm_segment_t oldfs;
	struct timespec in, out;
	struct restart_blockrestart;

	if (compat_get_timespec(&amp;in, rqtp))
		return -EFAULT;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = sys_clock_nanosleep(which_clock, flags,
				  (struct timespec __user) &amp;in,
				  (struct timespec __user) &amp;out);
	set_fs(oldfs);

	if ((err == -ERESTART_RESTARTBLOCK) &amp;&amp; rmtp &amp;&amp;
	    compat_put_timespec(&amp;out, rmtp))
		return -EFAULT;

	if (err == -ERESTART_RESTARTBLOCK) {
		restart = &amp;current-&gt;restart_block;
		restart-&gt;fn = compat_clock_nanosleep_restart;
		restart-&gt;nanosleep.compat_rmtp = rmtp;
	}
	return err;
}

*/
 We currently only need the following fields from the sigevent
 structure: sigev_value, sigev_signo, sig_notify and (sometimes
 sigev_notify_thread_id).  The others are handled in user mode.
 We also assume that copying sigev_value.sival_int is sufficient
 to keep all the bits of sigev_value.sival_ptr intact.
 /*
int get_compat_sigevent(struct sigeventevent,
		const struct compat_sigevent __useru_event)
{
	memset(event, 0, sizeof(*event));
	return (!access_ok(VERIFY_READ, u_event, sizeof(*u_event)) ||
		__get_user(event-&gt;sigev_value.sival_int,
			&amp;u_event-&gt;sigev_value.sival_int) ||
		__get_user(event-&gt;sigev_signo, &amp;u_event-&gt;sigev_signo) ||
		__get_user(event-&gt;sigev_notify, &amp;u_event-&gt;sigev_notify) ||
		__get_user(event-&gt;sigev_notify_thread_id,
			&amp;u_event-&gt;sigev_notify_thread_id))
		? -EFAULT : 0;
}

long compat_get_bitmap(unsigned longmask, const compat_ulong_t __userumask,
		       unsigned long bitmap_size)
{
	int i, j;
	unsigned long m;
	compat_ulong_t um;
	unsigned long nr_compat_longs;

	*/ align bitmap up to nearest compat_long_t boundary /*
	bitmap_size = ALIGN(bitmap_size, BITS_PER_COMPAT_LONG);

	if (!access_ok(VERIFY_READ, umask, bitmap_size / 8))
		return -EFAULT;

	nr_compat_longs = BITS_TO_COMPAT_LONGS(bitmap_size);

	for (i = 0; i &lt; BITS_TO_LONGS(bitmap_size); i++) {
		m = 0;

		for (j = 0; j &lt; sizeof(m)/sizeof(um); j++) {
			*/
			 We dont want to read past the end of the userspace
			 bitmap. We must however ensure the end of the
			 kernel bitmap is zeroed.
			 /*
			if (nr_compat_longs) {
				nr_compat_longs--;
				if (__get_user(um, umask))
					return -EFAULT;
			} else {
				um = 0;
			}

			umask++;
			m |= (long)um &lt;&lt; (j BITS_PER_COMPAT_LONG);
		}
		*mask++ = m;
	}

	return 0;
}

long compat_put_bitmap(compat_ulong_t __userumask, unsigned longmask,
		       unsigned long bitmap_size)
{
	int i, j;
	unsigned long m;
	compat_ulong_t um;
	unsigned long nr_compat_longs;

	*/ align bitmap up to nearest compat_long_t boundary /*
	bitmap_size = ALIGN(bitmap_size, BITS_PER_COMPAT_LONG);

	if (!access_ok(VERIFY_WRITE, umask, bitmap_size / 8))
		return -EFAULT;

	nr_compat_longs = BITS_TO_COMPAT_LONGS(bitmap_size);

	for (i = 0; i &lt; BITS_TO_LONGS(bitmap_size); i++) {
		m =mask++;

		for (j = 0; j &lt; sizeof(m)/sizeof(um); j++) {
			um = m;

			*/
			 We dont want to write past the end of the userspace
			 bitmap.
			 /*
			if (nr_compat_longs) {
				nr_compat_longs--;
				if (__put_user(um, umask))
					return -EFAULT;
			}

			umask++;
			m &gt;&gt;= 4*sizeof(um);
			m &gt;&gt;= 4*sizeof(um);
		}
	}

	return 0;
}

void
sigset_from_compat(sigset_tset, const compat_sigset_tcompat)
{
	switch (_NSIG_WORDS) {
	case 4: set-&gt;sig[3] = compat-&gt;sig[6] | (((long)compat-&gt;sig[7]) &lt;&lt; 32 );
	case 3: set-&gt;sig[2] = compat-&gt;sig[4] | (((long)compat-&gt;sig[5]) &lt;&lt; 32 );
	case 2: set-&gt;sig[1] = compat-&gt;sig[2] | (((long)compat-&gt;sig[3]) &lt;&lt; 32 );
	case 1: set-&gt;sig[0] = compat-&gt;sig[0] | (((long)compat-&gt;sig[1]) &lt;&lt; 32 );
	}
}
EXPORT_SYMBOL_GPL(sigset_from_compat);

void
sigset_to_compat(compat_sigset_tcompat, const sigset_tset)
{
	switch (_NSIG_WORDS) {
	case 4: compat-&gt;sig[7] = (set-&gt;sig[3] &gt;&gt; 32); compat-&gt;sig[6] = set-&gt;sig[3];
	case 3: compat-&gt;sig[5] = (set-&gt;sig[2] &gt;&gt; 32); compat-&gt;sig[4] = set-&gt;sig[2];
	case 2: compat-&gt;sig[3] = (set-&gt;sig[1] &gt;&gt; 32); compat-&gt;sig[2] = set-&gt;sig[1];
	case 1: compat-&gt;sig[1] = (set-&gt;sig[0] &gt;&gt; 32); compat-&gt;sig[0] = set-&gt;sig[0];
	}
}

COMPAT_SYSCALL_DEFINE4(rt_sigtimedwait, compat_sigset_t __user, uthese,
		struct compat_siginfo __user, uinfo,
		struct compat_timespec __user, uts, compat_size_t, sigsetsize)
{
	compat_sigset_t s32;
	sigset_t s;
	struct timespec t;
	siginfo_t info;
	long ret;

	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (copy_from_user(&amp;s32, uthese, sizeof(compat_sigset_t)))
		return -EFAULT;
	sigset_from_compat(&amp;s, &amp;s32);

	if (uts) {
		if (compat_get_timespec(&amp;t, uts))
			return -EFAULT;
	}

	ret = do_sigtimedwait(&amp;s, &amp;info, uts ? &amp;t : NULL);

	if (ret &gt; 0 &amp;&amp; uinfo) {
		if (copy_siginfo_to_user32(uinfo, &amp;info))
			ret = -EFAULT;
	}

	return ret;
}

#ifdef __ARCH_WANT_COMPAT_SYS_TIME

*/ compat_time_t is a 32 bit &quot;long&quot; and needs to get converted. /*

COMPAT_SYSCALL_DEFINE1(time, compat_time_t __user, tloc)
{
	compat_time_t i;
	struct timeval tv;

	do_gettimeofday(&amp;tv);
	i = tv.tv_sec;

	if (tloc) {
		if (put_user(i,tloc))
			return -EFAULT;
	}
	force_successful_syscall_return();
	return i;
}

COMPAT_SYSCALL_DEFINE1(stime, compat_time_t __user, tptr)
{
	struct timespec tv;
	int err;

	if (get_user(tv.tv_sec, tptr))
		return -EFAULT;

	tv.tv_nsec = 0;

	err = security_settime(&amp;tv, NULL);
	if (err)
		return err;

	do_settimeofday(&amp;tv);
	return 0;
}

#endif */ __ARCH_WANT_COMPAT_SYS_TIME /*

COMPAT_SYSCALL_DEFINE1(adjtimex, struct compat_timex __user, utp)
{
	struct timex txc;
	int err, ret;

	err = compat_get_timex(&amp;txc, utp);
	if (err)
		return err;

	ret = do_adjtimex(&amp;txc);

	err = compat_put_timex(utp, &amp;txc);
	if (err)
		return err;

	return ret;
}

#ifdef CONFIG_NUMA
COMPAT_SYSCALL_DEFINE6(move_pages, pid_t, pid, compat_ulong_t, nr_pages,
		       compat_uptr_t __user, pages32,
		       const int __user, nodes,
		       int __user, status,
		       int, flags)
{
	const void __user __userpages;
	int i;

	pages = compat_alloc_user_space(nr_pages sizeof(void));
	for (i = 0; i &lt; nr_pages; i++) {
		compat_uptr_t p;

		if (get_user(p, pages32 + i) ||
			put_user(compat_ptr(p), pages + i))
			return -EFAULT;
	}
	return sys_move_pages(pid, nr_pages, pages, nodes, status, flags);
}

COMPAT_SYSCALL_DEFINE4(migrate_pages, compat_pid_t, pid,
		       compat_ulong_t, maxnode,
		       const compat_ulong_t __user, old_nodes,
		       const compat_ulong_t __user, new_nodes)
{
	unsigned long __userold = NULL;
	unsigned long __usernew = NULL;
	nodemask_t tmp_mask;
	unsigned long nr_bits;
	unsigned long size;

	nr_bits = min_t(unsigned long, maxnode - 1, MAX_NUMNODES);
	size = ALIGN(nr_bits, BITS_PER_LONG) / 8;
	if (old_nodes) {
		if (compat_get_bitmap(nodes_addr(tmp_mask), old_nodes, nr_bits))
			return -EFAULT;
		old = compat_alloc_user_space(new_nodes ? size 2 : size);
		if (new_nodes)
			new = old + size / sizeof(unsigned long);
		if (copy_to_user(old, nodes_addr(tmp_mask), size))
			return -EFAULT;
	}
	if (new_nodes) {
		if (compat_get_bitmap(nodes_addr(tmp_mask), new_nodes, nr_bits))
			return -EFAULT;
		if (new == NULL)
			new = compat_alloc_user_space(size);
		if (copy_to_user(new, nodes_addr(tmp_mask), size))
			return -EFAULT;
	}
	return sys_migrate_pages(pid, nr_bits + 1, old, new);
}
#endif

COMPAT_SYSCALL_DEFINE2(sched_rr_get_interval,
		       compat_pid_t, pid,
		       struct compat_timespec __user, interval)
{
	struct timespec t;
	int ret;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	ret = sys_sched_rr_get_interval(pid, (struct timespec __user)&amp;t);
	set_fs(old_fs);
	if (compat_put_timespec(&amp;t, interval))
		return -EFAULT;
	return ret;
}

*/
 Allocate user-space memory for the duration of a single system call,
 in order to marshall parameters inside a compat thunk.
 /*
void __usercompat_alloc_user_space(unsigned long len)
{
	void __userptr;

	*/ If len would occupy more than half of the entire compat space... /*
	if (unlikely(len &gt; (((compat_uptr_t)~0) &gt;&gt; 1)))
		return NULL;

	ptr = arch_compat_alloc_user_space(len);

	if (unlikely(!access_ok(VERIFY_WRITE, ptr, len)))
		return NULL;

	return ptr;
}
EXPORT_SYMBOL_GPL(compat_alloc_user_space);
*/
