
 Detect Hung Task

 kernel/hung_task.c - kernel thread for detecting tasks stuck in D state

 /*

#include &lt;linux/mm.h&gt;
#include &lt;linux/cpu.h&gt;
#include &lt;linux/nmi.h&gt;
#include &lt;linux/init.h&gt;
#include &lt;linux/delay.h&gt;
#include &lt;linux/freezer.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/lockdep.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/sysctl.h&gt;
#include &lt;linux/utsname.h&gt;
#include &lt;trace/events/sched.h&gt;

*/
 The number of tasks checked:
 /*
int __read_mostly sysctl_hung_task_check_count = PID_MAX_LIMIT;

*/
 Limit number of tasks checked in a batch.

 This value controls the preemptibility of khungtaskd since preemption
 is disabled during the critical section. It also controls the size of
 the RCU grace period. So it needs to be upper-bound.
 /*
#define HUNG_TASK_BATCHING 1024

*/
 Zero means infinite timeout - no checking done:
 /*
unsigned long __read_mostly sysctl_hung_task_timeout_secs = CONFIG_DEFAULT_HUNG_TASK_TIMEOUT;

int __read_mostly sysctl_hung_task_warnings = 10;

static int __read_mostly did_panic;

static struct task_structwatchdog_task;

*/
 Should we panic (and reboot, if panic_timeout= is set) when a
 hung task is detected:
 /*
unsigned int __read_mostly sysctl_hung_task_panic =
				CONFIG_BOOTPARAM_HUNG_TASK_PANIC_VALUE;

static int __init hung_task_panic_setup(charstr)
{
	int rc = kstrtouint(str, 0, &amp;sysctl_hung_task_panic);

	if (rc)
		return rc;
	return 1;
}
__setup(&quot;hung_task_panic=&quot;, hung_task_panic_setup);

static int
hung_task_panic(struct notifier_blockthis, unsigned long event, voidptr)
{
	did_panic = 1;

	return NOTIFY_DONE;
}

static struct notifier_block panic_block = {
	.notifier_call = hung_task_panic,
};

static void check_hung_task(struct task_structt, unsigned long timeout)
{
	unsigned long switch_count = t-&gt;nvcsw + t-&gt;nivcsw;

	*/
	 Ensure the task is not frozen.
	 Also, skip vfork and any other user process that freezer should skip.
	 /*
	if (unlikely(t-&gt;flags &amp; (PF_FROZEN | PF_FREEZER_SKIP)))
	    return;

	*/
	 When a freshly created task is scheduled once, changes its state to
	 TASK_UNINTERRUPTIBLE without having ever been switched out once, it
	 musn&#39;t be checked.
	 /*
	if (unlikely(!switch_count))
		return;

	if (switch_count != t-&gt;last_switch_count) {
		t-&gt;last_switch_count = switch_count;
		return;
	}

	trace_sched_process_hang(t);

	if (!sysctl_hung_task_warnings)
		return;

	if (sysctl_hung_task_warnings &gt; 0)
		sysctl_hung_task_warnings--;

	*/
	 Ok, the task did not get scheduled for more than 2 minutes,
	 complain:
	 /*
	pr_err(&quot;INFO: task %s:%d blocked for more than %ld seconds.\n&quot;,
		t-&gt;comm, t-&gt;pid, timeout);
	pr_err(&quot;      %s %s %.*s\n&quot;,
		print_tainted(), init_utsname()-&gt;release,
		(int)strcspn(init_utsname()-&gt;version, &quot; &quot;),
		init_utsname()-&gt;version);
	pr_err(&quot;\&quot;echo 0 &gt; /proc/sys/kernel/hung_task_timeout_secs\&quot;&quot;
		&quot; disables this message.\n&quot;);
	sched_show_task(t);
	debug_show_held_locks(t);

	touch_nmi_watchdog();

	if (sysctl_hung_task_panic) {
		trigger_all_cpu_backtrace();
		panic(&quot;hung_task: blocked tasks&quot;);
	}
}

*/
 To avoid extending the RCU grace period for an unbounded amount of time,
 periodically exit the critical section and enter a new one.

 For preemptible RCU it is sufficient to call rcu_read_unlock in order
 to exit the grace period. For classic RCU, a reschedule is required.
 /*
static bool rcu_lock_break(struct task_structg, struct task_structt)
{
	bool can_cont;

	get_task_struct(g);
	get_task_struct(t);
	rcu_read_unlock();
	cond_resched();
	rcu_read_lock();
	can_cont = pid_alive(g) &amp;&amp; pid_alive(t);
	put_task_struct(t);
	put_task_struct(g);

	return can_cont;
}

*/
 Check whether a TASK_UNINTERRUPTIBLE does not get woken up for
 a really long time (120 seconds). If that happens, print out
 a warning.
 /*
static void check_hung_uninterruptible_tasks(unsigned long timeout)
{
	int max_count = sysctl_hung_task_check_count;
	int batch_count = HUNG_TASK_BATCHING;
	struct task_structg,t;

	*/
	 If the system crashed already then all bets are off,
	 do not report extra hung tasks:
	 /*
	if (test_taint(TAINT_DIE) || did_panic)
		return;

	rcu_read_lock();
	for_each_process_thread(g, t) {
		if (!max_count--)
			goto unlock;
		if (!--batch_count) {
			batch_count = HUNG_TASK_BATCHING;
			if (!rcu_lock_break(g, t))
				goto unlock;
		}
		*/ use &quot;==&quot; to skip the TASK_KILLABLE tasks waiting on NFS /*
		if (t-&gt;state == TASK_UNINTERRUPTIBLE)
			check_hung_task(t, timeout);
	}
 unlock:
	rcu_read_unlock();
}

static long hung_timeout_jiffies(unsigned long last_checked,
				 unsigned long timeout)
{
	*/ timeout of 0 will disable the watchdog /*
	return timeout ? last_checked - jiffies + timeout HZ :
		MAX_SCHEDULE_TIMEOUT;
}

*/
 Process updating of timeout sysctl
 /*
int proc_dohung_task_timeout_secs(struct ctl_tabletable, int write,
				  void __userbuffer,
				  size_tlenp, loff_tppos)
{
	int ret;

	ret = proc_doulongvec_minmax(table, write, buffer, lenp, ppos);

	if (ret || !write)
		goto out;

	wake_up_process(watchdog_task);

 out:
	return ret;
}

static atomic_t reset_hung_task = ATOMIC_INIT(0);

void reset_hung_task_detector(void)
{
	atomic_set(&amp;reset_hung_task, 1);
}
EXPORT_SYMBOL_GPL(reset_hung_task_detector);

*/
 kthread which checks for tasks stuck in D state
 /*
static int watchdog(voiddummy)
{
	unsigned long hung_last_checked = jiffies;

	set_user_nice(current, 0);

	for ( ; ; ) {
		unsigned long timeout = sysctl_hung_task_timeout_secs;
		long t = hung_timeout_jiffies(hung_last_checked, timeout);

		if (t &lt;= 0) {
			if (!atomic_xchg(&amp;reset_hung_task, 0))
				check_hung_uninterruptible_tasks(timeout);
			hung_last_checked = jiffies;
			continue;
		}
		schedule_timeout_interruptible(t);
	}

	return 0;
}

static int __init hung_task_init(void)
{
	atomic_notifier_chain_register(&amp;panic_notifier_list, &amp;panic_block);
	watchdog_task = kthread_run(watchdog, NULL, &quot;khungtaskd&quot;);

	return 0;
}
subsys_initcall(hung_task_init);
*/
