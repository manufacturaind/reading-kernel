 audit_fsnotify.c -- tracking inodes

 Copyright 2003-2009,2014-2015 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 /*

#include &lt;linux/kernel.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/fsnotify_backend.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/netlink.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/security.h&gt;
#include &quot;audit.h&quot;

*/
 this mark lives on the parent directory of the inode in question.
 but dev, ino, and path are about the child
 /*
struct audit_fsnotify_mark {
	dev_t dev;		*/ associated superblock device /*
	unsigned long ino;	*/ associated inode number /*
	charpath;		*/ insertion path /*
	struct fsnotify_mark mark;/ fsnotify mark on the inode /*
	struct audit_krulerule;
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_fsnotify_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_EVENTS (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
			 FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_fsnotify_mark_free(struct audit_fsnotify_markaudit_mark)
{
	kfree(audit_mark-&gt;path);
	kfree(audit_mark);
}

static void audit_fsnotify_free_mark(struct fsnotify_markmark)
{
	struct audit_fsnotify_markaudit_mark;

	audit_mark = container_of(mark, struct audit_fsnotify_mark, mark);
	audit_fsnotify_mark_free(audit_mark);
}

charaudit_mark_path(struct audit_fsnotify_markmark)
{
	return mark-&gt;path;
}

int audit_mark_compare(struct audit_fsnotify_markmark, unsigned long ino, dev_t dev)
{
	if (mark-&gt;ino == AUDIT_INO_UNSET)
		return 0;
	return (mark-&gt;ino == ino) &amp;&amp; (mark-&gt;dev == dev);
}

static void audit_update_mark(struct audit_fsnotify_markaudit_mark,
			     struct inodeinode)
{
	audit_mark-&gt;dev = inode ? inode-&gt;i_sb-&gt;s_dev : AUDIT_DEV_UNSET;
	audit_mark-&gt;ino = inode ? inode-&gt;i_ino : AUDIT_INO_UNSET;
}

struct audit_fsnotify_markaudit_alloc_mark(struct audit_krulekrule, charpathname, int len)
{
	struct audit_fsnotify_markaudit_mark;
	struct path path;
	struct dentrydentry;
	struct inodeinode;
	int ret;

	if (pathname[0] != &#39;/&#39; || pathname[len-1] == &#39;/&#39;)
		return ERR_PTR(-EINVAL);

	dentry = kern_path_locked(pathname, &amp;path);
	if (IS_ERR(dentry))
		return (void)dentry;/ returning an error /*
	inode = path.dentry-&gt;d_inode;
	inode_unlock(inode);

	audit_mark = kzalloc(sizeof(*audit_mark), GFP_KERNEL);
	if (unlikely(!audit_mark)) {
		audit_mark = ERR_PTR(-ENOMEM);
		goto out;
	}

	fsnotify_init_mark(&amp;audit_mark-&gt;mark, audit_fsnotify_free_mark);
	audit_mark-&gt;mark.mask = AUDIT_FS_EVENTS;
	audit_mark-&gt;path = pathname;
	audit_update_mark(audit_mark, dentry-&gt;d_inode);
	audit_mark-&gt;rule = krule;

	ret = fsnotify_add_mark(&amp;audit_mark-&gt;mark, audit_fsnotify_group, inode, NULL, true);
	if (ret &lt; 0) {
		audit_fsnotify_mark_free(audit_mark);
		audit_mark = ERR_PTR(ret);
	}
out:
	dput(dentry);
	path_put(&amp;path);
	return audit_mark;
}

static void audit_mark_log_rule_change(struct audit_fsnotify_markaudit_mark, charop)
{
	struct audit_bufferab;
	struct audit_krulerule = audit_mark-&gt;rule;

	if (!audit_enabled)
		return;
	ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
	if (unlikely(!ab))
		return;
	audit_log_format(ab, &quot;auid=%u ses=%u op=&quot;,
			 from_kuid(&amp;init_user_ns, audit_get_loginuid(current)),
			 audit_get_sessionid(current));
	audit_log_string(ab, op);
	audit_log_format(ab, &quot; path=&quot;);
	audit_log_untrustedstring(ab, audit_mark-&gt;path);
	audit_log_key(ab, rule-&gt;filterkey);
	audit_log_format(ab, &quot; list=%d res=1&quot;, rule-&gt;listnr);
	audit_log_end(ab);
}

void audit_remove_mark(struct audit_fsnotify_markaudit_mark)
{
	fsnotify_destroy_mark(&amp;audit_mark-&gt;mark, audit_fsnotify_group);
	fsnotify_put_mark(&amp;audit_mark-&gt;mark);
}

void audit_remove_mark_rule(struct audit_krulekrule)
{
	struct audit_fsnotify_markmark = krule-&gt;exe;

	audit_remove_mark(mark);
}

static void audit_autoremove_mark_rule(struct audit_fsnotify_markaudit_mark)
{
	struct audit_krulerule = audit_mark-&gt;rule;
	struct audit_entryentry = container_of(rule, struct audit_entry, rule);

	audit_mark_log_rule_change(audit_mark, &quot;autoremove_rule&quot;);
	audit_del_rule(entry);
}

*/ Update mark data in audit rules based on fsnotify events. /*
static int audit_mark_handle_event(struct fsnotify_groupgroup,
				    struct inodeto_tell,
				    struct fsnotify_markinode_mark,
				    struct fsnotify_markvfsmount_mark,
				    u32 mask, voiddata, int data_type,
				    const unsigned chardname, u32 cookie)
{
	struct audit_fsnotify_markaudit_mark;
	struct inodeinode = NULL;

	audit_mark = container_of(inode_mark, struct audit_fsnotify_mark, mark);

	BUG_ON(group != audit_fsnotify_group);

	switch (data_type) {
	case (FSNOTIFY_EVENT_PATH):
		inode = ((struct path)data)-&gt;dentry-&gt;d_inode;
		break;
	case (FSNOTIFY_EVENT_INODE):
		inode = (struct inode)data;
		break;
	default:
		BUG();
		return 0;
	};

	if (mask &amp; (FS_CREATE|FS_MOVED_TO|FS_DELETE|FS_MOVED_FROM)) {
		if (audit_compare_dname_path(dname, audit_mark-&gt;path, AUDIT_NAME_FULL))
			return 0;
		audit_update_mark(audit_mark, inode);
	} else if (mask &amp; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
		audit_autoremove_mark_rule(audit_mark);

	return 0;
}

static const struct fsnotify_ops audit_mark_fsnotify_ops = {
	.handle_event =	audit_mark_handle_event,
};

static int __init audit_fsnotify_init(void)
{
	audit_fsnotify_group = fsnotify_alloc_group(&amp;audit_mark_fsnotify_ops);
	if (IS_ERR(audit_fsnotify_group)) {
		audit_fsnotify_group = NULL;
		audit_panic(&quot;cannot create audit fsnotify group&quot;);
	}
	return 0;
}
device_initcall(audit_fsnotify_init);
*/
