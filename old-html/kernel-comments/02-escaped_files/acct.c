
  linux/kernel/acct.c

  BSD Process Accounting for Linux

  Author: Marco van Wieringen &lt;mvw@planets.elm.net&gt;

  Some code based on ideas and code from:
  Thomas K. Dyas &lt;tdyas@eden.rutgers.edu&gt;

  This file implements BSD-style process accounting. Whenever any
  process exits, an accounting record of type &quot;struct acct&quot; is
  written to the file specified with the acct() system call. It is
  up to user-level programs to do useful things with the accounting
  log. The kernel just provides the raw accounting information.

 (C) Copyright 1995 - 1997 Marco van Wieringen - ELM Consultancy B.V.

  Plugged two leaks. 1) It didn&#39;t return acct_file into the free_filps if
  the file happened to be read-only. 2) If the accounting was suspended
  due to the lack of space it happily allowed to reopen it and completely
  lost the old acct_file. 3/10/98, Al Viro.

  Now we silently close acct_file on attempt to reopen. Cleaned sys_acct().
  XTerms and EMACS are manifestations of pure evil. 21/10/98, AV.

  Fixed a nasty interaction with with sys_umount(). If the accointing
  was suspeneded we failed to stop it on umount(). Messy.
  Another one: remount to readonly didn&#39;t stop accounting.
	Question: what should we do if we have CAP_SYS_ADMIN but not
  CAP_SYS_PACCT? Current code does the following: umount returns -EBUSY
  unless we are messing with the root. In that case we are getting a
  real mess with do_remount_sb(). 9/11/98, AV.

  Fixed a bunch of races (and pair of leaks). Probably not the best way,
  but this one obviously doesn&#39;t introduce deadlocks. Later. BTW, found
  one race (and leak) in BSD implementation.
  OK, that&#39;s better. ANOTHER race and leak in BSD variant. There always
  is one more bug... 10/11/98, AV.

	Oh, fsck... Oopsable SMP race in do_process_acct() - we must hold
 -&gt;mmap_sem to walk the vma list of current-&gt;mm. Nasty, since it leaks
 a struct file opened for write. Fixed. 2/6/2000, AV.
 /*

#include &lt;linux/mm.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/acct.h&gt;
#include &lt;linux/capability.h&gt;
#include &lt;linux/file.h&gt;
#include &lt;linux/tty.h&gt;
#include &lt;linux/security.h&gt;
#include &lt;linux/vfs.h&gt;
#include &lt;linux/jiffies.h&gt;
#include &lt;linux/times.h&gt;
#include &lt;linux/syscalls.h&gt;
#include &lt;linux/mount.h&gt;
#include &lt;linux/uaccess.h&gt;
#include &lt;asm/div64.h&gt;
#include &lt;linux/blkdev.h&gt;/ sector_div /*
#include &lt;linux/pid_namespace.h&gt;
#include &lt;linux/fs_pin.h&gt;

*/
 These constants control the amount of freespace that suspend and
 resume the process accounting system, and the time delay between
 each check.
 Turned into sysctl-controllable parameters. AV, 12/11/98
 /*

int acct_parm[3] = {4, 2, 30};
#define RESUME		(acct_parm[0])	*/ &gt;foo% free space - resume /*
#define SUSPEND		(acct_parm[1])	*/ &lt;foo% free space - suspend /*
#define ACCT_TIMEOUT	(acct_parm[2])	*/ foo second timeout between checks /*

*/
 External references and all of the globals.
 /*

struct bsd_acct_struct {
	struct fs_pin		pin;
	atomic_long_t		count;
	struct rcu_head		rcu;
	struct mutex		lock;
	int			active;
	unsigned long		needcheck;
	struct file		*file;
	struct pid_namespace	*ns;
	struct work_struct	work;
	struct completion	done;
};

static void do_acct_process(struct bsd_acct_structacct);

*/
 Check the amount of free space and suspend/resume accordingly.
 /*
static int check_free_space(struct bsd_acct_structacct)
{
	struct kstatfs sbuf;

	if (time_is_before_jiffies(acct-&gt;needcheck))
		goto out;

	*/ May block /*
	if (vfs_statfs(&amp;acct-&gt;file-&gt;f_path, &amp;sbuf))
		goto out;

	if (acct-&gt;active) {
		u64 suspend = sbuf.f_blocks SUSPEND;
		do_div(suspend, 100);
		if (sbuf.f_bavail &lt;= suspend) {
			acct-&gt;active = 0;
			pr_info(&quot;Process accounting paused\n&quot;);
		}
	} else {
		u64 resume = sbuf.f_blocks RESUME;
		do_div(resume, 100);
		if (sbuf.f_bavail &gt;= resume) {
			acct-&gt;active = 1;
			pr_info(&quot;Process accounting resumed\n&quot;);
		}
	}

	acct-&gt;needcheck = jiffies + ACCT_TIMEOUT*HZ;
out:
	return acct-&gt;active;
}

static void acct_put(struct bsd_acct_structp)
{
	if (atomic_long_dec_and_test(&amp;p-&gt;count))
		kfree_rcu(p, rcu);
}

static inline struct bsd_acct_structto_acct(struct fs_pinp)
{
	return p ? container_of(p, struct bsd_acct_struct, pin) : NULL;
}

static struct bsd_acct_structacct_get(struct pid_namespacens)
{
	struct bsd_acct_structres;
again:
	smp_rmb();
	rcu_read_lock();
	res = to_acct(ACCESS_ONCE(ns-&gt;bacct));
	if (!res) {
		rcu_read_unlock();
		return NULL;
	}
	if (!atomic_long_inc_not_zero(&amp;res-&gt;count)) {
		rcu_read_unlock();
		cpu_relax();
		goto again;
	}
	rcu_read_unlock();
	mutex_lock(&amp;res-&gt;lock);
	if (res != to_acct(ACCESS_ONCE(ns-&gt;bacct))) {
		mutex_unlock(&amp;res-&gt;lock);
		acct_put(res);
		goto again;
	}
	return res;
}

static void acct_pin_kill(struct fs_pinpin)
{
	struct bsd_acct_structacct = to_acct(pin);
	mutex_lock(&amp;acct-&gt;lock);
	do_acct_process(acct);
	schedule_work(&amp;acct-&gt;work);
	wait_for_completion(&amp;acct-&gt;done);
	cmpxchg(&amp;acct-&gt;ns-&gt;bacct, pin, NULL);
	mutex_unlock(&amp;acct-&gt;lock);
	pin_remove(pin);
	acct_put(acct);
}

static void close_work(struct work_structwork)
{
	struct bsd_acct_structacct = container_of(work, struct bsd_acct_struct, work);
	struct filefile = acct-&gt;file;
	if (file-&gt;f_op-&gt;flush)
		file-&gt;f_op-&gt;flush(file, NULL);
	__fput_sync(file);
	complete(&amp;acct-&gt;done);
}

static int acct_on(struct filenamepathname)
{
	struct filefile;
	struct vfsmountmnt,internal;
	struct pid_namespacens = task_active_pid_ns(current);
	struct bsd_acct_structacct;
	struct fs_pinold;
	int err;

	acct = kzalloc(sizeof(struct bsd_acct_struct), GFP_KERNEL);
	if (!acct)
		return -ENOMEM;

	*/ Difference from BSD - they don&#39;t do O_APPEND /*
	file = file_open_name(pathname, O_WRONLY|O_APPEND|O_LARGEFILE, 0);
	if (IS_ERR(file)) {
		kfree(acct);
		return PTR_ERR(file);
	}

	if (!S_ISREG(file_inode(file)-&gt;i_mode)) {
		kfree(acct);
		filp_close(file, NULL);
		return -EACCES;
	}

	if (!(file-&gt;f_mode &amp; FMODE_CAN_WRITE)) {
		kfree(acct);
		filp_close(file, NULL);
		return -EIO;
	}
	internal = mnt_clone_internal(&amp;file-&gt;f_path);
	if (IS_ERR(internal)) {
		kfree(acct);
		filp_close(file, NULL);
		return PTR_ERR(internal);
	}
	err = mnt_want_write(internal);
	if (err) {
		mntput(internal);
		kfree(acct);
		filp_close(file, NULL);
		return err;
	}
	mnt = file-&gt;f_path.mnt;
	file-&gt;f_path.mnt = internal;

	atomic_long_set(&amp;acct-&gt;count, 1);
	init_fs_pin(&amp;acct-&gt;pin, acct_pin_kill);
	acct-&gt;file = file;
	acct-&gt;needcheck = jiffies;
	acct-&gt;ns = ns;
	mutex_init(&amp;acct-&gt;lock);
	INIT_WORK(&amp;acct-&gt;work, close_work);
	init_completion(&amp;acct-&gt;done);
	mutex_lock_nested(&amp;acct-&gt;lock, 1);	*/ nobody has seen it yet /*
	pin_insert(&amp;acct-&gt;pin, mnt);

	rcu_read_lock();
	old = xchg(&amp;ns-&gt;bacct, &amp;acct-&gt;pin);
	mutex_unlock(&amp;acct-&gt;lock);
	pin_kill(old);
	mnt_drop_write(mnt);
	mntput(mnt);
	return 0;
}

static DEFINE_MUTEX(acct_on_mutex);

*/
 sys_acct - enable/disable process accounting
 @name: file name for accounting records or NULL to shutdown accounting

 Returns 0 for success or negative errno values for failure.

 sys_acct() is the only system call needed to implement process
 accounting. It takes the name of the file where accounting records
 should be written. If the filename is NULL, accounting will be
 shutdown.
 /*
SYSCALL_DEFINE1(acct, const char __user, name)
{
	int error = 0;

	if (!capable(CAP_SYS_PACCT))
		return -EPERM;

	if (name) {
		struct filenametmp = getname(name);

		if (IS_ERR(tmp))
			return PTR_ERR(tmp);
		mutex_lock(&amp;acct_on_mutex);
		error = acct_on(tmp);
		mutex_unlock(&amp;acct_on_mutex);
		putname(tmp);
	} else {
		rcu_read_lock();
		pin_kill(task_active_pid_ns(current)-&gt;bacct);
	}

	return error;
}

void acct_exit_ns(struct pid_namespacens)
{
	rcu_read_lock();
	pin_kill(ns-&gt;bacct);
}

*/
  encode an unsigned long into a comp_t

  This routine has been adopted from the encode_comp_t() function in
  the kern_acct.c file of the FreeBSD operating system. The encoding
  is a 13-bit fraction with a 3-bit (base 8) exponent.
 /*

#define	MANTSIZE	13			*/ 13 bit mantissa. /*
#define	EXPSIZE		3			*/ Base 8 (3 bit) exponent. /*
#define	MAXFRACT	((1 &lt;&lt; MANTSIZE) - 1)	*/ Maximum fractional value. /*

static comp_t encode_comp_t(unsigned long value)
{
	int exp, rnd;

	exp = rnd = 0;
	while (value &gt; MAXFRACT) {
		rnd = value &amp; (1 &lt;&lt; (EXPSIZE - 1));	*/ Round up? /*
		value &gt;&gt;= EXPSIZE;	*/ Base 8 exponent == 3 bit shift. /*
		exp++;
	}

	*/
	 If we need to round up, do it (and handle overflow correctly).
	 /*
	if (rnd &amp;&amp; (++value &gt; MAXFRACT)) {
		value &gt;&gt;= EXPSIZE;
		exp++;
	}

	*/
	 Clean it up and polish it off.
	 /*
	exp &lt;&lt;= MANTSIZE;		*/ Shift the exponent into place /*
	exp += value;			*/ and add on the mantissa. /*
	return exp;
}

#if ACCT_VERSION == 1 || ACCT_VERSION == 2
*/
 encode an u64 into a comp2_t (24 bits)

 Format: 5 bit base 2 exponent, 20 bits mantissa.
 The leading bit of the mantissa is not stored, but implied for
 non-zero exponents.
 Largest encodable value is 50 bits.
 /*

#define MANTSIZE2       20                     / 20 bit mantissa. /*
#define EXPSIZE2        5                      / 5 bit base 2 exponent. /*
#define MAXFRACT2       ((1ul &lt;&lt; MANTSIZE2) - 1)/ Maximum fractional value. /*
#define MAXEXP2         ((1 &lt;&lt; EXPSIZE2) - 1)   / Maximum exponent. /*

static comp2_t encode_comp2_t(u64 value)
{
	int exp, rnd;

	exp = (value &gt; (MAXFRACT2&gt;&gt;1));
	rnd = 0;
	while (value &gt; MAXFRACT2) {
		rnd = value &amp; 1;
		value &gt;&gt;= 1;
		exp++;
	}

	*/
	 If we need to round up, do it (and handle overflow correctly).
	 /*
	if (rnd &amp;&amp; (++value &gt; MAXFRACT2)) {
		value &gt;&gt;= 1;
		exp++;
	}

	if (exp &gt; MAXEXP2) {
		*/ Overflow. Return largest representable number instead. /*
		return (1ul &lt;&lt; (MANTSIZE2+EXPSIZE2-1)) - 1;
	} else {
		return (value &amp; (MAXFRACT2&gt;&gt;1)) | (exp &lt;&lt; (MANTSIZE2-1));
	}
}
#endif

#if ACCT_VERSION == 3
*/
 encode an u64 into a 32 bit IEEE float
 /*
static u32 encode_float(u64 value)
{
	unsigned exp = 190;
	unsigned u;

	if (value == 0)
		return 0;
	while ((s64)value &gt; 0) {
		value &lt;&lt;= 1;
		exp--;
	}
	u = (u32)(value &gt;&gt; 40) &amp; 0x7fffffu;
	return u | (exp &lt;&lt; 23);
}
#endif

*/
  Write an accounting entry for an exiting process

  The acct_process() call is the workhorse of the process
  accounting system. The struct acct is built here and then written
  into the accounting file. This function should only be called from
  do_exit() or when switching to a different output file.
 /*

static void fill_ac(acct_tac)
{
	struct pacct_structpacct = &amp;current-&gt;signal-&gt;pacct;
	u64 elapsed, run_time;
	struct tty_structtty;

	*/
	 Fill the accounting struct with the needed info as recorded
	 by the different kernel functions.
	 /*
	memset(ac, 0, sizeof(acct_t));

	ac-&gt;ac_version = ACCT_VERSION | ACCT_BYTEORDER;
	strlcpy(ac-&gt;ac_comm, current-&gt;comm, sizeof(ac-&gt;ac_comm));

	*/ calculate run_time in nsec/*
	run_time = ktime_get_ns();
	run_time -= current-&gt;group_leader-&gt;start_time;
	*/ convert nsec -&gt; AHZ /*
	elapsed = nsec_to_AHZ(run_time);
#if ACCT_VERSION == 3
	ac-&gt;ac_etime = encode_float(elapsed);
#else
	ac-&gt;ac_etime = encode_comp_t(elapsed &lt; (unsigned long) -1l ?
				(unsigned long) elapsed : (unsigned long) -1l);
#endif
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
	{
		*/ new enlarged etime field /*
		comp2_t etime = encode_comp2_t(elapsed);

		ac-&gt;ac_etime_hi = etime &gt;&gt; 16;
		ac-&gt;ac_etime_lo = (u16) etime;
	}
#endif
	do_div(elapsed, AHZ);
	ac-&gt;ac_btime = get_seconds() - elapsed;
#if ACCT_VERSION==2
	ac-&gt;ac_ahz = AHZ;
#endif

	spin_lock_irq(&amp;current-&gt;sighand-&gt;siglock);
	tty = current-&gt;signal-&gt;tty;	*/ Safe as we hold the siglock /*
	ac-&gt;ac_tty = tty ? old_encode_dev(tty_devnum(tty)) : 0;
	ac-&gt;ac_utime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&gt;ac_utime)));
	ac-&gt;ac_stime = encode_comp_t(jiffies_to_AHZ(cputime_to_jiffies(pacct-&gt;ac_stime)));
	ac-&gt;ac_flag = pacct-&gt;ac_flag;
	ac-&gt;ac_mem = encode_comp_t(pacct-&gt;ac_mem);
	ac-&gt;ac_minflt = encode_comp_t(pacct-&gt;ac_minflt);
	ac-&gt;ac_majflt = encode_comp_t(pacct-&gt;ac_majflt);
	ac-&gt;ac_exitcode = pacct-&gt;ac_exitcode;
	spin_unlock_irq(&amp;current-&gt;sighand-&gt;siglock);
}
*/
  do_acct_process does all actual work. Caller holds the reference to file.
 /*
static void do_acct_process(struct bsd_acct_structacct)
{
	acct_t ac;
	unsigned long flim;
	const struct credorig_cred;
	struct filefile = acct-&gt;file;

	*/
	 Accounting records are not subject to resource limits.
	 /*
	flim = current-&gt;signal-&gt;rlim[RLIMIT_FSIZE].rlim_cur;
	current-&gt;signal-&gt;rlim[RLIMIT_FSIZE].rlim_cur = RLIM_INFINITY;
	*/ Perform file operations on behalf of whoever enabled accounting /*
	orig_cred = override_creds(file-&gt;f_cred);

	*/
	 First check to see if there is enough free_space to continue
	 the process accounting system.
	 /*
	if (!check_free_space(acct))
		goto out;

	fill_ac(&amp;ac);
	*/ we really need to bite the bullet and change layout /*
	ac.ac_uid = from_kuid_munged(file-&gt;f_cred-&gt;user_ns, orig_cred-&gt;uid);
	ac.ac_gid = from_kgid_munged(file-&gt;f_cred-&gt;user_ns, orig_cred-&gt;gid);
#if ACCT_VERSION == 1 || ACCT_VERSION == 2
	*/ backward-compatible 16 bit fields /*
	ac.ac_uid16 = ac.ac_uid;
	ac.ac_gid16 = ac.ac_gid;
#endif
#if ACCT_VERSION == 3
	{
		struct pid_namespacens = acct-&gt;ns;

		ac.ac_pid = task_tgid_nr_ns(current, ns);
		rcu_read_lock();
		ac.ac_ppid = task_tgid_nr_ns(rcu_dereference(current-&gt;real_parent),
					     ns);
		rcu_read_unlock();
	}
#endif
	*/
	 Get freeze protection. If the fs is frozen, just skip the write
	 as we could deadlock the system otherwise.
	 /*
	if (file_start_write_trylock(file)) {
		*/ it&#39;s been opened O_APPEND, so position is irrelevant /*
		loff_t pos = 0;
		__kernel_write(file, (char)&amp;ac, sizeof(acct_t), &amp;pos);
		file_end_write(file);
	}
out:
	current-&gt;signal-&gt;rlim[RLIMIT_FSIZE].rlim_cur = flim;
	revert_creds(orig_cred);
}

*/
 acct_collect - collect accounting information into pacct_struct
 @exitcode: task exit code
 @group_dead: not 0, if this thread is the last one in the process.
 /*
void acct_collect(long exitcode, int group_dead)
{
	struct pacct_structpacct = &amp;current-&gt;signal-&gt;pacct;
	cputime_t utime, stime;
	unsigned long vsize = 0;

	if (group_dead &amp;&amp; current-&gt;mm) {
		struct vm_area_structvma;

		down_read(&amp;current-&gt;mm-&gt;mmap_sem);
		vma = current-&gt;mm-&gt;mmap;
		while (vma) {
			vsize += vma-&gt;vm_end - vma-&gt;vm_start;
			vma = vma-&gt;vm_next;
		}
		up_read(&amp;current-&gt;mm-&gt;mmap_sem);
	}

	spin_lock_irq(&amp;current-&gt;sighand-&gt;siglock);
	if (group_dead)
		pacct-&gt;ac_mem = vsize / 1024;
	if (thread_group_leader(current)) {
		pacct-&gt;ac_exitcode = exitcode;
		if (current-&gt;flags &amp; PF_FORKNOEXEC)
			pacct-&gt;ac_flag |= AFORK;
	}
	if (current-&gt;flags &amp; PF_SUPERPRIV)
		pacct-&gt;ac_flag |= ASU;
	if (current-&gt;flags &amp; PF_DUMPCORE)
		pacct-&gt;ac_flag |= ACORE;
	if (current-&gt;flags &amp; PF_SIGNALED)
		pacct-&gt;ac_flag |= AXSIG;
	task_cputime(current, &amp;utime, &amp;stime);
	pacct-&gt;ac_utime += utime;
	pacct-&gt;ac_stime += stime;
	pacct-&gt;ac_minflt += current-&gt;min_flt;
	pacct-&gt;ac_majflt += current-&gt;maj_flt;
	spin_unlock_irq(&amp;current-&gt;sighand-&gt;siglock);
}

static void slow_acct_process(struct pid_namespacens)
{
	for ( ; ns; ns = ns-&gt;parent) {
		struct bsd_acct_structacct = acct_get(ns);
		if (acct) {
			do_acct_process(acct);
			mutex_unlock(&amp;acct-&gt;lock);
			acct_put(acct);
		}
	}
}

*/
 acct_process

 handles process accounting for an exiting task
 /*
void acct_process(void)
{
	struct pid_namespacens;

	*/
	 This loop is safe lockless, since current is still
	 alive and holds its namespace, which in turn holds
	 its parent.
	 /*
	for (ns = task_active_pid_ns(current); ns != NULL; ns = ns-&gt;parent) {
		if (ns-&gt;bacct)
			break;
	}
	if (unlikely(ns))
		slow_acct_process(ns);
}
*/
