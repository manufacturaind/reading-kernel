/*
#define pr_fmt(fmt) &quot;kcov: &quot; fmt

#include &lt;linux/compiler.h&gt;
#include &lt;linux/types.h&gt;
#include &lt;linux/file.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/mm.h&gt;
#include &lt;linux/printk.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/spinlock.h&gt;
#include &lt;linux/vmalloc.h&gt;
#include &lt;linux/debugfs.h&gt;
#include &lt;linux/uaccess.h&gt;
#include &lt;linux/kcov.h&gt;

*/
 kcov descriptor (one per opened debugfs file).
 State transitions of the descriptor:
  - initial state after open()
  - then there must be a single ioctl(KCOV_INIT_TRACE) call
  - then, mmap() call (several calls are allowed but not useful)
  - then, repeated enable/disable for a task (only one task a time allowed)
 /*
struct kcov {
	*/
	 Reference counter. We keep one for:
	  - opened file descriptor
	  - task with enabled coverage (we can&#39;t unwire it from another task)
	 /*
	atomic_t		refcount;
	*/ The lock protects mode, size, area and t. /*
	spinlock_t		lock;
	enum kcov_mode		mode;
	*/ Size of arena (in long&#39;s for KCOV_MODE_TRACE). /*
	unsigned		size;
	*/ Coverage buffer shared with user space. /*
	void			*area;
	*/ Task for which we collect coverage, or NULL. /*
	struct task_struct	*t;
};

*/
 Entry point from instrumented code.
 This is called once per basic-block/edge.
 /*
void __sanitizer_cov_trace_pc(void)
{
	struct task_structt;
	enum kcov_mode mode;

	t = current;
	*/
	 We are interested in code coverage as a function of a syscall inputs,
	 so we ignore code executed in interrupts.
	 /*
	if (!t || in_interrupt())
		return;
	mode = READ_ONCE(t-&gt;kcov_mode);
	if (mode == KCOV_MODE_TRACE) {
		unsigned longarea;
		unsigned long pos;

		*/
		 There is some code that runs in interrupts but for which
		 in_interrupt() returns false (e.g. preempt_schedule_irq()).
		 READ_ONCE()/barrier() effectively provides load-acquire wrt
		 interrupts, there are paired barrier()/WRITE_ONCE() in
		 kcov_ioctl_locked().
		 /*
		barrier();
		area = t-&gt;kcov_area;
		*/ The first word is number of subsequent PCs. /*
		pos = READ_ONCE(area[0]) + 1;
		if (likely(pos &lt; t-&gt;kcov_size)) {
			area[pos] = _RET_IP_;
			WRITE_ONCE(area[0], pos);
		}
	}
}
EXPORT_SYMBOL(__sanitizer_cov_trace_pc);

static void kcov_get(struct kcovkcov)
{
	atomic_inc(&amp;kcov-&gt;refcount);
}

static void kcov_put(struct kcovkcov)
{
	if (atomic_dec_and_test(&amp;kcov-&gt;refcount)) {
		vfree(kcov-&gt;area);
		kfree(kcov);
	}
}

void kcov_task_init(struct task_structt)
{
	t-&gt;kcov_mode = KCOV_MODE_DISABLED;
	t-&gt;kcov_size = 0;
	t-&gt;kcov_area = NULL;
	t-&gt;kcov = NULL;
}

void kcov_task_exit(struct task_structt)
{
	struct kcovkcov;

	kcov = t-&gt;kcov;
	if (kcov == NULL)
		return;
	spin_lock(&amp;kcov-&gt;lock);
	if (WARN_ON(kcov-&gt;t != t)) {
		spin_unlock(&amp;kcov-&gt;lock);
		return;
	}
	*/ Just to not leave dangling references behind. /*
	kcov_task_init(t);
	kcov-&gt;t = NULL;
	spin_unlock(&amp;kcov-&gt;lock);
	kcov_put(kcov);
}

static int kcov_mmap(struct filefilep, struct vm_area_structvma)
{
	int res = 0;
	voidarea;
	struct kcovkcov = vma-&gt;vm_file-&gt;private_data;
	unsigned long size, off;
	struct pagepage;

	area = vmalloc_user(vma-&gt;vm_end - vma-&gt;vm_start);
	if (!area)
		return -ENOMEM;

	spin_lock(&amp;kcov-&gt;lock);
	size = kcov-&gt;size sizeof(unsigned long);
	if (kcov-&gt;mode == KCOV_MODE_DISABLED || vma-&gt;vm_pgoff != 0 ||
	    vma-&gt;vm_end - vma-&gt;vm_start != size) {
		res = -EINVAL;
		goto exit;
	}
	if (!kcov-&gt;area) {
		kcov-&gt;area = area;
		vma-&gt;vm_flags |= VM_DONTEXPAND;
		spin_unlock(&amp;kcov-&gt;lock);
		for (off = 0; off &lt; size; off += PAGE_SIZE) {
			page = vmalloc_to_page(kcov-&gt;area + off);
			if (vm_insert_page(vma, vma-&gt;vm_start + off, page))
				WARN_ONCE(1, &quot;vm_insert_page() failed&quot;);
		}
		return 0;
	}
exit:
	spin_unlock(&amp;kcov-&gt;lock);
	vfree(area);
	return res;
}

static int kcov_open(struct inodeinode, struct filefilep)
{
	struct kcovkcov;

	kcov = kzalloc(sizeof(*kcov), GFP_KERNEL);
	if (!kcov)
		return -ENOMEM;
	atomic_set(&amp;kcov-&gt;refcount, 1);
	spin_lock_init(&amp;kcov-&gt;lock);
	filep-&gt;private_data = kcov;
	return nonseekable_open(inode, filep);
}

static int kcov_close(struct inodeinode, struct filefilep)
{
	kcov_put(filep-&gt;private_data);
	return 0;
}

static int kcov_ioctl_locked(struct kcovkcov, unsigned int cmd,
			     unsigned long arg)
{
	struct task_structt;
	unsigned long size, unused;

	switch (cmd) {
	case KCOV_INIT_TRACE:
		*/
		 Enable kcov in trace mode and setup buffer size.
		 Must happen before anything else.
		 /*
		if (kcov-&gt;mode != KCOV_MODE_DISABLED)
			return -EBUSY;
		*/
		 Size must be at least 2 to hold current position and one PC.
		 Later we allocate size sizeof(unsigned long) memory,
		 that must not overflow.
		 /*
		size = arg;
		if (size &lt; 2 || size &gt; INT_MAX / sizeof(unsigned long))
			return -EINVAL;
		kcov-&gt;size = size;
		kcov-&gt;mode = KCOV_MODE_TRACE;
		return 0;
	case KCOV_ENABLE:
		*/
		 Enable coverage for the current task.
		 At this point user must have been enabled trace mode,
		 and mmapped the file. Coverage collection is disabled only
		 at task exit or voluntary by KCOV_DISABLE. After that it can
		 be enabled for another task.
		 /*
		unused = arg;
		if (unused != 0 || kcov-&gt;mode == KCOV_MODE_DISABLED ||
		    kcov-&gt;area == NULL)
			return -EINVAL;
		if (kcov-&gt;t != NULL)
			return -EBUSY;
		t = current;
		*/ Cache in task struct for performance. /*
		t-&gt;kcov_size = kcov-&gt;size;
		t-&gt;kcov_area = kcov-&gt;area;
		*/ See comment in __sanitizer_cov_trace_pc(). /*
		barrier();
		WRITE_ONCE(t-&gt;kcov_mode, kcov-&gt;mode);
		t-&gt;kcov = kcov;
		kcov-&gt;t = t;
		*/ This is put either in kcov_task_exit() or in KCOV_DISABLE. /*
		kcov_get(kcov);
		return 0;
	case KCOV_DISABLE:
		*/ Disable coverage for the current task. /*
		unused = arg;
		if (unused != 0 || current-&gt;kcov != kcov)
			return -EINVAL;
		t = current;
		if (WARN_ON(kcov-&gt;t != t))
			return -EINVAL;
		kcov_task_init(t);
		kcov-&gt;t = NULL;
		kcov_put(kcov);
		return 0;
	default:
		return -ENOTTY;
	}
}

static long kcov_ioctl(struct filefilep, unsigned int cmd, unsigned long arg)
{
	struct kcovkcov;
	int res;

	kcov = filep-&gt;private_data;
	spin_lock(&amp;kcov-&gt;lock);
	res = kcov_ioctl_locked(kcov, cmd, arg);
	spin_unlock(&amp;kcov-&gt;lock);
	return res;
}

static const struct file_operations kcov_fops = {
	.open		= kcov_open,
	.unlocked_ioctl	= kcov_ioctl,
	.mmap		= kcov_mmap,
	.release        = kcov_close,
};

static int __init kcov_init(void)
{
	if (!debugfs_create_file(&quot;kcov&quot;, 0600, NULL, NULL, &amp;kcov_fops)) {
		pr_err(&quot;failed to create kcov in debugfs\n&quot;);
		return -ENOMEM;
	}
	return 0;
}

device_initcall(kcov_init);
*/
