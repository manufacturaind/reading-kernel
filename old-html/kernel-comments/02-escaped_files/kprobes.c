
  Kernel Probes (KProbes)
  kernel/kprobes.c

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 Copyright (C) IBM Corporation, 2002, 2004

 2002-Oct	Created by Vamsi Krishna S &lt;vamsi_krishna@in.ibm.com&gt; Kernel
		Probes initial implementation (includes suggestions from
		Rusty Russell).
 2004-Aug	Updated by Prasanna S Panchamukhi &lt;prasanna@in.ibm.com&gt; with
		hlists and exceptions notifier as suggested by Andi Kleen.
 2004-July	Suparna Bhattacharya &lt;suparna@in.ibm.com&gt; added jumper probes
		interface to access function arguments.
 2004-Sep	Prasanna S Panchamukhi &lt;prasanna@in.ibm.com&gt; Changed Kprobes
		exceptions notifier to be first on the priority list.
 2005-May	Hien Nguyen &lt;hien@us.ibm.com&gt;, Jim Keniston
		&lt;jkenisto@us.ibm.com&gt; and Prasanna S Panchamukhi
		&lt;prasanna@in.ibm.com&gt; added function-return probes.
 /*
#include &lt;linux/kprobes.h&gt;
#include &lt;linux/hash.h&gt;
#include &lt;linux/init.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/stddef.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/moduleloader.h&gt;
#include &lt;linux/kallsyms.h&gt;
#include &lt;linux/freezer.h&gt;
#include &lt;linux/seq_file.h&gt;
#include &lt;linux/debugfs.h&gt;
#include &lt;linux/sysctl.h&gt;
#include &lt;linux/kdebug.h&gt;
#include &lt;linux/memory.h&gt;
#include &lt;linux/ftrace.h&gt;
#include &lt;linux/cpu.h&gt;
#include &lt;linux/jump_label.h&gt;

#include &lt;asm-generic/sections.h&gt;
#include &lt;asm/cacheflush.h&gt;
#include &lt;asm/errno.h&gt;
#include &lt;asm/uaccess.h&gt;

#define KPROBE_HASH_BITS 6
#define KPROBE_TABLE_SIZE (1 &lt;&lt; KPROBE_HASH_BITS)


*/
 Some oddball architectures like 64bit powerpc have function descriptors
 so this must be overridable.
 /*
#ifndef kprobe_lookup_name
#define kprobe_lookup_name(name, addr) \
	addr = ((kprobe_opcode_t)(kallsyms_lookup_name(name)))
#endif

static int kprobes_initialized;
static struct hlist_head kprobe_table[KPROBE_TABLE_SIZE];
static struct hlist_head kretprobe_inst_table[KPROBE_TABLE_SIZE];

*/ NOTE: change this value only with kprobe_mutex held /*
static bool kprobes_all_disarmed;

*/ This protects kprobe_table and optimizing_list /*
static DEFINE_MUTEX(kprobe_mutex);
static DEFINE_PER_CPU(struct kprobe, kprobe_instance) = NULL;
static struct {
	raw_spinlock_t lock ____cacheline_aligned_in_smp;
} kretprobe_table_locks[KPROBE_TABLE_SIZE];

static raw_spinlock_tkretprobe_table_lock_ptr(unsigned long hash)
{
	return &amp;(kretprobe_table_locks[hash].lock);
}

*/ Blacklist -- list of struct kprobe_blacklist_entry /*
static LIST_HEAD(kprobe_blacklist);

#ifdef __ARCH_WANT_KPROBES_INSN_SLOT
*/
 kprobe-&gt;ainsn.insn points to the copy of the instruction to be
 single-stepped. x86_64, POWER4 and above have no-exec support and
 stepping on the instruction on a vmalloced/kmalloced/data page
 is a recipe for disaster
 /*
struct kprobe_insn_page {
	struct list_head list;
	kprobe_opcode_tinsns;		*/ Page of instruction slots /*
	struct kprobe_insn_cachecache;
	int nused;
	int ngarbage;
	char slot_used[];
};

#define KPROBE_INSN_PAGE_SIZE(slots)			\
	(offsetof(struct kprobe_insn_page, slot_used) +	\
	 (sizeof(char) (slots)))

static int slots_per_page(struct kprobe_insn_cachec)
{
	return PAGE_SIZE/(c-&gt;insn_size sizeof(kprobe_opcode_t));
}

enum kprobe_slot_state {
	SLOT_CLEAN = 0,
	SLOT_DIRTY = 1,
	SLOT_USED = 2,
};

static voidalloc_insn_page(void)
{
	return module_alloc(PAGE_SIZE);
}

static void free_insn_page(voidpage)
{
	module_memfree(page);
}

struct kprobe_insn_cache kprobe_insn_slots = {
	.mutex = __MUTEX_INITIALIZER(kprobe_insn_slots.mutex),
	.alloc = alloc_insn_page,
	.free = free_insn_page,
	.pages = LIST_HEAD_INIT(kprobe_insn_slots.pages),
	.insn_size = MAX_INSN_SIZE,
	.nr_garbage = 0,
};
static int collect_garbage_slots(struct kprobe_insn_cachec);

*/
 __get_insn_slot() - Find a slot on an executable page for an instruction.
 We allocate an executable page if there&#39;s no room on existing ones.
 /*
kprobe_opcode_t__get_insn_slot(struct kprobe_insn_cachec)
{
	struct kprobe_insn_pagekip;
	kprobe_opcode_tslot = NULL;

	mutex_lock(&amp;c-&gt;mutex);
 retry:
	list_for_each_entry(kip, &amp;c-&gt;pages, list) {
		if (kip-&gt;nused &lt; slots_per_page(c)) {
			int i;
			for (i = 0; i &lt; slots_per_page(c); i++) {
				if (kip-&gt;slot_used[i] == SLOT_CLEAN) {
					kip-&gt;slot_used[i] = SLOT_USED;
					kip-&gt;nused++;
					slot = kip-&gt;insns + (i c-&gt;insn_size);
					goto out;
				}
			}
			*/ kip-&gt;nused is broken. Fix it. /*
			kip-&gt;nused = slots_per_page(c);
			WARN_ON(1);
		}
	}

	*/ If there are any garbage slots, collect it and try again. /*
	if (c-&gt;nr_garbage &amp;&amp; collect_garbage_slots(c) == 0)
		goto retry;

	*/ All out of space.  Need to allocate a new page. /*
	kip = kmalloc(KPROBE_INSN_PAGE_SIZE(slots_per_page(c)), GFP_KERNEL);
	if (!kip)
		goto out;

	*/
	 Use module_alloc so this page is within +/- 2GB of where the
	 kernel image and loaded module images reside. This is required
	 so x86_64 can correctly handle the %rip-relative fixups.
	 /*
	kip-&gt;insns = c-&gt;alloc();
	if (!kip-&gt;insns) {
		kfree(kip);
		goto out;
	}
	INIT_LIST_HEAD(&amp;kip-&gt;list);
	memset(kip-&gt;slot_used, SLOT_CLEAN, slots_per_page(c));
	kip-&gt;slot_used[0] = SLOT_USED;
	kip-&gt;nused = 1;
	kip-&gt;ngarbage = 0;
	kip-&gt;cache = c;
	list_add(&amp;kip-&gt;list, &amp;c-&gt;pages);
	slot = kip-&gt;insns;
out:
	mutex_unlock(&amp;c-&gt;mutex);
	return slot;
}

*/ Return 1 if all garbages are collected, otherwise 0. /*
static int collect_one_slot(struct kprobe_insn_pagekip, int idx)
{
	kip-&gt;slot_used[idx] = SLOT_CLEAN;
	kip-&gt;nused--;
	if (kip-&gt;nused == 0) {
		*/
		 Page is no longer in use.  Free it unless
		 it&#39;s the last one.  We keep the last one
		 so as not to have to set it up again the
		 next time somebody inserts a probe.
		 /*
		if (!list_is_singular(&amp;kip-&gt;list)) {
			list_del(&amp;kip-&gt;list);
			kip-&gt;cache-&gt;free(kip-&gt;insns);
			kfree(kip);
		}
		return 1;
	}
	return 0;
}

static int collect_garbage_slots(struct kprobe_insn_cachec)
{
	struct kprobe_insn_pagekip,next;

	*/ Ensure no-one is interrupted on the garbages /*
	synchronize_sched();

	list_for_each_entry_safe(kip, next, &amp;c-&gt;pages, list) {
		int i;
		if (kip-&gt;ngarbage == 0)
			continue;
		kip-&gt;ngarbage = 0;	*/ we will collect all garbages /*
		for (i = 0; i &lt; slots_per_page(c); i++) {
			if (kip-&gt;slot_used[i] == SLOT_DIRTY &amp;&amp;
			    collect_one_slot(kip, i))
				break;
		}
	}
	c-&gt;nr_garbage = 0;
	return 0;
}

void __free_insn_slot(struct kprobe_insn_cachec,
		      kprobe_opcode_tslot, int dirty)
{
	struct kprobe_insn_pagekip;

	mutex_lock(&amp;c-&gt;mutex);
	list_for_each_entry(kip, &amp;c-&gt;pages, list) {
		long idx = ((long)slot - (long)kip-&gt;insns) /
				(c-&gt;insn_size sizeof(kprobe_opcode_t));
		if (idx &gt;= 0 &amp;&amp; idx &lt; slots_per_page(c)) {
			WARN_ON(kip-&gt;slot_used[idx] != SLOT_USED);
			if (dirty) {
				kip-&gt;slot_used[idx] = SLOT_DIRTY;
				kip-&gt;ngarbage++;
				if (++c-&gt;nr_garbage &gt; slots_per_page(c))
					collect_garbage_slots(c);
			} else
				collect_one_slot(kip, idx);
			goto out;
		}
	}
	*/ Could not free this slot. /*
	WARN_ON(1);
out:
	mutex_unlock(&amp;c-&gt;mutex);
}

#ifdef CONFIG_OPTPROBES
*/ For optimized_kprobe buffer /*
struct kprobe_insn_cache kprobe_optinsn_slots = {
	.mutex = __MUTEX_INITIALIZER(kprobe_optinsn_slots.mutex),
	.alloc = alloc_insn_page,
	.free = free_insn_page,
	.pages = LIST_HEAD_INIT(kprobe_optinsn_slots.pages),
	*/ .insn_size is initialized later /*
	.nr_garbage = 0,
};
#endif
#endif

*/ We have preemption disabled.. so it is safe to use __ versions /*
static inline void set_kprobe_instance(struct kprobekp)
{
	__this_cpu_write(kprobe_instance, kp);
}

static inline void reset_kprobe_instance(void)
{
	__this_cpu_write(kprobe_instance, NULL);
}

*/
 This routine is called either:
 	- under the kprobe_mutex - during kprobe_[un]register()
 				OR
 	- with preemption disabled - from arch/xxx/kernel/kprobes.c
 /*
struct kprobeget_kprobe(voidaddr)
{
	struct hlist_headhead;
	struct kprobep;

	head = &amp;kprobe_table[hash_ptr(addr, KPROBE_HASH_BITS)];
	hlist_for_each_entry_rcu(p, head, hlist) {
		if (p-&gt;addr == addr)
			return p;
	}

	return NULL;
}
NOKPROBE_SYMBOL(get_kprobe);

static int aggr_pre_handler(struct kprobep, struct pt_regsregs);

*/ Return true if the kprobe is an aggregator /*
static inline int kprobe_aggrprobe(struct kprobep)
{
	return p-&gt;pre_handler == aggr_pre_handler;
}

*/ Return true(!0) if the kprobe is unused /*
static inline int kprobe_unused(struct kprobep)
{
	return kprobe_aggrprobe(p) &amp;&amp; kprobe_disabled(p) &amp;&amp;
	       list_empty(&amp;p-&gt;list);
}

*/
 Keep all fields in the kprobe consistent
 /*
static inline void copy_kprobe(struct kprobeap, struct kprobep)
{
	memcpy(&amp;p-&gt;opcode, &amp;ap-&gt;opcode, sizeof(kprobe_opcode_t));
	memcpy(&amp;p-&gt;ainsn, &amp;ap-&gt;ainsn, sizeof(struct arch_specific_insn));
}

#ifdef CONFIG_OPTPROBES
*/ NOTE: change this value only with kprobe_mutex held /*
static bool kprobes_allow_optimization;

*/
 Call all pre_handler on the list, but ignores its return value.
 This must be called from arch-dep optimized caller.
 /*
void opt_pre_handler(struct kprobep, struct pt_regsregs)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &amp;p-&gt;list, list) {
		if (kp-&gt;pre_handler &amp;&amp; likely(!kprobe_disabled(kp))) {
			set_kprobe_instance(kp);
			kp-&gt;pre_handler(kp, regs);
		}
		reset_kprobe_instance();
	}
}
NOKPROBE_SYMBOL(opt_pre_handler);

*/ Free optimized instructions and optimized_kprobe /*
static void free_aggr_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = container_of(p, struct optimized_kprobe, kp);
	arch_remove_optimized_kprobe(op);
	arch_remove_kprobe(p);
	kfree(op);
}

*/ Return true(!0) if the kprobe is ready for optimization. /*
static inline int kprobe_optready(struct kprobep)
{
	struct optimized_kprobeop;

	if (kprobe_aggrprobe(p)) {
		op = container_of(p, struct optimized_kprobe, kp);
		return arch_prepared_optinsn(&amp;op-&gt;optinsn);
	}

	return 0;
}

*/ Return true(!0) if the kprobe is disarmed. Note: p must be on hash list /*
static inline int kprobe_disarmed(struct kprobep)
{
	struct optimized_kprobeop;

	*/ If kprobe is not aggr/opt probe, just return kprobe is disabled /*
	if (!kprobe_aggrprobe(p))
		return kprobe_disabled(p);

	op = container_of(p, struct optimized_kprobe, kp);

	return kprobe_disabled(p) &amp;&amp; list_empty(&amp;op-&gt;list);
}

*/ Return true(!0) if the probe is queued on (un)optimizing lists /*
static int kprobe_queued(struct kprobep)
{
	struct optimized_kprobeop;

	if (kprobe_aggrprobe(p)) {
		op = container_of(p, struct optimized_kprobe, kp);
		if (!list_empty(&amp;op-&gt;list))
			return 1;
	}
	return 0;
}

*/
 Return an optimized kprobe whose optimizing code replaces
 instructions including addr (exclude breakpoint).
 /*
static struct kprobeget_optimized_kprobe(unsigned long addr)
{
	int i;
	struct kprobep = NULL;
	struct optimized_kprobeop;

	*/ Don&#39;t check i == 0, since that is a breakpoint case. /*
	for (i = 1; !p &amp;&amp; i &lt; MAX_OPTIMIZED_LENGTH; i++)
		p = get_kprobe((void)(addr - i));

	if (p &amp;&amp; kprobe_optready(p)) {
		op = container_of(p, struct optimized_kprobe, kp);
		if (arch_within_optimized_kprobe(op, addr))
			return p;
	}

	return NULL;
}

*/ Optimization staging list, protected by kprobe_mutex /*
static LIST_HEAD(optimizing_list);
static LIST_HEAD(unoptimizing_list);
static LIST_HEAD(freeing_list);

static void kprobe_optimizer(struct work_structwork);
static DECLARE_DELAYED_WORK(optimizing_work, kprobe_optimizer);
#define OPTIMIZE_DELAY 5

*/
 Optimize (replace a breakpoint with a jump) kprobes listed on
 optimizing_list.
 /*
static void do_optimize_kprobes(void)
{
	*/ Optimization never be done when disarmed /*
	if (kprobes_all_disarmed || !kprobes_allow_optimization ||
	    list_empty(&amp;optimizing_list))
		return;

	*/
	 The optimization/unoptimization refers online_cpus via
	 stop_machine() and cpu-hotplug modifies online_cpus.
	 And same time, text_mutex will be held in cpu-hotplug and here.
	 This combination can cause a deadlock (cpu-hotplug try to lock
	 text_mutex but stop_machine can not be done because online_cpus
	 has been changed)
	 To avoid this deadlock, we need to call get_online_cpus()
	 for preventing cpu-hotplug outside of text_mutex locking.
	 /*
	get_online_cpus();
	mutex_lock(&amp;text_mutex);
	arch_optimize_kprobes(&amp;optimizing_list);
	mutex_unlock(&amp;text_mutex);
	put_online_cpus();
}

*/
 Unoptimize (replace a jump with a breakpoint and remove the breakpoint
 if need) kprobes listed on unoptimizing_list.
 /*
static void do_unoptimize_kprobes(void)
{
	struct optimized_kprobeop,tmp;

	*/ Unoptimization must be done anytime /*
	if (list_empty(&amp;unoptimizing_list))
		return;

	*/ Ditto to do_optimize_kprobes /*
	get_online_cpus();
	mutex_lock(&amp;text_mutex);
	arch_unoptimize_kprobes(&amp;unoptimizing_list, &amp;freeing_list);
	*/ Loop free_list for disarming /*
	list_for_each_entry_safe(op, tmp, &amp;freeing_list, list) {
		*/ Disarm probes if marked disabled /*
		if (kprobe_disabled(&amp;op-&gt;kp))
			arch_disarm_kprobe(&amp;op-&gt;kp);
		if (kprobe_unused(&amp;op-&gt;kp)) {
			*/
			 Remove unused probes from hash list. After waiting
			 for synchronization, these probes are reclaimed.
			 (reclaiming is done by do_free_cleaned_kprobes.)
			 /*
			hlist_del_rcu(&amp;op-&gt;kp.hlist);
		} else
			list_del_init(&amp;op-&gt;list);
	}
	mutex_unlock(&amp;text_mutex);
	put_online_cpus();
}

*/ Reclaim all kprobes on the free_list /*
static void do_free_cleaned_kprobes(void)
{
	struct optimized_kprobeop,tmp;

	list_for_each_entry_safe(op, tmp, &amp;freeing_list, list) {
		BUG_ON(!kprobe_unused(&amp;op-&gt;kp));
		list_del_init(&amp;op-&gt;list);
		free_aggr_kprobe(&amp;op-&gt;kp);
	}
}

*/ Start optimizer after OPTIMIZE_DELAY passed /*
static void kick_kprobe_optimizer(void)
{
	schedule_delayed_work(&amp;optimizing_work, OPTIMIZE_DELAY);
}

*/ Kprobe jump optimizer /*
static void kprobe_optimizer(struct work_structwork)
{
	mutex_lock(&amp;kprobe_mutex);
	*/ Lock modules while optimizing kprobes /*
	mutex_lock(&amp;module_mutex);

	*/
	 Step 1: Unoptimize kprobes and collect cleaned (unused and disarmed)
	 kprobes before waiting for quiesence period.
	 /*
	do_unoptimize_kprobes();

	*/
	 Step 2: Wait for quiesence period to ensure all running interrupts
	 are done. Because optprobe may modify multiple instructions
	 there is a chance that Nth instruction is interrupted. In that
	 case, running interrupt can return to 2nd-Nth byte of jump
	 instruction. This wait is for avoiding it.
	 /*
	synchronize_sched();

	*/ Step 3: Optimize kprobes after quiesence period /*
	do_optimize_kprobes();

	*/ Step 4: Free cleaned kprobes after quiesence period /*
	do_free_cleaned_kprobes();

	mutex_unlock(&amp;module_mutex);
	mutex_unlock(&amp;kprobe_mutex);

	*/ Step 5: Kick optimizer again if needed /*
	if (!list_empty(&amp;optimizing_list) || !list_empty(&amp;unoptimizing_list))
		kick_kprobe_optimizer();
}

*/ Wait for completing optimization and unoptimization /*
static void wait_for_kprobe_optimizer(void)
{
	mutex_lock(&amp;kprobe_mutex);

	while (!list_empty(&amp;optimizing_list) || !list_empty(&amp;unoptimizing_list)) {
		mutex_unlock(&amp;kprobe_mutex);

		*/ this will also make optimizing_work execute immmediately /*
		flush_delayed_work(&amp;optimizing_work);
		*/ @optimizing_work might not have been queued yet, relax /*
		cpu_relax();

		mutex_lock(&amp;kprobe_mutex);
	}

	mutex_unlock(&amp;kprobe_mutex);
}

*/ Optimize kprobe if p is ready to be optimized /*
static void optimize_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	*/ Check if the kprobe is disabled or not ready for optimization. /*
	if (!kprobe_optready(p) || !kprobes_allow_optimization ||
	    (kprobe_disabled(p) || kprobes_all_disarmed))
		return;

	*/ Both of break_handler and post_handler are not supported. /*
	if (p-&gt;break_handler || p-&gt;post_handler)
		return;

	op = container_of(p, struct optimized_kprobe, kp);

	*/ Check there is no other kprobes at the optimized instructions /*
	if (arch_check_optimized_kprobe(op) &lt; 0)
		return;

	*/ Check if it is already optimized. /*
	if (op-&gt;kp.flags &amp; KPROBE_FLAG_OPTIMIZED)
		return;
	op-&gt;kp.flags |= KPROBE_FLAG_OPTIMIZED;

	if (!list_empty(&amp;op-&gt;list))
		*/ This is under unoptimizing. Just dequeue the probe /*
		list_del_init(&amp;op-&gt;list);
	else {
		list_add(&amp;op-&gt;list, &amp;optimizing_list);
		kick_kprobe_optimizer();
	}
}

*/ Short cut to direct unoptimizing /*
static void force_unoptimize_kprobe(struct optimized_kprobeop)
{
	get_online_cpus();
	arch_unoptimize_kprobe(op);
	put_online_cpus();
	if (kprobe_disabled(&amp;op-&gt;kp))
		arch_disarm_kprobe(&amp;op-&gt;kp);
}

*/ Unoptimize a kprobe if p is optimized /*
static void unoptimize_kprobe(struct kprobep, bool force)
{
	struct optimized_kprobeop;

	if (!kprobe_aggrprobe(p) || kprobe_disarmed(p))
		return;/ This is not an optprobe nor optimized /*

	op = container_of(p, struct optimized_kprobe, kp);
	if (!kprobe_optimized(p)) {
		*/ Unoptimized or unoptimizing case /*
		if (force &amp;&amp; !list_empty(&amp;op-&gt;list)) {
			*/
			 Only if this is unoptimizing kprobe and forced,
			 forcibly unoptimize it. (No need to unoptimize
			 unoptimized kprobe again :)
			 /*
			list_del_init(&amp;op-&gt;list);
			force_unoptimize_kprobe(op);
		}
		return;
	}

	op-&gt;kp.flags &amp;= ~KPROBE_FLAG_OPTIMIZED;
	if (!list_empty(&amp;op-&gt;list)) {
		*/ Dequeue from the optimization queue /*
		list_del_init(&amp;op-&gt;list);
		return;
	}
	*/ Optimized kprobe case /*
	if (force)
		*/ Forcibly update the code: this is a special case /*
		force_unoptimize_kprobe(op);
	else {
		list_add(&amp;op-&gt;list, &amp;unoptimizing_list);
		kick_kprobe_optimizer();
	}
}

*/ Cancel unoptimizing for reusing /*
static void reuse_unused_kprobe(struct kprobeap)
{
	struct optimized_kprobeop;

	BUG_ON(!kprobe_unused(ap));
	*/
	 Unused kprobe MUST be on the way of delayed unoptimizing (means
	 there is still a relative jump) and disabled.
	 /*
	op = container_of(ap, struct optimized_kprobe, kp);
	if (unlikely(list_empty(&amp;op-&gt;list)))
		printk(KERN_WARNING &quot;Warning: found a stray unused &quot;
			&quot;aggrprobe@%p\n&quot;, ap-&gt;addr);
	*/ Enable the probe again /*
	ap-&gt;flags &amp;= ~KPROBE_FLAG_DISABLED;
	*/ Optimize it again (remove from op-&gt;list) /*
	BUG_ON(!kprobe_optready(ap));
	optimize_kprobe(ap);
}

*/ Remove optimized instructions /*
static void kill_optimized_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = container_of(p, struct optimized_kprobe, kp);
	if (!list_empty(&amp;op-&gt;list))
		*/ Dequeue from the (un)optimization queue /*
		list_del_init(&amp;op-&gt;list);
	op-&gt;kp.flags &amp;= ~KPROBE_FLAG_OPTIMIZED;

	if (kprobe_unused(p)) {
		*/ Enqueue if it is unused /*
		list_add(&amp;op-&gt;list, &amp;freeing_list);
		*/
		 Remove unused probes from the hash list. After waiting
		 for synchronization, this probe is reclaimed.
		 (reclaiming is done by do_free_cleaned_kprobes().)
		 /*
		hlist_del_rcu(&amp;op-&gt;kp.hlist);
	}

	*/ Don&#39;t touch the code, because it is already freed. /*
	arch_remove_optimized_kprobe(op);
}

*/ Try to prepare optimized instructions /*
static void prepare_optimized_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = container_of(p, struct optimized_kprobe, kp);
	arch_prepare_optimized_kprobe(op, p);
}

*/ Allocate new optimized_kprobe and try to prepare optimized instructions /*
static struct kprobealloc_aggr_kprobe(struct kprobep)
{
	struct optimized_kprobeop;

	op = kzalloc(sizeof(struct optimized_kprobe), GFP_KERNEL);
	if (!op)
		return NULL;

	INIT_LIST_HEAD(&amp;op-&gt;list);
	op-&gt;kp.addr = p-&gt;addr;
	arch_prepare_optimized_kprobe(op, p);

	return &amp;op-&gt;kp;
}

static void init_aggr_kprobe(struct kprobeap, struct kprobep);

*/
 Prepare an optimized_kprobe and optimize it
 NOTE: p must be a normal registered kprobe
 /*
static void try_to_optimize_kprobe(struct kprobep)
{
	struct kprobeap;
	struct optimized_kprobeop;

	*/ Impossible to optimize ftrace-based kprobe /*
	if (kprobe_ftrace(p))
		return;

	*/ For preparing optimization, jump_label_text_reserved() is called /*
	jump_label_lock();
	mutex_lock(&amp;text_mutex);

	ap = alloc_aggr_kprobe(p);
	if (!ap)
		goto out;

	op = container_of(ap, struct optimized_kprobe, kp);
	if (!arch_prepared_optinsn(&amp;op-&gt;optinsn)) {
		*/ If failed to setup optimizing, fallback to kprobe /*
		arch_remove_optimized_kprobe(op);
		kfree(op);
		goto out;
	}

	init_aggr_kprobe(ap, p);
	optimize_kprobe(ap);	*/ This just kicks optimizer thread /*

out:
	mutex_unlock(&amp;text_mutex);
	jump_label_unlock();
}

#ifdef CONFIG_SYSCTL
static void optimize_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&amp;kprobe_mutex);
	*/ If optimization is already allowed, just return /*
	if (kprobes_allow_optimization)
		goto out;

	kprobes_allow_optimization = true;
	for (i = 0; i &lt; KPROBE_TABLE_SIZE; i++) {
		head = &amp;kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist)
			if (!kprobe_disabled(p))
				optimize_kprobe(p);
	}
	printk(KERN_INFO &quot;Kprobes globally optimized\n&quot;);
out:
	mutex_unlock(&amp;kprobe_mutex);
}

static void unoptimize_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&amp;kprobe_mutex);
	*/ If optimization is already prohibited, just return /*
	if (!kprobes_allow_optimization) {
		mutex_unlock(&amp;kprobe_mutex);
		return;
	}

	kprobes_allow_optimization = false;
	for (i = 0; i &lt; KPROBE_TABLE_SIZE; i++) {
		head = &amp;kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist) {
			if (!kprobe_disabled(p))
				unoptimize_kprobe(p, false);
		}
	}
	mutex_unlock(&amp;kprobe_mutex);

	*/ Wait for unoptimizing completion /*
	wait_for_kprobe_optimizer();
	printk(KERN_INFO &quot;Kprobes globally unoptimized\n&quot;);
}

static DEFINE_MUTEX(kprobe_sysctl_mutex);
int sysctl_kprobes_optimization;
int proc_kprobes_optimization_handler(struct ctl_tabletable, int write,
				      void __userbuffer, size_tlength,
				      loff_tppos)
{
	int ret;

	mutex_lock(&amp;kprobe_sysctl_mutex);
	sysctl_kprobes_optimization = kprobes_allow_optimization ? 1 : 0;
	ret = proc_dointvec_minmax(table, write, buffer, length, ppos);

	if (sysctl_kprobes_optimization)
		optimize_all_kprobes();
	else
		unoptimize_all_kprobes();
	mutex_unlock(&amp;kprobe_sysctl_mutex);

	return ret;
}
#endif */ CONFIG_SYSCTL /*

*/ Put a breakpoint for a probe. Must be called with text_mutex locked /*
static void __arm_kprobe(struct kprobep)
{
	struct kprobe_p;

	*/ Check collision with other optimized kprobes /*
	_p = get_optimized_kprobe((unsigned long)p-&gt;addr);
	if (unlikely(_p))
		*/ Fallback to unoptimized kprobe /*
		unoptimize_kprobe(_p, true);

	arch_arm_kprobe(p);
	optimize_kprobe(p);	*/ Try to optimize (add kprobe to a list) /*
}

*/ Remove the breakpoint of a probe. Must be called with text_mutex locked /*
static void __disarm_kprobe(struct kprobep, bool reopt)
{
	struct kprobe_p;

	*/ Try to unoptimize /*
	unoptimize_kprobe(p, kprobes_all_disarmed);

	if (!kprobe_queued(p)) {
		arch_disarm_kprobe(p);
		*/ If another kprobe was blocked, optimize it. /*
		_p = get_optimized_kprobe((unsigned long)p-&gt;addr);
		if (unlikely(_p) &amp;&amp; reopt)
			optimize_kprobe(_p);
	}
	*/ TODO: reoptimize others after unoptimized this probe /*
}

#else */ !CONFIG_OPTPROBES /*

#define optimize_kprobe(p)			do {} while (0)
#define unoptimize_kprobe(p, f)			do {} while (0)
#define kill_optimized_kprobe(p)		do {} while (0)
#define prepare_optimized_kprobe(p)		do {} while (0)
#define try_to_optimize_kprobe(p)		do {} while (0)
#define __arm_kprobe(p)				arch_arm_kprobe(p)
#define __disarm_kprobe(p, o)			arch_disarm_kprobe(p)
#define kprobe_disarmed(p)			kprobe_disabled(p)
#define wait_for_kprobe_optimizer()		do {} while (0)

*/ There should be no unused kprobes can be reused without optimization /*
static void reuse_unused_kprobe(struct kprobeap)
{
	printk(KERN_ERR &quot;Error: There should be no unused kprobe here.\n&quot;);
	BUG_ON(kprobe_unused(ap));
}

static void free_aggr_kprobe(struct kprobep)
{
	arch_remove_kprobe(p);
	kfree(p);
}

static struct kprobealloc_aggr_kprobe(struct kprobep)
{
	return kzalloc(sizeof(struct kprobe), GFP_KERNEL);
}
#endif */ CONFIG_OPTPROBES /*

#ifdef CONFIG_KPROBES_ON_FTRACE
static struct ftrace_ops kprobe_ftrace_ops __read_mostly = {
	.func = kprobe_ftrace_handler,
	.flags = FTRACE_OPS_FL_SAVE_REGS | FTRACE_OPS_FL_IPMODIFY,
};
static int kprobe_ftrace_enabled;

*/ Must ensure p-&gt;addr is really on ftrace /*
static int prepare_kprobe(struct kprobep)
{
	if (!kprobe_ftrace(p))
		return arch_prepare_kprobe(p);

	return arch_prepare_kprobe_ftrace(p);
}

*/ Caller must lock kprobe_mutex /*
static void arm_kprobe_ftrace(struct kprobep)
{
	int ret;

	ret = ftrace_set_filter_ip(&amp;kprobe_ftrace_ops,
				   (unsigned long)p-&gt;addr, 0, 0);
	WARN(ret &lt; 0, &quot;Failed to arm kprobe-ftrace at %p (%d)\n&quot;, p-&gt;addr, ret);
	kprobe_ftrace_enabled++;
	if (kprobe_ftrace_enabled == 1) {
		ret = register_ftrace_function(&amp;kprobe_ftrace_ops);
		WARN(ret &lt; 0, &quot;Failed to init kprobe-ftrace (%d)\n&quot;, ret);
	}
}

*/ Caller must lock kprobe_mutex /*
static void disarm_kprobe_ftrace(struct kprobep)
{
	int ret;

	kprobe_ftrace_enabled--;
	if (kprobe_ftrace_enabled == 0) {
		ret = unregister_ftrace_function(&amp;kprobe_ftrace_ops);
		WARN(ret &lt; 0, &quot;Failed to init kprobe-ftrace (%d)\n&quot;, ret);
	}
	ret = ftrace_set_filter_ip(&amp;kprobe_ftrace_ops,
			   (unsigned long)p-&gt;addr, 1, 0);
	WARN(ret &lt; 0, &quot;Failed to disarm kprobe-ftrace at %p (%d)\n&quot;, p-&gt;addr, ret);
}
#else	*/ !CONFIG_KPROBES_ON_FTRACE /*
#define prepare_kprobe(p)	arch_prepare_kprobe(p)
#define arm_kprobe_ftrace(p)	do {} while (0)
#define disarm_kprobe_ftrace(p)	do {} while (0)
#endif

*/ Arm a kprobe with text_mutex /*
static void arm_kprobe(struct kprobekp)
{
	if (unlikely(kprobe_ftrace(kp))) {
		arm_kprobe_ftrace(kp);
		return;
	}
	*/
	 Here, since __arm_kprobe() doesn&#39;t use stop_machine(),
	 this doesn&#39;t cause deadlock on text_mutex. So, we don&#39;t
	 need get_online_cpus().
	 /*
	mutex_lock(&amp;text_mutex);
	__arm_kprobe(kp);
	mutex_unlock(&amp;text_mutex);
}

*/ Disarm a kprobe with text_mutex /*
static void disarm_kprobe(struct kprobekp, bool reopt)
{
	if (unlikely(kprobe_ftrace(kp))) {
		disarm_kprobe_ftrace(kp);
		return;
	}
	*/ Ditto /*
	mutex_lock(&amp;text_mutex);
	__disarm_kprobe(kp, reopt);
	mutex_unlock(&amp;text_mutex);
}

*/
 Aggregate handlers for multiple kprobes support - these handlers
 take care of invoking the individual kprobe handlers on p-&gt;list
 /*
static int aggr_pre_handler(struct kprobep, struct pt_regsregs)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &amp;p-&gt;list, list) {
		if (kp-&gt;pre_handler &amp;&amp; likely(!kprobe_disabled(kp))) {
			set_kprobe_instance(kp);
			if (kp-&gt;pre_handler(kp, regs))
				return 1;
		}
		reset_kprobe_instance();
	}
	return 0;
}
NOKPROBE_SYMBOL(aggr_pre_handler);

static void aggr_post_handler(struct kprobep, struct pt_regsregs,
			      unsigned long flags)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &amp;p-&gt;list, list) {
		if (kp-&gt;post_handler &amp;&amp; likely(!kprobe_disabled(kp))) {
			set_kprobe_instance(kp);
			kp-&gt;post_handler(kp, regs, flags);
			reset_kprobe_instance();
		}
	}
}
NOKPROBE_SYMBOL(aggr_post_handler);

static int aggr_fault_handler(struct kprobep, struct pt_regsregs,
			      int trapnr)
{
	struct kprobecur = __this_cpu_read(kprobe_instance);

	*/
	 if we faulted &quot;during&quot; the execution of a user specified
	 probe handler, invoke just that probe&#39;s fault handler
	 /*
	if (cur &amp;&amp; cur-&gt;fault_handler) {
		if (cur-&gt;fault_handler(cur, regs, trapnr))
			return 1;
	}
	return 0;
}
NOKPROBE_SYMBOL(aggr_fault_handler);

static int aggr_break_handler(struct kprobep, struct pt_regsregs)
{
	struct kprobecur = __this_cpu_read(kprobe_instance);
	int ret = 0;

	if (cur &amp;&amp; cur-&gt;break_handler) {
		if (cur-&gt;break_handler(cur, regs))
			ret = 1;
	}
	reset_kprobe_instance();
	return ret;
}
NOKPROBE_SYMBOL(aggr_break_handler);

*/ Walks the list and increments nmissed count for multiprobe case /*
void kprobes_inc_nmissed_count(struct kprobep)
{
	struct kprobekp;
	if (!kprobe_aggrprobe(p)) {
		p-&gt;nmissed++;
	} else {
		list_for_each_entry_rcu(kp, &amp;p-&gt;list, list)
			kp-&gt;nmissed++;
	}
	return;
}
NOKPROBE_SYMBOL(kprobes_inc_nmissed_count);

void recycle_rp_inst(struct kretprobe_instanceri,
		     struct hlist_headhead)
{
	struct kretproberp = ri-&gt;rp;

	*/ remove rp inst off the rprobe_inst_table /*
	hlist_del(&amp;ri-&gt;hlist);
	INIT_HLIST_NODE(&amp;ri-&gt;hlist);
	if (likely(rp)) {
		raw_spin_lock(&amp;rp-&gt;lock);
		hlist_add_head(&amp;ri-&gt;hlist, &amp;rp-&gt;free_instances);
		raw_spin_unlock(&amp;rp-&gt;lock);
	} else
		*/ Unregistering /*
		hlist_add_head(&amp;ri-&gt;hlist, head);
}
NOKPROBE_SYMBOL(recycle_rp_inst);

void kretprobe_hash_lock(struct task_structtsk,
			 struct hlist_head*head, unsigned longflags)
__acquires(hlist_lock)
{
	unsigned long hash = hash_ptr(tsk, KPROBE_HASH_BITS);
	raw_spinlock_thlist_lock;

	*head = &amp;kretprobe_inst_table[hash];
	hlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_lock_irqsave(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_hash_lock);

static void kretprobe_table_lock(unsigned long hash,
				 unsigned longflags)
__acquires(hlist_lock)
{
	raw_spinlock_thlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_lock_irqsave(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_table_lock);

void kretprobe_hash_unlock(struct task_structtsk,
			   unsigned longflags)
__releases(hlist_lock)
{
	unsigned long hash = hash_ptr(tsk, KPROBE_HASH_BITS);
	raw_spinlock_thlist_lock;

	hlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_unlock_irqrestore(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_hash_unlock);

static void kretprobe_table_unlock(unsigned long hash,
				   unsigned longflags)
__releases(hlist_lock)
{
	raw_spinlock_thlist_lock = kretprobe_table_lock_ptr(hash);
	raw_spin_unlock_irqrestore(hlist_lock,flags);
}
NOKPROBE_SYMBOL(kretprobe_table_unlock);

*/
 This function is called from finish_task_switch when task tk becomes dead,
 so that we can recycle any function-return probe instances associated
 with this task. These left over instances represent probed functions
 that have been called but will never return.
 /*
void kprobe_flush_task(struct task_structtk)
{
	struct kretprobe_instanceri;
	struct hlist_headhead, empty_rp;
	struct hlist_nodetmp;
	unsigned long hash, flags = 0;

	if (unlikely(!kprobes_initialized))
		*/ Early boot.  kretprobe_table_locks not yet initialized. /*
		return;

	INIT_HLIST_HEAD(&amp;empty_rp);
	hash = hash_ptr(tk, KPROBE_HASH_BITS);
	head = &amp;kretprobe_inst_table[hash];
	kretprobe_table_lock(hash, &amp;flags);
	hlist_for_each_entry_safe(ri, tmp, head, hlist) {
		if (ri-&gt;task == tk)
			recycle_rp_inst(ri, &amp;empty_rp);
	}
	kretprobe_table_unlock(hash, &amp;flags);
	hlist_for_each_entry_safe(ri, tmp, &amp;empty_rp, hlist) {
		hlist_del(&amp;ri-&gt;hlist);
		kfree(ri);
	}
}
NOKPROBE_SYMBOL(kprobe_flush_task);

static inline void free_rp_inst(struct kretproberp)
{
	struct kretprobe_instanceri;
	struct hlist_nodenext;

	hlist_for_each_entry_safe(ri, next, &amp;rp-&gt;free_instances, hlist) {
		hlist_del(&amp;ri-&gt;hlist);
		kfree(ri);
	}
}

static void cleanup_rp_inst(struct kretproberp)
{
	unsigned long flags, hash;
	struct kretprobe_instanceri;
	struct hlist_nodenext;
	struct hlist_headhead;

	*/ No race here /*
	for (hash = 0; hash &lt; KPROBE_TABLE_SIZE; hash++) {
		kretprobe_table_lock(hash, &amp;flags);
		head = &amp;kretprobe_inst_table[hash];
		hlist_for_each_entry_safe(ri, next, head, hlist) {
			if (ri-&gt;rp == rp)
				ri-&gt;rp = NULL;
		}
		kretprobe_table_unlock(hash, &amp;flags);
	}
	free_rp_inst(rp);
}
NOKPROBE_SYMBOL(cleanup_rp_inst);

*/
* Add the new probe to ap-&gt;list. Fail if this is the
* second jprobe at the address - two jprobes can&#39;t coexist
/*
static int add_new_kprobe(struct kprobeap, struct kprobep)
{
	BUG_ON(kprobe_gone(ap) || kprobe_gone(p));

	if (p-&gt;break_handler || p-&gt;post_handler)
		unoptimize_kprobe(ap, true);	*/ Fall back to normal kprobe /*

	if (p-&gt;break_handler) {
		if (ap-&gt;break_handler)
			return -EEXIST;
		list_add_tail_rcu(&amp;p-&gt;list, &amp;ap-&gt;list);
		ap-&gt;break_handler = aggr_break_handler;
	} else
		list_add_rcu(&amp;p-&gt;list, &amp;ap-&gt;list);
	if (p-&gt;post_handler &amp;&amp; !ap-&gt;post_handler)
		ap-&gt;post_handler = aggr_post_handler;

	return 0;
}

*/
 Fill in the required fields of the &quot;manager kprobe&quot;. Replace the
 earlier kprobe in the hlist with the manager kprobe
 /*
static void init_aggr_kprobe(struct kprobeap, struct kprobep)
{
	*/ Copy p&#39;s insn slot to ap /*
	copy_kprobe(p, ap);
	flush_insn_slot(ap);
	ap-&gt;addr = p-&gt;addr;
	ap-&gt;flags = p-&gt;flags &amp; ~KPROBE_FLAG_OPTIMIZED;
	ap-&gt;pre_handler = aggr_pre_handler;
	ap-&gt;fault_handler = aggr_fault_handler;
	*/ We don&#39;t care the kprobe which has gone. /*
	if (p-&gt;post_handler &amp;&amp; !kprobe_gone(p))
		ap-&gt;post_handler = aggr_post_handler;
	if (p-&gt;break_handler &amp;&amp; !kprobe_gone(p))
		ap-&gt;break_handler = aggr_break_handler;

	INIT_LIST_HEAD(&amp;ap-&gt;list);
	INIT_HLIST_NODE(&amp;ap-&gt;hlist);

	list_add_rcu(&amp;p-&gt;list, &amp;ap-&gt;list);
	hlist_replace_rcu(&amp;p-&gt;hlist, &amp;ap-&gt;hlist);
}

*/
 This is the second or subsequent kprobe at the address - handle
 the intricacies
 /*
static int register_aggr_kprobe(struct kprobeorig_p, struct kprobep)
{
	int ret = 0;
	struct kprobeap = orig_p;

	*/ For preparing optimization, jump_label_text_reserved() is called /*
	jump_label_lock();
	*/
	 Get online CPUs to avoid text_mutex deadlock.with stop machine,
	 which is invoked by unoptimize_kprobe() in add_new_kprobe()
	 /*
	get_online_cpus();
	mutex_lock(&amp;text_mutex);

	if (!kprobe_aggrprobe(orig_p)) {
		*/ If orig_p is not an aggr_kprobe, create new aggr_kprobe. /*
		ap = alloc_aggr_kprobe(orig_p);
		if (!ap) {
			ret = -ENOMEM;
			goto out;
		}
		init_aggr_kprobe(ap, orig_p);
	} else if (kprobe_unused(ap))
		*/ This probe is going to die. Rescue it /*
		reuse_unused_kprobe(ap);

	if (kprobe_gone(ap)) {
		*/
		 Attempting to insert new probe at the same location that
		 had a probe in the module vaddr area which already
		 freed. So, the instruction slot has already been
		 released. We need a new slot for the new probe.
		 /*
		ret = arch_prepare_kprobe(ap);
		if (ret)
			*/
			 Even if fail to allocate new slot, don&#39;t need to
			 free aggr_probe. It will be used next time, or
			 freed by unregister_kprobe.
			 /*
			goto out;

		*/ Prepare optimized instructions if possible. /*
		prepare_optimized_kprobe(ap);

		*/
		 Clear gone flag to prevent allocating new slot again, and
		 set disabled flag because it is not armed yet.
		 /*
		ap-&gt;flags = (ap-&gt;flags &amp; ~KPROBE_FLAG_GONE)
			    | KPROBE_FLAG_DISABLED;
	}

	*/ Copy ap&#39;s insn slot to p /*
	copy_kprobe(ap, p);
	ret = add_new_kprobe(ap, p);

out:
	mutex_unlock(&amp;text_mutex);
	put_online_cpus();
	jump_label_unlock();

	if (ret == 0 &amp;&amp; kprobe_disabled(ap) &amp;&amp; !kprobe_disabled(p)) {
		ap-&gt;flags &amp;= ~KPROBE_FLAG_DISABLED;
		if (!kprobes_all_disarmed)
			*/ Arm the breakpoint again. /*
			arm_kprobe(ap);
	}
	return ret;
}

bool __weak arch_within_kprobe_blacklist(unsigned long addr)
{
	*/ The __kprobes marked functions and entry code must not be probed /*
	return addr &gt;= (unsigned long)__kprobes_text_start &amp;&amp;
	       addr &lt; (unsigned long)__kprobes_text_end;
}

bool within_kprobe_blacklist(unsigned long addr)
{
	struct kprobe_blacklist_entryent;

	if (arch_within_kprobe_blacklist(addr))
		return true;
	*/
	 If there exists a kprobe_blacklist, verify and
	 fail any probe registration in the prohibited area
	 /*
	list_for_each_entry(ent, &amp;kprobe_blacklist, list) {
		if (addr &gt;= ent-&gt;start_addr &amp;&amp; addr &lt; ent-&gt;end_addr)
			return true;
	}

	return false;
}

*/
 If we have a symbol_name argument, look it up and add the offset field
 to it. This way, we can specify a relative address to a symbol.
 This returns encoded errors if it fails to look up symbol or invalid
 combination of parameters.
 /*
static kprobe_opcode_tkprobe_addr(struct kprobep)
{
	kprobe_opcode_taddr = p-&gt;addr;

	if ((p-&gt;symbol_name &amp;&amp; p-&gt;addr) ||
	    (!p-&gt;symbol_name &amp;&amp; !p-&gt;addr))
		goto invalid;

	if (p-&gt;symbol_name) {
		kprobe_lookup_name(p-&gt;symbol_name, addr);
		if (!addr)
			return ERR_PTR(-ENOENT);
	}

	addr = (kprobe_opcode_t)(((char)addr) + p-&gt;offset);
	if (addr)
		return addr;

invalid:
	return ERR_PTR(-EINVAL);
}

*/ Check passed kprobe is valid and return kprobe in kprobe_table. /*
static struct kprobe__get_valid_kprobe(struct kprobep)
{
	struct kprobeap,list_p;

	ap = get_kprobe(p-&gt;addr);
	if (unlikely(!ap))
		return NULL;

	if (p != ap) {
		list_for_each_entry_rcu(list_p, &amp;ap-&gt;list, list)
			if (list_p == p)
			*/ kprobe p is a valid probe /*
				goto valid;
		return NULL;
	}
valid:
	return ap;
}

*/ Return error if the kprobe is being re-registered /*
static inline int check_kprobe_rereg(struct kprobep)
{
	int ret = 0;

	mutex_lock(&amp;kprobe_mutex);
	if (__get_valid_kprobe(p))
		ret = -EINVAL;
	mutex_unlock(&amp;kprobe_mutex);

	return ret;
}

int __weak arch_check_ftrace_location(struct kprobep)
{
	unsigned long ftrace_addr;

	ftrace_addr = ftrace_location((unsigned long)p-&gt;addr);
	if (ftrace_addr) {
#ifdef CONFIG_KPROBES_ON_FTRACE
		*/ Given address is not on the instruction boundary /*
		if ((unsigned long)p-&gt;addr != ftrace_addr)
			return -EILSEQ;
		p-&gt;flags |= KPROBE_FLAG_FTRACE;
#else	*/ !CONFIG_KPROBES_ON_FTRACE /*
		return -EINVAL;
#endif
	}
	return 0;
}

static int check_kprobe_address_safe(struct kprobep,
				     struct module*probed_mod)
{
	int ret;

	ret = arch_check_ftrace_location(p);
	if (ret)
		return ret;
	jump_label_lock();
	preempt_disable();

	*/ Ensure it is not in reserved area nor out of text /*
	if (!kernel_text_address((unsigned long) p-&gt;addr) ||
	    within_kprobe_blacklist((unsigned long) p-&gt;addr) ||
	    jump_label_text_reserved(p-&gt;addr, p-&gt;addr)) {
		ret = -EINVAL;
		goto out;
	}

	*/ Check if are we probing a module /*
	*probed_mod = __module_text_address((unsigned long) p-&gt;addr);
	if (*probed_mod) {
		*/
		 We must hold a refcount of the probed module while updating
		 its code to prohibit unexpected unloading.
		 /*
		if (unlikely(!try_module_get(*probed_mod))) {
			ret = -ENOENT;
			goto out;
		}

		*/
		 If the module freed .init.text, we couldn&#39;t insert
		 kprobes in there.
		 /*
		if (within_module_init((unsigned long)p-&gt;addr,probed_mod) &amp;&amp;
		    (*probed_mod)-&gt;state != MODULE_STATE_COMING) {
			module_put(*probed_mod);
			*probed_mod = NULL;
			ret = -ENOENT;
		}
	}
out:
	preempt_enable();
	jump_label_unlock();

	return ret;
}

int register_kprobe(struct kprobep)
{
	int ret;
	struct kprobeold_p;
	struct moduleprobed_mod;
	kprobe_opcode_taddr;

	*/ Adjust probe address from symbol /*
	addr = kprobe_addr(p);
	if (IS_ERR(addr))
		return PTR_ERR(addr);
	p-&gt;addr = addr;

	ret = check_kprobe_rereg(p);
	if (ret)
		return ret;

	*/ User can pass only KPROBE_FLAG_DISABLED to register_kprobe /*
	p-&gt;flags &amp;= KPROBE_FLAG_DISABLED;
	p-&gt;nmissed = 0;
	INIT_LIST_HEAD(&amp;p-&gt;list);

	ret = check_kprobe_address_safe(p, &amp;probed_mod);
	if (ret)
		return ret;

	mutex_lock(&amp;kprobe_mutex);

	old_p = get_kprobe(p-&gt;addr);
	if (old_p) {
		*/ Since this may unoptimize old_p, locking text_mutex. /*
		ret = register_aggr_kprobe(old_p, p);
		goto out;
	}

	mutex_lock(&amp;text_mutex);	*/ Avoiding text modification /*
	ret = prepare_kprobe(p);
	mutex_unlock(&amp;text_mutex);
	if (ret)
		goto out;

	INIT_HLIST_NODE(&amp;p-&gt;hlist);
	hlist_add_head_rcu(&amp;p-&gt;hlist,
		       &amp;kprobe_table[hash_ptr(p-&gt;addr, KPROBE_HASH_BITS)]);

	if (!kprobes_all_disarmed &amp;&amp; !kprobe_disabled(p))
		arm_kprobe(p);

	*/ Try to optimize kprobe /*
	try_to_optimize_kprobe(p);

out:
	mutex_unlock(&amp;kprobe_mutex);

	if (probed_mod)
		module_put(probed_mod);

	return ret;
}
EXPORT_SYMBOL_GPL(register_kprobe);

*/ Check if all probes on the aggrprobe are disabled /*
static int aggr_kprobe_disabled(struct kprobeap)
{
	struct kprobekp;

	list_for_each_entry_rcu(kp, &amp;ap-&gt;list, list)
		if (!kprobe_disabled(kp))
			*/
			 There is an active probe on the list.
			 We can&#39;t disable this ap.
			 /*
			return 0;

	return 1;
}

*/ Disable one kprobe: Make sure called under kprobe_mutex is locked /*
static struct kprobe__disable_kprobe(struct kprobep)
{
	struct kprobeorig_p;

	*/ Get an original kprobe for return /*
	orig_p = __get_valid_kprobe(p);
	if (unlikely(orig_p == NULL))
		return NULL;

	if (!kprobe_disabled(p)) {
		*/ Disable probe if it is a child probe /*
		if (p != orig_p)
			p-&gt;flags |= KPROBE_FLAG_DISABLED;

		*/ Try to disarm and disable this/parent probe /*
		if (p == orig_p || aggr_kprobe_disabled(orig_p)) {
			*/
			 If kprobes_all_disarmed is set, orig_p
			 should have already been disarmed, so
			 skip unneed disarming process.
			 /*
			if (!kprobes_all_disarmed)
				disarm_kprobe(orig_p, true);
			orig_p-&gt;flags |= KPROBE_FLAG_DISABLED;
		}
	}

	return orig_p;
}

*/
 Unregister a kprobe without a scheduler synchronization.
 /*
static int __unregister_kprobe_top(struct kprobep)
{
	struct kprobeap,list_p;

	*/ Disable kprobe. This will disarm it if needed. /*
	ap = __disable_kprobe(p);
	if (ap == NULL)
		return -EINVAL;

	if (ap == p)
		*/
		 This probe is an independent(and non-optimized) kprobe
		 (not an aggrprobe). Remove from the hash list.
		 /*
		goto disarmed;

	*/ Following process expects this probe is an aggrprobe /*
	WARN_ON(!kprobe_aggrprobe(ap));

	if (list_is_singular(&amp;ap-&gt;list) &amp;&amp; kprobe_disarmed(ap))
		*/
		 !disarmed could be happen if the probe is under delayed
		 unoptimizing.
		 /*
		goto disarmed;
	else {
		*/ If disabling probe has special handlers, update aggrprobe /*
		if (p-&gt;break_handler &amp;&amp; !kprobe_gone(p))
			ap-&gt;break_handler = NULL;
		if (p-&gt;post_handler &amp;&amp; !kprobe_gone(p)) {
			list_for_each_entry_rcu(list_p, &amp;ap-&gt;list, list) {
				if ((list_p != p) &amp;&amp; (list_p-&gt;post_handler))
					goto noclean;
			}
			ap-&gt;post_handler = NULL;
		}
noclean:
		*/
		 Remove from the aggrprobe: this path will do nothing in
		 __unregister_kprobe_bottom().
		 /*
		list_del_rcu(&amp;p-&gt;list);
		if (!kprobe_disabled(ap) &amp;&amp; !kprobes_all_disarmed)
			*/
			 Try to optimize this probe again, because post
			 handler may have been changed.
			 /*
			optimize_kprobe(ap);
	}
	return 0;

disarmed:
	BUG_ON(!kprobe_disarmed(ap));
	hlist_del_rcu(&amp;ap-&gt;hlist);
	return 0;
}

static void __unregister_kprobe_bottom(struct kprobep)
{
	struct kprobeap;

	if (list_empty(&amp;p-&gt;list))
		*/ This is an independent kprobe /*
		arch_remove_kprobe(p);
	else if (list_is_singular(&amp;p-&gt;list)) {
		*/ This is the last child of an aggrprobe /*
		ap = list_entry(p-&gt;list.next, struct kprobe, list);
		list_del(&amp;p-&gt;list);
		free_aggr_kprobe(ap);
	}
	*/ Otherwise, do nothing. /*
}

int register_kprobes(struct kprobe*kps, int num)
{
	int i, ret = 0;

	if (num &lt;= 0)
		return -EINVAL;
	for (i = 0; i &lt; num; i++) {
		ret = register_kprobe(kps[i]);
		if (ret &lt; 0) {
			if (i &gt; 0)
				unregister_kprobes(kps, i);
			break;
		}
	}
	return ret;
}
EXPORT_SYMBOL_GPL(register_kprobes);

void unregister_kprobe(struct kprobep)
{
	unregister_kprobes(&amp;p, 1);
}
EXPORT_SYMBOL_GPL(unregister_kprobe);

void unregister_kprobes(struct kprobe*kps, int num)
{
	int i;

	if (num &lt;= 0)
		return;
	mutex_lock(&amp;kprobe_mutex);
	for (i = 0; i &lt; num; i++)
		if (__unregister_kprobe_top(kps[i]) &lt; 0)
			kps[i]-&gt;addr = NULL;
	mutex_unlock(&amp;kprobe_mutex);

	synchronize_sched();
	for (i = 0; i &lt; num; i++)
		if (kps[i]-&gt;addr)
			__unregister_kprobe_bottom(kps[i]);
}
EXPORT_SYMBOL_GPL(unregister_kprobes);

static struct notifier_block kprobe_exceptions_nb = {
	.notifier_call = kprobe_exceptions_notify,
	.priority = 0x7fffffff/ we need to be notified first /*
};

unsigned long __weak arch_deref_entry_point(voidentry)
{
	return (unsigned long)entry;
}

int register_jprobes(struct jprobe*jps, int num)
{
	struct jprobejp;
	int ret = 0, i;

	if (num &lt;= 0)
		return -EINVAL;
	for (i = 0; i &lt; num; i++) {
		unsigned long addr, offset;
		jp = jps[i];
		addr = arch_deref_entry_point(jp-&gt;entry);

		*/ Verify probepoint is a function entry point /*
		if (kallsyms_lookup_size_offset(addr, NULL, &amp;offset) &amp;&amp;
		    offset == 0) {
			jp-&gt;kp.pre_handler = setjmp_pre_handler;
			jp-&gt;kp.break_handler = longjmp_break_handler;
			ret = register_kprobe(&amp;jp-&gt;kp);
		} else
			ret = -EINVAL;

		if (ret &lt; 0) {
			if (i &gt; 0)
				unregister_jprobes(jps, i);
			break;
		}
	}
	return ret;
}
EXPORT_SYMBOL_GPL(register_jprobes);

int register_jprobe(struct jprobejp)
{
	return register_jprobes(&amp;jp, 1);
}
EXPORT_SYMBOL_GPL(register_jprobe);

void unregister_jprobe(struct jprobejp)
{
	unregister_jprobes(&amp;jp, 1);
}
EXPORT_SYMBOL_GPL(unregister_jprobe);

void unregister_jprobes(struct jprobe*jps, int num)
{
	int i;

	if (num &lt;= 0)
		return;
	mutex_lock(&amp;kprobe_mutex);
	for (i = 0; i &lt; num; i++)
		if (__unregister_kprobe_top(&amp;jps[i]-&gt;kp) &lt; 0)
			jps[i]-&gt;kp.addr = NULL;
	mutex_unlock(&amp;kprobe_mutex);

	synchronize_sched();
	for (i = 0; i &lt; num; i++) {
		if (jps[i]-&gt;kp.addr)
			__unregister_kprobe_bottom(&amp;jps[i]-&gt;kp);
	}
}
EXPORT_SYMBOL_GPL(unregister_jprobes);

#ifdef CONFIG_KRETPROBES
*/
 This kprobe pre_handler is registered with every kretprobe. When probe
 hits it will set up the return probe.
 /*
static int pre_handler_kretprobe(struct kprobep, struct pt_regsregs)
{
	struct kretproberp = container_of(p, struct kretprobe, kp);
	unsigned long hash, flags = 0;
	struct kretprobe_instanceri;

	*/
	 To avoid deadlocks, prohibit return probing in NMI contexts,
	 just skip the probe and increase the (inexact) &#39;nmissed&#39;
	 statistical counter, so that the user is informed that
	 something happened:
	 /*
	if (unlikely(in_nmi())) {
		rp-&gt;nmissed++;
		return 0;
	}

	*/ TODO: consider to only swap the RA after the last pre_handler fired /*
	hash = hash_ptr(current, KPROBE_HASH_BITS);
	raw_spin_lock_irqsave(&amp;rp-&gt;lock, flags);
	if (!hlist_empty(&amp;rp-&gt;free_instances)) {
		ri = hlist_entry(rp-&gt;free_instances.first,
				struct kretprobe_instance, hlist);
		hlist_del(&amp;ri-&gt;hlist);
		raw_spin_unlock_irqrestore(&amp;rp-&gt;lock, flags);

		ri-&gt;rp = rp;
		ri-&gt;task = current;

		if (rp-&gt;entry_handler &amp;&amp; rp-&gt;entry_handler(ri, regs)) {
			raw_spin_lock_irqsave(&amp;rp-&gt;lock, flags);
			hlist_add_head(&amp;ri-&gt;hlist, &amp;rp-&gt;free_instances);
			raw_spin_unlock_irqrestore(&amp;rp-&gt;lock, flags);
			return 0;
		}

		arch_prepare_kretprobe(ri, regs);

		*/ XXX(hch): why is there no hlist_move_head? /*
		INIT_HLIST_NODE(&amp;ri-&gt;hlist);
		kretprobe_table_lock(hash, &amp;flags);
		hlist_add_head(&amp;ri-&gt;hlist, &amp;kretprobe_inst_table[hash]);
		kretprobe_table_unlock(hash, &amp;flags);
	} else {
		rp-&gt;nmissed++;
		raw_spin_unlock_irqrestore(&amp;rp-&gt;lock, flags);
	}
	return 0;
}
NOKPROBE_SYMBOL(pre_handler_kretprobe);

int register_kretprobe(struct kretproberp)
{
	int ret = 0;
	struct kretprobe_instanceinst;
	int i;
	voidaddr;

	if (kretprobe_blacklist_size) {
		addr = kprobe_addr(&amp;rp-&gt;kp);
		if (IS_ERR(addr))
			return PTR_ERR(addr);

		for (i = 0; kretprobe_blacklist[i].name != NULL; i++) {
			if (kretprobe_blacklist[i].addr == addr)
				return -EINVAL;
		}
	}

	rp-&gt;kp.pre_handler = pre_handler_kretprobe;
	rp-&gt;kp.post_handler = NULL;
	rp-&gt;kp.fault_handler = NULL;
	rp-&gt;kp.break_handler = NULL;

	*/ Pre-allocate memory for max kretprobe instances /*
	if (rp-&gt;maxactive &lt;= 0) {
#ifdef CONFIG_PREEMPT
		rp-&gt;maxactive = max_t(unsigned int, 10, 2*num_possible_cpus());
#else
		rp-&gt;maxactive = num_possible_cpus();
#endif
	}
	raw_spin_lock_init(&amp;rp-&gt;lock);
	INIT_HLIST_HEAD(&amp;rp-&gt;free_instances);
	for (i = 0; i &lt; rp-&gt;maxactive; i++) {
		inst = kmalloc(sizeof(struct kretprobe_instance) +
			       rp-&gt;data_size, GFP_KERNEL);
		if (inst == NULL) {
			free_rp_inst(rp);
			return -ENOMEM;
		}
		INIT_HLIST_NODE(&amp;inst-&gt;hlist);
		hlist_add_head(&amp;inst-&gt;hlist, &amp;rp-&gt;free_instances);
	}

	rp-&gt;nmissed = 0;
	*/ Establish function entry probe point /*
	ret = register_kprobe(&amp;rp-&gt;kp);
	if (ret != 0)
		free_rp_inst(rp);
	return ret;
}
EXPORT_SYMBOL_GPL(register_kretprobe);

int register_kretprobes(struct kretprobe*rps, int num)
{
	int ret = 0, i;

	if (num &lt;= 0)
		return -EINVAL;
	for (i = 0; i &lt; num; i++) {
		ret = register_kretprobe(rps[i]);
		if (ret &lt; 0) {
			if (i &gt; 0)
				unregister_kretprobes(rps, i);
			break;
		}
	}
	return ret;
}
EXPORT_SYMBOL_GPL(register_kretprobes);

void unregister_kretprobe(struct kretproberp)
{
	unregister_kretprobes(&amp;rp, 1);
}
EXPORT_SYMBOL_GPL(unregister_kretprobe);

void unregister_kretprobes(struct kretprobe*rps, int num)
{
	int i;

	if (num &lt;= 0)
		return;
	mutex_lock(&amp;kprobe_mutex);
	for (i = 0; i &lt; num; i++)
		if (__unregister_kprobe_top(&amp;rps[i]-&gt;kp) &lt; 0)
			rps[i]-&gt;kp.addr = NULL;
	mutex_unlock(&amp;kprobe_mutex);

	synchronize_sched();
	for (i = 0; i &lt; num; i++) {
		if (rps[i]-&gt;kp.addr) {
			__unregister_kprobe_bottom(&amp;rps[i]-&gt;kp);
			cleanup_rp_inst(rps[i]);
		}
	}
}
EXPORT_SYMBOL_GPL(unregister_kretprobes);

#else */ CONFIG_KRETPROBES /*
int register_kretprobe(struct kretproberp)
{
	return -ENOSYS;
}
EXPORT_SYMBOL_GPL(register_kretprobe);

int register_kretprobes(struct kretprobe*rps, int num)
{
	return -ENOSYS;
}
EXPORT_SYMBOL_GPL(register_kretprobes);

void unregister_kretprobe(struct kretproberp)
{
}
EXPORT_SYMBOL_GPL(unregister_kretprobe);

void unregister_kretprobes(struct kretprobe*rps, int num)
{
}
EXPORT_SYMBOL_GPL(unregister_kretprobes);

static int pre_handler_kretprobe(struct kprobep, struct pt_regsregs)
{
	return 0;
}
NOKPROBE_SYMBOL(pre_handler_kretprobe);

#endif */ CONFIG_KRETPROBES /*

*/ Set the kprobe gone and remove its instruction buffer. /*
static void kill_kprobe(struct kprobep)
{
	struct kprobekp;

	p-&gt;flags |= KPROBE_FLAG_GONE;
	if (kprobe_aggrprobe(p)) {
		*/
		 If this is an aggr_kprobe, we have to list all the
		 chained probes and mark them GONE.
		 /*
		list_for_each_entry_rcu(kp, &amp;p-&gt;list, list)
			kp-&gt;flags |= KPROBE_FLAG_GONE;
		p-&gt;post_handler = NULL;
		p-&gt;break_handler = NULL;
		kill_optimized_kprobe(p);
	}
	*/
	 Here, we can remove insn_slot safely, because no thread calls
	 the original probed function (which will be freed soon) any more.
	 /*
	arch_remove_kprobe(p);
}

*/ Disable one kprobe /*
int disable_kprobe(struct kprobekp)
{
	int ret = 0;

	mutex_lock(&amp;kprobe_mutex);

	*/ Disable this kprobe /*
	if (__disable_kprobe(kp) == NULL)
		ret = -EINVAL;

	mutex_unlock(&amp;kprobe_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(disable_kprobe);

*/ Enable one kprobe /*
int enable_kprobe(struct kprobekp)
{
	int ret = 0;
	struct kprobep;

	mutex_lock(&amp;kprobe_mutex);

	*/ Check whether specified probe is valid. /*
	p = __get_valid_kprobe(kp);
	if (unlikely(p == NULL)) {
		ret = -EINVAL;
		goto out;
	}

	if (kprobe_gone(kp)) {
		*/ This kprobe has gone, we couldn&#39;t enable it. /*
		ret = -EINVAL;
		goto out;
	}

	if (p != kp)
		kp-&gt;flags &amp;= ~KPROBE_FLAG_DISABLED;

	if (!kprobes_all_disarmed &amp;&amp; kprobe_disabled(p)) {
		p-&gt;flags &amp;= ~KPROBE_FLAG_DISABLED;
		arm_kprobe(p);
	}
out:
	mutex_unlock(&amp;kprobe_mutex);
	return ret;
}
EXPORT_SYMBOL_GPL(enable_kprobe);

void dump_kprobe(struct kprobekp)
{
	printk(KERN_WARNING &quot;Dumping kprobe:\n&quot;);
	printk(KERN_WARNING &quot;Name: %s\nAddress: %p\nOffset: %x\n&quot;,
	       kp-&gt;symbol_name, kp-&gt;addr, kp-&gt;offset);
}
NOKPROBE_SYMBOL(dump_kprobe);

*/
 Lookup and populate the kprobe_blacklist.

 Unlike the kretprobe blacklist, we&#39;ll need to determine
 the range of addresses that belong to the said functions,
 since a kprobe need not necessarily be at the beginning
 of a function.
 /*
static int __init populate_kprobe_blacklist(unsigned longstart,
					     unsigned longend)
{
	unsigned longiter;
	struct kprobe_blacklist_entryent;
	unsigned long entry, offset = 0, size = 0;

	for (iter = start; iter &lt; end; iter++) {
		entry = arch_deref_entry_point((void)*iter);

		if (!kernel_text_address(entry) ||
		    !kallsyms_lookup_size_offset(entry, &amp;size, &amp;offset)) {
			pr_err(&quot;Failed to find blacklist at %p\n&quot;,
				(void)entry);
			continue;
		}

		ent = kmalloc(sizeof(*ent), GFP_KERNEL);
		if (!ent)
			return -ENOMEM;
		ent-&gt;start_addr = entry;
		ent-&gt;end_addr = entry + size;
		INIT_LIST_HEAD(&amp;ent-&gt;list);
		list_add_tail(&amp;ent-&gt;list, &amp;kprobe_blacklist);
	}
	return 0;
}

*/ Module notifier call back, checking kprobes on the module /*
static int kprobes_module_callback(struct notifier_blocknb,
				   unsigned long val, voiddata)
{
	struct modulemod = data;
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;
	int checkcore = (val == MODULE_STATE_GOING);

	if (val != MODULE_STATE_GOING &amp;&amp; val != MODULE_STATE_LIVE)
		return NOTIFY_DONE;

	*/
	 When MODULE_STATE_GOING was notified, both of module .text and
	 .init.text sections would be freed. When MODULE_STATE_LIVE was
	 notified, only .init.text section would be freed. We need to
	 disable kprobes which have been inserted in the sections.
	 /*
	mutex_lock(&amp;kprobe_mutex);
	for (i = 0; i &lt; KPROBE_TABLE_SIZE; i++) {
		head = &amp;kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist)
			if (within_module_init((unsigned long)p-&gt;addr, mod) ||
			    (checkcore &amp;&amp;
			     within_module_core((unsigned long)p-&gt;addr, mod))) {
				*/
				 The vaddr this probe is installed will soon
				 be vfreed buy not synced to disk. Hence,
				 disarming the breakpoint isn&#39;t needed.
				 /*
				kill_kprobe(p);
			}
	}
	mutex_unlock(&amp;kprobe_mutex);
	return NOTIFY_DONE;
}

static struct notifier_block kprobe_module_nb = {
	.notifier_call = kprobes_module_callback,
	.priority = 0
};

*/ Markers of _kprobe_blacklist section /*
extern unsigned long __start_kprobe_blacklist[];
extern unsigned long __stop_kprobe_blacklist[];

static int __init init_kprobes(void)
{
	int i, err = 0;

	*/ FIXME allocate the probe table, currently defined statically /*
	*/ initialize all list heads /*
	for (i = 0; i &lt; KPROBE_TABLE_SIZE; i++) {
		INIT_HLIST_HEAD(&amp;kprobe_table[i]);
		INIT_HLIST_HEAD(&amp;kretprobe_inst_table[i]);
		raw_spin_lock_init(&amp;(kretprobe_table_locks[i].lock));
	}

	err = populate_kprobe_blacklist(__start_kprobe_blacklist,
					__stop_kprobe_blacklist);
	if (err) {
		pr_err(&quot;kprobes: failed to populate blacklist: %d\n&quot;, err);
		pr_err(&quot;Please take care of using kprobes.\n&quot;);
	}

	if (kretprobe_blacklist_size) {
		*/ lookup the function address from its name /*
		for (i = 0; kretprobe_blacklist[i].name != NULL; i++) {
			kprobe_lookup_name(kretprobe_blacklist[i].name,
					   kretprobe_blacklist[i].addr);
			if (!kretprobe_blacklist[i].addr)
				printk(&quot;kretprobe: lookup failed: %s\n&quot;,
				       kretprobe_blacklist[i].name);
		}
	}

#if defined(CONFIG_OPTPROBES)
#if defined(__ARCH_WANT_KPROBES_INSN_SLOT)
	*/ Init kprobe_optinsn_slots /*
	kprobe_optinsn_slots.insn_size = MAX_OPTINSN_SIZE;
#endif
	*/ By default, kprobes can be optimized /*
	kprobes_allow_optimization = true;
#endif

	*/ By default, kprobes are armed /*
	kprobes_all_disarmed = false;

	err = arch_init_kprobes();
	if (!err)
		err = register_die_notifier(&amp;kprobe_exceptions_nb);
	if (!err)
		err = register_module_notifier(&amp;kprobe_module_nb);

	kprobes_initialized = (err == 0);

	if (!err)
		init_test_probes();
	return err;
}

#ifdef CONFIG_DEBUG_FS
static void report_probe(struct seq_filepi, struct kprobep,
		const charsym, int offset, charmodname, struct kprobepp)
{
	charkprobe_type;

	if (p-&gt;pre_handler == pre_handler_kretprobe)
		kprobe_type = &quot;r&quot;;
	else if (p-&gt;pre_handler == setjmp_pre_handler)
		kprobe_type = &quot;j&quot;;
	else
		kprobe_type = &quot;k&quot;;

	if (sym)
		seq_printf(pi, &quot;%p  %s  %s+0x%x  %s &quot;,
			p-&gt;addr, kprobe_type, sym, offset,
			(modname ? modname : &quot; &quot;));
	else
		seq_printf(pi, &quot;%p  %s  %p &quot;,
			p-&gt;addr, kprobe_type, p-&gt;addr);

	if (!pp)
		pp = p;
	seq_printf(pi, &quot;%s%s%s%s\n&quot;,
		(kprobe_gone(p) ? &quot;[GONE]&quot; : &quot;&quot;),
		((kprobe_disabled(p) &amp;&amp; !kprobe_gone(p)) ?  &quot;[DISABLED]&quot; : &quot;&quot;),
		(kprobe_optimized(pp) ? &quot;[OPTIMIZED]&quot; : &quot;&quot;),
		(kprobe_ftrace(pp) ? &quot;[FTRACE]&quot; : &quot;&quot;));
}

static voidkprobe_seq_start(struct seq_filef, loff_tpos)
{
	return (*pos &lt; KPROBE_TABLE_SIZE) ? pos : NULL;
}

static voidkprobe_seq_next(struct seq_filef, voidv, loff_tpos)
{
	(*pos)++;
	if (*pos &gt;= KPROBE_TABLE_SIZE)
		return NULL;
	return pos;
}

static void kprobe_seq_stop(struct seq_filef, voidv)
{
	*/ Nothing to do /*
}

static int show_kprobe_addr(struct seq_filepi, voidv)
{
	struct hlist_headhead;
	struct kprobep,kp;
	const charsym = NULL;
	unsigned int i =(loff_t) v;
	unsigned long offset = 0;
	charmodname, namebuf[KSYM_NAME_LEN];

	head = &amp;kprobe_table[i];
	preempt_disable();
	hlist_for_each_entry_rcu(p, head, hlist) {
		sym = kallsyms_lookup((unsigned long)p-&gt;addr, NULL,
					&amp;offset, &amp;modname, namebuf);
		if (kprobe_aggrprobe(p)) {
			list_for_each_entry_rcu(kp, &amp;p-&gt;list, list)
				report_probe(pi, kp, sym, offset, modname, p);
		} else
			report_probe(pi, p, sym, offset, modname, NULL);
	}
	preempt_enable();
	return 0;
}

static const struct seq_operations kprobes_seq_ops = {
	.start = kprobe_seq_start,
	.next  = kprobe_seq_next,
	.stop  = kprobe_seq_stop,
	.show  = show_kprobe_addr
};

static int kprobes_open(struct inodeinode, struct filefilp)
{
	return seq_open(filp, &amp;kprobes_seq_ops);
}

static const struct file_operations debugfs_kprobes_operations = {
	.open           = kprobes_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = seq_release,
};

*/ kprobes/blacklist -- shows which functions can not be probed /*
static voidkprobe_blacklist_seq_start(struct seq_filem, loff_tpos)
{
	return seq_list_start(&amp;kprobe_blacklist,pos);
}

static voidkprobe_blacklist_seq_next(struct seq_filem, voidv, loff_tpos)
{
	return seq_list_next(v, &amp;kprobe_blacklist, pos);
}

static int kprobe_blacklist_seq_show(struct seq_filem, voidv)
{
	struct kprobe_blacklist_entryent =
		list_entry(v, struct kprobe_blacklist_entry, list);

	seq_printf(m, &quot;0x%p-0x%p\t%ps\n&quot;, (void)ent-&gt;start_addr,
		   (void)ent-&gt;end_addr, (void)ent-&gt;start_addr);
	return 0;
}

static const struct seq_operations kprobe_blacklist_seq_ops = {
	.start = kprobe_blacklist_seq_start,
	.next  = kprobe_blacklist_seq_next,
	.stop  = kprobe_seq_stop,	*/ Reuse void function /*
	.show  = kprobe_blacklist_seq_show,
};

static int kprobe_blacklist_open(struct inodeinode, struct filefilp)
{
	return seq_open(filp, &amp;kprobe_blacklist_seq_ops);
}

static const struct file_operations debugfs_kprobe_blacklist_ops = {
	.open           = kprobe_blacklist_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = seq_release,
};

static void arm_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&amp;kprobe_mutex);

	*/ If kprobes are armed, just return /*
	if (!kprobes_all_disarmed)
		goto already_enabled;

	*/
	 optimize_kprobe() called by arm_kprobe() checks
	 kprobes_all_disarmed, so set kprobes_all_disarmed before
	 arm_kprobe.
	 /*
	kprobes_all_disarmed = false;
	*/ Arming kprobes doesn&#39;t optimize kprobe itself /*
	for (i = 0; i &lt; KPROBE_TABLE_SIZE; i++) {
		head = &amp;kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist)
			if (!kprobe_disabled(p))
				arm_kprobe(p);
	}

	printk(KERN_INFO &quot;Kprobes globally enabled\n&quot;);

already_enabled:
	mutex_unlock(&amp;kprobe_mutex);
	return;
}

static void disarm_all_kprobes(void)
{
	struct hlist_headhead;
	struct kprobep;
	unsigned int i;

	mutex_lock(&amp;kprobe_mutex);

	*/ If kprobes are already disarmed, just return /*
	if (kprobes_all_disarmed) {
		mutex_unlock(&amp;kprobe_mutex);
		return;
	}

	kprobes_all_disarmed = true;
	printk(KERN_INFO &quot;Kprobes globally disabled\n&quot;);

	for (i = 0; i &lt; KPROBE_TABLE_SIZE; i++) {
		head = &amp;kprobe_table[i];
		hlist_for_each_entry_rcu(p, head, hlist) {
			if (!arch_trampoline_kprobe(p) &amp;&amp; !kprobe_disabled(p))
				disarm_kprobe(p, false);
		}
	}
	mutex_unlock(&amp;kprobe_mutex);

	*/ Wait for disarming all kprobes by optimizer /*
	wait_for_kprobe_optimizer();
}

*/
 XXX: The debugfs bool file interface doesn&#39;t allow for callbacks
 when the bool state is switched. We can reuse that facility when
 available
 /*
static ssize_t read_enabled_file_bool(struct filefile,
	       char __useruser_buf, size_t count, loff_tppos)
{
	char buf[3];

	if (!kprobes_all_disarmed)
		buf[0] = &#39;1&#39;;
	else
		buf[0] = &#39;0&#39;;
	buf[1] = &#39;\n&#39;;
	buf[2] = 0x00;
	return simple_read_from_buffer(user_buf, count, ppos, buf, 2);
}

static ssize_t write_enabled_file_bool(struct filefile,
	       const char __useruser_buf, size_t count, loff_tppos)
{
	char buf[32];
	size_t buf_size;

	buf_size = min(count, (sizeof(buf)-1));
	if (copy_from_user(buf, user_buf, buf_size))
		return -EFAULT;

	buf[buf_size] = &#39;\0&#39;;
	switch (buf[0]) {
	case &#39;y&#39;:
	case &#39;Y&#39;:
	case &#39;1&#39;:
		arm_all_kprobes();
		break;
	case &#39;n&#39;:
	case &#39;N&#39;:
	case &#39;0&#39;:
		disarm_all_kprobes();
		break;
	default:
		return -EINVAL;
	}

	return count;
}

static const struct file_operations fops_kp = {
	.read =         read_enabled_file_bool,
	.write =        write_enabled_file_bool,
	.llseek =	default_llseek,
};

static int __init debugfs_kprobe_init(void)
{
	struct dentrydir,file;
	unsigned int value = 1;

	dir = debugfs_create_dir(&quot;kprobes&quot;, NULL);
	if (!dir)
		return -ENOMEM;

	file = debugfs_create_file(&quot;list&quot;, 0444, dir, NULL,
				&amp;debugfs_kprobes_operations);
	if (!file)
		goto error;

	file = debugfs_create_file(&quot;enabled&quot;, 0600, dir,
					&amp;value, &amp;fops_kp);
	if (!file)
		goto error;

	file = debugfs_create_file(&quot;blacklist&quot;, 0444, dir, NULL,
				&amp;debugfs_kprobe_blacklist_ops);
	if (!file)
		goto error;

	return 0;

error:
	debugfs_remove(dir);
	return -ENOMEM;
}

late_initcall(debugfs_kprobe_init);
#endif*/ CONFIG_DEBUG_FS /*

module_init(init_kprobes);

*/ defined in arch/.../kernel/kprobes.c /*
EXPORT_SYMBOL_GPL(jprobe_return);
*/
