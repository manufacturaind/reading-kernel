   Rewritten by Rusty Russell, on the backs of many others...
   Copyright (C) 2001 Rusty Russell, 2002 Rusty Russell IBM.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/*
#include &lt;linux/ftrace.h&gt;
#include &lt;linux/memory.h&gt;
#include &lt;linux/module.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/init.h&gt;

#include &lt;asm/sections.h&gt;
#include &lt;asm/uaccess.h&gt;

*/
 mutex protecting text section modification (dynamic code patching).
 some users need to sleep (allocating memory...) while they hold this lock.

 NOT exported to modules - patching kernel text is a really delicate matter.
 /*
DEFINE_MUTEX(text_mutex);

extern struct exception_table_entry __start___ex_table[];
extern struct exception_table_entry __stop___ex_table[];

*/ Cleared by build time tools if the table is already sorted. /*
u32 __initdata __visible main_extable_sort_needed = 1;

*/ Sort the kernel&#39;s built-in exception table /*
void __init sort_main_extable(void)
{
	if (main_extable_sort_needed &amp;&amp; __stop___ex_table &gt; __start___ex_table) {
		pr_notice(&quot;Sorting __ex_table...\n&quot;);
		sort_extable(__start___ex_table, __stop___ex_table);
	}
}

*/ Given an address, look for it in the exception tables. /*
const struct exception_table_entrysearch_exception_tables(unsigned long addr)
{
	const struct exception_table_entrye;

	e = search_extable(__start___ex_table, __stop___ex_table-1, addr);
	if (!e)
		e = search_module_extables(addr);
	return e;
}

static inline int init_kernel_text(unsigned long addr)
{
	if (addr &gt;= (unsigned long)_sinittext &amp;&amp;
	    addr &lt; (unsigned long)_einittext)
		return 1;
	return 0;
}

int core_kernel_text(unsigned long addr)
{
	if (addr &gt;= (unsigned long)_stext &amp;&amp;
	    addr &lt; (unsigned long)_etext)
		return 1;

	if (system_state == SYSTEM_BOOTING &amp;&amp;
	    init_kernel_text(addr))
		return 1;
	return 0;
}

*/
 core_kernel_data - tell if addr points to kernel data
 @addr: address to test

 Returns true if @addr passed in is from the core kernel data
 section.

 Note: On some archs it may return true for core RODATA, and false
  for others. But will always be true for core RW data.
 /*
int core_kernel_data(unsigned long addr)
{
	if (addr &gt;= (unsigned long)_sdata &amp;&amp;
	    addr &lt; (unsigned long)_edata)
		return 1;
	return 0;
}

int __kernel_text_address(unsigned long addr)
{
	if (core_kernel_text(addr))
		return 1;
	if (is_module_text_address(addr))
		return 1;
	if (is_ftrace_trampoline(addr))
		return 1;
	*/
	 There might be init symbols in saved stacktraces.
	 Give those symbols a chance to be printed in
	 backtraces (such as lockdep traces).
	
	 Since we are after the module-symbols check, there&#39;s
	 no danger of address overlap:
	 /*
	if (init_kernel_text(addr))
		return 1;
	return 0;
}

int kernel_text_address(unsigned long addr)
{
	if (core_kernel_text(addr))
		return 1;
	if (is_module_text_address(addr))
		return 1;
	return is_ftrace_trampoline(addr);
}

*/
 On some architectures (PPC64, IA64) function pointers
 are actually only tokens to some data that then holds the
 real function address. As a result, to find if a function
 pointer is part of the kernel text, we need to do some
 special dereferencing first.
 /*
int func_ptr_is_kernel_text(voidptr)
{
	unsigned long addr;
	addr = (unsigned long) dereference_function_descriptor(ptr);
	if (core_kernel_text(addr))
		return 1;
	return is_module_text_address(addr);
}
*/
