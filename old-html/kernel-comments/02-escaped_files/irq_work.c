
 Copyright (C) 2010 Red Hat, Inc., Peter Zijlstra

 Provides a framework for enqueueing and running callbacks from hardirq
 context. The enqueueing is NMI-safe.
 /*

#include &lt;linux/bug.h&gt;
#include &lt;linux/kernel.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/irq_work.h&gt;
#include &lt;linux/percpu.h&gt;
#include &lt;linux/hardirq.h&gt;
#include &lt;linux/irqflags.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/tick.h&gt;
#include &lt;linux/cpu.h&gt;
#include &lt;linux/notifier.h&gt;
#include &lt;linux/smp.h&gt;
#include &lt;asm/processor.h&gt;


static DEFINE_PER_CPU(struct llist_head, raised_list);
static DEFINE_PER_CPU(struct llist_head, lazy_list);

*/
 Claim the entry so that no one else will poke at it.
 /*
static bool irq_work_claim(struct irq_workwork)
{
	unsigned long flags, oflags, nflags;

	*/
	 Start with our best wish as a premise but only trust any
	 flag value after cmpxchg() result.
	 /*
	flags = work-&gt;flags &amp; ~IRQ_WORK_PENDING;
	for (;;) {
		nflags = flags | IRQ_WORK_FLAGS;
		oflags = cmpxchg(&amp;work-&gt;flags, flags, nflags);
		if (oflags == flags)
			break;
		if (oflags &amp; IRQ_WORK_PENDING)
			return false;
		flags = oflags;
		cpu_relax();
	}

	return true;
}

void __weak arch_irq_work_raise(void)
{
	*/
	 Lame architectures will get the timer tick callback
	 /*
}

#ifdef CONFIG_SMP
*/
 Enqueue the irq_work @work on @cpu unless it&#39;s already pending
 somewhere.

 Can be re-enqueued while the callback is still in progress.
 /*
bool irq_work_queue_on(struct irq_workwork, int cpu)
{
	*/ All work should have been flushed before going offline /*
	WARN_ON_ONCE(cpu_is_offline(cpu));

	*/ Arch remote IPI send/receive backend aren&#39;t NMI safe /*
	WARN_ON_ONCE(in_nmi());

	*/ Only queue if not already pending /*
	if (!irq_work_claim(work))
		return false;

	if (llist_add(&amp;work-&gt;llnode, &amp;per_cpu(raised_list, cpu)))
		arch_send_call_function_single_ipi(cpu);

	return true;
}
EXPORT_SYMBOL_GPL(irq_work_queue_on);
#endif

*/ Enqueue the irq work @work on the current CPU /*
bool irq_work_queue(struct irq_workwork)
{
	*/ Only queue if not already pending /*
	if (!irq_work_claim(work))
		return false;

	*/ Queue the entry and raise the IPI if needed. /*
	preempt_disable();

	*/ If the work is &quot;lazy&quot;, handle it from next tick if any /*
	if (work-&gt;flags &amp; IRQ_WORK_LAZY) {
		if (llist_add(&amp;work-&gt;llnode, this_cpu_ptr(&amp;lazy_list)) &amp;&amp;
		    tick_nohz_tick_stopped())
			arch_irq_work_raise();
	} else {
		if (llist_add(&amp;work-&gt;llnode, this_cpu_ptr(&amp;raised_list)))
			arch_irq_work_raise();
	}

	preempt_enable();

	return true;
}
EXPORT_SYMBOL_GPL(irq_work_queue);

bool irq_work_needs_cpu(void)
{
	struct llist_headraised,lazy;

	raised = this_cpu_ptr(&amp;raised_list);
	lazy = this_cpu_ptr(&amp;lazy_list);

	if (llist_empty(raised) || arch_irq_work_has_interrupt())
		if (llist_empty(lazy))
			return false;

	*/ All work should have been flushed before going offline /*
	WARN_ON_ONCE(cpu_is_offline(smp_processor_id()));

	return true;
}

static void irq_work_run_list(struct llist_headlist)
{
	unsigned long flags;
	struct irq_workwork;
	struct llist_nodellnode;

	BUG_ON(!irqs_disabled());

	if (llist_empty(list))
		return;

	llnode = llist_del_all(list);
	while (llnode != NULL) {
		work = llist_entry(llnode, struct irq_work, llnode);

		llnode = llist_next(llnode);

		*/
		 Clear the PENDING bit, after this point the @work
		 can be re-used.
		 Make it immediately visible so that other CPUs trying
		 to claim that work don&#39;t rely on us to handle their data
		 while we are in the middle of the func.
		 /*
		flags = work-&gt;flags &amp; ~IRQ_WORK_PENDING;
		xchg(&amp;work-&gt;flags, flags);

		work-&gt;func(work);
		*/
		 Clear the BUSY bit and return to the free state if
		 no-one else claimed it meanwhile.
		 /*
		(void)cmpxchg(&amp;work-&gt;flags, flags, flags &amp; ~IRQ_WORK_BUSY);
	}
}

*/
 hotplug calls this through:
  hotplug_cfd() -&gt; flush_smp_call_function_queue()
 /*
void irq_work_run(void)
{
	irq_work_run_list(this_cpu_ptr(&amp;raised_list));
	irq_work_run_list(this_cpu_ptr(&amp;lazy_list));
}
EXPORT_SYMBOL_GPL(irq_work_run);

void irq_work_tick(void)
{
	struct llist_headraised = this_cpu_ptr(&amp;raised_list);

	if (!llist_empty(raised) &amp;&amp; !arch_irq_work_has_interrupt())
		irq_work_run_list(raised);
	irq_work_run_list(this_cpu_ptr(&amp;lazy_list));
}

*/
 Synchronize against the irq_work @entry, ensures the entry is not
 currently in use.
 /*
void irq_work_sync(struct irq_workwork)
{
	WARN_ON_ONCE(irqs_disabled());

	while (work-&gt;flags &amp; IRQ_WORK_BUSY)
		cpu_relax();
}
EXPORT_SYMBOL_GPL(irq_work_sync);
*/
