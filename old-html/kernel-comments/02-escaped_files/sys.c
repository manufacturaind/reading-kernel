
  linux/kernel/sys.c

  Copyright (C) 1991, 1992  Linus Torvalds
 /*

#include &lt;linux/export.h&gt;
#include &lt;linux/mm.h&gt;
#include &lt;linux/utsname.h&gt;
#include &lt;linux/mman.h&gt;
#include &lt;linux/reboot.h&gt;
#include &lt;linux/prctl.h&gt;
#include &lt;linux/highuid.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/kmod.h&gt;
#include &lt;linux/perf_event.h&gt;
#include &lt;linux/resource.h&gt;
#include &lt;linux/kernel.h&gt;
#include &lt;linux/workqueue.h&gt;
#include &lt;linux/capability.h&gt;
#include &lt;linux/device.h&gt;
#include &lt;linux/key.h&gt;
#include &lt;linux/times.h&gt;
#include &lt;linux/posix-timers.h&gt;
#include &lt;linux/security.h&gt;
#include &lt;linux/dcookies.h&gt;
#include &lt;linux/suspend.h&gt;
#include &lt;linux/tty.h&gt;
#include &lt;linux/signal.h&gt;
#include &lt;linux/cn_proc.h&gt;
#include &lt;linux/getcpu.h&gt;
#include &lt;linux/task_io_accounting_ops.h&gt;
#include &lt;linux/seccomp.h&gt;
#include &lt;linux/cpu.h&gt;
#include &lt;linux/personality.h&gt;
#include &lt;linux/ptrace.h&gt;
#include &lt;linux/fs_struct.h&gt;
#include &lt;linux/file.h&gt;
#include &lt;linux/mount.h&gt;
#include &lt;linux/gfp.h&gt;
#include &lt;linux/syscore_ops.h&gt;
#include &lt;linux/version.h&gt;
#include &lt;linux/ctype.h&gt;

#include &lt;linux/compat.h&gt;
#include &lt;linux/syscalls.h&gt;
#include &lt;linux/kprobes.h&gt;
#include &lt;linux/user_namespace.h&gt;
#include &lt;linux/binfmts.h&gt;

#include &lt;linux/sched.h&gt;
#include &lt;linux/rcupdate.h&gt;
#include &lt;linux/uidgid.h&gt;
#include &lt;linux/cred.h&gt;

#include &lt;linux/kmsg_dump.h&gt;
*/ Move somewhere else to avoid recompiling? /*
#include &lt;generated/utsrelease.h&gt;

#include &lt;asm/uaccess.h&gt;
#include &lt;asm/io.h&gt;
#include &lt;asm/unistd.h&gt;

#ifndef SET_UNALIGN_CTL
# define SET_UNALIGN_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_UNALIGN_CTL
# define GET_UNALIGN_CTL(a, b)	(-EINVAL)
#endif
#ifndef SET_FPEMU_CTL
# define SET_FPEMU_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_FPEMU_CTL
# define GET_FPEMU_CTL(a, b)	(-EINVAL)
#endif
#ifndef SET_FPEXC_CTL
# define SET_FPEXC_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_FPEXC_CTL
# define GET_FPEXC_CTL(a, b)	(-EINVAL)
#endif
#ifndef GET_ENDIAN
# define GET_ENDIAN(a, b)	(-EINVAL)
#endif
#ifndef SET_ENDIAN
# define SET_ENDIAN(a, b)	(-EINVAL)
#endif
#ifndef GET_TSC_CTL
# define GET_TSC_CTL(a)		(-EINVAL)
#endif
#ifndef SET_TSC_CTL
# define SET_TSC_CTL(a)		(-EINVAL)
#endif
#ifndef MPX_ENABLE_MANAGEMENT
# define MPX_ENABLE_MANAGEMENT()	(-EINVAL)
#endif
#ifndef MPX_DISABLE_MANAGEMENT
# define MPX_DISABLE_MANAGEMENT()	(-EINVAL)
#endif
#ifndef GET_FP_MODE
# define GET_FP_MODE(a)		(-EINVAL)
#endif
#ifndef SET_FP_MODE
# define SET_FP_MODE(a,b)	(-EINVAL)
#endif

*/
 this is where the system-wide overflow UID and GID are defined, for
 architectures that now have 32-bit UID/GID but didn&#39;t in the past
 /*

int overflowuid = DEFAULT_OVERFLOWUID;
int overflowgid = DEFAULT_OVERFLOWGID;

EXPORT_SYMBOL(overflowuid);
EXPORT_SYMBOL(overflowgid);

*/
 the same as above, but for filesystems which can only store a 16-bit
 UID and GID. as such, this is needed on all architectures
 /*

int fs_overflowuid = DEFAULT_FS_OVERFLOWUID;
int fs_overflowgid = DEFAULT_FS_OVERFLOWUID;

EXPORT_SYMBOL(fs_overflowuid);
EXPORT_SYMBOL(fs_overflowgid);

*/
 Returns true if current&#39;s euid is same as p&#39;s uid or euid,
 or has CAP_SYS_NICE to p&#39;s user_ns.

 Called with rcu_read_lock, creds are safe
 /*
static bool set_one_prio_perm(struct task_structp)
{
	const struct credcred = current_cred(),pcred = __task_cred(p);

	if (uid_eq(pcred-&gt;uid,  cred-&gt;euid) ||
	    uid_eq(pcred-&gt;euid, cred-&gt;euid))
		return true;
	if (ns_capable(pcred-&gt;user_ns, CAP_SYS_NICE))
		return true;
	return false;
}

*/
 set the priority of a task
 - the caller must hold the RCU read lock
 /*
static int set_one_prio(struct task_structp, int niceval, int error)
{
	int no_nice;

	if (!set_one_prio_perm(p)) {
		error = -EPERM;
		goto out;
	}
	if (niceval &lt; task_nice(p) &amp;&amp; !can_nice(p, niceval)) {
		error = -EACCES;
		goto out;
	}
	no_nice = security_task_setnice(p, niceval);
	if (no_nice) {
		error = no_nice;
		goto out;
	}
	if (error == -ESRCH)
		error = 0;
	set_user_nice(p, niceval);
out:
	return error;
}

SYSCALL_DEFINE3(setpriority, int, which, int, who, int, niceval)
{
	struct task_structg,p;
	struct user_structuser;
	const struct credcred = current_cred();
	int error = -EINVAL;
	struct pidpgrp;
	kuid_t uid;

	if (which &gt; PRIO_USER || which &lt; PRIO_PROCESS)
		goto out;

	*/ normalize: avoid signed division (rounding problems) /*
	error = -ESRCH;
	if (niceval &lt; MIN_NICE)
		niceval = MIN_NICE;
	if (niceval &gt; MAX_NICE)
		niceval = MAX_NICE;

	rcu_read_lock();
	read_lock(&amp;tasklist_lock);
	switch (which) {
	case PRIO_PROCESS:
		if (who)
			p = find_task_by_vpid(who);
		else
			p = current;
		if (p)
			error = set_one_prio(p, niceval, error);
		break;
	case PRIO_PGRP:
		if (who)
			pgrp = find_vpid(who);
		else
			pgrp = task_pgrp(current);
		do_each_pid_thread(pgrp, PIDTYPE_PGID, p) {
			error = set_one_prio(p, niceval, error);
		} while_each_pid_thread(pgrp, PIDTYPE_PGID, p);
		break;
	case PRIO_USER:
		uid = make_kuid(cred-&gt;user_ns, who);
		user = cred-&gt;user;
		if (!who)
			uid = cred-&gt;uid;
		else if (!uid_eq(uid, cred-&gt;uid)) {
			user = find_user(uid);
			if (!user)
				goto out_unlock;	*/ No processes for this user /*
		}
		do_each_thread(g, p) {
			if (uid_eq(task_uid(p), uid) &amp;&amp; task_pid_vnr(p))
				error = set_one_prio(p, niceval, error);
		} while_each_thread(g, p);
		if (!uid_eq(uid, cred-&gt;uid))
			free_uid(user);		*/ For find_user() /*
		break;
	}
out_unlock:
	read_unlock(&amp;tasklist_lock);
	rcu_read_unlock();
out:
	return error;
}

*/
 Ugh. To avoid negative return values, &quot;getpriority()&quot; will
 not return the normal nice-value, but a negated value that
 has been offset by 20 (ie it returns 40..1 instead of -20..19)
 to stay compatible.
 /*
SYSCALL_DEFINE2(getpriority, int, which, int, who)
{
	struct task_structg,p;
	struct user_structuser;
	const struct credcred = current_cred();
	long niceval, retval = -ESRCH;
	struct pidpgrp;
	kuid_t uid;

	if (which &gt; PRIO_USER || which &lt; PRIO_PROCESS)
		return -EINVAL;

	rcu_read_lock();
	read_lock(&amp;tasklist_lock);
	switch (which) {
	case PRIO_PROCESS:
		if (who)
			p = find_task_by_vpid(who);
		else
			p = current;
		if (p) {
			niceval = nice_to_rlimit(task_nice(p));
			if (niceval &gt; retval)
				retval = niceval;
		}
		break;
	case PRIO_PGRP:
		if (who)
			pgrp = find_vpid(who);
		else
			pgrp = task_pgrp(current);
		do_each_pid_thread(pgrp, PIDTYPE_PGID, p) {
			niceval = nice_to_rlimit(task_nice(p));
			if (niceval &gt; retval)
				retval = niceval;
		} while_each_pid_thread(pgrp, PIDTYPE_PGID, p);
		break;
	case PRIO_USER:
		uid = make_kuid(cred-&gt;user_ns, who);
		user = cred-&gt;user;
		if (!who)
			uid = cred-&gt;uid;
		else if (!uid_eq(uid, cred-&gt;uid)) {
			user = find_user(uid);
			if (!user)
				goto out_unlock;	*/ No processes for this user /*
		}
		do_each_thread(g, p) {
			if (uid_eq(task_uid(p), uid) &amp;&amp; task_pid_vnr(p)) {
				niceval = nice_to_rlimit(task_nice(p));
				if (niceval &gt; retval)
					retval = niceval;
			}
		} while_each_thread(g, p);
		if (!uid_eq(uid, cred-&gt;uid))
			free_uid(user);		*/ for find_user() /*
		break;
	}
out_unlock:
	read_unlock(&amp;tasklist_lock);
	rcu_read_unlock();

	return retval;
}

*/
 Unprivileged users may change the real gid to the effective gid
 or vice versa.  (BSD-style)

 If you set the real gid at all, or set the effective gid to a value not
 equal to the real gid, then the saved gid is set to the new effective gid.

 This makes it possible for a setgid program to completely drop its
 privileges, which is often a useful assertion to make when you are doing
 a security audit over a program.

 The general idea is that a program which uses just setregid() will be
 100% compatible with BSD.  A program which uses just setgid() will be
 100% compatible with POSIX with saved IDs.

 SMP: There are not races, the GIDs are checked only by filesystem
      operations (as far as semantic preservation is concerned).
 /*
#ifdef CONFIG_MULTIUSER
SYSCALL_DEFINE2(setregid, gid_t, rgid, gid_t, egid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kgid_t krgid, kegid;

	krgid = make_kgid(ns, rgid);
	kegid = make_kgid(ns, egid);

	if ((rgid != (gid_t) -1) &amp;&amp; !gid_valid(krgid))
		return -EINVAL;
	if ((egid != (gid_t) -1) &amp;&amp; !gid_valid(kegid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (rgid != (gid_t) -1) {
		if (gid_eq(old-&gt;gid, krgid) ||
		    gid_eq(old-&gt;egid, krgid) ||
		    ns_capable(old-&gt;user_ns, CAP_SETGID))
			new-&gt;gid = krgid;
		else
			goto error;
	}
	if (egid != (gid_t) -1) {
		if (gid_eq(old-&gt;gid, kegid) ||
		    gid_eq(old-&gt;egid, kegid) ||
		    gid_eq(old-&gt;sgid, kegid) ||
		    ns_capable(old-&gt;user_ns, CAP_SETGID))
			new-&gt;egid = kegid;
		else
			goto error;
	}

	if (rgid != (gid_t) -1 ||
	    (egid != (gid_t) -1 &amp;&amp; !gid_eq(kegid, old-&gt;gid)))
		new-&gt;sgid = new-&gt;egid;
	new-&gt;fsgid = new-&gt;egid;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

*/
 setgid() is implemented like SysV w/ SAVED_IDS

 SMP: Same implicit races as above.
 /*
SYSCALL_DEFINE1(setgid, gid_t, gid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kgid_t kgid;

	kgid = make_kgid(ns, gid);
	if (!gid_valid(kgid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (ns_capable(old-&gt;user_ns, CAP_SETGID))
		new-&gt;gid = new-&gt;egid = new-&gt;sgid = new-&gt;fsgid = kgid;
	else if (gid_eq(kgid, old-&gt;gid) || gid_eq(kgid, old-&gt;sgid))
		new-&gt;egid = new-&gt;fsgid = kgid;
	else
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

*/
 change the user struct in a credentials set to match the new UID
 /*
static int set_user(struct crednew)
{
	struct user_structnew_user;

	new_user = alloc_uid(new-&gt;uid);
	if (!new_user)
		return -EAGAIN;

	*/
	 We don&#39;t fail in case of NPROC limit excess here because too many
	 poorly written programs don&#39;t check set*uid() return code, assuming
	 it never fails if called by root.  We may still enforce NPROC limit
	 for programs doing set*uid()+execve() by harmlessly deferring the
	 failure to the execve() stage.
	 /*
	if (atomic_read(&amp;new_user-&gt;processes) &gt;= rlimit(RLIMIT_NPROC) &amp;&amp;
			new_user != INIT_USER)
		current-&gt;flags |= PF_NPROC_EXCEEDED;
	else
		current-&gt;flags &amp;= ~PF_NPROC_EXCEEDED;

	free_uid(new-&gt;user);
	new-&gt;user = new_user;
	return 0;
}

*/
 Unprivileged users may change the real uid to the effective uid
 or vice versa.  (BSD-style)

 If you set the real uid at all, or set the effective uid to a value not
 equal to the real uid, then the saved uid is set to the new effective uid.

 This makes it possible for a setuid program to completely drop its
 privileges, which is often a useful assertion to make when you are doing
 a security audit over a program.

 The general idea is that a program which uses just setreuid() will be
 100% compatible with BSD.  A program which uses just setuid() will be
 100% compatible with POSIX with saved IDs.
 /*
SYSCALL_DEFINE2(setreuid, uid_t, ruid, uid_t, euid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kuid_t kruid, keuid;

	kruid = make_kuid(ns, ruid);
	keuid = make_kuid(ns, euid);

	if ((ruid != (uid_t) -1) &amp;&amp; !uid_valid(kruid))
		return -EINVAL;
	if ((euid != (uid_t) -1) &amp;&amp; !uid_valid(keuid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (ruid != (uid_t) -1) {
		new-&gt;uid = kruid;
		if (!uid_eq(old-&gt;uid, kruid) &amp;&amp;
		    !uid_eq(old-&gt;euid, kruid) &amp;&amp;
		    !ns_capable(old-&gt;user_ns, CAP_SETUID))
			goto error;
	}

	if (euid != (uid_t) -1) {
		new-&gt;euid = keuid;
		if (!uid_eq(old-&gt;uid, keuid) &amp;&amp;
		    !uid_eq(old-&gt;euid, keuid) &amp;&amp;
		    !uid_eq(old-&gt;suid, keuid) &amp;&amp;
		    !ns_capable(old-&gt;user_ns, CAP_SETUID))
			goto error;
	}

	if (!uid_eq(new-&gt;uid, old-&gt;uid)) {
		retval = set_user(new);
		if (retval &lt; 0)
			goto error;
	}
	if (ruid != (uid_t) -1 ||
	    (euid != (uid_t) -1 &amp;&amp; !uid_eq(keuid, old-&gt;uid)))
		new-&gt;suid = new-&gt;euid;
	new-&gt;fsuid = new-&gt;euid;

	retval = security_task_fix_setuid(new, old, LSM_SETID_RE);
	if (retval &lt; 0)
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

*/
 setuid() is implemented like SysV with SAVED_IDS

 Note that SAVED_ID&#39;s is deficient in that a setuid root program
 like sendmail, for example, cannot set its uid to be a normal
 user and then switch back, because if you&#39;re root, setuid() sets
 the saved uid too.  If you don&#39;t like this, blame the bright people
 in the POSIX committee and/or USG.  Note that the BSD-style setreuid()
 will allow a root program to temporarily drop privileges and be able to
 regain them by swapping the real and effective uid.
 /*
SYSCALL_DEFINE1(setuid, uid_t, uid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kuid_t kuid;

	kuid = make_kuid(ns, uid);
	if (!uid_valid(kuid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (ns_capable(old-&gt;user_ns, CAP_SETUID)) {
		new-&gt;suid = new-&gt;uid = kuid;
		if (!uid_eq(kuid, old-&gt;uid)) {
			retval = set_user(new);
			if (retval &lt; 0)
				goto error;
		}
	} else if (!uid_eq(kuid, old-&gt;uid) &amp;&amp; !uid_eq(kuid, new-&gt;suid)) {
		goto error;
	}

	new-&gt;fsuid = new-&gt;euid = kuid;

	retval = security_task_fix_setuid(new, old, LSM_SETID_ID);
	if (retval &lt; 0)
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}


*/
 This function implements a generic ability to update ruid, euid,
 and suid.  This allows you to implement the 4.4 compatible seteuid().
 /*
SYSCALL_DEFINE3(setresuid, uid_t, ruid, uid_t, euid, uid_t, suid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kuid_t kruid, keuid, ksuid;

	kruid = make_kuid(ns, ruid);
	keuid = make_kuid(ns, euid);
	ksuid = make_kuid(ns, suid);

	if ((ruid != (uid_t) -1) &amp;&amp; !uid_valid(kruid))
		return -EINVAL;

	if ((euid != (uid_t) -1) &amp;&amp; !uid_valid(keuid))
		return -EINVAL;

	if ((suid != (uid_t) -1) &amp;&amp; !uid_valid(ksuid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;

	old = current_cred();

	retval = -EPERM;
	if (!ns_capable(old-&gt;user_ns, CAP_SETUID)) {
		if (ruid != (uid_t) -1        &amp;&amp; !uid_eq(kruid, old-&gt;uid) &amp;&amp;
		    !uid_eq(kruid, old-&gt;euid) &amp;&amp; !uid_eq(kruid, old-&gt;suid))
			goto error;
		if (euid != (uid_t) -1        &amp;&amp; !uid_eq(keuid, old-&gt;uid) &amp;&amp;
		    !uid_eq(keuid, old-&gt;euid) &amp;&amp; !uid_eq(keuid, old-&gt;suid))
			goto error;
		if (suid != (uid_t) -1        &amp;&amp; !uid_eq(ksuid, old-&gt;uid) &amp;&amp;
		    !uid_eq(ksuid, old-&gt;euid) &amp;&amp; !uid_eq(ksuid, old-&gt;suid))
			goto error;
	}

	if (ruid != (uid_t) -1) {
		new-&gt;uid = kruid;
		if (!uid_eq(kruid, old-&gt;uid)) {
			retval = set_user(new);
			if (retval &lt; 0)
				goto error;
		}
	}
	if (euid != (uid_t) -1)
		new-&gt;euid = keuid;
	if (suid != (uid_t) -1)
		new-&gt;suid = ksuid;
	new-&gt;fsuid = new-&gt;euid;

	retval = security_task_fix_setuid(new, old, LSM_SETID_RES);
	if (retval &lt; 0)
		goto error;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

SYSCALL_DEFINE3(getresuid, uid_t __user, ruidp, uid_t __user, euidp, uid_t __user, suidp)
{
	const struct credcred = current_cred();
	int retval;
	uid_t ruid, euid, suid;

	ruid = from_kuid_munged(cred-&gt;user_ns, cred-&gt;uid);
	euid = from_kuid_munged(cred-&gt;user_ns, cred-&gt;euid);
	suid = from_kuid_munged(cred-&gt;user_ns, cred-&gt;suid);

	retval = put_user(ruid, ruidp);
	if (!retval) {
		retval = put_user(euid, euidp);
		if (!retval)
			return put_user(suid, suidp);
	}
	return retval;
}

*/
 Same as above, but for rgid, egid, sgid.
 /*
SYSCALL_DEFINE3(setresgid, gid_t, rgid, gid_t, egid, gid_t, sgid)
{
	struct user_namespacens = current_user_ns();
	const struct credold;
	struct crednew;
	int retval;
	kgid_t krgid, kegid, ksgid;

	krgid = make_kgid(ns, rgid);
	kegid = make_kgid(ns, egid);
	ksgid = make_kgid(ns, sgid);

	if ((rgid != (gid_t) -1) &amp;&amp; !gid_valid(krgid))
		return -EINVAL;
	if ((egid != (gid_t) -1) &amp;&amp; !gid_valid(kegid))
		return -EINVAL;
	if ((sgid != (gid_t) -1) &amp;&amp; !gid_valid(ksgid))
		return -EINVAL;

	new = prepare_creds();
	if (!new)
		return -ENOMEM;
	old = current_cred();

	retval = -EPERM;
	if (!ns_capable(old-&gt;user_ns, CAP_SETGID)) {
		if (rgid != (gid_t) -1        &amp;&amp; !gid_eq(krgid, old-&gt;gid) &amp;&amp;
		    !gid_eq(krgid, old-&gt;egid) &amp;&amp; !gid_eq(krgid, old-&gt;sgid))
			goto error;
		if (egid != (gid_t) -1        &amp;&amp; !gid_eq(kegid, old-&gt;gid) &amp;&amp;
		    !gid_eq(kegid, old-&gt;egid) &amp;&amp; !gid_eq(kegid, old-&gt;sgid))
			goto error;
		if (sgid != (gid_t) -1        &amp;&amp; !gid_eq(ksgid, old-&gt;gid) &amp;&amp;
		    !gid_eq(ksgid, old-&gt;egid) &amp;&amp; !gid_eq(ksgid, old-&gt;sgid))
			goto error;
	}

	if (rgid != (gid_t) -1)
		new-&gt;gid = krgid;
	if (egid != (gid_t) -1)
		new-&gt;egid = kegid;
	if (sgid != (gid_t) -1)
		new-&gt;sgid = ksgid;
	new-&gt;fsgid = new-&gt;egid;

	return commit_creds(new);

error:
	abort_creds(new);
	return retval;
}

SYSCALL_DEFINE3(getresgid, gid_t __user, rgidp, gid_t __user, egidp, gid_t __user, sgidp)
{
	const struct credcred = current_cred();
	int retval;
	gid_t rgid, egid, sgid;

	rgid = from_kgid_munged(cred-&gt;user_ns, cred-&gt;gid);
	egid = from_kgid_munged(cred-&gt;user_ns, cred-&gt;egid);
	sgid = from_kgid_munged(cred-&gt;user_ns, cred-&gt;sgid);

	retval = put_user(rgid, rgidp);
	if (!retval) {
		retval = put_user(egid, egidp);
		if (!retval)
			retval = put_user(sgid, sgidp);
	}

	return retval;
}


*/
 &quot;setfsuid()&quot; sets the fsuid - the uid used for filesystem checks. This
 is used for &quot;access()&quot; and for the NFS daemon (letting nfsd stay at
 whatever uid it wants to). It normally shadows &quot;euid&quot;, except when
 explicitly set by setfsuid() or for access..
 /*
SYSCALL_DEFINE1(setfsuid, uid_t, uid)
{
	const struct credold;
	struct crednew;
	uid_t old_fsuid;
	kuid_t kuid;

	old = current_cred();
	old_fsuid = from_kuid_munged(old-&gt;user_ns, old-&gt;fsuid);

	kuid = make_kuid(old-&gt;user_ns, uid);
	if (!uid_valid(kuid))
		return old_fsuid;

	new = prepare_creds();
	if (!new)
		return old_fsuid;

	if (uid_eq(kuid, old-&gt;uid)  || uid_eq(kuid, old-&gt;euid)  ||
	    uid_eq(kuid, old-&gt;suid) || uid_eq(kuid, old-&gt;fsuid) ||
	    ns_capable(old-&gt;user_ns, CAP_SETUID)) {
		if (!uid_eq(kuid, old-&gt;fsuid)) {
			new-&gt;fsuid = kuid;
			if (security_task_fix_setuid(new, old, LSM_SETID_FS) == 0)
				goto change_okay;
		}
	}

	abort_creds(new);
	return old_fsuid;

change_okay:
	commit_creds(new);
	return old_fsuid;
}

*/
 Samma p&Atilde;&yen; svenska..
 /*
SYSCALL_DEFINE1(setfsgid, gid_t, gid)
{
	const struct credold;
	struct crednew;
	gid_t old_fsgid;
	kgid_t kgid;

	old = current_cred();
	old_fsgid = from_kgid_munged(old-&gt;user_ns, old-&gt;fsgid);

	kgid = make_kgid(old-&gt;user_ns, gid);
	if (!gid_valid(kgid))
		return old_fsgid;

	new = prepare_creds();
	if (!new)
		return old_fsgid;

	if (gid_eq(kgid, old-&gt;gid)  || gid_eq(kgid, old-&gt;egid)  ||
	    gid_eq(kgid, old-&gt;sgid) || gid_eq(kgid, old-&gt;fsgid) ||
	    ns_capable(old-&gt;user_ns, CAP_SETGID)) {
		if (!gid_eq(kgid, old-&gt;fsgid)) {
			new-&gt;fsgid = kgid;
			goto change_okay;
		}
	}

	abort_creds(new);
	return old_fsgid;

change_okay:
	commit_creds(new);
	return old_fsgid;
}
#endif */ CONFIG_MULTIUSER /*

*/
 sys_getpid - return the thread group id of the current process

 Note, despite the name, this returns the tgid not the pid.  The tgid and
 the pid are identical unless CLONE_THREAD was specified on clone() in
 which case the tgid is the same in all threads of the same group.

 This is SMP safe as current-&gt;tgid does not change.
 /*
SYSCALL_DEFINE0(getpid)
{
	return task_tgid_vnr(current);
}

*/ Thread ID - the internal kernel &quot;pid&quot; /*
SYSCALL_DEFINE0(gettid)
{
	return task_pid_vnr(current);
}

*/
 Accessing -&gt;real_parent is not SMP-safe, it could
 change from under us. However, we can use a stale
 value of -&gt;real_parent under rcu_read_lock(), see
 release_task()-&gt;call_rcu(delayed_put_task_struct).
 /*
SYSCALL_DEFINE0(getppid)
{
	int pid;

	rcu_read_lock();
	pid = task_tgid_vnr(rcu_dereference(current-&gt;real_parent));
	rcu_read_unlock();

	return pid;
}

SYSCALL_DEFINE0(getuid)
{
	*/ Only we change this so SMP safe /*
	return from_kuid_munged(current_user_ns(), current_uid());
}

SYSCALL_DEFINE0(geteuid)
{
	*/ Only we change this so SMP safe /*
	return from_kuid_munged(current_user_ns(), current_euid());
}

SYSCALL_DEFINE0(getgid)
{
	*/ Only we change this so SMP safe /*
	return from_kgid_munged(current_user_ns(), current_gid());
}

SYSCALL_DEFINE0(getegid)
{
	*/ Only we change this so SMP safe /*
	return from_kgid_munged(current_user_ns(), current_egid());
}

void do_sys_times(struct tmstms)
{
	cputime_t tgutime, tgstime, cutime, cstime;

	thread_group_cputime_adjusted(current, &amp;tgutime, &amp;tgstime);
	cutime = current-&gt;signal-&gt;cutime;
	cstime = current-&gt;signal-&gt;cstime;
	tms-&gt;tms_utime = cputime_to_clock_t(tgutime);
	tms-&gt;tms_stime = cputime_to_clock_t(tgstime);
	tms-&gt;tms_cutime = cputime_to_clock_t(cutime);
	tms-&gt;tms_cstime = cputime_to_clock_t(cstime);
}

SYSCALL_DEFINE1(times, struct tms __user, tbuf)
{
	if (tbuf) {
		struct tms tmp;

		do_sys_times(&amp;tmp);
		if (copy_to_user(tbuf, &amp;tmp, sizeof(struct tms)))
			return -EFAULT;
	}
	force_successful_syscall_return();
	return (long) jiffies_64_to_clock_t(get_jiffies_64());
}

*/
 This needs some heavy checking ...
 I just haven&#39;t the stomach for it. I also don&#39;t fully
 understand sessions/pgrp etc. Let somebody who does explain it.

 OK, I think I have the protection semantics right.... this is really
 only important on a multi-user system anyway, to make sure one user
 can&#39;t send a signal to a process owned by another.  -TYT, 12/12/91

 !PF_FORKNOEXEC check to conform completely to POSIX.
 /*
SYSCALL_DEFINE2(setpgid, pid_t, pid, pid_t, pgid)
{
	struct task_structp;
	struct task_structgroup_leader = current-&gt;group_leader;
	struct pidpgrp;
	int err;

	if (!pid)
		pid = task_pid_vnr(group_leader);
	if (!pgid)
		pgid = pid;
	if (pgid &lt; 0)
		return -EINVAL;
	rcu_read_lock();

	*/ From this point forward we keep holding onto the tasklist lock
	 so that our parent does not change from under us. -DaveM
	 /*
	write_lock_irq(&amp;tasklist_lock);

	err = -ESRCH;
	p = find_task_by_vpid(pid);
	if (!p)
		goto out;

	err = -EINVAL;
	if (!thread_group_leader(p))
		goto out;

	if (same_thread_group(p-&gt;real_parent, group_leader)) {
		err = -EPERM;
		if (task_session(p) != task_session(group_leader))
			goto out;
		err = -EACCES;
		if (!(p-&gt;flags &amp; PF_FORKNOEXEC))
			goto out;
	} else {
		err = -ESRCH;
		if (p != group_leader)
			goto out;
	}

	err = -EPERM;
	if (p-&gt;signal-&gt;leader)
		goto out;

	pgrp = task_pid(p);
	if (pgid != pid) {
		struct task_structg;

		pgrp = find_vpid(pgid);
		g = pid_task(pgrp, PIDTYPE_PGID);
		if (!g || task_session(g) != task_session(group_leader))
			goto out;
	}

	err = security_task_setpgid(p, pgid);
	if (err)
		goto out;

	if (task_pgrp(p) != pgrp)
		change_pid(p, PIDTYPE_PGID, pgrp);

	err = 0;
out:
	*/ All paths lead to here, thus we are safe. -DaveM /*
	write_unlock_irq(&amp;tasklist_lock);
	rcu_read_unlock();
	return err;
}

SYSCALL_DEFINE1(getpgid, pid_t, pid)
{
	struct task_structp;
	struct pidgrp;
	int retval;

	rcu_read_lock();
	if (!pid)
		grp = task_pgrp(current);
	else {
		retval = -ESRCH;
		p = find_task_by_vpid(pid);
		if (!p)
			goto out;
		grp = task_pgrp(p);
		if (!grp)
			goto out;

		retval = security_task_getpgid(p);
		if (retval)
			goto out;
	}
	retval = pid_vnr(grp);
out:
	rcu_read_unlock();
	return retval;
}

#ifdef __ARCH_WANT_SYS_GETPGRP

SYSCALL_DEFINE0(getpgrp)
{
	return sys_getpgid(0);
}

#endif

SYSCALL_DEFINE1(getsid, pid_t, pid)
{
	struct task_structp;
	struct pidsid;
	int retval;

	rcu_read_lock();
	if (!pid)
		sid = task_session(current);
	else {
		retval = -ESRCH;
		p = find_task_by_vpid(pid);
		if (!p)
			goto out;
		sid = task_session(p);
		if (!sid)
			goto out;

		retval = security_task_getsid(p);
		if (retval)
			goto out;
	}
	retval = pid_vnr(sid);
out:
	rcu_read_unlock();
	return retval;
}

static void set_special_pids(struct pidpid)
{
	struct task_structcurr = current-&gt;group_leader;

	if (task_session(curr) != pid)
		change_pid(curr, PIDTYPE_SID, pid);

	if (task_pgrp(curr) != pid)
		change_pid(curr, PIDTYPE_PGID, pid);
}

SYSCALL_DEFINE0(setsid)
{
	struct task_structgroup_leader = current-&gt;group_leader;
	struct pidsid = task_pid(group_leader);
	pid_t session = pid_vnr(sid);
	int err = -EPERM;

	write_lock_irq(&amp;tasklist_lock);
	*/ Fail if I am already a session leader /*
	if (group_leader-&gt;signal-&gt;leader)
		goto out;

	*/ Fail if a process group id already exists that equals the
	 proposed session id.
	 /*
	if (pid_task(sid, PIDTYPE_PGID))
		goto out;

	group_leader-&gt;signal-&gt;leader = 1;
	set_special_pids(sid);

	proc_clear_tty(group_leader);

	err = session;
out:
	write_unlock_irq(&amp;tasklist_lock);
	if (err &gt; 0) {
		proc_sid_connector(group_leader);
		sched_autogroup_create_attach(group_leader);
	}
	return err;
}

DECLARE_RWSEM(uts_sem);

#ifdef COMPAT_UTS_MACHINE
#define override_architecture(name) \
	(personality(current-&gt;personality) == PER_LINUX32 &amp;&amp; \
	 copy_to_user(name-&gt;machine, COMPAT_UTS_MACHINE, \
		      sizeof(COMPAT_UTS_MACHINE)))
#else
#define override_architecture(name)	0
#endif

*/
 Work around broken programs that cannot handle &quot;Linux 3.0&quot;.
 Instead we map 3.x to 2.6.40+x, so e.g. 3.0 would be 2.6.40
 And we map 4.x to 2.6.60+x, so 4.0 would be 2.6.60.
 /*
static int override_release(char __userrelease, size_t len)
{
	int ret = 0;

	if (current-&gt;personality &amp; UNAME26) {
		const charrest = UTS_RELEASE;
		char buf[65] = { 0 };
		int ndots = 0;
		unsigned v;
		size_t copy;

		while (*rest) {
			if (*rest == &#39;.&#39; &amp;&amp; ++ndots &gt;= 3)
				break;
			if (!isdigit(*rest) &amp;&amp;rest != &#39;.&#39;)
				break;
			rest++;
		}
		v = ((LINUX_VERSION_CODE &gt;&gt; 8) &amp; 0xff) + 60;
		copy = clamp_t(size_t, len, 1, sizeof(buf));
		copy = scnprintf(buf, copy, &quot;2.6.%u%s&quot;, v, rest);
		ret = copy_to_user(release, buf, copy + 1);
	}
	return ret;
}

SYSCALL_DEFINE1(newuname, struct new_utsname __user, name)
{
	int errno = 0;

	down_read(&amp;uts_sem);
	if (copy_to_user(name, utsname(), sizeofname))
		errno = -EFAULT;
	up_read(&amp;uts_sem);

	if (!errno &amp;&amp; override_release(name-&gt;release, sizeof(name-&gt;release)))
		errno = -EFAULT;
	if (!errno &amp;&amp; override_architecture(name))
		errno = -EFAULT;
	return errno;
}

#ifdef __ARCH_WANT_SYS_OLD_UNAME
*/
 Old cruft
 /*
SYSCALL_DEFINE1(uname, struct old_utsname __user, name)
{
	int error = 0;

	if (!name)
		return -EFAULT;

	down_read(&amp;uts_sem);
	if (copy_to_user(name, utsname(), sizeof(*name)))
		error = -EFAULT;
	up_read(&amp;uts_sem);

	if (!error &amp;&amp; override_release(name-&gt;release, sizeof(name-&gt;release)))
		error = -EFAULT;
	if (!error &amp;&amp; override_architecture(name))
		error = -EFAULT;
	return error;
}

SYSCALL_DEFINE1(olduname, struct oldold_utsname __user, name)
{
	int error;

	if (!name)
		return -EFAULT;
	if (!access_ok(VERIFY_WRITE, name, sizeof(struct oldold_utsname)))
		return -EFAULT;

	down_read(&amp;uts_sem);
	error = __copy_to_user(&amp;name-&gt;sysname, &amp;utsname()-&gt;sysname,
			       __OLD_UTS_LEN);
	error |= __put_user(0, name-&gt;sysname + __OLD_UTS_LEN);
	error |= __copy_to_user(&amp;name-&gt;nodename, &amp;utsname()-&gt;nodename,
				__OLD_UTS_LEN);
	error |= __put_user(0, name-&gt;nodename + __OLD_UTS_LEN);
	error |= __copy_to_user(&amp;name-&gt;release, &amp;utsname()-&gt;release,
				__OLD_UTS_LEN);
	error |= __put_user(0, name-&gt;release + __OLD_UTS_LEN);
	error |= __copy_to_user(&amp;name-&gt;version, &amp;utsname()-&gt;version,
				__OLD_UTS_LEN);
	error |= __put_user(0, name-&gt;version + __OLD_UTS_LEN);
	error |= __copy_to_user(&amp;name-&gt;machine, &amp;utsname()-&gt;machine,
				__OLD_UTS_LEN);
	error |= __put_user(0, name-&gt;machine + __OLD_UTS_LEN);
	up_read(&amp;uts_sem);

	if (!error &amp;&amp; override_architecture(name))
		error = -EFAULT;
	if (!error &amp;&amp; override_release(name-&gt;release, sizeof(name-&gt;release)))
		error = -EFAULT;
	return error ? -EFAULT : 0;
}
#endif

SYSCALL_DEFINE2(sethostname, char __user, name, int, len)
{
	int errno;
	char tmp[__NEW_UTS_LEN];

	if (!ns_capable(current-&gt;nsproxy-&gt;uts_ns-&gt;user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	if (len &lt; 0 || len &gt; __NEW_UTS_LEN)
		return -EINVAL;
	down_write(&amp;uts_sem);
	errno = -EFAULT;
	if (!copy_from_user(tmp, name, len)) {
		struct new_utsnameu = utsname();

		memcpy(u-&gt;nodename, tmp, len);
		memset(u-&gt;nodename + len, 0, sizeof(u-&gt;nodename) - len);
		errno = 0;
		uts_proc_notify(UTS_PROC_HOSTNAME);
	}
	up_write(&amp;uts_sem);
	return errno;
}

#ifdef __ARCH_WANT_SYS_GETHOSTNAME

SYSCALL_DEFINE2(gethostname, char __user, name, int, len)
{
	int i, errno;
	struct new_utsnameu;

	if (len &lt; 0)
		return -EINVAL;
	down_read(&amp;uts_sem);
	u = utsname();
	i = 1 + strlen(u-&gt;nodename);
	if (i &gt; len)
		i = len;
	errno = 0;
	if (copy_to_user(name, u-&gt;nodename, i))
		errno = -EFAULT;
	up_read(&amp;uts_sem);
	return errno;
}

#endif

*/
 Only setdomainname; getdomainname can be implemented by calling
 uname()
 /*
SYSCALL_DEFINE2(setdomainname, char __user, name, int, len)
{
	int errno;
	char tmp[__NEW_UTS_LEN];

	if (!ns_capable(current-&gt;nsproxy-&gt;uts_ns-&gt;user_ns, CAP_SYS_ADMIN))
		return -EPERM;
	if (len &lt; 0 || len &gt; __NEW_UTS_LEN)
		return -EINVAL;

	down_write(&amp;uts_sem);
	errno = -EFAULT;
	if (!copy_from_user(tmp, name, len)) {
		struct new_utsnameu = utsname();

		memcpy(u-&gt;domainname, tmp, len);
		memset(u-&gt;domainname + len, 0, sizeof(u-&gt;domainname) - len);
		errno = 0;
		uts_proc_notify(UTS_PROC_DOMAINNAME);
	}
	up_write(&amp;uts_sem);
	return errno;
}

SYSCALL_DEFINE2(getrlimit, unsigned int, resource, struct rlimit __user, rlim)
{
	struct rlimit value;
	int ret;

	ret = do_prlimit(current, resource, NULL, &amp;value);
	if (!ret)
		ret = copy_to_user(rlim, &amp;value, sizeof(*rlim)) ? -EFAULT : 0;

	return ret;
}

#ifdef __ARCH_WANT_SYS_OLD_GETRLIMIT

*/
	Back compatibility for getrlimit. Needed for some apps.
 /*
SYSCALL_DEFINE2(old_getrlimit, unsigned int, resource,
		struct rlimit __user, rlim)
{
	struct rlimit x;
	if (resource &gt;= RLIM_NLIMITS)
		return -EINVAL;

	task_lock(current-&gt;group_leader);
	x = current-&gt;signal-&gt;rlim[resource];
	task_unlock(current-&gt;group_leader);
	if (x.rlim_cur &gt; 0x7FFFFFFF)
		x.rlim_cur = 0x7FFFFFFF;
	if (x.rlim_max &gt; 0x7FFFFFFF)
		x.rlim_max = 0x7FFFFFFF;
	return copy_to_user(rlim, &amp;x, sizeof(x)) ? -EFAULT : 0;
}

#endif

static inline bool rlim64_is_infinity(__u64 rlim64)
{
#if BITS_PER_LONG &lt; 64
	return rlim64 &gt;= ULONG_MAX;
#else
	return rlim64 == RLIM64_INFINITY;
#endif
}

static void rlim_to_rlim64(const struct rlimitrlim, struct rlimit64rlim64)
{
	if (rlim-&gt;rlim_cur == RLIM_INFINITY)
		rlim64-&gt;rlim_cur = RLIM64_INFINITY;
	else
		rlim64-&gt;rlim_cur = rlim-&gt;rlim_cur;
	if (rlim-&gt;rlim_max == RLIM_INFINITY)
		rlim64-&gt;rlim_max = RLIM64_INFINITY;
	else
		rlim64-&gt;rlim_max = rlim-&gt;rlim_max;
}

static void rlim64_to_rlim(const struct rlimit64rlim64, struct rlimitrlim)
{
	if (rlim64_is_infinity(rlim64-&gt;rlim_cur))
		rlim-&gt;rlim_cur = RLIM_INFINITY;
	else
		rlim-&gt;rlim_cur = (unsigned long)rlim64-&gt;rlim_cur;
	if (rlim64_is_infinity(rlim64-&gt;rlim_max))
		rlim-&gt;rlim_max = RLIM_INFINITY;
	else
		rlim-&gt;rlim_max = (unsigned long)rlim64-&gt;rlim_max;
}

*/ make sure you are allowed to change @tsk limits before calling this /*
int do_prlimit(struct task_structtsk, unsigned int resource,
		struct rlimitnew_rlim, struct rlimitold_rlim)
{
	struct rlimitrlim;
	int retval = 0;

	if (resource &gt;= RLIM_NLIMITS)
		return -EINVAL;
	if (new_rlim) {
		if (new_rlim-&gt;rlim_cur &gt; new_rlim-&gt;rlim_max)
			return -EINVAL;
		if (resource == RLIMIT_NOFILE &amp;&amp;
				new_rlim-&gt;rlim_max &gt; sysctl_nr_open)
			return -EPERM;
	}

	*/ protect tsk-&gt;signal and tsk-&gt;sighand from disappearing /*
	read_lock(&amp;tasklist_lock);
	if (!tsk-&gt;sighand) {
		retval = -ESRCH;
		goto out;
	}

	rlim = tsk-&gt;signal-&gt;rlim + resource;
	task_lock(tsk-&gt;group_leader);
	if (new_rlim) {
		*/ Keep the capable check against init_user_ns until
		   cgroups can contain all limits /*
		if (new_rlim-&gt;rlim_max &gt; rlim-&gt;rlim_max &amp;&amp;
				!capable(CAP_SYS_RESOURCE))
			retval = -EPERM;
		if (!retval)
			retval = security_task_setrlimit(tsk-&gt;group_leader,
					resource, new_rlim);
		if (resource == RLIMIT_CPU &amp;&amp; new_rlim-&gt;rlim_cur == 0) {
			*/
			 The caller is asking for an immediate RLIMIT_CPU
			 expiry.  But we use the zero value to mean &quot;it was
			 never set&quot;.  So let&#39;s cheat and make it one second
			 instead
			 /*
			new_rlim-&gt;rlim_cur = 1;
		}
	}
	if (!retval) {
		if (old_rlim)
			*old_rlim =rlim;
		if (new_rlim)
			*rlim =new_rlim;
	}
	task_unlock(tsk-&gt;group_leader);

	*/
	 RLIMIT_CPU handling.   Note that the kernel fails to return an error
	 code if it rejected the user&#39;s attempt to set RLIMIT_CPU.  This is a
	 very long-standing error, and fixing it now risks breakage of
	 applications, so we live with it
	 /*
	 if (!retval &amp;&amp; new_rlim &amp;&amp; resource == RLIMIT_CPU &amp;&amp;
			 new_rlim-&gt;rlim_cur != RLIM_INFINITY)
		update_rlimit_cpu(tsk, new_rlim-&gt;rlim_cur);
out:
	read_unlock(&amp;tasklist_lock);
	return retval;
}

*/ rcu lock must be held /*
static int check_prlimit_permission(struct task_structtask)
{
	const struct credcred = current_cred(),tcred;

	if (current == task)
		return 0;

	tcred = __task_cred(task);
	if (uid_eq(cred-&gt;uid, tcred-&gt;euid) &amp;&amp;
	    uid_eq(cred-&gt;uid, tcred-&gt;suid) &amp;&amp;
	    uid_eq(cred-&gt;uid, tcred-&gt;uid)  &amp;&amp;
	    gid_eq(cred-&gt;gid, tcred-&gt;egid) &amp;&amp;
	    gid_eq(cred-&gt;gid, tcred-&gt;sgid) &amp;&amp;
	    gid_eq(cred-&gt;gid, tcred-&gt;gid))
		return 0;
	if (ns_capable(tcred-&gt;user_ns, CAP_SYS_RESOURCE))
		return 0;

	return -EPERM;
}

SYSCALL_DEFINE4(prlimit64, pid_t, pid, unsigned int, resource,
		const struct rlimit64 __user, new_rlim,
		struct rlimit64 __user, old_rlim)
{
	struct rlimit64 old64, new64;
	struct rlimit old, new;
	struct task_structtsk;
	int ret;

	if (new_rlim) {
		if (copy_from_user(&amp;new64, new_rlim, sizeof(new64)))
			return -EFAULT;
		rlim64_to_rlim(&amp;new64, &amp;new);
	}

	rcu_read_lock();
	tsk = pid ? find_task_by_vpid(pid) : current;
	if (!tsk) {
		rcu_read_unlock();
		return -ESRCH;
	}
	ret = check_prlimit_permission(tsk);
	if (ret) {
		rcu_read_unlock();
		return ret;
	}
	get_task_struct(tsk);
	rcu_read_unlock();

	ret = do_prlimit(tsk, resource, new_rlim ? &amp;new : NULL,
			old_rlim ? &amp;old : NULL);

	if (!ret &amp;&amp; old_rlim) {
		rlim_to_rlim64(&amp;old, &amp;old64);
		if (copy_to_user(old_rlim, &amp;old64, sizeof(old64)))
			ret = -EFAULT;
	}

	put_task_struct(tsk);
	return ret;
}

SYSCALL_DEFINE2(setrlimit, unsigned int, resource, struct rlimit __user, rlim)
{
	struct rlimit new_rlim;

	if (copy_from_user(&amp;new_rlim, rlim, sizeof(*rlim)))
		return -EFAULT;
	return do_prlimit(current, resource, &amp;new_rlim, NULL);
}

*/
 It would make sense to put struct rusage in the task_struct,
 except that would make the task_struct bereally big*.  After
 task_struct gets moved into malloc&#39;ed memory, it would
 make sense to do this.  It will make moving the rest of the information
 a lot simpler!  (Which we&#39;re not doing right now because we&#39;re not
 measuring them yet).

 When sampling multiple threads for RUSAGE_SELF, under SMP we might have
 races with threads incrementing their own counters.  But since word
 reads are atomic, we either get new values or old values and we don&#39;t
 care which for the sums.  We always take the siglock to protect reading
 the c* fields from p-&gt;signal from races with exit.c updating those
 fields when reaping, so a sample either gets all the additions of a
 given child after it&#39;s reaped, or none so this sample is before reaping.

 Locking:
 We need to take the siglock for CHILDEREN, SELF and BOTH
 for  the cases current multithreaded, non-current single threaded
 non-current multithreaded.  Thread traversal is now safe with
 the siglock held.
 Strictly speaking, we donot need to take the siglock if we are current and
 single threaded,  as no one else can take our signal_struct away, no one
 else can  reap the  children to update signal-&gt;c* counters, and no one else
 can race with the signal-&gt; fields. If we do not take any lock, the
 signal-&gt; fields could be read out of order while another thread was just
 exiting. So we should  place a read memory barrier when we avoid the lock.
 On the writer side,  write memory barrier is implied in  __exit_signal
 as __exit_signal releases  the siglock spinlock after updating the signal-&gt;
 fields. But we don&#39;t do this yet to keep things simple.

 /*

static void accumulate_thread_rusage(struct task_structt, struct rusager)
{
	r-&gt;ru_nvcsw += t-&gt;nvcsw;
	r-&gt;ru_nivcsw += t-&gt;nivcsw;
	r-&gt;ru_minflt += t-&gt;min_flt;
	r-&gt;ru_majflt += t-&gt;maj_flt;
	r-&gt;ru_inblock += task_io_get_inblock(t);
	r-&gt;ru_oublock += task_io_get_oublock(t);
}

static void k_getrusage(struct task_structp, int who, struct rusager)
{
	struct task_structt;
	unsigned long flags;
	cputime_t tgutime, tgstime, utime, stime;
	unsigned long maxrss = 0;

	memset((char)r, 0, sizeof (*r));
	utime = stime = 0;

	if (who == RUSAGE_THREAD) {
		task_cputime_adjusted(current, &amp;utime, &amp;stime);
		accumulate_thread_rusage(p, r);
		maxrss = p-&gt;signal-&gt;maxrss;
		goto out;
	}

	if (!lock_task_sighand(p, &amp;flags))
		return;

	switch (who) {
	case RUSAGE_BOTH:
	case RUSAGE_CHILDREN:
		utime = p-&gt;signal-&gt;cutime;
		stime = p-&gt;signal-&gt;cstime;
		r-&gt;ru_nvcsw = p-&gt;signal-&gt;cnvcsw;
		r-&gt;ru_nivcsw = p-&gt;signal-&gt;cnivcsw;
		r-&gt;ru_minflt = p-&gt;signal-&gt;cmin_flt;
		r-&gt;ru_majflt = p-&gt;signal-&gt;cmaj_flt;
		r-&gt;ru_inblock = p-&gt;signal-&gt;cinblock;
		r-&gt;ru_oublock = p-&gt;signal-&gt;coublock;
		maxrss = p-&gt;signal-&gt;cmaxrss;

		if (who == RUSAGE_CHILDREN)
			break;

	case RUSAGE_SELF:
		thread_group_cputime_adjusted(p, &amp;tgutime, &amp;tgstime);
		utime += tgutime;
		stime += tgstime;
		r-&gt;ru_nvcsw += p-&gt;signal-&gt;nvcsw;
		r-&gt;ru_nivcsw += p-&gt;signal-&gt;nivcsw;
		r-&gt;ru_minflt += p-&gt;signal-&gt;min_flt;
		r-&gt;ru_majflt += p-&gt;signal-&gt;maj_flt;
		r-&gt;ru_inblock += p-&gt;signal-&gt;inblock;
		r-&gt;ru_oublock += p-&gt;signal-&gt;oublock;
		if (maxrss &lt; p-&gt;signal-&gt;maxrss)
			maxrss = p-&gt;signal-&gt;maxrss;
		t = p;
		do {
			accumulate_thread_rusage(t, r);
		} while_each_thread(p, t);
		break;

	default:
		BUG();
	}
	unlock_task_sighand(p, &amp;flags);

out:
	cputime_to_timeval(utime, &amp;r-&gt;ru_utime);
	cputime_to_timeval(stime, &amp;r-&gt;ru_stime);

	if (who != RUSAGE_CHILDREN) {
		struct mm_structmm = get_task_mm(p);

		if (mm) {
			setmax_mm_hiwater_rss(&amp;maxrss, mm);
			mmput(mm);
		}
	}
	r-&gt;ru_maxrss = maxrss (PAGE_SIZE / 1024);/ convert pages to KBs /*
}

int getrusage(struct task_structp, int who, struct rusage __userru)
{
	struct rusage r;

	k_getrusage(p, who, &amp;r);
	return copy_to_user(ru, &amp;r, sizeof(r)) ? -EFAULT : 0;
}

SYSCALL_DEFINE2(getrusage, int, who, struct rusage __user, ru)
{
	if (who != RUSAGE_SELF &amp;&amp; who != RUSAGE_CHILDREN &amp;&amp;
	    who != RUSAGE_THREAD)
		return -EINVAL;
	return getrusage(current, who, ru);
}

#ifdef CONFIG_COMPAT
COMPAT_SYSCALL_DEFINE2(getrusage, int, who, struct compat_rusage __user, ru)
{
	struct rusage r;

	if (who != RUSAGE_SELF &amp;&amp; who != RUSAGE_CHILDREN &amp;&amp;
	    who != RUSAGE_THREAD)
		return -EINVAL;

	k_getrusage(current, who, &amp;r);
	return put_compat_rusage(&amp;r, ru);
}
#endif

SYSCALL_DEFINE1(umask, int, mask)
{
	mask = xchg(&amp;current-&gt;fs-&gt;umask, mask &amp; S_IRWXUGO);
	return mask;
}

static int prctl_set_mm_exe_file(struct mm_structmm, unsigned int fd)
{
	struct fd exe;
	struct fileold_exe,exe_file;
	struct inodeinode;
	int err;

	exe = fdget(fd);
	if (!exe.file)
		return -EBADF;

	inode = file_inode(exe.file);

	*/
	 Because the original mm-&gt;exe_file points to executable file, make
	 sure that this one is executable as well, to avoid breaking an
	 overall picture.
	 /*
	err = -EACCES;
	if (!S_ISREG(inode-&gt;i_mode) || path_noexec(&amp;exe.file-&gt;f_path))
		goto exit;

	err = inode_permission(inode, MAY_EXEC);
	if (err)
		goto exit;

	*/
	 Forbid mm-&gt;exe_file change if old file still mapped.
	 /*
	exe_file = get_mm_exe_file(mm);
	err = -EBUSY;
	if (exe_file) {
		struct vm_area_structvma;

		down_read(&amp;mm-&gt;mmap_sem);
		for (vma = mm-&gt;mmap; vma; vma = vma-&gt;vm_next) {
			if (!vma-&gt;vm_file)
				continue;
			if (path_equal(&amp;vma-&gt;vm_file-&gt;f_path,
				       &amp;exe_file-&gt;f_path))
				goto exit_err;
		}

		up_read(&amp;mm-&gt;mmap_sem);
		fput(exe_file);
	}

	*/
	 The symlink can be changed only once, just to disallow arbitrary
	 transitions malicious software might bring in. This means one
	 could make a snapshot over all processes running and monitor
	 /proc/pid/exe changes to notice unusual activity if needed.
	 /*
	err = -EPERM;
	if (test_and_set_bit(MMF_EXE_FILE_CHANGED, &amp;mm-&gt;flags))
		goto exit;

	err = 0;
	*/ set the new file, lockless /*
	get_file(exe.file);
	old_exe = xchg(&amp;mm-&gt;exe_file, exe.file);
	if (old_exe)
		fput(old_exe);
exit:
	fdput(exe);
	return err;
exit_err:
	up_read(&amp;mm-&gt;mmap_sem);
	fput(exe_file);
	goto exit;
}

*/
 WARNING: we don&#39;t require any capability here so be very careful
 in what is allowed for modification from userspace.
 /*
static int validate_prctl_map(struct prctl_mm_mapprctl_map)
{
	unsigned long mmap_max_addr = TASK_SIZE;
	struct mm_structmm = current-&gt;mm;
	int error = -EINVAL, i;

	static const unsigned char offsets[] = {
		offsetof(struct prctl_mm_map, start_code),
		offsetof(struct prctl_mm_map, end_code),
		offsetof(struct prctl_mm_map, start_data),
		offsetof(struct prctl_mm_map, end_data),
		offsetof(struct prctl_mm_map, start_brk),
		offsetof(struct prctl_mm_map, brk),
		offsetof(struct prctl_mm_map, start_stack),
		offsetof(struct prctl_mm_map, arg_start),
		offsetof(struct prctl_mm_map, arg_end),
		offsetof(struct prctl_mm_map, env_start),
		offsetof(struct prctl_mm_map, env_end),
	};

	*/
	 Make sure the members are not somewhere outside
	 of allowed address space.
	 /*
	for (i = 0; i &lt; ARRAY_SIZE(offsets); i++) {
		u64 val =(u64)((char)prctl_map + offsets[i]);

		if ((unsigned long)val &gt;= mmap_max_addr ||
		    (unsigned long)val &lt; mmap_min_addr)
			goto out;
	}

	*/
	 Make sure the pairs are ordered.
	 /*
#define __prctl_check_order(__m1, __op, __m2)				\
	((unsigned long)prctl_map-&gt;__m1 __op				\
	 (unsigned long)prctl_map-&gt;__m2) ? 0 : -EINVAL
	error  = __prctl_check_order(start_code, &lt;, end_code);
	error |= __prctl_check_order(start_data, &lt;, end_data);
	error |= __prctl_check_order(start_brk, &lt;=, brk);
	error |= __prctl_check_order(arg_start, &lt;=, arg_end);
	error |= __prctl_check_order(env_start, &lt;=, env_end);
	if (error)
		goto out;
#undef __prctl_check_order

	error = -EINVAL;

	*/
	 @brk should be after @end_data in traditional maps.
	 /*
	if (prctl_map-&gt;start_brk &lt;= prctl_map-&gt;end_data ||
	    prctl_map-&gt;brk &lt;= prctl_map-&gt;end_data)
		goto out;

	*/
	 Neither we should allow to override limits if they set.
	 /*
	if (check_data_rlimit(rlimit(RLIMIT_DATA), prctl_map-&gt;brk,
			      prctl_map-&gt;start_brk, prctl_map-&gt;end_data,
			      prctl_map-&gt;start_data))
			goto out;

	*/
	 Someone is trying to cheat the auxv vector.
	 /*
	if (prctl_map-&gt;auxv_size) {
		if (!prctl_map-&gt;auxv || prctl_map-&gt;auxv_size &gt; sizeof(mm-&gt;saved_auxv))
			goto out;
	}

	*/
	 Finally, make sure the caller has the rights to
	 change /proc/pid/exe link: only local root should
	 be allowed to.
	 /*
	if (prctl_map-&gt;exe_fd != (u32)-1) {
		struct user_namespacens = current_user_ns();
		const struct credcred = current_cred();

		if (!uid_eq(cred-&gt;uid, make_kuid(ns, 0)) ||
		    !gid_eq(cred-&gt;gid, make_kgid(ns, 0)))
			goto out;
	}

	error = 0;
out:
	return error;
}

#ifdef CONFIG_CHECKPOINT_RESTORE
static int prctl_set_mm_map(int opt, const void __useraddr, unsigned long data_size)
{
	struct prctl_mm_map prctl_map = { .exe_fd = (u32)-1, };
	unsigned long user_auxv[AT_VECTOR_SIZE];
	struct mm_structmm = current-&gt;mm;
	int error;

	BUILD_BUG_ON(sizeof(user_auxv) != sizeof(mm-&gt;saved_auxv));
	BUILD_BUG_ON(sizeof(struct prctl_mm_map) &gt; 256);

	if (opt == PR_SET_MM_MAP_SIZE)
		return put_user((unsigned int)sizeof(prctl_map),
				(unsigned int __user)addr);

	if (data_size != sizeof(prctl_map))
		return -EINVAL;

	if (copy_from_user(&amp;prctl_map, addr, sizeof(prctl_map)))
		return -EFAULT;

	error = validate_prctl_map(&amp;prctl_map);
	if (error)
		return error;

	if (prctl_map.auxv_size) {
		memset(user_auxv, 0, sizeof(user_auxv));
		if (copy_from_user(user_auxv,
				   (const void __user)prctl_map.auxv,
				   prctl_map.auxv_size))
			return -EFAULT;

		*/ Last entry must be AT_NULL as specification requires /*
		user_auxv[AT_VECTOR_SIZE - 2] = AT_NULL;
		user_auxv[AT_VECTOR_SIZE - 1] = AT_NULL;
	}

	if (prctl_map.exe_fd != (u32)-1) {
		error = prctl_set_mm_exe_file(mm, prctl_map.exe_fd);
		if (error)
			return error;
	}

	down_write(&amp;mm-&gt;mmap_sem);

	*/
	 We don&#39;t validate if these members are pointing to
	 real present VMAs because application may have correspond
	 VMAs already unmapped and kernel uses these members for statistics
	 output in procfs mostly, except
	
	  - @start_brk/@brk which are used in do_brk but kernel lookups
	    for VMAs when updating these memvers so anything wrong written
	    here cause kernel to swear at userspace program but won&#39;t lead
	    to any problem in kernel itself
	 /*

	mm-&gt;start_code	= prctl_map.start_code;
	mm-&gt;end_code	= prctl_map.end_code;
	mm-&gt;start_data	= prctl_map.start_data;
	mm-&gt;end_data	= prctl_map.end_data;
	mm-&gt;start_brk	= prctl_map.start_brk;
	mm-&gt;brk		= prctl_map.brk;
	mm-&gt;start_stack	= prctl_map.start_stack;
	mm-&gt;arg_start	= prctl_map.arg_start;
	mm-&gt;arg_end	= prctl_map.arg_end;
	mm-&gt;env_start	= prctl_map.env_start;
	mm-&gt;env_end	= prctl_map.env_end;

	*/
	 Note this update of @saved_auxv is lockless thus
	 if someone reads this member in procfs while we&#39;re
	 updating -- it may get partly updated results. It&#39;s
	 known and acceptable trade off: we leave it as is to
	 not introduce additional locks here making the kernel
	 more complex.
	 /*
	if (prctl_map.auxv_size)
		memcpy(mm-&gt;saved_auxv, user_auxv, sizeof(user_auxv));

	up_write(&amp;mm-&gt;mmap_sem);
	return 0;
}
#endif */ CONFIG_CHECKPOINT_RESTORE /*

static int prctl_set_auxv(struct mm_structmm, unsigned long addr,
			  unsigned long len)
{
	*/
	 This doesn&#39;t move the auxiliary vector itself since it&#39;s pinned to
	 mm_struct, but it permits filling the vector with new values.  It&#39;s
	 up to the caller to provide sane values here, otherwise userspace
	 tools which use this vector might be unhappy.
	 /*
	unsigned long user_auxv[AT_VECTOR_SIZE];

	if (len &gt; sizeof(user_auxv))
		return -EINVAL;

	if (copy_from_user(user_auxv, (const void __user)addr, len))
		return -EFAULT;

	*/ Make sure the last entry is always AT_NULL /*
	user_auxv[AT_VECTOR_SIZE - 2] = 0;
	user_auxv[AT_VECTOR_SIZE - 1] = 0;

	BUILD_BUG_ON(sizeof(user_auxv) != sizeof(mm-&gt;saved_auxv));

	task_lock(current);
	memcpy(mm-&gt;saved_auxv, user_auxv, len);
	task_unlock(current);

	return 0;
}

static int prctl_set_mm(int opt, unsigned long addr,
			unsigned long arg4, unsigned long arg5)
{
	struct mm_structmm = current-&gt;mm;
	struct prctl_mm_map prctl_map;
	struct vm_area_structvma;
	int error;

	if (arg5 || (arg4 &amp;&amp; (opt != PR_SET_MM_AUXV &amp;&amp;
			      opt != PR_SET_MM_MAP &amp;&amp;
			      opt != PR_SET_MM_MAP_SIZE)))
		return -EINVAL;

#ifdef CONFIG_CHECKPOINT_RESTORE
	if (opt == PR_SET_MM_MAP || opt == PR_SET_MM_MAP_SIZE)
		return prctl_set_mm_map(opt, (const void __user)addr, arg4);
#endif

	if (!capable(CAP_SYS_RESOURCE))
		return -EPERM;

	if (opt == PR_SET_MM_EXE_FILE)
		return prctl_set_mm_exe_file(mm, (unsigned int)addr);

	if (opt == PR_SET_MM_AUXV)
		return prctl_set_auxv(mm, addr, arg4);

	if (addr &gt;= TASK_SIZE || addr &lt; mmap_min_addr)
		return -EINVAL;

	error = -EINVAL;

	down_write(&amp;mm-&gt;mmap_sem);
	vma = find_vma(mm, addr);

	prctl_map.start_code	= mm-&gt;start_code;
	prctl_map.end_code	= mm-&gt;end_code;
	prctl_map.start_data	= mm-&gt;start_data;
	prctl_map.end_data	= mm-&gt;end_data;
	prctl_map.start_brk	= mm-&gt;start_brk;
	prctl_map.brk		= mm-&gt;brk;
	prctl_map.start_stack	= mm-&gt;start_stack;
	prctl_map.arg_start	= mm-&gt;arg_start;
	prctl_map.arg_end	= mm-&gt;arg_end;
	prctl_map.env_start	= mm-&gt;env_start;
	prctl_map.env_end	= mm-&gt;env_end;
	prctl_map.auxv		= NULL;
	prctl_map.auxv_size	= 0;
	prctl_map.exe_fd	= -1;

	switch (opt) {
	case PR_SET_MM_START_CODE:
		prctl_map.start_code = addr;
		break;
	case PR_SET_MM_END_CODE:
		prctl_map.end_code = addr;
		break;
	case PR_SET_MM_START_DATA:
		prctl_map.start_data = addr;
		break;
	case PR_SET_MM_END_DATA:
		prctl_map.end_data = addr;
		break;
	case PR_SET_MM_START_STACK:
		prctl_map.start_stack = addr;
		break;
	case PR_SET_MM_START_BRK:
		prctl_map.start_brk = addr;
		break;
	case PR_SET_MM_BRK:
		prctl_map.brk = addr;
		break;
	case PR_SET_MM_ARG_START:
		prctl_map.arg_start = addr;
		break;
	case PR_SET_MM_ARG_END:
		prctl_map.arg_end = addr;
		break;
	case PR_SET_MM_ENV_START:
		prctl_map.env_start = addr;
		break;
	case PR_SET_MM_ENV_END:
		prctl_map.env_end = addr;
		break;
	default:
		goto out;
	}

	error = validate_prctl_map(&amp;prctl_map);
	if (error)
		goto out;

	switch (opt) {
	*/
	 If command line arguments and environment
	 are placed somewhere else on stack, we can
	 set them up here, ARG_START/END to setup
	 command line argumets and ENV_START/END
	 for environment.
	 /*
	case PR_SET_MM_START_STACK:
	case PR_SET_MM_ARG_START:
	case PR_SET_MM_ARG_END:
	case PR_SET_MM_ENV_START:
	case PR_SET_MM_ENV_END:
		if (!vma) {
			error = -EFAULT;
			goto out;
		}
	}

	mm-&gt;start_code	= prctl_map.start_code;
	mm-&gt;end_code	= prctl_map.end_code;
	mm-&gt;start_data	= prctl_map.start_data;
	mm-&gt;end_data	= prctl_map.end_data;
	mm-&gt;start_brk	= prctl_map.start_brk;
	mm-&gt;brk		= prctl_map.brk;
	mm-&gt;start_stack	= prctl_map.start_stack;
	mm-&gt;arg_start	= prctl_map.arg_start;
	mm-&gt;arg_end	= prctl_map.arg_end;
	mm-&gt;env_start	= prctl_map.env_start;
	mm-&gt;env_end	= prctl_map.env_end;

	error = 0;
out:
	up_write(&amp;mm-&gt;mmap_sem);
	return error;
}

#ifdef CONFIG_CHECKPOINT_RESTORE
static int prctl_get_tid_address(struct task_structme, int __user*tid_addr)
{
	return put_user(me-&gt;clear_child_tid, tid_addr);
}
#else
static int prctl_get_tid_address(struct task_structme, int __user*tid_addr)
{
	return -EINVAL;
}
#endif

SYSCALL_DEFINE5(prctl, int, option, unsigned long, arg2, unsigned long, arg3,
		unsigned long, arg4, unsigned long, arg5)
{
	struct task_structme = current;
	unsigned char comm[sizeof(me-&gt;comm)];
	long error;

	error = security_task_prctl(option, arg2, arg3, arg4, arg5);
	if (error != -ENOSYS)
		return error;

	error = 0;
	switch (option) {
	case PR_SET_PDEATHSIG:
		if (!valid_signal(arg2)) {
			error = -EINVAL;
			break;
		}
		me-&gt;pdeath_signal = arg2;
		break;
	case PR_GET_PDEATHSIG:
		error = put_user(me-&gt;pdeath_signal, (int __user)arg2);
		break;
	case PR_GET_DUMPABLE:
		error = get_dumpable(me-&gt;mm);
		break;
	case PR_SET_DUMPABLE:
		if (arg2 != SUID_DUMP_DISABLE &amp;&amp; arg2 != SUID_DUMP_USER) {
			error = -EINVAL;
			break;
		}
		set_dumpable(me-&gt;mm, arg2);
		break;

	case PR_SET_UNALIGN:
		error = SET_UNALIGN_CTL(me, arg2);
		break;
	case PR_GET_UNALIGN:
		error = GET_UNALIGN_CTL(me, arg2);
		break;
	case PR_SET_FPEMU:
		error = SET_FPEMU_CTL(me, arg2);
		break;
	case PR_GET_FPEMU:
		error = GET_FPEMU_CTL(me, arg2);
		break;
	case PR_SET_FPEXC:
		error = SET_FPEXC_CTL(me, arg2);
		break;
	case PR_GET_FPEXC:
		error = GET_FPEXC_CTL(me, arg2);
		break;
	case PR_GET_TIMING:
		error = PR_TIMING_STATISTICAL;
		break;
	case PR_SET_TIMING:
		if (arg2 != PR_TIMING_STATISTICAL)
			error = -EINVAL;
		break;
	case PR_SET_NAME:
		comm[sizeof(me-&gt;comm) - 1] = 0;
		if (strncpy_from_user(comm, (char __user)arg2,
				      sizeof(me-&gt;comm) - 1) &lt; 0)
			return -EFAULT;
		set_task_comm(me, comm);
		proc_comm_connector(me);
		break;
	case PR_GET_NAME:
		get_task_comm(comm, me);
		if (copy_to_user((char __user)arg2, comm, sizeof(comm)))
			return -EFAULT;
		break;
	case PR_GET_ENDIAN:
		error = GET_ENDIAN(me, arg2);
		break;
	case PR_SET_ENDIAN:
		error = SET_ENDIAN(me, arg2);
		break;
	case PR_GET_SECCOMP:
		error = prctl_get_seccomp();
		break;
	case PR_SET_SECCOMP:
		error = prctl_set_seccomp(arg2, (char __user)arg3);
		break;
	case PR_GET_TSC:
		error = GET_TSC_CTL(arg2);
		break;
	case PR_SET_TSC:
		error = SET_TSC_CTL(arg2);
		break;
	case PR_TASK_PERF_EVENTS_DISABLE:
		error = perf_event_task_disable();
		break;
	case PR_TASK_PERF_EVENTS_ENABLE:
		error = perf_event_task_enable();
		break;
	case PR_GET_TIMERSLACK:
		if (current-&gt;timer_slack_ns &gt; ULONG_MAX)
			error = ULONG_MAX;
		else
			error = current-&gt;timer_slack_ns;
		break;
	case PR_SET_TIMERSLACK:
		if (arg2 &lt;= 0)
			current-&gt;timer_slack_ns =
					current-&gt;default_timer_slack_ns;
		else
			current-&gt;timer_slack_ns = arg2;
		break;
	case PR_MCE_KILL:
		if (arg4 | arg5)
			return -EINVAL;
		switch (arg2) {
		case PR_MCE_KILL_CLEAR:
			if (arg3 != 0)
				return -EINVAL;
			current-&gt;flags &amp;= ~PF_MCE_PROCESS;
			break;
		case PR_MCE_KILL_SET:
			current-&gt;flags |= PF_MCE_PROCESS;
			if (arg3 == PR_MCE_KILL_EARLY)
				current-&gt;flags |= PF_MCE_EARLY;
			else if (arg3 == PR_MCE_KILL_LATE)
				current-&gt;flags &amp;= ~PF_MCE_EARLY;
			else if (arg3 == PR_MCE_KILL_DEFAULT)
				current-&gt;flags &amp;=
						~(PF_MCE_EARLY|PF_MCE_PROCESS);
			else
				return -EINVAL;
			break;
		default:
			return -EINVAL;
		}
		break;
	case PR_MCE_KILL_GET:
		if (arg2 | arg3 | arg4 | arg5)
			return -EINVAL;
		if (current-&gt;flags &amp; PF_MCE_PROCESS)
			error = (current-&gt;flags &amp; PF_MCE_EARLY) ?
				PR_MCE_KILL_EARLY : PR_MCE_KILL_LATE;
		else
			error = PR_MCE_KILL_DEFAULT;
		break;
	case PR_SET_MM:
		error = prctl_set_mm(arg2, arg3, arg4, arg5);
		break;
	case PR_GET_TID_ADDRESS:
		error = prctl_get_tid_address(me, (int __user*)arg2);
		break;
	case PR_SET_CHILD_SUBREAPER:
		me-&gt;signal-&gt;is_child_subreaper = !!arg2;
		break;
	case PR_GET_CHILD_SUBREAPER:
		error = put_user(me-&gt;signal-&gt;is_child_subreaper,
				 (int __user)arg2);
		break;
	case PR_SET_NO_NEW_PRIVS:
		if (arg2 != 1 || arg3 || arg4 || arg5)
			return -EINVAL;

		task_set_no_new_privs(current);
		break;
	case PR_GET_NO_NEW_PRIVS:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		return task_no_new_privs(current) ? 1 : 0;
	case PR_GET_THP_DISABLE:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		error = !!(me-&gt;mm-&gt;def_flags &amp; VM_NOHUGEPAGE);
		break;
	case PR_SET_THP_DISABLE:
		if (arg3 || arg4 || arg5)
			return -EINVAL;
		down_write(&amp;me-&gt;mm-&gt;mmap_sem);
		if (arg2)
			me-&gt;mm-&gt;def_flags |= VM_NOHUGEPAGE;
		else
			me-&gt;mm-&gt;def_flags &amp;= ~VM_NOHUGEPAGE;
		up_write(&amp;me-&gt;mm-&gt;mmap_sem);
		break;
	case PR_MPX_ENABLE_MANAGEMENT:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		error = MPX_ENABLE_MANAGEMENT();
		break;
	case PR_MPX_DISABLE_MANAGEMENT:
		if (arg2 || arg3 || arg4 || arg5)
			return -EINVAL;
		error = MPX_DISABLE_MANAGEMENT();
		break;
	case PR_SET_FP_MODE:
		error = SET_FP_MODE(me, arg2);
		break;
	case PR_GET_FP_MODE:
		error = GET_FP_MODE(me);
		break;
	default:
		error = -EINVAL;
		break;
	}
	return error;
}

SYSCALL_DEFINE3(getcpu, unsigned __user, cpup, unsigned __user, nodep,
		struct getcpu_cache __user, unused)
{
	int err = 0;
	int cpu = raw_smp_processor_id();

	if (cpup)
		err |= put_user(cpu, cpup);
	if (nodep)
		err |= put_user(cpu_to_node(cpu), nodep);
	return err ? -EFAULT : 0;
}

*/
 do_sysinfo - fill in sysinfo struct
 @info: pointer to buffer to fill
 /*
static int do_sysinfo(struct sysinfoinfo)
{
	unsigned long mem_total, sav_total;
	unsigned int mem_unit, bitcount;
	struct timespec tp;

	memset(info, 0, sizeof(struct sysinfo));

	get_monotonic_boottime(&amp;tp);
	info-&gt;uptime = tp.tv_sec + (tp.tv_nsec ? 1 : 0);

	get_avenrun(info-&gt;loads, 0, SI_LOAD_SHIFT - FSHIFT);

	info-&gt;procs = nr_threads;

	si_meminfo(info);
	si_swapinfo(info);

	*/
	 If the sum of all the available memory (i.e. ram + swap)
	 is less than can be stored in a 32 bit unsigned long then
	 we can be binary compatible with 2.2.x kernels.  If not,
	 well, in that case 2.2.x was broken anyways...
	
	  -Erik Andersen &lt;andersee@debian.org&gt;
	 /*

	mem_total = info-&gt;totalram + info-&gt;totalswap;
	if (mem_total &lt; info-&gt;totalram || mem_total &lt; info-&gt;totalswap)
		goto out;
	bitcount = 0;
	mem_unit = info-&gt;mem_unit;
	while (mem_unit &gt; 1) {
		bitcount++;
		mem_unit &gt;&gt;= 1;
		sav_total = mem_total;
		mem_total &lt;&lt;= 1;
		if (mem_total &lt; sav_total)
			goto out;
	}

	*/
	 If mem_total did not overflow, multiply all memory values by
	 info-&gt;mem_unit and set it to 1.  This leaves things compatible
	 with 2.2.x, and also retains compatibility with earlier 2.4.x
	 kernels...
	 /*

	info-&gt;mem_unit = 1;
	info-&gt;totalram &lt;&lt;= bitcount;
	info-&gt;freeram &lt;&lt;= bitcount;
	info-&gt;sharedram &lt;&lt;= bitcount;
	info-&gt;bufferram &lt;&lt;= bitcount;
	info-&gt;totalswap &lt;&lt;= bitcount;
	info-&gt;freeswap &lt;&lt;= bitcount;
	info-&gt;totalhigh &lt;&lt;= bitcount;
	info-&gt;freehigh &lt;&lt;= bitcount;

out:
	return 0;
}

SYSCALL_DEFINE1(sysinfo, struct sysinfo __user, info)
{
	struct sysinfo val;

	do_sysinfo(&amp;val);

	if (copy_to_user(info, &amp;val, sizeof(struct sysinfo)))
		return -EFAULT;

	return 0;
}

#ifdef CONFIG_COMPAT
struct compat_sysinfo {
	s32 uptime;
	u32 loads[3];
	u32 totalram;
	u32 freeram;
	u32 sharedram;
	u32 bufferram;
	u32 totalswap;
	u32 freeswap;
	u16 procs;
	u16 pad;
	u32 totalhigh;
	u32 freehigh;
	u32 mem_unit;
	char _f[20-2*sizeof(u32)-sizeof(int)];
};

COMPAT_SYSCALL_DEFINE1(sysinfo, struct compat_sysinfo __user, info)
{
	struct sysinfo s;

	do_sysinfo(&amp;s);

	*/ Check to see if any memory value is too large for 32-bit and scale
	  down if needed
	 /*
	if (upper_32_bits(s.totalram) || upper_32_bits(s.totalswap)) {
		int bitcount = 0;

		while (s.mem_unit &lt; PAGE_SIZE) {
			s.mem_unit &lt;&lt;= 1;
			bitcount++;
		}

		s.totalram &gt;&gt;= bitcount;
		s.freeram &gt;&gt;= bitcount;
		s.sharedram &gt;&gt;= bitcount;
		s.bufferram &gt;&gt;= bitcount;
		s.totalswap &gt;&gt;= bitcount;
		s.freeswap &gt;&gt;= bitcount;
		s.totalhigh &gt;&gt;= bitcount;
		s.freehigh &gt;&gt;= bitcount;
	}

	if (!access_ok(VERIFY_WRITE, info, sizeof(struct compat_sysinfo)) ||
	    __put_user(s.uptime, &amp;info-&gt;uptime) ||
	    __put_user(s.loads[0], &amp;info-&gt;loads[0]) ||
	    __put_user(s.loads[1], &amp;info-&gt;loads[1]) ||
	    __put_user(s.loads[2], &amp;info-&gt;loads[2]) ||
	    __put_user(s.totalram, &amp;info-&gt;totalram) ||
	    __put_user(s.freeram, &amp;info-&gt;freeram) ||
	    __put_user(s.sharedram, &amp;info-&gt;sharedram) ||
	    __put_user(s.bufferram, &amp;info-&gt;bufferram) ||
	    __put_user(s.totalswap, &amp;info-&gt;totalswap) ||
	    __put_user(s.freeswap, &amp;info-&gt;freeswap) ||
	    __put_user(s.procs, &amp;info-&gt;procs) ||
	    __put_user(s.totalhigh, &amp;info-&gt;totalhigh) ||
	    __put_user(s.freehigh, &amp;info-&gt;freehigh) ||
	    __put_user(s.mem_unit, &amp;info-&gt;mem_unit))
		return -EFAULT;

	return 0;
}
#endif*/ CONFIG_COMPAT
