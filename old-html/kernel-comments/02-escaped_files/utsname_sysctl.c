
  Copyright (C) 2007

  Author: Eric Biederman &lt;ebiederm@xmision.com&gt;

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, version 2 of the
  License.
 /*

#include &lt;linux/export.h&gt;
#include &lt;linux/uts.h&gt;
#include &lt;linux/utsname.h&gt;
#include &lt;linux/sysctl.h&gt;
#include &lt;linux/wait.h&gt;

#ifdef CONFIG_PROC_SYSCTL

static voidget_uts(struct ctl_tabletable, int write)
{
	charwhich = table-&gt;data;
	struct uts_namespaceuts_ns;

	uts_ns = current-&gt;nsproxy-&gt;uts_ns;
	which = (which - (char)&amp;init_uts_ns) + (char)uts_ns;

	if (!write)
		down_read(&amp;uts_sem);
	else
		down_write(&amp;uts_sem);
	return which;
}

static void put_uts(struct ctl_tabletable, int write, voidwhich)
{
	if (!write)
		up_read(&amp;uts_sem);
	else
		up_write(&amp;uts_sem);
}

*/
	Special case of dostring for the UTS structure. This has locks
	to observe. Should this be in kernel/sys.c ????
 /*
static int proc_do_uts_string(struct ctl_tabletable, int write,
		  void __userbuffer, size_tlenp, loff_tppos)
{
	struct ctl_table uts_table;
	int r;
	memcpy(&amp;uts_table, table, sizeof(uts_table));
	uts_table.data = get_uts(table, write);
	r = proc_dostring(&amp;uts_table, write, buffer, lenp, ppos);
	put_uts(table, write, uts_table.data);

	if (write)
		proc_sys_poll_notify(table-&gt;poll);

	return r;
}
#else
#define proc_do_uts_string NULL
#endif

static DEFINE_CTL_TABLE_POLL(hostname_poll);
static DEFINE_CTL_TABLE_POLL(domainname_poll);

static struct ctl_table uts_kern_table[] = {
	{
		.procname	= &quot;ostype&quot;,
		.data		= init_uts_ns.name.sysname,
		.maxlen		= sizeof(init_uts_ns.name.sysname),
		.mode		= 0444,
		.proc_handler	= proc_do_uts_string,
	},
	{
		.procname	= &quot;osrelease&quot;,
		.data		= init_uts_ns.name.release,
		.maxlen		= sizeof(init_uts_ns.name.release),
		.mode		= 0444,
		.proc_handler	= proc_do_uts_string,
	},
	{
		.procname	= &quot;version&quot;,
		.data		= init_uts_ns.name.version,
		.maxlen		= sizeof(init_uts_ns.name.version),
		.mode		= 0444,
		.proc_handler	= proc_do_uts_string,
	},
	{
		.procname	= &quot;hostname&quot;,
		.data		= init_uts_ns.name.nodename,
		.maxlen		= sizeof(init_uts_ns.name.nodename),
		.mode		= 0644,
		.proc_handler	= proc_do_uts_string,
		.poll		= &amp;hostname_poll,
	},
	{
		.procname	= &quot;domainname&quot;,
		.data		= init_uts_ns.name.domainname,
		.maxlen		= sizeof(init_uts_ns.name.domainname),
		.mode		= 0644,
		.proc_handler	= proc_do_uts_string,
		.poll		= &amp;domainname_poll,
	},
	{}
};

static struct ctl_table uts_root_table[] = {
	{
		.procname	= &quot;kernel&quot;,
		.mode		= 0555,
		.child		= uts_kern_table,
	},
	{}
};

#ifdef CONFIG_PROC_SYSCTL
*/
 Notify userspace about a change in a certain entry of uts_kern_table,
 identified by the parameter proc.
 /*
void uts_proc_notify(enum uts_proc proc)
{
	struct ctl_tabletable = &amp;uts_kern_table[proc];

	proc_sys_poll_notify(table-&gt;poll);
}
#endif

static int __init utsname_sysctl_init(void)
{
	register_sysctl_table(uts_root_table);
	return 0;
}

device_initcall(utsname_sysctl_init);
*/
