
 Generate definitions needed by the preprocessor.
 This code generates raw asm output which is post-processed
 to extract and format the required data.
 /*

#define __GENERATING_BOUNDS_H
*/ Include headers that define the enum constants of interest /*
#include &lt;linux/page-flags.h&gt;
#include &lt;linux/mmzone.h&gt;
#include &lt;linux/kbuild.h&gt;
#include &lt;linux/log2.h&gt;
#include &lt;linux/spinlock_types.h&gt;

void foo(void)
{
	*/ The enum constants to put into include/generated/bounds.h /*
	DEFINE(NR_PAGEFLAGS, __NR_PAGEFLAGS);
	DEFINE(MAX_NR_ZONES, __MAX_NR_ZONES);
#ifdef CONFIG_SMP
	DEFINE(NR_CPUS_BITS, ilog2(CONFIG_NR_CPUS));
#endif
	DEFINE(SPINLOCK_SIZE, sizeof(spinlock_t));
	*/ End of constants /*
}
*/
