 audit_watch.c -- watching inodes

 Copyright 2003-2009 Red Hat, Inc.
 Copyright 2005 Hewlett-Packard Development Company, L.P.
 Copyright 2005 IBM Corporation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 /*

#include &lt;linux/kernel.h&gt;
#include &lt;linux/audit.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/fs.h&gt;
#include &lt;linux/fsnotify_backend.h&gt;
#include &lt;linux/namei.h&gt;
#include &lt;linux/netlink.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/security.h&gt;
#include &quot;audit.h&quot;

*/
 Reference counting:

 audit_parent: lifetime is from audit_init_parent() to receipt of an FS_IGNORED
 	event.  Each audit_watch holds a reference to its associated parent.

 audit_watch: if added to lists, lifetime is from audit_init_watch() to
 	audit_remove_watch().  Additionally, an audit_watch may exist
 	temporarily to assist in searching existing filter data.  Each
 	audit_krule holds a reference to its associated watch.
 /*

struct audit_watch {
	atomic_t		count;	*/ reference count /*
	dev_t			dev;	*/ associated superblock device /*
	char			*path;	*/ insertion path /*
	unsigned long		ino;	*/ associated inode number /*
	struct audit_parent	*parent;/ associated parent /*
	struct list_head	wlist;	*/ entry in parent-&gt;watches list /*
	struct list_head	rules;	*/ anchor for krule-&gt;rlist /*
};

struct audit_parent {
	struct list_head	watches;/ anchor for audit_watch-&gt;wlist /*
	struct fsnotify_mark mark;/ fsnotify mark on the inode /*
};

*/ fsnotify handle. /*
static struct fsnotify_groupaudit_watch_group;

*/ fsnotify events we care about. /*
#define AUDIT_FS_WATCH (FS_MOVE | FS_CREATE | FS_DELETE | FS_DELETE_SELF |\
			FS_MOVE_SELF | FS_EVENT_ON_CHILD)

static void audit_free_parent(struct audit_parentparent)
{
	WARN_ON(!list_empty(&amp;parent-&gt;watches));
	kfree(parent);
}

static void audit_watch_free_mark(struct fsnotify_markentry)
{
	struct audit_parentparent;

	parent = container_of(entry, struct audit_parent, mark);
	audit_free_parent(parent);
}

static void audit_get_parent(struct audit_parentparent)
{
	if (likely(parent))
		fsnotify_get_mark(&amp;parent-&gt;mark);
}

static void audit_put_parent(struct audit_parentparent)
{
	if (likely(parent))
		fsnotify_put_mark(&amp;parent-&gt;mark);
}

*/
 Find and return the audit_parent on the given inode.  If found a reference
 is taken on this parent.
 /*
static inline struct audit_parentaudit_find_parent(struct inodeinode)
{
	struct audit_parentparent = NULL;
	struct fsnotify_markentry;

	entry = fsnotify_find_inode_mark(audit_watch_group, inode);
	if (entry)
		parent = container_of(entry, struct audit_parent, mark);

	return parent;
}

void audit_get_watch(struct audit_watchwatch)
{
	atomic_inc(&amp;watch-&gt;count);
}

void audit_put_watch(struct audit_watchwatch)
{
	if (atomic_dec_and_test(&amp;watch-&gt;count)) {
		WARN_ON(watch-&gt;parent);
		WARN_ON(!list_empty(&amp;watch-&gt;rules));
		kfree(watch-&gt;path);
		kfree(watch);
	}
}

static void audit_remove_watch(struct audit_watchwatch)
{
	list_del(&amp;watch-&gt;wlist);
	audit_put_parent(watch-&gt;parent);
	watch-&gt;parent = NULL;
	audit_put_watch(watch);/ match initial get /*
}

charaudit_watch_path(struct audit_watchwatch)
{
	return watch-&gt;path;
}

int audit_watch_compare(struct audit_watchwatch, unsigned long ino, dev_t dev)
{
	return (watch-&gt;ino != AUDIT_INO_UNSET) &amp;&amp;
		(watch-&gt;ino == ino) &amp;&amp;
		(watch-&gt;dev == dev);
}

*/ Initialize a parent watch entry. /*
static struct audit_parentaudit_init_parent(struct pathpath)
{
	struct inodeinode = d_backing_inode(path-&gt;dentry);
	struct audit_parentparent;
	int ret;

	parent = kzalloc(sizeof(*parent), GFP_KERNEL);
	if (unlikely(!parent))
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&amp;parent-&gt;watches);

	fsnotify_init_mark(&amp;parent-&gt;mark, audit_watch_free_mark);
	parent-&gt;mark.mask = AUDIT_FS_WATCH;
	ret = fsnotify_add_mark(&amp;parent-&gt;mark, audit_watch_group, inode, NULL, 0);
	if (ret &lt; 0) {
		audit_free_parent(parent);
		return ERR_PTR(ret);
	}

	return parent;
}

*/ Initialize a watch entry. /*
static struct audit_watchaudit_init_watch(charpath)
{
	struct audit_watchwatch;

	watch = kzalloc(sizeof(*watch), GFP_KERNEL);
	if (unlikely(!watch))
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&amp;watch-&gt;rules);
	atomic_set(&amp;watch-&gt;count, 1);
	watch-&gt;path = path;
	watch-&gt;dev = AUDIT_DEV_UNSET;
	watch-&gt;ino = AUDIT_INO_UNSET;

	return watch;
}

*/ Translate a watch string to kernel representation. /*
int audit_to_watch(struct audit_krulekrule, charpath, int len, u32 op)
{
	struct audit_watchwatch;

	if (!audit_watch_group)
		return -EOPNOTSUPP;

	if (path[0] != &#39;/&#39; || path[len-1] == &#39;/&#39; ||
	    krule-&gt;listnr != AUDIT_FILTER_EXIT ||
	    op != Audit_equal ||
	    krule-&gt;inode_f || krule-&gt;watch || krule-&gt;tree)
		return -EINVAL;

	watch = audit_init_watch(path);
	if (IS_ERR(watch))
		return PTR_ERR(watch);

	krule-&gt;watch = watch;

	return 0;
}

*/ Duplicate the given audit watch.  The new watch&#39;s rules list is initialized
 to an empty list and wlist is undefined. /*
static struct audit_watchaudit_dupe_watch(struct audit_watchold)
{
	charpath;
	struct audit_watchnew;

	path = kstrdup(old-&gt;path, GFP_KERNEL);
	if (unlikely(!path))
		return ERR_PTR(-ENOMEM);

	new = audit_init_watch(path);
	if (IS_ERR(new)) {
		kfree(path);
		goto out;
	}

	new-&gt;dev = old-&gt;dev;
	new-&gt;ino = old-&gt;ino;
	audit_get_parent(old-&gt;parent);
	new-&gt;parent = old-&gt;parent;

out:
	return new;
}

static void audit_watch_log_rule_change(struct audit_kruler, struct audit_watchw, charop)
{
	if (audit_enabled) {
		struct audit_bufferab;
		ab = audit_log_start(NULL, GFP_NOFS, AUDIT_CONFIG_CHANGE);
		if (unlikely(!ab))
			return;
		audit_log_format(ab, &quot;auid=%u ses=%u op=&quot;,
				 from_kuid(&amp;init_user_ns, audit_get_loginuid(current)),
				 audit_get_sessionid(current));
		audit_log_string(ab, op);
		audit_log_format(ab, &quot; path=&quot;);
		audit_log_untrustedstring(ab, w-&gt;path);
		audit_log_key(ab, r-&gt;filterkey);
		audit_log_format(ab, &quot; list=%d res=1&quot;, r-&gt;listnr);
		audit_log_end(ab);
	}
}

*/ Update inode info in audit rules based on filesystem event. /*
static void audit_update_watch(struct audit_parentparent,
			       const chardname, dev_t dev,
			       unsigned long ino, unsigned invalidating)
{
	struct audit_watchowatch,nwatch,nextw;
	struct audit_kruler,nextr;
	struct audit_entryoentry,nentry;

	mutex_lock(&amp;audit_filter_mutex);
	*/ Run all of the watches on this parent looking for the one that
	 matches the given dname /*
	list_for_each_entry_safe(owatch, nextw, &amp;parent-&gt;watches, wlist) {
		if (audit_compare_dname_path(dname, owatch-&gt;path,
					     AUDIT_NAME_FULL))
			continue;

		*/ If the update involves invalidating rules, do the inode-based
		 filtering now, so we don&#39;t omit records. /*
		if (invalidating &amp;&amp; !audit_dummy_context())
			audit_filter_inodes(current, current-&gt;audit_context);

		*/ updating ino will likely change which audit_hash_list we
		 are on so we need a new watch for the new list /*
		nwatch = audit_dupe_watch(owatch);
		if (IS_ERR(nwatch)) {
			mutex_unlock(&amp;audit_filter_mutex);
			audit_panic(&quot;error updating watch, skipping&quot;);
			return;
		}
		nwatch-&gt;dev = dev;
		nwatch-&gt;ino = ino;

		list_for_each_entry_safe(r, nextr, &amp;owatch-&gt;rules, rlist) {

			oentry = container_of(r, struct audit_entry, rule);
			list_del(&amp;oentry-&gt;rule.rlist);
			list_del_rcu(&amp;oentry-&gt;list);

			nentry = audit_dupe_rule(&amp;oentry-&gt;rule);
			if (IS_ERR(nentry)) {
				list_del(&amp;oentry-&gt;rule.list);
				audit_panic(&quot;error updating watch, removing&quot;);
			} else {
				int h = audit_hash_ino((u32)ino);

				*/
				 nentry-&gt;rule.watch == oentry-&gt;rule.watch so
				 we must drop that reference and set it to our
				 new watch.
				 /*
				audit_put_watch(nentry-&gt;rule.watch);
				audit_get_watch(nwatch);
				nentry-&gt;rule.watch = nwatch;
				list_add(&amp;nentry-&gt;rule.rlist, &amp;nwatch-&gt;rules);
				list_add_rcu(&amp;nentry-&gt;list, &amp;audit_inode_hash[h]);
				list_replace(&amp;oentry-&gt;rule.list,
					     &amp;nentry-&gt;rule.list);
			}
			if (oentry-&gt;rule.exe)
				audit_remove_mark(oentry-&gt;rule.exe);

			audit_watch_log_rule_change(r, owatch, &quot;updated_rules&quot;);

			call_rcu(&amp;oentry-&gt;rcu, audit_free_rule_rcu);
		}

		audit_remove_watch(owatch);
		goto add_watch_to_parent;/ event applies to a single watch /*
	}
	mutex_unlock(&amp;audit_filter_mutex);
	return;

add_watch_to_parent:
	list_add(&amp;nwatch-&gt;wlist, &amp;parent-&gt;watches);
	mutex_unlock(&amp;audit_filter_mutex);
	return;
}

*/ Remove all watches &amp; rules associated with a parent that is going away. /*
static void audit_remove_parent_watches(struct audit_parentparent)
{
	struct audit_watchw,nextw;
	struct audit_kruler,nextr;
	struct audit_entrye;

	mutex_lock(&amp;audit_filter_mutex);
	list_for_each_entry_safe(w, nextw, &amp;parent-&gt;watches, wlist) {
		list_for_each_entry_safe(r, nextr, &amp;w-&gt;rules, rlist) {
			e = container_of(r, struct audit_entry, rule);
			audit_watch_log_rule_change(r, w, &quot;remove_rule&quot;);
			if (e-&gt;rule.exe)
				audit_remove_mark(e-&gt;rule.exe);
			list_del(&amp;r-&gt;rlist);
			list_del(&amp;r-&gt;list);
			list_del_rcu(&amp;e-&gt;list);
			call_rcu(&amp;e-&gt;rcu, audit_free_rule_rcu);
		}
		audit_remove_watch(w);
	}
	mutex_unlock(&amp;audit_filter_mutex);

	fsnotify_destroy_mark(&amp;parent-&gt;mark, audit_watch_group);
}

*/ Get path information necessary for adding watches. /*
static int audit_get_nd(struct audit_watchwatch, struct pathparent)
{
	struct dentryd = kern_path_locked(watch-&gt;path, parent);
	if (IS_ERR(d))
		return PTR_ERR(d);
	inode_unlock(d_backing_inode(parent-&gt;dentry));
	if (d_is_positive(d)) {
		*/ update watch filter fields /*
		watch-&gt;dev = d_backing_inode(d)-&gt;i_sb-&gt;s_dev;
		watch-&gt;ino = d_backing_inode(d)-&gt;i_ino;
	}
	dput(d);
	return 0;
}

*/ Associate the given rule with an existing parent.
 Caller must hold audit_filter_mutex. /*
static void audit_add_to_parent(struct audit_krulekrule,
				struct audit_parentparent)
{
	struct audit_watchw,watch = krule-&gt;watch;
	int watch_found = 0;

	BUG_ON(!mutex_is_locked(&amp;audit_filter_mutex));

	list_for_each_entry(w, &amp;parent-&gt;watches, wlist) {
		if (strcmp(watch-&gt;path, w-&gt;path))
			continue;

		watch_found = 1;

		*/ put krule&#39;s ref to temporary watch /*
		audit_put_watch(watch);

		audit_get_watch(w);
		krule-&gt;watch = watch = w;

		audit_put_parent(parent);
		break;
	}

	if (!watch_found) {
		watch-&gt;parent = parent;

		audit_get_watch(watch);
		list_add(&amp;watch-&gt;wlist, &amp;parent-&gt;watches);
	}
	list_add(&amp;krule-&gt;rlist, &amp;watch-&gt;rules);
}

*/ Find a matching watch entry, or add this one.
 Caller must hold audit_filter_mutex. /*
int audit_add_watch(struct audit_krulekrule, struct list_head*list)
{
	struct audit_watchwatch = krule-&gt;watch;
	struct audit_parentparent;
	struct path parent_path;
	int h, ret = 0;

	mutex_unlock(&amp;audit_filter_mutex);

	*/ Avoid calling path_lookup under audit_filter_mutex. /*
	ret = audit_get_nd(watch, &amp;parent_path);

	*/ caller expects mutex locked /*
	mutex_lock(&amp;audit_filter_mutex);

	if (ret)
		return ret;

	*/ either find an old parent or attach a new one /*
	parent = audit_find_parent(d_backing_inode(parent_path.dentry));
	if (!parent) {
		parent = audit_init_parent(&amp;parent_path);
		if (IS_ERR(parent)) {
			ret = PTR_ERR(parent);
			goto error;
		}
	}

	audit_add_to_parent(krule, parent);

	h = audit_hash_ino((u32)watch-&gt;ino);
	*list = &amp;audit_inode_hash[h];
error:
	path_put(&amp;parent_path);
	return ret;
}

void audit_remove_watch_rule(struct audit_krulekrule)
{
	struct audit_watchwatch = krule-&gt;watch;
	struct audit_parentparent = watch-&gt;parent;

	list_del(&amp;krule-&gt;rlist);

	if (list_empty(&amp;watch-&gt;rules)) {
		audit_remove_watch(watch);

		if (list_empty(&amp;parent-&gt;watches)) {
			audit_get_parent(parent);
			fsnotify_destroy_mark(&amp;parent-&gt;mark, audit_watch_group);
			audit_put_parent(parent);
		}
	}
}

*/ Update watch data in audit rules based on fsnotify events. /*
static int audit_watch_handle_event(struct fsnotify_groupgroup,
				    struct inodeto_tell,
				    struct fsnotify_markinode_mark,
				    struct fsnotify_markvfsmount_mark,
				    u32 mask, voiddata, int data_type,
				    const unsigned chardname, u32 cookie)
{
	struct inodeinode;
	struct audit_parentparent;

	parent = container_of(inode_mark, struct audit_parent, mark);

	BUG_ON(group != audit_watch_group);

	switch (data_type) {
	case (FSNOTIFY_EVENT_PATH):
		inode = d_backing_inode(((struct path)data)-&gt;dentry);
		break;
	case (FSNOTIFY_EVENT_INODE):
		inode = (struct inode)data;
		break;
	default:
		BUG();
		inode = NULL;
		break;
	};

	if (mask &amp; (FS_CREATE|FS_MOVED_TO) &amp;&amp; inode)
		audit_update_watch(parent, dname, inode-&gt;i_sb-&gt;s_dev, inode-&gt;i_ino, 0);
	else if (mask &amp; (FS_DELETE|FS_MOVED_FROM))
		audit_update_watch(parent, dname, AUDIT_DEV_UNSET, AUDIT_INO_UNSET, 1);
	else if (mask &amp; (FS_DELETE_SELF|FS_UNMOUNT|FS_MOVE_SELF))
		audit_remove_parent_watches(parent);

	return 0;
}

static const struct fsnotify_ops audit_watch_fsnotify_ops = {
	.handle_event = 	audit_watch_handle_event,
};

static int __init audit_watch_init(void)
{
	audit_watch_group = fsnotify_alloc_group(&amp;audit_watch_fsnotify_ops);
	if (IS_ERR(audit_watch_group)) {
		audit_watch_group = NULL;
		audit_panic(&quot;cannot create audit fsnotify group&quot;);
	}
	return 0;
}
device_initcall(audit_watch_init);

int audit_dupe_exe(struct audit_krulenew, struct audit_kruleold)
{
	struct audit_fsnotify_markaudit_mark;
	charpathname;

	pathname = kstrdup(audit_mark_path(old-&gt;exe), GFP_KERNEL);
	if (!pathname)
		return -ENOMEM;

	audit_mark = audit_alloc_mark(new, pathname, strlen(pathname));
	if (IS_ERR(audit_mark)) {
		kfree(pathname);
		return PTR_ERR(audit_mark);
	}
	new-&gt;exe = audit_mark;

	return 0;
}

int audit_exe_compare(struct task_structtsk, struct audit_fsnotify_markmark)
{
	struct fileexe_file;
	unsigned long ino;
	dev_t dev;

	rcu_read_lock();
	exe_file = rcu_dereference(tsk-&gt;mm-&gt;exe_file);
	ino = exe_file-&gt;f_inode-&gt;i_ino;
	dev = exe_file-&gt;f_inode-&gt;i_sb-&gt;s_dev;
	rcu_read_unlock();
	return audit_mark_compare(mark, ino, dev);
}
*/
