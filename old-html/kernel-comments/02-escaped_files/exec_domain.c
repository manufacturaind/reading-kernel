
 Handling of different ABIs (personalities).

 We group personalities into execution domains which have their
 own handlers for kernel entry points, signal mapping, etc...

 2001-05-06	Complete rewrite,  Christoph Hellwig (hch@infradead.org)
 /*

#include &lt;linux/init.h&gt;
#include &lt;linux/kernel.h&gt;
#include &lt;linux/kmod.h&gt;
#include &lt;linux/module.h&gt;
#include &lt;linux/personality.h&gt;
#include &lt;linux/proc_fs.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/seq_file.h&gt;
#include &lt;linux/syscalls.h&gt;
#include &lt;linux/sysctl.h&gt;
#include &lt;linux/types.h&gt;
#include &lt;linux/fs_struct.h&gt;

#ifdef CONFIG_PROC_FS
static int execdomains_proc_show(struct seq_filem, voidv)
{
	seq_puts(m, &quot;0-0\tLinux           \t[kernel]\n&quot;);
	return 0;
}

static int execdomains_proc_open(struct inodeinode, struct filefile)
{
	return single_open(file, execdomains_proc_show, NULL);
}

static const struct file_operations execdomains_proc_fops = {
	.open		= execdomains_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_execdomains_init(void)
{
	proc_create(&quot;execdomains&quot;, 0, NULL, &amp;execdomains_proc_fops);
	return 0;
}
module_init(proc_execdomains_init);
#endif

SYSCALL_DEFINE1(personality, unsigned int, personality)
{
	unsigned int old = current-&gt;personality;

	if (personality != 0xffffffff)
		set_personality(personality);

	return old;
}
*/
