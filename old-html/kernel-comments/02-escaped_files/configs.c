
 kernel/configs.c
 Echo the kernel .config file used to build the kernel

 Copyright (C) 2002 Khalid Aziz &lt;khalid_aziz@hp.com&gt;
 Copyright (C) 2002 Randy Dunlap &lt;rdunlap@xenotime.net&gt;
 Copyright (C) 2002 Al Stone &lt;ahs3@fc.hp.com&gt;
 Copyright (C) 2002 Hewlett-Packard Company

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 NON INFRINGEMENT.  See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 /*

#include &lt;linux/kernel.h&gt;
#include &lt;linux/module.h&gt;
#include &lt;linux/proc_fs.h&gt;
#include &lt;linux/seq_file.h&gt;
#include &lt;linux/init.h&gt;
#include &lt;asm/uaccess.h&gt;

*/***********************************************/
*/ the actual current config file                 /*

*/
 Define kernel_config_data and kernel_config_data_size, which contains the
 wrapped and compressed configuration file.  The file is first compressed
 with gzip and then bounded by two eight byte magic numbers to allow
 extraction from a binary kernel image:

   IKCFG_ST
   &lt;image&gt;
   IKCFG_ED
 /*
#define MAGIC_START	&quot;IKCFG_ST&quot;
#define MAGIC_END	&quot;IKCFG_ED&quot;
#include &quot;config_data.h&quot;


#define MAGIC_SIZE (sizeof(MAGIC_START) - 1)
#define kernel_config_data_size \
	(sizeof(kernel_config_data) - 1 - MAGIC_SIZE 2)

#ifdef CONFIG_IKCONFIG_PROC

static ssize_t
ikconfig_read_current(struct filefile, char __userbuf,
		      size_t len, loff_t offset)
{
	return simple_read_from_buffer(buf, len, offset,
				       kernel_config_data + MAGIC_SIZE,
				       kernel_config_data_size);
}

static const struct file_operations ikconfig_file_ops = {
	.owner = THIS_MODULE,
	.read = ikconfig_read_current,
	.llseek = default_llseek,
};

static int __init ikconfig_init(void)
{
	struct proc_dir_entryentry;

	*/ create the current config file /*
	entry = proc_create(&quot;config.gz&quot;, S_IFREG | S_IRUGO, NULL,
			    &amp;ikconfig_file_ops);
	if (!entry)
		return -ENOMEM;

	proc_set_size(entry, kernel_config_data_size);

	return 0;
}

static void __exit ikconfig_cleanup(void)
{
	remove_proc_entry(&quot;config.gz&quot;, NULL);
}

module_init(ikconfig_init);
module_exit(ikconfig_cleanup);

#endif */ CONFIG_IKCONFIG_PROC /*

MODULE_LICENSE(&quot;GPL&quot;);
MODULE_AUTHOR(&quot;Randy Dunlap&quot;);
MODULE_DESCRIPTION(&quot;Echo the kernel .config file used to build the kernel&quot;);
*/
