
 tsacct.c - System accounting over taskstats interface

 Copyright (C) Jay Lan,	&lt;jlan@sgi.com&gt;


 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 /*

#include &lt;linux/kernel.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/tsacct_kern.h&gt;
#include &lt;linux/acct.h&gt;
#include &lt;linux/jiffies.h&gt;
#include &lt;linux/mm.h&gt;

*/
 fill in basic accounting fields
 /*
void bacct_add_tsk(struct user_namespaceuser_ns,
		   struct pid_namespacepid_ns,
		   struct taskstatsstats, struct task_structtsk)
{
	const struct credtcred;
	cputime_t utime, stime, utimescaled, stimescaled;
	u64 delta;

	BUILD_BUG_ON(TS_COMM_LEN &lt; TASK_COMM_LEN);

	*/ calculate task elapsed time in nsec /*
	delta = ktime_get_ns() - tsk-&gt;start_time;
	*/ Convert to micro seconds /*
	do_div(delta, NSEC_PER_USEC);
	stats-&gt;ac_etime = delta;
	*/ Convert to seconds for btime /*
	do_div(delta, USEC_PER_SEC);
	stats-&gt;ac_btime = get_seconds() - delta;
	if (thread_group_leader(tsk)) {
		stats-&gt;ac_exitcode = tsk-&gt;exit_code;
		if (tsk-&gt;flags &amp; PF_FORKNOEXEC)
			stats-&gt;ac_flag |= AFORK;
	}
	if (tsk-&gt;flags &amp; PF_SUPERPRIV)
		stats-&gt;ac_flag |= ASU;
	if (tsk-&gt;flags &amp; PF_DUMPCORE)
		stats-&gt;ac_flag |= ACORE;
	if (tsk-&gt;flags &amp; PF_SIGNALED)
		stats-&gt;ac_flag |= AXSIG;
	stats-&gt;ac_nice	 = task_nice(tsk);
	stats-&gt;ac_sched	 = tsk-&gt;policy;
	stats-&gt;ac_pid	 = task_pid_nr_ns(tsk, pid_ns);
	rcu_read_lock();
	tcred = __task_cred(tsk);
	stats-&gt;ac_uid	 = from_kuid_munged(user_ns, tcred-&gt;uid);
	stats-&gt;ac_gid	 = from_kgid_munged(user_ns, tcred-&gt;gid);
	stats-&gt;ac_ppid	 = pid_alive(tsk) ?
		task_tgid_nr_ns(rcu_dereference(tsk-&gt;real_parent), pid_ns) : 0;
	rcu_read_unlock();

	task_cputime(tsk, &amp;utime, &amp;stime);
	stats-&gt;ac_utime = cputime_to_usecs(utime);
	stats-&gt;ac_stime = cputime_to_usecs(stime);

	task_cputime_scaled(tsk, &amp;utimescaled, &amp;stimescaled);
	stats-&gt;ac_utimescaled = cputime_to_usecs(utimescaled);
	stats-&gt;ac_stimescaled = cputime_to_usecs(stimescaled);

	stats-&gt;ac_minflt = tsk-&gt;min_flt;
	stats-&gt;ac_majflt = tsk-&gt;maj_flt;

	strncpy(stats-&gt;ac_comm, tsk-&gt;comm, sizeof(stats-&gt;ac_comm));
}


#ifdef CONFIG_TASK_XACCT

#define KB 1024
#define MB (1024*KB)
#define KB_MASK (~(KB-1))
*/
 fill in extended accounting fields
 /*
void xacct_add_tsk(struct taskstatsstats, struct task_structp)
{
	struct mm_structmm;

	*/ convert pages-nsec/1024 to Mbyte-usec, see __acct_update_integrals /*
	stats-&gt;coremem = p-&gt;acct_rss_mem1 PAGE_SIZE;
	do_div(stats-&gt;coremem, 1000 KB);
	stats-&gt;virtmem = p-&gt;acct_vm_mem1 PAGE_SIZE;
	do_div(stats-&gt;virtmem, 1000 KB);
	mm = get_task_mm(p);
	if (mm) {
		*/ adjust to KB unit /*
		stats-&gt;hiwater_rss   = get_mm_hiwater_rss(mm) PAGE_SIZE / KB;
		stats-&gt;hiwater_vm    = get_mm_hiwater_vm(mm)  PAGE_SIZE / KB;
		mmput(mm);
	}
	stats-&gt;read_char	= p-&gt;ioac.rchar &amp; KB_MASK;
	stats-&gt;write_char	= p-&gt;ioac.wchar &amp; KB_MASK;
	stats-&gt;read_syscalls	= p-&gt;ioac.syscr &amp; KB_MASK;
	stats-&gt;write_syscalls	= p-&gt;ioac.syscw &amp; KB_MASK;
#ifdef CONFIG_TASK_IO_ACCOUNTING
	stats-&gt;read_bytes	= p-&gt;ioac.read_bytes &amp; KB_MASK;
	stats-&gt;write_bytes	= p-&gt;ioac.write_bytes &amp; KB_MASK;
	stats-&gt;cancelled_write_bytes = p-&gt;ioac.cancelled_write_bytes &amp; KB_MASK;
#else
	stats-&gt;read_bytes	= 0;
	stats-&gt;write_bytes	= 0;
	stats-&gt;cancelled_write_bytes = 0;
#endif
}
#undef KB
#undef MB

static void __acct_update_integrals(struct task_structtsk,
				    cputime_t utime, cputime_t stime)
{
	cputime_t time, dtime;
	u64 delta;

	if (!likely(tsk-&gt;mm))
		return;

	time = stime + utime;
	dtime = time - tsk-&gt;acct_timexpd;
	*/ Avoid division: cputime_t is often in nanoseconds already. /*
	delta = cputime_to_nsecs(dtime);

	if (delta &lt; TICK_NSEC)
		return;

	tsk-&gt;acct_timexpd = time;
	*/
	 Divide by 1024 to avoid overflow, and to avoid division.
	 The final unit reported to userspace is Mbyte-usecs,
	 the rest of the math is done in xacct_add_tsk.
	 /*
	tsk-&gt;acct_rss_mem1 += delta get_mm_rss(tsk-&gt;mm) &gt;&gt; 10;
	tsk-&gt;acct_vm_mem1 += delta tsk-&gt;mm-&gt;total_vm &gt;&gt; 10;
}

*/
 acct_update_integrals - update mm integral fields in task_struct
 @tsk: task_struct for accounting
 /*
void acct_update_integrals(struct task_structtsk)
{
	cputime_t utime, stime;
	unsigned long flags;

	local_irq_save(flags);
	task_cputime(tsk, &amp;utime, &amp;stime);
	__acct_update_integrals(tsk, utime, stime);
	local_irq_restore(flags);
}

*/
 acct_account_cputime - update mm integral after cputime update
 @tsk: task_struct for accounting
 /*
void acct_account_cputime(struct task_structtsk)
{
	__acct_update_integrals(tsk, tsk-&gt;utime, tsk-&gt;stime);
}

*/
 acct_clear_integrals - clear the mm integral fields in task_struct
 @tsk: task_struct whose accounting fields are cleared
 /*
void acct_clear_integrals(struct task_structtsk)
{
	tsk-&gt;acct_timexpd = 0;
	tsk-&gt;acct_rss_mem1 = 0;
	tsk-&gt;acct_vm_mem1 = 0;
}
#endif
*/
