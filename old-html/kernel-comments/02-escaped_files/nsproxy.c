
  Copyright (C) 2006 IBM Corporation

  Author: Serge Hallyn &lt;serue@us.ibm.com&gt;

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, version 2 of the
  License.

  Jun 2006 - namespaces support
             OpenVZ, SWsoft Inc.
             Pavel Emelianov &lt;xemul@openvz.org&gt;
 /*

#include &lt;linux/slab.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/nsproxy.h&gt;
#include &lt;linux/init_task.h&gt;
#include &lt;linux/mnt_namespace.h&gt;
#include &lt;linux/utsname.h&gt;
#include &lt;linux/pid_namespace.h&gt;
#include &lt;net/net_namespace.h&gt;
#include &lt;linux/ipc_namespace.h&gt;
#include &lt;linux/proc_ns.h&gt;
#include &lt;linux/file.h&gt;
#include &lt;linux/syscalls.h&gt;
#include &lt;linux/cgroup.h&gt;

static struct kmem_cachensproxy_cachep;

struct nsproxy init_nsproxy = {
	.count			= ATOMIC_INIT(1),
	.uts_ns			= &amp;init_uts_ns,
#if defined(CONFIG_POSIX_MQUEUE) || defined(CONFIG_SYSVIPC)
	.ipc_ns			= &amp;init_ipc_ns,
#endif
	.mnt_ns			= NULL,
	.pid_ns_for_children	= &amp;init_pid_ns,
#ifdef CONFIG_NET
	.net_ns			= &amp;init_net,
#endif
#ifdef CONFIG_CGROUPS
	.cgroup_ns		= &amp;init_cgroup_ns,
#endif
};

static inline struct nsproxycreate_nsproxy(void)
{
	struct nsproxynsproxy;

	nsproxy = kmem_cache_alloc(nsproxy_cachep, GFP_KERNEL);
	if (nsproxy)
		atomic_set(&amp;nsproxy-&gt;count, 1);
	return nsproxy;
}

*/
 Create new nsproxy and all of its the associated namespaces.
 Return the newly created nsproxy.  Do not attach this to the task,
 leave it to the caller to do proper locking and attach it to task.
 /*
static struct nsproxycreate_new_namespaces(unsigned long flags,
	struct task_structtsk, struct user_namespaceuser_ns,
	struct fs_structnew_fs)
{
	struct nsproxynew_nsp;
	int err;

	new_nsp = create_nsproxy();
	if (!new_nsp)
		return ERR_PTR(-ENOMEM);

	new_nsp-&gt;mnt_ns = copy_mnt_ns(flags, tsk-&gt;nsproxy-&gt;mnt_ns, user_ns, new_fs);
	if (IS_ERR(new_nsp-&gt;mnt_ns)) {
		err = PTR_ERR(new_nsp-&gt;mnt_ns);
		goto out_ns;
	}

	new_nsp-&gt;uts_ns = copy_utsname(flags, user_ns, tsk-&gt;nsproxy-&gt;uts_ns);
	if (IS_ERR(new_nsp-&gt;uts_ns)) {
		err = PTR_ERR(new_nsp-&gt;uts_ns);
		goto out_uts;
	}

	new_nsp-&gt;ipc_ns = copy_ipcs(flags, user_ns, tsk-&gt;nsproxy-&gt;ipc_ns);
	if (IS_ERR(new_nsp-&gt;ipc_ns)) {
		err = PTR_ERR(new_nsp-&gt;ipc_ns);
		goto out_ipc;
	}

	new_nsp-&gt;pid_ns_for_children =
		copy_pid_ns(flags, user_ns, tsk-&gt;nsproxy-&gt;pid_ns_for_children);
	if (IS_ERR(new_nsp-&gt;pid_ns_for_children)) {
		err = PTR_ERR(new_nsp-&gt;pid_ns_for_children);
		goto out_pid;
	}

	new_nsp-&gt;cgroup_ns = copy_cgroup_ns(flags, user_ns,
					    tsk-&gt;nsproxy-&gt;cgroup_ns);
	if (IS_ERR(new_nsp-&gt;cgroup_ns)) {
		err = PTR_ERR(new_nsp-&gt;cgroup_ns);
		goto out_cgroup;
	}

	new_nsp-&gt;net_ns = copy_net_ns(flags, user_ns, tsk-&gt;nsproxy-&gt;net_ns);
	if (IS_ERR(new_nsp-&gt;net_ns)) {
		err = PTR_ERR(new_nsp-&gt;net_ns);
		goto out_net;
	}

	return new_nsp;

out_net:
	put_cgroup_ns(new_nsp-&gt;cgroup_ns);
out_cgroup:
	if (new_nsp-&gt;pid_ns_for_children)
		put_pid_ns(new_nsp-&gt;pid_ns_for_children);
out_pid:
	if (new_nsp-&gt;ipc_ns)
		put_ipc_ns(new_nsp-&gt;ipc_ns);
out_ipc:
	if (new_nsp-&gt;uts_ns)
		put_uts_ns(new_nsp-&gt;uts_ns);
out_uts:
	if (new_nsp-&gt;mnt_ns)
		put_mnt_ns(new_nsp-&gt;mnt_ns);
out_ns:
	kmem_cache_free(nsproxy_cachep, new_nsp);
	return ERR_PTR(err);
}

*/
 called from clone.  This now handles copy for nsproxy and all
 namespaces therein.
 /*
int copy_namespaces(unsigned long flags, struct task_structtsk)
{
	struct nsproxyold_ns = tsk-&gt;nsproxy;
	struct user_namespaceuser_ns = task_cred_xxx(tsk, user_ns);
	struct nsproxynew_ns;

	if (likely(!(flags &amp; (CLONE_NEWNS | CLONE_NEWUTS | CLONE_NEWIPC |
			      CLONE_NEWPID | CLONE_NEWNET |
			      CLONE_NEWCGROUP)))) {
		get_nsproxy(old_ns);
		return 0;
	}

	if (!ns_capable(user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	*/
	 CLONE_NEWIPC must detach from the undolist: after switching
	 to a new ipc namespace, the semaphore arrays from the old
	 namespace are unreachable.  In clone parlance, CLONE_SYSVSEM
	 means share undolist with parent, so we must forbid using
	 it along with CLONE_NEWIPC.
	 /*
	if ((flags &amp; (CLONE_NEWIPC | CLONE_SYSVSEM)) ==
		(CLONE_NEWIPC | CLONE_SYSVSEM)) 
		return -EINVAL;

	new_ns = create_new_namespaces(flags, tsk, user_ns, tsk-&gt;fs);
	if (IS_ERR(new_ns))
		return  PTR_ERR(new_ns);

	tsk-&gt;nsproxy = new_ns;
	return 0;
}

void free_nsproxy(struct nsproxyns)
{
	if (ns-&gt;mnt_ns)
		put_mnt_ns(ns-&gt;mnt_ns);
	if (ns-&gt;uts_ns)
		put_uts_ns(ns-&gt;uts_ns);
	if (ns-&gt;ipc_ns)
		put_ipc_ns(ns-&gt;ipc_ns);
	if (ns-&gt;pid_ns_for_children)
		put_pid_ns(ns-&gt;pid_ns_for_children);
	put_cgroup_ns(ns-&gt;cgroup_ns);
	put_net(ns-&gt;net_ns);
	kmem_cache_free(nsproxy_cachep, ns);
}

*/
 Called from unshare. Unshare all the namespaces part of nsproxy.
 On success, returns the new nsproxy.
 /*
int unshare_nsproxy_namespaces(unsigned long unshare_flags,
	struct nsproxy*new_nsp, struct crednew_cred, struct fs_structnew_fs)
{
	struct user_namespaceuser_ns;
	int err = 0;

	if (!(unshare_flags &amp; (CLONE_NEWNS | CLONE_NEWUTS | CLONE_NEWIPC |
			       CLONE_NEWNET | CLONE_NEWPID | CLONE_NEWCGROUP)))
		return 0;

	user_ns = new_cred ? new_cred-&gt;user_ns : current_user_ns();
	if (!ns_capable(user_ns, CAP_SYS_ADMIN))
		return -EPERM;

	*new_nsp = create_new_namespaces(unshare_flags, current, user_ns,
					 new_fs ? new_fs : current-&gt;fs);
	if (IS_ERR(*new_nsp)) {
		err = PTR_ERR(*new_nsp);
		goto out;
	}

out:
	return err;
}

void switch_task_namespaces(struct task_structp, struct nsproxynew)
{
	struct nsproxyns;

	might_sleep();

	task_lock(p);
	ns = p-&gt;nsproxy;
	p-&gt;nsproxy = new;
	task_unlock(p);

	if (ns &amp;&amp; atomic_dec_and_test(&amp;ns-&gt;count))
		free_nsproxy(ns);
}

void exit_task_namespaces(struct task_structp)
{
	switch_task_namespaces(p, NULL);
}

SYSCALL_DEFINE2(setns, int, fd, int, nstype)
{
	struct task_structtsk = current;
	struct nsproxynew_nsproxy;
	struct filefile;
	struct ns_commonns;
	int err;

	file = proc_ns_fget(fd);
	if (IS_ERR(file))
		return PTR_ERR(file);

	err = -EINVAL;
	ns = get_proc_ns(file_inode(file));
	if (nstype &amp;&amp; (ns-&gt;ops-&gt;type != nstype))
		goto out;

	new_nsproxy = create_new_namespaces(0, tsk, current_user_ns(), tsk-&gt;fs);
	if (IS_ERR(new_nsproxy)) {
		err = PTR_ERR(new_nsproxy);
		goto out;
	}

	err = ns-&gt;ops-&gt;install(new_nsproxy, ns);
	if (err) {
		free_nsproxy(new_nsproxy);
		goto out;
	}
	switch_task_namespaces(tsk, new_nsproxy);
out:
	fput(file);
	return err;
}

int __init nsproxy_cache_init(void)
{
	nsproxy_cachep = KMEM_CACHE(nsproxy, SLAB_PANIC);
	return 0;
}
*/
