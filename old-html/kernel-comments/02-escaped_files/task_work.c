/*
#include &lt;linux/spinlock.h&gt;
#include &lt;linux/task_work.h&gt;
#include &lt;linux/tracehook.h&gt;

static struct callback_head work_exited;/ all we need is -&gt;next == NULL /*

*/
 task_work_add - ask the @task to execute @work-&gt;func()
 @task: the task which should run the callback
 @work: the callback to run
 @notify: send the notification if true

 Queue @work for task_work_run() below and notify the @task if @notify.
 Fails if the @task is exiting/exited and thus it can&#39;t process this @work.
 Otherwise @work-&gt;func() will be called when the @task returns from kernel
 mode or exits.

 This is like the signal handler which runs in kernel mode, but it doesn&#39;t
 try to wake up the @task.

 Note: there is no ordering guarantee on works queued here.

 RETURNS:
 0 if succeeds or -ESRCH.
 /*
int
task_work_add(struct task_structtask, struct callback_headwork, bool notify)
{
	struct callback_headhead;

	do {
		head = ACCESS_ONCE(task-&gt;task_works);
		if (unlikely(head == &amp;work_exited))
			return -ESRCH;
		work-&gt;next = head;
	} while (cmpxchg(&amp;task-&gt;task_works, head, work) != head);

	if (notify)
		set_notify_resume(task);
	return 0;
}

*/
 task_work_cancel - cancel a pending work added by task_work_add()
 @task: the task which should execute the work
 @func: identifies the work to remove

 Find the last queued pending work with -&gt;func == @func and remove
 it from queue.

 RETURNS:
 The found work or NULL if not found.
 /*
struct callback_head
task_work_cancel(struct task_structtask, task_work_func_t func)
{
	struct callback_head*pprev = &amp;task-&gt;task_works;
	struct callback_headwork;
	unsigned long flags;
	*/
	 If cmpxchg() fails we continue without updating pprev.
	 Either we raced with task_work_add() which added the
	 new entry before this work, we will find it again. Or
	 we raced with task_work_run(),pprev == NULL/exited.
	 /*
	raw_spin_lock_irqsave(&amp;task-&gt;pi_lock, flags);
	while ((work = ACCESS_ONCE(*pprev))) {
		smp_read_barrier_depends();
		if (work-&gt;func != func)
			pprev = &amp;work-&gt;next;
		else if (cmpxchg(pprev, work, work-&gt;next) == work)
			break;
	}
	raw_spin_unlock_irqrestore(&amp;task-&gt;pi_lock, flags);

	return work;
}

*/
 task_work_run - execute the works added by task_work_add()

 Flush the pending works. Should be used by the core kernel code.
 Called before the task returns to the user-mode or stops, or when
 it exits. In the latter case task_work_add() can no longer add the
 new work after task_work_run() returns.
 /*
void task_work_run(void)
{
	struct task_structtask = current;
	struct callback_headwork,head,next;

	for (;;) {
		*/
		 work-&gt;func() can do task_work_add(), do not set
		 work_exited unless the list is empty.
		 /*
		do {
			work = ACCESS_ONCE(task-&gt;task_works);
			head = !work &amp;&amp; (task-&gt;flags &amp; PF_EXITING) ?
				&amp;work_exited : NULL;
		} while (cmpxchg(&amp;task-&gt;task_works, work, head) != work);

		if (!work)
			break;
		*/
		 Synchronize with task_work_cancel(). It can&#39;t remove
		 the first entry == work, cmpxchg(task_works) should
		 fail, but it can play withwork and other entries.
		 /*
		raw_spin_unlock_wait(&amp;task-&gt;pi_lock);
		smp_mb();

		do {
			next = work-&gt;next;
			work-&gt;func(work);
			work = next;
			cond_resched();
		} while (work);
	}
}
*/
