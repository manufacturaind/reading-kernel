
  Copyright (C) 2004 IBM Corporation

  Author: Serge Hallyn &lt;serue@us.ibm.com&gt;

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, version 2 of the
  License.
 /*

#include &lt;linux/export.h&gt;
#include &lt;linux/uts.h&gt;
#include &lt;linux/utsname.h&gt;
#include &lt;linux/err.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/user_namespace.h&gt;
#include &lt;linux/proc_ns.h&gt;

static struct uts_namespacecreate_uts_ns(void)
{
	struct uts_namespaceuts_ns;

	uts_ns = kmalloc(sizeof(struct uts_namespace), GFP_KERNEL);
	if (uts_ns)
		kref_init(&amp;uts_ns-&gt;kref);
	return uts_ns;
}

*/
 Clone a new ns copying an original utsname, setting refcount to 1
 @old_ns: namespace to clone
 Return ERR_PTR(-ENOMEM) on error (failure to kmalloc), new ns otherwise
 /*
static struct uts_namespaceclone_uts_ns(struct user_namespaceuser_ns,
					  struct uts_namespaceold_ns)
{
	struct uts_namespacens;
	int err;

	ns = create_uts_ns();
	if (!ns)
		return ERR_PTR(-ENOMEM);

	err = ns_alloc_inum(&amp;ns-&gt;ns);
	if (err) {
		kfree(ns);
		return ERR_PTR(err);
	}

	ns-&gt;ns.ops = &amp;utsns_operations;

	down_read(&amp;uts_sem);
	memcpy(&amp;ns-&gt;name, &amp;old_ns-&gt;name, sizeof(ns-&gt;name));
	ns-&gt;user_ns = get_user_ns(user_ns);
	up_read(&amp;uts_sem);
	return ns;
}

*/
 Copy task tsk&#39;s utsname namespace, or clone it if flags
 specifies CLONE_NEWUTS.  In latter case, changes to the
 utsname of this process won&#39;t be seen by parent, and vice
 versa.
 /*
struct uts_namespacecopy_utsname(unsigned long flags,
	struct user_namespaceuser_ns, struct uts_namespaceold_ns)
{
	struct uts_namespacenew_ns;

	BUG_ON(!old_ns);
	get_uts_ns(old_ns);

	if (!(flags &amp; CLONE_NEWUTS))
		return old_ns;

	new_ns = clone_uts_ns(user_ns, old_ns);

	put_uts_ns(old_ns);
	return new_ns;
}

void free_uts_ns(struct krefkref)
{
	struct uts_namespacens;

	ns = container_of(kref, struct uts_namespace, kref);
	put_user_ns(ns-&gt;user_ns);
	ns_free_inum(&amp;ns-&gt;ns);
	kfree(ns);
}

static inline struct uts_namespaceto_uts_ns(struct ns_commonns)
{
	return container_of(ns, struct uts_namespace, ns);
}

static struct ns_commonutsns_get(struct task_structtask)
{
	struct uts_namespacens = NULL;
	struct nsproxynsproxy;

	task_lock(task);
	nsproxy = task-&gt;nsproxy;
	if (nsproxy) {
		ns = nsproxy-&gt;uts_ns;
		get_uts_ns(ns);
	}
	task_unlock(task);

	return ns ? &amp;ns-&gt;ns : NULL;
}

static void utsns_put(struct ns_commonns)
{
	put_uts_ns(to_uts_ns(ns));
}

static int utsns_install(struct nsproxynsproxy, struct ns_commonnew)
{
	struct uts_namespacens = to_uts_ns(new);

	if (!ns_capable(ns-&gt;user_ns, CAP_SYS_ADMIN) ||
	    !ns_capable(current_user_ns(), CAP_SYS_ADMIN))
		return -EPERM;

	get_uts_ns(ns);
	put_uts_ns(nsproxy-&gt;uts_ns);
	nsproxy-&gt;uts_ns = ns;
	return 0;
}

const struct proc_ns_operations utsns_operations = {
	.name		= &quot;uts&quot;,
	.type		= CLONE_NEWUTS,
	.get		= utsns_get,
	.put		= utsns_put,
	.install	= utsns_install,
};
*/
