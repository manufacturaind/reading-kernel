
 kernel/stacktrace.c

 Stack trace management functions

  Copyright (C) 2006 Red Hat, Inc., Ingo Molnar &lt;mingo@redhat.com&gt;
 /*
#include &lt;linux/sched.h&gt;
#include &lt;linux/kernel.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/kallsyms.h&gt;
#include &lt;linux/stacktrace.h&gt;

void print_stack_trace(struct stack_tracetrace, int spaces)
{
	int i;

	if (WARN_ON(!trace-&gt;entries))
		return;

	for (i = 0; i &lt; trace-&gt;nr_entries; i++) {
		printk(&quot;%*c&quot;, 1 + spaces, &#39; &#39;);
		print_ip_sym(trace-&gt;entries[i]);
	}
}
EXPORT_SYMBOL_GPL(print_stack_trace);

int snprint_stack_trace(charbuf, size_t size,
			struct stack_tracetrace, int spaces)
{
	int i;
	unsigned long ip;
	int generated;
	int total = 0;

	if (WARN_ON(!trace-&gt;entries))
		return 0;

	for (i = 0; i &lt; trace-&gt;nr_entries; i++) {
		ip = trace-&gt;entries[i];
		generated = snprintf(buf, size, &quot;%*c[&lt;%p&gt;] %pS\n&quot;,
				1 + spaces, &#39; &#39;, (void) ip, (void) ip);

		total += generated;

		*/ Assume that generated isn&#39;t a negative number /*
		if (generated &gt;= size) {
			buf += size;
			size = 0;
		} else {
			buf += generated;
			size -= generated;
		}
	}

	return total;
}
EXPORT_SYMBOL_GPL(snprint_stack_trace);

*/
 Architectures that do not implement save_stack_trace_tsk or
 save_stack_trace_regs get this weak alias and a once-per-bootup warning
 (whenever this facility is utilized - for example by procfs):
 /*
__weak void
save_stack_trace_tsk(struct task_structtsk, struct stack_tracetrace)
{
	WARN_ONCE(1, KERN_INFO &quot;save_stack_trace_tsk() not implemented yet.\n&quot;);
}

__weak void
save_stack_trace_regs(struct pt_regsregs, struct stack_tracetrace)
{
	WARN_ONCE(1, KERN_INFO &quot;save_stack_trace_regs() not implemented yet.\n&quot;);
}
*/
