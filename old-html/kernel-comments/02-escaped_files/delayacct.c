 delayacct.c - per-task delay accounting

 Copyright (C) Shailabh Nagar, IBM Corp. 2006

 This program is free software;  you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it would be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 the GNU General Public License for more details.
 /*

#include &lt;linux/sched.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/taskstats.h&gt;
#include &lt;linux/time.h&gt;
#include &lt;linux/sysctl.h&gt;
#include &lt;linux/delayacct.h&gt;
#include &lt;linux/module.h&gt;

int delayacct_on __read_mostly = 1;	*/ Delay accounting turned on/off /*
EXPORT_SYMBOL_GPL(delayacct_on);
struct kmem_cachedelayacct_cache;

static int __init delayacct_setup_disable(charstr)
{
	delayacct_on = 0;
	return 1;
}
__setup(&quot;nodelayacct&quot;, delayacct_setup_disable);

void delayacct_init(void)
{
	delayacct_cache = KMEM_CACHE(task_delay_info, SLAB_PANIC|SLAB_ACCOUNT);
	delayacct_tsk_init(&amp;init_task);
}

void __delayacct_tsk_init(struct task_structtsk)
{
	tsk-&gt;delays = kmem_cache_zalloc(delayacct_cache, GFP_KERNEL);
	if (tsk-&gt;delays)
		spin_lock_init(&amp;tsk-&gt;delays-&gt;lock);
}

*/
 Finish delay accounting for a statistic using its timestamps (@start),
 accumalator (@total) and @count
 /*
static void delayacct_end(u64start, u64total, u32count)
{
	s64 ns = ktime_get_ns() -start;
	unsigned long flags;

	if (ns &gt; 0) {
		spin_lock_irqsave(&amp;current-&gt;delays-&gt;lock, flags);
		*total += ns;
		(*count)++;
		spin_unlock_irqrestore(&amp;current-&gt;delays-&gt;lock, flags);
	}
}

void __delayacct_blkio_start(void)
{
	current-&gt;delays-&gt;blkio_start = ktime_get_ns();
}

void __delayacct_blkio_end(void)
{
	if (current-&gt;delays-&gt;flags &amp; DELAYACCT_PF_SWAPIN)
		*/ Swapin block I/O /*
		delayacct_end(&amp;current-&gt;delays-&gt;blkio_start,
			&amp;current-&gt;delays-&gt;swapin_delay,
			&amp;current-&gt;delays-&gt;swapin_count);
	else	*/ Other block I/O /*
		delayacct_end(&amp;current-&gt;delays-&gt;blkio_start,
			&amp;current-&gt;delays-&gt;blkio_delay,
			&amp;current-&gt;delays-&gt;blkio_count);
}

int __delayacct_add_tsk(struct taskstatsd, struct task_structtsk)
{
	cputime_t utime, stime, stimescaled, utimescaled;
	unsigned long long t2, t3;
	unsigned long flags, t1;
	s64 tmp;

	task_cputime(tsk, &amp;utime, &amp;stime);
	tmp = (s64)d-&gt;cpu_run_real_total;
	tmp += cputime_to_nsecs(utime + stime);
	d-&gt;cpu_run_real_total = (tmp &lt; (s64)d-&gt;cpu_run_real_total) ? 0 : tmp;

	task_cputime_scaled(tsk, &amp;utimescaled, &amp;stimescaled);
	tmp = (s64)d-&gt;cpu_scaled_run_real_total;
	tmp += cputime_to_nsecs(utimescaled + stimescaled);
	d-&gt;cpu_scaled_run_real_total =
		(tmp &lt; (s64)d-&gt;cpu_scaled_run_real_total) ? 0 : tmp;

	*/
	 No locking available for sched_info (and too expensive to add one)
	 Mitigate by taking snapshot of values
	 /*
	t1 = tsk-&gt;sched_info.pcount;
	t2 = tsk-&gt;sched_info.run_delay;
	t3 = tsk-&gt;se.sum_exec_runtime;

	d-&gt;cpu_count += t1;

	tmp = (s64)d-&gt;cpu_delay_total + t2;
	d-&gt;cpu_delay_total = (tmp &lt; (s64)d-&gt;cpu_delay_total) ? 0 : tmp;

	tmp = (s64)d-&gt;cpu_run_virtual_total + t3;
	d-&gt;cpu_run_virtual_total =
		(tmp &lt; (s64)d-&gt;cpu_run_virtual_total) ?	0 : tmp;

	*/ zero XXX_total, non-zero XXX_count implies XXX stat overflowed /*

	spin_lock_irqsave(&amp;tsk-&gt;delays-&gt;lock, flags);
	tmp = d-&gt;blkio_delay_total + tsk-&gt;delays-&gt;blkio_delay;
	d-&gt;blkio_delay_total = (tmp &lt; d-&gt;blkio_delay_total) ? 0 : tmp;
	tmp = d-&gt;swapin_delay_total + tsk-&gt;delays-&gt;swapin_delay;
	d-&gt;swapin_delay_total = (tmp &lt; d-&gt;swapin_delay_total) ? 0 : tmp;
	tmp = d-&gt;freepages_delay_total + tsk-&gt;delays-&gt;freepages_delay;
	d-&gt;freepages_delay_total = (tmp &lt; d-&gt;freepages_delay_total) ? 0 : tmp;
	d-&gt;blkio_count += tsk-&gt;delays-&gt;blkio_count;
	d-&gt;swapin_count += tsk-&gt;delays-&gt;swapin_count;
	d-&gt;freepages_count += tsk-&gt;delays-&gt;freepages_count;
	spin_unlock_irqrestore(&amp;tsk-&gt;delays-&gt;lock, flags);

	return 0;
}

__u64 __delayacct_blkio_ticks(struct task_structtsk)
{
	__u64 ret;
	unsigned long flags;

	spin_lock_irqsave(&amp;tsk-&gt;delays-&gt;lock, flags);
	ret = nsec_to_clock_t(tsk-&gt;delays-&gt;blkio_delay +
				tsk-&gt;delays-&gt;swapin_delay);
	spin_unlock_irqrestore(&amp;tsk-&gt;delays-&gt;lock, flags);
	return ret;
}

void __delayacct_freepages_start(void)
{
	current-&gt;delays-&gt;freepages_start = ktime_get_ns();
}

void __delayacct_freepages_end(void)
{
	delayacct_end(&amp;current-&gt;delays-&gt;freepages_start,
			&amp;current-&gt;delays-&gt;freepages_delay,
			&amp;current-&gt;delays-&gt;freepages_count);
}
*/
