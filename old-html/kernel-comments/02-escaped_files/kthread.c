   Kernel thread helper functions.
   Copyright (C) 2004 IBM Corporation, Rusty Russell.

 Creation is done via kthreadd, so that we get a clean environment
 even if we&#39;re invoked from userspace (think modprobe, hotplug cpu,
 etc.).
 /*
#include &lt;linux/sched.h&gt;
#include &lt;linux/kthread.h&gt;
#include &lt;linux/completion.h&gt;
#include &lt;linux/err.h&gt;
#include &lt;linux/cpuset.h&gt;
#include &lt;linux/unistd.h&gt;
#include &lt;linux/file.h&gt;
#include &lt;linux/export.h&gt;
#include &lt;linux/mutex.h&gt;
#include &lt;linux/slab.h&gt;
#include &lt;linux/freezer.h&gt;
#include &lt;linux/ptrace.h&gt;
#include &lt;linux/uaccess.h&gt;
#include &lt;trace/events/sched.h&gt;

static DEFINE_SPINLOCK(kthread_create_lock);
static LIST_HEAD(kthread_create_list);
struct task_structkthreadd_task;

struct kthread_create_info
{
	*/ Information passed to kthread() from kthreadd. /*
	int (*threadfn)(voiddata);
	voiddata;
	int node;

	*/ Result passed back to kthread_create() from kthreadd. /*
	struct task_structresult;
	struct completiondone;

	struct list_head list;
};

struct kthread {
	unsigned long flags;
	unsigned int cpu;
	voiddata;
	struct completion parked;
	struct completion exited;
};

enum KTHREAD_BITS {
	KTHREAD_IS_PER_CPU = 0,
	KTHREAD_SHOULD_STOP,
	KTHREAD_SHOULD_PARK,
	KTHREAD_IS_PARKED,
};

#define __to_kthread(vfork)	\
	container_of(vfork, struct kthread, exited)

static inline struct kthreadto_kthread(struct task_structk)
{
	return __to_kthread(k-&gt;vfork_done);
}

static struct kthreadto_live_kthread(struct task_structk)
{
	struct completionvfork = ACCESS_ONCE(k-&gt;vfork_done);
	if (likely(vfork))
		return __to_kthread(vfork);
	return NULL;
}

*/
 kthread_should_stop - should this kthread return now?

 When someone calls kthread_stop() on your kthread, it will be woken
 and this will return true.  You should then return, and your return
 value will be passed through to kthread_stop().
 /*
bool kthread_should_stop(void)
{
	return test_bit(KTHREAD_SHOULD_STOP, &amp;to_kthread(current)-&gt;flags);
}
EXPORT_SYMBOL(kthread_should_stop);

*/
 kthread_should_park - should this kthread park now?

 When someone calls kthread_park() on your kthread, it will be woken
 and this will return true.  You should then do the necessary
 cleanup and call kthread_parkme()

 Similar to kthread_should_stop(), but this keeps the thread alive
 and in a park position. kthread_unpark() &quot;restarts&quot; the thread and
 calls the thread function again.
 /*
bool kthread_should_park(void)
{
	return test_bit(KTHREAD_SHOULD_PARK, &amp;to_kthread(current)-&gt;flags);
}
EXPORT_SYMBOL_GPL(kthread_should_park);

*/
 kthread_freezable_should_stop - should this freezable kthread return now?
 @was_frozen: optional out parameter, indicates whether %current was frozen

 kthread_should_stop() for freezable kthreads, which will enter
 refrigerator if necessary.  This function is safe from kthread_stop() /
 freezer deadlock and freezable kthreads should use this function instead
 of calling try_to_freeze() directly.
 /*
bool kthread_freezable_should_stop(boolwas_frozen)
{
	bool frozen = false;

	might_sleep();

	if (unlikely(freezing(current)))
		frozen = __refrigerator(true);

	if (was_frozen)
		*was_frozen = frozen;

	return kthread_should_stop();
}
EXPORT_SYMBOL_GPL(kthread_freezable_should_stop);

*/
 kthread_data - return data value specified on kthread creation
 @task: kthread task in question

 Return the data value specified when kthread @task was created.
 The caller is responsible for ensuring the validity of @task when
 calling this function.
 /*
voidkthread_data(struct task_structtask)
{
	return to_kthread(task)-&gt;data;
}

*/
 probe_kthread_data - speculative version of kthread_data()
 @task: possible kthread task in question

 @task could be a kthread task.  Return the data value specified when it
 was created if accessible.  If @task isn&#39;t a kthread task or its data is
 inaccessible for any reason, %NULL is returned.  This function requires
 that @task itself is safe to dereference.
 /*
voidprobe_kthread_data(struct task_structtask)
{
	struct kthreadkthread = to_kthread(task);
	voiddata = NULL;

	probe_kernel_read(&amp;data, &amp;kthread-&gt;data, sizeof(data));
	return data;
}

static void __kthread_parkme(struct kthreadself)
{
	__set_current_state(TASK_PARKED);
	while (test_bit(KTHREAD_SHOULD_PARK, &amp;self-&gt;flags)) {
		if (!test_and_set_bit(KTHREAD_IS_PARKED, &amp;self-&gt;flags))
			complete(&amp;self-&gt;parked);
		schedule();
		__set_current_state(TASK_PARKED);
	}
	clear_bit(KTHREAD_IS_PARKED, &amp;self-&gt;flags);
	__set_current_state(TASK_RUNNING);
}

void kthread_parkme(void)
{
	__kthread_parkme(to_kthread(current));
}
EXPORT_SYMBOL_GPL(kthread_parkme);

static int kthread(void_create)
{
	*/ Copy data: it&#39;s on kthread&#39;s stack /*
	struct kthread_create_infocreate = _create;
	int (*threadfn)(voiddata) = create-&gt;threadfn;
	voiddata = create-&gt;data;
	struct completiondone;
	struct kthread self;
	int ret;

	self.flags = 0;
	self.data = data;
	init_completion(&amp;self.exited);
	init_completion(&amp;self.parked);
	current-&gt;vfork_done = &amp;self.exited;

	*/ If user was SIGKILLed, I release the structure. /*
	done = xchg(&amp;create-&gt;done, NULL);
	if (!done) {
		kfree(create);
		do_exit(-EINTR);
	}
	*/ OK, tell user we&#39;re spawned, wait for stop or wakeup /*
	__set_current_state(TASK_UNINTERRUPTIBLE);
	create-&gt;result = current;
	complete(done);
	schedule();

	ret = -EINTR;

	if (!test_bit(KTHREAD_SHOULD_STOP, &amp;self.flags)) {
		__kthread_parkme(&amp;self);
		ret = threadfn(data);
	}
	*/ we can&#39;t just return, we must preserve &quot;self&quot; on stack /*
	do_exit(ret);
}

*/ called from do_fork() to get node information for about to be created task /*
int tsk_fork_get_node(struct task_structtsk)
{
#ifdef CONFIG_NUMA
	if (tsk == kthreadd_task)
		return tsk-&gt;pref_node_fork;
#endif
	return NUMA_NO_NODE;
}

static void create_kthread(struct kthread_create_infocreate)
{
	int pid;

#ifdef CONFIG_NUMA
	current-&gt;pref_node_fork = create-&gt;node;
#endif
	*/ We want our own signal handler (we take no signals by default). /*
	pid = kernel_thread(kthread, create, CLONE_FS | CLONE_FILES | SIGCHLD);
	if (pid &lt; 0) {
		*/ If user was SIGKILLed, I release the structure. /*
		struct completiondone = xchg(&amp;create-&gt;done, NULL);

		if (!done) {
			kfree(create);
			return;
		}
		create-&gt;result = ERR_PTR(pid);
		complete(done);
	}
}

*/
 kthread_create_on_node - create a kthread.
 @threadfn: the function to run until signal_pending(current).
 @data: data ptr for @threadfn.
 @node: task and thread structures for the thread are allocated on this node
 @namefmt: printf-style name for the thread.

 Description: This helper function creates and names a kernel
 thread.  The thread will be stopped: use wake_up_process() to start
 it.  See also kthread_run().  The new thread has SCHED_NORMAL policy and
 is affine to all CPUs.

 If thread is going to be bound on a particular cpu, give its node
 in @node, to get NUMA affinity for kthread stack, or else give NUMA_NO_NODE.
 When woken, the thread will run @threadfn() with @data as its
 argument. @threadfn() can either call do_exit() directly if it is a
 standalone thread for which no one will call kthread_stop(), or
 return when &#39;kthread_should_stop()&#39; is true (which means
 kthread_stop() has been called).  The return value should be zero
 or a negative error number; it will be passed to kthread_stop().

 Returns a task_struct or ERR_PTR(-ENOMEM) or ERR_PTR(-EINTR).
 /*
struct task_structkthread_create_on_node(int (*threadfn)(voiddata),
					   voiddata, int node,
					   const char namefmt[],
					   ...)
{
	DECLARE_COMPLETION_ONSTACK(done);
	struct task_structtask;
	struct kthread_create_infocreate = kmalloc(sizeof(*create),
						     GFP_KERNEL);

	if (!create)
		return ERR_PTR(-ENOMEM);
	create-&gt;threadfn = threadfn;
	create-&gt;data = data;
	create-&gt;node = node;
	create-&gt;done = &amp;done;

	spin_lock(&amp;kthread_create_lock);
	list_add_tail(&amp;create-&gt;list, &amp;kthread_create_list);
	spin_unlock(&amp;kthread_create_lock);

	wake_up_process(kthreadd_task);
	*/
	 Wait for completion in killable state, for I might be chosen by
	 the OOM killer while kthreadd is trying to allocate memory for
	 new kernel thread.
	 /*
	if (unlikely(wait_for_completion_killable(&amp;done))) {
		*/
		 If I was SIGKILLed before kthreadd (or new kernel thread)
		 calls complete(), leave the cleanup of this structure to
		 that thread.
		 /*
		if (xchg(&amp;create-&gt;done, NULL))
			return ERR_PTR(-EINTR);
		*/
		 kthreadd (or new kernel thread) will call complete()
		 shortly.
		 /*
		wait_for_completion(&amp;done);
	}
	task = create-&gt;result;
	if (!IS_ERR(task)) {
		static const struct sched_param param = { .sched_priority = 0 };
		va_list args;

		va_start(args, namefmt);
		vsnprintf(task-&gt;comm, sizeof(task-&gt;comm), namefmt, args);
		va_end(args);
		*/
		 root may have changed our (kthreadd&#39;s) priority or CPU mask.
		 The kernel thread should not inherit these properties.
		 /*
		sched_setscheduler_nocheck(task, SCHED_NORMAL, &amp;param);
		set_cpus_allowed_ptr(task, cpu_all_mask);
	}
	kfree(create);
	return task;
}
EXPORT_SYMBOL(kthread_create_on_node);

static void __kthread_bind_mask(struct task_structp, const struct cpumaskmask, long state)
{
	unsigned long flags;

	if (!wait_task_inactive(p, state)) {
		WARN_ON(1);
		return;
	}

	*/ It&#39;s safe because the task is inactive. /*
	raw_spin_lock_irqsave(&amp;p-&gt;pi_lock, flags);
	do_set_cpus_allowed(p, mask);
	p-&gt;flags |= PF_NO_SETAFFINITY;
	raw_spin_unlock_irqrestore(&amp;p-&gt;pi_lock, flags);
}

static void __kthread_bind(struct task_structp, unsigned int cpu, long state)
{
	__kthread_bind_mask(p, cpumask_of(cpu), state);
}

void kthread_bind_mask(struct task_structp, const struct cpumaskmask)
{
	__kthread_bind_mask(p, mask, TASK_UNINTERRUPTIBLE);
}

*/
 kthread_bind - bind a just-created kthread to a cpu.
 @p: thread created by kthread_create().
 @cpu: cpu (might not be online, must be possible) for @k to run on.

 Description: This function is equivalent to set_cpus_allowed(),
 except that @cpu doesn&#39;t need to be online, and the thread must be
 stopped (i.e., just returned from kthread_create()).
 /*
void kthread_bind(struct task_structp, unsigned int cpu)
{
	__kthread_bind(p, cpu, TASK_UNINTERRUPTIBLE);
}
EXPORT_SYMBOL(kthread_bind);

*/
 kthread_create_on_cpu - Create a cpu bound kthread
 @threadfn: the function to run until signal_pending(current).
 @data: data ptr for @threadfn.
 @cpu: The cpu on which the thread should be bound,
 @namefmt: printf-style name for the thread. Format is restricted
	     to &quot;name.*%u&quot;. Code fills in cpu number.

 Description: This helper function creates and names a kernel thread
 The thread will be woken and put into park mode.
 /*
struct task_structkthread_create_on_cpu(int (*threadfn)(voiddata),
					  voiddata, unsigned int cpu,
					  const charnamefmt)
{
	struct task_structp;

	p = kthread_create_on_node(threadfn, data, cpu_to_node(cpu), namefmt,
				   cpu);
	if (IS_ERR(p))
		return p;
	set_bit(KTHREAD_IS_PER_CPU, &amp;to_kthread(p)-&gt;flags);
	to_kthread(p)-&gt;cpu = cpu;
	*/ Park the thread to get it out of TASK_UNINTERRUPTIBLE state /*
	kthread_park(p);
	return p;
}

static void __kthread_unpark(struct task_structk, struct kthreadkthread)
{
	clear_bit(KTHREAD_SHOULD_PARK, &amp;kthread-&gt;flags);
	*/
	 We clear the IS_PARKED bit here as we don&#39;t wait
	 until the task has left the park code. So if we&#39;d
	 park before that happens we&#39;d see the IS_PARKED bit
	 which might be about to be cleared.
	 /*
	if (test_and_clear_bit(KTHREAD_IS_PARKED, &amp;kthread-&gt;flags)) {
		if (test_bit(KTHREAD_IS_PER_CPU, &amp;kthread-&gt;flags))
			__kthread_bind(k, kthread-&gt;cpu, TASK_PARKED);
		wake_up_state(k, TASK_PARKED);
	}
}

*/
 kthread_unpark - unpark a thread created by kthread_create().
 @k:		thread created by kthread_create().

 Sets kthread_should_park() for @k to return false, wakes it, and
 waits for it to return. If the thread is marked percpu then its
 bound to the cpu again.
 /*
void kthread_unpark(struct task_structk)
{
	struct kthreadkthread = to_live_kthread(k);

	if (kthread)
		__kthread_unpark(k, kthread);
}
EXPORT_SYMBOL_GPL(kthread_unpark);

*/
 kthread_park - park a thread created by kthread_create().
 @k: thread created by kthread_create().

 Sets kthread_should_park() for @k to return true, wakes it, and
 waits for it to return. This can also be called after kthread_create()
 instead of calling wake_up_process(): the thread will park without
 calling threadfn().

 Returns 0 if the thread is parked, -ENOSYS if the thread exited.
 If called by the kthread itself just the park bit is set.
 /*
int kthread_park(struct task_structk)
{
	struct kthreadkthread = to_live_kthread(k);
	int ret = -ENOSYS;

	if (kthread) {
		if (!test_bit(KTHREAD_IS_PARKED, &amp;kthread-&gt;flags)) {
			set_bit(KTHREAD_SHOULD_PARK, &amp;kthread-&gt;flags);
			if (k != current) {
				wake_up_process(k);
				wait_for_completion(&amp;kthread-&gt;parked);
			}
		}
		ret = 0;
	}
	return ret;
}
EXPORT_SYMBOL_GPL(kthread_park);

*/
 kthread_stop - stop a thread created by kthread_create().
 @k: thread created by kthread_create().

 Sets kthread_should_stop() for @k to return true, wakes it, and
 waits for it to exit. This can also be called after kthread_create()
 instead of calling wake_up_process(): the thread will exit without
 calling threadfn().

 If threadfn() may call do_exit() itself, the caller must ensure
 task_struct can&#39;t go away.

 Returns the result of threadfn(), or %-EINTR if wake_up_process()
 was never called.
 /*
int kthread_stop(struct task_structk)
{
	struct kthreadkthread;
	int ret;

	trace_sched_kthread_stop(k);

	get_task_struct(k);
	kthread = to_live_kthread(k);
	if (kthread) {
		set_bit(KTHREAD_SHOULD_STOP, &amp;kthread-&gt;flags);
		__kthread_unpark(k, kthread);
		wake_up_process(k);
		wait_for_completion(&amp;kthread-&gt;exited);
	}
	ret = k-&gt;exit_code;
	put_task_struct(k);

	trace_sched_kthread_stop_ret(ret);
	return ret;
}
EXPORT_SYMBOL(kthread_stop);

int kthreadd(voidunused)
{
	struct task_structtsk = current;

	*/ Setup a clean context for our children to inherit. /*
	set_task_comm(tsk, &quot;kthreadd&quot;);
	ignore_signals(tsk);
	set_cpus_allowed_ptr(tsk, cpu_all_mask);
	set_mems_allowed(node_states[N_MEMORY]);

	current-&gt;flags |= PF_NOFREEZE;

	for (;;) {
		set_current_state(TASK_INTERRUPTIBLE);
		if (list_empty(&amp;kthread_create_list))
			schedule();
		__set_current_state(TASK_RUNNING);

		spin_lock(&amp;kthread_create_lock);
		while (!list_empty(&amp;kthread_create_list)) {
			struct kthread_create_infocreate;

			create = list_entry(kthread_create_list.next,
					    struct kthread_create_info, list);
			list_del_init(&amp;create-&gt;list);
			spin_unlock(&amp;kthread_create_lock);

			create_kthread(create);

			spin_lock(&amp;kthread_create_lock);
		}
		spin_unlock(&amp;kthread_create_lock);
	}

	return 0;
}

void __init_kthread_worker(struct kthread_workerworker,
				const charname,
				struct lock_class_keykey)
{
	spin_lock_init(&amp;worker-&gt;lock);
	lockdep_set_class_and_name(&amp;worker-&gt;lock, key, name);
	INIT_LIST_HEAD(&amp;worker-&gt;work_list);
	worker-&gt;task = NULL;
}
EXPORT_SYMBOL_GPL(__init_kthread_worker);

*/
 kthread_worker_fn - kthread function to process kthread_worker
 @worker_ptr: pointer to initialized kthread_worker

 This function can be used as @threadfn to kthread_create() or
 kthread_run() with @worker_ptr argument pointing to an initialized
 kthread_worker.  The started kthread will process work_list until
 the it is stopped with kthread_stop().  A kthread can also call
 this function directly after extra initialization.

 Different kthreads can be used for the same kthread_worker as long
 as there&#39;s only one kthread attached to it at any given time.  A
 kthread_worker without an attached kthread simply collects queued
 kthread_works.
 /*
int kthread_worker_fn(voidworker_ptr)
{
	struct kthread_workerworker = worker_ptr;
	struct kthread_workwork;

	WARN_ON(worker-&gt;task);
	worker-&gt;task = current;
repeat:
	set_current_state(TASK_INTERRUPTIBLE);	*/ mb paired w/ kthread_stop /*

	if (kthread_should_stop()) {
		__set_current_state(TASK_RUNNING);
		spin_lock_irq(&amp;worker-&gt;lock);
		worker-&gt;task = NULL;
		spin_unlock_irq(&amp;worker-&gt;lock);
		return 0;
	}

	work = NULL;
	spin_lock_irq(&amp;worker-&gt;lock);
	if (!list_empty(&amp;worker-&gt;work_list)) {
		work = list_first_entry(&amp;worker-&gt;work_list,
					struct kthread_work, node);
		list_del_init(&amp;work-&gt;node);
	}
	worker-&gt;current_work = work;
	spin_unlock_irq(&amp;worker-&gt;lock);

	if (work) {
		__set_current_state(TASK_RUNNING);
		work-&gt;func(work);
	} else if (!freezing(current))
		schedule();

	try_to_freeze();
	goto repeat;
}
EXPORT_SYMBOL_GPL(kthread_worker_fn);

*/ insert @work before @pos in @worker /*
static void insert_kthread_work(struct kthread_workerworker,
			       struct kthread_workwork,
			       struct list_headpos)
{
	lockdep_assert_held(&amp;worker-&gt;lock);

	list_add_tail(&amp;work-&gt;node, pos);
	work-&gt;worker = worker;
	if (!worker-&gt;current_work &amp;&amp; likely(worker-&gt;task))
		wake_up_process(worker-&gt;task);
}

*/
 queue_kthread_work - queue a kthread_work
 @worker: target kthread_worker
 @work: kthread_work to queue

 Queue @work to work processor @task for async execution.  @task
 must have been created with kthread_worker_create().  Returns %true
 if @work was successfully queued, %false if it was already pending.
 /*
bool queue_kthread_work(struct kthread_workerworker,
			struct kthread_workwork)
{
	bool ret = false;
	unsigned long flags;

	spin_lock_irqsave(&amp;worker-&gt;lock, flags);
	if (list_empty(&amp;work-&gt;node)) {
		insert_kthread_work(worker, work, &amp;worker-&gt;work_list);
		ret = true;
	}
	spin_unlock_irqrestore(&amp;worker-&gt;lock, flags);
	return ret;
}
EXPORT_SYMBOL_GPL(queue_kthread_work);

struct kthread_flush_work {
	struct kthread_work	work;
	struct completion	done;
};

static void kthread_flush_work_fn(struct kthread_workwork)
{
	struct kthread_flush_workfwork =
		container_of(work, struct kthread_flush_work, work);
	complete(&amp;fwork-&gt;done);
}

*/
 flush_kthread_work - flush a kthread_work
 @work: work to flush

 If @work is queued or executing, wait for it to finish execution.
 /*
void flush_kthread_work(struct kthread_workwork)
{
	struct kthread_flush_work fwork = {
		KTHREAD_WORK_INIT(fwork.work, kthread_flush_work_fn),
		COMPLETION_INITIALIZER_ONSTACK(fwork.done),
	};
	struct kthread_workerworker;
	bool noop = false;

retry:
	worker = work-&gt;worker;
	if (!worker)
		return;

	spin_lock_irq(&amp;worker-&gt;lock);
	if (work-&gt;worker != worker) {
		spin_unlock_irq(&amp;worker-&gt;lock);
		goto retry;
	}

	if (!list_empty(&amp;work-&gt;node))
		insert_kthread_work(worker, &amp;fwork.work, work-&gt;node.next);
	else if (worker-&gt;current_work == work)
		insert_kthread_work(worker, &amp;fwork.work, worker-&gt;work_list.next);
	else
		noop = true;

	spin_unlock_irq(&amp;worker-&gt;lock);

	if (!noop)
		wait_for_completion(&amp;fwork.done);
}
EXPORT_SYMBOL_GPL(flush_kthread_work);

*/
 flush_kthread_worker - flush all current works on a kthread_worker
 @worker: worker to flush

 Wait until all currently executing or pending works on @worker are
 finished.
 /*
void flush_kthread_worker(struct kthread_workerworker)
{
	struct kthread_flush_work fwork = {
		KTHREAD_WORK_INIT(fwork.work, kthread_flush_work_fn),
		COMPLETION_INITIALIZER_ONSTACK(fwork.done),
	};

	queue_kthread_work(worker, &amp;fwork.work);
	wait_for_completion(&amp;fwork.done);
}
EXPORT_SYMBOL_GPL(flush_kthread_worker);
*/
