
 Simple stack backtrace regression test module

 (C) Copyright 2008 Intel Corporation
 Author: Arjan van de Ven &lt;arjan@linux.intel.com&gt;

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2
 of the License.
 /*

#include &lt;linux/completion.h&gt;
#include &lt;linux/delay.h&gt;
#include &lt;linux/interrupt.h&gt;
#include &lt;linux/module.h&gt;
#include &lt;linux/sched.h&gt;
#include &lt;linux/stacktrace.h&gt;

static void backtrace_test_normal(void)
{
	pr_info(&quot;Testing a backtrace from process context.\n&quot;);
	pr_info(&quot;The following trace is a kernel self test and not a bug!\n&quot;);

	dump_stack();
}

static DECLARE_COMPLETION(backtrace_work);

static void backtrace_test_irq_callback(unsigned long data)
{
	dump_stack();
	complete(&amp;backtrace_work);
}

static DECLARE_TASKLET(backtrace_tasklet, &amp;backtrace_test_irq_callback, 0);

static void backtrace_test_irq(void)
{
	pr_info(&quot;Testing a backtrace from irq context.\n&quot;);
	pr_info(&quot;The following trace is a kernel self test and not a bug!\n&quot;);

	init_completion(&amp;backtrace_work);
	tasklet_schedule(&amp;backtrace_tasklet);
	wait_for_completion(&amp;backtrace_work);
}

#ifdef CONFIG_STACKTRACE
static void backtrace_test_saved(void)
{
	struct stack_trace trace;
	unsigned long entries[8];

	pr_info(&quot;Testing a saved backtrace.\n&quot;);
	pr_info(&quot;The following trace is a kernel self test and not a bug!\n&quot;);

	trace.nr_entries = 0;
	trace.max_entries = ARRAY_SIZE(entries);
	trace.entries = entries;
	trace.skip = 0;

	save_stack_trace(&amp;trace);
	print_stack_trace(&amp;trace, 0);
}
#else
static void backtrace_test_saved(void)
{
	pr_info(&quot;Saved backtrace test skipped.\n&quot;);
}
#endif

static int backtrace_regression_test(void)
{
	pr_info(&quot;====[ backtrace testing ]===========\n&quot;);

	backtrace_test_normal();
	backtrace_test_irq();
	backtrace_test_saved();

	pr_info(&quot;====[ end of backtrace testing ]====\n&quot;);
	return 0;
}

static void exitf(void)
{
}

module_init(backtrace_regression_test);
module_exit(exitf);
MODULE_LICENSE(&quot;GPL&quot;);
MODULE_AUTHOR(&quot;Arjan van de Ven &lt;arjan@linux.intel.com&gt;&quot;);
*/
