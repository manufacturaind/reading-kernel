default: build

build: c2html buildhtml buildpdf

c2html:
	# temp dirs for the generated HTML
	mkdir build-01-html-initial build-02-html-full -p
	# kernel/*.c -> build-01-html-initial/*.html
	cd kernel; for f in *.c; do \
	  . ../.env/bin/activate; bash ../scripts/c2html.sh $$f ../build-01-html-initial/$${f%.c}.html; \
	done

buildhtml:
	# join it with the HTML header and footer
	cd build-01-html-initial; for f in *.html; do \
	    cat ../assets/_header.html $$f ../assets/_footer.html > ../build-02-html-full/$${f%.html}-text.html; \
	    rm -f ../build-02-html-full/$${f%.html}-cover.html; \
	    sh -c "cat ../assets/_header-cover.html && echo '<h1>'$${f%.html}'</h1>' && cat ../assets/_footer.html" > ../build-02-html-full/$${f%.html}-cover.html; \
	done
	# copy static assets to the HTML dir
	mkdir build-02-html-full/css build-02-html-full/fonts -p
	cp assets/*.css build-02-html-full/css
	cp assets/*.ttf build-02-html-full/fonts
	# remove temp dir
	# rm -fr build-01-html-initial

buildpdf:
	mkdir -p build-03-pdf-parts
	mkdir -p pdf
	# run Weasyprint to convert HTML into PDF
	cd build-02-html-full; for f in *.html; do \
	    . ../.env/bin/activate; weasyprint $$f $${f%.html}.pdf; \
	done
	mv build-02-html-full/*.pdf build-03-pdf-parts
	# join all PDF files into one
	pdftk build-03-pdf-parts/*.pdf cat output pdf/book.pdf

install:
	# local virtualenv for dependencies
	virtualenv .env --no-site-packages --distribute --prompt=\(kern\)
	. `pwd`/.env/bin/activate; pip install -r requirements.txt
	# install the fonts we use on the user's system
	mkdir -p ~/.fonts/weasyprint
	cp assets/*.otf ~/.fonts/weasyprint

clean:
	rm -fr build-01-html-initial build-02-html-full build-03-pdf-parts

